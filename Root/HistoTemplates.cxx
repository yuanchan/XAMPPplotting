#include <TError.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>
#include <TMath.h>
#include <iostream>
#include <sstream>

#include <PathResolver/PathResolver.h>
#include <XAMPPplotting/HistoTemplates.h>
#include <XAMPPplotting/PlottingUtils.h>
namespace XAMPP {
    HistoTemplates* HistoTemplates::m_DB = nullptr;
    HistoTemplates* HistoTemplates::getHistoTemplater() {
        if (!m_DB) m_DB = new HistoTemplates();
        return m_DB;
    }
    bool HistoTemplates::IsTemplateDefined(const std::string& Name) const { return m_Templates.find(Name) != m_Templates.end(); }
    HistoTemplates::HistoTemplates() : m_Templates(), m_ImportedCfgs() { TH1::SetDefaultSumw2(); }
    HistoTemplates::~HistoTemplates() {
        m_Templates.clear();
        m_DB = nullptr;
    }
    bool HistoTemplates::Create1D(const std::string& Line) {
        std::stringstream sstr(Line);
        std::string Name;
        int n_Bins(0);
        double lowEdge(0), highEdge(0);
        while (Name.find("1DHisto") < Name.size() || Name.empty()) { sstr >> Name; }
        sstr >> n_Bins >> lowEdge >> highEdge;
        if (lowEdge > highEdge) {
            Error("HistoTemplates::Create1D()", Form("The low edge %f is larger than the upper edge %f", lowEdge, highEdge));
            return false;
        }
        if (m_Templates.find(Name) != m_Templates.end()) {
            Error("HistoTemplates::Create1D()", "The template " + Name + " already exists.");
            return false;
        }
        std::shared_ptr<TH1> H =
            std::make_shared<TH1D>(Form("temp_%s", Name.c_str()), Form("temp_%s", Name.c_str()), n_Bins, lowEdge, highEdge);
        H->SetDirectory(nullptr);
        m_Templates.insert(std::pair<std::string, std::shared_ptr<TH1>>(Name, H));
        Info("HistoTemplates::Create1D()", "New template " + Name + " with " + std::to_string(n_Bins) + " bins ranging between " +
                                               std::to_string(lowEdge) + " and " + std::to_string(highEdge));
        return true;
    }
    bool HistoTemplates::Create2D(const std::string& Line) {
        std::stringstream sstr(Line);
        std::string Name;
        int n_BinsX(0), n_BinsY(0);
        double lowEdgeX(0.), highEdgeX(0.), lowEdgeY(0.), highEdgeY(0.);
        while (Name.find("2DHisto") < Name.size() || Name.empty()) { sstr >> Name; }
        sstr >> n_BinsX >> lowEdgeX >> highEdgeX >> n_BinsY >> lowEdgeY >> highEdgeY;
        if (lowEdgeX > highEdgeX) {
            Error("HistoTemplates::Create2D()", Form("The low edge %f is larger than the upper edge %f", lowEdgeY, highEdgeX));
            return false;
        }
        if (lowEdgeY > highEdgeY) {
            Error("HistoTemplates::Create2D()", Form("The low edge %f is larger than the upper edge %f", lowEdgeY, highEdgeY));
            return false;
        }
        if (m_Templates.find(Name) != m_Templates.end()) {
            Error("HistoTemplates::Create2D()", "The template " + Name + " already exists.");
            return false;
        }
        std::shared_ptr<TH1> H = std::make_shared<TH2D>(Form("temp_%s", Name.c_str()), Form("temp_%s", Name.c_str()), n_BinsX, lowEdgeX,
                                                        highEdgeX, n_BinsY, lowEdgeY, highEdgeY);
        H->SetDirectory(nullptr);
        m_Templates.insert(std::pair<std::string, std::shared_ptr<TH1>>(Name, H));
        Info("HistoTemplates::Create2D()", "New 2D template " + Name + " with " + std::to_string(n_BinsX) + " and " +
                                               std::to_string(n_BinsY) + " bins ranging between " + std::to_string(lowEdgeX) + " - " +
                                               std::to_string(highEdgeX) + " and " + std::to_string(lowEdgeY) + " - " +
                                               std::to_string(highEdgeY));
        return true;
    }
    bool HistoTemplates::Create3D(const std::string& Line) {
        std::stringstream sstr(Line);
        std::string Name;
        int n_BinsX(0), n_BinsY(0), n_BinsZ(0.);
        double lowEdgeX(0.), highEdgeX(0.), lowEdgeY(0.), highEdgeY(0.), lowEdgeZ(0.), highEdgeZ(0.);
        while (Name.find("3DHisto") < Name.size() || Name.empty()) { sstr >> Name; }
        sstr >> n_BinsX >> lowEdgeX >> highEdgeX >> n_BinsY >> lowEdgeY >> highEdgeY >> n_BinsZ >> lowEdgeZ >> highEdgeZ;
        if (lowEdgeX > highEdgeX) {
            Error("HistoTemplates::Create3D()", Form("The low edge %f is larger than the upper edge %f", lowEdgeY, highEdgeX));
            return false;
        }
        if (lowEdgeY > highEdgeY) {
            Error("HistoTemplates::Create3D()", Form("The low edge %f is larger than the upper edge %f", lowEdgeY, highEdgeY));
            return false;
        }
        if (lowEdgeZ > highEdgeZ) {
            Error("HistoTemplates::Create3D()", Form("The low edge %f is larger than the upper edge %f", lowEdgeZ, highEdgeZ));
            return false;
        }
        if (m_Templates.find(Name) != m_Templates.end()) {
            Error("HistoTemplates::Create3D()", "The template " + Name + " already exists.");
            return false;
        }
        std::shared_ptr<TH1> H = std::make_shared<TH3D>(Form("temp_%s", Name.c_str()), Form("temp_%s", Name.c_str()), n_BinsX, lowEdgeX,
                                                        highEdgeX, n_BinsY, lowEdgeY, highEdgeY, n_BinsZ, lowEdgeZ, highEdgeZ);
        H->SetDirectory(nullptr);
        m_Templates.insert(std::pair<std::string, std::shared_ptr<TH1>>(Name, H));
        Info("HistoTemplates::Create3D()", "New 3D template " + Name + " with " + std::to_string(n_BinsX) + ", " + std::to_string(n_BinsY) +
                                               " and " + std::to_string(n_BinsZ) + " bins ranging between " + std::to_string(lowEdgeX) +
                                               " - " + std::to_string(highEdgeX) + ", " + std::to_string(lowEdgeY) + " - " +
                                               std::to_string(highEdgeY) + " and " + std::to_string(lowEdgeZ) + " - " +
                                               std::to_string(highEdgeZ));
        return true;
    }
    bool HistoTemplates::CrossProduct(const std::string& Line) {
        std::stringstream sstr(Line);
        GetWordFromStream(sstr);  // eliminate the keyword
        std::string NewName(GetWordFromStream(sstr)), FirstAxis(GetWordFromStream(sstr)), SecondAxis(GetWordFromStream(sstr));
        if (!IsTemplateDefined(FirstAxis)) {
            Error("HistoTemplates::CrossProduct()", "You did not define the template " + FirstAxis + " yet.");
            return false;
        } else if (!IsTemplateDefined(SecondAxis)) {
            Error("HistoTemplates::CrossProduct()", "You did not define the template " + SecondAxis + " yet.");
            return false;
        }
        TH1* first_template = GetTemplate(FirstAxis);
        TH1* second_template = GetTemplate(SecondAxis);
        unsigned int final_dim = first_template->GetDimension() + second_template->GetDimension();
        if (final_dim > 3) {
            Error("HistoTemplate::CrossProduct()", "You try to combine two templates which make up an effective dimesnion of " +
                                                       std::to_string(final_dim) + ". Write the TH" + std::to_string(final_dim) +
                                                       " first.");
            return false;
        }
        std::vector<double> x = ExtractBinning(first_template->GetXaxis());
        std::vector<double> y =
            ExtractBinning(first_template->GetDimension() == 1 ? second_template->GetXaxis() : first_template->GetYaxis());
        std::vector<double> z;
        if (first_template->GetDimension() == 2)
            z = ExtractBinning(second_template->GetXaxis());
        else if (second_template->GetDimension() == 2)
            z = ExtractBinning(second_template->GetYaxis());
        return CreateVarHisto(NewName, x, y, z);
    }
    bool HistoTemplates::CreateVarHisto(const std::string& Name, std::ifstream& Config) {
        if (!Config.good()) {
            Error("HistoTemplates::CreateVarHisto()", "Could not create histogram template '" + Name + "' with variable binning.");
            return false;
        }
        std::string line;
        bool TermLine = false;
        std::vector<double> BinEdgesX, BinEdgesY, BinEdgesZ;
        while (!TermLine && XAMPP::GetLine(Config, line)) {
            if (line.find("End_VarHisto") < line.size())
                TermLine = true;
            else {
                std::stringstream sstr(line);
                double Edge(0);
                bool FillX = BinEdgesX.empty();
                bool FillY = BinEdgesY.empty();
                bool FillZ = BinEdgesZ.empty();
                while (sstr >> Edge) {
                    if (FillX)
                        BinEdgesX.push_back(Edge);
                    else if (FillY)
                        BinEdgesY.push_back(Edge);
                    else if (FillZ)
                        BinEdgesZ.push_back(Edge);
                    else
                        Warning("HistoTemplates::CreateVarHisto()", "No idea where to append bin edge " + std::to_string(Edge));
                }
            }
        }
        // sort the bins of the histograms
        if (TermLine) return CreateVarHisto(Name, BinEdgesX, BinEdgesY, BinEdgesZ);
        Error("HistoTemplates::CreateVarHisto()", "Missing 'End_VarHisto' statement in the config file for template " + Name);
        return false;
    }
    bool HistoTemplates::CreateVarHisto(const std::string& Name, std::vector<double>& BinEdgesX, std::vector<double>& BinEdgesY,
                                        std::vector<double>& BinEdgesZ) {
        if (Name.empty() || m_Templates.find(Name) != m_Templates.end()) {
            Error("HistoTemplates::CreateVarHisto()", "Could not create histogram template '" + Name + "' with variable binning.");
            return false;
        }
        std::shared_ptr<TH1> H = MakeTH1(BinEdgesX, BinEdgesY, BinEdgesZ);
        if (!H) {
            Error("HistoTemplates::CreateVarHisto()", "There were no bins defined for " + Name);
            return false;
        }
        m_Templates.insert(std::pair<std::string, std::shared_ptr<TH1>>(Name, H));
        Info("HistoTemplates::CreateVarHisto()", "Created histogram template " + Name + " with variable binnig.");
        PrintVarBinning(BinEdgesX, "X");
        PrintVarBinning(BinEdgesY, "Y");
        PrintVarBinning(BinEdgesZ, "Z");
        return true;
    }
    void HistoTemplates::PrintVarBinning(std::vector<double>& Edges, const std::string& Axis) {
        if (!Edges.size()) return;
        std::string Binning = "Binning in " + Axis + " : ";
        for (const auto E : Edges) Binning += std::to_string(E) + ", ";
        Info("HistoTemplates::PrintVarBinning()", Binning);
    }
    TH1* HistoTemplates::GetTemplate(const std::string& Name) const {
        std::map<std::string, std::shared_ptr<TH1>>::const_iterator Itr = m_Templates.find(Name);
        if (Itr == m_Templates.end()) {
            Error("HistoTemplates::GetTemplate()", "There does not exist a template named " + Name);
            return nullptr;
        }
        return Itr->second.get();
    }
    bool HistoTemplates::InsertTemplate(const std::string& Name, TH1* H) { return InsertTemplate(Name, std::shared_ptr<TH1>(H)); }
    bool HistoTemplates::InsertTemplate(const std::string& Name, std::shared_ptr<TH1> H) {
        if (!H) {
            Error("HistoTemplate::InsertTemplate()", "No histogram was given to define " + Name);
            return false;
        }
        H->SetDirectory(nullptr);
        if (m_Templates.find(Name) != m_Templates.end()) {
            Error("HistoTemplates::InsertTemplate()", "There already exists a template " + Name + ". Please chose another name");
            return false;
        }
        m_Templates.insert(std::pair<std::string, std::shared_ptr<TH1>>(Name, H));
        m_Templates.insert(std::pair<std::string, std::shared_ptr<TH1>>(Name, H));
        return true;
    }
    bool HistoTemplates::Import(const std::string& Config) {
        std::ifstream Stream;
        std::string Path = ResolvePath(Config);
        if (Path.empty()) {
            Error("HistoTemplates::Import()", Form("Failed to import %s", Config.c_str()));
            return false;
        }
        if (XAMPP::IsElementInList(m_ImportedCfgs, Path)) {
            XAMPP::Info("HistoTemplates::Import()", "The file " + Path + " has already been imported. It will not be imported twice.");
            return true;
        }
        m_ImportedCfgs.push_back(Path);
        XAMPP::Info("HistoTemplates::Import()", "Import new config file " + Path);
        Stream.open(Path);

        return ReadTemplateConfig(Stream);
    }
    bool HistoTemplates::ReadTemplateConfig(std::ifstream& Inf) {
        std::string line;
        while (XAMPP::GetLine(Inf, line)) {
            std::stringstream sstr(line);
            std::string word = XAMPP::GetWordFromStream(sstr);
            if (word == "1DHisto" && !Create1D(line))
                return false;
            else if (word == "2DHisto" && !Create2D(line))
                return false;
            else if (word == "3DHisto" && !Create3D(line))
                return false;
            else if (word == "CrossProduct" && !CrossProduct(line))
                return false;

            else if (word == "2DPolyHisto" && !CreateTH2Poly(XAMPP::GetWordFromStream(sstr), Inf)) {
                return false;
            } else if (word == "VarHisto" && !CreateVarHisto(XAMPP::GetWordFromStream(sstr), Inf))
                return false;
            else if (word == "Import" && !Import(XAMPP::GetWordFromStream(sstr)))
                return false;
        }
        return true;
    }
    bool HistoTemplates::InitTemplates(const std::string& Path) { return Import(Path); }
    bool HistoTemplates::CreateTH2Poly(const std::string& Name, std::ifstream& Config) {
        if (IsTemplateDefined(Name)) {
            Error("HistoTemplates::CreateTH2Poly()", "Error the template " + Name + " is already defined");
            return false;
        }
        std::string line;
        std::shared_ptr<TH2Poly> Poly = std::make_shared<TH2Poly>();
        while (XAMPP::GetLine(Config, line)) {
            if (line.find("End_2DPolyHisto") != std::string::npos) {
                return InsertTemplate(Name, Poly);
            } else if (line.find("New_Bin") < line.size() && !AddTH2PolyBin(Poly, Config)) {
                return false;
            }
        }
        Error("HistoTemplates::CreateTH2Poly()", "Missing 'End_2DPolyHisto' statement in the config file.");
        return false;
    }
    bool HistoTemplates::AddTH2PolyBin(std::shared_ptr<TH2Poly>& Poly, std::ifstream& Inf) {
        std::vector<double> x_Values;
        std::vector<double> y_Values;
        int LineNumber = 0;
        bool FoundEnd = false;
        std::string line;
        while (!FoundEnd && XAMPP::GetLine(Inf, line)) {
            if (line.find("End_Bin") < line.size())
                FoundEnd = true;
            else {
                std::stringstream sstr(line);
                double Bin;
                while (sstr >> Bin) {
                    if (LineNumber % 2 == 0) {
                        x_Values.push_back(Bin);
                    } else {
                        y_Values.push_back(Bin);
                    }
                }
                if (LineNumber % 2 == 1 && x_Values.size() != y_Values.size()) {
                    Error("HistoTemplates::AddTH2PolyBin()",
                          "The numbers of x-Values does not match the number of y-Values. A pair of lines in the config file is "
                          "interpreted as x & y coordinates of each bin.");
                    return false;
                }
            }
            ++LineNumber;
        }
        if (!FoundEnd) {
            Error("HistoTemplates::AddTH2PolyBin()", "Missing 'End_Bin' statement in the config file. ");
            return false;
        }
        if (x_Values.empty() || x_Values.size() != y_Values.size()) {
            Error("HistoTemplates::AddTH2PolyBin()",
                  "The numbers of x-Values does not match the number of y-Values. A pair of lines in the config file is interpreted as x & "
                  "y coordinates of each bin.");
            return false;
        }
        // Ensure that the bin is always closed
        if ((*x_Values.begin()) != (*x_Values.end() - 1) || (*y_Values.begin()) != (*y_Values.end() - 1)) {
            x_Values.push_back((*x_Values.begin()));
            y_Values.push_back((*y_Values.begin()));
        }
        const int N = x_Values.size();
        Poly->AddBin(N, &x_Values[0], &y_Values[0]);
        return true;
    }

}  // namespace XAMPP
