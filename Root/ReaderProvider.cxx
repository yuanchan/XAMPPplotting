#include <XAMPPplotting/ArithmetricReaders.h>
#include <XAMPPplotting/CombinatoricSelection.h>
#include <XAMPPplotting/IfDefHelpers.h>
#include <XAMPPplotting/ObservableReader.h>
#include <XAMPPplotting/ParticleTagger.h>
#include <XAMPPplotting/PlottingUtils.h>
#include <XAMPPplotting/ReaderProvider.h>
#include <XAMPPplotting/ReconstructedParticleReaders.h>
#include <XAMPPplotting/TreeVarReader.h>
#include <XAMPPplotting/UtilityReader.h>
namespace XAMPP {
    // Initialization of the singleton pointer
    ReaderProvider* ReaderProvider::m_Inst = nullptr;
    ReaderProvider::ReaderProvider(bool DisableMeVtoGeV) : ReadLine(), m_disableMeVtoGeV(DisableMeVtoGeV) {}
    ReaderProvider* ReaderProvider::GetInstance(bool DisableMeVtoGeV) {
        if (!m_Inst) m_Inst = new ReaderProvider(DisableMeVtoGeV);
        return m_Inst;
    }
    bool ReaderProvider::disableGeVtoMeV() const { return m_disableMeVtoGeV; }
    ReaderProvider::~ReaderProvider() { m_Inst = nullptr; }
    ITreeVarReader* ReaderProvider::CreateReader(std::stringstream& sstr) {
        ITreeVarReader* R = nullptr;
        std::string word(GetWordFromStream(sstr));
        bool CreateAbs = word.find("|") == 0 && word.rfind("|") == word.size() - 1;
        if (CreateAbs) word = word.substr(1, word.size() - 2);
        if (word == "EvReader")
            R = CreateScalarReader(sstr);
        else if (word == "ParReader")
            return CreateParticleVarReader(sstr);
        else if (word == "RawParticleReader")
            return ParReaderStorage::GetInstance()->GetReader(GetWordFromStream(sstr));
        else if (word == "PseudoReader")
            R = CreatePseudoReader(sstr);
        else if (word == "PeriodReader")
            R = PeriodReader::GetReader();
        else if (word == "LumiBlockReader")
            R = DataLumiReader::GetReader();
        else if (word == "BinnedRunReader")
            R = BinnedDataTakingReader::GetReader(GetDoubleFromStream(sstr));
        else if (word == "ParVecSizeReader")
            R = CreateParticleVecVarSizeReader(sstr);
        else if (word == "NumParReader")
            R = ParticleSizeReader::GetReader(GetWordFromStream(sstr));
        else if (word == "SumNumParReader")
            R = CreateMultiNumParticleReader(sstr);
        else if (word == "dYReader")
            R = CreateFixedDeltaYReader(sstr);
        else if (word == "dRReader")
            R = CreateFixedDeltaRReader(sstr);
        else if (word == "ClosestdRReader")
            R = CreateClosestDeltaRReader(sstr);
        else if (word == "ClosestdRReaderY")
            R = CreateClosestDeltaRReaderY(sstr);
        else if (word == "dPhiReader")
            R = CreateFixedDeltaPhiReader(sstr);
        else if (word == "dEtaReader")
            R = CreateFixedDeltaEtaReader(sstr);
        else if (word == "InvDiMReader")
            R = CreateInvariantDiMassReader(sstr);
        else if (word == "ConMTReader")
            R = CreateContraTransverseMassReader(sstr);
        else if (word == "InvDiPtReader")
            R = CreateInvariantDiPtReader(sstr);
        else if (word == "PtImbalReader")
            R = CreateMomentumImbalanceReader(sstr);
        else if (word == "dPhiToMetReader")
            R = CreateDeltaPhiToMetReader(sstr);
        else if (word == "MtMetReader")
            R = CreateMtMetReader(sstr);
        else if (word == "HtReader")
            R = CreateHtReader(sstr);
        else if (word == "MeffReader")
            R = CreateMeffReader(sstr);
        else if (word == "DiPartCompReader")
            R = CreateDiPartCompReader(sstr);
        else if (word == "CondAppliedReader")
            R = ConditionalWeightAppliedReader::GetReader(GetWordFromStream(sstr));
        else if (word == "TaggerReader")
            R = CreateTaggerReader(sstr);
        else if (word == "L1CaloReader")
            R = L1CaloReader::GetReader(GetWordFromStream(sstr));
        else if (word == "VecReader")
            return CreateVectorReader(sstr);
        else if (word == "VecSizeReader")
            return CreateVectorSizeReader(sstr);

        else
            Error("CreateReader()", "The keyword " + word + " has not been yet known");
        if (!R) {
            SeparationLine();
            Error("CreateReader()", "Could not create a Reader from line ");
            sstr.seekg(sstr.beg);
            Error("CreateReader()", sstr.str());
            SeparationLine();
        }
        if (CreateAbs && R) return MathITreeVarReader::GetReader(R, R, MathITreeVarReader::ArithmetricOperators::Absolute);
        return R;
    }
    ITreeVarReader* ReaderProvider::CreateReader(std::ifstream& inf, std::stringstream& sstr) {
        sstr.clear();
        sstr.seekg(0, sstr.beg);
        std::string Keyword = GetWordFromStream(sstr);
        if (Keyword == "New_MaxReader" || Keyword == "New_MinReader") {
            return CreateExtremumReader(inf, GetWordFromStream(sstr), Keyword.find("Max") != std::string::npos);
        }
        // The definition of a classification reader are inserted here
        else if (Keyword == "New_ClassReader") {
            return CreateClassificationReader(GetWordFromStream(sstr), inf);
        } else if (Keyword == "New_SumUpReader") {
            return CreateSumUpReader(GetWordFromStream(sstr), inf);
        } else if (Keyword == "New_SignificanceReader") {
            return CreateSignificanceReader(GetWordFromStream(sstr), inf);
        } else if (Keyword == "New_MathReader") {
            return CreateArithmetricReader(inf, GetWordFromStream(sstr));
        } else if (Keyword == "New_SetReader") {
            return CreateSetReader(GetWordFromStream(sstr), inf);
        }
        return nullptr;
    }
    ITreeVarReader* ReaderProvider::CreateArithmetricReader(std::ifstream& inf, const std::string& math_op) {
        ITreeVarReader* V1 = nullptr;
        ITreeVarReader* V2 = nullptr;
        int modulo = INT_MAX;
        std::string line(""), alias("");

        while (ReadLine(inf, line) == IfDefLineParser::NewProperty) {
            std::stringstream sstr(line);
            std::string keyword = GetWordFromStream(sstr);
            if (keyword == "Alias")
                alias = GetWordFromStream(sstr);
            else if (keyword == "Modulo") {
                modulo = GetIntegerFromStream(sstr);
            } else if (keyword == "End_MathReader") {
                MathITreeVarReader::ArithmetricOperators OP = MathITreeVarReader::operator_name(math_op);
                if (OP == MathITreeVarReader::ArithmetricOperators::Modulo)
                    return MathITreeVarReader::GetReader(V1, modulo, alias);
                else
                    return MathITreeVarReader::GetReader(V1, V2, OP, alias);
            } else if (keyword.find("Reader") != std::string::npos) {
                ITreeVarReader* R = keyword.find("New_") == std::string::npos ? CreateReader(line) : CreateReader(inf, sstr);
                if (!R) return nullptr;
                if (!V1)
                    V1 = R;
                else if (!V2)
                    V2 = R;
                else
                    Warning("CreateArithmetricReader()", "Where should I assign " + R->name());
            }
        }
        Error("CreateArithmetricReader()", "Missing 'End_MathReader' statement.");
        return nullptr;
    }

    IParticleReader* ReaderProvider::CreateParticle(std::ifstream& inf, const std::string& line) {
        std::stringstream sstr(line);
        std::string word(GetWordFromStream(sstr)), Name(GetWordFromStream(sstr)), Coll(GetWordFromStream(sstr));
        if (word == "New_ParticleM") {
            ParticleReader* Particle = GetParticle(XAMPP::TimeComponent::Mass, Name, Coll);
            if (!Particle->Setup(inf)) return nullptr;
            return Particle;
        } else if (word == "New_Particle") {
            ParticleReader* Particle = GetParticle(XAMPP::TimeComponent::Energy, Name, Coll);
            if (!Particle->Setup(inf)) return nullptr;
            return Particle;
        } else if (word == "New_ParticleMTAR") {
            ParticleReader* Particle = GetParticle(XAMPP::TimeComponent::MTAR, Name, Coll);
            if (!Particle->Setup(inf)) return nullptr;
            return Particle;
        } else if (word == "New_OverlapParticle")
            return CreateParticleORReader(inf, Name);
        return nullptr;
    }
    ParticleReader* ReaderProvider::GetParticle(XAMPP::TimeComponent t, const std::string& Name, const std::string& Coll) {
        ParticleReader* R = nullptr;
        if (t == XAMPP::TimeComponent::Mass)
            R = ParticleReaderM::GetReader(Name, Coll);
        else if (t == XAMPP::TimeComponent::Energy)
            R = ParticleReader::GetReader(Name, Coll);
        else if (t == XAMPP::TimeComponent::MTAR)
            R = ParticleReaderMTAR::GetReader(Name, Coll);
        if (R && m_disableMeVtoGeV) R->DisableMeVtoGeV();
        return R;
    }
    IParticleReader* ReaderProvider::CreateParticle(const std::string& line) {
        std::stringstream sstr(line);
        std::string word = GetWordFromStream(sstr);
        if (word == "DiParticle" || word == "UnSortedDiParticle") {
            std::string Name = GetWordFromStream(sstr);
            std::string FirstParticle = GetWordFromStream(sstr);
            std::string SecondParticle = GetWordFromStream(sstr);
            IParticleReader* DiPart = word.find("UnSorted") == std::string::npos
                                          ? DiParticleReader::GetReader(Name, FirstParticle, SecondParticle)
                                          : UnSortedDiParticleReader::GetReader(Name, FirstParticle, SecondParticle);
            if (!DiPart) {
                Error("ReaderProvider::CreateProvider()",
                      "Could not create DiParticle" + Name + " build up from " + FirstParticle + " and " + SecondParticle);
            }
            return DiPart;
        } else if (word == "ResortedParticle_DESC" || word == "ResortedParticle_ASC") {
            std::string Name(""), Sorter("");
            float Ref(0);
            sstr >> Name >> Sorter >> Ref;
            if (word == "ResortedParticle_DESC")
                return ResortedParticleReaderDESC::GetReader(Name, Sorter, Ref);
            else
                return ResortedParticleReaderASC::GetReader(Name, Sorter, Ref);
        } else if (word == "ResortedDPhiParticleMet_DESC" || word == "ResortedDPhiParticleMet_ASC") {
            std::string Particle(""), Met("");
            sstr >> Particle >> Met;
            if (word == "ResortedDPhiParticleMet_DESC")
                return SortedMetParticleReader_DESC::GetReader(Particle, Met);
            else
                return SortedMetParticleReader_ASC::GetReader(Particle, Met);
        } else if (word == "CombinedParticle") {
            std::string Name = GetWordFromStream(sstr);
            std::string FirstParticle = GetWordFromStream(sstr);
            std::string SecondParticle = GetWordFromStream(sstr);
            IParticleReader* CombinedPart = CombinedParticleReader::GetReader(Name, FirstParticle, SecondParticle);
            if (!CombinedPart) {
                Error("ReaderProvider::CreateProvider()",
                      "Could not create CombinedPart" + Name + " build up from " + FirstParticle + " and " + SecondParticle);
            }
            return CombinedPart;
        } else if (word == "SubSetParticle") {
            std::string Name = GetWordFromStream(sstr);
            std::string InputCollection = GetWordFromStream(sstr);
            return SubSetParticleReader::GetReader(Name, ParReaderStorage::GetInstance()->GetReader(InputCollection));
        }
        Error("ReaderProvider::CreateParticle()", "Unable to create particle from " + line);
        return nullptr;
    }

    ITreeVarReader* ReaderProvider::CreateReader(const std::string& line) {
        std::stringstream sstr(line);
        return CreateReader(sstr);
    }
    ITreeVarReader* ReaderProvider::CreateReader(std::ifstream& inf, const std::string& line) {
        std::stringstream sstr(line);
        return CreateReader(inf, sstr);
    }

    ITreeVarReader* ReaderProvider::CreateScalarReader(std::stringstream& sstr) {
        std::string Type(GetWordFromStream(sstr)), VarName(GetWordFromStream(sstr));
        ITreeVarReaderStorage* S = ITreeVarReaderStorage::GetInstance();
        if (S->GetReader(VarName))
            return S->GetReader(VarName);
        else if (Type == "char")
            return ScalarVarReader<char>::GetReader(VarName);
        else if (Type == "bool")
            return ScalarVarReader<bool>::GetReader(VarName);
        else if (Type == "double" || (m_disableMeVtoGeV && Type == "doubleGeV"))
            return ScalarVarReader<double>::GetReader(VarName);
        else if (Type == "float" || (m_disableMeVtoGeV && Type == "floatGeV"))
            return ScalarVarReader<float>::GetReader(VarName);
        else if (Type == "int")
            return ScalarVarReader<int>::GetReader(VarName);
        else if (Type == "uint")
            return ScalarVarReader<unsigned int>::GetReader(VarName);
        else if (Type == "llu")
            return ScalarVarReader<unsigned long long>::GetReader(VarName);
        // These readers convert meff/ met into GeV units
        else if (Type == "doubleGeV")
            return ScalarVarReaderGeV<double>::GetReader(VarName);
        else if (Type == "floatGeV")
            return ScalarVarReaderGeV<float>::GetReader(VarName);
        Error("CreateScalarReader()", "Unknown type: '" + Type + "' for variable '" + VarName + "'");
        return nullptr;
    }

    ITreeVarReader* ReaderProvider::CreateInvariantDiMassReader(std::stringstream& sstr) {
        std::string P1(""), P2("");
        sstr >> P1 >> P2;
        Cube Mat1 = GetMatrixRangesToRead(P1);
        Cube Mat2 = GetMatrixRangesToRead(P2);
        ParticleCorrelationReader* CorrReader = InvariantDiMassReader::GetReader(P1, P2);
        if (Mat1.second.first != -1)
            Warning("ReaderProvider::CreateInvariantDiMassReader()", "You specified a second dimension for the particle " + P1 + "! Typo?");
        if (Mat2.second.first != -1)
            Warning("ReaderProvider::CreateInvariantDiMassReader()", "You specified a second dimension for the particle " + P1 + "! Typo?");
        if ((Mat1.first.first != -1 && Mat1.first.first == Mat1.first.second) ||
            (Mat2.first.first != -1 && Mat2.first.first == Mat2.first.second)) {
            ITreeVectorReader* Col = nullptr;
            if (Mat1.first.first != -1) Col = MatrixNthRowReader::GetReader(CorrReader, Mat1.first.first);
            if (Mat2.first.first != -1) return VectorNthEntryReader::GetReader(Col, Mat2.first.first);
            return Col;
        } else if ((Mat1.first.first != -1 && Mat1.first.second != -1) || (Mat2.first.first != -1 && Mat2.first.second != -1)) {
            Cube MatCombined = Cube(Ranges(Mat1.first.first, Mat1.first.second), Ranges(Mat2.first.first, Mat2.first.second));
            return MatrixBlockReader::GetReader(CorrReader, MatCombined);
        } else
            return CorrReader;
    }

    ITreeVarReader* ReaderProvider::CreateContraTransverseMassReader(std::stringstream& sstr) {
        std::string P1(""), P2("");
        sstr >> P1 >> P2;
        Cube Mat1 = GetMatrixRangesToRead(P1);
        Cube Mat2 = GetMatrixRangesToRead(P2);
        ParticleCorrelationReader* CorrReader = ConTransverseMassReader::GetReader(P1, P2);
        if (Mat1.second.first != -1)
            Warning("ReaderProvider::CreateContraTransverseMassReader()",
                    "You specified a second dimension for the particle " + P1 + "! Typo?");
        if (Mat2.second.first != -1)
            Warning("ReaderProvider::CreateContraTransverseMassReader()",
                    "You specified a second dimension for the particle " + P1 + "! Typo?");
        if ((Mat1.first.first != -1 && Mat1.first.first == Mat1.first.second) ||
            (Mat2.first.first != -1 && Mat2.first.first == Mat2.first.second)) {
            ITreeVectorReader* Col = nullptr;
            if (Mat1.first.first != -1) Col = MatrixNthRowReader::GetReader(CorrReader, Mat1.first.first);
            if (Mat2.first.first != -1) return VectorNthEntryReader::GetReader(Col, Mat2.first.first);
            return Col;
        } else if ((Mat1.first.first != -1 && Mat1.first.second != -1) || (Mat2.first.first != -1 && Mat2.first.second != -1)) {
            Cube MatCombined = Cube(Ranges(Mat1.first.first, Mat1.first.second), Ranges(Mat2.first.first, Mat2.first.second));
            return MatrixBlockReader::GetReader(CorrReader, MatCombined);
        } else
            return CorrReader;
    }
    ITreeVarReader* ReaderProvider::CreateVectorReader(std::stringstream& sstr) {
        std::string type, name;
        sstr >> type >> name;
        ITreeVectorReader* R = nullptr;
        std::string main_body = name.substr(0, name.rfind("["));
        if (type == "char")
            R = VectorVarReader<char>::GetReader(main_body);
        else if (type == "bool")
            R = VectorVarReader<bool>::GetReader(main_body);
        else if (type == "int")
            R = VectorVarReader<int>::GetReader(main_body);
        else if (type == "float" || (m_disableMeVtoGeV && type == "floatGeV"))
            R = VectorVarReader<float>::GetReader(main_body);
        else if (type == "double" || (m_disableMeVtoGeV && type == "doubleGeV"))
            R = VectorVarReader<double>::GetReader(main_body);
        else if (type == "floatGeV")
            R = VectorVarReaderGeV<float>::GetReader(main_body);
        else if (type == "doubleGeV")
            R = VectorVarReaderGeV<double>::GetReader(main_body);
        else if (ITreeVarReaderStorage::GetInstance()->GetReader(main_body)) {
            R = dynamic_cast<ITreeVectorReader*>(ITreeVarReaderStorage::GetInstance()->GetReader(main_body));
        } else
            Error("CreateVectorReader()", "Unknown type " + type + ".");

        if (!R) {
            Error("CreateVectorReader()", "Could not create reader " + name);
            return nullptr;
        }
        if (name.find("[") != std::string::npos) {
            std::string Index = name.substr(name.rfind("[") + 1, name.size());
            Index = Index.substr(0, Index.size() - 1);
            return VectorNthEntryReader::GetReader(R, atoi(Index.c_str()));
        }
        return R;
    }
    ITreeVarReader* ReaderProvider::CreateMomentumImbalanceReader(std::stringstream& sstr) {
        std::string P1(""), P2("");
        sstr >> P1 >> P2;
        Cube Mat1 = GetMatrixRangesToRead(P1);
        Cube Mat2 = GetMatrixRangesToRead(P2);
        ParticleCorrelationReader* CorrReader = MomentumImbalanceReader::GetReader(P1, P2);
        if (Mat1.second.first != -1)
            Warning("ReaderProvider::CreateMomentumImbalanceReader()",
                    "You specified a second dimension for the particle " + P1 + "! Typo?");
        if (Mat2.second.first != -1)
            Warning("ReaderProvider::CreateMomentumImbalanceReader()",
                    "You specified a second dimension for the particle " + P1 + "! Typo?");
        if ((Mat1.first.first != -1 && Mat1.first.first == Mat1.first.second) ||
            (Mat2.first.first != -1 && Mat2.first.first == Mat2.first.second)) {
            ITreeVectorReader* Col = nullptr;
            if (Mat1.first.first != -1) Col = MatrixNthRowReader::GetReader(CorrReader, Mat1.first.first);
            if (Mat2.first.first != -1) return VectorNthEntryReader::GetReader(Col, Mat2.first.first);
            return Col;
        } else if ((Mat1.first.first != -1 && Mat1.first.second != -1) || (Mat2.first.first != -1 && Mat2.first.second != -1)) {
            Cube MatCombined = Cube(Ranges(Mat1.first.first, Mat1.first.second), Ranges(Mat2.first.first, Mat2.first.second));
            std::cout << GetMatrixRangesFromCube(MatCombined) << std::endl;
            return MatrixBlockReader::GetReader(CorrReader, MatCombined);
        } else
            return CorrReader;
    }

    ITreeVarReader* ReaderProvider::CreateInvariantDiPtReader(std::stringstream& sstr) {
        std::string P1(""), P2("");
        sstr >> P1 >> P2;
        Cube Mat1 = GetMatrixRangesToRead(P1);
        Cube Mat2 = GetMatrixRangesToRead(P2);
        ParticleCorrelationReader* CorrReader = InvariantDiPtReader::GetReader(P1, P2);
        if (Mat1.second.first != -1)
            Warning("ReaderProvider::CreateInvariantDiPtReader()", "You specified a second dimension for the particle " + P1 + "! Typo?");
        if (Mat2.second.first != -1)
            Warning("ReaderProvider::CreateInvariantDiPtReader()", "You specified a second dimension for the particle " + P1 + "! Typo?");
        if ((Mat1.first.first != -1 && Mat1.first.first == Mat1.first.second) ||
            (Mat2.first.first != -1 && Mat2.first.first == Mat2.first.second)) {
            ITreeVectorReader* Col = nullptr;
            if (Mat1.first.first != -1) Col = MatrixNthRowReader::GetReader(CorrReader, Mat1.first.first);
            if (Mat2.first.first != -1) return VectorNthEntryReader::GetReader(Col, Mat2.first.first);
            return Col;
        } else if ((Mat1.first.first != -1 && Mat1.first.second != -1) || (Mat2.first.first != -1 && Mat2.first.second != -1)) {
            Cube MatCombined = Cube(Ranges(Mat1.first.first, Mat1.first.second), Ranges(Mat2.first.first, Mat2.first.second));
            std::cout << GetMatrixRangesFromCube(MatCombined) << std::endl;
            return MatrixBlockReader::GetReader(CorrReader, MatCombined);
        } else
            return CorrReader;
    }

    ITreeVarReader* ReaderProvider::CreateFixedDeltaYReader(std::stringstream& sstr) {
        std::string P1(""), P2("");
        sstr >> P1 >> P2;
        Cube Mat1 = GetMatrixRangesToRead(P1);
        Cube Mat2 = GetMatrixRangesToRead(P2);
        ParticleCorrelationReader* CorrReader = DeltaYReader::GetReader(P1, P2);
        if (Mat1.second.first != -1)
            Warning("ReaderProvider::CreateFixedDeltaYReader()", "You specified a second dimension for the particle " + P1 + "! Typo?");
        if (Mat2.second.first != -1)
            Warning("ReaderProvider::CreateFixedDeltaYReader()", "You specified a second dimension for the particle " + P1 + "! Typo?");
        if ((Mat1.first.first != -1 && Mat1.first.first == Mat1.first.second) ||
            (Mat2.first.first != -1 && Mat2.first.first == Mat2.first.second)) {
            ITreeVectorReader* Col = nullptr;
            if (Mat1.first.first != -1) Col = MatrixNthRowReader::GetReader(CorrReader, Mat1.first.first);
            if (Mat2.first.first != -1) return VectorNthEntryReader::GetReader(Col, Mat2.first.first);
            return Col;
        } else if ((Mat1.first.first != -1 && Mat1.first.second != -1) || (Mat2.first.first != -1 && Mat2.first.second != -1)) {
            Cube MatCombined = Cube(Ranges(Mat1.first.first, Mat1.first.second), Ranges(Mat2.first.first, Mat2.first.second));
            std::cout << GetMatrixRangesFromCube(MatCombined) << std::endl;
            return MatrixBlockReader::GetReader(CorrReader, MatCombined);
        } else
            return CorrReader;
    }

    ITreeVarReader* ReaderProvider::CreateFixedDeltaRReader(std::stringstream& sstr) {
        std::string P1(""), P2("");
        sstr >> P1 >> P2;
        Cube Mat1 = GetMatrixRangesToRead(P1);
        Cube Mat2 = GetMatrixRangesToRead(P2);
        ParticleCorrelationReader* CorrReader = DeltaRReader::GetReader(P1, P2);
        if (Mat1.second.first != -1)
            Warning("ReaderProvider::CreateFixedDeltaRReader()", "You specified a second dimension for the particle " + P1 + "! Typo?");
        if (Mat2.second.first != -1)
            Warning("ReaderProvider::CreateFixedDeltaRReader()", "You specified a second dimension for the particle " + P1 + "! Typo?");
        if ((Mat1.first.first != -1 && Mat1.first.first == Mat1.first.second) ||
            (Mat2.first.first != -1 && Mat2.first.first == Mat2.first.second)) {
            ITreeVectorReader* Col = nullptr;
            if (Mat1.first.first != -1) Col = MatrixNthRowReader::GetReader(CorrReader, Mat1.first.first);
            if (Mat2.first.first != -1) return VectorNthEntryReader::GetReader(Col, Mat2.first.first);
            return Col;
        } else if ((Mat1.first.first != -1 && Mat1.first.second != -1) || (Mat2.first.first != -1 && Mat2.first.second != -1)) {
            Cube MatCombined = Cube(Ranges(Mat1.first.first, Mat1.first.second), Ranges(Mat2.first.first, Mat2.first.second));
            std::cout << GetMatrixRangesFromCube(MatCombined) << std::endl;
            return MatrixBlockReader::GetReader(CorrReader, MatCombined);
        } else
            return CorrReader;
    }

    ITreeVarReader* ReaderProvider::CreateFixedDeltaPhiReader(std::stringstream& sstr) {
        std::string P1(""), P2("");
        sstr >> P1 >> P2;
        Cube Mat1 = GetMatrixRangesToRead(P1);
        Cube Mat2 = GetMatrixRangesToRead(P2);
        ParticleCorrelationReader* CorrReader = DeltaPhiReader::GetReader(P1, P2);
        if (Mat1.second.first != -1)
            Warning("ReaderProvider::CreateFixedDeltaPhiReader()", "You specified a second dimension for the particle " + P1 + "! Typo?");
        if (Mat2.second.first != -1)
            Warning("ReaderProvider::CreateFixedDeltaPhiReader()", "You specified a second dimension for the particle " + P1 + "! Typo?");
        if ((Mat1.first.first != -1 && Mat1.first.first == Mat1.first.second) ||
            (Mat2.first.first != -1 && Mat2.first.first == Mat2.first.second)) {
            ITreeVectorReader* Col = nullptr;
            if (Mat1.first.first != -1) Col = MatrixNthRowReader::GetReader(CorrReader, Mat1.first.first);
            if (Mat2.first.first != -1) return VectorNthEntryReader::GetReader(Col, Mat2.first.first);
            return Col;
        } else if ((Mat1.first.first != -1 && Mat1.first.second != -1) || (Mat2.first.first != -1 && Mat2.first.second != -1)) {
            Cube MatCombined = Cube(Ranges(Mat1.first.first, Mat1.first.second), Ranges(Mat2.first.first, Mat2.first.second));
            return MatrixBlockReader::GetReader(CorrReader, MatCombined);
        } else
            return CorrReader;
    }
    ITreeVarReader* ReaderProvider::CreateFixedDeltaEtaReader(std::stringstream& sstr) {
        std::string P1(""), P2("");
        sstr >> P1 >> P2;
        Cube Mat1 = GetMatrixRangesToRead(P1);
        Cube Mat2 = GetMatrixRangesToRead(P2);
        ParticleCorrelationReader* CorrReader = DeltaEtaReader::GetReader(P1, P2);
        if (Mat1.second.first != -1)
            Warning("ReaderProvider::CreateFixedDeltaEtaReader()", "You specified a second dimension for the particle " + P1 + "! Typo?");
        if (Mat2.second.first != -1)
            Warning("ReaderProvider::CreateFixedDeltaEtaReader()", "You specified a second dimension for the particle " + P1 + "! Typo?");
        if ((Mat1.first.first != -1 && Mat1.first.first == Mat1.first.second) ||
            (Mat2.first.first != -1 && Mat2.first.first == Mat2.first.second)) {
            ITreeVectorReader* Col = nullptr;
            if (Mat1.first.first != -1) Col = MatrixNthRowReader::GetReader(CorrReader, Mat1.first.first);
            if (Mat2.first.first != -1) return VectorNthEntryReader::GetReader(Col, Mat2.first.first);
            return Col;
        } else if ((Mat1.first.first != -1 && Mat1.first.second != -1) || (Mat2.first.first != -1 && Mat2.first.second != -1)) {
            Cube MatCombined = Cube(Ranges(Mat1.first.first, Mat1.first.second), Ranges(Mat2.first.first, Mat2.first.second));
            return MatrixBlockReader::GetReader(CorrReader, MatCombined);
        } else
            return CorrReader;
    }
    ITreeVarReader* ReaderProvider::CreatePseudoReader(std::stringstream& sstr) {
        std::string Name, Value_string;
        sstr >> Name >> Value_string;
        float Value = Value_string.empty() ? -1 : atof(Value_string.c_str());
        std::string ReaderName = !Value_string.empty() ? Form("%s_%f", Name.c_str(), Value) : Name;
        PseudoScalarVarReader* R1 = PseudoScalarVarReader::GetReader(ReaderName);
        if (!R1)
            Error("CreatePseudoReader()", "Could not create PseudoScalarVarReader " + ReaderName);
        else
            R1->SetValue(Value);
        return R1;
    }
    ITreeVarReader* ReaderProvider::CreateParticleVarReader(std::stringstream& sstr) {
        ITreeVarReader* R = nullptr;
        std::string Particle, Variable;
        sstr >> Particle >> Variable;
        Cube Mat = GetMatrixRangesToRead(Variable);
        bool CreateAbs = Variable.find("|") == 0 && Variable.rfind("|") == Variable.size() - 1;
        if (CreateAbs) Variable = Variable.substr(1, Variable.size() - 2);
        ParReaderStorage* S = ParReaderStorage::GetInstance();
        IParticleReader* P = S->GetReader(Particle);
        if (!P) {
            Error("CreateParticleVarReader()", "The particle " + Particle + " is not defined");
            return nullptr;
        }
        IParticleVarReader* V = P->RetrieveVariable(Variable);
        if (Mat.first.first != -1 || Mat.second.first != -1 || Mat.first.first != Mat.first.second ||
            Mat.second.first != Mat.second.second) {
            /// Check whether the variable itself is a vector variable or not
            IParticleVectorReader* M = dynamic_cast<IParticleVectorReader*>(V);
            /// Nope only the n-th particle should be read
            if (!M) {
                R = ConstrainRanges(V, Mat.first);
                return !CreateAbs ? R : MathITreeVarReader::GetReader(R, R, MathITreeVarReader::ArithmetricOperators::Absolute);
            }

            else if (M) {
                // The n-th particle shall be read out
                if (Mat.first.first != -1 && Mat.first.first == Mat.first.second) {
                    /// Just read out the n-th particle
                    if (Mat.second.first == Mat.second.second && Mat.second.first == -1) {
                        R = ParticleNthRowReader::GetReader(M, Mat.first.first);
                    }
                    /// Read out the n-th particle from to
                    else {
                        R = VectorRangeReader::GetReader(ParticleNthRowReader::GetReader(M, Mat.first.first), Mat.second.first,
                                                         Mat.second.second);
                    }
                    return !CreateAbs ? R : MathITreeVarReader::GetReader(R, R, MathITreeVarReader::ArithmetricOperators::Absolute);
                } else
                    R = M;
            }
        }
        return !CreateAbs ? V : AbsIParticleVarReader::GetReader(V);
    }
    ITreeVarReader* ReaderProvider::ConstrainRanges(IParticleVarReader* Variable, Ranges Rng) {
        if (Rng.first == Rng.second && Rng.first != -1)
            return ParticleNthEntryReader::GetReader(Variable, Rng.first);
        else if (Rng.first != -1)
            return ParticleRangeReader::GetReader(Variable, Rng.first, Rng.second);
        return Variable;
    }

    ITreeVarReader* ReaderProvider::CreateParticleVecVarSizeReader(std::stringstream& sstr) {
        std::string Particle, Variable;
        sstr >> Particle >> Variable;
        IParticleReader* P = ParReaderStorage::GetInstance()->GetReader(Particle);
        if (!P) {
            Error("CreateParticleVecVarSizeReader()", "There is no particle " + Particle);
            return nullptr;
        }
        IParticleVectorReader* V = dynamic_cast<IParticleVectorReader*>(P->RetrieveVariable(Variable.substr(0, Variable.find("["))));
        if (!V) {
            Error("CreateParticleVecVarSizeReader()", "The variable " + Variable + " is no vector variable");
            return nullptr;
        }
        ITreeVectorReader* S = ParticleRowSizeReader::GetReader(V);
        if (Variable.find("[") != std::string::npos) {
            std::string Index = Variable.substr(Variable.find("[") + 1, Variable.find("]") - (Variable.find("[") + 1));
            return VectorNthEntryReader::GetReader(S, atoi(Index.c_str()));
        }
        return S;
    }
    ITreeVarReader* ReaderProvider::CreateMeffReader(std::stringstream& sstr) {
        std::string P(""), Met(""), N("Meff ");
        sstr >> P >> Met;
        ParReaderStorage* S = ParReaderStorage::GetInstance();
        ITreeVarReader* R = nullptr;
        if (!S->GetReader(P))
            Error("CreateMeffReader()", "There is no particle " + P + " to build the meff from. ");
        else {
            N += P + "_" + Met;
            R = SumUpReader::GetReader(N, S->GetReader(P)->RetrieveVariable("pt"), ScalarVarReaderGeV<float>::GetReader(Met + "_met"));
        }
        return R;
    }
    ITreeVarReader* ReaderProvider::CreateDiPartCompReader(std::stringstream& sstr) {
        std::string P = GetWordFromStream(sstr);
        DiParticleReader* DiP = DiParticleReader::GetReader(P);
        if (!DiP) {
            Error("CreateDiPartCompReader()", "There is no particle " + P + " to build the DiParticle component from. ");
            return nullptr;
        }
        return DiP->RetrieveComponentReader();
    }
    ITreeVarReader* ReaderProvider::CreateHtReader(std::stringstream& sstr) {
        std::string P(""), N("HT ");
        sstr >> P;
        ParReaderStorage* S = ParReaderStorage::GetInstance();
        ITreeVarReader* R = nullptr;
        if (!S->GetReader(P))
            Error("CreateHtReader()", "There is no particle " + P + " to build the Ht from.");
        else {
            N += P;
            R = SumUpReader::GetReader(N, S->GetReader(P)->RetrieveVariable("pt"));
        }
        return R;
    }
    ITreeVarReader* ReaderProvider::CreateTaggerReader(std::stringstream& sstr) {
        std::string Particle(GetWordFromStream(sstr)), Tagger_Name(GetWordFromStream(sstr));
        return IsParticleTaggedReader::getReader(Particle, Tagger_Name);
    }
    ITreeVarReader* ReaderProvider::CreateDeltaPhiToMetReader(std::stringstream& sstr) {
        std::string Particle, Met;
        sstr >> Particle >> Met;
        bool create_abs = false;
        Cube Mat = GetMatrixRangesToRead(Particle);
        if (Particle.find("|") == 0 && Particle.rfind("|") == Particle.size() - 1) {
            create_abs = true;
            Particle = Particle.substr(1, Particle.size() - 2);
        }
        IParticleVarReader* V = DeltaPhiToMetReader::GetReader(Particle, Met);
        if (create_abs) V = AbsIParticleVarReader::GetReader(V);
        if (Mat.first.first != -1 || Mat.second.first != -1 || Mat.first.first != Mat.first.second ||
            Mat.second.first != Mat.second.second) {
            if (V) return ConstrainRanges(V, Mat.first);
        }
        return V;
    }
    ITreeVarReader* ReaderProvider::CreateMtMetReader(std::stringstream& sstr) {
        std::string Particle, Met;
        sstr >> Particle >> Met;
        if (Particle.find("[") < Particle.size()) {
            std::string Index = Particle.substr(Particle.rfind("[") + 1, Particle.size());
            Particle = Particle.substr(0, Particle.rfind("["));
            Index = Index.substr(0, Index.size() - 1);
            return ParticleNthEntryReader::GetReader(MtMetReader::GetReader(Particle, Met), atoi(Index.c_str()));
        }
        return MtMetReader::GetReader(Particle, Met);
    }
    ITreeVarReader* ReaderProvider::CreateExtremumReader(std::ifstream& inf, const std::string& Name, bool Max) {
        XAMPP::ITreeVarReader* R = nullptr;
        std::vector<ITreeVarReader*> InputReaders;
        std::string line;
        while (ReadLine(inf, line) == IfDefLineParser::NewProperty) {
            std::stringstream sstr(line);
            std::string word(GetWordFromStream(sstr));
            if (word == "End_MaxReader" || word == "End_MinReader") {
                if (Name.empty()) {
                    Error("CreateExtremumReader()", "The reader is nameless");
                    return nullptr;
                }
                if (Max)
                    return MaximumVarReader::GetReader(Name, InputReaders);
                else
                    return MinimumVarReader::GetReader(Name, InputReaders);
                return nullptr;
            } else if (word.find("Reader") != std::string::npos) {
                R = word.find("New_") == std::string::npos ? CreateReader(line) : CreateReader(inf, word);
                if (!R) return nullptr;
                InputReaders.push_back(R);
            }
        }
        return nullptr;
    }
    ITreeVarReader* ReaderProvider::CreateSumUpReader(const std::string& Name, std::ifstream& inf) {
        std::string line;
        std::vector<ITreeVarReader*> Readers;
        while (ReadLine(inf, line) == IfDefLineParser::NewProperty) {
            std::stringstream sstr(line);
            std::string word = GetWordFromStream(sstr);
            if (word == "End_SumUpReader") {
                if (Name.empty()) {
                    Error("CreateSumUpReader()", "Empty names not allowed");
                } else
                    return SumUpReader::GetReader(Name, Readers);
            } else if (word.find("Reader") != std::string::npos) {
                ITreeVarReader* NewRead = CreateReader(line);
                if (!NewRead) { return nullptr; }
                Readers.push_back(NewRead);
            }
        }
        Error("CreateSumUpReader", "Missing 'End_SumReader' statement");
        return nullptr;
    }
    ITreeVarReader* ReaderProvider::CreateMultiNumParticleReader(std::stringstream& sstr) {
        ITreeVarReader* V1 = ParticleSizeReader::GetReader(GetWordFromStream(sstr));
        ITreeVarReader* V2 = ParticleSizeReader::GetReader(GetWordFromStream(sstr));
        return MathITreeVarReader::GetReader(V1, V2, MathITreeVarReader::ArithmetricOperators::Plus);
    }
    ITreeVarReader* ReaderProvider::CreateClassificationReader(const std::string& Name, std::ifstream& inf) {
        ClassificationReader* R = ClassificationReader::GetReader(Name);
        if (!R) { Error("ReaderProvider::CreateClassificationReader()", "Could not retrieve Reader " + Name); }
        std::string line("");
        while (ReadLine(inf, line) == IfDefLineParser::NewProperty) {
            if (line.find("End_ClassReader") < line.size()) {
                Info("CreateClassificationReader()", "Created new Reader " + R->name());
                R->SetupDone();
                return R;
            } else if (line.find("New_Class") < line.size() && !R->AddClassification(inf)) {
                return nullptr;
            }
        }
        Error("CreateClassificationReader()", "Missing 'End_ClassReader' statement.");
        return nullptr;
    }
    ITreeVarReader* ReaderProvider::CreateSignificanceReader(const std::string& Name, std::ifstream& inf) {
        std::string line;
        ITreeVarReader* Numerator = nullptr;
        ITreeVarReader* SqrtReader = nullptr;
        while (ReadLine(inf, line) == IfDefLineParser::NewProperty) {
            std::stringstream sstr(line);
            if (IsKeyWordSatisfied(sstr, "End_SignificanceReader")) {
                return MathITreeVarReader::GetReader(
                    Numerator, MathITreeVarReader::GetReader(SqrtReader, SqrtReader, MathITreeVarReader::ArithmetricOperators::Sqrt),
                    MathITreeVarReader::ArithmetricOperators::Divide, Name);
            } else if (IsKeyWordSatisfied(sstr, "Numerator")) {
                Numerator = GetWordFromStream(sstr, true).find("New_") == 0 ? CreateReader(inf, sstr) : CreateReader(sstr);
            } else if (IsKeyWordSatisfied(sstr, "Sqrt")) {
                SqrtReader = GetWordFromStream(sstr, true).find("New_") == 0 ? CreateReader(inf, sstr) : CreateReader(sstr);
            }
        }
        Error("CreateSignificanceReader()", "Missing 'End_SignificanceReader' statement.");
        return nullptr;
    }
    ITreeVarReader* ReaderProvider::CreateSetReader(const std::string& Name, std::ifstream& inf) {
        std::string line;
        std::vector<ITreeVarReader*> Set;
        while (ReadLine(inf, line) == IfDefLineParser::NewProperty) {
            std::stringstream sstr(line);
            std::string word = GetWordFromStream(sstr);
            if (word == "End_SetReader") {
                return SetReader::GetReader(Name, Set);
            } else if (word.find("Reader") != std::string::npos) {
                ITreeVarReader* R = word.find("New_") == std::string::npos ? CreateReader(line) : CreateReader(inf, sstr);
                if (!R) return nullptr;
                Set.push_back(R);
            }
        }
        Error("CreateSetReader()", "Missing End_SetReader statement");
        return nullptr;
    }
    IParticleReader* ReaderProvider::CreateParticleORReader(std::ifstream& inf, const std::string& r_name) {
        std::string line, from, with, or_idx, ct_idx;
        float d_R = -1;
        bool keep_or = false;
        while (ReadLine(inf, line) == IfDefLineParser::NewProperty) {
            std::stringstream sstr(line);
            if (IsKeyWordSatisfied(sstr, "From"))
                from = GetWordFromStream(sstr);
            else if (IsKeyWordSatisfied(sstr, "With"))
                with = GetWordFromStream(sstr);
            else if (IsKeyWordSatisfied(sstr, "OverlapIndex"))
                or_idx = GetWordFromStream(sstr);
            else if (IsKeyWordSatisfied(sstr, "WithIndex"))
                ct_idx = GetWordFromStream(sstr);
            else if (IsKeyWordSatisfied(sstr, "dR"))
                d_R = GetFloatFromStream(sstr);
            else if (IsKeyWordSatisfied(sstr, "KeepOverlap"))
                keep_or = true;
            else if (IsKeyWordSatisfied(sstr, "End_OverlapParticle")) {
                if (!or_idx.empty() && !ct_idx.empty())
                    return OverlapRemovalParticleReader::GetReader(r_name, from, with, or_idx, ct_idx, keep_or);
                else if (d_R > 0)
                    return OverlapRemovalParticleReader::GetReader(r_name, from, with, d_R, keep_or);
            }
        }
        Error("CreateParticleORReader()", "Missing 'End_OoverlapParticle' statement");
        return nullptr;
    }
    ITreeVarReader* ReaderProvider::CreateClosestDeltaRReader(std::stringstream& sstr) {
        std::string P1(""), P2("");
        sstr >> P1 >> P2;
        Cube Mat1 = GetMatrixRangesToRead(P1);
        Cube Mat2 = GetMatrixRangesToRead(P2);
        ParticleCorrelationReader* dR = DeltaRReader::GetReader(P1, P2);
        if (!dR) {
            Error("CreateClosestDeltaRReader()", "Failed to make closest delta R reader from " + P1 + " and " + P2 + ".");
            return nullptr;
        }
        IParticleVarReader* R = ExtremumCorrelationReader::GetReader(dR, true);

        if (Mat1.first.first != -1 || Mat1.first.first != Mat1.first.second) { return ConstrainRanges(R, Mat1.first); }
        return R;
    }
    ITreeVarReader* ReaderProvider::CreateClosestDeltaRReaderY(std::stringstream& sstr) {
        std::string P1(""), P2("");
        sstr >> P1 >> P2;
        Cube Mat1 = GetMatrixRangesToRead(P1);
        Cube Mat2 = GetMatrixRangesToRead(P2);
        ParticleCorrelationReader* dR = DeltaRReaderY::GetReader(P1, P2);
        if (!dR) {
            Error("CreateClosestDeltaRReader()", "Failed to make closest delta R reader from " + P1 + " and " + P2 + ".");
            return nullptr;
        }
        IParticleVarReader* R = ExtremumCorrelationReader::GetReader(dR, true);

        if (Mat1.first.first != -1 || Mat1.first.first != Mat1.first.second) { return ConstrainRanges(R, Mat1.first); }
        return R;
    }
    ITreeVarReader* ReaderProvider::CreateVectorSizeReader(std::stringstream& sstr) {
        ITreeVectorReader* R = dynamic_cast<ITreeVectorReader*>(CreateVectorReader(sstr));
        return VectorSizeReader::GetReader(R);
    }

}  // namespace XAMPP
