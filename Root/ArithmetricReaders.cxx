#include <FourMomUtils/xAODP4Helpers.h>
#include <XAMPPplotting/ArithmetricReaders.h>
#include <XAMPPplotting/PlottingUtils.h>
#include <XAMPPplotting/Weight.h>
#include <cmath>

namespace XAMPP {
    //######################################################################
    //                      MathITreeVarReader
    //######################################################################
    std::string MathITreeVarReader::operator_name(MathITreeVarReader::ArithmetricOperators O) {
        if (O == ArithmetricOperators::Plus) return "+";
        if (O == ArithmetricOperators::Minus) return "-";
        if (O == ArithmetricOperators::Times) return "*";
        if (O == ArithmetricOperators::Divide) return "/";
        if (O == ArithmetricOperators::MinusOnCirc) return "-(circ)";
        if (O == ArithmetricOperators::Modulo) return "%";
        if (O == ArithmetricOperators::Absolute) return "abs";
        if (O == ArithmetricOperators::Sqrt) return "sqrt";
        if (O == ArithmetricOperators::Ln) return "ln";
        if (O == ArithmetricOperators::Exp) return "exp";
        if (O == ArithmetricOperators::Inverse) return "inv";
        if (O == ArithmetricOperators::Cos) return "cos";
        if (O == ArithmetricOperators::Sin) return "sin";
        if (O == ArithmetricOperators::Cosh) return "cosh";
        if (O == ArithmetricOperators::Sinh) return "sinh";
        if (O == ArithmetricOperators::Atanh) return "atanh";
        if (O == ArithmetricOperators::Acos) return "acos";
        return "Unknown";
    }
    MathITreeVarReader::ArithmetricOperators MathITreeVarReader::operator_name(const std::string& O) {
        if (O == "+") return ArithmetricOperators::Plus;
        if (O == "-") return ArithmetricOperators::Minus;
        if (O == "*") return ArithmetricOperators::Times;
        if (O == "/") return ArithmetricOperators::Divide;
        if (O == "-(circ)") return ArithmetricOperators::MinusOnCirc;
        if (O == "%") return ArithmetricOperators::Modulo;
        if (O == "abs") return ArithmetricOperators::Absolute;
        if (O == "sqrt") return ArithmetricOperators::Sqrt;
        if (O == "ln") return ArithmetricOperators::Ln;
        if (O == "exp") return ArithmetricOperators::Exp;
        if (O == "inv") return ArithmetricOperators::Inverse;
        if (O == "cos") return ArithmetricOperators::Cos;
        if (O == "sin") return ArithmetricOperators::Sin;
        if (O == "cosh") return ArithmetricOperators::Cosh;
        if (O == "sinh") return ArithmetricOperators::Sinh;
        if (O == "atanh") return ArithmetricOperators::Atanh;
        if (O == "acos") return ArithmetricOperators::Acos;
        return ArithmetricOperators::Unknown;
    }

    ITreeVectorReader* MathITreeVarReader::GetReader(ITreeVarReader* V1, ITreeVarReader* V2, const std::string& op,
                                                     const std::string& alias) {
        return GetReader(V1, V2, operator_name(op), alias);
    }
    ITreeVectorReader* MathITreeVarReader::MathITreeVarReader::GetReader(ITreeVarReader* V1, ITreeVarReader* V2, ArithmetricOperators op,
                                                                         const std::string& alias) {
        std::string r_name = alias.empty() ? reader_name(V1, V2, op) : alias;
        if (!ITreeVarReaderStorage::GetInstance()->GetReader(r_name)) return new MathITreeVarReader(V1, V2, op, alias);
        return dynamic_cast<ITreeVectorReader*>(ITreeVarReaderStorage::GetInstance()->GetReader(r_name));
    }
    ITreeVectorReader* MathITreeVarReader::MathITreeVarReader::GetReader(ITreeVarReader* V1, int modulo, const std::string& alias) {
        std::string r_name =
            alias.empty() ? Form("(%s %s %d)", V1->name().c_str(), operator_name(ArithmetricOperators::Modulo).c_str(), modulo) : alias;
        if (!ITreeVarReaderStorage::GetInstance()->GetReader(r_name)) return new MathITreeVarReader(V1, modulo, alias);
        return dynamic_cast<ITreeVectorReader*>(ITreeVarReaderStorage::GetInstance()->GetReader(r_name));
    }
    std::string MathITreeVarReader::reader_name(ITreeVarReader* R1, ITreeVarReader* R2, MathITreeVarReader::ArithmetricOperators O) {
        if (R1 == nullptr || (isBinary(O) && R2 == nullptr)) return std::string();
        if (isBinary(O)) return Form("(%s %s %s)", R1->name().c_str(), operator_name(O).c_str(), R2->name().c_str());
        return Form("(%s %s)", operator_name(O).c_str(), R1->name().c_str());
    }
    bool MathITreeVarReader::isBinary(MathITreeVarReader::ArithmetricOperators O) {
        return ((int)O >= ArithmetricOperators::Plus && (int)O <= ArithmetricOperators::Modulo);
    }
    std::string MathITreeVarReader::name() const {
        if (!m_alias.empty()) return m_alias;
        if (m_operator == ArithmetricOperators::Modulo)
            return Form("(%s %s %d)", m_V1->name().c_str(), operator_name(m_operator).c_str(), m_modulo);
        return reader_name(m_V1, m_V2, m_operator);
    }
    bool MathITreeVarReader::init(TTree* t) {
        if (m_unary_fct == nullptr) {
            m_unary_fct = create_functional(m_operator);
            if (m_unary_fct == nullptr) {
                Error("MathITreeVarReader()", name() + " could not find any sensible operation to perform");
                return false;
            }
        }
        if (!m_Register) {
            Error("MathITreeVarReader()", "Fly you fools..... Gandaaalf");
            return false;
        } else if (m_operator == ArithmetricOperators::Unknown) {
            Error("MathITreeVarReader()", "Where is my operator?");
            return false;
        } else if (m_V1 == m_V2 && m_inReaderDiffer) {
            Error("MathITreeVarReader()",
                  "The reader " + name() + " got two times the same ITreeVarReader as input. However, both must differ");
            return false;
        }
        return m_V1->init(t) && m_V2->init(t);
    }
    MathITreeVarReader::~MathITreeVarReader() {}

    MathITreeVarReader::MathITreeVarReader(ITreeVarReader* V1, ITreeVarReader* V2, ArithmetricOperators O, const std::string& alias) :
        m_V1(V1),
        m_V2(V2),
        m_modulo(INT_MAX),
        m_operator(O),
        m_unary_fct(nullptr),
        m_alias(alias),
        m_Register(false),
        m_inReaderDiffer(false),
        m_cache(),
        m_eventNumber(-1),
        m_service(EventService::getService()) {
        if (!isBinary(m_operator)) m_V2 = m_V1;

        if (m_V1 && m_V2) m_Register = ITreeVarReaderStorage::GetInstance()->Register(this);
    }
    MathITreeVarReader::MathITreeVarReader(ITreeVarReader* V1, int modulo, const std::string& alias) :
        m_V1(V1),
        m_V2(V1),
        m_modulo(modulo),
        m_operator(ArithmetricOperators::Modulo),
        m_unary_fct(nullptr),
        m_alias(alias),
        m_Register(false),
        m_inReaderDiffer(false),
        m_cache(),
        m_eventNumber(-1),
        m_service(EventService::getService()) {
        if (!isBinary(m_operator)) m_V2 = m_V1;
        if (m_V1) m_Register = ITreeVarReaderStorage::GetInstance()->Register(this);

        m_unary_fct = std::make_shared<UnaryFunc>([this](const size_t I) -> double {
            int m = m_V1->readEntry(I);
            return m % m_modulo;
        });
    }
    std::shared_ptr<UnaryFunc> MathITreeVarReader::create_functional(ArithmetricOperators O) {
        if (O == ArithmetricOperators::Plus) {
            return std::make_shared<UnaryFunc>([this](const size_t I) { return m_V1->readEntry(I) + m_V2->readEntry(I); });
        } else if (O == ArithmetricOperators::Minus) {
            inputMustNotTheSame();
            return std::make_shared<UnaryFunc>([this](const size_t I) { return m_V1->readEntry(I) - m_V2->readEntry(I); });
        } else if (O == ArithmetricOperators::Times) {
            return std::make_shared<UnaryFunc>([this](const size_t I) { return m_V1->readEntry(I) * m_V2->readEntry(I); });
        } else if (O == ArithmetricOperators::Divide) {
            inputMustNotTheSame();
            return std::make_shared<UnaryFunc>([this](const size_t I) {
                double den = m_V2->readEntry(I);
                if (den == 0) {
                    Warning("MathITreeVarReader()", name() + " has 0 denominator. Return 1.e25");
                    return 1.e25;
                }
                return m_V1->readEntry(I) / den;
            });
        } else if (O == ArithmetricOperators::MinusOnCirc) {
            inputMustNotTheSame();
            return std::make_shared<UnaryFunc>(
                [this](const size_t I) { return xAOD::P4Helpers::deltaPhi(m_V1->readEntry(I), m_V2->readEntry(I)); });
        } else if (O == ArithmetricOperators::Absolute) {
            return std::make_shared<UnaryFunc>([this](const size_t I) { return std::fabs(m_V1->readEntry(I)); });
        } else if (O == ArithmetricOperators::Sqrt) {
            return std::make_shared<UnaryFunc>([this](const size_t I) { return std::sqrt(m_V1->readEntry(I)); });
        } else if (O == ArithmetricOperators::Ln) {
            return std::make_shared<UnaryFunc>([this](const size_t I) { return std::log(m_V1->readEntry(I)); });
        } else if (O == ArithmetricOperators::Exp) {
            return std::make_shared<UnaryFunc>([this](const size_t I) { return std::exp(m_V1->readEntry(I)); });
        } else if (O == ArithmetricOperators::Inverse) {
            return std::make_shared<UnaryFunc>([this](const size_t I) {
                double val = m_V1->readEntry(I);
                if (val == 0.) {
                    Warning("MathITreeVarReader()", name() + " is zero. Return 1.e25");
                    return 1.e25;
                }
                return 1. / val;
            });
        } else if (O == ArithmetricOperators::Cos) {
            return std::make_shared<UnaryFunc>([this](const size_t I) { return std::cos(m_V1->readEntry(I)); });
        } else if (O == ArithmetricOperators::Sin) {
            return std::make_shared<UnaryFunc>([this](const size_t I) { return std::sin(m_V1->readEntry(I)); });
        } else if (O == ArithmetricOperators::Cosh) {
            return std::make_shared<UnaryFunc>([this](const size_t I) { return std::cosh(m_V1->readEntry(I)); });
        } else if (O == ArithmetricOperators::Sinh) {
            return std::make_shared<UnaryFunc>([this](const size_t I) { return std::sinh(m_V1->readEntry(I)); });
        } else if (O == ArithmetricOperators::Atanh) {
            return std::make_shared<UnaryFunc>([this](const size_t I) { return std::atanh(m_V1->readEntry(I)); });
        } else if (O == ArithmetricOperators::Acos) {
            return std::make_shared<UnaryFunc>([this](const size_t I) { return std::acos(m_V1->readEntry(I)); });
        }
        return std::shared_ptr<UnaryFunc>();
    }
    void MathITreeVarReader::inputMustNotTheSame() { m_inReaderDiffer = true; }

    double MathITreeVarReader::readEntry(const size_t I) const {
        const size_t N = entries();
        if (I >= N) {
            Warning("MathITreeVarReader::readEntry()", name() + " got an index out of range. Return 1.e25");
            return 1.e25;
        }
        if (m_eventNumber != m_service->currentEvent()) {
            m_eventNumber = m_service->currentEvent();
            m_cache.clear();
            if (m_cache.capacity() < N) m_cache.reserve(m_cache.capacity() + N);
            for (size_t n = 0; n < N; ++n) m_cache.push_back((*m_unary_fct)(n));
        }
        return m_cache.at(I);
    }
    size_t MathITreeVarReader::entries() const {
        return isBinary(m_operator) ? (m_V1->entries() < m_V2->entries() ? m_V1->entries() : m_V2->entries()) : m_V1->entries();
    }

    //######################################################################
    //                     MathVectorScalarReader
    //######################################################################
    size_t MathVectorScalarReader::entries() const {
        if (m_V2->entries() != 1) return 0;
        return m_V1->entries();
    }
    std::shared_ptr<UnaryFunc> MathVectorScalarReader::create_functional(ArithmetricOperators O) {
        if (O == ArithmetricOperators::Plus) {
            return std::make_shared<UnaryFunc>([this](const size_t I) { return m_V1->readEntry(I) + m_V2->readEntry(0); });
        } else if (O == ArithmetricOperators::Minus) {
            return std::make_shared<UnaryFunc>([this](const size_t I) { return m_V1->readEntry(I) - m_V2->readEntry(0); });
        } else if (O == ArithmetricOperators::Times) {
            return std::make_shared<UnaryFunc>([this](const size_t I) { return m_V1->readEntry(I) * m_V2->readEntry(0); });
        } else if (O == ArithmetricOperators::Divide) {
            return std::make_shared<UnaryFunc>([this](const size_t I) {
                double den = m_V2->readEntry(0);
                if (den == 0) {
                    Warning("MathITreeVarReader()", name() + " has 0 denominator. Return 1.e25");
                    return 1.e25;
                }
                return m_V1->readEntry(I) / den;
            });
        }
        return std::shared_ptr<UnaryFunc>();
    }

    MathVectorScalarReader::MathVectorScalarReader(ITreeVarReader* V1, ITreeVarReader* V2, ArithmetricOperators O,
                                                   const std::string& alias) :
        MathITreeVarReader(V1, V2, O, alias) {
        inputMustNotTheSame();
    }
    ITreeVectorReader* MathVectorScalarReader::GetReader(ITreeVarReader* V1, ITreeVarReader* V2, const std::string& op,
                                                         const std::string& alias) {
        return GetReader(V1, V2, operator_name(op), alias);
    }
    ITreeVectorReader* MathVectorScalarReader::GetReader(ITreeVarReader* V1, ITreeVarReader* V2, ArithmetricOperators op,
                                                         const std::string& alias) {
        std::string r_name = alias.empty() ? reader_name(V1, V2, op) : alias;
        if (!ITreeVarReaderStorage::GetInstance()->GetReader(r_name)) return new MathVectorScalarReader(V1, V2, op, alias);
        return dynamic_cast<ITreeVectorReader*>(ITreeVarReaderStorage::GetInstance()->GetReader(r_name));
    }
}  // namespace XAMPP
