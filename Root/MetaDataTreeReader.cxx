#include <XAMPPplotting/CombinatoricSelection.h>
#include <XAMPPplotting/EventService.h>
#include <XAMPPplotting/HistoTemplates.h>
#include <XAMPPplotting/MetaDataTreeReader.h>
#include <XAMPPplotting/ParticleTagger.h>
#include <XAMPPplotting/PlottingUtils.h>
#include <XAMPPplotting/Weight.h>

#if ATHENA_RELEASE_SERIES == 212
#include <AsgTools/AnaToolHandle.h>
#include <PMGAnalysisInterfaces/IPMGCrossSectionTool.h>
#include <SUSYTools/SUSYCrossSection.h>
#endif

#include <TFile.h>
#include <TStopwatch.h>
#include <TTree.h>
#include <iostream>
#include <sstream>
namespace XAMPP {
    //###################################################################################################################
    //                                      MetaDataStore
    //###################################################################################################################
    MetaDataStore::MetaDataStore() :
        m_DSID(-1),
        m_ProcID(0),
        m_runNumber(0),
        m_xSection(0.),
        m_kFactor(0.),
        m_Efficiency(0.),
        m_SumW(0.),
        m_SumW2(0.),
        m_relUncert(1.),
        m_TotalEvents(0),
        m_ProcessedEvents(0),
        m_TotalLumiBlocks(),
        m_ProcessedLumiBlocks(),
        m_Locked(false),
        m_xSectLocked(false),
        m_prwLumi(0),
        m_smp_name() {}
    MetaDataStore::MetaDataStore(unsigned int DSID, unsigned int SUSYFinalState, unsigned int runNumber) : MetaDataStore() {
        m_DSID = DSID;
        m_ProcID = SUSYFinalState;
        m_runNumber = runNumber;
    }
    std::string MetaDataStore::sample_name() const { return m_smp_name; }
    MetaDataStore::MetaDataStore(unsigned int runNumber) : MetaDataStore() { m_runNumber = runNumber; }
    void MetaDataStore::PinCrossSection() {
        m_xSectLocked = true;
        Lock();
    }
    double MetaDataStore::prwLuminosity() const { return m_prwLumi; }
    void MetaDataStore::SetRelUncertainty(double UnCert) {
        if (m_DSID == (unsigned int)-1) {
            Warning("MetaDataStore::AddSumW()", "The storage is already locked");
            return;
        }
        if (!m_xSectLocked) { m_relUncert = UnCert; }
    }
    void MetaDataStore::AddTotalEvents(Long64_t TotEvents) {
        if (m_Locked) {
            Warning("MetaDataStore::AddTotalEvents()", "The storage is already locked");
            return;
        }
        m_TotalEvents += TotEvents;
    }
    void MetaDataStore::AddProcessedEvents(Long64_t ProcessedEvents) {
        if (m_Locked) {
            Warning("MetaDataStore::AddTotalEvents()", "The storage is already locked");
            return;
        }
        m_ProcessedEvents += ProcessedEvents;
    }
    void MetaDataStore::AddSumW(double SumW) {
        if (m_DSID == (unsigned int)-1 || m_Locked) {
            Warning("MetaDataStore::AddSumW()", "The storage is already locked");
            return;
        }
        m_SumW += SumW;
    }
    void MetaDataStore::AddSumW2(double SumW2) {
        if (m_DSID == (unsigned int)-1 || m_Locked) {
            Warning("MetaDataStore::AddSumW2()", "The storage is already locked");
            return;
        }
        m_SumW2 += SumW2;
    }
    void MetaDataStore::SetxSectionInfo(double xSection, double kFactor, double Efficiency, const std::string& smp_name) {
        if (m_DSID == (unsigned int)-1) {
            Warning("MetaDataStore::SetxSectionInfo()", "The storage is already locked");
            return;
        }
        if (m_xSectLocked) { return; }
        m_xSection = xSection;
        m_kFactor = kFactor;
        m_Efficiency = Efficiency;
        if (m_smp_name.empty() || !smp_name.empty()) m_smp_name = smp_name;
    }
    void MetaDataStore::InsertTotalLumiBlock(unsigned int BCID) {
        if (m_DSID != (unsigned int)-1 || m_Locked) {
            Warning("MetaDataStore::InsertTotalLumiBlock()", "The storage is already locked");
            return;
        }
        m_TotalLumiBlocks.push_back(BCID);
    }
    void MetaDataStore::InsertProcessedLumiBlock(unsigned int BCID) {
        if (m_DSID != (unsigned int)-1 || m_Locked) {
            Warning("MetaDataStore::InsertProcessedLumiBlock()", "The storage is already locked");
            return;
        }
        m_ProcessedLumiBlocks.push_back(BCID);
    }
    void MetaDataStore::Lock() {
        m_Locked = true;
        ClearFromDuplicates(m_TotalLumiBlocks);
        ClearFromDuplicates(m_ProcessedLumiBlocks);
    }
    void MetaDataStore::ReserveSpaceTotalLumi(unsigned int N) {
        if (m_TotalLumiBlocks.size() + N >= m_TotalLumiBlocks.capacity()) m_TotalLumiBlocks.reserve(N + m_TotalLumiBlocks.capacity());
    }
    void MetaDataStore::ReserveSpaceProcessedLumi(unsigned int N) {
        if (m_ProcessedLumiBlocks.size() + N >= m_ProcessedLumiBlocks.capacity())
            m_ProcessedLumiBlocks.reserve(N + m_ProcessedLumiBlocks.capacity());
    }
    unsigned int MetaDataStore::runNumber() const { return m_runNumber; }
    unsigned int MetaDataStore::DSID() const { return m_DSID; }
    unsigned int MetaDataStore::ProcID() const { return m_ProcID; }
    double MetaDataStore::SumW() const { return m_SumW; }
    double MetaDataStore::SumW2() const { return m_SumW2; }
    Long64_t MetaDataStore::TotalEvents() const { return m_TotalEvents; }
    Long64_t MetaDataStore::ProcessedEvents() const { return m_ProcessedEvents; }
    double MetaDataStore::xSection() const { return m_xSection; }
    double MetaDataStore::kFactor() const { return m_kFactor; }
    double MetaDataStore::FilterEfficiency() const { return m_Efficiency; }
    double MetaDataStore::relUncertainty() const { return m_relUncert; }
    const std::vector<unsigned int>& MetaDataStore::TotalLumiBlocks() const { return m_TotalLumiBlocks; }
    const std::vector<unsigned int>& MetaDataStore::ProcessedLumiBlocks() const { return m_ProcessedLumiBlocks; }
    MetaDataStore::~MetaDataStore() {
        m_TotalLumiBlocks.clear();
        m_ProcessedLumiBlocks.clear();
    }
    void MetaDataStore::SetPileUpLuminosity(double prwLumi) {
        if (m_Locked) {
            Warning("MetaDataStore::SetPileUpLuminosity()", "The storage is already locked");
            return;
        }
        m_prwLumi = prwLumi;
    }

    //###################################################################################################################
    //                                      MonteCarloStoreHandler
    //###################################################################################################################
    MonteCarloStoreHandler::MonteCarloStoreHandler(unsigned int DSID, unsigned int runNumber) :
        m_processes(),
        m_current(nullptr),
        m_DSID(DSID),
        m_runNumber(runNumber),
        m_locked(false) {}
    std::shared_ptr<MetaDataStore> MonteCarloStoreHandler::getSubProcess(unsigned int processID) const {
        if (!m_current || m_current->ProcID() != processID) {
            for (auto& store : m_processes) {
                if (store->ProcID() == processID) {
                    m_current = store;
                    return store;
                }
            }
            if (m_locked)
                Warning("MonteCarloStoreHandler()", "Process ID " + std::to_string(processID) + " unknown for dataset " +
                                                        std::to_string(DSID()) + " with runNumber " + std::to_string(runNumber()) +
                                                        ". Return nullptr");
            m_current = std::shared_ptr<MetaDataStore>();
        }
        return m_current;
    }
    std::shared_ptr<MetaDataStore> MonteCarloStoreHandler::insertSubProcess(unsigned int processID) {
        std::shared_ptr<MetaDataStore> proc = getSubProcess(processID);
        if (proc.get() != nullptr) {
            return proc;
        } else if (!m_locked) {
            proc = std::make_shared<MetaDataStore>(DSID(), processID, runNumber());
            m_processes.push_back(proc);
        }
        return proc;
    }
    void MonteCarloStoreHandler::Lock() {
        m_locked = true;
        for (auto& proc : m_processes) { proc->Lock(); }
    }
    double MonteCarloStoreHandler::prwLuminosity(unsigned int procID) const {
        const std::shared_ptr<MetaDataStore> proc = getSubProcess(procID);
        if (!proc) return 0.;
        return proc->prwLuminosity();
    }
    unsigned int MonteCarloStoreHandler::DSID() const { return m_DSID; }
    unsigned int MonteCarloStoreHandler::runNumber() const { return m_runNumber; }
    double MonteCarloStoreHandler::SumW(unsigned int procID) const {
        const std::shared_ptr<MetaDataStore> proc = getSubProcess(procID);
        if (!proc) return 0.;
        return proc->SumW();
    }
    double MonteCarloStoreHandler::SumW2(unsigned int procID) const {
        const std::shared_ptr<MetaDataStore> proc = getSubProcess(procID);
        if (!proc) return 0.;
        return proc->SumW2();
    }
    std::string MonteCarloStoreHandler::sample_name() const {
        const std::shared_ptr<MetaDataStore> proc = getSubProcess(0);
        if (!proc) return std::string("Unknown");
        return proc->sample_name();
    }
    Long64_t MonteCarloStoreHandler::TotalEvents(unsigned int procID) const {
        const std::shared_ptr<MetaDataStore> proc = getSubProcess(procID);
        if (!proc) return 0.;
        return proc->TotalEvents();
    }
    Long64_t MonteCarloStoreHandler::ProcessedEvents(unsigned int procID) const {
        const std::shared_ptr<MetaDataStore> proc = getSubProcess(procID);
        if (!proc) return 0.;
        return proc->ProcessedEvents();
    }
    double MonteCarloStoreHandler::xSection(unsigned int procID) const {
        const std::shared_ptr<MetaDataStore> proc = getSubProcess(procID);
        if (!proc) return -1.;
        return proc->xSection();
    }
    double MonteCarloStoreHandler::kFactor(unsigned int procID) const {
        const std::shared_ptr<MetaDataStore> proc = getSubProcess(procID);
        if (!proc) return -1.;
        return proc->kFactor();
    }
    double MonteCarloStoreHandler::FilterEfficiency(unsigned int procID) const {
        const std::shared_ptr<MetaDataStore> proc = getSubProcess(procID);
        if (!proc) return -1.;
        return proc->FilterEfficiency();
    }
    double MonteCarloStoreHandler::relUncertainty(unsigned int procID) const {
        const std::shared_ptr<MetaDataStore> proc = getSubProcess(procID);
        if (!proc) return -1.;
        return proc->relUncertainty();
    }
    std::vector<unsigned int> MonteCarloStoreHandler::getListOfProcesses() const {
        std::vector<unsigned int> PIDs;
        for (const auto& proc : m_processes) { PIDs.push_back(proc->ProcID()); }
        std::sort(PIDs.begin(), PIDs.end());
        return PIDs;
    }
    //###################################################################################################################
    //                                      MonteCarloPeriodHandler
    //###################################################################################################################
    MonteCarloPeriodHandler::MonteCarloPeriodHandler(unsigned int DSID) :
        m_periods(),
        m_summary_period(std::make_shared<MonteCarloStoreHandler>(DSID, -1)),
        m_locked(false) {
        m_periods.push_back(m_summary_period);
    }
    unsigned int MonteCarloPeriodHandler::DSID() const { return m_summary_period->DSID(); }
    std::shared_ptr<MonteCarloStoreHandler> MonteCarloPeriodHandler::getHandler(unsigned int run) const {
        if (run == m_summary_period->runNumber()) return m_summary_period;
        for (auto period : m_periods) {
            if (period->runNumber() == run) return period;
        }
        // return by default the summary period
        return m_summary_period;
    }
    std::shared_ptr<MonteCarloStoreHandler> MonteCarloPeriodHandler::insertHandler(unsigned int run) {
        std::shared_ptr<MonteCarloStoreHandler> handler = getHandler(run);
        // no handler has been assigned yet
        if (handler == m_summary_period && m_summary_period->runNumber() != run) {
            handler = std::make_shared<MonteCarloStoreHandler>(DSID(), run);
            m_periods.push_back(handler);
        }
        return handler;
    }
    std::shared_ptr<MetaDataStore> MonteCarloPeriodHandler::getStore(unsigned int procID, unsigned int run) const {
        return getHandler(run)->getSubProcess(procID);
    }
    std::shared_ptr<MetaDataStore> MonteCarloPeriodHandler::insertStore(unsigned int procID, unsigned int run) {
        return insertHandler(run)->insertSubProcess(procID);
    }
    void MonteCarloPeriodHandler::AddTotalEvents(Long64_t TotEvents, unsigned procID, unsigned int run) {
        insertStore(procID, run)->AddTotalEvents(TotEvents);
        if (run != m_summary_period->runNumber()) m_summary_period->insertSubProcess(procID)->AddTotalEvents(TotEvents);
    }
    void MonteCarloPeriodHandler::AddProcessedEvents(Long64_t ProcessedEvents, unsigned procID, unsigned int run) {
        insertStore(procID, run)->AddProcessedEvents(ProcessedEvents);
        if (run != m_summary_period->runNumber()) m_summary_period->insertSubProcess(procID)->AddProcessedEvents(ProcessedEvents);
    }
    void MonteCarloPeriodHandler::AddSumW(double SumW, unsigned procID, unsigned int run) {
        insertStore(procID, run)->AddSumW(SumW);
        if (run != m_summary_period->runNumber()) m_summary_period->insertSubProcess(procID)->AddSumW(SumW);
    }
    void MonteCarloPeriodHandler::AddSumW2(double SumW2, unsigned procID, unsigned int run) {
        insertStore(procID, run)->AddSumW2(SumW2);
        if (run != m_summary_period->runNumber()) m_summary_period->insertSubProcess(procID)->AddSumW2(SumW2);
    }
    void MonteCarloPeriodHandler::SetPileUpLuminosity(double prwLumi, unsigned int procID, unsigned int run) {
        // The pile-up luminosity is set once per dataset
        if (insertStore(procID, run)->prwLuminosity() > 0) return;
        insertStore(procID, run)->SetPileUpLuminosity(prwLumi);
        // Add the luminosity to the summary period
        // since we need to normalize to the total luminosity
        if (run != m_summary_period->runNumber()) {
            m_summary_period->insertSubProcess(procID)->SetPileUpLuminosity(m_summary_period->prwLuminosity(procID) + prwLumi);
        }
    }
    void MonteCarloPeriodHandler::SetxSectionInfo(double xSection, double kFactor, double Efficiency, unsigned int procID,
                                                  const std::string& smp_name) {
        insertStore(procID, -1)->SetxSectionInfo(xSection, kFactor, Efficiency, procID == 0 ? smp_name : "");
    }
    void MonteCarloPeriodHandler::SetRelUncertainty(double UnCert, unsigned int procID) {
        insertStore(procID, -1)->SetRelUncertainty(UnCert);
    }
    std::vector<unsigned int> MonteCarloPeriodHandler::getMCcampaigns() const {
        std::vector<unsigned int> campaigns;
        campaigns.reserve(m_periods.size() - 1);
        for (const auto& P : m_periods) {
            if (P != m_summary_period) campaigns.push_back(P->runNumber());
        }
        std::sort(campaigns.begin(), campaigns.end());
        return campaigns;
    }

    void MonteCarloPeriodHandler::Lock() {
        m_locked = true;
        for (auto& period : m_periods) period->Lock();
    }
    void MonteCarloPeriodHandler::PinCrossSection() {
        Lock();
        std::vector<unsigned int> procIDs = m_summary_period->getListOfProcesses();
        for (auto& period : m_periods) {
            for (auto& pid : procIDs) {
                period->getSubProcess(pid)->SetxSectionInfo(m_summary_period->xSection(pid), m_summary_period->kFactor(pid),
                                                            m_summary_period->FilterEfficiency(pid));
                period->getSubProcess(pid)->SetRelUncertainty(m_summary_period->relUncertainty(pid));
                period->getSubProcess(pid)->PinCrossSection();
            }
        }
    }
    double MonteCarloPeriodHandler::prwTotalLuminosity(unsigned int procID) const { return m_summary_period->prwLuminosity(procID); }
    //###################################################################################################################
    //                                      NormalizationDataBase
    //###################################################################################################################
    NormalizationDataBase* NormalizationDataBase::m_Inst = nullptr;
    NormalizationDataBase* NormalizationDataBase::getDataBase() {
        if (m_Inst == nullptr) m_Inst = new NormalizationDataBase();
        return m_Inst;
    }
    NormalizationDataBase::NormalizationDataBase() :
        m_mcDB(),
        m_ActMC(nullptr),
        m_dataDB(),
        m_ActMeta(nullptr),
        m_Lumi(1000.),
        m_weight(1.),
        m_Norm(-1.),
        m_xSecLoaded(false),
        m_init(false) {
        Info("NormalizationDataBase()", "Create new database");
    }
    bool NormalizationDataBase::init(const std::vector<std::shared_ptr<TFile>>& In) {
        if (m_init) return true;
        m_init = true;
        Info("NormalizationDataBase::init()", "Read in the database to normalize monte-carlo or getting run information");
        if (In.empty()) {
            Error("NormalizationDataBase::init()", "No files were given to the database. Please check you input configs");
            return false;
        }
        unsigned int NFiles = In.size(), readN = 0, Prompt = NFiles / 10;
        TStopwatch tsw;
        tsw.Start();
        double t2 = 0;
        for (const auto File : In) {
            if (!ReadInFile(File)) return false;
            ++readN;
            if (NFiles > 10 && readN % Prompt == 0) {
                t2 = tsw.RealTime();
                Info("NormalizationDataBase::init()", "Successully read in file " + std::to_string(readN) + "/" + std::to_string(NFiles) +
                                                          ". Needed time " + TimeHMS(t2) + ". Will be finished in " +
                                                          TimeHMS(t2 * ((float)NFiles / (float)readN - 1)) + ".");
                tsw.Continue();
            }
        }
        if (m_mcDB.empty() && m_dataDB.empty()) {
            Error("NormalizationDataBase::init()", "Not metadata was found. Please check carefully the logs of your analysis jobs");
            return false;
        } else if (!m_mcDB.empty() && !m_dataDB.empty()) {
            Error("NormalizationDatabase::init()", "It seems you have provided both MC and data to the database. This is prohibited.");
            return false;
        }
        m_ActMeta = nullptr;
        Weight::getWeighter(isData());
        LockStores();
        XAMPP::PrintHeadLine("NormalizationDatabase - Read MetaData");
        return true;
    }
    bool NormalizationDataBase::init(const std::vector<std::string>& In) {
        if (m_init) return true;
        m_init = true;
        Info("NormalizationDataBase::init()", "Read in the database to normalize monte-carlo or getting run information");
        std::vector<std::string> FileList;
        CopyVector(In, FileList);
        if (FileList.empty()) {
            Error("NormalizationDataBase::init()", "No files were given to the database. Please check you input configs");
            return false;
        }
        unsigned int NFiles = FileList.size(), readN = 0, Prompt = NFiles / 10;
        TStopwatch tsw;
        tsw.Start();
        double t2 = 0;
        for (const auto File : FileList) {
            if (!ReadInFile(Open(File))) return false;
            ++readN;
            if (NFiles > 10 && readN % Prompt == 0) {
                t2 = tsw.RealTime();
                Info("NormalizationDataBase::init()", "Successully read in file " + std::to_string(readN) + "/" + std::to_string(NFiles) +
                                                          ". Needed time " + TimeHMS(t2) + ". Will be finished in " +
                                                          TimeHMS(t2 * ((float)NFiles / (float)readN - 1)) + ".");
                tsw.Continue();
            }
        }
        if (m_mcDB.empty() && m_dataDB.empty()) {
            Error("NormalizationDataBase::init()", "Not metadata was found. Please check carefully the logs of your analysis jobs");
            return false;
        } else if (!m_mcDB.empty() && !m_dataDB.empty()) {
            Error("NormalizationDatabase::init()", "It seems you have provided both MC and data to the database. This is prohibited.");
            return false;
        }
        m_ActMeta = nullptr;
        Weight::getWeighter(isData());
        LockStores();
        XAMPP::PrintHeadLine("NormalizationDatabase - Read MetaData");
        return true;
    }

    bool NormalizationDataBase::ReadInFile(const std::shared_ptr<TFile>& F) {
        if (!F) return false;
        TTree* MetaDataTree = nullptr;
        F->GetObject("MetaDataTree", MetaDataTree);
        if (!ReadTree(MetaDataTree)) {
            Error("NormalizationDataBase::init()", Form("Could not read the metadata from %s", F->GetName()));
            return false;
        }
        return true;
    }
    bool NormalizationDataBase::ReadTree(TTree* t) {
        if (t == nullptr) {
            Error("NormalizationDataBase::ReadTree()", "No MetaDataTree is given");
            return false;
        }
        if (t->GetEntries() == 0) {
            Warning("NormalizationDataBase::ReadTree()", "The metadata tree does not contain any entry");
            return true;
        }
        bool isData;
        if (t->SetBranchAddress("isData", &isData) < 0) return false;
        t->GetEntry(0);
        bool isDataInFirst = isData;
        for (long int e = 0; t->GetEntry(e) && (e < t->GetEntries()); ++e) {
            if (isDataInFirst != isData) {
                Error("NormalizationDataBase::ReadTree()",
                      "It seems that the tree contains MC and data at the same time. Your input is invalid");
                return false;
            }
        }
        if (!isData) return ReadMCTree(t);
        return ReadDataTree(t);
    }
    bool NormalizationDataBase::ReadMCTree(TTree* t) {
        double xSec(0), SumWeights(0.), SumWeights2(0.), filterEff(0.), kFactor(0.), prwLumi(0.);
        unsigned int McChannel(-1), ProcId(0), runNumber(0);
        Long64_t TotalEvents(0), ProcessedEv(0);
        if (t->SetBranchAddress("xSection", &xSec) < 0) return false;
        if (t->SetBranchAddress("kFactor", &kFactor) < 0) return false;
        if (t->SetBranchAddress("FilterEfficiency", &filterEff) < 0) return false;
        if (t->SetBranchAddress("TotalSumW", &SumWeights) < 0) return false;
        if (t->SetBranchAddress("TotalSumW2", &SumWeights2) < 0) return false;
        if (t->SetBranchAddress("mcChannelNumber", &McChannel) < 0) return false;
        if (t->SetBranchAddress("ProcessID", &ProcId) < 0) return false;
        if (t->SetBranchAddress("TotalEvents", &TotalEvents) < 0) return false;
        if (t->SetBranchAddress("ProcessedEvents", &ProcessedEv) < 0) return false;
        if (t->SetBranchAddress("runNumber", &runNumber) < 0) return false;
        bool hasPRWLumi = t->GetBranch("prwLuminosity") != nullptr;
        if (hasPRWLumi && t->SetBranchAddress("prwLuminosity", &prwLumi) < 0) return false;

        for (long int e = 0; t->GetEntry(e) && (e < t->GetEntries()); ++e) {
            if (GetDSIDStatus(McChannel) == NormalizationDataBase::DSIDStatus::Failed) {
                m_mcDB.push_back(std::make_shared<MonteCarloPeriodHandler>(McChannel));
                if (GetDSIDStatus(McChannel) == NormalizationDataBase::DSIDStatus::Failed) {
                    Error("NormalizationDataBase()", "Could not create a meta element for " + std::to_string(McChannel));
                    return false;
                }
            }
            //
            //  Sum up the SumW and SumW2, etc.
            //
            m_ActMC->AddTotalEvents(TotalEvents, ProcId, runNumber);
            m_ActMC->AddProcessedEvents(ProcessedEv, ProcId, runNumber);
            m_ActMC->AddSumW(SumWeights, ProcId, runNumber);
            m_ActMC->AddSumW2(SumWeights2, ProcId, runNumber);
            m_ActMC->SetxSectionInfo(xSec, kFactor, filterEff, ProcId);
            if (hasPRWLumi) m_ActMC->SetPileUpLuminosity(prwLumi, ProcId, runNumber);
        }
        delete t;
        return true;
    }
    bool NormalizationDataBase::ReadDataTree(TTree* t) {
        unsigned int runNumber(0);
        Long64_t TotalEvents(0), ProcessedEvents(0);
        std::set<unsigned int>* TotalLumiBlocks = nullptr;
        std::set<unsigned int>* ProcessedLumiBlocks = nullptr;
        if (t->SetBranchAddress("TotalLumiBlocks", &TotalLumiBlocks) < 0) return false;
        if (t->SetBranchAddress("ProcessedLumiBlocks", &ProcessedLumiBlocks) < 0) return false;
        if (t->SetBranchAddress("ProcessedEvents", &ProcessedEvents) < 0) return false;
        if (t->SetBranchAddress("TotalEvents", &TotalEvents) < 0) return false;
        if (t->SetBranchAddress("runNumber", &runNumber) < 0) return false;
        unsigned int N = t->GetEntries();
        for (long int e = 0; t->GetEntry(e) && (e < N); ++e) {
            if (GetRunStatus(runNumber) == NormalizationDataBase::DSIDStatus::Failed) {
                m_dataDB.insert(
                    std::pair<unsigned int, std::shared_ptr<MetaDataStore>>(runNumber, std::make_shared<MetaDataStore>(runNumber)));
                m_ActMeta = std::shared_ptr<MetaDataStore>();
                if (GetRunStatus(runNumber) == NormalizationDataBase::DSIDStatus::Failed) {
                    Error("NormalizationDataBase::ReadDataTree()", "Something went wrong during storing the information");
                    return false;
                }
            }
            m_ActMeta->AddProcessedEvents(ProcessedEvents);
            m_ActMeta->AddTotalEvents(TotalEvents);
            m_ActMeta->ReserveSpaceTotalLumi(TotalLumiBlocks->size() * (1 + N));
            m_ActMeta->ReserveSpaceProcessedLumi(ProcessedLumiBlocks->size() * (1 + N));
            for (auto& BCID : *ProcessedLumiBlocks) m_ActMeta->InsertProcessedLumiBlock(BCID);
            for (auto& BCID : *TotalLumiBlocks) m_ActMeta->InsertTotalLumiBlock(BCID);
        }
        delete t;
        return true;
    }
    const double& NormalizationDataBase::getNormTimesXsec(unsigned int DSID, unsigned int ProcID) {
        NormalizationDataBase::DSIDStatus Status = GetDSIDStatus(DSID, ProcID);
        if (m_Norm >= 0. && Status == NormalizationDataBase::DSIDStatus::Present)
            return m_weight;
        else if (Status == NormalizationDataBase::DSIDStatus::Updated || m_Norm < 0.) {
            m_weight = GetxSectTimes(DSID, ProcID) * getNormalization(DSID, ProcID);
            Info("NormalizationDataBase::getNormTimesXsec()", "Updated event weight " + std::to_string(m_weight) + " for DSID " +
                                                                  std::to_string(m_ActMeta->DSID()) + " with process ID " +
                                                                  std::to_string(m_ActMeta->ProcID()) + ".");
        } else {
            Error("NormalizationDataBase::getNormTimesXsec()",
                  "MC sample not found " + std::to_string(DSID) + " with process ID " + std::to_string(ProcID) + ". Return -1.");
            m_Norm = m_weight = -1.;
        }
        return m_weight;
    }
    NormalizationDataBase::DSIDStatus NormalizationDataBase::GetDSIDStatus(unsigned int DSID) {
        if (!m_init) {
            Error("NormalizationDataBase::GetDSIDStatus()", "The database is not initialized yet");
            return NormalizationDataBase::DSIDStatus::Failed;
        }
        if (!m_ActMC || m_ActMC->DSID() != DSID) {
            m_ActMC = std::shared_ptr<MonteCarloPeriodHandler>();
            for (const auto& handler : m_mcDB) {
                if (handler->DSID() == DSID) {
                    m_ActMC = handler;
                    break;
                }
            }
            if (!m_ActMC) { return NormalizationDataBase::DSIDStatus::Failed; }
            m_Norm = -1;
            return NormalizationDataBase::DSIDStatus::Updated;
        }
        return NormalizationDataBase::DSIDStatus::Present;
    }
    NormalizationDataBase::DSIDStatus NormalizationDataBase::GetDSIDStatus(unsigned int DSID, unsigned int ProcID) {
        if (m_ActMeta && DSID == m_ActMeta->DSID() && ProcID == m_ActMeta->ProcID()) { return NormalizationDataBase::DSIDStatus::Present; }
        if (GetDSIDStatus(DSID) == NormalizationDataBase::DSIDStatus::Failed) { return NormalizationDataBase::DSIDStatus::Failed; }
        m_ActMeta = m_ActMC->getStore(ProcID);
        if (!m_ActMeta) return NormalizationDataBase::DSIDStatus::Failed;
        m_Norm = -1.;
        return NormalizationDataBase::DSIDStatus::Updated;
    }
    NormalizationDataBase::DSIDStatus NormalizationDataBase::GetRunStatus(unsigned int runNumber) {
        if (!m_init) {
            Error("NormalizationDataBase::GetRunStatus()", "The database is not initialized yet");
            return NormalizationDataBase::DSIDStatus::Failed;
        }
        if (m_ActMeta && runNumber == m_ActMeta->runNumber()) return NormalizationDataBase::DSIDStatus::Present;
        MetaID::iterator Meta = m_dataDB.find(runNumber);
        if (Meta == m_dataDB.end()) return NormalizationDataBase::DSIDStatus::Failed;
        m_ActMeta = Meta->second;
        return NormalizationDataBase::DSIDStatus::Updated;
    }
    const double& NormalizationDataBase::getNormalization(unsigned int DSID, unsigned int ProcID) {
        NormalizationDataBase::DSIDStatus Status = GetDSIDStatus(DSID, ProcID);
        if (Status == NormalizationDataBase::DSIDStatus::Failed) {
            Error("NormalizationDataBase::getNormalization()",
                  "MC sample not found " + std::to_string(DSID) + " with process ID " + std::to_string(ProcID) + ". Return 0.");
            m_Norm = 0.;
        } else if (m_Norm > 0. && Status == NormalizationDataBase::DSIDStatus::Present)
            return m_Norm;
        else
            m_Norm = m_Lumi / m_ActMeta->SumW();
        return m_Norm;
    }

    void NormalizationDataBase::SetLumi(double Lumi) {
        PrintHeadLine(Form("NormalizationDataBase: Set luminosity to %.4f fb^-1", Lumi / 1000.));
        m_Lumi = Lumi;
    }
    const std::vector<unsigned int>& NormalizationDataBase::GetTotalLumiBlocks(unsigned int runNumber) {
        static std::vector<unsigned int> DummyLumis;
        if (GetRunStatus(runNumber) != NormalizationDataBase::DSIDStatus::Failed) return m_ActMeta->TotalLumiBlocks();
        Error("NormalizationDataBase::GetTotalLumiBlocks()", "No run-meta data found");
        return DummyLumis;
    }
    const std::vector<unsigned int>& NormalizationDataBase::GetProcessedLumiBlocks(unsigned int runNumber) {
        static std::vector<unsigned int> DummyLumis;
        if (GetRunStatus(runNumber) != NormalizationDataBase::DSIDStatus::Failed) return m_ActMeta->ProcessedLumiBlocks();
        Error("NormalizationDataBase::GetProcessedLumiBlocks()", "No run-meta data found");
        return DummyLumis;
    }
    std::vector<unsigned int> NormalizationDataBase::GetListOfMCSamples() const {
        std::vector<unsigned int> DSIDs;
        for (auto& Meta : m_mcDB) { DSIDs.push_back(Meta->DSID()); }
        std::sort(DSIDs.begin(), DSIDs.end());
        return DSIDs;
    }
    Long64_t NormalizationDataBase::GetTotalEvents(unsigned int runNumber) {
        if (GetRunStatus(runNumber) != NormalizationDataBase::DSIDStatus::Failed) return m_ActMeta->TotalEvents();
        Error("NormalizationDataBase::GetTotalEvents()", "Unkown runNumber " + std::to_string(runNumber));
        return 0;
    }
    Long64_t NormalizationDataBase::GetProcessedEvents(unsigned int runNumber) {
        if (GetRunStatus(runNumber) != NormalizationDataBase::DSIDStatus::Failed) return m_ActMeta->ProcessedEvents();
        Error("NormalizationDataBase::GetProcessedEvents()", "Unkown runNumber " + std::to_string(runNumber));
        return 0;
    }
    double NormalizationDataBase::GetSumW(unsigned int DSID, unsigned int ProcID) {
        if (GetDSIDStatus(DSID, ProcID) == NormalizationDataBase::DSIDStatus::Failed) {
            Error("NormalizationDataBase::GetSumW()",
                  "Unkown combination of DSID " + std::to_string(DSID) + " and ProcessID: " + std::to_string(ProcID));
            return 0;
        }
        return m_ActMeta->SumW();
    }
    double NormalizationDataBase::GetSumW2(unsigned int DSID, unsigned int ProcID) {
        if (GetDSIDStatus(DSID, ProcID) == NormalizationDataBase::DSIDStatus::Failed) {
            Error("NormalizationDataBase::GetSumW2()",
                  "Unkown combination of DSID " + std::to_string(DSID) + " and ProcessID: " + std::to_string(ProcID));
            return 0;
        }
        return m_ActMeta->SumW2();
    }
    unsigned int NormalizationDataBase::GetNumMCsamples() const { return GetListOfMCSamples().size(); }
    std::vector<unsigned int> NormalizationDataBase::GetListOfProcesses(unsigned int DSID) {
        std::vector<unsigned int> procIDs;
        if (GetDSIDStatus(DSID) == NormalizationDataBase::DSIDStatus::Failed) {
            Error("NormalizationDataBase::GetListOfProcesses()", "Invalid DSID: " + std::to_string(DSID));
            return procIDs;
        }
        return m_ActMC->getHandler()->getListOfProcesses();
    }
    unsigned int NormalizationDataBase::GetNumberOfProcesses(unsigned int DSID) { return GetListOfProcesses(DSID).size(); }
    Long64_t NormalizationDataBase::GetTotalEvents(unsigned int DSID, unsigned int ProcID) {
        if (GetDSIDStatus(DSID, ProcID) == NormalizationDataBase::DSIDStatus::Failed) {
            Error("NormalizationDataBase::GetTotalEvents()",
                  "Unkown combination of DSID " + std::to_string(DSID) + " and ProcessID: " + std::to_string(ProcID));
            return 0;
        }
        return m_ActMeta->TotalEvents();
    }
    double NormalizationDataBase::GetRelUncertainty(unsigned int DSID, unsigned int ProcID) {
        if (GetDSIDStatus(DSID, ProcID) == NormalizationDataBase::DSIDStatus::Failed) {
            Error("NormalizationDataBase::GetRelUncertainty()",
                  "Unkown combination of DSID " + std::to_string(DSID) + " and ProcessID: " + std::to_string(ProcID));
            return 0;
        }
        return m_ActMeta->relUncertainty();
    }
    Long64_t NormalizationDataBase::GetProcessedEvents(unsigned int DSID, unsigned int ProcID) {
        if (GetDSIDStatus(DSID, ProcID) == NormalizationDataBase::DSIDStatus::Failed) {
            Error("NormalizationDataBase::GetProcessedEvents()",
                  "Unkown combination of DSID " + std::to_string(DSID) + " and ProcessID: " + std::to_string(ProcID));
            return 0;
        }
        return m_ActMeta->ProcessedEvents();
    }
    double NormalizationDataBase::GetxSection(unsigned int DSID, unsigned int ProcID) {
        if (GetDSIDStatus(DSID, ProcID) == NormalizationDataBase::DSIDStatus::Failed) {
            Error("NormalizationDataBase::GetxSection()",
                  "Unkown combination of DSID " + std::to_string(DSID) + " and ProcessID: " + std::to_string(ProcID));
            return 0;
        }
        return m_ActMeta->xSection();
    }

    double NormalizationDataBase::GetFilterEfficiency(unsigned int DSID, unsigned int ProcID) {
        if (GetDSIDStatus(DSID, ProcID) == NormalizationDataBase::DSIDStatus::Failed) {
            Error("NormalizationDataBase::GetFilterEfficiency()",
                  "Unkown combination of DSID " + std::to_string(DSID) + " and ProcessID: " + std::to_string(ProcID));
            return 0;
        }
        return m_ActMeta->FilterEfficiency();
    }

    double NormalizationDataBase::GetkFactor(unsigned int DSID, unsigned int ProcID) {
        if (GetDSIDStatus(DSID, ProcID) == NormalizationDataBase::DSIDStatus::Failed) {
            Error("NormalizationDataBase::GetkFactor()",
                  "Unkown combination of DSID " + std::to_string(DSID) + " and ProcessID: " + std::to_string(ProcID));
            return 0;
        }
        return m_ActMeta->kFactor();
    }
    void NormalizationDataBase::PromptMCMetaDataTree() {
        if (m_mcDB.empty()) return;
        unsigned int Width_Ev(9), Width_Pev(6), Width_SumW(4), Width_SumW2(2), Width_xSec(8), Width_kFac(8), Width_Eff(9), Width_ProcID(4),
            Width_SampleName(0);
        std::vector<unsigned int> SampleList = GetListOfMCSamples();
        for (const auto& DSID : SampleList) {
            std::string smp_name = GetSampleName(DSID);
            if (smp_name.size() > Width_SampleName) Width_SampleName = smp_name.size();
            std::vector<unsigned int> ProcessList = GetListOfProcesses(DSID);
            for (const auto& procID : ProcessList) {
                std::string TotEvents = Form("%llu", GetTotalEvents(DSID, procID));
                std::string ProcEvents = Form("%llu", GetProcessedEvents(DSID, procID));
                std::string ProcID = Form("%u", procID);
                std::string SumW = Form("%.4f", GetSumW(DSID, procID));
                std::string SumW2 = Form("%.4f", GetSumW2(DSID, procID));
                std::string xSec = Form("%.4f", 1.e3 * GetxSection(DSID, procID));
                std::string kFac = Form("%.4f", GetkFactor(DSID, procID));
                std::string Eff = Form("%.4f", GetFilterEfficiency(DSID, procID));
                if (TotEvents.size() > Width_Ev) Width_Ev = TotEvents.size();
                if (ProcEvents.size() > Width_Pev) Width_Pev = ProcEvents.size();
                if (ProcID.size() > Width_ProcID) Width_ProcID = ProcID.size();
                if (SumW.size() > Width_SumW) Width_SumW = SumW.size();
                if (SumW2.size() > Width_SumW2) Width_SumW2 = SumW2.size();
                if (xSec.size() > Width_xSec) Width_xSec = xSec.size();
                if (kFac.size() > Width_kFac) Width_kFac = kFac.size();
                if (Eff.size() > Width_Eff) Width_Eff = Eff.size();
            }
        }
        PrintHeadLine("MonteCarlo metadata");
        std::cout << "  DSID  | SUSY" << WhiteSpaces(Width_ProcID - 4);
        if (Width_SampleName) std::cout << " | Name" << WhiteSpaces(Width_SampleName - 4);
        std::cout << " | TotEvents" << WhiteSpaces(Width_Ev - 9) << " | ProcEv" << WhiteSpaces(Width_Pev - 6) << " | SumW"
                  << WhiteSpaces(Width_SumW - 4) << " | SumW2" << WhiteSpaces(Width_SumW2 - 5) << " | xSection"
                  << WhiteSpaces(Width_xSec - 8) << " | k-Factor" << WhiteSpaces(Width_kFac - 8) << " | FilterEff"
                  << WhiteSpaces(Width_Eff - 9) << " | FilterEffxSecTimesEff" << std::endl;
        for (const auto& DSID : SampleList) {
            std::vector<unsigned int> ProcessList = GetListOfProcesses(DSID);
            std::string smp_name = GetSampleName(DSID);
            for (const auto& procID : ProcessList) {
                std::string TotEvents = Form("%llu", GetTotalEvents(DSID, procID));
                std::string ProcEvents = Form("%llu", GetProcessedEvents(DSID, procID));
                std::string ProcID = Form("%u", procID);
                std::string SumW = Form("%.4f", GetSumW(DSID, procID));
                std::string SumW2 = Form("%.4f", GetSumW2(DSID, procID));
                std::string xSec = Form("%.4f", 1.e3 * GetxSection(DSID, procID));
                std::string kFac = Form("%.4f", GetkFactor(DSID, procID));
                std::string Eff = Form("%.4f", GetFilterEfficiency(DSID, procID));
                std::string xSecTimes =
                    Form("%.4f", 1.e3 * GetxSection(DSID, procID) * GetkFactor(DSID, procID) * GetFilterEfficiency(DSID, procID));
                std::cout << " " << DSID << " | " << ProcID << WhiteSpaces(Width_ProcID - ProcID.size());
                if (Width_SampleName > 0) { std::cout << " | " << smp_name << WhiteSpaces(Width_SampleName - smp_name.size()); }
                std::cout << " | " << TotEvents << WhiteSpaces(Width_Ev - TotEvents.size()) << " | " << ProcEvents
                          << WhiteSpaces(Width_Pev - ProcEvents.size()) << " | " << SumW << WhiteSpaces(Width_SumW - SumW.size()) << " | "
                          << SumW2 << WhiteSpaces(Width_SumW2 - SumW2.size()) << " | " << xSec << WhiteSpaces(Width_xSec - xSec.size())
                          << " | " << kFac << WhiteSpaces(Width_kFac - kFac.size()) << " | " << Eff << WhiteSpaces(Width_Eff - Eff.size())
                          << " | " << xSecTimes << std::endl;
            }
        }
    }
    void NormalizationDataBase::PromptRunMetaDataTree() {
        if (m_dataDB.empty()) return;
        std::vector<unsigned int> RunNumbers = GetRunNumbers();
        unsigned int Width_Ev(9), Width_Pev(6), Width_Run(3);
        for (const auto& R : RunNumbers) {
            std::string TotEvents = Form("%llu", GetTotalEvents(R));
            std::string ProcEvents = Form("%llu", GetProcessedEvents(R));
            std::string Run = Form("%u", R);
            if (TotEvents.size() > Width_Ev) Width_Ev = TotEvents.size();
            if (ProcEvents.size() > Width_Pev) Width_Pev = ProcEvents.size();
            if (Run.size() > Width_Run) Width_Run = Run.size();
        }
        PrintHeadLine("Run metadata");
        std::cout << " Run" << WhiteSpaces(Width_Run - 3) << " | TotEvents" << WhiteSpaces(Width_Ev - 9) << " | ProcEv"
                  << WhiteSpaces(Width_Pev - 6) << " | " << std::endl;
        for (const auto& R : RunNumbers) {
            std::string TotEvents = Form("%llu", GetTotalEvents(R));
            std::string ProcEvents = Form("%llu", GetProcessedEvents(R));
            std::string Run = Form("%u", R);
            std::cout << " " << Run << WhiteSpaces(Width_Run - Run.size()) << " | " << TotEvents << WhiteSpaces(Width_Ev - TotEvents.size())
                      << " | " << ProcEvents << WhiteSpaces(Width_Pev - ProcEvents.size()) << " | " << std::endl;
        }
    }
    void NormalizationDataBase::PromptMetaDataTree() {
        XAMPP::PrintFooter();
        PromptMCMetaDataTree();
        PromptRunMetaDataTree();
        XAMPP::PrintFooter();
    }
    bool NormalizationDataBase::isData() const {
        if (!m_init) Warning("NormalizationDataBase::isData()", "The DB is unitialized");
        return m_mcDB.empty();
    }
    NormalizationDataBase::~NormalizationDataBase() {
        delete IfDefFlags::Instance();
        delete HistoTemplates::getHistoTemplater();
        delete CombinatoricService::getService();
        delete ParticleTaggerManager::Instance();
        delete Weight::getWeighter();
        delete ITreeVarReaderStorage::GetInstance();
        delete EventService::getService();
        m_Inst = nullptr;
    }
    std::vector<unsigned int> NormalizationDataBase::GetRunNumbers() const {
        std::vector<unsigned int> runs;
        for (const auto Meta : m_dataDB) runs.push_back(Meta.first);
        std::sort(runs.begin(), runs.end());
        return runs;
    }
    void NormalizationDataBase::LockStores() {
        if (isData()) {
            std::vector<unsigned int> runs = GetRunNumbers();
            for (auto& R : runs) {
                if (GetRunStatus(R) == NormalizationDataBase::DSIDStatus::Failed) {
                    Warning("NormalizationDataBase::LockStores()", "Could not lock run store " + std::to_string(R));
                    continue;
                }
                m_ActMeta->Lock();
            }
        } else {
            std::vector<unsigned int> DSIDs = GetListOfMCSamples();
            for (const auto DS : DSIDs) {
                std::vector<unsigned int> processIDs = GetListOfProcesses(DS);
                for (const auto& P : processIDs) {
                    if (GetDSIDStatus(DS, P) == NormalizationDataBase::DSIDStatus::Failed) {
                        Warning("NormalizationDataBase::LockStores()",
                                "Could not DSID store " + std::to_string(DS) + " with processID " + std::to_string(P));
                        continue;
                    }
                    m_ActMeta->Lock();
                }
            }
        }
    }

    void NormalizationDataBase::LoadCrossSections(const std::string& xSecDir_Bkg, const std::string& xSecDir_Sig) {
        if (!m_init) {
            XAMPP::Warning("NormalizationDataBase::LoadxSections()", "No metadata is read in thus far");
            return;
        }
        if (isData() || m_xSecLoaded) return;
#if ATHENA_RELEASE_SERIES == 212
        XAMPP::Info("NormalizationDataBase::LoadxSections()", "Load xSections from the DB located at " + xSecDir_Bkg);
        XAMPP::Info("NormalizationDataBase::LoadxSections()", "Load xSections from the DB located at " + xSecDir_Sig);
        // CrossSectionDB(const std::string& txtfilenameOrDir = "SUSYTools/data/mc15_13TeV/",
        //                      bool usePathResolver = false, bool isExtended = false, bool usePMGTool = true);

        /// Swim away PMG cross-section file. Swim and drown in the depths
        /// of the sea no one likes you!!
        SUSY::CrossSectionDB xSecDB(ResolvePath("dev/PMGTools/PMGxsecDB_mc16.txt"));  //"", false, false, true);
        std::vector<std::string> the_signals = ListDirectory(ResolvePath(xSecDir_Sig), ".txt");

        for (const auto& sig : the_signals) {
            Info("NormalizationDataBase::LoadCrossSections()", "Load signal cross-sections from " + sig);
            xSecDB.loadFile(sig);
        }
        asg::AnaToolHandle<PMGTools::IPMGCrossSectionTool> pmg_DB("PMGTools::PMGCrossSectionTool/BkgXsecTool");
        pmg_DB.retrieve().ignore();
        pmg_DB->readInfosFromFiles(GetPathResolvedFileList(std::vector<std::string>{xSecDir_Bkg}));

        std::vector<unsigned int> DSIDs = GetListOfMCSamples();
        for (const auto DS : DSIDs) {
            std::vector<unsigned int> processIDs = GetListOfProcesses(DS);
            for (const auto P : processIDs) {
                // Account for LHE weight variations. Which have final states beyond 1000
                // We might also account for the variations of the finalstates + LHE but that is
                unsigned int PID = P < 1000 ? P : 0;
                if (PID != 0) {
                    SetxSection(xSecDB.rawxsect(DS, PID), xSecDB.kfactor(DS, PID), xSecDB.efficiency(DS, PID),
                                xSecDB.rel_uncertainty(DS, PID), DS, P, xSecDB.name(DS));
                } else {
                    SetxSection(pmg_DB->getAMIXsection(DS), pmg_DB->getKfactor(DS), pmg_DB->getFilterEff(DS),
                                pmg_DB->getXsectionUncertainty(DS), DS, P, xSecDB.name(DS));
                }
            }
        }
#else
        xSecDir_Bkg.empty();
        xSecDir_Sig.empty();
#endif
        m_xSecLoaded = true;
    }
    double NormalizationDataBase::GetxSectTimes(unsigned int DSID, unsigned int ProcID) {
        return GetxSection(DSID, ProcID) * GetkFactor(DSID, ProcID) * GetFilterEfficiency(DSID, ProcID);
    }
    void NormalizationDataBase::SetxSection(double xSec, double kFac, double filtEff, double relUnc, unsigned int DSID, unsigned int ProcID,
                                            const std::string& smp_name) {
        if (GetDSIDStatus(DSID, ProcID) == NormalizationDataBase::DSIDStatus::Failed) { return; }
        if (m_ActMeta->xSection() > 0. && xSec < 0.)
            XAMPP::Warning("NormalizationDataBase::SetxSection()",
                           "The current xSection for DSID " + std::to_string(DSID) + " and processID: " + std::to_string(ProcID) + " is " +
                               std::to_string(m_ActMeta->xSection()) + ". No valid value found in the current DB");
        else if (xSec < 0.)
            XAMPP::Warning("NormalizationDataBase::SetxSection()", "The current xSection for DSID " + std::to_string(DSID) +
                                                                       " and processID: " + std::to_string(ProcID) +
                                                                       " is invalid (negative).");
        m_ActMeta->SetxSectionInfo(xSec, kFac, filtEff, smp_name);
        m_ActMeta->SetRelUncertainty(relUnc);
        m_ActMeta->PinCrossSection();
    }
    void NormalizationDataBase::initialized() { m_init = true; }
    bool NormalizationDataBase::isInitialized() const { return m_init; }
    std::shared_ptr<MonteCarloPeriodHandler> NormalizationDataBase::getMCperiodHandler(unsigned int DSID) {
        if (!isInitialized() || isData()) {
            Warning("NormalizationDataBase::getMCperiodHandler()", "DB not initialized yet");
            return std::shared_ptr<MonteCarloPeriodHandler>();
        }
        if (GetDSIDStatus(DSID) == NormalizationDataBase::DSIDStatus::Failed) {
            Error("NormalizationDataBase::getMCperiodHandler()", "DSID " + std::to_string(DSID) + " is unknown.");
            return std::shared_ptr<MonteCarloPeriodHandler>();
        }
        return m_ActMC;
    }
    std::string NormalizationDataBase::GetSampleName(unsigned int DSID) {
        if (!isInitialized() || isData()) {
            Warning("NormalizationDataBase::GetSampleName()", "DB not initialized yet");
            return "not initialized";
        }
        if (GetDSIDStatus(DSID) == NormalizationDataBase::DSIDStatus::Failed) {
            Error("NormalizationDataBase::GetSampleName()", "DSID " + std::to_string(DSID) + " is unknown.");
            return "unknown sample";
        }
        return m_ActMC->getStore(0)->sample_name();
    }
    void NormalizationDataBase::resetDataBase() { delete getDataBase(); }
}  // namespace XAMPP
