#include <PathResolver/PathResolver.h>
#include <TFile.h>
#include <XAMPPplotting/DataDrivenWeights.h>
#include <XAMPPplotting/ObservableReader.h>
#include <XAMPPplotting/PlottingUtils.h>
#include <XAMPPplotting/ReaderProvider.h>
#include <XAMPPplotting/UtilityReader.h>
#include <functional>
#include <numeric>
#include <sstream>

namespace XAMPP {
    //############################################################
    //                           TH1LookUp
    //############################################################
    TH1LookUp::TH1LookUp() : m_Histo(nullptr) {}
    std::shared_ptr<IExternalLookUpTable> TH1LookUp::clone() const { return std::make_shared<TH1LookUp>(*this); }
    TH1LookUp::TH1LookUp(const TH1LookUp& other) : m_Histo(nullptr) {
        if (other.m_Histo) m_Histo = std::unique_ptr<TH1>(dynamic_cast<TH1*>(other.m_Histo->Clone(RandomString(43).c_str())));
        m_Histo->SetDirectory(0);
    }
    unsigned int TH1LookUp::dimension() const { return histo() ? m_Histo->GetDimension() : 0; }
    TAxis* TH1LookUp::axis(unsigned int d) const {
        if (!histo()) return nullptr;
        if (d == 0)
            return histo()->GetXaxis();
        else if (d == 1)
            return histo()->GetYaxis();
        else if (d == 2)
            return histo()->GetZaxis();
        return nullptr;
    }
    TH1* TH1LookUp::histo() const { return m_Histo.get(); }
    bool TH1LookUp::isOverFlow(TAxis* axis, const int Bin) const { return Bin <= 0 || Bin > axis->GetNbins(); }
    double TH1LookUp::read(const double x) const {
        if (dimension() != 1) {
            Warning("TH1LookUp::calculate()",
                    Form("%s is %d dimensional. You want to pass 1D. Return 1.e25.", m_Histo->GetName(), m_Histo->GetDimension()));
            return 1.e25;
        }
        int Bin = m_Histo->FindBin(x);
        return m_Histo->GetBinContent(Bin);
    }
    double TH1LookUp::read(const double x, const double y) const {
        if (dimension() != 2) {
            Warning("TH1LookUp::calculate()",
                    Form("%s is %d dimensional. You want to pass 2D. Return 1.e25", m_Histo->GetName(), m_Histo->GetDimension()));
            return 1.e25;
        }
        int BinX = m_Histo->GetXaxis()->FindBin(x);
        int BinY = m_Histo->GetYaxis()->FindBin(y);
        int Bin = m_Histo->GetBin(BinX, BinY);
        return m_Histo->GetBinContent(Bin);
    }
    double TH1LookUp::read(const double x, const double y, const double z) const {
        if (dimension() != 3) {
            Warning("TH1LookUp::calculate()",
                    Form("%s is %d dimensional. You want to pass 3D. Return 1.e25", m_Histo->GetName(), m_Histo->GetDimension()));
            return 1.e25;
        }
        int BinX = m_Histo->GetXaxis()->FindBin(x);
        int BinY = m_Histo->GetYaxis()->FindBin(y);
        int BinZ = m_Histo->GetZaxis()->FindBin(z);
        int Bin = m_Histo->GetBin(BinX, BinY, BinZ);
        return m_Histo->GetBinContent(Bin);
    }
    bool TH1LookUp::isOverFlow(const double x) const {
        if (dimension() != 1) return true;
        int BinX = m_Histo->GetXaxis()->FindBin(x);
        return isOverFlow(m_Histo->GetXaxis(), BinX);
    }
    bool TH1LookUp::isOverFlow(const double x, const double y) const {
        if (dimension() != 2) return true;
        int BinX = m_Histo->GetXaxis()->FindBin(x);
        int BinY = m_Histo->GetYaxis()->FindBin(y);
        return isOverFlow(m_Histo->GetXaxis(), BinX) || isOverFlow(m_Histo->GetYaxis(), BinY);
    }
    bool TH1LookUp::isOverFlow(const double x, const double y, const double z) const {
        if (dimension() != 3) return true;
        int BinX = m_Histo->GetXaxis()->FindBin(x);
        int BinY = m_Histo->GetYaxis()->FindBin(y);
        int BinZ = m_Histo->GetYaxis()->FindBin(z);
        return isOverFlow(m_Histo->GetXaxis(), BinX) || isOverFlow(m_Histo->GetYaxis(), BinY) || isOverFlow(m_Histo->GetZaxis(), BinZ);
    }
    bool TH1LookUp::SetHisto(TH1* Histo) {
        if (!Histo) {
            Error("TH1LookUp::SetHisto()", "No Histo given");
            return false;
        } else if (m_Histo) {
            Error("TH1LookUp::SetHisto()", "There is already a reference histo defined");
            delete Histo;
            return false;
        }
        Histo->SetDirectory(nullptr);
        m_Histo = std::unique_ptr<TH1>(Histo);
        PrintHistogram(m_Histo.get());
        return true;
    }

    bool TH1LookUp::LoadHistogramFromFile(std::stringstream& sstr) {
        // The line in the config files is as follows
        // <Blah>  <path to the file> <HistoName>
        std::string file_path = GetWordFromStream(sstr);
        std::string histo_path = GetWordFromStream(sstr);
        return LoadHistogramFromFile(file_path, histo_path);
    }
    bool TH1LookUp::LoadHistogramFromFile(const std::string& File, const std::string& histo_path) {
        std::unique_ptr<TFile> F(TFile::Open(PathResolverFindCalibFile(File).c_str(), "READ"));
        if (!F || !F->IsOpen()) {
            Error("TH1LookUp::LoadHistogramFromFile()", "Could not open " + File);
            return false;
        }
        TH1* H = nullptr;
        F->GetObject(histo_path.c_str(), H);
        if (!H)
            Error("TH1LookUp::LoadHistogramFromFile()", "Could not retrieve " + histo_path + " from " + File);
        else {
            H->SetDirectory(nullptr);
            H->SetTitle(Form("%s in %s", histo_path.c_str(), File.c_str()));
        }
        return SetHisto(H);
    }

    bool TH1LookUp::applyUncertainty(double w) {
        if (!m_Histo) {
            Error("TH1LookUp::applyUncertainty()", "No histogram given");
            return false;
        }
        if (w == 0) {
            Error("TH1LookUp::applyUncertainty()", "No weight has been assigned");
            return false;
        }

        for (unsigned int i = 0; i < GetNbins(m_Histo.get()); ++i) {
            m_Histo->SetBinContent(i, m_Histo->GetBinContent(i) + w * m_Histo->GetBinError(i));
            m_Histo->SetBinError(i, 0);
        }
        return true;
    }
    //############################################################
    //                           TF1LookUp
    //############################################################
    TF1LookUp::TF1LookUp() : m_Func(nullptr) {}
    TF1LookUp::TF1LookUp(const TF1LookUp& other) : m_Func(nullptr) {
        if (other.m_Func) m_Func = std::unique_ptr<TF1>(dynamic_cast<TF1*>(other.m_Func->Clone(RandomString(23).c_str())));
    }
    bool TF1LookUp::applyUncertainty(double w) {
        if (!m_Func) {
            Error("TF1LookUp::applyUncertainty()", "No histogram given");
            return false;
        }
        if (w == 0) {
            Error("TF1LookUp::applyUncertainty()", "No weight has been assigned");
            return false;
        }
        return true;
    }
    double TF1LookUp::read(const double x) const { return x; }
    double TF1LookUp::read(const double, const double) const {
        Warning("TF1LookUp::read()", "1D function");
        return 1.e25;
    }
    double TF1LookUp::read(const double, const double, const double) const {
        Warning("TF1LookUp::read()", "1D function");
        return 1.e25;
    }
    bool TF1LookUp::isOverFlow(const double) const { return false; }
    bool TF1LookUp::isOverFlow(const double, const double) const { return true; }
    bool TF1LookUp::isOverFlow(const double, const double, const double) const { return true; }
    std::shared_ptr<IExternalLookUpTable> TF1LookUp::clone() const { return std::make_shared<TF1LookUp>(*this); }

    //##############################################################################################################
    //                                        IExternalWeight
    //##############################################################################################################
    IExternalWeight::IExternalWeight() : m_Cut(nullptr), m_table(nullptr) {}
    IExternalWeight::IExternalWeight(const IExternalWeight& other) : m_Cut(other.m_Cut), m_table(nullptr) {
        if (other.m_table) m_table = other.m_table->clone();
    }
    void IExternalWeight::setCut(std::shared_ptr<Condition> Cut) {
        if (Cut) m_Cut = Cut;
    }
    bool IExternalWeight::passCut() const { return !m_Cut || m_Cut->Pass(); }
    void IExternalWeight::setLookUp(const std::shared_ptr<IExternalLookUpTable>& table) { m_table = table; }
    IExternalLookUpTable* IExternalWeight::lookup_table() const { return m_table.get(); }

    bool IExternalWeight::applyUncertainty(double w) {
        if (!m_table) return false;
        return m_table->applyUncertainty(w);
    }
    bool IExternalWeight::LoadHistogramFromFile(std::stringstream& sstr) {
        std::shared_ptr<TH1LookUp> LP = std::make_shared<TH1LookUp>();
        setLookUp(LP);
        return LP->LoadHistogramFromFile(sstr);
    }
    bool IExternalWeight::LoadHistogramFromFile(const std::string& file, const std::string& histo_path) {
        std::shared_ptr<TH1LookUp> LP = std::make_shared<TH1LookUp>();
        setLookUp(LP);
        return LP->LoadHistogramFromFile(file, histo_path);
    }
    //##############################################################################################################
    //                                        FakeFactor
    //##############################################################################################################
    double FakeFactor::Evaluate(size_t entry) {
        set_entered(false);
        if (entry < m_start_in_container || entry >= m_end_in_container || !m_Particle->At(entry) || !passCut()) { return 1.; }
        double W = 1.;
        /// One dimensional plot has been given
        if (!m_z && !m_y && m_x) {
            const double x_val = m_x->readEntry(entry);
            if (!lookup_table()->isOverFlow(x_val)) {
                set_entered(true);
                W = lookup_table()->read(x_val);
            }
        }
        /// Two dimensisional histogram is given now
        else if (!m_z && m_y && m_x) {
            const double x_val = m_x->readEntry(entry);
            const double y_val = m_y->readEntry(entry);
            if (!lookup_table()->isOverFlow(x_val, y_val)) {
                set_entered(true);
                W = lookup_table()->read(x_val, y_val);
            }
        }
        /// Last but not least the weight is binned in 3D
        else if (m_z && m_y && m_x) {
            const double x_val = m_x->readEntry(entry);
            const double y_val = m_y->readEntry(entry);
            const double z_val = m_z->readEntry(entry);
            if (!lookup_table()->isOverFlow(x_val, y_val, z_val)) {
                set_entered(true);
                W = lookup_table()->read(x_val, y_val, z_val);
            }
        }
        if (include_combinatorics()) {
            /// The zero-th combinatoric is always the trivial one
            if (m_comb_service->current_combinatoric() == 0) {
                if (entered_component()) { m_entered_particles.push_back(entry); }
                if (nPassed() + combinatoric_offset() > m_designpart) {
                    set_entered(false);
                    return 1.;
                }
            } else {
                /// Special use case we need to think of how to treat it
                if (n_combinations() <= m_comb_service->current_combinatoric()) {
                    set_entered(false);
                    return 1.;
                }
                std::vector<unsigned int> tuple = combinatoric_set()->draw(m_comb_service->current_combinatoric());
                /// Keep in mind that one previous particle might fall into the overflow of the fake-factor map.
                std::vector<unsigned int>::const_iterator itr =
                    std::lower_bound(m_entered_particles.begin(), m_entered_particles.end(), entry);
                unsigned int to_look_in_tuple = combinatoric_offset() + (itr - m_entered_particles.begin());
                if (!IsElementInList(tuple, to_look_in_tuple)) {
                    set_entered(false);
                    return 1.;
                }
            }
        }
        return W;
    }
    bool FakeFactor::entered_globally() const { return m_globally_entered && (m_last_ev == m_service->currentEvent()); }
    bool FakeFactor::entered_component() const { return m_last_entry_entered; }
    int FakeFactor::processBin() const { return m_processBin; }
    void FakeFactor::setProcessBin(int bin) { m_processBin = bin; }
    int FakeFactor::fakeType() const { return m_fakeBin; }
    void FakeFactor::setFakeType(int type) { m_fakeBin = type; }
    void FakeFactor::doNotFlipDataMC(bool B) { m_freeze_sign = B; }
    void FakeFactor::isSubtractingFakeFF(bool B) { m_is_subtracting = B; }
    int FakeFactor::sign() const {
        if (entered_globally()) {
            if (m_is_subtracting) return (m_freeze_sign || m_weighter->isData()) ? -1 : 1;
            return (m_freeze_sign || m_weighter->isData()) ? 1 : -1;
        }
        return 1;
    }
    FakeFactor::FakeFactor() :
        IExternalWeight(),
        IPermutationElement(),
        m_Particle(nullptr),
        m_x(nullptr),
        m_y(nullptr),
        m_z(nullptr),
        m_fakeBin(-1),
        m_processBin(-1),
        m_freeze_sign(false),
        m_is_subtracting(false),
        m_service(EventService::getService()),
        m_comb_service(CombinatoricService::getService()),
        m_weighter(Weight::getWeighter()),
        m_start_in_container(0),
        m_end_in_container(-1),
        m_designpart(0),
        m_includeCombinatorics(false),
        m_combinatorics(nullptr),
        m_entered_particles(),
        m_comb_grp(0),
        m_comb_grp_helper(nullptr),
        m_comb_offset(0),
        m_last_ev(-1),
        m_globally_entered(false),
        m_last_entry_entered(false),
        m_n_passed(0),
        m_syst_sub_grp() {}

    std::string FakeFactor::systematic_subgroup() const { return m_syst_sub_grp; }
    void FakeFactor::setSystematicSubgroup(const std::string& sys_group) { m_syst_sub_grp = sys_group; }

    void FakeFactor::setCombinatoricGroup(unsigned int grp) { m_comb_grp = grp; }
    unsigned int FakeFactor::combinatoric_group() const { return m_comb_grp; }
    void FakeFactor::setCombinatoricParticleGrouping(std::shared_ptr<CombinatoricParticleGrouping> grp) {
        if (grp) m_comb_grp_helper = grp;
    }
    void FakeFactor::set_entered(bool B) {
        if (m_last_ev != m_service->currentEvent()) {
            m_last_ev = m_service->currentEvent();
            m_globally_entered = false;
            m_n_passed = 0;
            if (include_combinatorics()) {
                m_combinatorics.reset();
                m_entered_particles.clear();
                if (m_entered_particles.capacity() < particle()->Size()) m_entered_particles.reserve(particle()->Size());
                if (m_comb_grp_helper) m_comb_grp_helper->reset_combinatorics();
            }
        }
        if (B && (!m_includeCombinatorics || m_comb_service->current_combinatoric() == 0)) ++m_n_passed;
        m_last_entry_entered = B;
        m_globally_entered |= B;
        if (m_comb_grp_helper) {
            m_comb_grp_helper->setObject(this);
            m_comb_offset = m_comb_grp_helper->nPassed();
        }
        if (B && m_includeCombinatorics && m_comb_service->current_combinatoric() == 0) {
            // make sure that n is always greater then the number of design particles --> exception otherwise
            unsigned int n = std::max(nPassed() + combinatoric_offset(), num_design_parts());

            if (!m_comb_grp_helper)
                m_combinatorics = m_comb_service->comb_to_draw(n, num_design_parts(), m_start_in_container);
            else
                m_comb_grp_helper->set_combinatoric(m_comb_service->comb_to_draw(n, num_design_parts(), m_start_in_container));
        }
    }
    unsigned int FakeFactor::combinatoric_offset() const { return m_comb_offset; }
    unsigned int FakeFactor::n_combinations() const {
        std::shared_ptr<DrawElementsFromSet> set = combinatoric_set();
        return set ? set->size() : 0;
    }
    const std::vector<unsigned int>& FakeFactor::get_entered() const { return m_entered_particles; }
    std::shared_ptr<DrawElementsFromSet> FakeFactor::combinatoric_set() const {
        if (m_comb_grp_helper) { return m_comb_grp_helper->combinatoric_set(); }
        return m_combinatorics;
    }
    void FakeFactor::setBeginPosition(size_t pos) { m_start_in_container = pos; }
    void FakeFactor::setEndPosition(size_t pos) { m_end_in_container = pos; }
    void FakeFactor::setDesignPartNumber(unsigned int p) {
        m_includeCombinatorics = p > 0;
        m_designpart = p;
    }
    unsigned int FakeFactor::num_design_parts() const { return m_designpart; }
    bool FakeFactor::include_combinatorics() const { return m_includeCombinatorics; }
    bool FakeFactor::SetParticle(IParticleReader* P) {
        if (m_Particle) {
            Error("FakeFactor::SetParticle()", "The weight has already an associated particle " + m_Particle->name());
            return false;
        } else if (!P) {
            Error("FakeFactor::SetParticle()", "No particle given.");
            return false;
        }
        m_Particle = P;
        return true;
    }
    bool FakeFactor::SetVar(IParticleVarReader* out, IParticleVarReader*& Ref) {
        if (Ref) {
            Error("FakeFactor::SetVar()", "The Fake factor already uses " + Ref->name());
            return false;
        } else if (!out) {
            Error("FakeFactor::SetVar()", "No variable given");
            return false;
        } else if (!m_Particle) {
            Warning("FakeFactorVar::SetVar()", "No particle defined yet");
        } else if (out->getParticle() != m_Particle) {
            Error("FakeFactorVar::SetVar()", "The variable " + out->name() + " cannot be associated with " + m_Particle->name());
            return false;
        }
        Ref = out;
        return true;
    }
    bool FakeFactor::SetXVar(IParticleVarReader* x) { return SetVar(x, m_x); }
    bool FakeFactor::SetYVar(IParticleVarReader* y) { return SetVar(y, m_y); }
    bool FakeFactor::SetZVar(IParticleVarReader* z) { return SetVar(z, m_z); }
    bool FakeFactor::init(TTree* t) {
        if (!m_Particle || !lookup_table()) {
            Error("FakeFactor::init()", "No histogram or particle defined. These are the two basic things to define a fake-factor");
            return false;
        }
        if (m_end_in_container <= m_start_in_container) {
            Error("FakeFactor::init()", "What's the sense of this? No element is selected from the container");
            return false;
        }
        if (m_includeCombinatorics && m_designpart == 0) {
            Error("FakeFactor::init()",
                  "The combinatorics should be respected although it's not clear how often the factor should be applied");
            return false;
        }
        if (!m_Particle->init(t)) return false;
        if (m_x && !m_x->init(t)) return false;
        if (m_y && !m_y->init(t)) return false;
        if (m_z && !m_z->init(t)) return false;
        return true;
    }
    IParticleVarReader* FakeFactor::xVariable() const { return m_x; }
    IParticleVarReader* FakeFactor::yVariable() const { return m_y; }
    IParticleVarReader* FakeFactor::zVariable() const { return m_z; }
    IParticleReader* FakeFactor::particle() const { return m_Particle; }
    unsigned int FakeFactor::nPassed() const { return entered_globally() ? m_n_passed : 0; }

    std::shared_ptr<FakeFactor> FakeFactor::clone() const { return std::make_shared<FakeFactor>(*this); }

    //##############################################################################################################
    //                                        CombinedFakeFactor
    //##############################################################################################################
    CombinedFakeFactor::CombinedFakeFactor() : FakeFactor(), m_ff() {}
    unsigned int CombinedFakeFactor::global_pos(unsigned int fake_type, unsigned int process_bin) const {
        return fake_type + process_bin * fakeType();
    }
    double CombinedFakeFactor::Evaluate(size_t p) {
        set_entered(false);
        if (!particle()->At(p) || !passCut()) { return 1.; }
        double W = 1.;
        if (fakeType() < 1) {
            for (const auto& ff : m_ff) {
                W *= ff->Evaluate(p);
                if (!entered_component()) set_entered(ff->entered_component());
            }
        } else {
            /// Here the fun kicks in we need to setup a matrix first containing all of the content
            int dimension = fakeType() * (processBin() < 1 ? 1 : processBin());
            std::vector<std::pair<double, bool>> fake_matrix(dimension, std::pair<double, bool>(0., false));
            std::vector<std::pair<double, bool>>::iterator begin = fake_matrix.begin();

            std::function<void(int, double)> multiply_ff = [&dimension, &begin, this](int i, double w) {
                if (i >= dimension) {
                    throw std::runtime_error(Form("Maximum %d fake factors (%d, %d) could be given. While %d is requested", dimension,
                                                  fakeType(), processBin(), i));
                }
                std::vector<std::pair<double, bool>>::iterator itr = begin + i;
                if (!itr->second) {
                    itr->first = w;
                    itr->second = true;
                } else {
                    (itr->first) *= w;
                }
                if (!entered_component()) set_entered(true);
            };

            std::function<bool(int)> has_hit = [&dimension, &begin](int i) {
                if (i >= dimension) {
                    throw std::runtime_error(Form("Maximum %d fake factors could be given. While %d is requested", dimension, i));
                }
                std::vector<std::pair<double, bool>>::const_iterator itr = begin + i;
                return itr->second;
            };
            for (const auto& FF : m_ff) {
                if (FF->fakeType() < 0) {
                    Warning("CombinedFakeFactor()",
                            "Found fake-component with negative fakeBin. Please fix the config via setting SetFakeType");
                    break;
                }
                double fw = FF->Evaluate(p);
                if (!FF->entered_component()) continue;

                /// No split in SM proceses is given so just apply the weight
                if (FF->processBin() < 0 && processBin() < 1) {
                    multiply_ff(FF->fakeType(), fw);
                }
                /// The fake type and background process are well
                /// defined quantities
                else if (FF->processBin() >= 0) {
                    unsigned int k = global_pos(FF->fakeType(), FF->processBin());
                    multiply_ff(k, fw);
                }
                /// Presumably that's a scale-factor to be applied for all of the components so let's multiply it
                /// throughout the matrix. No check of the matrix_set is required at this stage since negative processes
                /// are evaluated at the very last of each fake-type
                else {
                    for (int i = 0; i < processBin(); ++i) {
                        unsigned int k = global_pos(FF->fakeType(), i);
                        if (has_hit(k)) multiply_ff(k, fw);
                    }
                }
            }
            /// Wuhu the matrix is built. Now let's contract it
            if (entered_component()) {
                W = std::accumulate(fake_matrix.begin(), fake_matrix.end(), 0.,
                                    [](double a, const std::pair<double, bool>& e) { return a + e.first; });
            }
        }
        return W;
    }
    int CombinedFakeFactor::sign() const {
        if (!entered_globally()) return 1;
        for (const auto& ff : m_ff) {
            if (ff->sign() < 0) return ff->sign();
        }
        return 1;
    }

    bool CombinedFakeFactor::init(TTree* t) {
        if (!particle()) {
            Error("CombinedFakeFactor()", "No particle has been given");
            return false;
        }
        for (const auto& ff : m_ff) {
            if (!ff->init(t)) return false;
        }
        return true;
    }
    bool CombinedFakeFactor::add_fake_factor(const std::shared_ptr<FakeFactor>& ff) {
        if (!ff || (!m_ff.empty() && ff->particle() != particle()) || ff.get() == this) return false;
        if (m_ff.empty()) SetParticle(ff->particle());
        m_ff.push_back(ff);
        setProcessBin(std::max(processBin(), ff->processBin() + 1));
        setFakeType(std::max(fakeType(), ff->fakeType() + 1));
        setDesignPartNumber(std::max(num_design_parts(), ff->num_design_parts()));
        setCombinatoricGroup(std::max(combinatoric_group(), ff->combinatoric_group()));
        std::sort(m_ff.begin(), m_ff.end(), [](const std::shared_ptr<FakeFactor>& a, const std::shared_ptr<FakeFactor>& b) {
            if (a->fakeType() != b->fakeType()) return a->fakeType() > b->fakeType();
            return a->processBin() > b->processBin();
        });
        for (const auto& ff : m_ff) { ff->setDesignPartNumber(num_design_parts()); }
        return true;
    }
    std::shared_ptr<FakeFactor> CombinedFakeFactor::clone() const {
        std::shared_ptr<CombinedFakeFactor> cloned = std::make_shared<CombinedFakeFactor>(*this);
        cloned->m_ff.clear();
        for (const auto& ff : m_ff) cloned->add_fake_factor(ff->clone());
        return cloned;
    }
    bool CombinedFakeFactor::applyUncertainty(double w) {
        for (auto& ff : m_ff) {
            if (ff->systematic_subgroup() == systematic_subgroup() && !ff->applyUncertainty(w)) return false;
        }
        return true;
    }
    void CombinedFakeFactor::setCombinatoricParticleGrouping(std::shared_ptr<CombinatoricParticleGrouping> grp) {
        for (const auto& ff : m_ff) { ff->setCombinatoricParticleGrouping(grp); }
        if (grp) grp->add_member(this);
        FakeFactor::setCombinatoricParticleGrouping(grp);
    }
    //##############################################################################################################
    //                                        MixedCombFakeFactor
    //##############################################################################################################
    MixedCombFakeFactor::MixedCombFakeFactor() : CombinedFakeFactor() {}

    std::shared_ptr<FakeFactor> MixedCombFakeFactor::clone() const {
        std::shared_ptr<MixedCombFakeFactor> cloned = std::make_shared<MixedCombFakeFactor>(*this);
        cloned->m_ff.clear();
        for (const auto& ff : m_ff) cloned->add_fake_factor(ff->clone());
        return cloned;
    }
    bool MixedCombFakeFactor::add_fake_factor(const std::shared_ptr<FakeFactor>& ff) {
        if (!ff || ff.get() == this) return false;
        if (m_ff.empty()) SetParticle(ff->particle());
        m_ff.push_back(ff);
        setProcessBin(std::max(processBin(), ff->processBin() + 1));
        setFakeType(std::max(fakeType(), ff->fakeType() + 1));
        setDesignPartNumber(std::max(num_design_parts(), ff->num_design_parts()));
        setCombinatoricGroup(std::max(combinatoric_group(), ff->combinatoric_group()));
        std::sort(m_ff.begin(), m_ff.end(), [](const std::shared_ptr<FakeFactor>& a, const std::shared_ptr<FakeFactor>& b) {
            if (a->fakeType() != b->fakeType()) return a->fakeType() > b->fakeType();
            return a->processBin() > b->processBin();
        });
        for (const auto& ff : m_ff) { ff->setDesignPartNumber(num_design_parts()); }

        return true;
    }

    //##############################################################################################################
    //                                        CombinatoricParticleGrouping
    //##############################################################################################################
    CombinatoricParticleGrouping::CombinatoricParticleGrouping(unsigned int grp_num) :
        m_grp_num(grp_num),
        m_members(),
        m_combinatorics(nullptr),
        m_num_passed(0),
        m_current_obj(nullptr),
        m_weighter(Weight::getWeighter()),
        m_last_ev(-1) {}
    void CombinatoricParticleGrouping::reset_combinatorics() {
        if (m_last_ev == m_weighter->eventNumber()) return;
        m_last_ev = m_weighter->eventNumber();
        m_combinatorics.reset();
    }
    void CombinatoricParticleGrouping::set_combinatoric(std::shared_ptr<DrawElementsFromSet> comb) { m_combinatorics = comb; }
    std::shared_ptr<DrawElementsFromSet> CombinatoricParticleGrouping::combinatoric_set() const { return m_combinatorics; }
    unsigned int CombinatoricParticleGrouping::combinatoric_group() const { return m_grp_num; }
    void CombinatoricParticleGrouping::add_member(FakeFactor* ff) {
        m_members.push_back(ff);
        std::sort(m_members.begin(), m_members.end(),
                  [](const FakeFactor* a, const FakeFactor* b) { return a->particle()->name() < b->particle()->name(); });
    }
    unsigned int CombinatoricParticleGrouping::nPassed() const { return m_num_passed; }
    void CombinatoricParticleGrouping::setObject(FakeFactor* ff) {
        if (m_current_obj && m_current_obj->particle() == ff->particle()) return;
        m_current_obj = nullptr;
        m_num_passed = 0;
        for (const auto& mem : m_members) {
            if (mem->particle() == ff->particle()) {
                m_current_obj = mem;
                return;
            }
            if (mem->entered_globally()) m_num_passed += mem->nPassed();
        }
    }
    //##############################################################################################################
    //                                        FakeFactorBundle
    //##############################################################################################################
    FakeFactorBundle::FakeFactorBundle(const std::string& WeightName, const std::string& Syst) :
        m_Syst(Syst),
        m_FF(),
        m_Pseudo(PseudoScalarVarReader::GetReader(WeightName + (Syst.empty() ? "" : "_" + Syst))),
        m_weight(Weight::getWeighter()),
        m_comb_service(CombinatoricService::getService()),
        m_types_cached(false),
        m_lumi(-1),
        m_DoCombinatorics(false),
        m_cut(nullptr),
        m_last_ev(-1),
        m_allow_mixed_particles(false) {}

    std::string FakeFactorBundle::name() const { return m_Syst; }
    void FakeFactorBundle::Add(std::shared_ptr<FakeFactor> FF) { m_FF.push_back(FF); }
    void FakeFactorBundle::includeCombinatorics(bool B) { m_DoCombinatorics = B; }
    double FakeFactorBundle::Evaluate() {
        if (m_last_ev == m_weight->eventNumber()) { return m_Pseudo->read(); }
        m_last_ev = m_weight->eventNumber();
        double W = 1.;
        if (passCut()) {
            bool passed = false;
            int sign = 1;
            std::map<unsigned int, unsigned int> group_factors;
            for (auto& ff : m_FF) {
                double p_w = 1.;
                for (size_t p = 0; p < ff->particle()->Size(); ++p) { p_w *= ff->Evaluate(p); }
                sign = std::min(sign, ff->sign());
                if (ff->entered_globally()) {
                    if (ff->include_combinatorics()) {
                        // The weight is indepndent from any combinatoric group
                        if (ff->combinatoric_group() == 0)
                            p_w /= ff->n_combinations();
                        else {
                            std::map<unsigned int, unsigned int>::iterator itr = group_factors.find(ff->combinatoric_group());
                            if (itr == group_factors.end())
                                group_factors.insert(std::pair<unsigned int, unsigned int>(ff->combinatoric_group(), ff->n_combinations()));
                            else
                                itr->second = std::max(itr->second, ff->n_combinations());
                        }
                    }
                    passed = true;
                }
                W *= p_w;
            }
            if (passed && m_lumi > 0 && m_weight->isData()) W /= m_lumi;
            if (!group_factors.empty()) {
                for (const auto& comb_fac : group_factors) { W /= comb_fac.second; }
            }
            W *= sign;
        }
        m_Pseudo->SetValue(W);
        return W;
    }
    bool FakeFactorBundle::init(TTree* T) {
        if (m_FF.empty()) {
            Error("FakeFactorBundle::init()", Form("No fake factors defined in %s", name().c_str()));
            return false;
        }
        m_last_ev = -1;
        if (!m_types_cached) {
            std::vector<std::shared_ptr<FakeFactor>> tmp_ff = m_FF;
            std::vector<std::shared_ptr<CombinatoricParticleGrouping>> grp_elements;
            m_FF.clear();
            while (!tmp_ff.empty()) {
                if (!m_allow_mixed_particles) {
                    std::shared_ptr<CombinedFakeFactor> comb = std::make_shared<CombinedFakeFactor>();

                    std::vector<std::shared_ptr<FakeFactor>>::iterator begin = tmp_ff.begin();
                    while (begin != tmp_ff.end()) {
                        if (!comb->add_fake_factor(*begin)) return false;
                        tmp_ff.erase(begin);
                        begin = std::find_if(tmp_ff.begin(), tmp_ff.end(),
                                             [&comb](const std::shared_ptr<FakeFactor>& ff) { return ff->particle() == comb->particle(); });
                    }
                    /// Build together the combinatoric groups
                    if (comb->combinatoric_group() > 0) {
                        std::vector<std::shared_ptr<CombinatoricParticleGrouping>>::const_iterator itr = std::find_if(
                            grp_elements.begin(), grp_elements.end(), [&comb](const std::shared_ptr<CombinatoricParticleGrouping>& grp) {
                                return comb->combinatoric_group() == grp->combinatoric_group();
                            });
                        if (itr != grp_elements.end())
                            comb->setCombinatoricParticleGrouping(*itr);
                        else {
                            grp_elements.push_back(std::make_shared<CombinatoricParticleGrouping>(comb->combinatoric_group()));
                            comb->setCombinatoricParticleGrouping(grp_elements.back());
                        }
                    }

                    m_FF.push_back(comb);
                } else {
                    std::shared_ptr<MixedCombFakeFactor> comb = std::make_shared<MixedCombFakeFactor>();
                    for (auto& ff : tmp_ff) {
                        if (!comb->add_fake_factor(ff)) return false;
                    }
                    m_FF.push_back(comb);
                }
            }
            std::sort(m_FF.begin(), m_FF.end(), [](const std::shared_ptr<FakeFactor>& a, const std::shared_ptr<FakeFactor>& b) {
                return a->particle()->name() < b->particle()->name();
            });
            m_types_cached = true;
        }
        for (const auto& FF : m_FF) {
            if (!FF->init(T)) return false;
        }

        return true;
    }
    ITreeVarReader* FakeFactorBundle::GetReader() const { return m_Pseudo; }
    size_t FakeFactorBundle::nComponents() const { return m_FF.size(); }
    bool FakeFactorBundle::applyUncertainty(double W) {
        for (auto& ff : m_FF) {
            if (!ff->applyUncertainty(W)) return false;
        };
        return true;
    }
    void FakeFactorBundle::clone_from(std::shared_ptr<FakeFactorBundle> other) {
        if (!other || other.get() == this) return;
        m_FF.clear();
        m_types_cached = false;
        for (auto FF : other->m_FF) m_FF.push_back(FF->clone());
    }
    void FakeFactorBundle::setDataLumi(double lumi) {
        if (lumi > 0) m_lumi = lumi;
    }
    bool FakeFactorBundle::passCut() const { return !m_cut || m_cut->Pass(); }
    void FakeFactorBundle::setCut(std::shared_ptr<Condition> cut) { m_cut = cut; }
    void FakeFactorBundle::setSystematicSubgroup(const std::string& grp) {
        for (auto& ff : m_FF) { ff->setSystematicSubgroup(grp); }
    }
    void FakeFactorBundle::allow_particle_mixing() { m_allow_mixed_particles = true; }
    //##############################################################################################################
    //                                        FakeFactorWeight
    //##############################################################################################################
    FakeFactorWeight* FakeFactorWeight::GetWeighter(const std::string& Element) {
        if (!Weight::getWeighter()->GetWeightElement(Element)) return new FakeFactorWeight(Element);
        return dynamic_cast<FakeFactorWeight*>(Weight::getWeighter()->GetWeightElement(Element));
    }
    FakeFactorWeight::FakeFactorWeight(const std::string& Name) :
        m_Name(Name),
        m_FF(),
        m_FFSyst(nullptr),
        m_Init(false),
        m_Registered(false),
        m_lumi(-1),
        m_cut(nullptr),
        m_includeCombinatorics(false),
        m_syst_sub_grp(),
        m_allow_particle_mixing(false) {
        m_Registered = Weight::getWeighter()->RegisterWeightElement(this);
    }

    std::string FakeFactorWeight::name() const { return m_Name; }
    void FakeFactorWeight::CreateSystematic(const std::string& Syst) {
        if (m_FF.empty() || !LoadSystematic(Syst)) {
            XAMPP::Info("FakeFactorWeight::CreateSystematic()", "New systematic " + Syst + " inserted");
            m_FF.push_back(std::make_shared<FakeFactorBundle>(name(), Syst));
            LoadSystematic(Syst);
        }
    }
    bool FakeFactorWeight::LoadSystematic(const std::string& Syst) {
        for (auto& FF : m_FF) {
            if (FF->name() == Syst) {
                m_FFSyst = FF;
                return true;
            }
        }
        LoadSystematic("");
        return false;
    }
    bool FakeFactorWeight::init(TTree* t) {
        if (!m_Registered || m_FF.empty()) {
            XAMPP::Error("FakeFactorWeight::init()",
                         "No fake-factors have been configured yet. Please make sure to give the proper configuration.");
            return false;
        }
        CreateStatErrors();
        if (SimultaneousSyst()) {
            for (auto& FF : m_FF)
                if (!FF->init(t)) return false;

        } else if (!m_FFSyst->init(t))
            return false;

        return true;
    }
    double FakeFactorWeight::read() {
        // The FakeFactors of all defined systematics must be present for each event.

        if (SimultaneousSyst()) {
            for (auto& FF : m_FF) FF->Evaluate();
            return GetTreeVarReader()->read();
        }
        return m_FFSyst->Evaluate();
    }
    ITreeVarReader* FakeFactorWeight::GetTreeVarReader() { return m_FFSyst->GetReader(); }
    ITreeVarReader* FakeFactorWeight::GetSystematicReader(const std::string& Syst) {
        LoadSystematic(Syst);
        ITreeVarReader* R = GetTreeVarReader();
        LoadSystematic();
        return R;
    }
    void FakeFactorWeight::CreateStatErrors() {
        if (m_Init) return;
        if (m_allow_particle_mixing) {
            for (auto& ff : m_FF) ff->allow_particle_mixing();
        }

        /// Automatically create the stat uncertainties from the nominal
        LoadSystematic("");
        std::shared_ptr<FakeFactorBundle> nominal = m_FFSyst;
        std::vector<std::string> sub_grps = m_syst_sub_grp;
        if (sub_grps.empty()) sub_grps.push_back("");
        for (auto& sub : sub_grps) {
            CreateSystematic(name() + std::string(sub.empty() ? "" : "_" + sub) + "_STAT__1UP");
            m_FFSyst->clone_from(nominal);
            m_FFSyst->setSystematicSubgroup(sub);
            m_FFSyst->applyUncertainty(1.);
            CreateSystematic(name() + std::string(sub.empty() ? "" : "_" + sub) + "_STAT__1DN");
            m_FFSyst->clone_from(nominal);
            m_FFSyst->setSystematicSubgroup(sub);
            m_FFSyst->applyUncertainty(-1.);
        }
        LoadSystematic("");
        for (auto& ff : m_FF) {
            ff->setDataLumi(m_lumi);
            ff->includeCombinatorics(m_includeCombinatorics);
            ff->setCut(m_cut);
        }
        m_syst_sub_grp.clear();
        m_Init = true;
    }
    bool FakeFactorWeight::ReadFakeFactorConfig(std::ifstream& inf, std::vector<ITreeVarReader*>& readers) {
        if (!m_FF.empty()) {
            Warning("FakeFactorWeight::ReadFakeFactorConfig()",
                    Form("%s has already been initialized before. What comes in the following", name().c_str()));
            Warning("FakeFactorWeight::ReadFakeFactorConfig()",
                    "is straight added to the definition of the weight. There is no safety mechansim avoiding double attachment of the "
                    "same histogram!");
        }
        CreateSystematic("");
        std::string line;
        IfDefLineParser ReadLine;
        while (ReadLine(inf, line) == IfDefLineParser::NewProperty) {
            std::stringstream sstr(line);
            if (IsKeyWordSatisfied(sstr, "End_FakeFactorWeight"))
                return true;
            else if (IsKeyWordSatisfied(sstr, "AddFake") && !AppendFakeFactor(inf, readers))
                return false;
            else if (IsKeyWordSatisfied(sstr, "SetLumi"))
                m_lumi = GetDoubleFromStream(sstr);
            else if (IsKeyWordSatisfied(sstr, "IncludeCombinatoric"))
                m_includeCombinatorics = true;
            else if (IsKeyWordSatisfied(sstr, "MixParticles"))
                m_allow_particle_mixing = true;
            std::string word = GetWordFromStream(sstr);
            if (word.find("Cut") != std::string::npos) {
                std::shared_ptr<Condition> C;
                if (word.find("CombCut") != std::string::npos)
                    C = CreateCombinedCut(inf, readers, line);
                else
                    C = CreateCut(line, readers);
                if (C == nullptr) return false;
                m_cut = C;
            }
        }
        Error("FakeFactorWeight::ReadFakeFactorConfig()", "Missing 'End_FakeFactorWeight' statement");
        return false;
    }
    bool FakeFactorWeight::AppendFakeFactor(std::ifstream& inf, std::vector<ITreeVarReader*>& Readers) {
        std::shared_ptr<FakeFactor> NewFF = std::make_shared<FakeFactor>();
        std::string line, word;
        IfDefLineParser ReadLine;
        while (ReadLine(inf, line) == IfDefLineParser::NewProperty) {
            std::stringstream sstr(line);
            word = GetWordFromStream(sstr);
            if (word == "Particle" && !NewFF->SetParticle(ParReaderStorage::GetInstance()->GetReader(GetWordFromStream(sstr))))
                return false;
            else if (word == "xVariable" &&
                     !NewFF->SetXVar(dynamic_cast<IParticleVarReader*>(ReaderProvider::GetInstance()->CreateReader(sstr))))
                return false;
            else if (word == "yVariable" &&
                     !NewFF->SetYVar(dynamic_cast<IParticleVarReader*>(ReaderProvider::GetInstance()->CreateReader(sstr))))
                return false;
            else if (word == "zVariable" &&
                     !NewFF->SetZVar(dynamic_cast<IParticleVarReader*>(ReaderProvider::GetInstance()->CreateReader(sstr))))
                return false;
            else if (word == "FakeFactorHisto" && !NewFF->LoadHistogramFromFile(sstr))
                return false;
            else if (word == "Systematic")
                CreateSystematic(GetWordFromStream(sstr));
            else if (word.find("Cut") != std::string::npos) {
                std::shared_ptr<Condition> C;
                if (word.find("CombCut") != std::string::npos)
                    C = CreateCombinedCut(inf, Readers, line);
                else
                    C = CreateCut(line, Readers);
                if (C == nullptr) return false;
                NewFF->setCut(C);
            } else if (word == "SetCombinatoricGroup")
                NewFF->setCombinatoricGroup(GetIntegerFromStream(sstr));
            else if (word == "FreezeSign")
                NewFF->doNotFlipDataMC();
            else if (word == "IsDestructive")
                NewFF->isSubtractingFakeFF();
            else if (word == "SetBeginPosition")
                NewFF->setBeginPosition(GetIntegerFromStream(sstr));
            else if (word == "SetEndPosition")
                NewFF->setEndPosition(GetIntegerFromStream(sstr));
            else if (word == "SetDesignNumber")
                NewFF->setDesignPartNumber(GetIntegerFromStream(sstr));
            else if (word == "SetLumi")
                m_lumi = GetDoubleFromStream(sstr);
            else if (word == "SetProcessBin") {
                NewFF->setProcessBin(GetIntegerFromStream(sstr));
            } else if (word == "SetFakeType") {
                NewFF->setFakeType(GetIntegerFromStream(sstr));
            } else if (word == "End_Fake") {
                m_FFSyst->Add(NewFF);
                ResetSystematic();
                return true;
            } else if (word == "SetSystematicSubGroup") {
                std::string sub_grp = GetWordFromStream(sstr);
                if (!IsElementInList(m_syst_sub_grp, sub_grp)) {
                    m_syst_sub_grp.push_back(sub_grp);
                    std::sort(m_syst_sub_grp.begin(), m_syst_sub_grp.end());
                }
                NewFF->setSystematicSubgroup(sub_grp);
            }
        }
        Error("FakeFactorWeight::AppendFakeFactor()", "Missing 'End_Fake' statement");
        return false;
    }
    std::vector<std::string> FakeFactorWeight::FindWeightVariations(TTree*) { return GetWeightVariations(); }
    std::vector<std::string> FakeFactorWeight::GetWeightVariations() {
        std::vector<std::string> V;
        CreateStatErrors();
        for (auto& Syst : m_FF)
            if (!Syst->name().empty()) V.push_back(Syst->name());
        return V;
    }
    IWeightElement::SystStatus FakeFactorWeight::ApplySystematic(const std::string& variation) {
        if (LoadSystematic(variation)) return IWeightElement::SystStatus::Affected;
        return IWeightElement::SystStatus::NotAffected;
    }
    void FakeFactorWeight::ResetSystematic() { LoadSystematic(""); }
    bool FakeFactorWeight::AddFakeFactor(std::shared_ptr<FakeFactorBundle> bundle) {
        if (m_Init) {
            Error("FakeFactorWeight::AddFakeFactor()", Form("%s is already initialized", name().c_str()));
            return false;
        }
        if (bundle->nComponents() == 0) {
            Error("FakeFactorWeight::AddFakeFactor()", Form("Nothing has been declared for %s", bundle->name().c_str()));
            return false;
        }
        for (const auto& known : m_FF) {
            if (known->name() == bundle->name()) {
                Error("FakeFactorWeight::AddFakeFactor()", Form("%s has been already added as systematic", known->name().c_str()));
                return false;
            }
        }
        m_FF.push_back(bundle);
        LoadSystematic("");
        return true;
    }
    bool FakeFactorWeight::AddFakeFactor(std::shared_ptr<FakeFactor> fakes, const std::string& syst) {
        std::shared_ptr<FakeFactorBundle> bundle = std::make_shared<FakeFactorBundle>(name(), syst);
        bundle->Add(fakes);
        if (!AddFakeFactor(bundle)) return false;
        return true;
    }
    //##############################################################################################################
    //                                        ConditionalWeight
    //##############################################################################################################
    std::string ConditionalWeight::name() const { return m_Name; }
    bool ConditionalWeight::init(TTree* t) {
        if (!m_Init || !m_Registered) {
            XAMPP::Error("ConditionalWeight::init()", "Something went wrong during initialization.");
            return false;
        }
        if (m_Weighters.empty()) {
            XAMPP::Error("ConditionalWeight::init()", "No weight or condition has been configured");
            return false;
        }
        for (const auto& Cond : m_Weighters) {
            if (Cond.Weight == this) {
                XAMPP::Error("ConditionalWeight::init()",
                             "Weightception encountered. Not clever to impose conditions on the Weight itself.");
                return false;
            }
            if (m_disabled) continue;
            if (!Cond.Weight->init(t)) return false;
        }
        m_eventNumber = -1;
        return true;
    }
    double ConditionalWeight::read() {
        double W = 1.;
        UpdateCuts();
        if (m_disabled)
            return W;
        else if (SimultaneousSyst()) {
            for (const auto& Var : m_Variations) { Var->NewEvent(true); }
        } else if (m_VarItr != m_VarEndItr) {
            (*m_VarItr)->NewEvent(true);
            W = (*m_VarItr)->GetTreeVarReader()->read();
        } else if (!SimultaneousSyst()) {
            Warning("ConditionalWeight::read()", "Unable to apply " + name() + ".");
        }
        return W;
    }
    bool ConditionalWeight::ApplyWeighter(IWeightElement* Weight) const {
        bool Apply = false;
        bool Found = false;
        for (const auto& CondWeighter : m_Weighters) {
            if (CondWeighter.Weight == Weight) {
                Apply = Apply || CondWeighter.Passed;
                Found = true;
            }
            if (Apply) { return Apply; }
        }
        if (!Found) {
            Warning("ConditionalWeight::ApplyWeighter()", " does not know any condition to be applied on " + Weight->name() + ".");
        }
        return Apply;
    }
    std::vector<std::string> ConditionalWeight::FindWeightVariations(TTree* Tree) {
        if (m_Variations.size() < 2) {
            std::vector<std::string> Variations;
            // Initialize first all variations
            for (const auto& Cond : m_Weighters) {
                std::vector<std::string> WeighterVar = Cond.Weight->FindWeightVariations(Tree);
                CopyVector(WeighterVar, Variations);
            }
            // Then copy them to the tree
            for (const auto& V : Variations) {
                Conditional_Ptr CondVariation = std::make_shared<ConditionalWeight::ConditionalVariations>(V, *this);
                m_Variations.push_back(CondVariation);
                CondVariation->AppendConditional(m_Weighters);
            }
            ResetSystematic();
            m_VarEndItr = m_Variations.end();
        }
        return GetWeightVariations();
    }
    bool ConditionalWeight::LoadSystematic(const std::string& Variation) {
        for (m_VarItr = m_Variations.begin(); m_VarItr != m_VarEndItr; ++m_VarItr) {
            if ((*m_VarItr)->variation() == Variation) return true;
        }
        LoadSystematic();
        return false;
    }

    std::vector<std::string> ConditionalWeight::GetWeightVariations() {
        std::vector<std::string> Variations;
        for (const auto& V : m_Variations)
            if (!V->variation().empty()) Variations.push_back(V->variation());
        return Variations;
    }
    IWeightElement::SystStatus ConditionalWeight::ApplySystematic(const std::string& variation) {
        IWeightElement::SystStatus Status = IWeightElement::SystStatus::NotAffected;
        for (const auto& Cond : m_Weighters) {
            IWeightElement::SystStatus S = Cond.Weight->ApplySystematic(variation);
            if (S == IWeightElement::SystStatus::Affected)
                Status = S;
            else if (S == IWeightElement::SystStatus::SystError)
                return S;
        }
        if (Status == IWeightElement::SystStatus::Affected) {
            Info("ConditionalWeight::ApplySystematic()", "The systematic " + variation + " affects the weight " + name());
            LoadSystematic(variation);
        }
        return Status;
    }
    ITreeVarReader* ConditionalWeight::GetTreeVarReader() {
        if (m_VarItr == m_Variations.end()) {
            Warning("ConditionalWeight::GetTreeVarReader()", "No readers has been defined yet");
            return nullptr;
        }
        return (*m_VarItr)->GetTreeVarReader();
    }
    ITreeVarReader* ConditionalWeight::GetSystematicReader(const std::string& Syst) {
        LoadSystematic(Syst);
        ITreeVarReader* R = GetTreeVarReader();
        LoadSystematic();
        return R;
    }
    void ConditionalWeight::ResetSystematic() {
        for (const auto& Cond : m_Weighters) Cond.Weight->ResetSystematic();
        LoadSystematic();
    }
    ConditionalWeight::ConditionalWeight(const std::string& Name) :
        m_Name(Name),
        m_Weighters(),
        m_Variations(),
        m_VarItr(m_Variations.end()),
        m_VarEndItr(m_Variations.end()),
        m_Init(false),
        m_Registered(false),
        m_eventNumber(-1),
        m_weightService(Weight::getWeighter()),
        m_disabled(false),
        m_glob_cut(nullptr),
        m_glob_cut_passed(true)

    {
        m_Registered = m_weightService->RegisterWeightElement(this);
    }
    ConditionalWeight::~ConditionalWeight() {}
    ConditionalWeight* ConditionalWeight::GetWeighter(const std::string& Name) {
        if (!Weight::getWeighter()->GetWeightElement(Name)) return new ConditionalWeight(Name);
        return dynamic_cast<ConditionalWeight*>(Weight::getWeighter()->GetWeightElement(Name));
    }
    bool ConditionalWeight::LoadConfiguration(std::ifstream& inf, std::vector<ITreeVarReader*>& Readers) {
        if (m_Init) { Warning("ConditionalWeight::LoadConfiguration()", "The weight " + name() + " has already been initialized."); }
        Info("ConditionalWeight::LoadConfiguration()", "Configure " + name());
        std::string line;
        IfDefLineParser ReadLine;
        while (ReadLine(inf, line) == IfDefLineParser::NewProperty) {
            std::stringstream sstr(line);
            std::string word = GetWordFromStream(sstr);
            // Create a new Weight assigning new conditions to it
            if (word == "New_Conditional" && !GetWeightElement(inf, Readers)) return false;
            // If the information is temporarilly not in the tree for some reason but the user relies on the
            // conditions of the weight for his event selection, he can optionally disable the weight. The weighter
            // returns always 1, but it maintains the service of cuts
            else if (word == "Disable")
                m_disabled = true;
            else if (word.find("Cut") + 3 == word.size()) {
                if (word == "CombCut") {
                    m_glob_cut = CreateCombinedCut(inf, Readers, line);
                } else
                    m_glob_cut = CreateCut(line, Readers);
                if (!m_glob_cut) {
                    Error("ConditionalWeight::GetWeightElement()", "No Cut could be extracted.");
                    return false;
                }
            } else if (word == "End_CondWeight") {
                m_Init = true;
                // Insert the nominal weight variation
                Conditional_Ptr V = std::make_shared<ConditionalWeight::ConditionalVariations>("", *this);
                V->AppendConditional(m_Weighters);
                m_Variations.push_back(V);
                m_VarEndItr = m_Variations.end();
                return LoadSystematic();
            }
        }
        Error("ConditionalWeight::LoadConfiguration()", "Missing 'End_CondWeight' statement");
        return false;
    }
    bool ConditionalWeight::GetWeightElement(std::ifstream& inf, std::vector<ITreeVarReader*>& Readers) {
        std::string line;
        IWeightElement* Weight = nullptr;
        std::shared_ptr<Condition> Cut;
        IfDefLineParser ReadLine;
        while (ReadLine(inf, line) == IfDefLineParser::NewProperty) {
            std::stringstream sstr(line);
            std::string word = GetWordFromStream(sstr);
            if (word == "End_Conditional") return AppendWeightElement(Weight, Cut);
            if (word == "Weight") {
                word = GetWordFromStream(sstr);
                // Retrieve first the generic weight element
                Weight = m_weightService->GetWeightElement(word);
                // The user did not define the name of the Weight thus far ->
                // Take it from the tree
                if (!Weight) Weight = WeightElementFromTree::GetWeighter(word);
            } else if (word == "FixedWeight") {
                word = GetWordFromStream(sstr);
                double FixedValue = GetDoubleFromStream(sstr);
                std::string ReaderName = Form("Fixed W %s_%.5f", word.c_str(), FixedValue);
                Weight = FixedExternalWeight::GetWeighter(ReaderName, ReaderName, FixedValue);
                if (!Weight) {
                    Error("ConditionalWeight::GetWeightElement()", "No FixedExternalWeight could be created.");
                    return false;
                }
            } else if (word == "FloatWeight")
                Weight = FloatWeightFromTree::GetWeighter(GetWordFromStream(sstr));
            // Impose the conditions here
            else if (word.find("Cut") + 3 == word.size()) {
                if (word == "CombCut") {
                    Cut = CreateCombinedCut(inf, Readers, line);
                } else
                    Cut = CreateCut(line, Readers);
                if (!Cut) {
                    Error("ConditionalWeight::GetWeightElement()", "No Cut could be extracted.");
                    return false;
                }
            }
        }
        Error("ConditionalWeight::GetWeightElement()", "Missing 'End_Condition' statement");
        return false;
    }
    bool ConditionalWeight::AppendWeightElement(IWeightElement* Weight, std::shared_ptr<Condition> Cut) {
        if (!Weight) {
            Error("ConditionalWeight::AppendWeightElement()", "No element was given");
            return false;
        }
        for (auto& W : m_Weighters) {
            if (W.Weight == Weight) {
                Warning("ConditionalWeight::AppendWeightElement()", "The weight " + Weight->name() +
                                                                        " has already been inserted as a conditional Weight using " +
                                                                        (W.Cut ? W.Cut->name() : "no condition defined"));
                /// Define the logical OR between both conditions
                if (W.Cut && Cut)
                    W.Cut == Condition::Combine(W.Cut, Cut, Condition::Mode::OR);
                else if (!W.Cut)
                    W.Cut = Cut;
                return true;
            }
        }
        CondWeight C;
        C.Weight = Weight;
        C.Cut = Cut;
        m_Weighters.push_back(C);
        Info("ConditionalWeight::AppendWeightElement()",
             "Append new element " + Weight->name() + " which is applied " + (Cut ? "if " + Cut->name() + " is satisfied" : "at any time"));
        return true;
    }
    bool ConditionalWeight::IsWeightApplied() {
        UpdateCuts();
        if (!m_glob_cut_passed) return false;
        for (auto Cond : m_Weighters) {
            if (Cond.Passed) return true;
        }
        return false;
    }
    void ConditionalWeight::UpdateCuts() {
        if (m_eventNumber == m_weightService->eventNumber()) return;
        m_eventNumber = m_weightService->eventNumber();
        m_glob_cut_passed = !m_glob_cut || m_glob_cut->Pass();
        for (auto& CondW : m_Weighters) { CondW.Passed = !CondW.Cut || CondW.Cut->Pass(); }
    }
    //##############################################################################################################
    //                                 ConditionalWeight::ConditionalVariations
    //##############################################################################################################
    ConditionalWeight::ConditionalVariations::ConditionalVariations(const std::string& Variation, ConditionalWeight& Conditional) :
        m_VarName(Variation),
        m_RefInstance(Conditional),
        m_Weights(),
        m_PseudoReader(nullptr) {
        m_PseudoReader = PseudoScalarVarReader::GetReader(m_RefInstance.name() + (variation().empty() ? "" : "_" + variation()));
        m_PseudoReader->SetValue(1.);
    }
    std::string ConditionalWeight::ConditionalVariations::variation() const { return m_VarName; }
    void ConditionalWeight::ConditionalVariations::AppendConditional(const std::vector<ConditionalWeight::CondWeight>& Weighters) {
        for (const auto& W : Weighters) { AppendConditional(W); }
    }
    void ConditionalWeight::ConditionalVariations::AppendConditional(ConditionalWeight::CondWeight Cond) {
        ITreeVarReader* R = Cond.Weight->GetSystematicReader(variation());
        if (m_Weights.find(R) == m_Weights.end()) {
            m_Weights.insert(std::pair<ITreeVarReader*, IWeightElement*>(R, Cond.Weight));
        } else {
            Warning("ConditionalVariations::AppendConditional()", "The weight " + Cond.Weight->name() + " is already known");
        }
    }
    void ConditionalWeight::ConditionalVariations::NewEvent(bool UseWeighter) {
        if (!m_PseudoReader) {
            Error("ConditionalVariations::NewEvent()", "There is no PseudoScalarVarReader where the Weight can be saved to");
            return;
        }
        if (m_Weights.empty()) {
            Error("ConditionalVariations::NewEvent()", "No weighter's given....");
            return;
        }
        m_PseudoReader->SetValue(1.);
        for (const auto& Weight : m_Weights) {
            if (!m_RefInstance.ApplyWeighter(Weight.second)) continue;
            // The use weighter function is called once the event
            if (UseWeighter) { Weight.second->read(); }
            // The UseWeighter switch does not matter for Weights stored in the input. If the Weight is more complex
            // Then each Weight pocesses a PseudoScalarVarReader where the calculated weight is already stored in
            double W = Weight.first->read();
            if (!IsFinite(W)) {
                W = m_PseudoReader->read();
                XAMPP::Warning("ConditionalWeight::NewEvent()",
                               "Weight " + (UseWeighter ? Weight.second->name() : Weight.first->name()) + " does not return a number");
            } else
                W = W * m_PseudoReader->read();
            m_PseudoReader->SetValue(W);
        }
    }
    ITreeVarReader* ConditionalWeight::ConditionalVariations::GetTreeVarReader() { return m_PseudoReader; }
    //##############################################################################################################
    //                                        FixedExternalWeight
    //##############################################################################################################
    std::string FixedExternalWeight::name() const { return m_name; }
    bool FixedExternalWeight::init(TTree* t) { return m_reader && m_reader->init(t); }
    double FixedExternalWeight::read() { return m_reader->read(); }
    ITreeVarReader* FixedExternalWeight::GetTreeVarReader() { return m_reader; }
    ITreeVarReader* FixedExternalWeight::GetSystematicReader(const std::string&) { return GetTreeVarReader(); }
    std::vector<std::string> FixedExternalWeight::FindWeightVariations(TTree*) { return GetWeightVariations(); }
    IWeightElement::SystStatus FixedExternalWeight::ApplySystematic(const std::string&) { return IWeightElement::SystStatus::NotAffected; }
    void FixedExternalWeight::ResetSystematic() {}
    std::vector<std::string> FixedExternalWeight::GetWeightVariations() { return std::vector<std::string>(); }
    FixedExternalWeight::~FixedExternalWeight() {}
    FixedExternalWeight::FixedExternalWeight(const std::string& WeightName, const std::string& ReaderName, double FixedValue) :
        m_name(WeightName),
        m_reader(PseudoScalarVarReader::GetReader(ReaderName, FixedValue)) {
        Weight::getWeighter()->RegisterWeightElement(this);
    }
    IWeightElement* FixedExternalWeight::GetWeighter(const std::string& Name, const std::string& ReaderName, double FixedValue) {
        if (!Weight::getWeighter()->GetWeightElement(Name)) return new FixedExternalWeight(Name, ReaderName, FixedValue);
        return Weight::getWeighter()->GetWeightElement(Name);
    }
}  // namespace XAMPP
