#include <XAMPPplotting/LumiCalculator.h>
#include <XAMPPplotting/ReconstructedParticleReaders.h>
#include <XAMPPplotting/UtilityReader.h>
#include <XAMPPplotting/Weight.h>

#include <PATInterfaces/SystematicRegistry.h>
#include <PATInterfaces/SystematicsUtil.h>
#include <PathResolver/PathResolver.h>
#include <PileupReweighting/PileupReweightingTool.h>

namespace XAMPP {
    //#############################
    //          Period numbers
    //#############################
    const unsigned int period_mc16a = 284500;
    const unsigned int period_mc16d = 300000;
    const unsigned int period_mc16e = 310000;

    //######################################################################################################
    //                                  IWeightElement
    //######################################################################################################
    bool IWeightElement::m_FillSystSimul = false;
    void IWeightElement::FillSystSimul(bool B) { m_FillSystSimul = B; }
    bool IWeightElement::SimultaneousSyst() { return m_FillSystSimul; }
    //######################################################################################################
    //                                  WeightSystematic
    //######################################################################################################

    WeightSystematic::WeightSystematic(const std::string& syst) :
        m_syst_name(syst),
        m_weighter(Weight::getWeighter()),
        m_service(EventService::getService()) {}
    std::string WeightSystematic::syst_name() const { return m_syst_name; }
    std::string WeightSystematic::tree_name() const { return m_service->getNominal()->tree_name(); }
    bool WeightSystematic::is_nominal() const { return false; }
    bool WeightSystematic::apply_syst() { return m_weighter->ApplySystematic(syst_name()); }
    void WeightSystematic::reset() { m_weighter->ResetSystematic(); }
    //######################################################################################################
    //                                  Weight
    //######################################################################################################
    // Initialization of the singleton pointer
    Weight* Weight::m_DB = nullptr;
    Weight::Weight(bool Data) :
        m_isData(Data),
        m_ApplyXS(true),
        m_UpdateXsec(false),
        m_UseSUSYstates(true),
        m_CreateInfo(false),
        m_ApplyPRW(true),
        m_weight(1.),
        m_weight2(1.),
        m_MetaData(nullptr),
        m_RegisteredWeighters(),
        m_MCchannel(nullptr),
        m_FinalState(nullptr),
        m_WeightReader(),
        m_UseWeights(),
        m_UseDataWeights(),
        m_excludedSystematics(),
        m_lastEvUpdate(0),
        m_eventInfo(nullptr),
        m_eventInfoAux(nullptr),
        m_eventInfoCont(nullptr),
        m_mu_actual_reader(nullptr),
        m_mu_reader(nullptr),
        m_RunNumber(nullptr),
        m_LumiBlock(nullptr),
        m_evNumber(nullptr),
        m_GenWeight(nullptr),
        m_EventNumber(0),
        m_LHENorm(LHENormalization::AcceptanceOnly) {
        m_MetaData = XAMPP::NormalizationDataBase::getDataBase();
    }

    const std::vector<std::string>& Weight::WeightNames() const { return m_UseWeights; }
    const std::vector<std::string>& Weight::DataWeightNames() const { return m_UseDataWeights; }
    int Weight::LHEnormMode() const { return m_LHENorm; }
    void Weight::setLHEnormMode(LHENormalization N) {
        if (N == Weight::AcceptanceOnly) {
            Info("Weight::setLHEnormMode()",
                 "Each LHE varitaion is going to be weighted to it's own SumW. Recommended for acceptance effects");
        } else {
            Info("Weight::setLHEnomMode()",
                 "The LHE variations are going to be normalized to the nominal SumW. That mode includes the uncertainties on the "
                 "cross-section as well");
        }
        m_LHENorm = N;
    }
    bool Weight::applyPRW() const { return m_ApplyPRW; }
    bool Weight::normToProdProcess() const { return m_UseSUSYstates; }
    bool Weight::applyXSection() const { return m_ApplyXS; }
    std::vector<std::string> Weight::excluded_weights() const {
        const static std::vector<std::string> PileUpWeights{"muWeight", "GenWeight", "NormWeight", "prwWeight"};
        return PileUpWeights;
    }
    void Weight::UseWeights(const std::vector<std::string>& W_Names) {
        // These two weight branches are handled by the XAMPPplotting code internally
        // Users must not parse them in their config files.
        CopyVector(W_Names, m_UseWeights, true);
        EraseFromVector<std::string>(m_UseWeights, [this](const std::string& w) { return IsElementInList(excluded_weights(), w); });
    }
    void Weight::UseDataWeights(const std::vector<std::string>& W_Names) {
        CopyVector(W_Names, m_UseDataWeights, true);
        EraseFromVector<std::string>(m_UseDataWeights, [this](const std::string& w) { return IsElementInList(excluded_weights(), w); });
    }
    int Weight::SUSYFinalState() const {
        if (isData()) return -1;
        return m_FinalState->read();
    }
    int Weight::processID() const {
        if (m_GenWeight == nullptr) return 0;
        return m_GenWeight->processID();
    }
    void Weight::SetupEventInfo(bool B) {
        m_CreateInfo = B;
        if (!m_eventInfoCont) {
            m_mu_actual_reader = ScalarVarReader<float>::GetReader("actualInteractionsPerCrossing");
            m_mu_reader = ScalarVarReader<float>::GetReader("averageInteractionsPerCrossing");
            m_LumiBlock = ScalarVarReader<unsigned int>::GetReader("lumiBlock");
            m_RunNumber = ScalarVarReader<unsigned int>::GetReader("runNumber");
            m_evNumber = ScalarVarReader<unsigned long long>::GetReader("eventNumber");
            m_eventInfoCont = std::make_unique<xAOD::EventInfoContainer>();
            m_eventInfoAux = std::make_unique<xAOD::EventInfoAuxContainer>();
            m_eventInfoCont->setStore(m_eventInfoAux.get());
            m_eventInfo = new xAOD::EventInfo();
            m_eventInfoCont->push_back(m_eventInfo);
            uint32_t bitmask = !isData() ? xAOD::EventInfo::IS_SIMULATION : 0;
            m_eventInfo->setEventTypeBitmask(bitmask);
        }
    }
    void Weight::NewEvent() {
        ++m_EventNumber;
        m_weight = 1.;
        for (const auto& w : m_WeightReader) {
            m_weight *= w->read();
            if (m_weight == 0.) break;
        }
        m_weight2 = m_weight * m_weight;
    }
    void Weight::UpdateEventInfo() {
        if (!m_eventInfo || m_lastEvUpdate == m_EventNumber) return;
        m_lastEvUpdate = m_EventNumber;
        m_eventInfo->setLumiBlock(m_LumiBlock->read());
        m_eventInfo->setRunNumber(m_RunNumber->read());
        m_eventInfo->setEventNumber(m_evNumber->read());
        m_eventInfo->setActualInteractionsPerCrossing(m_mu_actual_reader->read());
        m_eventInfo->setAverageInteractionsPerCrossing(m_mu_reader->read());
        if (!isData()) m_eventInfo->setMCChannelNumber(mcChannelNumber());
    }
    Weight::~Weight() {
        delete LumiCalculator::getCalculator();
        m_DB = nullptr;
    }
    IWeightElement* Weight::GetWeightElement(const std::string& Element) const {
        std::map<std::string, std::shared_ptr<IWeightElement>>::const_iterator Itr = m_RegisteredWeighters.find(Element);
        if (Itr != m_RegisteredWeighters.end()) return Itr->second.get();
        return nullptr;
    }

    void Weight::LoadWeightElements(std::vector<std::string>& Weights) {
        for (const auto& W : Weights) {
            IWeightElement* Weight = GetWeightElement(W);
            if (!Weight) Weight = WeightElementFromTree::GetWeighter(W);
            AppendWeight(Weight);
        }
        Weights.clear();
    }
    bool Weight::InitWeights() {
        Info("Weight::InitWeights()", "Initialize...");
        m_RunNumber = ScalarVarReader<unsigned int>::GetReader("runNumber");
        if (!isData()) {
            m_GenWeight = NormalizationWeight::getElement();
            AppendWeight(m_GenWeight);
            m_MCchannel = ScalarVarReader<int>::GetReader("mcChannelNumber");
            m_FinalState = ScalarVarReader<int>::GetReader("SUSYFinalState");
            // PileUpPRW
            if (m_ApplyPRW) { AppendWeight(PileUpWeightElement::getElement()); }
            if (m_UpdateXsec) {
                Info("Weight::InitWeights()", "The Cross Sections are read out from the online xSection database.");
                XAMPP::PrintFooter();
                m_MetaData->PromptMetaDataTree();
                XAMPP::PrintFooter();
            }
        }
        if (isData())
            LoadWeightElements(m_UseDataWeights);
        else
            LoadWeightElements(m_UseWeights);
        m_EventNumber = 0;
        for (const auto& Weight : m_WeightReader) {
            XAMPP::Info("Weight::InitWeights()",
                        "The weight " + Weight->name() + " will be applied using " + Weight->GetTreeVarReader()->name());
        }
        /// Check whether all cross-sections are well defined if
        /// they're going to be applied
        if (!isData() && applyXSection()) {
            for (const auto& dsid : m_MetaData->GetListOfMCSamples()) {
                if (dsid == 0) continue;
                std::vector<unsigned int> pids = m_MetaData->GetListOfProcesses(dsid);
                EraseFromVector<unsigned int>(pids,
                                              [this](const unsigned int& pid) { return pid >= 1000 || (!normToProdProcess() && pid > 0); });
                if (pids.size() == 1 || !normToProdProcess()) {
                    if (m_MetaData->GetxSectTimes(dsid, 0) < 0) {
                        Error("Weight::InitWeights()",
                              Form("Found negative cross-section for sample %u. If you're fairly certain that there should be a "
                                   "cross-section available *AND* you're using the central PMG file,",
                                   dsid));
                        Error("Weight::InitWeights()",
                              "then write an E-Mail to PMG via atlas-phys-pmgATSPAMNOTcern.ch or open a ticket on "
                              "'https://its.cern.ch/jira/projects/CENTRPAGE'");
                        Error("Weight::InitWeights()", "telling them that the sample is dead and you're willing to organize its funeral");
                        return false;
                    }
                } else {
                    for (const auto& pid : pids) {
                        // SUSY cross-sections usually have
                        if (pid == 0) continue;
                        if (m_MetaData->GetxSectTimes(dsid, pid) < 0) {
                            Error("Weight::InitWeights()",
                                  Form("Found negative cross-section for sample %u with process-id %u.", dsid, pid));
                            Error("Weight::InitWeights()", "If you're using the dev/SUSYTools area to load the cross-sections from then");
                            Error(
                                "Weight::InitWeights()",
                                "prepare a signal cross-section file with all missing processes and send it to the SUSY background forum.");
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }
    void Weight::AppendWeight(IWeightElement* Weight) {
        if (!IsWeightAlreadyUsed(Weight)) m_WeightReader.push_back(Weight);
    }
    bool Weight::init(TTree* tree) {
        if (!isData() && (!m_MCchannel->init(tree) || !m_FinalState->init(tree))) return false;
        for (auto& Weight : m_WeightReader) {
            if (!Weight->init(tree)) {
                Error("Weight::init()", Form("Failed to setup %s", Weight->name().c_str()));
                return false;
            }
        }
        if (!m_RunNumber->init(tree)) return false;
        if (m_CreateInfo &&
            (!m_LumiBlock->init(tree) || !m_evNumber->init(tree) || !m_mu_actual_reader->init(tree) || !m_mu_reader->init(tree)))
            return false;

        return true;
    }
    bool Weight::IsWeightAlreadyUsed(IWeightElement* R) const {
        if (R == nullptr) return true;
        if (IsElementInList(m_WeightReader, R)) return true;
        return false;
    }
    std::vector<std::string> Weight::FindWeightVariations(TTree* T) {
        std::vector<std::string> WeightVariations;
        for (auto& W : m_WeightReader) {
            std::vector<std::string> Var = W->FindWeightVariations(T);
            for (auto& V : Var) {
                if (IsElementInList(WeightVariations, V))
                    XAMPP::Warning("Weight::FindWeightVariations()", "Variation " + V + " is already in List");
                else
                    WeightVariations.push_back(V);
            }
        }
        return WeightVariations;
    }
    bool Weight::ApplySystematic(const std::string& Syst) {
        bool Applied = false;
        for (const auto& W : m_WeightReader) {
            IWeightElement::SystStatus Status = W->ApplySystematic(Syst);
            if (Status == IWeightElement::SystStatus::Affected)
                Applied = true;
            else if (Status == IWeightElement::SystStatus::SystError)
                return false;
        }
        if (!Applied) XAMPP::Error("Weight::ApplySystematic()", "Could not find any weight affected by " + Syst);
        return Applied;
    }
    void Weight::ResetSystematic() {
        for (const auto& W : m_WeightReader) W->ResetSystematic();
    }
    void Weight::SetLumi(double Lumi) { m_MetaData->SetLumi(Lumi); }
    bool Weight::RegisterWeightElement(IWeightElement* W) { return RegisterWeightElement(std::shared_ptr<IWeightElement>(W)); }
    bool Weight::RegisterWeightElement(std::shared_ptr<IWeightElement> W) {
        if (!W) {
            XAMPP::Error("Weight::RegisterWeightElement()", "No weight element given");
            return false;
        }
        if (GetWeightElement(W->name()) != nullptr) {
            XAMPP::Error("Weight::RegisterWeightElement()", "The element " + W->name() + " is already registered");
            return false;
        }
        m_RegisteredWeighters.insert(std::pair<std::string, std::shared_ptr<IWeightElement>>(W->name(), W));
        XAMPP::Info("Weight::RegisterWeightElement()", "New weight element " + W->name() + " appended.");
        return true;
    }
    const std::vector<IWeightElement*>& Weight::GetUsedWeighters() const { return m_WeightReader; }
    void Weight::ApplyXSection(bool B) {
        Info("Weight::ApplyXSection()", "Will " + std::string(B ? "enable" : "disable") + " the application of the cross-section");
        m_ApplyXS = B;
    }
    void Weight::ApplyPRW(bool B) {
        Info("Weight::ApplyPRW()", "Will " + std::string(B ? "enable" : "disable") + " the application of the pile up weight");
        m_ApplyPRW = B;
    }
    void Weight::NormProcesses(bool B) {
        Info("Weight::NormProcesses()", "Monte Carlo will " + std::string((B ? "" : "not ")) + "be normalized to each SUSY process ID");
        m_UseSUSYstates = B;
    }
    bool Weight::isData() const { return m_isData; }

    Weight* Weight::getWeighter(bool isData) {
        if (!m_DB) m_DB = new Weight(isData);
        return m_DB;
    }

    xAOD::EventInfo* Weight::GetInfo() const {
        if (!m_eventInfo) XAMPP::Warning("Weight::GetInfo()", "No info hast been defined. Please call SetupEventInfo()");
        return m_eventInfo;
    }
    unsigned int Weight::eventNumber() const { return m_EventNumber; }
    unsigned int Weight::runNumber() const { return m_RunNumber->read(); }
    int Weight::mcChannelNumber() const {
        if (isData()) return -1;
        return m_MCchannel->read();
    }
    void Weight::excludeSystematic(const std::string& Syst) {
        if (!IsElementInList(m_excludedSystematics, Syst)) m_excludedSystematics.push_back(Syst);
    }
    void Weight::excludeSystematic(const std::vector<std::string>& Syst) { CopyVector(Syst, m_excludedSystematics, false); }
    const std::vector<std::string> Weight::excludedSystematics() const { return m_excludedSystematics; }

    //######################################################################################################
    //                                  WeightElementFromTree
    //######################################################################################################
    bool WeightElementFromTree::init(TTree* tree) {
        if (!m_Registered) {
            XAMPP::Error("WeightElementsFromTree::init()", "Something went wrong during initialization of " + name());
            return false;
        }
        if (!m_Weighter) {
            XAMPP::Error("WeightElementsFromTree::init()", "Error no weighter is given to " + name());
            return false;
        } else if (!m_Weighter->init(tree)) {
            return false;
        }
        if (SimultaneousSyst()) {
            // Ensure that nominal is loaded as well
            if (!LoadReader()->init(tree)) return false;
            for (auto& Syst : m_Systematics) {
                if (!LoadReader(Syst)->init(tree)) return false;
            }
        }
        m_init = true;
        return true;
    }
    double WeightElementFromTree::read() {
        if (!m_init) { return 1.; }
        double W = m_Weighter->read();
        if (!IsFinite(W)) {
            XAMPP::Warning("WeightElementFromTree::read()",
                           Form("The weight %s is not a number in event %llu. Return 1.", name().c_str(), m_eventService->currentEvent()));
            return 1.;
        }
        return W;
    }
    std::vector<std::string> WeightElementFromTree::FindWeightVariations(TTree* Tree) {
        if (!Tree)
            XAMPP::Error("Weight::FindWeightVariations()", "No Tree was given.");
        else {
            for (const auto& obj : *Tree->GetListOfBranches()) {
                std::string N = obj->GetName();
                size_t EndOfName = N.find(name()) + name().size();
                // Reject the weight branch itself
                // Branches where the name is not part of the weight
                // Similar branches like EleWeightReco for EleWeight
                if (N == name() || N.find(name()) != 0 || (N.substr(EndOfName, N.size()).find("_") != 0)) continue;
                std::string Syst = N.substr(N.find(name()) + name().size() + 1, N.size());
                // Exclude already existing systematics and ones which have been exclued externally
                if (XAMPP::IsElementInList(m_Systematics, Syst) ||
                    XAMPP::IsElementInList(Weight::getWeighter()->excludedSystematics(), Syst))
                    continue;
                XAMPP::Info("WeightElementFromTree::FindWeightVariations()", "Found new systematic " + Syst + " affecting " + name());
                m_Systematics.push_back(Syst);
            }
            if (Tree->GetListOfFriends()) {
                for (const auto& tree_friend : *Tree->GetListOfFriends()) {
                    FindWeightVariations(dynamic_cast<TFriendElement*>(tree_friend)->GetTree());
                }
            }
        }
        return m_Systematics;
    }
    std::vector<std::string> WeightElementFromTree::GetWeightVariations() { return m_Systematics; }
    IWeightElement::SystStatus WeightElementFromTree::ApplySystematic(const std::string& variation) {
        for (const auto Syst : m_Systematics) {
            if (variation == Syst) {
                XAMPP::Info("WeightElementsFromTree::ApplySystematic()", "Variation " + variation + " affects the weight " + name() + ".");
                m_Weighter = LoadReader(variation);
                return IWeightElement::SystStatus::Affected;
            }
        }
        return IWeightElement::SystStatus::NotAffected;
    }
    void WeightElementFromTree::ResetSystematic() { m_Weighter = LoadReader(); }
    std::string WeightElementFromTree::name() const { return m_Name; }
    ITreeVarReader* WeightElementFromTree::LoadReader(const std::string& syst) {
        return ScalarVarReader<double>::GetReader(name() + (syst.empty() ? "" : "_" + syst));
    }
    WeightElementFromTree::WeightElementFromTree() :
        m_Name(),
        m_Weighter(nullptr),
        m_Systematics(),
        m_init(false),
        m_Registered(false),
        m_eventService(EventService::getService()) {}
    WeightElementFromTree::WeightElementFromTree(const std::string& Weight) : WeightElementFromTree() {
        m_Name = Weight;
        m_Weighter = LoadReader();
        m_Registered = Weight::getWeighter()->RegisterWeightElement(this);
    }
    WeightElementFromTree::~WeightElementFromTree() { m_Systematics.clear(); }
    WeightElementFromTree* WeightElementFromTree::GetWeighter(const std::string& Name) {
        if (!Weight::getWeighter()->GetWeightElement(Name)) new WeightElementFromTree(Name);
        return dynamic_cast<WeightElementFromTree*>(Weight::getWeighter()->GetWeightElement(Name));
    }
    ITreeVarReader* WeightElementFromTree::GetTreeVarReader() { return m_Weighter; }
    ITreeVarReader* WeightElementFromTree::GetSystematicReader(const std::string& Syst) {
        if (IsElementInList(m_Systematics, Syst)) { return LoadReader(Syst); }
        return LoadReader();
    }
    //######################################################################################################
    //                                  FloatWeightFromTree
    //######################################################################################################
    FloatWeightFromTree::FloatWeightFromTree(const std::string& Weight) : WeightElementFromTree() {
        m_Name = Weight;
        m_Weighter = LoadReader();
        m_Registered = Weight::getWeighter()->RegisterWeightElement(this);
    }
    ITreeVarReader* FloatWeightFromTree::LoadReader(const std::string& syst) {
        return ScalarVarReader<float>::GetReader(name() + (syst.empty() ? "" : "_" + syst));
    }
    FloatWeightFromTree* FloatWeightFromTree::GetWeighter(const std::string& Name) {
        if (!Weight::getWeighter()->GetWeightElement(Name)) return new FloatWeightFromTree(Name);
        return dynamic_cast<FloatWeightFromTree*>(Weight::getWeighter()->GetWeightElement(Name));
    }
    FloatWeightFromTree::~FloatWeightFromTree() {}
    //######################################################################################################
    //                                  PileUpWeightElement
    //######################################################################################################
    PileUpWeightElement* PileUpWeightElement::getElement() {
        if (!Weight::getWeighter()->GetWeightElement("prwWeight")) return new PileUpWeightElement();
        return dynamic_cast<PileUpWeightElement*>(Weight::getWeighter()->GetWeightElement("prwWeight"));
    }
    std::string PileUpWeightElement::name() const { return "prwWeight"; }
    PileUpWeightElement::PileUpWeightElement() :
        m_Weighter(Weight::getWeighter()),
        m_MetaData(NormalizationDataBase::getDataBase()),
        m_TreePRW(WeightElementFromTree::GetWeighter("muWeight")),
        m_variationReaders(),
        m_current_syst(m_variationReaders.end()),
        m_periodMode(PileUpWeightElement::LumiPeriod::All),
        m_current_mc(),
        m_allowed_periods() {
        m_Weighter->RegisterWeightElement(this);
    }
    void PileUpWeightElement::reWeightToPeriod(PileUpWeightElement::LumiPeriod P) {
        m_periodMode = P;
        m_allowed_periods.clear();
    }
    bool PileUpWeightElement::init(TTree* tree) {
        if (!m_TreePRW || !m_TreePRW->init(tree)) {
            XAMPP::Error("PileUpWeightElement::init()", "Could not read the pile-up weight from the tree");
            return false;
        }
        if (m_variationReaders.empty()) {
            m_variationReaders.push_back(
                FinalPRWWeight(dynamic_cast<PseudoScalarVarReader*>(GetSystematicReader("")), m_TreePRW->GetSystematicReader("")));
            ResetSystematic();
        }
        if (m_periodMode != LumiPeriod::All) {
            Info("PileUpWeightElement::init()", "The user enabled to dismount the mc16a+mc16d+mc16e super MC.");
            Info("PileUpWeightElement::init()", "If you're histograms are empty from the begining please rethink:");
            Info("PileUpWeightElement::init()", "  1) What is displayed two lines above?");
            Info("PileUpWeightElement::init()", "  2) To which mc campaign do the input files belong?");
            Info("PileUpWeightElement::init()", "  3) If the config is inclusive. Do I run only over partial events?");
            Info("PileUpWeightElement::init()", "  4) Do I run only over partial events?");
            Info("PileUpWeightElement::init()", "  5) What is the order of the input files?");
        }
        // May be we're at the first tree and nothing has been initialized yet
        if (m_allowed_periods.empty()) {
            int p_mode = m_periodMode;
            if ((p_mode & LumiPeriod::data15_16) == LumiPeriod::data15_16) {
                Info("PileUpWeightElement::init()", "Add data 15-16 to the allowed periods");
                m_allowed_periods.push_back(period_mc16a);
            }
            if ((p_mode & LumiPeriod::data17) == LumiPeriod::data17) {
                Info("PileUpWeightElement::init()", "Add data 17 to the allowed periods");
                m_allowed_periods.push_back(period_mc16d);
            }
            if ((p_mode & LumiPeriod::data18) == LumiPeriod::data18) {
                Info("PileUpWeightElement::init()", "Add data 18 to the allowed periods");
                m_allowed_periods.push_back(period_mc16e);
            }
        }
        // Okay here we should definetly see allowed periods
        if (m_allowed_periods.empty()) {
            Error("PileUpWeightElement::init()", "Could not find out to which part of the super mc you want to reweight to. Bail out");
            return false;
        }
        return true;
    }
    double PileUpWeightElement::read() {
        if (!m_Weighter->applyPRW()) return 1;

        double W = m_TreePRW->read() * getPeriodWeight();
        (*m_current_syst).CombinedReader->SetValue(W);
        if (SimultaneousSyst()) {
            double p_w = getPeriodWeight();
            for (const auto& final_w : m_variationReaders) { final_w.CombinedReader->SetValue(final_w.prwReader->read() * p_w); }
        }
        return W;
    }
    double PileUpWeightElement::getPeriodWeight() const {
        unsigned int mcChannelNumber = m_Weighter->mcChannelNumber();

        if (!m_current_mc || m_current_mc->DSID() != mcChannelNumber) { m_current_mc = m_MetaData->getMCperiodHandler(mcChannelNumber); }
        // Total luminosity can be calculated w.r.t to the
        double tot_lumi = m_current_mc->prwTotalLuminosity(0);
        if (m_periodMode != LumiPeriod::All && !IsElementInList(m_allowed_periods, m_Weighter->runNumber()))
            return 0;
        else if (tot_lumi > 0) {
            std::shared_ptr<MetaDataStore> current_period = m_current_mc->getStore(m_Weighter->processID(), m_Weighter->runNumber());
            double rew_SumW = m_MetaData->GetSumW(mcChannelNumber, m_Weighter->processID()) / current_period->SumW();
            if (m_periodMode != LumiPeriod::All) {
                tot_lumi = 0;
                for (const auto& period : m_current_mc->getMCcampaigns()) {
                    if (!IsElementInList(m_allowed_periods, period)) continue;
                    tot_lumi += m_current_mc->getStore(m_Weighter->processID(), period)->prwLuminosity();
                }
            }
            return rew_SumW * current_period->prwLuminosity() / tot_lumi;
        }
        // Old way of doing the reweighint. Prone to errors
        else if (m_periodMode != LumiPeriod::All) {
            Warning("PileUpWeight()",
                    "You are using XAMPPtrees not containing any information about the prwLuminosty and you'd like to rewight the mc "
                    "campaigns.");
            Warning("PileUpWeight()", "The prw luminosity is crucuial to do the job. Please consider to produce new XAMPP trees");
        }
        return 1.;
    }
    std::vector<std::string> PileUpWeightElement::FindWeightVariations(TTree* Tree) {
        std::vector<std::string> V;
        if (!m_Weighter->applyPRW()) return V;

        V = m_TreePRW->FindWeightVariations(Tree);
        loadSystematics(V);
        return V;
    }
    IWeightElement::SystStatus PileUpWeightElement::ApplySystematic(const std::string& variation) {
        IWeightElement::SystStatus Status =
            (!m_Weighter->applyPRW() ? IWeightElement::SystStatus::NotAffected : m_TreePRW->ApplySystematic(variation));
        if (Status == IWeightElement::SystStatus::Affected) {
            ITreeVarReader* syst_reader = m_TreePRW->GetSystematicReader(variation);
            for (m_current_syst = m_variationReaders.begin(); m_current_syst != m_variationReaders.end(); ++m_current_syst) {
                if (m_current_syst->prwReader == syst_reader) { break; }
            }
        }
        return Status;
    }
    void PileUpWeightElement::ResetSystematic() {
        m_TreePRW->ResetSystematic();
        ITreeVarReader* syst_reader = m_TreePRW->GetSystematicReader("");
        for (m_current_syst = m_variationReaders.begin(); m_current_syst != m_variationReaders.end(); ++m_current_syst) {
            if (m_current_syst->prwReader == syst_reader) break;
        }
        // By construction the nominal reader should be at the end of the vector
    }
    ITreeVarReader* PileUpWeightElement::GetTreeVarReader() {
        if (m_current_syst == m_variationReaders.end()) {
            XAMPP::Warning("PileUpWeightElement::GetTreeVarReader()", "Could not find the TreeVarReader");
            return GetSystematicReader("");
        }
        return m_current_syst->CombinedReader;
    }
    ITreeVarReader* PileUpWeightElement::GetSystematicReader(const std::string& Syst) {
        if (!Syst.empty() && !IsElementInList(GetWeightVariations(), Syst)) return GetSystematicReader("");
        return PseudoScalarVarReader::GetReader(name() + (Syst.empty() ? "" : "_") + Syst);
    }
    std::vector<std::string> PileUpWeightElement::GetWeightVariations() {
        std::vector<std::string> Var = m_TreePRW->GetWeightVariations();
        loadSystematics(Var);
        return Var;
    }
    void PileUpWeightElement::loadSystematics(const std::vector<std::string>& Var) {
        if (m_variationReaders.empty()) {
            std::vector<std::string> vars_nom{""};
            CopyVector(Var, vars_nom);
            for (const auto& syst : vars_nom) {
                m_variationReaders.push_back(
                    FinalPRWWeight(dynamic_cast<PseudoScalarVarReader*>(GetSystematicReader(syst)), m_TreePRW->GetSystematicReader(syst)));
            }
            ResetSystematic();
        }
    }

    //###########################################################################
    //                  NormalizationVariation
    //###########################################################################
    NormalizationVariation::NormalizationVariation(const std::string& norm) :
        m_LHE(0),
        m_PseudoReader(PseudoScalarVarReader::GetReader(std::string("NormWeight") + (norm.empty() ? "" : "_") + norm)),
        m_eventWeight(WeightElementFromTree::GetWeighter("GenWeight")->GetSystematicReader(norm)),
        m_DSID(-1),
        m_FinalState(-1),
        m_MetaData(NormalizationDataBase::getDataBase()),
        m_weighter(Weight::getWeighter()),
        m_known_FS(),
        m_NormCache(0.),
        m_xSection(nullptr),
        m_xSectUncert(nullptr) {
        if (!norm.empty()) m_LHE = atoi(norm.substr(norm.find("_") + 1).c_str());
        m_xSection = PseudoScalarVarReader::GetReader("xSection");
        m_xSectUncert = PseudoScalarVarReader::GetReader("RelxSectUncert");
    }
    int NormalizationVariation::LHEvariation() const { return m_LHE; }
    int NormalizationVariation::processID() const {
        if (!m_weighter->normToProdProcess() || m_weighter->SUSYFinalState() == 0)
            return m_weighter->LHEnormMode() == Weight::LHENormalization::AcceptanceOnly ? LHEvariation() : 0;
        else
            return (m_weighter->LHEnormMode() == Weight::LHENormalization::AcceptanceOnly) * LHEvariation() * 10000 +
                   m_weighter->SUSYFinalState();
    }
    double NormalizationVariation::Read() {
        if (m_DSID != m_weighter->mcChannelNumber() || m_FinalState != processID()) {
            if (m_DSID != m_weighter->mcChannelNumber()) m_known_FS.clear();
            m_DSID = m_weighter->mcChannelNumber();
            m_FinalState = processID();
            m_NormCache = m_MetaData->getNormalization(m_weighter->mcChannelNumber(), processID());
            bool PromptMessage =
                (!IWeightElement::SimultaneousSyst() || !LHEvariation()) && (!m_FinalState || !IsElementInList(m_known_FS, m_FinalState));
            if (PromptMessage) {
                if (!m_FinalState)
                    Info("Weight::GetNormalization()", Form("Found new DSID %u using LHE variation %u. Update normalization to %.12f",
                                                            m_DSID, LHEvariation(), m_NormCache));
                else
                    Info("Weight::GetNormalization()",
                         Form("Found new DSID %u or FinalState %u using LHE variation %u. Update normalization to %.12f", m_DSID,
                              m_FinalState, LHEvariation(), m_NormCache));
            }
            if (m_weighter->applyXSection()) {
                double XS = m_MetaData->GetxSectTimes(m_DSID, m_FinalState);
                m_NormCache *= XS;
                if (!IWeightElement::SimultaneousSyst() || !LHEvariation()) {
                    m_xSectUncert->SetValue(m_MetaData->GetRelUncertainty(m_DSID, m_FinalState));
                    m_xSection->SetValue(XS);
                }
                if (PromptMessage) Info("Weight::GetNormalization()", "Found xSection of " + std::to_string(XS) + ". ");
            }
            if (!IsElementInList(m_known_FS, m_FinalState)) m_known_FS.push_back(m_FinalState);
        }
        double W = m_NormCache * m_eventWeight->read();
        m_PseudoReader->SetValue(W);
        return W;
    }
    ITreeVarReader* NormalizationVariation::GetReader() const { return m_PseudoReader; }
    NormalizationVariation::~NormalizationVariation() {}
    //###########################################################################
    //                  NormalizationWeight
    //###########################################################################
    std::shared_ptr<NormalizationVariation> NormalizationWeight::newVariation(const std::string& syst) {
        return std::make_shared<NormalizationVariation>(syst);
    }
    bool NormalizationWeight::init(TTree* tree) {
        checkNorm();
        return m_eventWeight->init(tree);
    }
    std::vector<std::string> NormalizationWeight::FindWeightVariations(TTree* tree) {
        std::vector<std::string> systematics = m_eventWeight->FindWeightVariations(tree);
        checkNorm();
        for (auto& syst : systematics) {
            if (!getVariation(syst)) m_LHE.push_back(newVariation(syst));
        }
        return systematics;
    }
    IWeightElement::SystStatus NormalizationWeight::ApplySystematic(const std::string& variation) {
        IWeightElement::SystStatus status = m_eventWeight->ApplySystematic(variation);
        if (status == IWeightElement::SystStatus::Affected) {
            XAMPP::Info("Weight::ApplySystematic()", "The variation " + variation + " affects the GenWeight.");
            m_Norm = getVariation(variation);
        } else
            m_Norm = getVariation(0);
        return status;
    }
    double NormalizationWeight::read() {
        if (m_weighter->isData()) return 1.;
        if (IWeightElement::SimultaneousSyst()) {
            for (auto& LHE : m_LHE) LHE->Read();
        }
        return m_Norm->Read();
    }
    void NormalizationWeight::ResetSystematic() {
        m_eventWeight->ResetSystematic();
        m_Norm = getVariation(0);
    }
    std::string NormalizationWeight::name() const { return "NormWeight"; }
    NormalizationWeight* NormalizationWeight::getElement() {
        if (!Weight::getWeighter()->GetWeightElement("NormWeight")) new NormalizationWeight();
        return dynamic_cast<NormalizationWeight*>(Weight::getWeighter()->GetWeightElement("NormWeight"));
    }
    ITreeVarReader* NormalizationWeight::GetTreeVarReader() {
        checkNorm();
        return m_Norm->GetReader();
    }
    void NormalizationWeight::checkNorm() {
        if (m_LHE.empty()) {
            m_LHE.push_back(newVariation(std::string()));
            m_Norm = getVariation(0);
        }
    }
    ITreeVarReader* NormalizationWeight::GetSystematicReader(const std::string& Syst) { return getVariation(Syst)->GetReader(); }
    std::vector<std::string> NormalizationWeight::GetWeightVariations() { return m_eventWeight->GetWeightVariations(); }
    NormalizationWeight::NormalizationWeight() :
        m_eventWeight(WeightElementFromTree::GetWeighter("GenWeight")),
        m_weighter(Weight::getWeighter()),
        m_LHE(),
        m_Norm(nullptr) {
        m_weighter->RegisterWeightElement(this);
    }
    std::shared_ptr<NormalizationVariation> NormalizationWeight::getVariation(const std::string& var) const {
        if (m_eventWeight->ApplySystematic(var) == IWeightElement::SystStatus::Affected) {
            int LHE = atoi(var.substr(var.find("_") + 1).c_str());
            return getVariation(LHE);
        }
        return getVariation(0);
    }
    std::shared_ptr<NormalizationVariation> NormalizationWeight::getVariation(int L) const {
        for (auto& LHE : m_LHE) {
            if (LHE->LHEvariation() == L) return LHE;
        }
        return std::shared_ptr<NormalizationVariation>();
    }
    int NormalizationWeight::processID() const { return m_Norm->processID(); }

}  // namespace XAMPP
