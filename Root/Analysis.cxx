#include <TSystem.h>
#include <XAMPPplotting/Analysis.h>
#include <XAMPPplotting/Cuts.h>
#include <XAMPPplotting/EventService.h>
#include <XAMPPplotting/Histo.h>
#include <XAMPPplotting/HistoTemplates.h>
#include <XAMPPplotting/TreeVarReader.h>
#include <XAMPPplotting/Weight.h>
#include <iomanip>  //for std::setprecision
#include <iostream>
#include <sstream>
namespace XAMPP {

    //########################################################
    //                  TreeSystematic
    //########################################################
    std::string TreeSystematic::syst_name() const { return m_tree_name; }
    std::string TreeSystematic::tree_name() const { return m_tree_name; }
    bool TreeSystematic::is_nominal() const { return m_is_nominal; }
    bool TreeSystematic::apply_syst() { return true; }
    void TreeSystematic::reset() {}
    TreeSystematic::TreeSystematic(const std::string& tree_name, bool is_nominal) : m_tree_name(tree_name), m_is_nominal(is_nominal) {}
    //########################################################
    //                  Analysis
    //########################################################
    Analysis::Analysis() :
        m_eventService(EventService::getService()),
        m_batchMode(false),
        m_DoSyst(true),
        m_DoSyst_W(true),
        m_DoNominal(true),
        m_DoCutFlow(false),
        m_Systematics(),
        m_Systematics_W(),
        m_input_dirs(),
        m_systToRun(),
        m_SystNames(),
        m_Cuts(),
        m_Histos(),
        m_tree(nullptr),
        m_CutFlow(nullptr),
        m_RawCutFlow(nullptr),
        m_TreeName(""),
        m_Name("MyAnalysis"),
        m_Region("Region"),
        m_SystAppendix(""),
        m_NormDB(nullptr),
        m_weight(nullptr),
        m_outFile(nullptr),
        m_TotEvents(-1),
        m_CurrentEvent(0),
        m_PrintInterval(100000),
        m_tsw(),
        m_PrintProgress(true),
        m_NominalName("Nominal") {}
    Analysis::~Analysis() { delete NormalizationDataBase::getDataBase(); }
    bool Analysis::ReadMetaDataTree() {
        m_NormDB = XAMPP::NormalizationDataBase::getDataBase();
        if (!m_NormDB->init(m_input_dirs)) return false;
        m_weight = XAMPP::Weight::getWeighter();
        return true;
    }
    bool Analysis::Process(long int nMax, long int nSkip) {
        if (!m_outFile) {
            Error("Analysis::Process()", "No OutFile is available");
            return false;
        }
        if (m_TreeName.empty()) {
            Error("Analysis::Proccess()", "No tree name has been set");
            return false;
        }
        if (!ReadMetaDataTree()) return false;
        m_CurrentEvent = 0;
        XAMPP::NormalizationDataBase::getDataBase()->PromptMetaDataTree();

        if (!ReadSystematics()) return false;
        if (!GetTotalEvents()) return false;
        if (!SetupCutFlow()) return false;
        m_tsw.Start();
        for (const auto& Syst : m_systToRun) {
            if (!Syst->apply_syst()) return false;
            if (!init(Syst->syst_name())) return false;
            if (!ProcessLoop(Syst, nMax, nSkip)) return false;
            Syst->reset();
        }
        return true;
    }
    bool Analysis::ProcessLoop(const std::shared_ptr<IKinematicSyst>& syst, long int nMax, long int nSkip) {
        if (nMax == 0) {
            Error("Analysis::ProcessLoop()", "You want to analyze 0 events ?!");
            return false;
        }
        long int nProcessed = 0;
        double t0 = m_tsw.RealTime();
        m_tsw.Continue();

        PrintHeadLine("Analysis::ProcessLoop() -- INFO:  ===== STARTING LOOP OVER EVENTS - " + syst->syst_name() + " ===========");
        unsigned int n_thfile = 0;
        long int tot_ev = 0;
        for (const auto& File : m_input_dirs) {
            bool Break = false;
            std::shared_ptr<TFile> F = Open(File);
            if (!LoadTreeFromFile(F, syst->tree_name())) return false;
            // If we're running in batch mode we want to minimize the file access. To give the user some hint where his analysis jobs might
            // have been exploded or ran out of quota, we add these info statements.
            if (m_batchMode) {
                tot_ev += m_tree->entries();
                ++n_thfile;
                m_TotEvents = tot_ev * m_input_dirs.size() / n_thfile;
                Info("Analysis::ProcessLoop()", Form("Opened file %s (%u/%lu)", File.c_str(), n_thfile, m_input_dirs.size()));
            }
            if (nSkip > m_tree->entries()) {
                Info("Analysis::ProcessLoop()", "The file " + std::string(F->GetName()) + " contains less entries (" +
                                                    std::to_string(m_tree->entries()) + ") than the ones that should be skipped (" +
                                                    std::to_string(nSkip) + "). Omit it.");
                nSkip -= m_tree->entries();
            } else if (m_tree->entries() == 0) {
                Info("Analysis::ProcessLoop()", "The file " + std::string(F->GetName()) + " contains no events.");
            } else {
                if (!ProcessTree(nMax, nSkip)) {
                    Error("Analysis::ProcessLoop()", "Could not process file " + std::string(F->GetName()) + ".");
                    return false;
                }
                if (nMax > 0) {
                    if ((nMax + nSkip) > m_tree->entries()) {
                        Info("Analysis::ProcessLoop()", "Only " + std::to_string((m_tree->entries() - nSkip)) + " events of " + nMax +
                                                            " were processed in input tree containing " +
                                                            std::to_string(m_tree->entries()) + ". Will process " +
                                                            std::to_string(nMax - m_tree->entries()) + " events in the next round.");
                        nMax -= (m_tree->entries() - nSkip);
                        nProcessed += (m_tree->entries() - nSkip);
                    } else {
                        nProcessed += nMax;
                        Break = true;
                    }
                } else
                    nProcessed += m_tree->entries();
                nSkip = 0;
            }
            if (Break) break;
        }
        WriteOutput();
        double t2 = m_tsw.RealTime() - t0;
        std::string Message = Form("LOOP FINISHED === Needed for %li events in %sh (%.2f kEvts/s)", nProcessed, TimeHMS(t2).c_str(),
                                   nProcessed / (t2 * 1.e3));
        PrintHeadLine("Analysis::ProcessLoop() -- INFO: ==== " + Message);
        m_tsw.Continue();
        return true;
    }
    bool Analysis::ProcessTree(long int nMax, long int nSkip) {
        if (!m_eventService->setupTree()) return false;
        if (nMax < 0 || (nSkip + nMax) > m_tree->entries()) nMax = m_tree->entries();
        if (nSkip > 0) {
            if ((nSkip + nMax) <= m_tree->entries()) nMax += nSkip;
            Info("Analysis::ProcessTree()", "Skip " + std::to_string(nSkip) + " events. Looping over " + std::to_string(nMax - nSkip) +
                                                " events of the tree. " + std::to_string(m_tree->entries()));
        }
        for (long int ev = nSkip; ev < nMax; ++ev) {
            ++m_CurrentEvent;
            if (!m_eventService->getEntry(ev)) return false;
            if (!AnalyzeEvent()) {
                Error("Analysis::ProcessTrees()",
                      Form("The AnalyzeEvent() method has encountered some errors in event %llu from file %s. Aborting the loop",
                           m_eventService->currentEvent(), m_eventService->in_file_path().c_str()));
                return false;
            }
            if (ev % m_PrintInterval == 0) { Progress(); }
        }
        return true;
    }
    bool Analysis::init(const std::string& Syst) {
        if (!m_weight->InitWeights()) return false;
        PrintFooter();
        if (!SetupHistograms(Syst)) return false;
        if (!initBranches(Syst)) return false;
        if (!m_Cuts.empty()) {
            Info("Analysis::init()", "The following cuts are defined:");
            for (const auto& C : m_Cuts) Info("Analysis::init()", WhiteSpaces(10) + " ***  " + C->name());
        }
        PrintFooter();
        return true;
    }
    bool Analysis::PassCuts(bool PassAnd) const {
        unsigned int nPassed = 0;
        // The user has not been defined any cuts -> Everything shall pass
        bool Pass = PassAnd || m_Cuts.empty();
        for (const auto& C : m_Cuts) {
            bool CutPassed = C->Pass();
            if (!CutPassed && PassAnd) {
                Pass = false;
                break;
            } else if (CutPassed && !PassAnd) {
                Pass = true;
                break;
            }
            ++nPassed;
        }
        if (Pass) { ++nPassed; }
        FillCutFlowHistograms(nPassed);
        return Pass;
    }

    bool Analysis::AnalyzeEvent() {
        // Check the cuts first to fill the raw-cut flow at least
        bool Skip = !m_DoCutFlow && m_weight->GetWeight() == 0.;
        if (!Skip && PassCuts()) {
            // Zero weighted entries will never contribute... Skip them from the beginning
            if (m_weight->GetWeight() == 0.) return true;
            for (const auto& H : m_Histos) { H->fill(); }
        }
        return true;
    }
    bool Analysis::HandleWeightVariations() {
        std::vector<std::shared_ptr<IKinematicSyst>> weight_syst;
        for (const auto& W : m_Systematics_W) {
            std::shared_ptr<IKinematicSyst> syst = std::make_shared<WeightSystematic>(W);
            if (!m_eventService->registerKinematic(syst)) {
                Error("Analysis::HandleWeightVariations()", "Could not add systematic  " + W);
                return false;
            }
            m_systToRun.push_back(syst);
        }
        m_weight->ResetSystematic();
        return true;
    }
    std::string Analysis::getFinalSystName(const std::string& Syst) const {
        std::map<std::string, std::string>::const_iterator Itr = m_SystNames.find(Syst);
        if (Itr != m_SystNames.end()) return Itr->second;
        return Syst + m_SystAppendix;
    }
    std::string Analysis::GetDirectoryName(const std::string& Syst, const std::string& Region) const {
        std::string DirName = "";
        std::string FinalSyst = getFinalSystName(Syst);
        DirName = m_Name + (FinalSyst.empty() ? "" : "_") + FinalSyst + "/" + Region;
        return DirName;
    }

    TDirectory* Analysis::CreateDirectory(const std::string& Syst, const std::string& Region) {
        std::string DirName = GetDirectoryName(Syst, Region);
        if (m_outFile->GetDirectory(DirName.c_str())) {
            Error("Analysis::CreateDirectory()",
                  "The directory " + DirName + "already exists. Do not want to clobber with existing content.");
            return nullptr;
        }
        Info("Analysis::CreateDirectory()", "Create new directory " + DirName + ".");
        m_outFile->mkdir((DirName + (m_DoCutFlow ? "/InfoHistograms/" : "")).c_str());
        return m_outFile->GetDirectory(DirName.c_str());
    }
    bool Analysis::SetupHistograms(const std::string& Syst) {
        if (!m_DoCutFlow && m_Histos.empty()) return true;
        TDirectory* D = CreateDirectory(Syst, m_Region);
        if (D == nullptr) return false;
        for (std::vector<std::shared_ptr<Histo>>::const_iterator itr = m_Histos.begin(); itr != m_Histos.end(); ++itr) {
            const std::shared_ptr<Histo> H = (*itr);
            unsigned int n_H = count<std::shared_ptr<Histo>>(itr + 1, m_Histos.end(),
                                                             [H](const std::shared_ptr<Histo>& C) { return C->name() == H->name(); });
            if (n_H > 0) {
                Error("Analysis::SetupHistograms()", Form("The histogram name %s has been defined %u other times", H->name().c_str(), n_H));
                return false;
            }
        }
        for (const auto& H : m_Histos)
            if (!H->init(D)) return false;
        if (m_DoCutFlow) {
            if (!m_RawCutFlow->init(D)) return false;
            if (m_CutFlow && !m_CutFlow->init(D)) return false;
        }
        return true;
    }
    void Analysis::WriteOutput() {
        for (auto& H : m_Histos) H->write();
        if (m_RawCutFlow) m_RawCutFlow->write();
        if (m_CutFlow) m_CutFlow->write();

        for (auto& H : m_Histos) H->Statistics();
        if (m_RawCutFlow) m_RawCutFlow->Statistics();
        if (m_CutFlow) m_CutFlow->Statistics();
    }
    bool Analysis::initBranches(const std::string&) { return true; }
    void Analysis::FillCutFlowHistograms(int N) const {
        if (m_RawCutFlow) m_RawCutFlow->Fill(N);
        if (m_CutFlow) m_CutFlow->Fill(N);
    }
    bool Analysis::ReadSystematics() {
        // Read systematics from the first file in the input list
        if (m_input_dirs.empty()) {
            Error("Analysis::ReadSystematics()", "No files to analyze");
            return false;
        }
        // Systematics have been added by other services
        if (m_DoSyst && !m_eventService->all_systematics().empty()) {
            for (const auto& known : m_eventService->all_systematics()) {
                if (m_Systematics.empty() || IsElementInList(m_Systematics, known->syst_name())) m_systToRun.push_back(known);
            }
        }

        // First of all create a nominal systematic object
        std::shared_ptr<IKinematicSyst> nominal_syst = std::make_shared<TreeSystematic>(m_NominalName, true);
        if (!m_eventService->registerKinematic(nominal_syst)) return false;
        if (m_DoNominal && (m_Systematics.empty() || IsElementInList(m_Systematics, m_NominalName))) m_systToRun.push_back(nominal_syst);

        std::shared_ptr<TFile> F = Open(*m_input_dirs.begin());
        if (!F) return false;
        TIter next(F->GetListOfKeys());
        std::set<std::string> syst_names;
        while (TObject* obj = next()) {
            std::string ObjName = obj->GetName();
            TTree* T = nullptr;
            F->GetObject(obj->GetName(), T);
            if (!T) continue;
            delete T;
            if (ObjName.find(m_TreeName) == 0 && m_TreeName.size() < ObjName.size()) {
                // Get rid of the trailing _
                std::string SystName = ObjName.substr(m_TreeName.size() + 1, std::string::npos);
                syst_names.insert(SystName);
            }
        }
        for (auto& SystName : syst_names) {
            Info("Analysis::ReadSystematics()", "Found new systematic " + SystName);
            // Recall that nominal is already registered at the begining of the loop
            std::shared_ptr<IKinematicSyst> kine_syst(nullptr);
            if (SystName != NominalName()) {
                kine_syst = std::make_shared<TreeSystematic>(SystName, false);
                if (!m_eventService->registerKinematic(kine_syst)) return false;
            } else
                continue;
            if ((m_DoSyst && (m_Systematics.empty() || IsElementInList(m_Systematics, SystName)))) m_systToRun.push_back(kine_syst);
        }
        if (!m_weight->InitWeights()) return false;
        if (m_DoSyst_W) {
            if (!LoadTreeFromFile(F, NominalName())) return false;
            std::vector<std::string> Variations = m_weight->FindWeightVariations(m_tree->tree());
            if (m_Systematics_W.empty()) m_Systematics_W = Variations;
            m_weight->ResetSystematic();
        }
        if (!HandleWeightVariations()) return false;
        if (m_systToRun.empty()) {
            Error("Analysis::ReadSystematics()", "There are no systematic variations of the Tree " + m_TreeName);
            return false;
        }
        std::sort(m_systToRun.begin(), m_systToRun.end(),
                  [](const std::shared_ptr<IKinematicSyst>& a, const std::shared_ptr<IKinematicSyst>& b) {
                      if (a->is_nominal()) return true;
                      if (b->is_nominal()) return false;
                      return a->syst_name() < b->syst_name();
                  });

        Info("Analysis::ReadSystematics()", "Found in total " + std::to_string(m_systToRun.size()) + " systematics.");
        return true;
    }
    bool Analysis::LoadTreeFromFile(std::shared_ptr<TFile> F, const std::string& Syst) {
        std::string TName = m_TreeName + (Syst.empty() ? "" : "_" + Syst);
        if (!F) {
            Error("Analysis::LoadTreeFromFile()", "No input file given. ");
            return false;
        }
        if (!m_eventService->openFile(F) || !m_eventService->loadTree(TName)) return false;
        m_tree = m_eventService->get_master();
        return true;
    }
    bool Analysis::GetTotalEvents() {
        m_TotEvents = 0;
        if (m_batchMode) {
            Info("Analysis::GetTotalEvents()", "Batch mode has been activated. Total events will be iterated on the fly");
            return true;
        }
        for (const auto& File : m_input_dirs) {
            std::shared_ptr<TFile> F = Open(File);
            Info("Analysis::GetTotalEvents()", "Open file " + std::string(File) + " to check the contained events");
            for (const auto& Syst : m_systToRun) {
                if (!LoadTreeFromFile(F, Syst->tree_name())) return false;
                m_TotEvents += m_tree->entries();
            }
        }
        m_PrintInterval = m_TotEvents / (m_systToRun.size() * 100);
        if (m_PrintInterval == 0)
            m_PrintInterval = 1;
        else if (m_PrintInterval >= 1.e6)
            m_PrintInterval = 1.e6;
        Info("Analysis::GetTotalEvents()", "Read in of the " + std::to_string(m_input_dirs.size()) + " files finished. Found in total " +
                                               std::to_string(m_TotEvents) + " events.");
        return true;
    }
    std::string Analysis::name() const { return m_Name; }
    void Analysis::SetReaders(const std::vector<ITreeVarReader*>& ReaderVect) {
        m_eventService->flushReaders();
        m_eventService->SetReaders(ReaderVect);
    }
    void Analysis::SetSystematics(const std::vector<std::string>& List) { CopyVector(List, m_Systematics, true); }
    void Analysis::SetWeightSystmatics(const std::vector<std::string>& List) { CopyVector(List, m_Systematics_W, true); }
    void Analysis::AppendReader(ITreeVarReader* R) { m_eventService->AppendReader(R); }
    bool Analysis::AppendHisto(std::shared_ptr<Histo> H) {
        if (!H) {
            Error("Analysis::AppendHisto()", "nullptr ptr given");
            return false;
        }
        if (IsElementInList(m_Histos, H)) {
            Error("Analysis::AppendHisto()", "The histogram " + H->name() + " is already known");
            return false;
        }
        m_Histos.push_back(H);
        return true;
    }
    bool Analysis::SetupCutFlow() {
        if (!m_DoCutFlow) return true;
        Info("Analysis::SetupCutFlow()", "Setup the histograms for region " + m_Region);

        std::shared_ptr<CutFlowHisto> CutFlow = std::make_shared<CutFlowHisto>("CutFlow", m_Region);
        if (!CutFlow->SetCuts(m_Cuts)) { return false; }
        m_RawCutFlow = CutFlow;
        m_RawCutFlow->DoRaw();
        if (!m_weight->GetUsedWeighters().empty()) {
            CutFlow = std::make_shared<CutFlowHisto>("CutFlow_weighted", m_Region);
            if (!CutFlow->SetCuts(m_Cuts)) return false;
            m_CutFlow = CutFlow;
        }
        return true;
    }
    void Analysis::SetTreeName(const std::string& Name) {
        if (!Name.empty()) m_TreeName = Name;
    }
    void Analysis::SetInputFiles(const std::vector<std::string>& Files) { CopyVector(Files, m_input_dirs, true); }
    void Analysis::SetCuts(const std::vector<std::shared_ptr<Condition>>& C) { CopyVector(C, m_Cuts, true); }
    void Analysis::SetHistos(const std::vector<std::shared_ptr<Histo>>& H) { CopyVector(H, m_Histos, true); }
    void Analysis::SetName(const std::string& N) {
        if (N.size() > 0) m_Name = N;
    }
    void Analysis::ProcessSystematics(bool B) { m_DoSyst = B; }
    void Analysis::ProcessWeightVariations(bool B) { m_DoSyst_W = B; }
    void Analysis::ProcessNominal(bool B) { m_DoNominal = B; }
    void Analysis::SetRegion(const std::string& Region) { m_Region = Region; }
    void Analysis::DoCutFlow(bool B) { m_DoCutFlow = B; }
    void Analysis::SetSystSuffixName(const std::string& N) { m_SystAppendix = N; }
    void Analysis::SetSystNames(const std::map<std::string, std::string>& Names) { m_SystNames = Names; }
    void Analysis::SetOutputLocation(const std::string& File) {
        if (m_outFile) {
            Warning("Analysis::SetOutputLocation()", "There is already an opened TFile " + std::string(m_outFile->GetName()));
            m_outFile->Close();
        }
        if (File.rfind("/") != std::string::npos) {
            std::string OutDir = File.substr(0, File.rfind("/"));
            if (DoesDirectoryExist(OutDir) == false) {
                Info("Analysis::SetOutputLocation()", "Create directory " + OutDir + " to store the output in there");
                gSystem->mkdir(OutDir.c_str(), true);
            } else
                Info("Analysis::SetOutputLocation()", "Output file will be stored in " + OutDir);
        }
        if (File.find(".root") == std::string::npos) Warning("Selector::SetOutputLocation()", "The File " + File + " has no .root suffix");
        Info("Analysis::SetOutputLocation()", "Create output TFile " + File);
        m_outFile = std::make_shared<TFile>(File.c_str(), "RECREATE");
        if (!m_outFile->IsOpen()) m_outFile.reset();
    }
    void Analysis::FinalizeOutput() {
        m_outFile->cd();
        Info("Analysis::FinalizeOutput()", "Writing to file " + std::string(m_outFile->GetName()));
        m_outFile->Purge();
        m_outFile->Close();
    }
    void Analysis::Progress() {
        if (!m_PrintProgress) { return; }
        double t2 = m_tsw.RealTime();
        double cput2 = m_tsw.CpuTime();
        std::cout << '\r' << "Entry " << (int)(m_CurrentEvent / 1000) << "k / " << (int)(m_TotEvents / 1000) << "k ("
                  << std::setprecision(2) << (float)m_CurrentEvent / (float)m_TotEvents * 100. << "%)" << std::setprecision(6);
        std::cout << " after " << TimeHMS(t2) << " (" << TimeHMS(cput2) << " CPU). " << std::setprecision(3)
                  << m_CurrentEvent / (t2 * 1000.) << " / " << m_CurrentEvent / (cput2 * 1.e3)
                  << " kEvt/s, E.T.A.: " << TimeHMS(t2 * ((float)m_TotEvents / (float)m_CurrentEvent - 1.)) << "               "
                  << std::flush;
        std::cout << '\r' << std::flush;  // make sure any other couts are properly displayed
        m_tsw.Continue();
    }
    void Analysis::DisableProgressBar() { m_PrintProgress = false; }
    void Analysis::SetNominalName(const std::string& Nominal) { m_NominalName = Nominal; }

    std::string Analysis::NominalName() const { return m_NominalName; }
    void Analysis::setBatchMode() { m_batchMode = true; }

    bool Analysis::RenameSystematic(const std::string& From, const std::string& To) {
        if (m_SystNames.find(From) != m_SystNames.end()) {
            Error("Selector::RenameSystematic()",
                  "The systematic " + From + " is already renamed to " + m_SystNames.at(From) + " please decide");
            return false;
        }
        m_SystNames.insert(std::pair<std::string, std::string>(From, To));
        return true;
    }
}  // namespace XAMPP
