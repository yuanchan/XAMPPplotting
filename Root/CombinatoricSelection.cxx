#include <XAMPPplotting/CombinatoricSelection.h>
#include <XAMPPplotting/DataDrivenWeights.h>
#include <XAMPPplotting/IfDefHelpers.h>
#include <XAMPPplotting/PlottingUtils.h>
#include <XAMPPplotting/TreeVarReader.h>
#include <XAMPPplotting/TreeVarWriter.h>

namespace XAMPP {
    //##################################################################
    //                      DrawElementsFromSet
    //##################################################################
    DrawElementsFromSet::DrawElementsFromSet(unsigned int N, unsigned int M, unsigned int offset) :
        m_N(N),
        m_M(M),
        m_offset(offset),
        m_size(0),
        m_n_minus1(nullptr),
        m_borders() {
        /// Use the identity (N, M) = (N, N-M)
        ///  (N, K) = sum_{i=K)^{N-1} (I, K)
        m_borders.reserve(N - M);
        for (unsigned int i = N - 1; i >= M - 1; --i) {
            m_size += binomial(i, M - 1);
            m_borders.push_back(m_size);
            if (i == 0) break;
        }
        if (N != M && M > 1) m_n_minus1 = std::make_unique<DrawElementsFromSet>(N - 1, M - 1, 0);
    }
    unsigned int DrawElementsFromSet::get_zeroth(unsigned int ele) const {
        for (unsigned int k = 0; k < m_borders.size(); ++k) {
            if (m_borders[k] > ele) return k;
        }
        throw std::range_error(Form("(%u, %u) element %u is out of range.", n(), m(), ele));
        return -1;
    }
    unsigned int DrawElementsFromSet::value_in_tuple(unsigned int ele, unsigned int pos) const {
        return m_offset + value_in_tuple(ele, pos, 0);
    }
    unsigned int DrawElementsFromSet::value_in_tuple(unsigned int ele, unsigned int pos, unsigned int min) const {
        if (pos >= m()) { throw std::range_error(Form("You can only draw %u elements. Why do you try %u?", m(), pos)); }
        /// Special case where N out of N elements are drawn. There is only one permutation possible
        if (n() == m()) return pos;
        unsigned int zero = get_zeroth(ele);
        if (pos == 0) { return zero + min; }
        unsigned int it_to_next = m_borders[0] - (m_borders[zero] - ele);
        return m_n_minus1->value_in_tuple(it_to_next, pos - 1, min + 1);
    }
    unsigned int DrawElementsFromSet::n() const { return m_N; }
    unsigned int DrawElementsFromSet::m() const { return m_M; }
    unsigned int DrawElementsFromSet::size() const { return m_size; }
    unsigned int DrawElementsFromSet::offset() const { return m_offset; }
    std::vector<unsigned int> DrawElementsFromSet::draw(unsigned int ele) const {
        if (ele >= size()) {
            throw std::range_error(
                Form("There are only %u combinations to draw %u elements out of %u, while you're asking for the %u-th element.", size(),
                     n(), m(), ele + 1));
        }
        std::vector<unsigned int> x(m(), 0);
        for (unsigned int i = 0; i < m(); ++i) { x[i] = value_in_tuple(ele, i); }
        return x;
    }
    //##################################################################
    //                      CombinatoricService
    //##################################################################
    std::vector<IPermutationElement*> CombinatoricService::permuations() const { return m_permut_ele; }
    bool CombinatoricService::has_permutable() const {
        return std::find_if(m_permut_ele.begin(), m_permut_ele.end(),
                            [](IPermutationElement* ele) { return ele->include_combinatorics(); }) != m_permut_ele.end();
    }
    CombinatoricService* CombinatoricService::m_Inst = nullptr;
    CombinatoricService* CombinatoricService::getService() {
        if (!m_Inst) m_Inst = new CombinatoricService();
        return m_Inst;
    }
    CombinatoricService::~CombinatoricService() { m_Inst = nullptr; }
    std::shared_ptr<DrawElementsFromSet> CombinatoricService::comb_to_draw(unsigned int n, unsigned k, unsigned int offset) {
        std::vector<std::shared_ptr<DrawElementsFromSet>>::iterator itr =
            std::find_if(m_draw_sets.begin(), m_draw_sets.end(), [&n, &k, &offset](const std::shared_ptr<DrawElementsFromSet>& ele) {
                return ele->n() == n && ele->m() == k && ele->offset() == offset;
            });
        if (itr != m_draw_sets.end()) return (*itr);
        std::shared_ptr<DrawElementsFromSet> cmb = std::make_shared<DrawElementsFromSet>(n, k, offset);
        m_draw_sets.push_back(cmb);
        return cmb;
    }
    CombinatoricService::CombinatoricService() :
        m_weighter(Weight::getWeighter()),
        m_service(EventService::getService()),
        m_draw_sets(),
        m_permut_ele(),
        m_event_number(-1),
        m_combinations(0),
        m_ptr(0),
        m_cleared(false) {}
    bool CombinatoricService::next() {
        if (m_event_number != m_service->currentEvent()) {
            m_event_number = m_service->currentEvent();
            if (!m_cleared) {
                EraseFromVector<IPermutationElement*>(m_permut_ele, [](IPermutationElement* ele) { return !ele->include_combinatorics(); });
                m_cleared = true;
            }
            if (!m_permut_ele.empty()) {
                std::vector<IPermutationElement*>::const_iterator itr = std::max_element(
                    m_permut_ele.begin(), m_permut_ele.end(),
                    [](IPermutationElement* a, IPermutationElement* b) { return a->n_combinations() < b->n_combinations(); });
                m_combinations = (*itr)->n_combinations();
            }
            return true;
        }
        ++m_ptr;

        if (m_ptr < m_combinations) {
            m_weighter->NewEvent();
            return true;
        }
        /// Reset the pointer for the next event
        m_ptr = 0;
        return false;
    }
    unsigned int CombinatoricService::current_combinatoric() const { return m_ptr; }

    void CombinatoricService::attach(IPermutationElement* element) { m_permut_ele.push_back(element); }
    void CombinatoricService::detach(IPermutationElement* element) { RemoveElement(m_permut_ele, element); }

    //##################################################################
    //                      IPermutationElement
    //##################################################################
    IPermutationElement::~IPermutationElement() { CombinatoricService::getService()->detach(this); }
    IPermutationElement::IPermutationElement() { CombinatoricService::getService()->attach(this); }

    //##################################################################
    //                      SubSetNTupleWriter
    //##################################################################
    SubSetNTupleWriter::SubSetNTupleWriter() : NTupleWriter(), m_service(CombinatoricService::getService()) {}
    SubSetNTupleWriter::~SubSetNTupleWriter() { delete CombinatoricService::getService(); }
    bool SubSetNTupleWriter::AnalyzeEvent() {
        while (m_service->next()) {
            if (!NTupleWriter::AnalyzeEvent()) return false;
        }
        return true;
    }
    //##################################################################
    //                      SubSetAnalysis
    //##################################################################
    SubSetAnalysis::SubSetAnalysis() : Analysis(), m_service(CombinatoricService::getService()) {}
    SubSetAnalysis::~SubSetAnalysis() { delete CombinatoricService::getService(); }
    bool SubSetAnalysis::AnalyzeEvent() {
        while (m_service->next()) {
            if (!Analysis::AnalyzeEvent()) return false;
        }
        return true;
    }
    //##################################################################
    //                      SubSetParticleReader
    //##################################################################

    IParticleReader* SubSetParticleReader::underyling_particle() const { return m_particle; }
    std::string SubSetParticleReader::name() const { return m_name; }
    bool SubSetParticleReader::init(TTree* t) {
        if (m_possible_permutations.empty()) {
            m_possible_permutations = m_comb_service->permuations();
            EraseFromVector<IPermutationElement*>(m_possible_permutations, [this](IPermutationElement* ele) {
                return !ele->include_combinatorics() || ele->particle() != underyling_particle();
            });
        }
        if (!underyling_particle()) {
            Error("SubSetParticleReader::init()", Form("No particle was given to %s", name().c_str()));
            return false;
        }
        return underyling_particle()->init(t);
    }

    IParticleVarReader* SubSetParticleReader::RetrieveVariable(const std::string& Var) {
        return SubSetParticleVariableReader::GetReader(this, Var);
    }
    size_t SubSetParticleReader::Size() {
        if (m_cached_event != m_weighter->eventNumber()) {
            m_cached_event = m_weighter->eventNumber();
            m_active_permutation.clear();
            m_active_element = nullptr;
            if (!m_possible_permutations.empty() && underyling_particle()->Size()) {
                std::vector<IPermutationElement*>::const_iterator itr = std::max_element(
                    m_possible_permutations.begin(), m_possible_permutations.end(),
                    [](IPermutationElement* a, IPermutationElement* b) { return a->get_entered().size() < b->get_entered().size(); });
                m_active_element = (*itr);
                if (m_active_element->get_entered().size()) {
                    m_active_permutation = m_active_element->combinatoric_set()->draw(m_comb_service->current_combinatoric());
                }
                EraseFromVector<unsigned int>(m_active_permutation, [this](const unsigned int& ele) {
                    return (ele < m_active_element->combinatoric_offset() && m_active_element->combinatoric_offset()) ||
                           ele >= underyling_particle()->Size() + m_active_element->combinatoric_offset();
                });
                if (m_active_permutation.size() > underyling_particle()->Size()) {
                    m_active_permutation.resize(underyling_particle()->Size());
                }
            }
        }
        return m_active_permutation.size();
    }
    size_t SubSetParticleReader::remap_entry(size_t i) {
        if (i >= Size()) {
            Warning("SubSetParticleReader::remap_entry()", Form("Index %lu is out of range", i));
            return -1;
        }
        return m_active_element->get_entered()[m_active_permutation[i] - m_active_element->combinatoric_offset()];
    }
    size_t SubSetParticleReader::ReaderIndex() const { return underyling_particle()->ReaderIndex(); }
    size_t SubSetParticleReader::ReaderIndex(size_t P) { return underyling_particle()->ReaderIndex(remap_entry(P)); }
    unsigned int SubSetParticleReader::GetCollectionHash() const { return underyling_particle()->GetCollectionHash(); }
    const TLorentzVector& SubSetParticleReader::P4() const { return underyling_particle()->P4(); }
    TLorentzVector SubSetParticleReader::GetP4(size_t E) { return underyling_particle()->GetP4(remap_entry(E)); }
    SubSetParticleReader::SubSetParticleReader(IParticleReader* particle, const std::string& alias_name) :
        m_name(alias_name),
        m_particle(particle),
        m_weighter(Weight::getWeighter()),
        m_comb_service(CombinatoricService::getService()),
        m_possible_permutations(),
        m_active_element(nullptr),
        m_active_permutation(),
        m_cached_event(-1) {
        ParReaderStorage::GetInstance()->Register(this);
    }
    SubSetParticleReader* SubSetParticleReader::GetReader(const std::string& alias_name, IParticleReader* collection) {
        if (!ParReaderStorage::GetInstance()->GetReader(alias_name)) return new SubSetParticleReader(collection, alias_name);
        return dynamic_cast<SubSetParticleReader*>(ParReaderStorage::GetInstance()->GetReader(alias_name));
    }
    //##################################################################
    //                      SubSetParticleVariableReader
    //##################################################################
    IParticleVarReader* SubSetParticleVariableReader::GetReader(SubSetParticleReader* particle, const std::string& var) {
        IParticleVarReader* var_reader = particle->underyling_particle()->RetrieveVariable(var);
        std::string r_name = "SubSet of " + var_reader->name();
        if (!ITreeVarReaderStorage::GetInstance()->GetReader(r_name)) return new SubSetParticleVariableReader(particle, var_reader);
        return dynamic_cast<IParticleVarReader*>(ITreeVarReaderStorage::GetInstance()->GetReader(r_name));
    }
    std::string SubSetParticleVariableReader::name() const { return "SubSet of " + m_variable->name(); }
    double SubSetParticleVariableReader::readEntry(size_t I) const { return m_variable->readEntry(m_subset_part->remap_entry(I)); }
    bool SubSetParticleVariableReader::init(TTree* t) {
        if (!m_Registered) {
            Error("SubSetParticleVariableReader()", "Something went wrong during the creation");
            return false;
        }
        return m_variable->init(t) && m_subset_part->init(t);
    }
    SubSetParticleVariableReader::SubSetParticleVariableReader(SubSetParticleReader* particle, IParticleVarReader* variable) :
        IParticleVariable(particle),
        m_subset_part(particle),
        m_variable(variable),
        m_Registered(false) {
        m_Registered = ITreeVarReaderStorage::GetInstance()->Register(this);
    }

}  // namespace XAMPP
