#include <FourMomUtils/xAODP4Helpers.h>
#include <TRandom3.h>
#include <XAMPPplotting/ArithmetricReaders.h>
#include <XAMPPplotting/ObservableReader.h>
#include <XAMPPplotting/ReaderProvider.h>
#include <XAMPPplotting/ReconstructedParticleReaders.h>
#include <XAMPPplotting/UtilityReader.h>
#include <XAMPPplotting/Weight.h>

namespace XAMPP {

    //#########################################################################################
    //                                  PeriodReader
    //#########################################################################################
    PeriodReader::PeriodReader() : IScalarReader(), m_Reader(nullptr), m_Registered(false), m_UsePRWTool(false) {
        m_Registered = ITreeVarReaderStorage::GetInstance()->Register(this);
    }
    ITreeVarReader* PeriodReader::GetReader() {
        ITreeVarReaderStorage* S = ITreeVarReaderStorage::GetInstance();
        if (!S->GetReader("Period RunNumberReader")) return new PeriodReader();
        return S->GetReader("Period RunNumberReader");
    }
    double PeriodReader::read() const {
        if (m_UsePRWTool) return GetRndRunNumber();
        return m_Reader->read();
    }
    unsigned int PeriodReader::GetRndRunNumber() const {
        Weight::getWeighter()->UpdateEventInfo();
        static SG::AuxElement::ConstAccessor<unsigned int> acc_rnd("RandomRunNumber");
        return acc_rnd((*Weight::getWeighter()->GetInfo()));
    }
    bool PeriodReader::init(TTree* t) {
        if (!m_Reader) {
            if (Weight::getWeighter()->isData()) {
                m_Reader = ScalarVarReader<unsigned int>::GetReader("runNumber");
                m_UsePRWTool = false;
            } else {
                m_Reader = ScalarVarReader<unsigned int>::GetReader("RandomRunNumber");
            }
        }
        if (!m_Reader->init(t)) return false;
        reset();
        return true;
    }
    std::string PeriodReader::name() const { return "Period RunNumberReader"; }

    //#########################################################################################
    //                                  DataLumiReader
    //#########################################################################################
    DataLumiReader::DataLumiReader() : IScalarReader(), m_Reader(nullptr), m_Registered(false), m_UsePRWTool(false) {
        m_Registered = ITreeVarReaderStorage::GetInstance()->Register(this);
    }
    ITreeVarReader* DataLumiReader::GetReader() {
        ITreeVarReaderStorage* S = ITreeVarReaderStorage::GetInstance();
        if (!S->GetReader("DataLumi LumiBlockReader")) return new DataLumiReader();
        return S->GetReader("DataLumi LumiBlockReader");
    }
    double DataLumiReader::read() const {
        if (m_UsePRWTool) return GetRandomLumiBlock();
        return m_Reader->read();
    }
    unsigned int DataLumiReader::GetRandomLumiBlock() const {
        Weight::getWeighter()->UpdateEventInfo();
        static SG::AuxElement::ConstAccessor<unsigned int> acc_rnd("RandomLumiBlockNumber");
        return acc_rnd((*Weight::getWeighter()->GetInfo()));
    }
    bool DataLumiReader::init(TTree* t) {
        if (!m_Reader) {
            if (Weight::getWeighter()->isData()) {
                m_Reader = ScalarVarReader<unsigned int>::GetReader("lumiBlock");
                m_UsePRWTool = false;
            } else {
                m_Reader = ScalarVarReader<unsigned int>::GetReader("RandomLumiBlockNumber");
            }
        }
        if (!m_Reader->init(t)) return false;
        reset();
        return true;
    }
    std::string DataLumiReader::name() const { return "DataLumi LumiBlockReader"; }

    //#########################################################################################
    //                                  ParticleCorrelationReader
    //#########################################################################################
    ParticleCorrelationReader::ParticleCorrelationReader(const std::string& First, const std::string& Second,
                                                         std::function<double(size_t, size_t)> Correlation) :
        m_FirstParticle(ParReaderStorage::GetInstance()->GetReader(First)),
        m_SecondParticle(ParReaderStorage::GetInstance()->GetReader(Second)),
        m_CorrelFct(Correlation),
        m_FirstPt(nullptr),
        m_SecondPt(nullptr),
        m_FirstEta(nullptr),
        m_SecondEta(nullptr),
        m_FirstPhi(nullptr),
        m_SecondPhi(nullptr) {}
    void ParticleCorrelationReader::Register() {
        if (!m_FirstParticle || !m_SecondParticle || !ITreeVarReaderStorage::GetInstance()->Register(this)) {
            m_FirstParticle = nullptr;
            m_SecondParticle = nullptr;
        } else {
            m_FirstPt = First()->RetrieveVariable("pt");
            m_FirstEta = First()->RetrieveVariable("eta");
            m_FirstPhi = First()->RetrieveVariable("phi");
            m_SecondPt = Second()->RetrieveVariable("pt");
            m_SecondEta = Second()->RetrieveVariable("eta");
            m_SecondPhi = Second()->RetrieveVariable("phi");
        }
    }
    double ParticleCorrelationReader::readMatrixEntry(const size_t i, const size_t j) const {
        if (j >= row_entries(i) || PointsToSameObject(i, j)) {
            Warning("ParticleCorrelationReader::readMatrixEntry()",
                    "Indices " + std::to_string(i) + ", " + std::to_string(j) + " are not in range of " + name());
            return 1.e25;
        }
        return m_CorrelFct(i, j);
    }
    bool ParticleCorrelationReader::PointsToSameObject(size_t F, size_t S) const {
        if (First() == Second()) { return F == S; }
        bool points = First()->At(F) && Second()->At(S) && First()->GetCollectionHash() == Second()->GetCollectionHash();
        First()->reset();
        Second()->reset();
        if (!points) return false;
        return First()->ReaderIndex(F) == Second()->ReaderIndex(S);
    }
    size_t ParticleCorrelationReader::row_entries(size_t i) const {
        // Constrain from the interface design of the IMatrixReader class
        if (i >= num_rows()) return 0;
        //
        if (First() == Second()) return i;
        return Second()->Size();
    }
    size_t ParticleCorrelationReader::num_rows() const { return First()->Size(); }
    bool ParticleCorrelationReader::init(TTree* t) {
        if (!First()) Error("ParticleCorrelationReader::init()", "First particle could not be found.");
        if (!Second()) Error("ParticleCorrelationReader::init()", "Second particle could not be found.");
        if (!First() || !Second() || !First()->init(t) || !Second()->init(t)) return false;
        if (!m_FirstPt->init(t) || !m_FirstEta->init(t) || !m_FirstPhi->init(t)) return false;
        if (!m_SecondPt->init(t) || !m_SecondEta->init(t) || !m_SecondPhi->init(t)) return false;
        reset();
        return true;
    }
    double ParticleCorrelationReader::DeltaPhi(size_t I, size_t J) const {
        return xAOD::P4Helpers::deltaPhi(m_FirstPhi->readEntry(I), m_SecondPhi->readEntry(J));
    }
    double ParticleCorrelationReader::DeltaEta(size_t I, size_t J) const { return m_FirstEta->readEntry(I) - m_SecondEta->readEntry(J); }
    double ParticleCorrelationReader::DeltaY(size_t I, size_t J) const {
        return First()->GetP4(I).Rapidity() - Second()->GetP4(J).Rapidity();
    }
    //#########################################################################################
    //                                  InvariantDiMassReader
    //#########################################################################################
    ParticleCorrelationReader* InvariantDiMassReader::GetReader(const std::string& FirstParticle, const std::string& SecondParticle) {
        ITreeVarReaderStorage* S = ITreeVarReaderStorage::GetInstance();
        std::string Name = Form("m(%s , %s)", FirstParticle.c_str(), SecondParticle.c_str());
        if (!S->GetReader(Name)) return new InvariantDiMassReader(FirstParticle, SecondParticle);
        return dynamic_cast<ParticleCorrelationReader*>(S->GetReader(Name));
    }
    InvariantDiMassReader::InvariantDiMassReader(const std::string& FirstParticle, const std::string& SecondParticle) :
        ParticleCorrelationReader(FirstParticle, SecondParticle,
                                  [this](size_t i, size_t j) { return (First()->GetP4(i) + Second()->GetP4(j)).M(); }) {
        Register();
    }
    std::string InvariantDiMassReader::name() const { return Form("m(%s , %s)", First()->name().c_str(), Second()->name().c_str()); }

    //#########################################################################################
    //                                  InvariantDiPtReader
    //#########################################################################################
    ParticleCorrelationReader* InvariantDiPtReader::GetReader(const std::string& FirstParticle, const std::string& SecondParticle) {
        ITreeVarReaderStorage* S = ITreeVarReaderStorage::GetInstance();
        std::string Name = Form("pt(%s + %s)", FirstParticle.c_str(), SecondParticle.c_str());
        if (!S->GetReader(Name)) return new InvariantDiPtReader(FirstParticle, SecondParticle);
        return dynamic_cast<ParticleCorrelationReader*>(S->GetReader(Name));
    }
    InvariantDiPtReader::InvariantDiPtReader(const std::string& FirstParticle, const std::string& SecondParticle) :
        ParticleCorrelationReader(FirstParticle, SecondParticle,
                                  [this](size_t i, size_t j) { return (First()->GetP4(i) + Second()->GetP4(j)).Pt(); }) {
        Register();
    }
    std::string InvariantDiPtReader::name() const { return Form("pt(%s + %s)", First()->name().c_str(), Second()->name().c_str()); }

    //#########################################################################################
    //                                  TransverseMassReader
    //#########################################################################################
    ParticleCorrelationReader* TransverseMassReader::GetReader(const std::string& FirstParticle, const std::string& SecondParticle) {
        ITreeVarReaderStorage* S = ITreeVarReaderStorage::GetInstance();
        std::string Name = Form("mT(%s , %s)", FirstParticle.c_str(), SecondParticle.c_str());
        if (!S->GetReader(Name)) return new TransverseMassReader(FirstParticle, SecondParticle);
        return dynamic_cast<ParticleCorrelationReader*>(S->GetReader(Name));
    }
    TransverseMassReader::TransverseMassReader(const std::string& FirstParticle, const std::string& SecondParticle) :
        ParticleCorrelationReader(
            FirstParticle, SecondParticle,
            [this](size_t i, size_t j) { return sqrt(2 * (m_FirstPt->readEntry(i) * m_SecondPt->readEntry(j)) * (1 - DeltaPhi(i, j))); }

        ) {
        Register();
    }
    std::string TransverseMassReader::name() const { return Form("mT(%s , %s)", First()->name().c_str(), Second()->name().c_str()); }
    //#########################################################################################
    //                                  MomentumImbalanceReader
    //#########################################################################################
    ParticleCorrelationReader* MomentumImbalanceReader::GetReader(const std::string& FirstParticle, const std::string& SecondParticle) {
        ITreeVarReaderStorage* S = ITreeVarReaderStorage::GetInstance();
        std::string Name = Form("ImBal(%s , %s)", FirstParticle.c_str(), SecondParticle.c_str());
        if (!S->GetReader(Name)) return new MomentumImbalanceReader(FirstParticle, SecondParticle);
        return dynamic_cast<ParticleCorrelationReader*>(S->GetReader(Name));
    }
    MomentumImbalanceReader::MomentumImbalanceReader(const std::string& FirstParticle, const std::string& SecondParticle) :
        ParticleCorrelationReader(FirstParticle, SecondParticle, [this](size_t i, size_t j) {
            return (m_FirstPt->readEntry(i) - m_SecondPt->readEntry(j)) / (m_FirstPt->readEntry(i) + m_SecondPt->readEntry(j));
        }) {
        Register();
    }
    std::string MomentumImbalanceReader::name() const { return Form("ImBal(%s , %s)", First()->name().c_str(), Second()->name().c_str()); }
    //#########################################################################################
    //                                  DeltaYReader
    //#########################################################################################
    ParticleCorrelationReader* DeltaYReader::GetReader(const std::string& FirstParticle, const std::string& SecondParticle) {
        ITreeVarReaderStorage* S = ITreeVarReaderStorage::GetInstance();
        std::string Name = "dY_" + FirstParticle + "_" + SecondParticle;
        if (!S->GetReader(Name)) return new DeltaYReader(FirstParticle, SecondParticle);
        return dynamic_cast<ParticleCorrelationReader*>(S->GetReader(Name));
    }
    std::string DeltaYReader::name() const { return "dY_" + First()->name() + "_" + Second()->name(); }
    DeltaYReader::DeltaYReader(const std::string& FirstParticle, const std::string& SecondParticle) :
        ParticleCorrelationReader(FirstParticle, SecondParticle, [this](size_t i, size_t j) { return DeltaY(i, j); }) {
        Register();
    }
    //#########################################################################################
    //                                  DeltaRReader
    //#########################################################################################
    ParticleCorrelationReader* DeltaRReader::GetReader(const std::string& FirstParticle, const std::string& SecondParticle) {
        ITreeVarReaderStorage* S = ITreeVarReaderStorage::GetInstance();
        std::string Name = "dR_" + FirstParticle + "_" + SecondParticle;
        if (!S->GetReader(Name)) return new DeltaRReader(FirstParticle, SecondParticle);
        return dynamic_cast<ParticleCorrelationReader*>(S->GetReader(Name));
    }
    std::string DeltaRReader::name() const { return "dR_" + First()->name() + "_" + Second()->name(); }
    DeltaRReader::DeltaRReader(const std::string& FirstParticle, const std::string& SecondParticle) :
        ParticleCorrelationReader(FirstParticle, SecondParticle, [this](size_t i, size_t j) {
            return std::sqrt(std::pow(DeltaPhi(i, j), 2) + std::pow(DeltaEta(i, j), 2));
        }) {
        Register();
    }

    //#########################################################################################
    //                                  DeltaRReader
    //#########################################################################################
    ParticleCorrelationReader* DeltaRReaderY::GetReader(const std::string& FirstParticle, const std::string& SecondParticle) {
        ITreeVarReaderStorage* S = ITreeVarReaderStorage::GetInstance();
        std::string Name = "dRrapidity_" + FirstParticle + "_" + SecondParticle;
        if (!S->GetReader(Name)) return new DeltaRReaderY(FirstParticle, SecondParticle);
        return dynamic_cast<ParticleCorrelationReader*>(S->GetReader(Name));
    }
    std::string DeltaRReaderY::name() const { return "dRrapidity_" + First()->name() + "_" + Second()->name(); }
    DeltaRReaderY::DeltaRReaderY(const std::string& FirstParticle, const std::string& SecondParticle) :
        ParticleCorrelationReader(FirstParticle, SecondParticle, [this](size_t i, size_t j) {
            return std::sqrt(std::pow(DeltaPhi(i, j), 2) + std::pow(DeltaY(i, j), 2));
        }) {
        Register();
    }
    //#########################################################################################
    //                                  DeltaPhiReader
    //#########################################################################################
    ParticleCorrelationReader* DeltaPhiReader::GetReader(const std::string& FirstParticle, const std::string& SecondParticle) {
        ITreeVarReaderStorage* S = ITreeVarReaderStorage::GetInstance();
        std::string Name = "dPhi_" + FirstParticle + "_" + SecondParticle;
        if (!S->GetReader(Name)) return new DeltaPhiReader(FirstParticle, SecondParticle);
        return dynamic_cast<ParticleCorrelationReader*>(S->GetReader(Name));
    }
    std::string DeltaPhiReader::name() const { return "dPhi_" + First()->name() + "_" + Second()->name(); }
    DeltaPhiReader::DeltaPhiReader(const std::string& FirstParticle, const std::string& SecondParticle) :
        ParticleCorrelationReader(FirstParticle, SecondParticle, [this](size_t i, size_t j) { return DeltaPhi(i, j); }) {
        Register();
    }
    //#########################################################################################
    //                                  DeltaEtaReader
    //#########################################################################################
    ParticleCorrelationReader* DeltaEtaReader::GetReader(const std::string& FirstParticle, const std::string& SecondParticle) {
        ITreeVarReaderStorage* S = ITreeVarReaderStorage::GetInstance();
        std::string Name = "dEta_" + FirstParticle + "_" + SecondParticle;
        if (!S->GetReader(Name)) return new DeltaEtaReader(FirstParticle, SecondParticle);
        return dynamic_cast<ParticleCorrelationReader*>(S->GetReader(Name));
    }
    std::string DeltaEtaReader::name() const { return "dEta_" + First()->name() + "_" + Second()->name(); }
    DeltaEtaReader::DeltaEtaReader(const std::string& FirstParticle, const std::string& SecondParticle) :
        ParticleCorrelationReader(FirstParticle, SecondParticle, [this](size_t i, size_t j) { return DeltaEta(i, j); }) {
        Register();
    }

    //#########################################################################################
    //                  ConTransverseMassReader
    //#########################################################################################
    std::string ConTransverseMassReader::name() const { return "mCT_" + First()->name() + "_" + Second()->name(); }
    ParticleCorrelationReader* ConTransverseMassReader::GetReader(const std::string& FirstParticle, const std::string& SecondParticle) {
        std::string Name = "mCT_" + FirstParticle + "_" + SecondParticle;
        ITreeVarReaderStorage* S = ITreeVarReaderStorage::GetInstance();
        if (!S->GetReader(Name)) return new ConTransverseMassReader(FirstParticle, SecondParticle);
        return dynamic_cast<ParticleCorrelationReader*>(S->GetReader(Name));
    }
    ConTransverseMassReader::ConTransverseMassReader(const std::string& FirstParticle, const std::string& SecondParticle) :
        ParticleCorrelationReader(FirstParticle, SecondParticle, [this](size_t i, size_t j) {
            return std::sqrt(2. * m_FirstPt->readEntry(i) * m_SecondPt->readEntry(j) * (1. + std::cos(DeltaPhi(i, j))));
        }) {
        Register();
    }

    //#########################################################################################
    //                  ExtremumCorrelationReader
    //#########################################################################################
    IParticleVariable* ExtremumCorrelationReader::GetReader(ParticleCorrelationReader* in, bool minimum) {
        std::string r_name = (minimum ? "Minimum " : "Maximum ") + in->name();
        if (!ITreeVarReaderStorage::GetInstance()->GetReader(r_name)) return new ExtremumCorrelationReader(in, minimum);
        return dynamic_cast<IParticleVariable*>(ITreeVarReaderStorage::GetInstance()->GetReader(r_name));
    }
    std::string ExtremumCorrelationReader::name() const { return (m_search_min ? "Minimum " : "Maximum ") + m_corr_reader->name(); }
    ExtremumCorrelationReader::ExtremumCorrelationReader(ParticleCorrelationReader* in, bool minimum) :
        IParticleVariable(in->First()),
        m_corr_reader(in),
        m_search_min(minimum) {
        ITreeVarReaderStorage::GetInstance()->Register(this);
    }
    bool ExtremumCorrelationReader::init(TTree* t) { return m_corr_reader->init(t); }
    double ExtremumCorrelationReader::readEntry(size_t i) const {
        double ext = m_search_min ? FLT_MAX : -FLT_MAX;
        if (i >= m_corr_reader->First()->Size() || m_corr_reader->Second()->Size() == 0) { return ext; }
        for (size_t j = 0; j < m_corr_reader->Second()->Size(); ++j) {
            if (m_corr_reader->PointsToSameObject(i, j)) continue;

            double value = m_corr_reader->First() != m_corr_reader->Second() || i > j ? m_corr_reader->readMatrixEntry(i, j)
                                                                                      : m_corr_reader->readMatrixEntry(j, i);
            if (m_search_min && value < ext)
                ext = value;
            else if (!m_search_min && value > ext)
                ext = value;
        }
        return ext;
    }
    //#########################################################################################
    //                                  ExtremumVarReader
    //#########################################################################################
    ExtremumVarReader::ExtremumVarReader(const std::string& Name, const std::vector<ITreeVarReader*>& Readers) :
        IScalarReader(),
        m_Name(Name),
        m_Ext(1.e25),
        m_EventNumber(-1),
        m_Readers() {
        CopyVector(Readers, m_Readers);
        if (!ITreeVarReaderStorage::GetInstance()->Register(this)) { m_Readers.clear(); }
    }
    bool ExtremumVarReader::init(TTree* t) {
        m_EventNumber = -1;
        if (m_Readers.empty()) {
            Error("ExtremumVarReader::init()", "No Valid reader was given");
            return false;
        }
        for (const auto& R : m_Readers) {
            if (!R->init(t)) return false;
        }
        reset();
        return true;
    }
    std::string ExtremumVarReader::name() const { return m_Name; }
    double ExtremumVarReader::read() const {
        if (Weight::getWeighter()->eventNumber() != m_EventNumber) {
            m_EventNumber = Weight::getWeighter()->eventNumber();
            m_Ext = GetInitial();
            for (const auto& V : m_Readers) {
                V->reset();
                while (V->update()) {
                    if (IsExtremum(m_Ext, V->read())) { m_Ext = V->read(); }
                }
                V->reset();
            }
        }
        return m_Ext;
    }
    //#########################################################################################
    //                                  MaximumVarReader
    //#########################################################################################
    MaximumVarReader* MaximumVarReader::GetReader(const std::string& Name, const std::vector<ITreeVarReader*>& Readers) {
        ITreeVarReaderStorage* S = ITreeVarReaderStorage::GetInstance();
        if (!S->GetReader(Name)) { new MaximumVarReader(Name, Readers); }
        return dynamic_cast<MaximumVarReader*>(S->GetReader(Name));
    }
    MaximumVarReader::MaximumVarReader(const std::string& Name, const std::vector<ITreeVarReader*>& Readers) :
        ExtremumVarReader(Name, Readers) {
        Info("MaximumVarReader()", "Created new maximum reader " + name() + " using:");
        for (const auto& R : m_Readers) { Info("MaximumVarReader()", "    *** " + R->name()); }
    }
    double MaximumVarReader::GetInitial() const { return -1.e25; }
    bool MaximumVarReader::IsExtremum(double Current, double Test) const { return Current < Test; }
    //#########################################################################################
    //                                  MinimumVarReader
    //#########################################################################################
    MinimumVarReader* MinimumVarReader::GetReader(const std::string& Name, const std::vector<ITreeVarReader*>& Readers) {
        ITreeVarReaderStorage* S = ITreeVarReaderStorage::GetInstance();
        if (!S->GetReader(Name)) { new MinimumVarReader(Name, Readers); }
        return dynamic_cast<MinimumVarReader*>(S->GetReader(Name));
    }
    MinimumVarReader::MinimumVarReader(const std::string& Name, const std::vector<ITreeVarReader*>& Readers) :
        ExtremumVarReader(Name, Readers) {
        Info("MinimumVarReader()", "Created new minimum reader " + name() + " using:");
        for (const auto& R : m_Readers) { Info("MinimumVarReader()", "    *** " + R->name()); }
    }
    double MinimumVarReader::GetInitial() const { return 1.e25; }
    bool MinimumVarReader::IsExtremum(double Current, double Test) const { return Current > Test; }
    //#########################################################################################
    //                                  DeltaPhiToMetReader
    //#########################################################################################
    IParticleVarReader* DeltaPhiToMetReader::GetReader(const std::string& Particle, const std::string& Met) {
        ITreeVarReaderStorage* S = ITreeVarReaderStorage::GetInstance();
        std::string Name = "DeltaPhi " + Particle + " " + Met;
        if (!ParReaderStorage::GetInstance()->GetReader(Particle)) {
            Error("DeltaPhiToMetReader()", "The particle " + Particle + " does not exist");
            return nullptr;
        }
        if (!S->GetReader(Name)) return new DeltaPhiToMetReader(Particle, Met);
        return dynamic_cast<IParticleVarReader*>(S->GetReader(Name));
    }
    DeltaPhiToMetReader::DeltaPhiToMetReader(const std::string& Particle, const std::string& met) :
        IParticleVariable(ParReaderStorage::GetInstance()->GetReader(Particle)),
        m_Met(met),
        m_Part_phi(nullptr),
        m_Met_phi(nullptr),
        m_Registered(false) {
        if (getParticle()) {
            m_Registered = ITreeVarReaderStorage::GetInstance()->Register(this);
            m_Part_phi = getParticle()->RetrieveVariable("phi");
        }
        m_Met_phi = ScalarVarReader<float>::GetReader(m_Met + "_phi");
    }
    bool DeltaPhiToMetReader::init(TTree* t) {
        if (!getParticle() || !m_Registered || m_Met.empty()) {
            Error("DeltaPhiToMetReader::init()", "Something went wrong during creation");
            return false;
        }
        return getParticle()->init(t) && m_Part_phi->init(t) && m_Met_phi->init(t);
    }
    std::string DeltaPhiToMetReader::name() const {
        return "DeltaPhi " + (getParticle() ? getParticle()->name() : "Fat Error") + " " + m_Met;
    }
    double DeltaPhiToMetReader::readEntry(size_t I) const {
        if (I >= getParticle()->Size()) {
            Warning("DeltaPhiToMetReader::readEntry()", "Index " + std::to_string(I) + " is out of range");
            return 10;
        }
        return xAOD::P4Helpers::deltaPhi(m_Part_phi->readEntry(I), m_Met_phi->read());
    }
    //#########################################################################################
    //                                  SumUpReader
    //#########################################################################################
    SumUpReader* SumUpReader::GetReader(const std::string& Name, ITreeVarReader* FirstReader, ITreeVarReader* SecondReader) {
        std::vector<ITreeVarReader*> Readers;
        if (FirstReader) Readers.push_back(FirstReader);
        if (SecondReader) Readers.push_back(SecondReader);

        return SumUpReader::GetReader(Name, Readers);
    }
    SumUpReader* SumUpReader::GetReader(const std::string& Name, const std::vector<ITreeVarReader*>& Readers) {
        ITreeVarReaderStorage* S = ITreeVarReaderStorage::GetInstance();
        if (!S->GetReader(Name)) new SumUpReader(Name, Readers);
        return dynamic_cast<SumUpReader*>(S->GetReader(Name));
    }
    SumUpReader::SumUpReader(const std::string& Name, const std::vector<ITreeVarReader*>& Readers) :
        m_Name(Name),
        m_Sums(),
        m_EventNumber(-1),
        m_Cache(-1.e25) {
        CopyVector(Readers, m_Sums);
        if (!ITreeVarReaderStorage::GetInstance()->Register(this)) m_Sums.clear();
    }
    std::string SumUpReader::name() const { return m_Name; }
    bool SumUpReader::init(TTree* t) {
        m_EventNumber = -1;
        if (m_Sums.empty()) {
            Error("SumUpReader::init()", "There is nothing to sum up in " + name());
            return false;
        }
        for (auto& S : m_Sums) {
            if (!S->init(t)) { return false; }
        }
        reset();
        return true;
    }
    double SumUpReader::SumUp(ITreeVarReader* R) const {
        if (R == nullptr) return 0;
        double S = 0;
        for (size_t i = 0; i < R->entries(); ++i) S += R->readEntry(i);
        return S;
    }
    double SumUpReader::read() const {
        if (XAMPP::Weight::getWeighter()->eventNumber() != m_EventNumber) {
            m_EventNumber = XAMPP::Weight::getWeighter()->eventNumber();
            m_Cache = 0;
            for (auto& S : m_Sums) { m_Cache += SumUp(S); }
        }
        return m_Cache;
    }
    //#########################################################################################
    //                                  PileUpReader
    //#########################################################################################
    double PileUpReader::read() const {
        static SG::AuxElement::ConstAccessor<float> dec_CaX("corrected_averageInteractionsPerCrossing");

        if (m_UsePRWTool) {
            if (m_Weight->isData()) {
                m_Weight->UpdateEventInfo();
                return dec_CaX(*m_Weight->GetInfo());
            } else {
                return m_Weight->GetInfo()->averageInteractionsPerCrossing();
            }
        }
        return m_Reader->read();
    }
    bool PileUpReader::init(TTree* t) {
        if (!m_Reader) {
            m_UsePRWTool = false;
            if (!m_UsePRWTool) {
                if (!m_Weight->isData())
                    m_Reader = ScalarVarReader<float>::GetReader("averageInteractionsPerCrossing");
                else
                    m_Reader = ScalarVarReader<float>::GetReader("corr_avgIntPerX");
            } else
                return true;
        }
        if (m_Reader && !m_Reader->init(t)) return false;
        reset();
        return true;
    }
    std::string PileUpReader::name() const { return "Pile Up"; }
    PileUpReader::PileUpReader() : IScalarReader(), m_Reader(nullptr), m_Weight(Weight::getWeighter()), m_UsePRWTool(false) {
        ITreeVarReaderStorage::GetInstance()->Register(this);
    }
    //#########################################################################################################################
    //                                          L1CaloReader
    //#########################################################################################################################
    L1CaloReader::L1CaloReader(ParticleReader* Jets) :
        IParticleVariable(Jets),
        m_EMFrac(getParticle() ? getParticle()->RetrieveVariable("EMFrac") : nullptr),
        m_HECFrac(getParticle() ? getParticle()->RetrieveVariable("HECFrac") : nullptr) {
        ITreeVarReaderStorage::GetInstance()->Register(this);
    }
    std::string L1CaloReader::name() const { return "L1Calo " + (getParticle() ? getParticle()->name() : " Fat error"); }
    bool L1CaloReader::init(TTree* t) {
        if (!m_EMFrac) {
            Error("L1CaloReader::init()", "No electromagnetic fraction");
            return false;
        }
        if (!m_HECFrac) {
            Error("L1CaloReader::init()", "No hadronic fraction");
            return false;
        }
        if (!m_EMFrac->init(t) || !m_HECFrac->init(t)) { return false; }
        return true;
    }
    IParticleVarReader* L1CaloReader::GetReader(const std::string& Name) {
        ParticleReader* Jet = ParticleReader::GetReader(Name);
        if (!Jet) {
            Error("L1CaloReader::GetReader()", "No jets found called " + Name);
            return nullptr;
        }
        if (!ITreeVarReaderStorage::GetInstance()->GetReader("L1Calo " + Name)) { return new L1CaloReader(Jet); }
        return dynamic_cast<IParticleVarReader*>(ITreeVarReaderStorage::GetInstance()->GetReader("L1Calo " + Name));
    }
    double L1CaloReader::readEntry(size_t I) const {
        if (I >= getParticle()->Size()) {
            Warning("L1CaloReader::readEntry()", "Index " + std::to_string(I) + " is out of range");
            return 1.e25;
        }
        return getParticle()->GetP4(I).E() * (1. - m_EMFrac->readEntry(I) - m_HECFrac->readEntry(I));
    }
    //#########################################################################################################################
    //                                          MtMetReader
    //#########################################################################################################################
    bool MtMetReader::init(TTree* t) {
        if (!m_Registered || !getParticle()) {
            Error("MtMetReader::init()", "Something went wrong during creation");
            return false;
        }
        if (!m_par_pt->init(t) || !m_par_phi->init(t)) return false;
        if (!m_met_phi->init(t) || !m_met_value->init(t)) return false;
        return true;
    }
    std::string MtMetReader::name() const { return "Mt " + m_met + " " + getParticle()->name(); }
    double MtMetReader::readEntry(size_t I) const {
        if (I >= getParticle()->Size()) {
            Warning("MtMetReader::readEntry()", "The index " + std::to_string(I) + " given to " + name() + " is out of range.");
            return 1.e25;
        }
        double dPhi = xAOD::P4Helpers::deltaPhi(m_par_phi->readEntry(I), m_met_phi->read());
        return TMath::Sqrt(2 * m_par_pt->readEntry(I) * m_met_value->read() * (1 - TMath::Cos(dPhi)));
    }
    MtMetReader::MtMetReader(const std::string& Particle, const std::string& Met) :
        IParticleVariable(ParReaderStorage::GetInstance()->GetReader(Particle)),
        m_par_pt(nullptr),
        m_par_phi(nullptr),
        m_met(Met),
        m_met_value(ReaderProvider::GetInstance()->CreateReader("EvReader floatGeV " + Met + "_met")),
        m_met_phi(ReaderProvider::GetInstance()->CreateReader("EvReader float " + Met + "_phi")),
        m_Registered(false) {
        if (getParticle()) {
            m_Registered = ITreeVarReaderStorage::GetInstance()->Register(this);
            m_par_pt = getParticle()->RetrieveVariable("pt");
            m_par_phi = getParticle()->RetrieveVariable("phi");
        } else
            Error("MtMetReader()", "The particle " + Particle + " has not yet been defined");
    }
    IParticleVarReader* MtMetReader::GetReader(const std::string& Particle, const std::string& Met) {
        std::string Name = "Mt " + Met + " " + Particle;
        if (!ITreeVarReaderStorage::GetInstance()->GetReader(Name)) return new MtMetReader(Particle, Met);
        return dynamic_cast<IParticleVarReader*>(ITreeVarReaderStorage::GetInstance()->GetReader(Name));
    }
}  // namespace XAMPP
