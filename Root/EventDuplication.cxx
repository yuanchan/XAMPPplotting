#include <XAMPPplotting/EventDuplication.h>
#include <XAMPPplotting/TreeVarReader.h>
namespace XAMPP {

    EventIndexer::EventIndexer(const std::string &Name, const std::string &TreeName, const std::vector<std::string> &Files) :
        Selector(Name, TreeName, Files),
        m_cache_stream(),
        m_event_number(nullptr),
        m_run_number(nullptr) {}

    bool EventIndexer::AnalyzeEvent() {
        if (!isData()) {
            Error("AnalyzeEvent()", "Indexing of Monte Carlo events is not very sensible");
            return false;
        }
        m_cache_stream << m_run_number->read() << " " << Form("%.f", m_event_number->read()) << std::endl;
        return true;
    }
    void EventIndexer::SetupReaders() {
        std::string cache_path = m_outFile->GetName();
        cache_path = cache_path.substr(0, cache_path.rfind(".")) + ".txt";
        m_cache_stream.open(cache_path);

        m_event_number = ScalarVarReader<unsigned long long>::GetReader("eventNumber");
        m_run_number = ScalarVarReader<unsigned int>::GetReader("runNumber");
        AppendReader(m_event_number);
        AppendReader(m_run_number);
    }

    void EventIndexer::WriteOutput() {
        std::set<std::pair<unsigned int, unsigned long long>> duplicated;
        std::ifstream inf, inf_look_up;
        unsigned int run(-1), run_look_up(-1);
        unsigned long long event(-1), event_look_up(-1);
        // Let's first close the cache file
        m_cache_stream.close();

        std::string cache_path = m_outFile->GetName();
        cache_path = cache_path.substr(0, cache_path.rfind(".")) + ".txt";

        inf.open(cache_path);
        inf_look_up.open(cache_path);
        std::string line;
        while (GetLine(inf, line)) {
            GetLine(inf_look_up, line);
            std::stringstream sstr(line);
            sstr >> run >> event;
            while ((run != run_look_up || event != event_look_up) && GetLine(inf_look_up, line)) {
                sstr = std::stringstream(line);
                sstr >> run_look_up >> event_look_up;
            }
            inf_look_up.clear();
            inf_look_up.seekg(inf.tellg(), inf_look_up.beg);
            // We've found a duplicated event... That's sooo saaad
            if (run == run_look_up && event == event_look_up) {
                Warning("EventIndexer()", Form("Event %llu is duplicated in %u", event, run));
                duplicated.insert(std::pair<unsigned int, unsigned long long>(event, run));
            }
        }
        inf.close();
        inf_look_up.close();

        // Finally skim the file
        m_cache_stream.open(cache_path);
        for (auto &dupl : duplicated) m_cache_stream << dupl.first << " " << dupl.second << std::endl;
        m_cache_stream.close();
    }

}  // namespace XAMPP
