#include <XAMPPplotting/Cuts.h>
#include <XAMPPplotting/TreeVarReader.h>
#include <XAMPPplotting/UtilityReader.h>

#include <XAMPPplotting/Weight.h>

#include <AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h>
#include <cstring>
#include <iostream>

namespace XAMPP {
    Condition::Condition() :
        m_Reader(nullptr),
        m_VectorReader(nullptr),
        m_MatrixReader(nullptr),
        m_hasScalarReader(false),
        m_hasVectorReader(false),
        m_hasMatrixReader(false),
        m_CutValue(0.),
        m_init(false) {}

    std::shared_ptr<Condition> Condition::Combine(std::shared_ptr<Condition> first, std::shared_ptr<Condition> second, Mode mode) {
        if (!first || !second) {
            Error("Condition::Combine()", "No valid cut was given");
            return std::shared_ptr<Condition>();
        } else if (first == second) {
            Warning("Condition::Combine()", "You want to combine me with myself");
            return first;
        } else if (mode == Condition::Mode::AND) {
            return std::make_shared<CompositeConditionAND>(first, second);
        } else if (mode == Condition::Mode::OR) {
            return std::make_shared<CompositeConditionOR>(first, second);
        }
        Error("Condition::Combine()", "Requested combination of cuts makes no sense.");

        return std::shared_ptr<Condition>();
    }
    Condition::~Condition() {}
    std::string Condition::reader_name() const {
        if (m_Reader) return m_Reader->name();
        if (m_VectorReader) return m_VectorReader->name();
        if (m_MatrixReader) return m_MatrixReader->name();
        Warning("Condition::reader_name()", "No reader provided");
        return "Unknown reader provided";
    }
    bool Condition::Condition::Init(ITreeVarReader* Reader, double Value) {
        if (m_init) return true;
        if (InitMatrix(dynamic_cast<ITreeMatrixReader*>(Reader), Value)) return true;
        if (InitVector(dynamic_cast<ITreeVectorReader*>(Reader), Value)) return true;
        m_Reader = Reader;
        m_CutValue = Value;
        m_hasScalarReader = (Reader != nullptr);
        if (m_hasScalarReader) m_init = true;
        return m_hasScalarReader;
    }
    bool Condition::InitVector(ITreeVectorReader* Vector, double Value) {
        if (m_init) return true;
        m_CutValue = Value;
        m_VectorReader = Vector;
        m_hasVectorReader = (Vector != nullptr);
        if (m_hasVectorReader) m_init = true;
        return m_hasVectorReader;
    }
    bool Condition::InitMatrix(ITreeMatrixReader* Matrix, double Value) {
        if (m_init) return true;
        m_CutValue = Value;
        m_MatrixReader = Matrix;
        m_hasMatrixReader = Matrix != nullptr;
        if (m_hasMatrixReader) m_init = true;
        return m_hasMatrixReader;
    }
    bool Condition::Pass() const {
        if (m_hasScalarReader) return PassScalar();
        if (m_hasVectorReader) {
            const size_t I =
                m_VectorReader->index() != reset_idx ? m_VectorReader->index() : (m_VectorReader->entries() == 1 ? 0 : reset_idx);
            return I < m_VectorReader->entries() && PassVector(I);
        }
        if (m_hasMatrixReader) {
            return m_MatrixReader->column_index() < m_MatrixReader->row_entries(m_MatrixReader->row_index()) &&
                   PassMatrix(m_MatrixReader->column_index(), m_MatrixReader->row_index());
        }
        Warning("Condition::Pass()", "No condition satisfied");
        return false;
    }
    //###############################################
    //          Composite condition                 #
    //###############################################
    CompositeCondition::CompositeCondition(std::shared_ptr<Condition> Cut1, std::shared_ptr<Condition> Cut2) :
        m_cut_1(Cut1),
        m_cut_2(Cut2) {}
    CompositeCondition::~CompositeCondition() {}

    //###############################################
    //          CompositeConditionAND               #
    //###############################################
    CompositeConditionAND::CompositeConditionAND(std::shared_ptr<Condition> Cut1, std::shared_ptr<Condition> Cut2) :
        CompositeCondition(Cut1, Cut2) {}
    bool CompositeConditionAND::Pass() const { return (m_cut_1->Pass() && m_cut_2->Pass()); }
    bool CompositeConditionAND::PassScalar() const { return (m_cut_1->PassScalar() && m_cut_2->PassScalar()); }
    bool CompositeConditionAND::PassVector(size_t entry) const { return (m_cut_1->PassVector(entry) && m_cut_2->PassVector(entry)); }
    bool CompositeConditionAND::PassMatrix(size_t column, size_t row) const {
        return (m_cut_1->PassMatrix(column, row) && m_cut_2->PassMatrix(column, row));
    }
    std::string CompositeConditionAND::name() const { return "( " + m_cut_1->name() + " && " + m_cut_2->name() + " )"; }

    //###############################################
    //          CompositeConditionOR                #
    //###############################################
    CompositeConditionOR::CompositeConditionOR(std::shared_ptr<Condition> Cut1, std::shared_ptr<Condition> Cut2) :
        CompositeCondition(Cut1, Cut2) {}
    bool CompositeConditionOR::Pass() const { return (m_cut_1->Pass() || m_cut_2->Pass()); }
    bool CompositeConditionOR::PassScalar() const { return (m_cut_1->PassScalar() || m_cut_2->PassScalar()); }
    bool CompositeConditionOR::PassVector(size_t entry) const { return (m_cut_1->PassVector(entry) || m_cut_2->PassVector(entry)); }
    bool CompositeConditionOR::PassMatrix(size_t column, size_t row) const {
        return (m_cut_1->PassMatrix(column, row) || m_cut_2->PassMatrix(column, row));
    }
    std::string CompositeConditionOR::name() const { return "( " + m_cut_1->name() + " || " + m_cut_2->name() + " )"; }

    //###############################################
    //              ConditionNOT                    #
    //###############################################
    ConditionNOT::ConditionNOT(std::shared_ptr<Condition> Cut) : CompositeCondition(Cut, Cut) {}
    bool ConditionNOT::Pass() const { return !m_cut_1->Pass(); }
    bool ConditionNOT::PassScalar() const { return !m_cut_1->PassScalar(); }
    bool ConditionNOT::PassVector(size_t entry) const { return !m_cut_1->PassVector(entry); }
    bool ConditionNOT::PassMatrix(size_t column, size_t row) const { return !m_cut_1->PassMatrix(column, row); }
    std::string ConditionNOT::name() const { return "!" + m_cut_1->name(); }

    //###############################################
    //              GreaterCondition                #
    //###############################################
    bool GreaterCondition::PassScalar() const { return m_Reader->read() > m_CutValue; }
    bool GreaterCondition::PassVector(size_t N) const { return m_VectorReader->readEntry(N) > m_CutValue; }
    bool GreaterCondition::PassMatrix(size_t I, size_t J) const { return m_MatrixReader->readMatrixEntry(I, J) > m_CutValue; }
    std::string GreaterCondition::name() const { return Form("%s > %.2f", reader_name().c_str(), m_CutValue); }
    //###############################################
    //          GreaterEqualCondition               #
    //###############################################
    bool GreaterEqualCondition::PassScalar() const { return m_Reader->read() >= m_CutValue; }
    bool GreaterEqualCondition::PassVector(size_t N) const { return m_VectorReader->readEntry(N) >= m_CutValue; }
    bool GreaterEqualCondition::PassMatrix(size_t I, size_t J) const { return m_MatrixReader->readMatrixEntry(I, J) >= m_CutValue; }
    std::string GreaterEqualCondition::name() const { return Form("%s >= %.2f", reader_name().c_str(), m_CutValue); }

    //###############################################
    //              EqualCondition                  #
    //###############################################
    bool EqualCondition::PassScalar() const { return m_Reader->read() == m_CutValue; }
    bool EqualCondition::PassVector(size_t N) const { return m_VectorReader->readEntry(N) == m_CutValue; }
    bool EqualCondition::PassMatrix(size_t I, size_t J) const { return m_MatrixReader->readMatrixEntry(I, J) == m_CutValue; }
    std::string EqualCondition::name() const { return Form("%s == %.2f", reader_name().c_str(), m_CutValue); }

    //###############################################
    //               LessCondition                  #
    //###############################################
    bool LessCondition::PassScalar() const { return m_Reader->read() < m_CutValue; }
    bool LessCondition::PassVector(size_t N) const { return m_VectorReader->readEntry(N) < m_CutValue; }
    bool LessCondition::PassMatrix(size_t I, size_t J) const { return m_MatrixReader->readMatrixEntry(I, J) < m_CutValue; }
    std::string LessCondition::name() const { return Form("%s < %.2f", reader_name().c_str(), m_CutValue); }

    //###############################################
    //            LessEqualCondition                #
    //###############################################
    bool LessEqualCondition::PassScalar() const { return m_Reader->read() <= m_CutValue; }
    bool LessEqualCondition::PassVector(size_t N) const { return m_VectorReader->readEntry(N) <= m_CutValue; }
    bool LessEqualCondition::PassMatrix(size_t I, size_t J) const { return m_MatrixReader->readMatrixEntry(I, J) <= m_CutValue; }
    std::string LessEqualCondition::name() const { return Form("%s <= %.2f", reader_name().c_str(), m_CutValue); }

    //###############################################
    //            NotEqualCondition                 #
    //###############################################
    bool NotEqualCondition::PassScalar() const { return m_Reader->read() != m_CutValue; }
    bool NotEqualCondition::PassVector(size_t N) const { return m_VectorReader->readEntry(N) != m_CutValue; }
    bool NotEqualCondition::PassMatrix(size_t I, size_t J) const { return m_MatrixReader->readMatrixEntry(I, J) != m_CutValue; }
    std::string NotEqualCondition::name() const { return Form("%s != %.2f", reader_name().c_str(), m_CutValue); }

    //###############################################
    //           AbsGreaterCondition                #
    //###############################################
    bool AbsGreaterCondition::PassScalar() const { return fabs(m_Reader->read()) > m_CutValue; }
    bool AbsGreaterCondition::PassVector(size_t N) const { return fabs(m_VectorReader->readEntry(N)) > m_CutValue; }
    bool AbsGreaterCondition::PassMatrix(size_t I, size_t J) const { return fabs(m_MatrixReader->readMatrixEntry(I, J)) > m_CutValue; }
    std::string AbsGreaterCondition::name() const { return Form("%s |>| %.2f", reader_name().c_str(), m_CutValue); }
    //###############################################
    //        AbsGreaterEqualCondition              #
    //###############################################
    bool AbsGreaterEqualCondition::PassScalar() const { return fabs(m_Reader->read()) >= m_CutValue; }
    bool AbsGreaterEqualCondition::PassVector(size_t N) const { return fabs(m_VectorReader->readEntry(N)) >= m_CutValue; }
    bool AbsGreaterEqualCondition::PassMatrix(size_t I, size_t J) const {
        return fabs(m_MatrixReader->readMatrixEntry(I, J)) >= m_CutValue;
    }
    std::string AbsGreaterEqualCondition::name() const { return Form("%s |>=| %.2f", reader_name().c_str(), m_CutValue); }

    //###############################################
    //           AbsEqualCondition                  #
    //###############################################
    bool AbsEqualCondition::PassScalar() const { return fabs(m_Reader->read()) == m_CutValue; }
    bool AbsEqualCondition::PassVector(size_t N) const { return fabs(m_VectorReader->readEntry(N)) == m_CutValue; }
    bool AbsEqualCondition::PassMatrix(size_t I, size_t J) const { return fabs(m_MatrixReader->readMatrixEntry(I, J)) == m_CutValue; }
    std::string AbsEqualCondition::name() const { return Form("%s |==| %.2f", reader_name().c_str(), m_CutValue); }

    //###############################################
    //            AbsLessCondition                  #
    //###############################################
    bool AbsLessCondition::PassScalar() const { return fabs(m_Reader->read()) < m_CutValue; }
    bool AbsLessCondition::PassVector(size_t N) const { return fabs(m_VectorReader->readEntry(N)) < m_CutValue; }
    bool AbsLessCondition::PassMatrix(size_t I, size_t J) const { return fabs(m_MatrixReader->readMatrixEntry(I, J)) < m_CutValue; }
    std::string AbsLessCondition::name() const { return Form("%s |<| %.2f", reader_name().c_str(), m_CutValue); }

    //###############################################
    //          AbsLessEqualCondition               #
    //###############################################
    bool AbsLessEqualCondition::PassScalar() const { return fabs(m_Reader->read()) <= m_CutValue; }
    bool AbsLessEqualCondition::PassVector(size_t N) const { return fabs(m_VectorReader->readEntry(N)) <= m_CutValue; }
    bool AbsLessEqualCondition::PassMatrix(size_t I, size_t J) const { return fabs(m_MatrixReader->readMatrixEntry(I, J)) <= m_CutValue; }
    std::string AbsLessEqualCondition::name() const { return Form("%s |<=| %.2f", reader_name().c_str(), m_CutValue); }

    //###############################################
    //            AbsNotEqualCondition              #
    //###############################################
    bool AbsNotEqualCondition::PassScalar() const { return fabs(m_Reader->read()) != m_CutValue; }
    bool AbsNotEqualCondition::PassVector(size_t N) const { return fabs(m_VectorReader->readEntry(N)) != m_CutValue; }
    bool AbsNotEqualCondition::PassMatrix(size_t I, size_t J) const { return fabs(m_MatrixReader->readMatrixEntry(I, J)) != m_CutValue; }
    std::string AbsNotEqualCondition::name() const { return Form("%s |!=| %.2f", reader_name().c_str(), m_CutValue); }
    //###########################################################################################
    //                      GRLCondition
    //###########################################################################################
    GRLCondition::GRLCondition(const std::vector<std::string>& GRLs) : m_init(false), m_grl("GRLTool") {
        m_grl.setTypeAndName("GoodRunsListSelectionTool/GoodRunsListSelectionTool");
        if (!m_grl.setProperty("PassThrough", false).isSuccess()) {
            Error("GRLCondition()", "Failed to set PassThrough property");
        } else if (GRLs.empty() || !m_grl.setProperty("GoodRunsListVec", GetPathResolvedFileList(GRLs)).isSuccess()) {
            Error("GRLCondition()", "Failed to set the GRL");
        } else if (!m_grl->initialize().isSuccess()) {
            Error("GRLCondition", "Failed to initialize the GRL tool");
        } else
            m_init = true;
        Weight::getWeighter()->SetupEventInfo();
    }
    bool GRLCondition::Pass() const {
        if (!m_init) {
            Error("GRLCondition::Pass()", "GRL tool was not properly setup. Please check");
            return false;
        }
        const xAOD::EventInfo* info = Weight::getWeighter()->GetInfo();
        if (!info) return false;
        return m_grl->passRunLB(*info);
    }
    std::string GRLCondition::name() const { return "Pass GRL"; }
    GRLCondition::~GRLCondition() {}

    //###########################################################################################
    //                      UniqueEventCondition
    //###########################################################################################
    UniqueEventCondition::UniqueEventCondition(const std::string& duplicate_list) :
        Condition(),
        m_weighter(Weight::getWeighter()),
        m_known_ev(),
        m_ev_with_duplication(),
        m_ev_in_tree(-1),
        m_ev_decision(false) {
        m_weighter->SetupEventInfo(true);
        load_duplicated_list(duplicate_list);
    }
    bool UniqueEventCondition::load_duplicated_list(const std::string& duplicate_list) {
        if (duplicate_list.empty()) return false;
        std::ifstream inf(ResolvePath(duplicate_list));
        if (!inf.good()) return false;
        std::string line;
        while (GetLine(inf, line)) {
            unsigned int run(-1);
            unsigned long long ev(-1);
            std::stringstream sstr(line);
            sstr >> run >> ev;
            std::shared_ptr<EventSeen> new_ev = std::make_shared<EventSeen>(run, ev);
            if (!find_event(run, ev)) m_ev_with_duplication.push_back(new_ev);
        }
        return true;
    }
    UniqueEventCondition::EventSeen* UniqueEventCondition::find_event(const unsigned int run, const unsigned long long event) const {
        std::vector<std::shared_ptr<EventSeen>>::const_iterator itr = std::find_if(
            m_ev_with_duplication.begin(), m_ev_with_duplication.end(),
            [run, event](const std::shared_ptr<EventSeen>& dupl) { return dupl->run_number() == run && dupl->event_number() == event; });
        if (itr != m_ev_with_duplication.end()) return (*itr).get();
        return nullptr;
    }

    bool UniqueEventCondition::Pass() const {
        if (!m_weighter->isData()) return true;
        if (m_ev_in_tree != m_weighter->eventNumber()) {
            m_ev_in_tree = m_weighter->eventNumber();
            m_weighter->UpdateEventInfo();
            const xAOD::EventInfo* ev_info = m_weighter->GetInfo();
            if (m_ev_with_duplication.empty()) {
                m_ev_decision =
                    m_known_ev.insert(std::pair<unsigned int, unsigned long long>(ev_info->runNumber(), ev_info->eventNumber())).second;
            } else {
                EventSeen* seen = find_event(ev_info->runNumber(), ev_info->eventNumber());
                m_ev_decision = (seen == nullptr) || !seen->is_duplicate();
            }
            if (!m_ev_decision)
                Warning("UniqueEventCondition()",
                        Form("The event %llu in run %u is duplicated", ev_info->eventNumber(), ev_info->runNumber()));
        }
        return m_ev_decision;
    }
    std::string UniqueEventCondition::name() const { return "Remove duplicated events"; }

    UniqueEventCondition::EventSeen::EventSeen(unsigned int run, unsigned long long event) : m_run(run), m_event(event), m_seen(false) {}
    unsigned int UniqueEventCondition::EventSeen::run_number() const { return m_run; }
    unsigned long long UniqueEventCondition::EventSeen::event_number() const { return m_event; }

    bool UniqueEventCondition::EventSeen::is_duplicate() {
        if (m_seen) return true;
        m_seen = true;
        return false;
    }

}  // namespace XAMPP
