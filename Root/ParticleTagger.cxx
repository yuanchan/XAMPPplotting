#include <XAMPPplotting/ObservableReader.h>
#include <XAMPPplotting/ParticleTagger.h>
#include <XAMPPplotting/ReconstructedParticleReaders.h>
#include <XAMPPplotting/TreeVarReader.h>

#include <XAMPPplotting/Cuts.h>
#include <XAMPPplotting/PlottingUtils.h>
#include <XAMPPplotting/ReaderProvider.h>
#include <XAMPPplotting/Weight.h>

namespace XAMPP {

    //########################################################################
    //                      IParticleTagger
    //########################################################################
    IParticleTagger::IParticleTagger(IParticleReader* P, unsigned int pos_inContainer, unsigned int tagger_ID,
                                     std::shared_ptr<Condition> C) :
        m_Particle(P),
        m_ParticleIdx(pos_inContainer),
        m_Cut(C),
        m_TreeIdx(-1),
        m_Id(tagger_ID),
        m_Tagged(false) {}
    IParticleTagger::IParticleTagger(IParticleReader* P, unsigned int pos_inContainer, const std::string& tagger_name,
                                     std::shared_ptr<Condition> C) :
        IParticleTagger(P, pos_inContainer, std::hash<std::string>()(tagger_name), C) {}
    IParticleTagger::~IParticleTagger() {}
    void IParticleTagger::newEvent() {
        unsigned int Idx = m_Particle->Index();
        m_Tagged = false;
        if (m_Particle->Size() < particle_index()) return;
        m_Tagged = m_Particle->At(particle_index()) && (!m_Cut || m_Cut->Pass());
        if (m_Tagged)
            m_TreeIdx = m_Particle->ReaderIndex(particle_index());
        else
            m_TreeIdx = -1;
        if (particle_index() != Idx) m_Particle->At(Idx);
    }
    bool IParticleTagger::isTagged() const { return m_Tagged; }
    bool IParticleTagger::isTagged(IParticleReader* P) const {
        return (m_Tagged && hash() == P->GetCollectionHash() && m_TreeIdx == P->ReaderIndex());
    }
    bool IParticleTagger::isTagged(IParticleReader* P, size_t particle_index) const {
        if (!m_Tagged) return false;
        if (hash() != P->GetCollectionHash()) return false;
        if (m_TreeIdx != P->ReaderIndex(particle_index)) return false;
        return true;
    }
    unsigned int IParticleTagger::particle_index() const { return m_ParticleIdx; }
    unsigned int IParticleTagger::tree_index() const { return m_TreeIdx; }
    unsigned int IParticleTagger::hash() const { return m_Particle->GetCollectionHash(); }
    std::string IParticleTagger::name() const { return m_Particle->name(); }
    int IParticleTagger::taggerID() const { return m_Id; }
    IParticleReader* IParticleTagger::Particle() const { return m_Particle; }
    std::shared_ptr<Condition> IParticleTagger::Cut() const { return m_Cut; }

    std::shared_ptr<IParticleTagger> IParticleTagger::Clone(unsigned target_index) {
        if (target_index == particle_index()) return ParticleTaggerManager::Instance()->smartPointer(this);
        std::shared_ptr<IParticleTagger> clone = std::make_shared<IParticleTagger>(Particle(), target_index, taggerID(), Cut());
        ParticleTaggerManager::Instance()->Register(clone);
        // This method cleans up the store from duplicates tagging the same particle
        ParticleTaggerManager::Instance()->clearStore();
        return clone;
    }

    //########################################
    //      ParticleTaggerCut
    //########################################
    bool ParticleTaggerCut::Pass() const { return PassVector(m_particle->Index()); }
    bool ParticleTaggerCut::loadTaggers() const {
        if (!m_taggers_loaded) {
            m_taggers = m_manager->findTaggers(m_particle->GetCollectionHash(), m_tagger_hash);
            m_taggers_loaded = true;
        }
        m_manager->TagParticles();
        return m_taggers != nullptr;
    }

    bool ParticleTaggerCut::PassScalar() const {
        if (!loadTaggers()) return false;
        m_manager->TagParticles();
        for (auto& Tag : *m_taggers) {
            if (Tag->isTagged(m_particle)) return true;
        }
        return false;
    }
    bool ParticleTaggerCut::PassVector(size_t entry) const {
        if (!loadTaggers()) return false;
        for (auto& Tag : *m_taggers) {
            if (Tag->isTagged(m_particle, entry)) return true;
        }
        return false;
    }
    bool ParticleTaggerCut::PassMatrix(size_t, size_t) const {
        Warning("ParticleTaggerCut::PassMatrix()", "This not makes much sense to do open a 2D thread");
        return false;
    }

    std::string ParticleTaggerCut::name() const { return std::string("Is ") + reader_name() + std::string(" tagged by ") + m_tagger_name; }
    ParticleTaggerCut::ParticleTaggerCut(IParticleReader* reader, const std::string tagger_name) :
        Condition(),
        m_manager(ParticleTaggerManager::Instance()),
        m_particle(reader),
        m_tagger_name(tagger_name),
        m_tagger_hash(std::hash<std::string>()(m_tagger_name)),
        m_taggers_loaded(false),
        m_taggers(nullptr) {
        Init(reader, FLT_MAX);
    }
    //#########################################################################################
    //                                  ContainerTaggerCut
    //#########################################################################################
    ContainerTaggerCut::ContainerTaggerCut(IParticleReader* reader, const std::string& tagger_name) :
        ParticleTaggerCut(reader, tagger_name),
        m_max_tagger(nullptr) {}
    bool ContainerTaggerCut::PassVector(size_t entry) const {
        if (!loadTaggers()) return false;
        unsigned int container_size = m_particle->Size();
        if (container_size <= entry) return false;
        // Find the maximum tagged particle in the current container
        if (m_max_tagger == nullptr) {
            for (auto& tag : *m_taggers) {
                if (tag->Particle() == m_particle) {
                    m_max_tagger = tag;
                    m_max_tagger = FillUp(0, entry);
                    break;
                }
            }
        }
        //  The lines above are the easy case where the container used to tag is the same
        //  as the one in the container cut. What if an other container is used in the tagger_system
        if (m_max_tagger == nullptr) {
            for (auto& tag : *m_taggers) {
                // Nothing useful can be extracted if the tagger did not fire in the event
                if (!tag->isTagged()) continue;
                m_max_tagger = tag;
                m_max_tagger = FillUp(0, tag->Particle()->Size());
                break;
            }
        }
        // If the event showed a valid tagger we need to check if the
        // ContainerTaggerCut still covers the entire container or if new
        // taggers need to be added
        if (m_max_tagger) {
            unsigned int TreeIdxAtEntry = m_particle->ReaderIndex(entry);
            // Easy case. Container - tagger particle and the cut particle coincide. So we need just to check if
            // the container from the last events was bigger than the current one and extend the container if needed
            if (m_max_tagger->Particle() == m_particle && m_max_tagger->particle_index() <= entry) {
                m_max_tagger = FillUp(m_max_tagger->particle_index(), entry + 1);
            }
            //  Here begins the fun. Both collections do not match. A priori we can only tell that collection A might overlap with
            //  collection B, but it does not need to.
            else if (m_max_tagger->Particle() != m_particle) {
                unsigned int tag_container_size = m_max_tagger->Particle()->Size();
                // Check if the current one is tagged first
                // The maximum we have is not enough for our purposes
                if (m_max_tagger->isTagged() && m_max_tagger->tree_index() < TreeIdxAtEntry &&
                    m_max_tagger->particle_index() != tag_container_size - 1) {
                    m_max_tagger = FillUp(m_max_tagger->particle_index(), tag_container_size);
                }
                // There is still room left upwards. Which is the only hope to tag the particle
                else if (m_max_tagger->particle_index() < tag_container_size - 1) {
                    m_max_tagger = FillUp(m_max_tagger->particle_index(), tag_container_size);
                }
            }
        } else
            return false;
        return ParticleTaggerCut::PassVector(entry);
    }
    std::shared_ptr<IParticleTagger> ContainerTaggerCut::FillUp(int begin, int end) const {
        std::shared_ptr<IParticleTagger> ptr = m_max_tagger;
        for (int m = begin; m < end; ++m) { ptr = m_max_tagger->Clone(m); }
        return ptr;
    }
    //#########################################################################################
    //                                  ParticleTaggerManager
    //#########################################################################################
    ParticleTaggerManager* ParticleTaggerManager::m_Inst = nullptr;
    ParticleTaggerManager* ParticleTaggerManager::Instance() {
        if (!m_Inst) m_Inst = new ParticleTaggerManager();
        return m_Inst;
    }
    ParticleTaggerManager::ParticleTaggerManager() :
        ReadLine(),
        m_CurrentEvent(-1),
        m_SampleName(),
        m_SampleHash(-1),
        m_TaggerStore(),
        m_Taggers() {}
    ParticleTaggerManager::~ParticleTaggerManager() { m_Inst = nullptr; }
    void ParticleTaggerManager::TagParticles() {
        // The event has already been updated. No need to retag everything again
        if (m_CurrentEvent == Weight::getWeighter()->eventNumber()) return;
        m_CurrentEvent = Weight::getWeighter()->eventNumber();
        // Tell each IParticleTagger that a new event is loaded and that it should check
        // whether the assigned particle satsfies the tagging criteria or not
        for (auto& Tag : m_TaggerStore) { Tag->newEvent(); }
    }
    const TaggerVector* ParticleTaggerManager::findTaggers(unsigned int particle_hash, unsigned int tagger_ID) const {
        // The tagger cut retrieves once from the ParticleTaggerManager the pointer to the list of
        // suitable taggers
        std::map<unsigned int, TaggerID_Map>::const_iterator Itr = m_Taggers.find(particle_hash);
        if (Itr == m_Taggers.end()) return nullptr;
        TaggerID_Map::const_iterator Tagger_Itr = Itr->second.find(tagger_ID);
        if (Tagger_Itr == Itr->second.end()) return nullptr;
        return &(Tagger_Itr->second);
    }
    std::shared_ptr<IParticleTagger> ParticleTaggerManager::smartPointer(std::shared_ptr<IParticleTagger> tagger) {
        TaggerVector::const_iterator itr = std::find(m_TaggerStore.begin(), m_TaggerStore.end(), tagger);
        if (itr == m_TaggerStore.end()) {
            m_TaggerStore.push_back(tagger);
            Info("ParticleTaggerManager::smartPointer()", "Add new tagger with ID " + std::to_string(tagger->taggerID()) + " " +
                                                              tagger->name() + " taggging " + std::to_string(tagger->particle_index()) +
                                                              ".");
        }
        return tagger;
    }
    std::shared_ptr<IParticleTagger> ParticleTaggerManager::smartPointer(IParticleTagger* tagger) const {
        TaggerVector::const_iterator itr = std::find_if(m_TaggerStore.begin(), m_TaggerStore.end(),
                                                        [tagger](const std::shared_ptr<IParticleTagger>& a) { return a.get() == tagger; });
        if (itr != m_TaggerStore.end()) return *itr;
        return std::shared_ptr<IParticleTagger>();
    }
    void ParticleTaggerManager::Register(std::shared_ptr<IParticleTagger> T) {
        if (!T) {
            Error("ParticleTaggerManager::Register()", "No IParticleTagger was parsed");
            return;
        }
        std::shared_ptr<IParticleTagger> smart_tagger = smartPointer(T);
        if (isTaggerInList(m_TaggerStore, smart_tagger)) { return; }
        m_CurrentEvent = -1;

        DiParticleReader::setHashRegMode(true);
        unsigned int Hash = T->hash();
        // DiParticleReader are composed of at least two
        // particles which themselves could also be DiParticleReaders
        // the collection hash depends on whether particle a is in position i or particle b
        // which is unknown apriori. But the total set of hashes is constrained from the beginning
        // Retrieve the hash as long as the DiParticleReaders spits out diffrent hashes per call
        // For the ordinary readers only loop once over the collection
        while (Hash) {
            std::map<unsigned int, TaggerID_Map>::iterator Itr = m_Taggers.find(Hash);
            if (Itr == m_Taggers.end()) {
                m_Taggers.insert(std::pair<unsigned int, TaggerID_Map>(Hash, TaggerID_Map()));
                Itr = m_Taggers.find(Hash);
            }
            TaggerID_Map::iterator ID_Itr = Itr->second.find(T->taggerID());
            if (ID_Itr == Itr->second.end()) {
                Itr->second.insert(std::pair<unsigned int, TaggerVector>(T->taggerID(), TaggerVector()));
                ID_Itr = Itr->second.find(T->taggerID());
            }
            if (isTaggerInList(ID_Itr->second, smart_tagger)) {
                Warning("ParticleTaggerManager::Register()", "The tagger for the " + std::to_string(smart_tagger->particle_index()) +
                                                                 "-th " + smart_tagger->name() + " has already been added");
            } else {
                Info("ParticleTaggerManager::Register()", "Added new tagger for the " + std::to_string(smart_tagger->particle_index()) +
                                                              "-th " + T->name() + " using hash " + std::to_string(Hash) + ". ");
                ID_Itr->second.push_back(smart_tagger);
                std::sort(ID_Itr->second.begin(), ID_Itr->second.end(),
                          [](const std::shared_ptr<IParticleTagger>& a, const std::shared_ptr<IParticleTagger>& b) {
                              if (a->taggerID() < b->taggerID()) return true;
                              if (a->particle_index() < b->particle_index()) return true;
                              return false;
                          });
            }
            unsigned int CachedHash = Hash;
            Hash = T->hash();
            if (CachedHash == Hash) Hash = 0;
        }
        DiParticleReader::setHashRegMode(false);
    }
    bool ParticleTaggerManager::isTaggerInList(const TaggerVector& Taggers, std::shared_ptr<IParticleTagger> toTest) const {
        for (const auto& inList : Taggers) {
            if (toTest == inList) continue;
            if (inList->taggerID() != toTest->taggerID()) continue;
            if (inList->Particle() != toTest->Particle()) continue;
            if (inList->particle_index() != toTest->particle_index()) continue;
            if (inList->Cut() != toTest->Cut() && toTest->Cut()->name() != inList->Cut()->name()) continue;
            return true;
        }
        return false;
    }
    void ParticleTaggerManager::clearStore() {
        TaggerVector TempCache = m_TaggerStore;
        m_TaggerStore.clear();
        for (const auto& tagger : TempCache) {
            if (!isTaggerInList(m_TaggerStore, tagger)) m_TaggerStore.push_back(tagger);
        }
    }
    void ParticleTaggerManager::SetSampleName(const std::string& name) {
        if (m_SampleName.empty()) m_SampleName = name;
        m_SampleHash = std::hash<std::string>()(m_SampleName);
    }
    std::string ParticleTaggerManager::sampleName() const { return m_SampleName; }
    size_t ParticleTaggerManager::sampleHash() const { return m_SampleHash; }
    bool ParticleTaggerManager::AddCommonTagger(std::ifstream& ifstream, std::vector<ITreeVarReader*>& Readers) {
        std::string line(""), tagger_name(""), tag_sample("");
        IParticleReader* particle = nullptr;
        std::shared_ptr<Condition> cut;
        int N_th = -1;

        while (ReadLine(ifstream, line) == IfDefLineParser::LineStatus::NewProperty) {
            std::stringstream sstr(line);
            if (IsKeyWordSatisfied(sstr, "Particle"))
                particle = ParReaderStorage::GetInstance()->GetReader(GetWordFromStream(sstr));
            else if (IsKeyWordSatisfied(sstr, "CombCut")) {
                std::shared_ptr<Condition> comb_cut = CreateCombinedCut(ifstream, Readers, line);
                if (!comb_cut) break;
                cut = cut ? Condition::Combine(cut, comb_cut, Condition::AND) : comb_cut;
            } else if (IsInNextWord(sstr, "Cut")) {
                std::shared_ptr<Condition> new_cut = CreateCut(line, Readers);
                if (!new_cut) break;
                cut = cut ? Condition::Combine(cut, new_cut, Condition::AND) : new_cut;
            } else if (IsKeyWordSatisfied(sstr, "TaggerName"))
                tagger_name = GetWordFromStream(sstr);
            else if (IsKeyWordSatisfied(sstr, "Tag"))
                N_th = GetIntegerFromStream(sstr);
            else if (IsKeyWordSatisfied(sstr, "InSample"))
                tag_sample = GetWordFromStream(sstr);
            else if (IsKeyWordSatisfied(sstr, "EndTagger")) {
                if (tagger_name.empty())
                    Error("ParticleTaggerManager::AddTagger()", "Empty names are forbidden");
                else if (!particle)
                    Error("ParticleTaggerManager::AddTagger()", "Please provide a particle to tag in " + tagger_name);
                else if (N_th < 0)
                    Error("ParticleTaggerManager::AddTagger()",
                          "Which entry " + particle->name() + " do you want to tag in " + tagger_name);
                else {
                    if (!tag_sample.empty() && tag_sample != sampleName()) {
                        Info("ParticleTaggerManager::AddTagger()", "The tagger " + tagger_name + " for the " + std::to_string(N_th) +
                                                                       "-th " + particle->name() + " is only requested in " + tag_sample +
                                                                       ". While the current sample is " + sampleName());
                        return true;
                    }
                    Register(std::make_shared<IParticleTagger>(particle, N_th, tagger_name, cut));
                    return true;
                }
            }
        }
        Error("ParticleTaggerManager::AddTagger()", "Missing 'EndTagger' statement");
        return false;
    }
    //###########################################
    //          IsParticleTaggedReader          #
    //###########################################

    std::string IsParticleTaggedReader::name() const { return m_tagger_cut->name(); }
    double IsParticleTaggedReader::readEntry(const size_t I) const { return m_tagger_cut->PassVector(I); }
    IParticleVarReader* IsParticleTaggedReader::getReader(const std::string& particle, const std::string& tagger_name) {
        IParticleReader* part = ParReaderStorage::GetInstance()->GetReader(particle);
        if (!part) {
            Error("IsParticleTaggedReader::getReader()", "There is no particle " + particle);
            return nullptr;
        }
        std::shared_ptr<ParticleTaggerCut> cut = std::make_shared<ParticleTaggerCut>(part, tagger_name);
        if (!ITreeVarReaderStorage::GetInstance()->GetReader(cut->name())) return new IsParticleTaggedReader(part, cut);
        return dynamic_cast<IParticleVarReader*>(ITreeVarReaderStorage::GetInstance()->GetReader(cut->name()));
    }

    IsParticleTaggedReader::IsParticleTaggedReader(IParticleReader* part, std::shared_ptr<ParticleTaggerCut> tagger_cut) :
        IParticleVariable(part),
        m_tagger_cut(tagger_cut) {
        ITreeVarReaderStorage::GetInstance()->Register(this);
    }
    bool IsParticleTaggedReader::init(TTree* t) { return getParticle()->init(t) && m_tagger_cut->loadTaggers(); }

}  // namespace XAMPP
