#include <XAMPPplotting/DMXSecUpdater.h>
#include <XAMPPplotting/MetaDataTreeReader.h>
#include <XAMPPplotting/PlottingUtils.h>

#if ATHENA_RELEASE_SERIES == 212
#include <SUSYTools/SUSYCrossSection.h>
#endif
#include <PathResolver/PathResolver.h>

namespace XAMPP {
    //##############################################################################################################
    //                                        DMXSecUpdater
    //##############################################################################################################
    DMXSecUpdater::DMXSecUpdater() :
#ifdef ROOTCORE_PACKAGE_DMXSecUtils
        m_DMXSecTool(),
#endif
        m_isSet(false),
        m_coupling(1.),
        m_xSecDir("data/SUSYTools/mc15_13TeV/") {
#ifdef ROOTCORE_PACKAGE_DMXSecUtils
        m_DMXSecTool.Init();
#endif
    }
    void DMXSecUpdater::UpdateDMXSection() {
        if (!m_isSet) { return; }
#if ATHENA_RELEASE_SERIES == 212
        std::string mainDir = getenv("ROOTCOREBIN");
        std::string inDirXSec = mainDir + "/" + m_xSecDir + "/";
        XAMPP::Info("DMXSecUpdater::DMXSecUpdater()", "Load xSections from the DB located at " + inDirXSec);
        SUSY::CrossSectionDB xSecDB(inDirXSec);
        NormalizationDataBase* database = NormalizationDataBase::getDataBase();
        std::vector<unsigned int> DSIDs = database->GetListOfMCSamples();
        for (const auto DS : DSIDs) {
            std::vector<unsigned int> processIDs = database->GetListOfProcesses(DS);
            for (const auto P : processIDs) {
                float xSec = xSecDB.rawxsect(DS, P);
                XAMPP::Info("DMXSecUpdater::UpdateDMXSection()", "Trying to update xSection for DSID " + std::to_string(DS) +
                                                                     " with coupling g=" + std::to_string(m_coupling) + "...");
#ifdef ROOTCORE_PACKAGE_DMXSecUtils
                float DMXSec = m_DMXSecTool.Get_xsec_byDISD(DS, m_coupling);
                if (DMXSec == -1) {
                    XAMPP::Warning(
                        "DMXSecUpdater::UpdateDMXSection()",
                        "xSection for DSID " + std::to_string(DS) + " was not found in DMXSecTool, taking the one from SUSYTools instead!");
                } else
                    xSec = DMXSec;
#endif
                database->SetxSection(xSec, xSecDB.kfactor(DS, P), xSecDB.efficiency(DS, P), xSecDB.rel_uncertainty(DS, P), DS, P);
            }
        }
#endif
    }

}  // namespace XAMPP
