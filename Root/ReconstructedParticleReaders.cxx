#include <FourMomUtils/xAODP4Helpers.h>
#include <XAMPPplotting/PlottingUtils.h>
#include <XAMPPplotting/ReconstructedParticleReaders.h>
#include <XAMPPplotting/Weight.h>
#include <algorithm>
#include <stdexcept>

namespace XAMPP {
    //#########################################################################################
    //                                  DiParticleReader
    //#########################################################################################
    bool DiParticleReader::m_hashRegMode = false;
    void DiParticleReader::setHashRegMode(bool B) { m_hashRegMode = B; }
    DiParticleReader* DiParticleReader::GetReader(const std::string& Name, const std::string& First, const std::string& Second) {
        ParReaderStorage* S = ParReaderStorage::GetInstance();
        if (!S->GetReader(Name)) return new DiParticleReader(Name, First, Second);
        return dynamic_cast<DiParticleReader*>(S->GetReader(Name));
    }
    DiParticleReader::DiParticleReader(const std::string& Name, const std::string& First, const std::string& Second) :
        IParticleCollection(),
        m_Name(Name),
        m_FirstParticle(nullptr),
        m_SecondParticle(nullptr),
        m_FirstPt(nullptr),
        m_SecondPt(nullptr),
        m_Order(),
        m_CurrentEv(-1),
        m_Idx(-1),
        m_FirstCachedHash(0),
        m_SecondCachedHash(0) {
        ParReaderStorage* S = ParReaderStorage::GetInstance();
        m_FirstParticle = S->GetReader(First);
        m_SecondParticle = S->GetReader(Second);
        if (S->Register(this)) {
            if (m_FirstParticle)
                m_FirstPt = m_FirstParticle->RetrieveVariable("pt");
            else
                Error("DiParticleReader()", "The particle " + First + " does not exist.");
            if (m_SecondParticle)
                m_SecondPt = m_SecondParticle->RetrieveVariable("pt");
            else
                Error("DiParticleReader()", "The particle " + Second + " does not exist.");
        }
    }
    std::string DiParticleReader::name() const { return m_Name; }
    bool DiParticleReader::init(TTree* t) {
        if (!m_FirstPt || !m_SecondPt) {
            Error("DiParticleReader::init()", "One of the two pt branches could not be retrieved to build " + name());
            return false;
        }
        return m_FirstParticle->init(t) && m_SecondParticle->init(t);
    }
    void DiParticleReader::FillOrdering(std::vector<DiPartOrdering>& ordering, size_t N_part, bool isFirst) {
        ordering.reserve(ordering.capacity() + N_part);
        for (unsigned int i = 0; i < N_part; ++i) { ordering.push_back(DiPartOrdering(i, isFirst)); }
    }
    void DiParticleReader::OrderParticleStreams() {
        if (m_CurrentEv == Weight::getWeighter()->eventNumber()) { return; }
        m_CurrentEv = Weight::getWeighter()->eventNumber();
        m_Order.clear();
        // Count the indices of the particles
        FillOrdering(m_Order, m_FirstParticle->Size(), true);
        FillOrdering(m_Order, m_SecondParticle->Size(), false);
        if (m_FirstParticle->Size() > 0 && m_SecondParticle->Size() > 0) {
            std::sort(m_Order.begin(), m_Order.end(), [this](const DiPartOrdering& a, const DiPartOrdering& b) {
                double a_pt = a.UseFirstParticle ? m_FirstPt->readEntry(a.Idx) : m_SecondPt->readEntry(a.Idx);
                double b_pt = b.UseFirstParticle ? m_FirstPt->readEntry(b.Idx) : m_SecondPt->readEntry(b.Idx);
                return a_pt >= b_pt;
            });
        }
    }
    size_t DiParticleReader::Size() {
        OrderParticleStreams();
        return m_Order.size();
    }
    bool DiParticleReader::UseFirst(size_t N_th) const {
        if (N_th >= m_Order.size()) {
            Warning("DiParticleReader::UseFirst()", "There are less " + name() + " in the event than requested " + std::to_string(N_th));
            return false;
        }
        return (m_Order.begin() + N_th)->UseFirstParticle;
    }
    IParticleVarReader* DiParticleReader::RetrieveVariable(const std::string& Var) {
        return dynamic_cast<IParticleVarReader*>(DiParticleVarReader::GetReader(this, Var));
    }
    IParticleVarReader* DiParticleReader::RetrievePartialVariable(const std::string& Var, bool FirstPart) {
        if (FirstPart && m_FirstParticle)
            return m_FirstParticle->RetrieveVariable(Var);
        else if (m_SecondParticle)
            return m_SecondParticle->RetrieveVariable(Var);
        return nullptr;
    }
    const TLorentzVector& DiParticleReader::P4() const { return (UseFirst(Index()) ? m_FirstParticle->P4() : m_SecondParticle->P4()); }
    TLorentzVector DiParticleReader::GetP4(size_t E) {
        if (UseFirst(E)) return m_FirstParticle->GetP4(ComponentIndex(E));
        return m_SecondParticle->GetP4(ComponentIndex(E));
    }
    IParticleVarReader* DiParticleReader::RetrieveComponentReader() { return DiParticleComponentReader::GetReader(name()); }
    size_t DiParticleReader::ReaderIndex() const {
        UseFirst(Index());
        if (UseFirst(Index())) return m_FirstParticle->ReaderIndex();
        return m_SecondParticle->ReaderIndex();
    }
    size_t DiParticleReader::ComponentIndex() const { return ComponentIndex(m_Idx); }
    size_t DiParticleReader::ComponentIndex(size_t N_th) const {
        if (m_Order.size() <= N_th) {
            Warning("DiParticleReader::ComponentIndex()",
                    "Something strange happens. You want to access the " + std::to_string(N_th) + " particle. It does not exist");
            return -1;
        }
        return (m_Order.begin() + N_th)->Idx;
    }
    unsigned int DiParticleReader::GetCollectionHash() const {
        if (!m_hashRegMode) {
            if (UseFirst(Index())) { return m_FirstParticle->GetCollectionHash(); }
            return m_SecondParticle->GetCollectionHash();
        }
        unsigned int Hash = m_FirstParticle->GetCollectionHash();
        if (Hash != 0 && Hash != m_FirstCachedHash) {
            m_FirstCachedHash = Hash;
            return Hash;
        }
        Hash = m_SecondParticle->GetCollectionHash();
        if (Hash != 0 && Hash != m_SecondCachedHash) {
            m_SecondCachedHash = Hash;
            return Hash;
        }
        m_SecondCachedHash = 0;
        m_FirstCachedHash = 0;
        return 0;
    }
    size_t DiParticleReader::ReaderIndex(size_t P) {
        if (P >= Size()) {
            Error("DiParticle::ReaderIndex()", "Stop asking for something non-existent. ");
            return -1;
        }
        UseFirst(P);
        if (UseFirst(P)) return m_FirstParticle->ReaderIndex(ComponentIndex(P));
        return m_SecondParticle->ReaderIndex(ComponentIndex(P));
    }
    //#########################################################################################
    //                                  UnSortedDiParticleReader
    //#########################################################################################
    DiParticleReader* UnSortedDiParticleReader::GetReader(const std::string& Name, const std::string& First, const std::string& Second) {
        ParReaderStorage* S = ParReaderStorage::GetInstance();
        if (!S->GetReader(Name)) return new UnSortedDiParticleReader(Name, First, Second);
        return dynamic_cast<XAMPP::DiParticleReader*>(S->GetReader(Name));
    }
    UnSortedDiParticleReader::UnSortedDiParticleReader(const std::string& Name, const std::string& First, const std::string& Second) :
        DiParticleReader(Name, First, Second) {}
    void UnSortedDiParticleReader::OrderParticleStreams() {
        if (m_CurrentEv == Weight::getWeighter()->eventNumber()) { return; }
        m_CurrentEv = Weight::getWeighter()->eventNumber();
        m_Order.clear();
        // Save the current state of the readers
        FillOrdering(m_Order, m_FirstParticle->Size(), true);
        FillOrdering(m_Order, m_SecondParticle->Size(), false);
    }

    //#########################################################################################
    //                                  DiParticleVarReader
    //#########################################################################################
    IParticleVarReader* DiParticleVarReader::GetReader(DiParticleReader* DiReader, const std::string& VarName) {
        ITreeVarReaderStorage* S = ITreeVarReaderStorage::GetInstance();
        if (!S->GetReader(DiReader->name() + " " + VarName)) return new DiParticleVarReader(DiReader, VarName);
        return dynamic_cast<IParticleVarReader*>(S->GetReader(DiReader->name() + " " + VarName));
    }
    DiParticleVarReader::DiParticleVarReader(DiParticleReader* DiReader, const std::string& VarName) :
        IParticleVariable(DiReader),
        m_Particle(DiReader),
        m_FirVar(nullptr),
        m_SecVar(nullptr),
        m_VarName(VarName) {
        if (ITreeVarReaderStorage::GetInstance()->Register(this)) {
            m_FirVar = m_Particle->RetrievePartialVariable(VarName, true);
            m_SecVar = m_Particle->RetrievePartialVariable(VarName, false);
        }
    }
    bool DiParticleVarReader::init(TTree* t) {
        if (!m_FirVar || !m_SecVar) {
            Error("DiParticleVarReader::init()", "Could  not retrieve one of the two input branches.");
            return false;
        }
        return m_FirVar->init(t) && m_SecVar->init(t) && m_Particle->init(t);
    }
    std::string DiParticleVarReader::name() const { return m_Particle->name() + " " + m_VarName; }
    double DiParticleVarReader::readEntry(size_t I) const {
        if (I >= m_Particle->Size()) {
            Error("DiParticleVarReader::readEntry()", "Index " + std::to_string(I) + " is out of range for " + name() + "... Return 1.e25");
            return 1.e25;
        }
        if (m_Particle->UseFirst(I)) return m_FirVar->readEntry(m_Particle->ComponentIndex(I));
        return m_SecVar->readEntry(m_Particle->ComponentIndex(I));
    }
    //#########################################################################################
    //                                  DiPartCompReader
    //#########################################################################################
    IParticleVarReader* DiParticleComponentReader::GetReader(const std::string& Name) {
        ITreeVarReaderStorage* S = ITreeVarReaderStorage::GetInstance();
        if (!S->GetReader(Name + " Component")) return new DiParticleComponentReader(Name);
        return dynamic_cast<IParticleVarReader*>(S->GetReader(Name + " Component"));
    }
    DiParticleComponentReader::DiParticleComponentReader(const std::string& Name) :
        IParticleVariable(DiParticleReader::GetReader(Name)),
        m_DiPartReader(DiParticleReader::GetReader(Name)) {
        ITreeVarReaderStorage::GetInstance()->Register(this);
    }
    std::string DiParticleComponentReader::name() const { return m_DiPartReader->name() + " Component"; }
    bool DiParticleComponentReader::init(TTree* t) { return m_DiPartReader->init(t); }
    double DiParticleComponentReader::readEntry(size_t I) const {
        if (I >= m_DiPartReader->Size()) {
            Warning("DiParticleComponentReader::readEntry()", "No valid entry given");
            return 1.e25;
        }
        return m_DiPartReader->UseFirst(I);
    }
    //#########################################################################################
    //                                  ResortedReader
    //#########################################################################################
    ResortedParticleReader::ResortedParticleReader(const std::string& Particle, const std::string& Sorter, const std::string& Appendix) :
        IParticleCollection(),
        m_Name(Particle + (Appendix.empty() ? "" : "_" + Appendix) + "_" + Sorter),
        m_Particle(nullptr),
        m_Sorter(nullptr),
        m_Idx(0),
        m_Sorted(),
        m_EventNumber(-1),
        m_eventService(EventService::getService()) {
        ParReaderStorage* S = ParReaderStorage::GetInstance();
        m_Particle = S->GetReader(Particle);
        if (S->Register(this)) {
            m_Sorter = m_Particle->RetrieveVariable(Sorter);
            Info("ResortedReader::Register()", "Register new reader " + name() + " with sorter " + m_Sorter->name());
        }
    }
    std::string ResortedParticleReader::name() const { return m_Name; }
    bool ResortedParticleReader::init(TTree* t) {
        if (!m_Sorter) {
            Error("ResortedReader::init()", "Could not retrieve sorter for ResortedReader " + name());
            return false;
        }
        m_EventNumber = -1;
        return m_Sorter->init(t) && m_Particle->init(t);
    }
    size_t ResortedParticleReader::Size() {
        if (m_eventService->currentEvent() != m_EventNumber) {
            m_EventNumber = m_eventService->currentEvent();
            m_Sorted.clear();
            size_t N = m_Particle->Size();
            m_Sorted.reserve(m_Sorted.capacity() + N);
            for (size_t s = 0; s < N; ++s) m_Sorted.push_back(s);
            std::sort(m_Sorted.begin(), m_Sorted.end(), [this](const size_t a, size_t b) { return this->Sort(a, b); });
        }
        return m_Particle->Size();
    }
    size_t ResortedParticleReader::ReaderIndex() const { return m_Particle->ReaderIndex(); }
    size_t ResortedParticleReader::Permute(size_t Idx) {
        if (Size() <= Idx) return Idx;
        return m_Sorted.at(Idx);
    }
    unsigned int ResortedParticleReader::GetCollectionHash() const { return m_Particle->GetCollectionHash(); }
    IParticleVarReader* ResortedParticleReader::RetrieveVariable(const std::string& Var) {
        return dynamic_cast<IParticleVarReader*>(ResortedParticleVarReader::GetReader(this, Var));
    }
    IParticleVarReader* ResortedParticleReader::RetrieveBranchAccess(const std::string& Var) const {
        return m_Particle->RetrieveVariable(Var);
    }
    const TLorentzVector& ResortedParticleReader::P4() const { return m_Particle->P4(); }
    TLorentzVector ResortedParticleReader::GetP4(size_t E) {
        if (At(E)) return P4();
        return m_Particle->GetP4(E + 1000);
    }
    size_t ResortedParticleReader::ReaderIndex(size_t P) { return m_Particle->ReaderIndex(P); }

    //#########################################################################################
    //                                  ResortedParticleVarReader
    //#########################################################################################
    IParticleVarReader* ResortedParticleVarReader::GetReader(ResortedParticleReader* Reader, const std::string& Var) {
        ITreeVarReaderStorage* S = ITreeVarReaderStorage::GetInstance();
        if (!S->GetReader(Reader->name() + " " + Var)) new ResortedParticleVarReader(Reader, Var);
        return dynamic_cast<IParticleVarReader*>(S->GetReader(Reader->name() + " " + Var));
    }
    ResortedParticleVarReader::ResortedParticleVarReader(ResortedParticleReader* Reader, const std::string& Var) :
        IParticleVariable(Reader),
        m_Reader(Reader),
        m_VarName(Var),
        m_Var(nullptr) {
        if (ITreeVarReaderStorage::GetInstance()->Register(this)) m_Var = m_Reader->RetrieveBranchAccess(Var);
    }
    std::string ResortedParticleVarReader::name() const { return m_Reader->name() + " " + m_VarName; }
    bool ResortedParticleVarReader::init(TTree* t) {
        if (!m_Var) {
            Error("ResortedParticleVarReader::init()", "Could not find a Reader for " + m_VarName);
            return false;
        }
        return m_Var->init(t) && m_Reader->init(t);
    }
    double ResortedParticleVarReader::readEntry(size_t I) const {
        if (I >= m_Reader->Size()) {
            Error("ResortedParticleVarReader::readEntry()",
                  "Index " + std::to_string(I) + " is out of range for " + name() + "... Return 1.e25");
            return 1.e25;
        }
        return m_Var->readEntry(m_Reader->Permute(I));
    }
    //#########################################################################################
    //                                  ResortedParticleReaderDESC
    //#########################################################################################
    ResortedParticleReaderDESC* ResortedParticleReaderDESC::GetReader(const std::string& Name, const std::string& Sorter, float Ref) {
        ParReaderStorage* S = ParReaderStorage::GetInstance();
        if (!S->GetReader(Name + "_DESC_" + Sorter)) new ResortedParticleReaderDESC(Name, Sorter, Ref);
        return dynamic_cast<XAMPP::ResortedParticleReaderDESC*>(S->GetReader(Name + "_DESC_" + Sorter));
    }
    ResortedParticleReaderDESC::ResortedParticleReaderDESC(const std::string& Name, const std::string& Sorter, float Ref) :
        ResortedParticleReader(Name, Sorter, "DESC"),
        m_ReferenceValue(Ref) {}
    bool ResortedParticleReaderDESC::Sort(size_t Old, size_t New) const {
        return (fabs(m_Sorter->readEntry(New) - m_ReferenceValue) < fabs(m_Sorter->readEntry(Old) - m_ReferenceValue));
    }
    //#########################################################################################
    //                                  ResortedParticleReaderASC
    //#########################################################################################
    ResortedParticleReaderASC* ResortedParticleReaderASC::GetReader(const std::string& Name, const std::string& Sorter, float Ref) {
        ParReaderStorage* S = ParReaderStorage::GetInstance();
        if (!S->GetReader(Name + "_ASC" + "_" + Sorter)) return new ResortedParticleReaderASC(Name, Sorter, Ref);
        return dynamic_cast<XAMPP::ResortedParticleReaderASC*>(S->GetReader(Name + "_ASC" + "_" + Sorter));
    }
    ResortedParticleReaderASC::ResortedParticleReaderASC(const std::string& Name, const std::string& Sorter, float Ref) :
        ResortedParticleReader(Name, Sorter, "ASC"),
        m_ReferenceValue(Ref) {}
    bool ResortedParticleReaderASC::Sort(size_t Old, size_t New) const {
        return (fabs(m_Sorter->readEntry(New) - m_ReferenceValue) > fabs(m_Sorter->readEntry(Old) - m_ReferenceValue));
    }

    //#########################################################################################
    //                                  EventVarSortedParticleReader
    //#########################################################################################
    EventVarSortedParticleReader::EventVarSortedParticleReader(const std::string& Name, const std::string& Sorter, ITreeVarReader* EvVar,
                                                               const std::string& App) :
        ResortedParticleReader(Name, Sorter, (EvVar ? EvVar->name() : "") + App),
        m_EvSorter(EvVar) {}
    bool EventVarSortedParticleReader::init(TTree* t) {
        if (!ResortedParticleReader::init(t)) return false;
        if (!m_EvSorter) {
            Error("EventVarSortedParticleReader::init()", "No additional ITreeVarReader was given to " + name());
            return false;
        }
        return m_EvSorter->init(t);
    }
    //#########################################################################################
    //                              SortedMetParticleReader_ASC
    //#########################################################################################
    SortedMetParticleReader_ASC::SortedMetParticleReader_ASC(const std::string& Name, const std::string& Met) :
        EventVarSortedParticleReader(Name, "phi", ScalarVarReader<float>::GetReader(Met + "_phi"), "ASC") {}
    bool SortedMetParticleReader_ASC::Sort(size_t Old, size_t New) const {
        return fabs(xAOD::P4Helpers::deltaPhi(m_Sorter->readEntry(Old), m_EvSorter->read())) <
               fabs(xAOD::P4Helpers::deltaPhi(m_Sorter->readEntry(New), m_EvSorter->read()));
    }
    SortedMetParticleReader_ASC* SortedMetParticleReader_ASC::GetReader(const std::string Particle, const std::string& Met) {
        std::string Name = Particle + "_" + Met + "ASC_dPhi";
        ParReaderStorage* S = ParReaderStorage::GetInstance();
        if (!S->GetReader(Name)) return new SortedMetParticleReader_ASC(Particle, Met);
        return dynamic_cast<SortedMetParticleReader_ASC*>(S->GetReader(Name));
    }
    //#########################################################################################
    //                              SortedMetParticleReader_DESC
    //#########################################################################################
    SortedMetParticleReader_DESC::SortedMetParticleReader_DESC(const std::string& Name, const std::string& Met) :
        EventVarSortedParticleReader(Name, "phi", ScalarVarReader<float>::GetReader(Met + "_phi"), "DESC") {}
    bool SortedMetParticleReader_DESC::Sort(size_t Old, size_t New) const {
        return fabs(xAOD::P4Helpers::deltaPhi(m_Sorter->readEntry(Old), m_EvSorter->read())) >
               fabs(xAOD::P4Helpers::deltaPhi(m_Sorter->readEntry(New), m_EvSorter->read()));
    }
    SortedMetParticleReader_DESC* SortedMetParticleReader_DESC::GetReader(const std::string Particle, const std::string& Met) {
        std::string Name = Particle + "_" + Met + "DESC_dPhi";
        ParReaderStorage* S = ParReaderStorage::GetInstance();
        if (!S->GetReader(Name)) return new SortedMetParticleReader_DESC(Particle, Met);
        return dynamic_cast<SortedMetParticleReader_DESC*>(S->GetReader(Name));
    }

    //#########################################################################################
    //                              CombinedParticleReader
    //#########################################################################################
    CombinedParticleReader* CombinedParticleReader::GetReader(const std::string& Name, const std::string& First,
                                                              const std::string& Second) {
        ParReaderStorage* S = ParReaderStorage::GetInstance();
        if (!S->GetReader(Name)) return new CombinedParticleReader(Name, First, Second);
        return dynamic_cast<XAMPP::CombinedParticleReader*>(S->GetReader(Name));
    }
    CombinedParticleReader::CombinedParticleReader(const std::string& r_name, const std::string& first, const std::string& second) :
        m_name(r_name),
        m_first(ParReaderStorage::GetInstance()->GetReader(first)),
        m_second(ParReaderStorage::GetInstance()->GetReader(second)),
        m_hash(std::hash<std::string>()(r_name + first + second)),
        m_combinations(),
        m_weighter(Weight::getWeighter()),
        m_ev_number(-1) {
        ParReaderStorage::GetInstance()->Register(this);
    }
    IParticleReader* CombinedParticleReader::first_incoming() const { return m_first; }
    IParticleReader* CombinedParticleReader::second_incoming() const { return m_second; }
    std::string CombinedParticleReader::name() const { return m_name; }
    bool CombinedParticleReader::init(TTree* t) {
        if (!m_first) Error("CombinedParticleReader::init()", "The particle " + m_first->name() + " does not exist.");
        if (!m_second) Error("CombinedParticleReader::init()", "The particle " + m_second->name() + " does not exist.");
        return m_first->init(t) && m_second->init(t);
    }
    size_t CombinedParticleReader::Size() {
        if (m_ev_number != m_weighter->eventNumber()) {
            m_ev_number = m_weighter->eventNumber();
            m_combinations.clear();
            // No hope of combining the two afterwards
            if (m_first->Size() == 0 || m_second->Size() == 0) return 0;
            size_t needed = m_first->Size() * (m_first != m_second ? m_second->Size() : m_first->Size() - 1);
            if (m_combinations.capacity() < needed) m_combinations.reserve(needed);
            // construct all possible allowed combinations
            for (size_t i = 0; i < m_first->Size(); ++i) {
                for (size_t j = 0; j < (m_first != m_second ? m_second->Size() : i); ++j) {
                    if (m_first->GetCollectionHash() == m_second->GetCollectionHash() &&
                        m_first->ReaderIndex(i) == m_second->ReaderIndex(j))
                        continue;
                    m_combinations.push_back(std::make_shared<TensorCombination>(this, i, j));
                }
            }
            // sort after pt
            std::sort(m_combinations.begin(), m_combinations.end(),
                      [this](const std::shared_ptr<TensorCombination>& a, const std::shared_ptr<TensorCombination>& b) {
                          return a->P4().Perp2() < b->P4().Perp2();
                      });
        }
        return m_combinations.size();
    }
    size_t CombinedParticleReader::ReaderIndex() const { return Index(); }
    size_t CombinedParticleReader::ReaderIndex(size_t P) { return std::min(P, Size()); }
    unsigned int CombinedParticleReader::GetCollectionHash() const { return m_hash; }
    /// Actually this method is a dummy method
    const TLorentzVector& CombinedParticleReader::P4() const {
        static TLorentzVector V;
        return V;
    }
    TLorentzVector CombinedParticleReader::GetP4(size_t E) {
        if (E >= Size()) {
            Warning("CombinedParticleReader::GetP4()",
                    Form("Index %lu given to %s is out of range. Return empty vector", E, name().c_str()));
            return P4();
        }
        return m_combinations[E]->P4();
    }
    std::shared_ptr<TensorCombination> CombinedParticleReader::get_combination(size_t E) {
        if (E >= Size()) return std::shared_ptr<TensorCombination>();
        return m_combinations[E];
    }
    IParticleVarReader* CombinedParticleReader::RetrieveVariable(const std::string& Var) {
        static std::vector<std::string> p4_vars{"pt", "eta", "phi", "m", "e"};
        if (std::find(p4_vars.begin(), p4_vars.end(), Var) != p4_vars.end())
            return dynamic_cast<IParticleVarReader*>(CombinedP4VarReader::GetReader(this, Var));
        else {
            Error("CombinedParticleReader::RetrieveVariable()", "Variable " + Var + "not avaliable!");
            return nullptr;
        }
    }

    //#########################################################################################
    //                              TensorCombination
    //#########################################################################################

    const TLorentzVector& TensorCombination::P4() const { return m_p4; }
    size_t TensorCombination::first_idx() const { return m_first; }
    size_t TensorCombination::second_idx() const { return m_second; }

    TensorCombination::TensorCombination(CombinedParticleReader* ref, size_t i, size_t j) :
        m_p4(ref->first_incoming()->GetP4(i) + ref->second_incoming()->GetP4(j)),
        m_first(i),
        m_second(j) {}

    //#########################################################################################
    //                              CombinedP4VarReader
    //#########################################################################################
    IParticleVarReader* CombinedP4VarReader::GetReader(CombinedParticleReader* Reader, const std::string& Var) {
        ITreeVarReaderStorage* S = ITreeVarReaderStorage::GetInstance();
        if (!S->GetReader(Reader->name() + " " + Var)) new CombinedP4VarReader(Reader, Var);
        return dynamic_cast<IParticleVarReader*>(S->GetReader(Reader->name() + " " + Var));
    }
    CombinedP4VarReader::CombinedP4VarReader(CombinedParticleReader* Reader, const std::string& component) :
        IParticleVariable(Reader),
        m_Particle(Reader),
        m_VarName(component),
        m_func() {
        ITreeVarReaderStorage::GetInstance()->Register(this);
        if (component == "pt") {
            m_func = [this](size_t I) { return m_Particle->get_combination(I)->P4().Pt(); };
        } else if (component == "eta") {
            m_func = [this](size_t I) { return m_Particle->get_combination(I)->P4().Eta(); };

        } else if (component == "phi") {
            m_func = [this](size_t I) { return m_Particle->get_combination(I)->P4().Phi(); };

        } else if (component == "e") {
            m_func = [this](size_t I) { return m_Particle->get_combination(I)->P4().E(); };

        } else if (component == "m") {
            m_func = [this](size_t I) { return m_Particle->get_combination(I)->P4().M(); };
        } else {
            Error("CombinedP4VarReader::CombinedP4VarReader()", "Variable " + component + "not avaliable!");
            throw std::invalid_argument("Variable " + component + "not avaliable!");
        }
    }
    std::string CombinedP4VarReader::name() const { return m_Particle->name() + " " + m_VarName; }
    double CombinedP4VarReader::readEntry(size_t I) const {
        if (I >= entries()) {
            Error("CombinedP4VarReader::readEntry()", "Index " + std::to_string(I) + " is out of range for " + name() + "... Return 1.e25");
            return 1.e25;
        }
        return m_func(I);
    }

    bool CombinedP4VarReader::init(TTree* t) { return m_Particle->init(t); }

    //#########################################################################################
    //                              CombinedParticleVarReader
    //#########################################################################################
    double CombinedParticleVarReader::readEntry(size_t I) const {
        const size_t N = entries();
        if (I >= N) {
            Error("CombinedParticleVarReader::readEntry()",
                  "Index " + std::to_string(I) + " is out of range for " + name() + "... Return 1.e25");
            return 1.e25;
        }
        if (m_weighter->eventNumber() != m_evNumber) {
            m_evNumber = m_weighter->eventNumber();
            m_cached.clear();
            if (m_cached.capacity() < N) m_cached.reserve(N);

            for (size_t i = 0; i < N; ++i) {
                std::shared_ptr<TensorCombination> tensor = m_particle->get_combination(i);
                m_cached.push_back((*m_func)(tensor->first_idx(), tensor->second_idx()));
            }
        }
        return I;
    }
    std::string CombinedParticleVarReader::name() const { return MathITreeVarReader::reader_name(m_first_var, m_second_var, m_operator); }
    bool CombinedParticleVarReader::init(TTree* t) {
        if (!m_Registered) {
            Error("CombinedParticleVarReader::init()", "Something went wrong during the creation of " + name());
            return false;
        }
        if (!m_first_var || !m_first_var->init(t)) {
            Error("CombinedParticleVarReader::init()", "Failed to initialize the first variable building " + name());
            return false;
        }
        if (!m_second_var || !m_second_var->init(t)) {
            Error("CombinedParticleVarReader::init()", "Failed to initialize the second variable building " + name());
            return false;
        }
        if (!m_func) {
            Error("CombinewdParticleVarReader::init()", "No idea how to combine " + m_first_var->name() + " and " + m_second_var->name());
            return false;
        }
        m_evNumber = -1;
        m_cached.clear();
        return m_particle->init(t);
    }
    CombinedParticleVarReader::CombinedParticleVarReader(CombinedParticleReader* particle, const std::string& var,
                                                         MathITreeVarReader::ArithmetricOperators O) :
        IParticleVariable(particle),
        m_particle(particle),
        m_operator(O),
        m_first_var(particle->first_incoming()->RetrieveVariable(var)),
        m_second_var(particle->second_incoming()->RetrieveVariable(var)),
        m_Registered(false),
        m_func(nullptr),
        m_weighter(Weight::getWeighter()),
        m_evNumber(-1),
        m_cached() {
        m_Registered = ITreeVarReaderStorage::GetInstance()->Register(this);
    }
    //#################################################################
    //              OverlapRemovalParticleReader
    //#################################################################
    std::string OverlapRemovalParticleReader::name() const { return m_name; }
    bool OverlapRemovalParticleReader::init(TTree* t) {
        if (!m_from) {
            Error("OverlapRemovalParticleReader::init()", Form("No initial collection given to %s", name().c_str()));
            return false;
        } else if (!m_with) {
            Error("OverlapRemovalParticleReader::init()", Form("%s shall removed the overlap with what?", name().c_str()));
            return false;
        }
        if (m_overlap_idx_from && m_idx_with) {
            if (!m_overlap_idx_from->init(t) || !m_idx_with->init(t)) { return false; }
        } else if (m_dR > 0) {
            if (!m_close_dR || !m_close_dR->init(t)) { return false; }
        } else {
            Error("OverlapRemovalParticleReader::init()", "Invalid OR configuration");
            return false;
        }

        return m_from->init(t) && m_with->init(t);
    }

    size_t OverlapRemovalParticleReader::Size() {
        if (m_weighter->eventNumber() != m_last_cached_ev) {
            m_last_cached_ev = m_weighter->eventNumber();
            // Generate a list with N elements
            m_surviving = sequence(m_from->Size());

            // There might be some overlap
            if (m_with->Size() > 0 && m_from->Size() > 0) {
                // OR has already been performed in the n-tuple production
                // stage an saved in the trees based on the SG::AuxElement.index()
                // information which is unique for any particle in a collection

                if (m_overlap_idx_from && m_idx_with) {
                    // Extract the list of overlapping indices
                    std::vector<int> overlapping_idx;
                    overlapping_idx.reserve(m_with->Size());
                    for (size_t w = 0; w < m_with->Size(); ++w) { overlapping_idx.push_back(m_idx_with->readEntry(w)); }
                    std::sort(overlapping_idx.begin(), overlapping_idx.end());
                    // Perform the overlap removal
                    EraseFromVector<size_t>(m_surviving, [&overlapping_idx, this](const size_t& s) {
                        // Index associated with the OR particle usually >0
                        int remove_idx = m_overlap_idx_from->readEntry(s);

                        std::vector<int>::iterator itr = std::lower_bound(overlapping_idx.begin(), overlapping_idx.end(), remove_idx);

                        // There is a match... How large is the probability that the overlapping object kicks two in the
                        // same container. Remove this object as well to reduce the phase-space of possibilities
                        if (itr != overlapping_idx.end()) {
                            overlapping_idx.erase(itr);
                            return !m_keep_overlapping;
                        }
                        return m_keep_overlapping;
                    });
                } else {
                    // Not performed at n-tuples production stage. The OR is fully based on dR information
                    EraseFromVector<size_t>(m_surviving, [this](const size_t s) {
                        double dR = m_close_dR->readEntry(s);
                        if (dR > m_dR) return m_keep_overlapping;
                        return !m_keep_overlapping;
                    });
                }
            }
        }
        return m_surviving.size();
    }
    size_t OverlapRemovalParticleReader::removeOR(size_t I) {
        if (I >= Size()) {
            Warning("OverlapRemovalParticleReader::remmoveOR()", Form("Index %lu is out of range", I));
            return -1;
        }
        return m_surviving[I];
    }
    size_t OverlapRemovalParticleReader::ReaderIndex(size_t P) { return m_from->ReaderIndex(removeOR(P)); }

    unsigned int OverlapRemovalParticleReader::GetCollectionHash() const { return m_from->GetCollectionHash(); }
    TLorentzVector OverlapRemovalParticleReader::GetP4(size_t E) { return m_from->GetP4(removeOR(E)); }

    IParticleVarReader* OverlapRemovalParticleReader::RetrieveVariable(const std::string& Var) {
        return OverlapRemovalParticleVarReader::GetReader(this, Var);
    }

    OverlapRemovalParticleReader::OverlapRemovalParticleReader(const std::string& r_name, const std::string& rem_from,
                                                               const std::string& rem_with, bool keep_overlapping) :
        m_name(r_name),
        m_from(ParReaderStorage::GetInstance()->GetReader(rem_from)),
        m_with(ParReaderStorage::GetInstance()->GetReader(rem_with)),
        m_keep_overlapping(keep_overlapping),
        m_dR(-1),
        m_close_dR(nullptr),
        m_overlap_idx_from(nullptr),
        m_idx_with(nullptr),
        m_weighter(Weight::getWeighter()),
        m_last_cached_ev(-1),
        m_surviving(),
        m_registered(false) {
        m_registered = ParReaderStorage::GetInstance()->Register(this);
    }
    OverlapRemovalParticleReader::OverlapRemovalParticleReader(const std::string& r_name, const std::string& rem_from,
                                                               const std::string& rem_with, float dR, bool keep_overlapping) :
        OverlapRemovalParticleReader(r_name, rem_from, rem_with, keep_overlapping) {
        m_dR = dR;
        m_close_dR = ExtremumCorrelationReader::GetReader(DeltaRReader::GetReader(rem_from, rem_with), true);
    }
    OverlapRemovalParticleReader::OverlapRemovalParticleReader(const std::string& r_name, const std::string& rem_from,
                                                               const std::string& rem_with, const std::string& from_label,
                                                               const std::string& cont_idx, bool keep_overlapping) :
        OverlapRemovalParticleReader(r_name, rem_from, rem_with, keep_overlapping) {
        if (m_from) { m_overlap_idx_from = m_from->RetrieveVariable(from_label); }
        if (m_with) { m_idx_with = m_with->RetrieveVariable(cont_idx); }
    }
    IParticleReader* OverlapRemovalParticleReader::underyling_particle() const { return m_from; }
    size_t OverlapRemovalParticleReader::ReaderIndex() const { return m_from->ReaderIndex(); }
    const TLorentzVector& OverlapRemovalParticleReader::P4() const { return m_from->P4(); }

    IParticleReader* OverlapRemovalParticleReader::GetReader(const std::string& resulting_part, const std::string& rem_from,
                                                             const std::string& rem_with, float dR, bool keep_or) {
        if (!ParReaderStorage::GetInstance()->GetReader(resulting_part))
            return new OverlapRemovalParticleReader(resulting_part, rem_from, rem_with, dR, keep_or);
        return ParReaderStorage::GetInstance()->GetReader(resulting_part);
    }

    IParticleReader* OverlapRemovalParticleReader::GetReader(const std::string& resulting_part, const std::string& rem_from,
                                                             const std::string& rem_with, const std::string& from_label,
                                                             const std::string& cont_idx, bool keep_or) {
        if (!ParReaderStorage::GetInstance()->GetReader(resulting_part))
            return new OverlapRemovalParticleReader(resulting_part, rem_from, rem_with, from_label, cont_idx, keep_or);
        return ParReaderStorage::GetInstance()->GetReader(resulting_part);
    }

    //##################################################################
    //                      OverlapRemovalParticleVarReader
    //##################################################################
    IParticleVarReader* OverlapRemovalParticleVarReader::GetReader(OverlapRemovalParticleReader* particle, const std::string& var) {
        std::string r_name = particle->name() + " " + var;
        if (!ITreeVarReaderStorage::GetInstance()->GetReader(r_name)) return new OverlapRemovalParticleVarReader(particle, var);
        return dynamic_cast<IParticleVarReader*>(ITreeVarReaderStorage::GetInstance()->GetReader(r_name));
    }
    std::string OverlapRemovalParticleVarReader::name() const { return m_or_part->name() + " " + m_var_name; }
    double OverlapRemovalParticleVarReader::readEntry(size_t I) const { return m_variable->readEntry(m_or_part->removeOR(I)); }
    bool OverlapRemovalParticleVarReader::init(TTree* t) {
        if (!m_Registered) {
            Error("OverlapRemovalParticleVarReader()", "Something went wrong during the creation");
            return false;
        }
        return m_variable->init(t) && m_or_part->init(t);
    }
    OverlapRemovalParticleVarReader::OverlapRemovalParticleVarReader(OverlapRemovalParticleReader* particle, const std::string& variable) :
        IParticleVariable(particle),
        m_or_part(particle),
        m_var_name(variable),
        m_variable(particle->underyling_particle()->RetrieveVariable(variable)),
        m_Registered(false) {
        m_Registered = ITreeVarReaderStorage::GetInstance()->Register(this);
    }
}  // namespace XAMPP
