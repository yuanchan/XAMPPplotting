#include <TError.h>
#include <TFile.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>
#include <TMath.h>
#include <iostream>
#include <sstream>

#include <PathResolver/PathResolver.h>
#include <XAMPPplotting/Histo.h>
#include <XAMPPplotting/HistoTemplates.h>
#include <XAMPPplotting/LumiCalculator.h>
#include <XAMPPplotting/ObservableReader.h>
#include <XAMPPplotting/TreeVarReader.h>
#include <XAMPPplotting/UtilityReader.h>
#include <XAMPPplotting/Weight.h>

namespace XAMPP {

    //#########################################################################################
    //                                  Histo
    //#########################################################################################
    Histo::Histo(const Histo& other) :
        m_Reader(other.m_Reader),
        m_Name(other.m_Name),
        m_Template(other.m_Template),
        m_Type(other.m_Type),
        m_AltConst(other.m_AltConst),
        m_UseWeights(other.m_UseWeights),
        m_PullOverFlow(other.m_PullOverFlow),
        m_PullUnderFlow(other.m_PullUnderFlow),
        m_normToFirstBin(other.m_normToFirstBin),
        m_divideByBinWidth(other.m_divideByBinWidth),
        m_entriesLabel(other.m_entriesLabel),
        m_unitsLabel(other.m_unitsLabel),
        m_Entries(0),
        m_xLabel(other.m_xLabel),
        m_yLabel(other.m_yLabel),
        m_zLabel(other.m_zLabel),
        m_xBinLabels(other.m_xBinLabels),
        m_yBinLabels(other.m_yBinLabels),
        m_zBinLabels(other.m_zBinLabels),
        m_Weight(nullptr),
        m_Cut(other.m_Cut),
        m_HasCut(other.m_HasCut),
        m_Dir(nullptr),
        m_TH1(nullptr) {}

    Histo::Histo(XAMPP::ITreeVarReader* Reader, const std::string& Name, const std::string& Template, Histo::HistoTypes Type) :
        m_Reader(Reader),
        m_Name(Name),
        m_Template(Template),
        m_Type(Type),
        m_AltConst(false),
        m_UseWeights(true),
        m_PullOverFlow(true),
        m_PullUnderFlow(true),
        m_normToFirstBin(true),
        m_divideByBinWidth(false),
        m_entriesLabel("Entries"),
        m_unitsLabel(""),
        m_Entries(0),
        m_xLabel(),
        m_yLabel(),
        m_zLabel(),
        m_xBinLabels(),
        m_yBinLabels(),
        m_zBinLabels(),
        m_Weight(nullptr),
        m_Cut(nullptr),
        m_HasCut(false),
        m_Dir(nullptr),
        m_TH1(nullptr) {}
    Histo::Histo(XAMPP::ITreeVarReader* Reader, const std::string& Name, const std::string& Template) :
        Histo::Histo(Reader, Name, Template, Histo::HistoTypes::H1) {}
    // Constuctor used by the XAMPP selector classes
    Histo::Histo(const std::string& Name, const std::string& Template, Histo::HistoTypes Type) :
        Histo::Histo(nullptr, Name, Template, Type) {
        m_AltConst = true;
    }
    Histo::Histo(const std::string& Name, const std::string& Template) : Histo::Histo(Name, Template, Histo::HistoTypes::H1) {}
    Histo::Histo(const std::string& Name, unsigned int Nbins, double low, double high) : Histo(Name, Name) {
        if (!XAMPP::HistoTemplates::getHistoTemplater()->IsTemplateDefined(Name)) {
            std::shared_ptr<TH1> Histo = std::make_shared<TH1D>(Name.c_str(), Name.c_str(), Nbins, low, high);
            XAMPP::HistoTemplates::getHistoTemplater()->InsertTemplate(Name, Histo);
        }
    }
    Histo::Histo(const std::string& Name, unsigned int Nbins, const double* xbins) : Histo(Name, Name) {
        if (!XAMPP::HistoTemplates::getHistoTemplater()->IsTemplateDefined(Name)) {
            std::shared_ptr<TH1> Histo = std::make_shared<TH1D>(Name.c_str(), Name.c_str(), Nbins, xbins);
            XAMPP::HistoTemplates::getHistoTemplater()->InsertTemplate(Name, Histo);
        }
    }
    Histo::Histo(const std::string& Name, const std::vector<double>& xbins) : Histo(Name, xbins.size() - 1, xbins.data()) {}
    Histo::Histo(XAMPP::ITreeVarReader* Reader, const std::string& Name, unsigned int Nbins, double low, double high) :
        Histo(Reader, Name, Name) {
        if (!XAMPP::HistoTemplates::getHistoTemplater()->IsTemplateDefined(Name)) {
            std::shared_ptr<TH1> Histo = std::make_shared<TH1D>(Name.c_str(), Name.c_str(), Nbins, low, high);
            XAMPP::HistoTemplates::getHistoTemplater()->InsertTemplate(Name, Histo);
        }
    }
    Histo::Histo(XAMPP::ITreeVarReader* Reader, const std::string& Name, unsigned int Nbins, const double* xbins) :
        Histo(Reader, Name, Name) {
        if (!XAMPP::HistoTemplates::getHistoTemplater()->IsTemplateDefined(Name)) {
            std::shared_ptr<TH1> Histo = std::make_shared<TH1D>(Name.c_str(), Name.c_str(), Nbins, xbins);
            XAMPP::HistoTemplates::getHistoTemplater()->InsertTemplate(Name, Histo);
        }
    }
    Histo::Histo(XAMPP::ITreeVarReader* Reader, const std::string& Name, const std::vector<double>& xbins) :
        Histo(Reader, Name, xbins.size() - 1, xbins.data()) {}
    bool Histo::init(TDirectory* D) {
        if (name().empty()) {
            Error("Histo::init()", "No name was given to the histogram");
            return false;
        }
        if (!D) {
            Error("Histo::init()", "No TDirectory was given to " + name());
            return false;
        }
        if (D->FindObject(name().c_str()) != nullptr) {
            Error("Histo::init()", "There exists already an object " + name() + " in TDirectory " + std::string(D->GetName()));
            return false;
        }
        if (!m_AltConst && !m_Reader) {
            Error("Histo::init()", "The histogram " + name() + " has no ITreeVarReader.");
            return false;
        }
        m_Entries = 0;
        m_Weight = XAMPP::Weight::getWeighter();
        XAMPP::HistoTemplates* Templater = XAMPP::HistoTemplates::getHistoTemplater();
        TH1* H_template = Templater->GetTemplate(GetTemplate());
        if (!H_template) {
            Error("Histo::init()", "The template " + GetTemplate() + " does not exist to create " + name());
            return false;
        }
        m_TH1 = std::unique_ptr<TH1>(dynamic_cast<TH1*>(H_template->Clone(name().c_str())));
        if (!m_TH1) return false;
        m_TH1->Reset();

        bool is2D = m_TH1->GetDimension() == 2;
        bool is3D = m_TH1->GetDimension() == 3;

        if ((is2D || is3D) && m_Type == HistoTypes::H1) {
            Error("Histo::init()",
                  "The  provided template " + GetTemplate() + " is " + std::to_string(is2D ? 2 : 3) + "D while you created a 1D instance");
            return false;
        } else if (m_Type == HistoTypes::H2 && (!is2D || is3D)) {
            Error("Histo::init()",
                  "The  provided template " + GetTemplate() + " is " + std::to_string(is3D ? 3 : 1) + "D while you created a 2D instance");
            return false;
        } else if (m_Type == HistoTypes::H3 && (!is3D || is2D)) {
            Error("Histo::init()",
                  "The  provided template " + GetTemplate() + " is " + std::to_string(is2D ? 2 : 1) + "D while you created a 3D instance");
            return false;
        }
        m_TH1->SetDirectory(nullptr);
        m_Dir = D;
        return true;
    }
    void Histo::SetTitleAndLabels() {
        m_TH1->GetXaxis()->SetTitle(m_xLabel.c_str());

        bool is2D = m_TH1->GetDimension() == 2;
        bool is3D = m_TH1->GetDimension() == 3;

        for (const auto& Label : m_xBinLabels) m_TH1->GetXaxis()->SetBinLabel(Label.first, Label.second.c_str());

        if (is2D || is3D) {
            for (const auto& Label : m_yBinLabels) m_TH1->GetYaxis()->SetBinLabel(Label.first, Label.second.c_str());
        }
        if (is3D) {
            for (const auto& Label : m_zBinLabels) m_TH1->GetZaxis()->SetBinLabel(Label.first, Label.second.c_str());
        }
        /// If the user activates bin-labels and by accident the fill method of the last bin is called TH1 simply doubles
        /// number of bins randomly
        m_TH1->GetXaxis()->SetCanExtend(false);
        m_TH1->GetYaxis()->SetCanExtend(false);
        m_TH1->GetZaxis()->SetCanExtend(false);

        if (m_Type == HistoTypes::H1) {
            // for style guide, cf. https://twiki.cern.ch/twiki/bin/view/AtlasProtected/PubComPlotStyle
            if (m_xLabel.find(" [") < m_xLabel.size())
                m_unitsLabel = m_xLabel.substr(m_xLabel.find("[") + 1, m_xLabel.rfind("]") - m_xLabel.find("[") - 1);
            if (!m_unitsLabel.empty()) m_unitsLabel = " " + m_unitsLabel;
            if (m_divideByBinWidth)
                m_TH1->GetYaxis()->SetTitle(Form("%s / (bin width)", m_entriesLabel.c_str()));
            else if (m_TH1->GetBinWidth(1) < 1)
                m_TH1->GetYaxis()->SetTitle(Form("%s / %.2f%s", m_entriesLabel.c_str(), m_TH1->GetBinWidth(1), m_unitsLabel.c_str()));
            else if (m_TH1->GetBinWidth(1) < 10)
                m_TH1->GetYaxis()->SetTitle(Form("%s / %.1f%s", m_entriesLabel.c_str(), m_TH1->GetBinWidth(1), m_unitsLabel.c_str()));
            else
                m_TH1->GetYaxis()->SetTitle(Form("%s / %.f%s", m_entriesLabel.c_str(), m_TH1->GetBinWidth(1), m_unitsLabel.c_str()));
        } else if (m_Type == HistoTypes::H2) {
            m_TH1->GetYaxis()->SetTitle(m_yLabel.c_str());
            m_TH1->GetZaxis()->SetTitle(m_entriesLabel.c_str());
            m_TH1->SetContour(99);
        } else if (m_Type == HistoTypes::H3) {
            m_TH1->GetYaxis()->SetTitle(m_yLabel.c_str());
            m_TH1->GetZaxis()->SetTitle(m_zLabel.c_str());
        }
    }
    Histo::~Histo() {}
    void Histo::Fill(double xValue) {
        if (!NoReaderConnected()) {
            Warning("Histo::Fill()", "The Histo class " + name() + " has been created with ITreeVarReader " + m_Reader->name() +
                                         ". Please use the fill() method.");
            return;
        }
        int Bin = m_TH1->FindBin(xValue);
        AddBinContent(Bin);
    }
    void Histo::Fill(double xValue, double yValue) {
        Warning("Histo::Fill()", "This method is not supported for x-Value: " + std::to_string(xValue) +
                                     " and y-Value: " + std::to_string(yValue) + ". Please us the Histo2D instead");
    }
    void Histo::Fill(double xValue, double yValue, double zValue) {
        Warning("Histo::Fill()", "This method is not supported for x-Value: " + std::to_string(xValue) + ", y-Value: " +
                                     std::to_string(yValue) + " and z-Value " + std::to_string(zValue) + ". Please us the Histo3D instead");
    }
    void Histo::fill() {
        if (NoReaderConnected()) {
            Warning("Histo::fill()", "The Histo " + name() + " has been created without ITreeVarReader. This method is deactivated");
            return;
        }
        m_Reader->reset();
        while (m_Reader->update()) {
            int Bin = m_TH1->FindBin(m_Reader->read());
            AddBinContent(Bin);
        }
        m_Reader->reset();
    }
    double Histo::weight() const {
        if (!m_UseWeights) { return 1.; }
        return m_Weight->GetWeight();
    }
    double Histo::weight2() const {
        if (!m_UseWeights) { return 1.; }
        return m_Weight->GetWeightSquared();
    }
    bool Histo::passCut() const {
        if (m_HasCut && !m_Cut->Pass()) return false;
        ++m_Entries;
        return true;
    }
    void Histo::AddBinContent(int Bin) const {
        if (!passCut()) return;
        double NewContent = m_TH1->GetBinContent(Bin) + weight();
        double NewError = m_TH1->GetBinError(Bin) + weight2();
        m_TH1->SetBinContent(Bin, NewContent);
        m_TH1->SetBinError(Bin, NewError);
    }
    void Histo::SetBinLabelX(int Bin, const std::string& L) {
        std::map<int, std::string>::iterator Itr = m_xBinLabels.find(Bin);
        if (Itr != m_xBinLabels.end()) {
            Warning("Histo::SetBinLabelX()", "The bin " + std::to_string(Bin) + " already has alphanumeric label " + Itr->second);
            Itr->second = L;
        } else
            m_xBinLabels.insert(std::pair<int, std::string>(Bin, L));
    }
    void Histo::SetBinLabelY(int Bin, const std::string& L) {
        std::map<int, std::string>::iterator Itr = m_yBinLabels.find(Bin);
        if (Itr != m_yBinLabels.end()) {
            Warning("Histo::SetBinLabelY()", "The bin " + std::to_string(Bin) + " already has alphanumeric label " + Itr->second);
            Itr->second = L;
        } else
            m_yBinLabels.insert(std::pair<int, std::string>(Bin, L));
    }
    void Histo::SetBinLabelZ(int Bin, const std::string& L) {
        std::map<int, std::string>::iterator Itr = m_zBinLabels.find(Bin);
        if (Itr != m_zBinLabels.end()) {
            Warning("Histo::SetBinLabelZ()", "The bin " + std::to_string(Bin) + " already has alphanumeric label " + Itr->second);
            Itr->second = L;
        } else
            m_zBinLabels.insert(std::pair<int, std::string>(Bin, L));
    }
    void Histo::DoRaw(bool B) { m_UseWeights = !B; }
    std::string Histo::name() const { return m_Name; }
    void Histo::SetXaxisLabel(const std::string& L) { m_xLabel = L; }
    void Histo::SetYaxisLabel(const std::string& L) { m_yLabel = L; }
    void Histo::SetZaxisLabel(const std::string& L) { m_zLabel = L; }
    void Histo::SetCut(std::shared_ptr<Condition> C) {
        m_Cut = C;
        m_HasCut = (m_Cut.get() != nullptr);
    }
    void Histo::PullOverFlowBins(bool B) { m_PullOverFlow = B; }
    void Histo::PullUnderFlowBins(bool B) { m_PullUnderFlow = B; }

    std::string Histo::GetTemplate() const { return m_Template; }
    bool Histo::PullOverFlow() const { return m_PullOverFlow; }
    bool Histo::PullUnderFlow() const { return m_PullUnderFlow; }

    void Histo::PullBinsOutOfRange() {
        if ((!PullOverFlow() && !PullUnderFlow()) || !m_xBinLabels.empty()) return;
        int Nx = m_TH1->GetNbinsX() + 1;
        if (m_PullUnderFlow) PullContentInBin(0, 1);
        if (m_PullOverFlow) PullContentInBin(Nx, m_TH1->GetNbinsX());
    }
    void Histo::SetNormToFirstBin(bool B) { m_normToFirstBin = B; }
    void Histo::SetDivideByBinWidth(bool B) { m_divideByBinWidth = B; }
    void Histo::SetEntriesLabel(const std::string& L) { m_entriesLabel = L; }
    void Histo::SetUnitsLabel(const std::string& L) { m_unitsLabel = L; }

    void Histo::Finalize() {
        double wX(1.), wY(1.), wZ(1.), divideFactor(1.);
        int Nx = m_TH1->GetNbinsX() + (m_xBinLabels.empty() ? 1 : 0);
        int Ny = (m_Type == Histo::HistoTypes::H1 ? 0 : m_TH1->GetNbinsY() + (m_yBinLabels.empty() ? 1 : 0));
        int Nz = (m_Type != Histo::HistoTypes::H3 ? 0 : m_TH1->GetNbinsZ() + (m_zBinLabels.empty() ? 1 : 0));
        PullBinsOutOfRange();
        for (int nX = Nx; nX >= 0; --nX) {
            wX = m_normToFirstBin ? m_TH1->GetXaxis()->GetBinWidth(1) / m_TH1->GetXaxis()->GetBinWidth(nX) : 1;
            for (int nY = Ny; nY >= 0; --nY) {
                wY = m_Type != Histo::HistoTypes::H1 || !m_normToFirstBin
                         ? m_TH1->GetYaxis()->GetBinWidth(1) / m_TH1->GetYaxis()->GetBinWidth(nY)
                         : 1.;
                for (int nZ = Nz; nZ >= 0; --nZ) {
                    wZ = m_Type == Histo::HistoTypes::H3 || !m_normToFirstBin
                             ? m_TH1->GetZaxis()->GetBinWidth(1) / m_TH1->GetZaxis()->GetBinWidth(nZ)
                             : 1.;
                    int Bin = m_TH1->GetBin(nX, nY, nZ);
                    if (m_divideByBinWidth) {
                        // since everything is normalised to the first bin, just divide by the bin width(s) of the first bin
                        divideFactor = 1. / (m_TH1->GetXaxis()->GetBinWidth(1));
                        if (m_Type != Histo::HistoTypes::H1) divideFactor /= m_TH1->GetYaxis()->GetBinWidth(1);
                        if (m_Type == Histo::HistoTypes::H3) divideFactor /= m_TH1->GetZaxis()->GetBinWidth(1);
                    }
                    m_TH1->SetBinContent(Bin, m_TH1->GetBinContent(Bin) * wX * wY * wZ * divideFactor);
                    m_TH1->SetBinError(Bin, TMath::Sqrt(m_TH1->GetBinError(Bin)) * wX * wY * wZ * divideFactor);
                }
            }
        }
    }
    void Histo::PullContentInBin(int From, int To) {
        if (m_TH1->GetBinContent(From) == 0.) return;
        double Cont = m_TH1->GetBinContent(From) + m_TH1->GetBinContent(To);
        double Err = m_TH1->GetBinError(From) + m_TH1->GetBinError(To);
        m_TH1->SetBinContent(From, 0.);
        m_TH1->SetBinError(From, 0.);
        m_TH1->SetBinContent(To, Cont);
        m_TH1->SetBinError(To, Err);
    }
    void Histo::write() {
        if (!m_TH1) {
            Warning("Histo::write()", "The histogram " + name() + " has already been written. ");
            return;
        }
        SetTitleAndLabels();
        Finalize();
        m_TH1->SetEntries(m_Entries);  // Diese Bengel ROOT SetBinContent funktion.....
        m_Dir->cd();
        m_TH1->Write();
        m_TH1 = std::unique_ptr<TH1>();
    }
    void Histo::Statistics() const {
        Info("Histo::Statistics()", "The histogram " + name() + " has been written containing " + std::to_string(m_Entries) + " entries.");
    }
    bool Histo::NoReaderConnected() const { return m_AltConst; }
    std::shared_ptr<Histo> Histo::Clone() { return std::make_shared<Histo>(*this); }
    //#########################################################################################
    //                                  Histo2D
    //#########################################################################################
    Histo2D::Histo2D(const std::string& Name, const std::string& Template) :
        Histo::Histo(Name, Template, Histo::HistoTypes::H2),
        m_yReader(nullptr) {}
    Histo2D::Histo2D(XAMPP::ITreeVarReader* xReader, XAMPP::ITreeVarReader* yReader, const std::string& Name, const std::string& Template) :
        Histo::Histo(xReader, Name, Template, Histo::HistoTypes::H2),
        m_yReader(yReader) {}
    Histo2D::Histo2D(const std::string& Name, unsigned int NBinsX, double xLow, double xHigh, unsigned int NBinsY, double yLow,
                     double yHigh) :
        Histo2D(Name, Name) {
        if (!XAMPP::HistoTemplates::getHistoTemplater()->IsTemplateDefined(Name)) {
            std::shared_ptr<TH1> Histo = std::make_shared<TH2D>(Name.c_str(), Name.c_str(), NBinsX, xLow, xHigh, NBinsY, yLow, yHigh);
            XAMPP::HistoTemplates::getHistoTemplater()->InsertTemplate(Name, Histo);
        }
    }
    Histo2D::Histo2D(const std::string& Name, unsigned int NBinsX, double xLow, double xHigh, unsigned int NBinsY, double* ybins) :
        Histo2D(Name, Name) {
        if (!XAMPP::HistoTemplates::getHistoTemplater()->IsTemplateDefined(Name)) {
            std::shared_ptr<TH1> Histo = std::make_shared<TH2D>(Name.c_str(), Name.c_str(), NBinsX, xLow, xHigh, NBinsY, ybins);
            XAMPP::HistoTemplates::getHistoTemplater()->InsertTemplate(Name, Histo);
        }
    }
    Histo2D::Histo2D(const std::string& Name, unsigned int NBinsX, double* xbins, unsigned int NBinsY, double yLow, double yHigh) :
        Histo2D(Name, Name) {
        if (!XAMPP::HistoTemplates::getHistoTemplater()->IsTemplateDefined(Name)) {
            std::shared_ptr<TH1> Histo = std::make_shared<TH2D>(Name.c_str(), Name.c_str(), NBinsX, xbins, NBinsY, yLow, yHigh);
            XAMPP::HistoTemplates::getHistoTemplater()->InsertTemplate(Name, Histo);
        }
    }

    Histo2D::Histo2D(XAMPP::ITreeVarReader* xReader, XAMPP::ITreeVarReader* yReader, const std::string& Name, unsigned int NBinsX,
                     double xLow, double xHigh, unsigned int NBinsY, double yLow, double yHigh) :
        Histo2D(xReader, yReader, Name, Name) {
        if (!XAMPP::HistoTemplates::getHistoTemplater()->IsTemplateDefined(Name)) {
            std::shared_ptr<TH1> Histo = std::make_shared<TH2D>(Name.c_str(), Name.c_str(), NBinsX, xLow, xHigh, NBinsY, yLow, yHigh);
            XAMPP::HistoTemplates::getHistoTemplater()->InsertTemplate(Name, Histo);
        }
    }
    Histo2D::Histo2D(XAMPP::ITreeVarReader* xReader, XAMPP::ITreeVarReader* yReader, const std::string& Name, unsigned int NBinsX,
                     double xLow, double xHigh, unsigned int NBinsY, double* ybins) :
        Histo2D(xReader, yReader, Name, Name) {
        if (!XAMPP::HistoTemplates::getHistoTemplater()->IsTemplateDefined(Name)) {
            std::shared_ptr<TH1> Histo = std::make_shared<TH2D>(Name.c_str(), Name.c_str(), NBinsX, xLow, xHigh, NBinsY, ybins);
            XAMPP::HistoTemplates::getHistoTemplater()->InsertTemplate(Name, Histo);
        }
    }
    Histo2D::Histo2D(XAMPP::ITreeVarReader* xReader, XAMPP::ITreeVarReader* yReader, const std::string& Name, unsigned int NBinsX,
                     double* xbins, unsigned int NBinsY, double yLow, double yHigh) :
        Histo2D(xReader, yReader, Name, Name) {
        if (!XAMPP::HistoTemplates::getHistoTemplater()->IsTemplateDefined(Name)) {
            std::shared_ptr<TH1> Histo = std::make_shared<TH2D>(Name.c_str(), Name.c_str(), NBinsX, xbins, NBinsY, yLow, yHigh);
            XAMPP::HistoTemplates::getHistoTemplater()->InsertTemplate(Name, Histo);
        }
    }

    void Histo2D::Fill(double xValue) {
        Warning("Histo2D::Fill()", "This method is not supported for only one x-Value " + std::to_string(xValue));
    }
    void Histo2D::Fill(double xValue, double yValue) {
        if (!NoReaderConnected()) {
            Warning("Histo2D::Fill()", "This method is deactivated since " + name() + " has been initialized with the readers " +
                                           m_Reader->name() + " & " + m_yReader->name());
            return;
        }
        int Bin = m_TH1->FindBin(xValue, yValue);
        AddBinContent(Bin);
    }
    void Histo2D::Fill(double xValue, double yValue, double zValue) { Histo::Fill(xValue, yValue, zValue); }
    void Histo2D::fill() {
        if (NoReaderConnected()) {
            Warning("Histo2D::fill()", "The Histo " + name() + " has been created without ITreeVarReader. This method is deactivated");
            return;
        }
        while (m_Reader->update() && m_yReader->update()) {
            int Bin = m_TH1->FindBin(m_Reader->read(), m_yReader->read());
            AddBinContent(Bin);
        }
        m_Reader->reset();
        m_yReader->reset();
    }
    bool Histo2D::init(TDirectory* D) {
        if (!Histo::init(D)) return false;
        if (!NoReaderConnected() && !m_yReader) {
            Error("Histo2D::init()", "No y-Reader was given to " + name());
            return false;
        }
        return true;
    }
    std::shared_ptr<Histo> Histo2D::Clone() { return std::make_shared<Histo2D>(*this); }
    void Histo2D::PullBinsOutOfRange() {
        if (!PullOverFlow() && !PullUnderFlow()) return;
        int nX = m_TH1->GetNbinsX() + 1;
        int nY = m_TH1->GetNbinsY() + 1;
        for (int x = 0; x <= nX; ++x) {
            int bin_uf_y = m_TH1->GetBin(x, 0);
            int bin_low_y = m_TH1->GetBin(x, 1);
            int bin_of_y = m_TH1->GetBin(x, nY);
            int bin_high_y = m_TH1->GetBin(x, nY - 1);

            if (PullUnderFlow()) PullContentInBin(bin_uf_y, bin_low_y);
            if (PullOverFlow()) PullContentInBin(bin_of_y, bin_high_y);
        }
        for (int y = 0; y <= nY; ++y) {
            int bin_uf_x = m_TH1->GetBin(0, y);
            int bin_low_x = m_TH1->GetBin(1, y);
            int bin_of_x = m_TH1->GetBin(nX, y);
            int bin_high_x = m_TH1->GetBin(nX - 1, y);
            if (PullUnderFlow()) PullContentInBin(bin_uf_x, bin_low_x);
            if (PullOverFlow()) PullContentInBin(bin_of_x, bin_high_x);
        }
    }
    //#########################################################################################
    //                                  Histo2DPoly
    //#########################################################################################
    Histo2DPoly::Histo2DPoly(const std::string& Name, const std::string& Template) : Histo2D::Histo2D(Name, Template), m_THPoly(nullptr) {}
    Histo2DPoly::Histo2DPoly(XAMPP::ITreeVarReader* xReader, XAMPP::ITreeVarReader* yReader, const std::string& Name,
                             const std::string& Template) :
        Histo2D::Histo2D(xReader, yReader, Name, Template),
        m_THPoly(nullptr) {}
    void Histo2DPoly::Fill(double xValue) { Histo::Fill(xValue); }

    void Histo2DPoly::Fill(double xValue, double yValue, double zValue) { Histo::Fill(xValue, yValue, zValue); }
    void Histo2DPoly::Fill(double xValue, double yValue) {
        if (!NoReaderConnected()) {
            Warning("Histo2D::Fill()", "This method is deactivated since " + name() + " has been initialized with the readers " +
                                           m_Reader->name() + " & " + m_yReader->name());
            return;
        }
        FillPoly(xValue, yValue);
    }
    void Histo2DPoly::FillPoly(double xValue, double yValue) {
        if (!passCut()) { return; }
        m_THPoly->Fill(xValue, yValue, weight());
    }
    void Histo2DPoly::fill() {
        while (m_Reader->update() && m_yReader->update()) { FillPoly(m_Reader->read(), m_yReader->read()); }
        m_Reader->reset();
        m_yReader->reset();
    }
    bool Histo2DPoly::init(TDirectory* D) {
        if (!Histo2D::init(D)) { return false; }
        m_THPoly = dynamic_cast<TH2Poly*>(m_TH1.get());
        if (!m_THPoly) {
            Error("Histo2DPoly::init()", "The template " + GetTemplate() + " is no TH2Poly");
            return false;
        }
        PullOverFlowBins(false);
        PullUnderFlowBins(false);
        return true;
    }
    void Histo2DPoly::Finalize() {}
    std::shared_ptr<Histo> Histo2DPoly::Clone() { return std::make_shared<Histo2DPoly>(*this); }
    //#########################################################################################
    //                                  Histo3D
    //#########################################################################################
    Histo3D::Histo3D(const std::string& Name, const std::string& Template) :
        Histo::Histo(Name, Template, Histo::HistoTypes::H3),
        m_yReader(nullptr),
        m_zReader(nullptr) {}
    Histo3D::Histo3D(XAMPP::ITreeVarReader* xReader, XAMPP::ITreeVarReader* yReader, XAMPP::ITreeVarReader* zReader,
                     const std::string& Name, const std::string& Template) :
        Histo::Histo(xReader, Name, Template, Histo::HistoTypes::H3),
        m_yReader(yReader),
        m_zReader(zReader) {}
    Histo3D::Histo3D(const std::string& Name, unsigned int NBinsX, double xLow, double xHigh, unsigned int NBinsY, double yLow,
                     double yHigh, unsigned int NBinsZ, double zLow, double zHigh) :
        Histo3D(Name, Name) {
        if (!XAMPP::HistoTemplates::getHistoTemplater()->IsTemplateDefined(Name)) {
            std::shared_ptr<TH1> Histo =
                std::make_shared<TH3D>(Name.c_str(), Name.c_str(), NBinsX, xLow, xHigh, NBinsY, yLow, yHigh, NBinsZ, zLow, zHigh);
            XAMPP::HistoTemplates::getHistoTemplater()->InsertTemplate(Name, Histo);
        }
    }
    Histo3D::Histo3D(const std::string& Name, unsigned int NBinsX, double* xbins, unsigned int NBinsY, double* ybins, unsigned int NBinsZ,
                     double* zbins) :
        Histo3D(Name, Name) {
        if (!XAMPP::HistoTemplates::getHistoTemplater()->IsTemplateDefined(Name)) {
            std::shared_ptr<TH1> Histo = std::make_shared<TH3D>(Name.c_str(), Name.c_str(), NBinsX, xbins, NBinsY, ybins, NBinsZ, zbins);
            XAMPP::HistoTemplates::getHistoTemplater()->InsertTemplate(Name, Histo);
        }
    }
    Histo3D::Histo3D(XAMPP::ITreeVarReader* xReader, XAMPP::ITreeVarReader* yReader, XAMPP::ITreeVarReader* zReader,
                     const std::string& Name, unsigned int NBinsX, double xLow, double xHigh, unsigned int NBinsY, double yLow,
                     double yHigh, unsigned int NBinsZ, double zLow, double zHigh) :
        Histo3D(xReader, yReader, zReader, Name, Name) {
        if (!XAMPP::HistoTemplates::getHistoTemplater()->IsTemplateDefined(Name)) {
            std::shared_ptr<TH1> Histo =
                std::make_shared<TH3D>(Name.c_str(), Name.c_str(), NBinsX, xLow, xHigh, NBinsY, yLow, yHigh, NBinsZ, zLow, zHigh);
            XAMPP::HistoTemplates::getHistoTemplater()->InsertTemplate(Name, Histo);
        }
    }
    Histo3D::Histo3D(XAMPP::ITreeVarReader* xReader, XAMPP::ITreeVarReader* yReader, XAMPP::ITreeVarReader* zReader,
                     const std::string& Name, unsigned int NBinsX, double* xbins, unsigned int NBinsY, double* ybins, unsigned int NBinsZ,
                     double* zbins) :
        Histo3D(xReader, yReader, zReader, Name, Name) {
        if (!XAMPP::HistoTemplates::getHistoTemplater()->IsTemplateDefined(Name)) {
            std::shared_ptr<TH1> Histo = std::make_shared<TH3D>(Name.c_str(), Name.c_str(), NBinsX, xbins, NBinsY, ybins, NBinsZ, zbins);
            XAMPP::HistoTemplates::getHistoTemplater()->InsertTemplate(Name, Histo);
        }
    }
    void Histo3D::fill() {
        if (NoReaderConnected()) {
            Warning("Histo3D::fill()", "The Histo " + name() + " has been created without ITreeVarReader. This method is deactivated");
            return;
        }
        while (m_Reader->update() && m_yReader->update() && m_zReader->update()) {
            int Bin = m_TH1->FindBin(m_Reader->read(), m_yReader->read(), m_zReader->read());
            AddBinContent(Bin);
        }
        m_Reader->reset();
        m_yReader->reset();
        m_zReader->reset();
    }
    bool Histo3D::init(TDirectory* D) {
        if (!Histo::init(D)) return false;
        if (!NoReaderConnected() && (!m_yReader || !m_zReader)) {
            Error("Histo3D::init()", "No y-Reader or z-Reader was given to " + name());
            return false;
        }
        return true;
    }

    void Histo3D::Fill(double xValue) {
        Warning("Histo3D::Fill()", "This method is not supported for only one x-Value " + std::to_string(xValue));
    }
    void Histo3D::Fill(double xValue, double yValue) { Histo::Fill(xValue, yValue); }
    void Histo3D::Fill(double xValue, double yValue, double zValue) {
        if (!NoReaderConnected()) {
            Warning("Histo3D::Fill()", "This method is deactivated since " + name() + " has been initialized with the readers " +
                                           m_Reader->name() + " & " + m_yReader->name() + "&" + m_zReader->name());
            return;
        }
        int Bin = m_TH1->FindBin(xValue, yValue, zValue);
        AddBinContent(Bin);
    }
    std::shared_ptr<Histo> Histo3D::Clone() { return std::make_shared<Histo3D>(*this); }
    //#########################################################################################
    //                                  CumulativeHisto
    //#########################################################################################
    CumulativeHisto::CumulativeHisto(XAMPP::ITreeVarReader* Reader, const std::string& Name, const std::string& Template) :
        Histo(Reader, Name, Template) {}
    CumulativeHisto::CumulativeHisto(const std::string& Name, const std::string& Template) : Histo(Name, Template) {}
    CumulativeHisto::CumulativeHisto(const std::string& Name, unsigned int Nbins, double low, double high) :
        Histo(Name, Nbins, low, high) {}
    CumulativeHisto::CumulativeHisto(const std::string& Name, unsigned int Nbins, const double* xbins) : Histo(Name, Nbins, xbins) {}
    CumulativeHisto::CumulativeHisto(XAMPP::ITreeVarReader* Reader, const std::string& Name, unsigned int Nbins, double low, double high) :
        Histo(Reader, Name, Nbins, low, high) {}
    CumulativeHisto::CumulativeHisto(XAMPP::ITreeVarReader* Reader, const std::string& Name, unsigned int Nbins, const double* xbins) :
        Histo(Reader, Name, Nbins, xbins) {}
    void CumulativeHisto::Finalize() {
        Histo::Finalize();
        int BinMax = m_TH1->GetNbinsX() + 1;
        for (int b = 0; b <= BinMax; ++b) {
            std::pair<double, double> IE = IntegrateAndError(m_TH1.get(), b, BinMax);
            m_TH1->SetBinContent(b, IE.first);
            m_TH1->SetBinError(b, IE.second);
        }
    }
    std::shared_ptr<Histo> CumulativeHisto::Clone() { return std::make_shared<CumulativeHisto>(*this); }
    //#########################################################################################
    //                                  RevCumulativeHisto
    //#########################################################################################
    RevCumulativeHisto::RevCumulativeHisto(XAMPP::ITreeVarReader* Reader, const std::string& Name, const std::string& Template) :
        CumulativeHisto(Reader, Name, Template) {}
    RevCumulativeHisto::RevCumulativeHisto(const std::string& Name, const std::string& Template) : CumulativeHisto(Name, Template) {}
    RevCumulativeHisto::RevCumulativeHisto(const std::string& Name, unsigned int Nbins, double low, double high) :
        CumulativeHisto(Name, Nbins, low, high) {}
    RevCumulativeHisto::RevCumulativeHisto(const std::string& Name, unsigned int Nbins, const double* xbins) :
        CumulativeHisto(Name, Nbins, xbins) {}
    RevCumulativeHisto::RevCumulativeHisto(XAMPP::ITreeVarReader* Reader, const std::string& Name, unsigned int Nbins, double low,
                                           double high) :
        CumulativeHisto(Reader, Name, Nbins, low, high) {}
    RevCumulativeHisto::RevCumulativeHisto(XAMPP::ITreeVarReader* Reader, const std::string& Name, unsigned int Nbins,
                                           const double* xbins) :
        CumulativeHisto(Reader, Name, Nbins, xbins) {}
    void RevCumulativeHisto::Finalize() {
        Histo::Finalize();
        int BinMax = m_TH1->GetNbinsX() + 1;
        for (int b = BinMax; b >= 0; --b) {
            std::pair<double, double> IE = IntegrateAndError(m_TH1.get(), 0, b);
            m_TH1->SetBinContent(b, IE.first);
            m_TH1->SetBinError(b, IE.second);
        }
    }
    std::shared_ptr<Histo> RevCumulativeHisto::Clone() { return std::make_shared<RevCumulativeHisto>(*this); }
    //#########################################################################################
    //                                  CumulativeHisto2D
    //#########################################################################################
    CumulativeHisto2D::CumulativeHisto2D(const std::string& Name, const std::string& Template) : Histo2D(Name, Template) {}
    CumulativeHisto2D::CumulativeHisto2D(XAMPP::ITreeVarReader* xReader, XAMPP::ITreeVarReader* yReader, const std::string& Name,
                                         const std::string& Template) :
        Histo2D(xReader, yReader, Name, Template) {}
    CumulativeHisto2D::CumulativeHisto2D(const std::string& Name, unsigned int NBinsX, double xLow, double xHigh, unsigned int NBinsY,
                                         double yLow, double yHigh) :
        Histo2D(Name, NBinsX, xLow, xHigh, NBinsY, yLow, yHigh) {}
    CumulativeHisto2D::CumulativeHisto2D(const std::string& Name, unsigned int NBinsX, double xLow, double xHigh, unsigned int NBinsY,
                                         double* ybins) :
        Histo2D(Name, NBinsX, xLow, xHigh, NBinsY, ybins) {}
    CumulativeHisto2D::CumulativeHisto2D(const std::string& Name, unsigned int NBinsX, double* xbins, unsigned int NBinsY, double yLow,
                                         double yHigh) :
        Histo2D(Name, NBinsX, xbins, NBinsY, yLow, yHigh) {}
    CumulativeHisto2D::CumulativeHisto2D(XAMPP::ITreeVarReader* xReader, XAMPP::ITreeVarReader* yReader, const std::string& Name,
                                         unsigned int NBinsX, double xLow, double xHigh, unsigned int NBinsY, double yLow, double yHigh) :
        Histo2D(xReader, yReader, Name, NBinsX, xLow, xHigh, NBinsY, yLow, yHigh) {}
    CumulativeHisto2D::CumulativeHisto2D(XAMPP::ITreeVarReader* xReader, XAMPP::ITreeVarReader* yReader, const std::string& Name,
                                         unsigned int NBinsX, double xLow, double xHigh, unsigned int NBinsY, double* ybins) :
        Histo2D(xReader, yReader, Name, NBinsX, xLow, xHigh, NBinsY, ybins) {}
    CumulativeHisto2D::CumulativeHisto2D(XAMPP::ITreeVarReader* xReader, XAMPP::ITreeVarReader* yReader, const std::string& Name,
                                         unsigned int NBinsX, double* xbins, unsigned int NBinsY, double yLow, double yHigh) :
        Histo2D(xReader, yReader, Name, NBinsX, xbins, NBinsY, yLow, yHigh) {}
    void CumulativeHisto2D::Finalize() {
        Histo2D::Finalize();
        int xMax = m_TH1->GetNbinsX() + 1;
        int yMax = m_TH1->GetNbinsY() + 1;
        for (int x = 0; x < xMax; ++x) {
            for (int y = 0; y < yMax; ++y) {
                std::pair<double, double> IE = IntegrateAndError(m_TH1.get(), x, xMax, y, yMax);
                m_TH1->SetBinContent(x, y, IE.first);
                m_TH1->SetBinError(x, y, IE.second);
            }
        }
    }
    std::shared_ptr<Histo> CumulativeHisto2D::Clone() { return std::make_shared<CumulativeHisto2D>(*this); }
    //############################################################################################################
    //                                  RevCumulativeHisto2D
    //############################################################################################################
    RevCumulativeHisto2D::RevCumulativeHisto2D(std::string Name, std::string Template) : CumulativeHisto2D(Name, Template) {}
    RevCumulativeHisto2D::RevCumulativeHisto2D(XAMPP::ITreeVarReader* xReader, XAMPP::ITreeVarReader* yReader, std::string Name,
                                               std::string Template) :
        CumulativeHisto2D(xReader, yReader, Name, Template) {}
    void RevCumulativeHisto2D::Finalize() {
        Histo2D::Finalize();
        int xMax = m_TH1->GetNbinsX() + 1;
        int yMax = m_TH1->GetNbinsY() + 1;
        for (int x = xMax; x >= 0; --x) {
            for (int y = yMax; y >= 0; --y) {
                std::pair<double, double> IE = IntegrateAndError(m_TH1.get(), 0, x, 0, y);
                m_TH1->SetBinContent(x, y, IE.first);
                m_TH1->SetBinError(x, y, IE.second);
            }
        }
    }
    std::shared_ptr<Histo> RevCumulativeHisto2D::Clone() { return std::make_shared<RevCumulativeHisto2D>(*this); }
    //############################################################################################################
    //                                  CutFlowHisto
    //############################################################################################################
    CutFlowHisto::CutFlowHisto(const std::string& Name, const std::string& Region) : Histo(Name, "CutFlow " + Region) {}
    void CutFlowHisto::Fill(double xValue) {
        unsigned int BinMax = xValue + 1;
        for (unsigned int b = 1; b <= BinMax; ++b) { AddBinContent(b); }
    }
    void CutFlowHisto::Fill(double, double) { XAMPP::Warning("CutFlowHisto::Fill()", "2D-CutFlow does not makes sense"); }
    void CutFlowHisto::Fill(double, double, double) { XAMPP::Warning("CutFlowHisto::Fill()", "3D-CutFlow does not makes sense"); }
    bool CutFlowHisto::SetCuts(const std::vector<std::shared_ptr<Condition>>& Cuts) {
        std::vector<std::string> CutNames;
        for (auto& C : Cuts) { CutNames.push_back(C->name()); }
        return SetCuts(CutNames);
    }
    bool CutFlowHisto::init(TDirectory* D) {
        TDirectory* CutFlowDir = D;
        if (std::string(CutFlowDir->GetName()) != "InfoHistograms") {
            if (!D->GetDirectory("InfoHistograms/")) {
                XAMPP::Info("CutFlowHisto::init()", "Create InfoHistograms subdirectory in " + std::string(CutFlowDir->GetName()));
                D->mkdir("InfoHistograms/");
            }
            CutFlowDir = D->GetDirectory("InfoHistograms/");
        }
        PullOverFlowBins(false);
        PullUnderFlowBins(false);
        return Histo::init(CutFlowDir);
    }
    bool CutFlowHisto::SetCuts(const std::vector<std::string>& Cuts) {
        if (!HistoTemplates::getHistoTemplater()->IsTemplateDefined(GetTemplate())) {
            std::shared_ptr<TH1> h_CF =
                std::make_shared<TH1D>(GetTemplate().c_str(), "CutFlow", (int)Cuts.size() + 2, 0, (int)Cuts.size() + 2);
            h_CF->LabelsDeflate("X");
            if (!HistoTemplates::getHistoTemplater()->InsertTemplate(GetTemplate(), h_CF)) { return false; }
        } else {
            if (HistoTemplates::getHistoTemplater()->GetTemplate(GetTemplate())->GetNbinsX() != (int)Cuts.size() + 2) {
                Error("CutFlowHisto::SetCuts()",
                      "There already exists an template " + GetTemplate() + " but the binning does not match the number of cuts");
                return false;
            }
        }
        SetBinLabelX(1, "Initial");
        int bin = 2;
        for (const auto& C : Cuts) {
            SetBinLabelX(bin, C);
            ++bin;
        }
        SetBinLabelX(bin, "Final");
        return true;
    }
    std::shared_ptr<Histo> CutFlowHisto::Clone() { return std::make_shared<CutFlowHisto>(*this); }
    //############################################################################################################
    //                                  HistoArray
    //############################################################################################################
    HistoArray::HistoArray(const std::string& Name) :
        Histo(Name, ""),
        m_ClientHistos(),
        m_Cuts(),
        m_DoCutFlow(false),
        m_RawCutFlow(nullptr),
        m_WeightedCutFlow(nullptr) {}
    bool HistoArray::init(TDirectory* Directory) {
        if (Directory->GetDirectory(name().c_str())) {
            Error("HistoArray::init()", "The directory " + name() + "already exists. Do not want to clobber with existing content.");
            return false;
        }
        if (!m_Cuts.empty()) {
            PrintFooter();
            Info("HistoArray()", Form("Thee following cuts are needed to fullfill %s", name().c_str()));
            for (auto& C : m_Cuts) { Info("HistoArray()", Form("    -+- %s", C->name().c_str())); }
        }
        if (m_DoCutFlow) {
            m_RawCutFlow = std::make_shared<CutFlowHisto>("CutFlow", name());
            m_RawCutFlow->DoRaw();
            m_RawCutFlow->SetCuts(m_Cuts);
            if (!XAMPP::Weight::getWeighter()->GetUsedWeighters().empty()) {
                m_WeightedCutFlow = std::make_shared<CutFlowHisto>("CutFlow_weighted", name());
                m_WeightedCutFlow->SetCuts(m_Cuts);
            }
        }
        Info("HistoArray::init()", "Create new directory " + name() + ".");
        Directory->mkdir((name() + (m_DoCutFlow ? "/InfoHistograms/" : "")).c_str());
        TDirectory* D = Directory->GetDirectory(name().c_str());

        for (std::vector<std::shared_ptr<Histo>>::const_iterator itr = m_ClientHistos.begin(); itr != m_ClientHistos.end(); ++itr) {
            const std::shared_ptr<Histo> H = (*itr);
            unsigned int n_H = count<std::shared_ptr<Histo>>(itr + 1, m_ClientHistos.end(),
                                                             [H](const std::shared_ptr<Histo>& C) { return C->name() == H->name(); });
            if (n_H > 0) {
                Error("HistoArray::init()", Form("The histogram name %s has been defined %u other times", H->name().c_str(), n_H));
                return false;
            }
        }

        for (const auto& Histo : m_ClientHistos) {
            if (!Histo->init(D)) { return false; }
        }
        if (m_DoCutFlow) {
            if (m_RawCutFlow && !m_RawCutFlow->init(D)) return false;
            if (m_WeightedCutFlow && !m_WeightedCutFlow->init(D)) return false;
        }

        return true;
    }
    void HistoArray::FillCutFlowHistos(unsigned int Passed) {
        if (m_RawCutFlow) m_RawCutFlow->Fill(Passed);
        if (m_WeightedCutFlow) m_WeightedCutFlow->Fill(Passed);
    }
    void HistoArray::fill() {
        unsigned int Passed = 0;
        for (const auto& C : m_Cuts) {
            if (!C->Pass()) {
                FillCutFlowHistos(Passed);
                return;
            }
            ++Passed;
        }
        FillCutFlowHistos(Passed + 1);
        for (const auto& Histo : m_ClientHistos) { Histo->fill(); }
    }
    void HistoArray::write() {
        for (const auto& Histo : m_ClientHistos) { Histo->write(); }
        if (m_RawCutFlow) m_RawCutFlow->write();
        if (m_WeightedCutFlow) m_WeightedCutFlow->write();
    }
    void HistoArray::Statistics() const {
        for (const auto& Histo : m_ClientHistos) { Histo->Statistics(); }
        if (m_RawCutFlow) m_RawCutFlow->Statistics();
        if (m_WeightedCutFlow) m_WeightedCutFlow->Statistics();
    }
    void HistoArray::AppendHistogram(const std::vector<std::shared_ptr<Histo>>& Histos) {
        for (auto& H : Histos) { AppendHistogram(H); }
    }
    void HistoArray::AppendHistogram(std::shared_ptr<Histo> In) { m_ClientHistos.push_back(In->Clone()); }

    void HistoArray::SetCut(const std::vector<std::shared_ptr<Condition>>& Cuts) { CopyVector(Cuts, m_Cuts, true); }
    HistoArray::~HistoArray() {}
    void HistoArray::DoCutFlow(bool B) { m_DoCutFlow = B; }
    std::shared_ptr<Histo> HistoArray::Clone() { return std::make_shared<HistoArray>(*this); }

}  // namespace XAMPP
