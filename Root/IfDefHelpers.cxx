#include <XAMPPplotting/IfDefHelpers.h>
#include <XAMPPplotting/PlottingUtils.h>
#include <XAMPPplotting/Weight.h>

namespace XAMPP {
    //###################################################################
    //                       IfDefFlags
    //###################################################################
    IfDefFlags* IfDefFlags::m_Inst = nullptr;
    IfDefFlags::IfDefFlags() : m_Flags(), m_Variables(), m_VarToRead() {}
    IfDefFlags::~IfDefFlags() { m_Inst = nullptr; }
    IfDefFlags* IfDefFlags::Instance() {
        if (!m_Inst) m_Inst = new IfDefFlags();
        return m_Inst;
    }
    void IfDefFlags::AddFlag(const std::string& Flag) {
        if (!IsFlagDefined(Flag)) {
            Info("IfDefFlags::AddFlag()", Flag);
            m_Flags.push_back(Flag);
        }
    }
    bool IfDefFlags::IsFlagDefined(const std::string& Flag) const { return IsElementInList(m_Flags, Flag); }
    bool IfDefFlags::IsVariableDefined(const std::string& variable) const { return RetrieveVariable(variable) != nullptr; }
    bool IfDefFlags::DefineVariable(const std::string& varname, const StringVector& values) {
        Variable_Ptr V_Ptr = RetrieveVariable(varname);
        if (V_Ptr && V_Ptr->nLines() > 1) {
            Error("IfDefFlags()", "The variable " + varname + " already has been defined containing");
            V_Ptr->Print();
            PrintFooter();
            return false;
        } else if (V_Ptr) {
            Info("IfDefFlags()", "Overwrite " + varname + " since it has only one single value");
            RemoveElement(m_Variables, V_Ptr);
        }
        V_Ptr = Variable_Ptr(new IfDefVariable(varname, values));

        if (!V_Ptr->isValid()) {
            Error("IfDefFlags()", "Please give something to store in " + varname);
            return false;
        }
        Info("IfDefFlags()", "New variable " + varname + " has been defined containing:");
        V_Ptr->Print();
        m_Variables.push_back(V_Ptr);
        return true;
    }
    bool IfDefFlags::allVarsValid() const {
        for (auto& V : m_Variables) {
            if (!V->isValid()) return false;
        }
        return true;
    }

    Variable_Ptr IfDefFlags::RetrieveVariable(const std::string& varName) const {
        for (auto& V : m_Variables) {
            if (V->name() == varName) return V;
        }
        return Variable_Ptr();
    }

    bool IfDefFlags::HasVariableCached(std::string& line) {
        if (!m_VarToRead) return false;
        if (m_VarToRead->Next(line)) return true;
        m_VarToRead = Variable_Ptr();
        return false;
    }
    bool IfDefFlags::CacheVariable(const std::string& name) {
        m_VarToRead = RetrieveVariable(name);
        if (!m_VarToRead || !m_VarToRead->isValid()) {
            Error("IfDefLineParser()", "The variable " + name + " has not been defined before");
            return false;
        }
        m_VarToRead->clearRegister();
        return true;
    }
    //##################################################################
    //                      IfDefVariable
    //##################################################################
    IfDefVariable::IfDefVariable(const std::string& name, const StringVector& values) :
        m_name(name),
        m_lines(values),
        m_cross_ref(nullptr),
        m_isValid(true),
        m_LineToRead(-1) {
        // Empty variables are useless....
        m_isValid = !m_lines.empty();
    }
    void IfDefVariable::Print() const {
        for (const auto& V : m_lines) Info(name(), WhiteSpaces(6) + V);
    }

    std::string IfDefVariable::name() const { return m_name; }
    bool IfDefVariable::Next(std::string& line) {
        if (!isValid()) {
            Error("IfDefVariable()", name() + " is ill defined for some reason");
            return false;
        }
        if (m_cross_ref) {
            if (m_cross_ref->Next(line)) return true;
            m_cross_ref = Variable_Ptr();
        }
        if (m_LineToRead >= nLines()) return false;

        const std::string& cache = m_lines.at(m_LineToRead);
        ++m_LineToRead;
        size_t AtFound = cache.find("@");
        if (AtFound == std::string::npos) {
            line = cache;
            return true;
        } else if (AtFound == 0 && cache.find("@{") == std::string::npos) {
            std::stringstream sstr(cache);
            std::string Var = GetWordFromStream(sstr).substr(1, std::string::npos);
            Variable_Ptr V_Ptr = IfDefFlags::Instance()->RetrieveVariable(Var);
            if (V_Ptr.get() == this) {
                Error("IfDefVariable()", "Captain infinite loop ahead. You want to include myself again " + name());
                m_isValid = false;
                return false;
            } else if (V_Ptr == nullptr) {
                Error("IfDefVariable()", Var + " does not exist to be inlcuded in " + name());
                m_isValid = false;
                return false;
            }
            m_cross_ref = V_Ptr;
            m_cross_ref->clearRegister();
            return m_cross_ref->Next(line);
        }
        line = cache;
        size_t found_at_brack = line.find("@{");

        while (isValid() && found_at_brack != std::string::npos) {
            std::string VarName = line.substr(found_at_brack + 2, line.find("}", found_at_brack) - found_at_brack - 2);
            Variable_Ptr V_Ptr = IfDefFlags::Instance()->RetrieveVariable(VarName);
            if (!V_Ptr || !V_Ptr->isValid()) {
                Error("IfDefVariable()", "Could not find " + VarName);
                m_isValid = false;
                return false;
            } else if (V_Ptr->nLines() > 1) {
                Error("IfDefVariable()", V_Ptr->name() + " is more complex than one line. How shall  I replace that from " + cache);
                m_isValid = false;
                return false;
            } else
                line = ReplaceExpInString(line, line.substr(found_at_brack, line.find("}", found_at_brack) - found_at_brack + 1),
                                          V_Ptr->getFirstElement());
            found_at_brack = line.find("@{");
        }
        return true;
    }
    std::string IfDefVariable::getFirstElement() const {
        if (m_lines.empty()) return std::string();
        return m_lines.at(0);
    }
    bool IfDefVariable::isValid() const { return m_isValid; }
    unsigned int IfDefVariable::nLines() const { return m_lines.size(); }
    void IfDefVariable::clearRegister() { m_LineToRead = 0; }
    //##################################################################
    //                      IfDefLineParser
    //##################################################################
    IfDefLineParser::IfDefLineParser() : m_InErrorState(false), m_ResolveVars(true), m_GlobalSkip(false) {}

    int IfDefLineParser::SkipLine(const std::string& Line) {
        std::stringstream sstr(Line);
        // Skip lines because flags are not set in the config file
        bool ifDef = IsKeyWordSatisfied(sstr, "ifdef");
        bool ifnDef = IsKeyWordSatisfied(sstr, "ifndef");
        bool Skip = false;
        if (ifnDef || ifDef) {
            if (m_GlobalSkip) return 1;
            std::string Flag(GetWordFromStream(sstr));
            // Global flags to be applied on data / MC
            if (Flag == "isMC" && Weight::getWeighter()->isData() == ifDef)
                Skip = true;
            else if (Flag == "isData" && Weight::getWeighter()->isData() != ifDef)
                Skip = true;

            // The user can set via define flags. Now we can ask specific blocks of whether they are true or not
            else if (Flag != "isData" && Flag != "isMC") {
                if (ifDef && !IfDefFlags::Instance()->IsFlagDefined(Flag))
                    Skip = true;
                else if (ifnDef && IfDefFlags::Instance()->IsFlagDefined(Flag))
                    Skip = true;
            }
            if (Skip) {
                Info("IfDefLineParser()",
                     "Found unsatisfied " + std::string(ifnDef ? "ifndef" : "ifdef") + " " + Flag + " expression. Skipping the next lines");
            }
        }
        // The next big feature. Compare the values of variables against each other
        // Search for ifVar statements in thte config file
        // Syntax ifVar <VariableName> == /!= <Expression>
        else if (IsKeyWordSatisfied(sstr, "ifVar")) {
            if (m_GlobalSkip) return 1;
            std::string VarName(GetWordFromStream(sstr)), Logic(GetWordFromStream(sstr)), Value(GetWordFromStream(sstr));
            Variable_Ptr ToCheck = IfDefFlags::Instance()->RetrieveVariable(VarName);
            // Check if the variable exists and has length 1
            if (!ToCheck) {
                Error("IfDefLineParser()", "Variable " + VarName + " has not been defined before");
                m_InErrorState = true;
                return false;
            } else if (ToCheck->nLines() > 1) {
                Error("IfDefLineParser()", "The variable " + VarName + " is not suitable for expression comparison.");
                m_InErrorState = true;
            }
            // Now let's check the operator
            bool Equal = (Logic == std::string("=") || Logic == std::string("=="));
            bool InEqual = (Logic == std::string("!=") || Logic == std::string("!"));
            if (!Equal && !InEqual) {
                Error("IfDefLineParser()", "Please give a  valid logic. Either '==', '='  or '!=', '!'");
                m_InErrorState = true;
            }
            Skip = (Equal && ToCheck->getFirstElement() != Value) || (InEqual && ToCheck->getFirstElement() == Value);
            if (Skip) {
                Info("IfDefLineParser()", Form("Skip the next lines because the expression  %s(%s) %s %s is not satisfied",
                                               ToCheck->name().c_str(), ToCheck->getFirstElement().c_str(), Logic.c_str(), Value.c_str()));
            }
        } else if (IsKeyWordSatisfied(sstr, "endif"))
            return -1;
        if (Skip) {
            m_GlobalSkip = true;
            return 1;
        }
        return 0;
    }
    bool IfDefLineParser::SkipLines(std::ifstream& inf, std::string& Line) {
        int Found = SkipLine(Line);
        while (Found > 0 && XAMPP::GetLine(inf, Line)) { Found += SkipLine(Line); }
        m_GlobalSkip = false;
        if (Found > 0) {
            Error("IfDefLineParser()", "Missing 'endif' statement.");
            m_InErrorState = true;
            return false;
        }
        return true;
    }
    IfDefLineParser::LineStatus IfDefLineParser::operator()(std::ifstream& inf, std::string& Line) {
        if (m_InErrorState) {
            Error("IfDefLineParser()", "Parsing lead to an error");
            return IfDefLineParser::ReadError;
        }
        //##########################################################
        //  A variable has been called which shall be read now,    #
        // instead of reading further through the config file.     #
        //##########################################################
        if (IfDefFlags::Instance()->HasVariableCached(Line)) {
            int Found = SkipLine(Line);
            while (Found > 0 && IfDefFlags::Instance()->HasVariableCached(Line)) { Found += SkipLine(Line); }
            m_GlobalSkip = false;
            if (Found > 0) {
                m_InErrorState = true;
                Error("IfDefLineParser()", "Unsatisfied ifdef satement inside variable found");
                return IfDefLineParser::ReadError;
            }
            return IfDefLineParser::NewProperty;
        }
        if (!XAMPP::GetLine(inf, Line)) {
            // Check at the end of the file if all variables have been resolved without any error"
            if (IfDefFlags::Instance()->allVarsValid()) return IfDefLineParser::EndOfFile;
            m_InErrorState = true;
            return IfDefLineParser::ReadError;
        } else if (!SkipLines(inf, Line)) {
            m_InErrorState = true;
            return IfDefLineParser::ReadError;
        }
        while (m_ResolveVars && !m_InErrorState && Line.find("@{") != std::string::npos) {
            std::string VarName = Line.substr(Line.find("@{") + 2, Line.find("}") - Line.find("@{") - 2);
            Variable_Ptr ToReplace = IfDefFlags::Instance()->RetrieveVariable(VarName);
            if (!ToReplace) {
                Error("IfDefLineParser()", "The variable " + VarName + " does not exist");
                m_InErrorState = true;
            } else if (!ToReplace->isValid() || ToReplace->nLines() > 1) {
                Error("IfDefLineParser()", "The variable " + VarName + " is not suitable to be replaced within one line");
                m_InErrorState = true;
            } else
                Line = ReplaceExpInString(Line, Line.substr(Line.find("@{"), Line.find("}") - Line.find("@{") + 1),
                                          ToReplace->getFirstElement());
        }
        std::stringstream sstr(Line);
        //################################################################
        //  Variables are defined via @ first symbol in the line         #
        //  Load the variable now from the cache and return its content. #
        //################################################################
        if (m_ResolveVars && Line.find("@") == 0) {
            std::string Var = GetWordFromStream(sstr).substr(1, std::string::npos);
            if (!IfDefFlags::Instance()->CacheVariable(Var)) m_InErrorState = true;
            return read(inf, Line);
        }

        //############################################
        //  Finally we need to define the variables  #
        //############################################
        else if (IsKeyWordSatisfied(sstr, "defineVar")) {
            std::string VarName = GetWordFromStream(sstr);
            std::string VarContent = Line.substr(Line.find(VarName) + VarName.size(), std::string::npos);
            VarContent = EraseWhiteSpaces(VarContent);
            if (!IfDefFlags::Instance()->DefineVariable(VarName, StringVector{VarContent})) m_InErrorState = true;
            return read(inf, Line);
        } else if (IsKeyWordSatisfied(sstr, "defineMultiLine")) {
            m_ResolveVars = false;
            std::string VarName = GetWordFromStream(sstr);
            StringVector VarContent;
            while (read(inf, Line) == IfDefLineParser::NewProperty) {
                sstr = std::stringstream(Line);
                if (IsKeyWordSatisfied(sstr, "End_MultiLine")) {
                    m_ResolveVars = true;
                    if (!IfDefFlags::Instance()->DefineVariable(VarName, VarContent)) {
                        m_InErrorState = true;
                    } else
                        return read(inf, Line);
                }
                VarContent.push_back(Line);
            }
            Error("IfDefLineParser()", "Missing 'End_MultiLine' statement");
            m_InErrorState = true;
        } else if (IsKeyWordSatisfied(sstr, "define"))
            AddFlag(GetWordFromStream(sstr));
        return IfDefLineParser::NewProperty;
    }
    IfDefLineParser::LineStatus IfDefLineParser::read(std::ifstream& inf, std::string& line) { return this->operator()(inf, line); }
    void IfDefLineParser::AddFlag(const std::string& Flag) { IfDefFlags::Instance()->AddFlag(Flag); }
}  // namespace XAMPP
