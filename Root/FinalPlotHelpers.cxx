#include <XAMPPplotting/FinalPlotHelpers.h>
#include <XAMPPplotting/PlottingUtils.h>

#include <TFile.h>
#include <TH1.h>
namespace XAMPP {
    FileHandler* FileHandler::m_Inst = nullptr;
    std::shared_ptr<TFile> FileHandler::LoadFile(const std::string& path) {
        std::string resolved_path = EraseWhiteSpaces(ResolvePath(path));
        for (const auto& structure : m_opened_files) {
            if (EraseWhiteSpaces(structure->reference()->GetName()) == resolved_path) return structure->reference();
        }
        std::shared_ptr<TFile> F(TFile::Open(resolved_path.c_str()));
        if (!F || !F->IsOpen()) {
            Error("FileHandler::getFile()", Form("Could not open file %s", path.c_str()));
            return std::shared_ptr<TFile>();
        }
        FileStructure_Ptr structure = std::make_shared<FileStructure>(F);
        m_opened_files.push_back(structure);
        if (showMsg()) Info("FileHandler::getFile()", Form("Open file %s", resolved_path.c_str()));
        return F;
    }
    std::vector<std::shared_ptr<TFile>> FileHandler::OpenedFiles() const {
        std::vector<std::shared_ptr<TFile>> Files;
        for (const auto& F : m_opened_files) Files.push_back(F->reference());
        return Files;
    }
    void FileHandler::switchOffMsg() { m_showMessages = false; }
    bool FileHandler::showMsg() const { return m_showMessages; }

    FileHandler* FileHandler::getInstance() {
        if (m_Inst == nullptr) m_Inst = new FileHandler();
        return m_Inst;
    }
    FileHandler::~FileHandler() {
        closeAll();
        delete SystematicPairer::getInstance();
        m_Inst = nullptr;
    }
    FileHandler::FileHandler() : m_opened_files(), m_known_vars(), m_showMessages(true) {}
    std::vector<FileStructure_Ptr> FileHandler::FileStructures() const { return m_opened_files; }
    FileStructure_Ptr FileHandler::GetFileStructure(const std::string& path) { return GetFileStructure(LoadFile(path)); }
    FileStructure_Ptr FileHandler::GetFileStructure(const std::shared_ptr<TFile>& File) const { return GetFileStructure(File.get()); }
    FileStructure_Ptr FileHandler::GetFileStructure(const TFile* File) const {
        for (const auto& structure : m_opened_files) {
            if (structure->reference().get() == File) return structure;
        }
        return FileStructure_Ptr();
    }
    void FileHandler::closeAll() {
        if (showMsg()) Info("FileHandler()", "Close all ROOT files");
        m_opened_files.clear();
    }
    void FileHandler::reset() { delete FileHandler::getInstance(); }

    bool FileHandler::variable_known(const std::string& var) const { return m_known_vars.find(var) != m_known_vars.end(); }
    void FileHandler::new_variable(const std::string& var) { m_known_vars.insert(var); }

    //##################################################################
    //                  SystematicPairer
    //##################################################################
    SystematicPairer* SystematicPairer::m_Inst = nullptr;

    SystematicPairer* SystematicPairer::getInstance() {
        if (m_Inst == nullptr) m_Inst = new SystematicPairer();
        return m_Inst;
    }
    const std::vector<std::string>& SystematicPairer::up_variation_endings() {
        static std::vector<std::string> up_endings{"1Up", "1up", "1UP"};
        return up_endings;
    }
    const std::vector<std::string>& SystematicPairer::down_variation_endings() {
        static std::vector<std::string> down_endings{"1Down", "1Dn", "1DN", "1DOWN", "1down"};
        return down_endings;
    }
    void SystematicPairer::append_systematic(const std::string& name) {
        if (name.empty() || is_systematic_known(name)) return;
        m_known_syst.push_back(name);
        std::sort(m_known_syst.begin(), m_known_syst.end());
        // Patterns how the systematic name should end if it is pairable
        // We've an underscore in the name. That's at least a tiny chance to have a paiable systematic
        if (name.find("_") != std::string::npos) {
            std::string ending = name.substr(name.rfind("_") + 1, std::string::npos);
            // Check if it's either an upwards or a downwards variation
            bool isDown = IsElementInList(down_variation_endings(), ending);
            bool isUp = IsElementInList(up_variation_endings(), ending);
            if (isDown || isUp) {
                // Search for the best matching systematic
                //  <SYST>__1DOWN  vs. <SYST>__1UP
                for (const auto& to_pair : m_known_syst) {
                    if (to_pair == name) continue;
                    std::string common_str = LongestCommonString(to_pair, name);
                    if (common_str.substr(0, common_str.size() - 1) + ending == name) {
                        // Sometimes people do stupid things
                        if (!get_paired(to_pair).empty()) {
                            throw std::runtime_error("The systematic " + to_pair + " is already paired with " + get_paired(to_pair) + ".");
                        } else {
                            pair_systematics(isDown ? name : to_pair, isUp ? name : to_pair);
                            break;
                        }
                    }
                }
            }
        }
    }
    SystematicPairer::SystematicPairer() :
        m_known_syst(),
        m_excluded_syst(),
        m_paired_syst(),
        m_envelope_grps(),
        m_syst_up_hashes(),
        m_syst_dn_hashes(),
        m_syst_titles(),
        m_doSyst(true) {}

    std::string SystematicPairer::get_syst_title(const std::string& syst_name) const {
        std::map<std::string, std::string>::const_iterator itr = m_syst_titles.find(syst_name);
        if (itr != m_syst_titles.end()) return itr->second;
        return syst_name;
    }
    void SystematicPairer::set_systematic_title(const std::string& syst_name, const std::string& syst_title) {
        if (get_syst_title(syst_name) == syst_name) {
            Info("SystematicPairer::set_systematic_title()",
                 Form("Giving systematic %s the title %s", syst_name.c_str(), syst_title.c_str()));
            m_syst_titles.insert(VariationPair(syst_name, syst_title));
        }
    }
    std::vector<std::shared_ptr<SystematicEnvelope>> SystematicPairer::get_envelopes() const { return m_envelope_grps; }
    SystematicPairer::~SystematicPairer() { m_Inst = nullptr; }
    bool SystematicPairer::is_systematic_known(const std::string& syst) const { return IsElementInList(m_known_syst, syst); }
    bool SystematicPairer::is_systematic_up(const std::string& syst) const {
        if (IsElementInList(m_syst_up_hashes, std::hash<std::string>()(syst))) return true;
        if (syst.find("_") != std::string::npos) {
            std::string ending = syst.substr(syst.rfind("_") + 1, std::string::npos);
            return IsElementInList(up_variation_endings(), ending);
        }
        return false;
    }
    bool SystematicPairer::is_systematic_down(const std::string& syst) const {
        if (IsElementInList(m_syst_dn_hashes, std::hash<std::string>()(syst))) return true;
        if (syst.find("_") != std::string::npos) {
            std::string ending = syst.substr(syst.rfind("_") + 1, std::string::npos);
            if (!IsElementInList(down_variation_endings(), ending)) return false;
            return true;
        }
        return false;
    }
    std::string SystematicPairer::get_paired(const std::string& syst) const {
        if (!is_systematic_excluded(syst) && (is_systematic_down(syst) || is_systematic_up(syst))) {
            for (const auto& paired : m_paired_syst) {
                const std::string* found_syst = nullptr;
                if (paired.first == syst)
                    found_syst = &paired.second;
                else if (paired.second == syst)
                    found_syst = &paired.first;
                if (found_syst == nullptr) continue;
                if (is_systematic_excluded(*found_syst)) break;
                return *found_syst;
            }
        }
        return std::string();
    }
    void SystematicPairer::exclude_systematic(const std::string& syst) {
        if (!syst.empty() && !is_systematic_excluded(syst)) m_excluded_syst.push_back(syst);
    }
    bool SystematicPairer::is_systematic_excluded(const std::string& syst) const { return IsElementInList(m_excluded_syst, syst); }
    std::shared_ptr<SystematicEnvelope> SystematicPairer::find_envelope(const std::string& syst) const {
        for (const auto& env : m_envelope_grps) {
            if (env->consider_in_envelope(syst)) return env;
        }
        return std::shared_ptr<SystematicEnvelope>();
    }
    bool SystematicPairer::pair_systematics(const std::string& down, const std::string& up) {
        if (!get_paired(down).empty() || !get_paired(up).empty() || down.empty() || up.empty()) return false;
        m_syst_up_hashes.push_back(std::hash<std::string>()(up));
        m_syst_dn_hashes.push_back(std::hash<std::string>()(down));
        m_paired_syst.push_back(VariationPair(down, up));
        bool sort = false;
        if (!is_systematic_known(down)) {
            m_known_syst.push_back(down);
            sort = true;
        }
        if (!is_systematic_known(up)) {
            m_known_syst.push_back(up);
            sort = true;
        }
        if (sort) std::sort(m_known_syst.begin(), m_known_syst.end());
        return true;
    }
    bool SystematicPairer::create_envelope(const std::string& env_pattern, const std::string& grp_name) {
        std::shared_ptr<SystematicEnvelope> existing = find_envelope(env_pattern);
        if (existing.get() != nullptr) return existing->get_pattern() == env_pattern;
        std::shared_ptr<SystematicEnvelope> new_env = std::make_shared<SystematicEnvelope>(env_pattern, grp_name);
        for (const auto& env : get_envelopes()) {
            if (new_env->consider_in_envelope(env->get_pattern())) return false;
        }
        m_envelope_grps.push_back(new_env);
        Info("SystematicPairer()",
             "Created new systematic envelope group. All systematics which satisfy '" + env_pattern + "' will be added");
        return true;
    }
    bool SystematicPairer::create_group(const std::string& grp_pattern, const std::string& grp_name, unsigned int sel_only) {
        std::shared_ptr<SystematicEnvelope> existing = find_envelope(grp_pattern);
        if (existing.get() != nullptr) return existing->get_pattern() == grp_pattern;
        if (sel_only != SystematicGroup::Any && sel_only != SystematicGroup::UpOnly && sel_only != SystematicGroup::DownOnly) return false;
        std::shared_ptr<SystematicEnvelope> new_env = std::make_shared<SystematicGroup>(grp_pattern, grp_name, sel_only);
        for (const auto& env : get_envelopes()) {
            if (new_env->consider_in_envelope(env->get_pattern())) return false;
        }
        m_envelope_grps.push_back(new_env);
        Info("SystematicPairer()",
             "Created new systematic envelope group. All systematics which satisfy '" + grp_pattern + "' will be added");
        return true;
    }
    bool SystematicPairer::create_deviation(const std::string& grp_pattern, const std::string& grp_name, unsigned int sel_only) {
        std::shared_ptr<SystematicEnvelope> existing = find_envelope(grp_pattern);
        if (existing.get() != nullptr) return existing->get_pattern() == grp_pattern;
        if (sel_only != SystematicGroup::Any && sel_only != SystematicGroup::UpOnly && sel_only != SystematicGroup::DownOnly) return false;
        std::shared_ptr<SystematicEnvelope> new_env = std::make_shared<SystematicDeviation>(grp_pattern, grp_name, sel_only);
        for (const auto& env : get_envelopes()) {
            if (new_env->consider_in_envelope(env->get_pattern())) return false;
        }
        m_envelope_grps.push_back(new_env);
        Info("SystematicPairer()",
             "Created new systematic envelope group. All systematics which satisfy '" + grp_pattern + "' will be added");
        return true;
    }

    bool SystematicPairer::do_systematics() const { return m_doSyst; }
    bool SystematicPairer::is_systematic_envelope(const std::string& syst) const {
        for (const auto& env : m_envelope_grps) {
            if (env->consider_in_envelope(syst)) return true;
        }
        return false;
    }
    void SystematicPairer::Print() const {
        if (!do_systematics()) return;
        PrintFooter();
        Info("SystematicPairer::Print()", "Found the following systematics");
        PrintFooter();
        size_t max_down_size = 0;
        for (const auto& syst : get_systematics()) {
            if (is_systematic_down(syst) && syst.size() > max_down_size && !get_paired(syst).empty()) max_down_size = syst.size();
        }
        Info("SystematicPairer::Print()", "Asymmetric systematics:");
        for (const auto& syst : get_systematics()) {
            if (is_systematic_envelope(syst)) continue;
            std::string paired_syst = get_paired(syst);
            if (paired_syst.empty() || is_systematic_up(syst)) continue;
            Info("SystematicPairer::Print()",
                 Form("    ***   %s%s---  %s", syst.c_str(), WhiteSpaces((max_down_size - syst.size()) + 3).c_str(), paired_syst.c_str()));
        }
        Info("SystematicPairer::Print()", "Symmetric systematics:");

        for (const auto& syst : get_systematics()) {
            if (get_paired(syst).empty() && !is_systematic_envelope(syst) && !is_systematic_excluded(syst))
                Info("SystematicPairer::Print()", Form("    +++   %s", syst.c_str()));
        }
        for (const auto& env : get_envelopes()) { env->Print(); }
        PrintFooter();
        PrintFooter();
        PrintFooter();
    }
    const std::vector<std::string>& SystematicPairer::get_systematics() const { return m_known_syst; }
    void SystematicPairer::do_systematics(bool B) { m_doSyst = B; }

    //##################################################################
    //                  SystematicEnvelope
    //##################################################################
    SystematicEnvelope::SystematicEnvelope(const std::string& pattern, const std::string& grp_name) :
        m_pattern(pattern),
        m_name(grp_name),
        m_syst(),
        m_site(SystematicEnvelope::Any) {}
    bool SystematicEnvelope::add_to_up() const { return m_site & SystematicEnvelope::UpOnly; }
    bool SystematicEnvelope::add_to_down() const { return m_site & SystematicEnvelope::DownOnly; }
    void SystematicEnvelope::apply_only_to(unsigned int direction) {
        if ((direction | SystematicEnvelope::Any) == SystematicEnvelope::Any) m_site = direction;
    }
    bool SystematicEnvelope::consider_in_envelope(const std::string& systematic) const {
        if (SystematicPairer::getInstance()->is_systematic_excluded(systematic)) return false;
        if (IsElementInList(m_syst, systematic)) return true;
        if (m_site != SystematicEnvelope::Any) {
            std::string ending = systematic.substr(systematic.rfind("_") + 1, std::string::npos);
            // Check if it's either an upwards or a downwards variation
            if (m_site == VariationSite::DownOnly && !IsElementInList(SystematicPairer::getInstance()->down_variation_endings(), ending))
                return false;
            else if (m_site == VariationSite::UpOnly && !IsElementInList(SystematicPairer::getInstance()->up_variation_endings(), ending))
                return false;
        }
        return (!m_pattern.empty() && systematic.find(m_pattern) == 0);
    }
    std::string SystematicEnvelope::name() const {
        if (m_name.empty()) return get_pattern();
        return m_name;
    }
    std::string SystematicEnvelope::get_pattern() const { return m_pattern; }
    void SystematicEnvelope::append_systematic(const std::string& syst) {
        if (!consider_in_envelope(syst)) m_syst.push_back(syst);
    }
    void SystematicEnvelope::add_up(unsigned int N, double diff, std::shared_ptr<TH1> envelope_histo) const {
        if (std::fabs(diff) > envelope_histo->GetBinContent(N)) { envelope_histo->SetBinContent(N, std::fabs(diff)); }
    }

    std::shared_ptr<TH1> SystematicEnvelope::calculate_syst(std::map<std::string, std::shared_ptr<TH1>>& syst_map,
                                                            std::shared_ptr<TH1> nominal) const {
        std::shared_ptr<TH1> envelope_histo = clone(nominal, true);
        for (std::map<std::string, std::shared_ptr<TH1>>::iterator syst_pair = syst_map.begin(); syst_pair != syst_map.end();) {
            if (consider_in_envelope(syst_pair->first)) {
                for (int N = GetNbins(nominal); N >= 0; --N) {
                    // take the envelope of the systematic histogram
                    if (isAlphaNumeric(nominal) && isOverflowBin(nominal, N)) continue;
                    double diff = std::fabs(syst_pair->second->GetBinContent(N) - nominal->GetBinContent(N));
                    add_up(N, diff, envelope_histo);
                }
                // Erase the systematic from the map
                syst_pair = syst_map.erase(syst_pair);
            } else
                ++syst_pair;
        }
        return envelope_histo;
    }
    void SystematicEnvelope::Print() const {
        Info("SystematicEnvelope()", Form("The following systematics  belong  to '%s'", get_pattern().c_str()));
        for (const auto& syst : SystematicPairer::getInstance()->get_systematics()) {
            if (consider_in_envelope(syst)) Info("SystematicEnvelope()", Form("    ===  %s", syst.c_str()));
        }
    }

    //##################################################################
    //                  SystematicGroup
    //##################################################################
    SystematicGroup::SystematicGroup(const std::string& pattern, const std::string& env_name, unsigned int s) :
        SystematicEnvelope(pattern, env_name) {
        apply_only_to(s);
    }
    void SystematicGroup::add_up(unsigned int N, double diff, std::shared_ptr<TH1> envelope_histo) const {
        envelope_histo->SetBinContent(N, std::sqrt(envelope_histo->GetBinContent(N) * envelope_histo->GetBinContent(N) + diff * diff));
    }
    //##################################################################
    //                  SystematicDeviation
    //####################################################################

    SystematicDeviation::SystematicDeviation(const std::string& pattern, const std::string& env_name, unsigned int s) :
        SystematicGroup(pattern, env_name, s) {}
    std::shared_ptr<TH1> SystematicDeviation::calculate_syst(std::map<std::string, std::shared_ptr<TH1>>& syst_map,
                                                             std::shared_ptr<TH1> nominal) const {
        unsigned int s_before = syst_map.size();
        std::shared_ptr<TH1> sys = SystematicEnvelope::calculate_syst(syst_map, nominal);
        unsigned int s_after = syst_map.size();
        if (s_before - s_after > 1) sys->Scale(1 / std::sqrt(s_before - s_after - 1));
        return sys;
    }
    //##################################################################
    //                  PlotAnalysis
    //##################################################################
    PlotAnalysis::PlotAnalysis(const FileStructure* ref_file, const std::string& ana_name) :
        m_reference(ref_file),
        m_name(ana_name),
        m_nominal(),
        m_systematics(),
        m_regions() {
        setNominal("Nominal");
        if (m_regions.empty()) update_regions();
    }
    std::string PlotAnalysis::name() const { return m_name; }
    std::string PlotAnalysis::nominal() const { return m_nominal; }
    const FileStructure* PlotAnalysis::get_reference() const { return m_reference; }
    std::shared_ptr<TFile> PlotAnalysis::get_file() const { return get_reference()->reference(); }
    void PlotAnalysis::setNominal(const std::string& syst) {
        std::vector<std::string> syst_candidates;
        for (const auto& key : *get_file()->GetListOfKeys()) {
            TDirectory* dir = nullptr;
            get_file()->GetObject(key->GetName(), dir);
            if (dir == nullptr) continue;
            std::string key_name = key->GetName();
            if (key_name.find(name()) != 0) continue;
            syst_candidates.push_back(key_name.substr(name().size(), std::string::npos));
            delete dir;
        }
        // Remove the trailing _ from the list
        for (auto& sys : syst_candidates) {
            while (sys.find("_") == 0) sys = sys.substr(1, std::string::npos);
            SystematicPairer::getInstance()->append_systematic(sys);
        }
        if (IsElementInList(syst_candidates, syst)) {
            m_nominal = syst;
            update_regions();
        }
        RemoveElement(syst_candidates, nominal());
        if (syst_candidates.size() > m_systematics.size()) {
            CopyVector(syst_candidates, m_systematics, true);
            std::sort(m_systematics.begin(), m_systematics.end());
        }
    }
    std::vector<std::string> PlotAnalysis::get_systematics() const {
        std::vector<std::string> syst;
        // Only parse not excluded systematics
        for (const auto& s : m_systematics) {
            if (!SystematicPairer::getInstance()->is_systematic_excluded(s)) syst.push_back(s);
        }
        return syst;
    }
    void PlotAnalysis::update_regions() {
        std::vector<PlotRegion_Ptr> region_candidates;
        TDirectory* nominal_dir = nullptr;
        get_file()->GetObject((name() + std::string(!nominal().empty() ? "_" : "") + nominal()).c_str(), nominal_dir);
        if (nominal_dir == nullptr) {
            if (FileHandler::getInstance()->showMsg())
                Warning("PlotAnalysis()",
                        Form("%s is not contained in %s", (name() + std::string(!nominal().empty() ? "_" : "") + nominal()).c_str(),
                             get_file()->GetName()));
            return;
        }
        for (const auto& key : *nominal_dir->GetListOfKeys()) {
            TDirectory* dir = nullptr;
            nominal_dir->GetObject(key->GetName(), dir);
            if (dir == nullptr) continue;
            region_candidates.push_back(std::make_shared<PlotRegion>(dir));
            delete dir;
        }
        delete nominal_dir;
        CopyVector(region_candidates, m_regions, true);
    }
    std::vector<PlotRegion_Ptr> PlotAnalysis::get_regions() const { return m_regions; }
    PlotRegion_Ptr PlotAnalysis::get_region(const std::string& reg_name) const {
        for (const auto& reg : m_regions) {
            if (reg->name() == reg_name) return reg;
        }
        Warning("PlotAnalysis()", Form("The region %s is unkown to %s", reg_name.c_str(), name().c_str()));
        return PlotRegion_Ptr();
    }
    std::vector<std::string> PlotAnalysis::get_region_names() const {
        std::vector<std::string> regions;
        regions.reserve(m_regions.size());
        for (const auto& reg : m_regions) regions.push_back(reg->name());
        return regions;
    }
    std::vector<std::string> PlotAnalysis::get_variables(const std::string& region_name) const {
        PlotRegion_Ptr r = get_region(region_name);
        if (r) return r->variables();
        Warning("PlotAnalysis()", Form("Unkown region %s", region_name.c_str()));
        return std::vector<std::string>();
    }
    //##################################################################
    //                      FileStructure
    //##################################################################
    std::shared_ptr<TFile> FileStructure::reference() const { return m_reference_file; }
    std::vector<PlotAnalysis_Ptr> FileStructure::get_analyses() const { return m_analyses; }
    std::vector<std::string> FileStructure::get_analyses_names() const {
        std::vector<std::string> names;
        names.reserve(m_analyses.size());
        for (const auto& ana : m_analyses) names.push_back(ana->name());
        return names;
    }
    PlotAnalysis_Ptr FileStructure::get_analysis(const std::string& ana_name) const {
        for (const auto& ana : m_analyses) {
            if (ana->name() == ana_name) return ana;
        }
        return PlotAnalysis_Ptr();
    }
    FileStructure::FileStructure(std::shared_ptr<TFile> ROOT_File) : m_reference_file(ROOT_File), m_analyses() {
        std::vector<std::string> folder_names;
        // load first what's available in the file
        for (const auto& key : *ROOT_File->GetListOfKeys()) {
            TDirectory* dir = nullptr;
            ROOT_File->GetObject(key->GetName(), dir);
            if (!dir) continue;
            std::string dir_name = EraseWhiteSpaces(key->GetName());
            folder_names.push_back(dir_name);
            delete dir;
        }
        // XAMPP files are built like this
        // --- MyAnalysis_Nominal
        // --- MyAnalysis_1Up
        // --- MyAnalysis_1Dn
        // --- AnotherThing_Nominal
        // --- AnotherThing_Blub
        std::sort(folder_names.begin(), folder_names.end());
        std::string last_analysis;
        for (auto& candidate : folder_names) {
            // Begin of a new analysis chain
            if (last_analysis.empty()) {
                last_analysis = candidate;
                continue;
            }
            std::string longest_substr = LongestCommonString(last_analysis, candidate);
            // Another analysis chain is added
            if (longest_substr.empty()) {
                append_analysis(last_analysis);
                last_analysis = candidate;
            } else {
                last_analysis = longest_substr;
            }
        }
        append_analysis(last_analysis);
    }
    void FileStructure::append_analysis(std::string& name) {
        /// Skim away the nominal in the analysis names
        if (name.rfind("_Nominal") == name.rfind("_")) name = name.substr(0, name.rfind("_"));
        while (name.rfind("_") == name.size() - 1) name = name.substr(0, name.rfind("_"));
        // Check if the current analysis is already known
        for (const auto& known : get_analyses_names()) {
            if (name.find(known) == 0) {
                name.clear();
                return;
            }
        }
        // Empty string has been parsed.
        if (name.empty()) return;
        PlotAnalysis_Ptr ana = std::make_shared<PlotAnalysis>(this, name);
        m_analyses.push_back(ana);
        name.clear();
    }
    std::vector<std::string> FileStructure::get_regions(const std::string& ana_name) const {
        PlotAnalysis_Ptr ana = get_analysis(ana_name);
        if (ana) return ana->get_region_names();
        Warning("FileStructure()", Form("Unkown analysis %s", ana_name.c_str()));
        return std::vector<std::string>();
    }
    std::vector<std::string> FileStructure::get_variables(const std::string& ana_name, const std::string& region) const {
        PlotAnalysis_Ptr ana = get_analysis(ana_name);
        if (ana) return ana->get_variables(region);
        Warning("FileStructure()", Form("Unkown analysis %s", ana_name.c_str()));
        return std::vector<std::string>();
    }
    //##################################################################
    //                      PlotRegion
    //##################################################################
    PlotRegion::PlotRegion(TDirectory* File_Dir) : m_region_name(File_Dir->GetName()), m_variables() {
        for (const auto& Key : *File_Dir->GetListOfKeys()) {
            std::string key_name = Key->GetName();
            if (FileHandler::getInstance()->variable_known(key_name)) {
                m_variables.push_back(key_name);
                continue;
            }
            TH1* is_histo = nullptr;
            File_Dir->GetObject(Key->GetName(), is_histo);
            if (is_histo != nullptr) {
                m_variables.push_back(key_name);
                FileHandler::getInstance()->new_variable(key_name);
                delete is_histo;
            }
        }
    }
    const std::vector<std::string>& PlotRegion::variables() const { return m_variables; }
    std::string PlotRegion::name() const { return m_region_name; }

}  // namespace XAMPP
