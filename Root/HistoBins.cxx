#include <TError.h>
#include <TFile.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>
#include <TMath.h>
#include <iostream>
#include <sstream>

#include <XAMPPplotting/Histo.h>
#include <XAMPPplotting/HistoBins.h>
#include <XAMPPplotting/HistoTemplates.h>
#include <XAMPPplotting/TreeVarReader.h>
#include <XAMPPplotting/UtilityReader.h>
#include <XAMPPplotting/Weight.h>

namespace XAMPP {
    bool HistoBin::m_FillSumW2 = true;
    //##################################################################
    //          HistoBin
    //##################################################################
    HistoBin::HistoBin(unsigned int D) :
        m_weighter(Weight::getWeighter()),
        m_content(0),
        m_error(0),
        m_dimension(D),
        m_low_edges(new double[D]),
        m_high_edges(new double[D]),
        m_is_Overflow(new bool[D]),
        m_is_Undeflow(new bool[D]),
        m_low_edges_ptr(m_low_edges.get()),
        m_high_edges_ptr(m_high_edges.get()),
        m_underflow_ptr(m_is_Undeflow.get()),
        m_overflow_ptr(m_is_Overflow.get()) {
        for (unsigned int i = 0; i < D; ++D) {
            m_low_edges_ptr[i] = m_high_edges_ptr[i] = 0.;
            m_overflow_ptr[i] = m_underflow_ptr[i] = false;
        }
    }
    HistoBin::HistoBin(const std::vector<double>& low_edges, const std::vector<double>& high_edges) :
        HistoBin(low_edges.size() > high_edges.size() ? low_edges.size() : (high_edges.empty() ? 1 : high_edges.size())) {
        for (unsigned int i = 0; i < low_edges.size(); ++i) {
            m_low_edges_ptr[i] = low_edges[i];
            m_high_edges_ptr[i] = high_edges[i];
        }
    }
    HistoBin::HistoBin(double x_low, double x_high) : HistoBin(std::vector<double>{x_low}, std::vector<double>{x_high}) {}
    HistoBin::HistoBin(double x_low, double x_high, double y_low, double y_high) :
        HistoBin(std::vector<double>{x_low, y_low}, std::vector<double>{x_high, y_high}) {}
    HistoBin::HistoBin(double x_low, double x_high, double y_low, double y_high, double z_low, double z_high) :
        HistoBin(std::vector<double>{x_low, y_low, z_low}, std::vector<double>{x_high, y_high, z_high}) {}

    // Manipulation of the content
    void HistoBin::AddContent(const double& content) { m_content += content; }
    void HistoBin::AddError(const double& error) { m_error += error; }
    void HistoBin::SetContent(const double& content) { m_content = content; }
    void HistoBin::SetError(const double& error) { m_error = error; }
    void HistoBin::Fill() {
        m_content += m_weighter->GetWeight();
        if (m_FillSumW2) m_error += m_weighter->GetWeightSquared();
    }
    void HistoBin::Fill(const double& content) {
        AddContent(content);
        if (m_FillSumW2) AddError(content * content);
    }

    // Get the content of the bin
    double HistoBin::GetContent() const { return m_content; }
    double HistoBin::GetError() const { return sqrt(m_error); }
    double HistoBin::GetError2() const { return m_error; }

    void HistoBin::AddSumW2(bool B) { m_FillSumW2 = B; }
    unsigned int HistoBin::GetdDimension() const { return m_dimension; }
    double HistoBin::GetVolume() const {
        double V = 1.;
        for (unsigned int i = 0; i < m_dimension; ++i) { V *= fabs(GetHighEdge(i) - GetLowEdge(i)); }
        return V;
    }
    bool HistoBin::isOverflow(unsigned int D) const {
        if (D >= m_dimension) {
            Warning("HistoBin()", Form("The bin is only defined in %u dimensions. The requested one %u is out of scope", m_dimension, D));
            return true;
        }
        return m_overflow_ptr[D];
    }
    bool HistoBin::isUnderFlow(unsigned int D) const {
        if (D >= m_dimension) {
            Warning("HistoBin()", Form("The bin is only defined in %u dimensions. The requested one %u is out of scope", m_dimension, D));
            return true;
        }
        return m_underflow_ptr[D];
    }
    double HistoBin::GetLowEdge(unsigned int D) const {
        if (D >= m_dimension) {
            Warning("HistoBin()", Form("The bin is only defined in %u dimensions. The requested one %u is out of scope", m_dimension, D));
            return std::nan("float");
        }
        return m_low_edges_ptr[D];
    }
    double HistoBin::GetHighEdge(unsigned int D) const {
        if (D >= m_dimension) {
            Warning("HistoBin()", Form("The bin is only defined in %u dimensions. The requested one %u is out of scope", m_dimension, D));
            return std::nan("float");
        }
        return m_high_edges_ptr[D];
    }
    double HistoBin::GetCenter(unsigned int D) const { return (GetHighEdge(D) + GetLowEdge(D)) / 2; }

    bool HistoBin::isInsideBin(const double& X) const {
        if (m_dimension != 1) return false;
        return PassAxisBorders(0, X);
    }
    bool HistoBin::isInsideBin(const double& X, const double& Y) const {
        if (m_dimension != 2) return false;
        return PassAxisBorders(0, X) && PassAxisBorders(1, Y);
    }
    bool HistoBin::isInsideBin(const double& X, const double& Y, const double& Z) const {
        if (m_dimension != 3) return false;
        return PassAxisBorders(0, X) && PassAxisBorders(1, Y) && PassAxisBorders(2, Z);
    }
    bool HistoBin::PassAxisBorders(unsigned int D, const double& X) const {
        if (isOverflow(D))
            return X >= GetLowEdge(D);
        else if (isUnderFlow(D))
            return X < GetHighEdge(D);
        return X >= GetLowEdge(D) && X < GetHighEdge(D);
    }
    //##################################################################
    //                          HistoAxis
    //##################################################################
    HistoAxis::HistoAxis(unsigned int Nbins, double low_edge, double high_edge) :
        m_low_edge(low_edge),
        m_high_edge(high_edge),
        m_nbins(Nbins),
        m_equal_distance(true),
        m_bin_width(-1),
        m_bin_edges(),
        m_itr_begin(m_bin_edges.begin()),
        m_itr_end(m_bin_edges.end()) {
        if (m_low_edge > m_high_edge) {
            Warning("HistoAxis()", Form("%f low edge is larger than the upper one %f", low_edge, high_edge));
            m_high_edge = low_edge;
            m_low_edge = high_edge;
        }
        m_bin_width = (m_high_edge - m_low_edge) / Nbins;
    }
    HistoAxis::HistoAxis(const std::vector<double>& edges) :
        m_low_edge(0),
        m_high_edge(-1),
        m_nbins(0),
        m_equal_distance(false),
        m_bin_width(-1),
        m_bin_edges(),
        m_itr_begin(m_bin_edges.begin()),
        m_itr_end(m_bin_edges.end()) {
        CopyVector(edges, m_bin_edges);
        std::sort(m_bin_edges.begin(), m_bin_edges.end());
        m_nbins = m_bin_edges.size();

        m_low_edge = m_bin_edges.at(0);
        m_high_edge = m_bin_edges.at(m_nbins - 1);
        // we can forget about the first and last element
        m_bin_edges.erase(m_bin_edges.begin());
        m_bin_edges.pop_back();
        m_itr_begin = m_bin_edges.begin();
        m_itr_end = m_bin_edges.end();
    }
    unsigned int HistoAxis::FindBin(double value) const {
        if (value < m_low_edge) return 0;
        if (value > m_high_edge) return m_nbins + 1;
        if (!m_equal_distance) return FindBin(value, m_itr_begin, m_itr_end);

        value -= m_low_edge;
        return ceil(std::fabs(value / m_bin_width));
    }

    unsigned int HistoAxis::FindBin(const double& value, std::vector<double>::const_iterator begin_itr,
                                    std::vector<double>::const_iterator end_itr) const {
        if (begin_itr == end_itr) return 1 + (begin_itr - m_itr_begin);
        // half way iterator
        unsigned int range = (end_itr - begin_itr);
        std::vector<double>::const_iterator half_way = (begin_itr + range / 2);
        if ((*half_way) > value) return FindBin(value, begin_itr, half_way);
        return FindBin(value, half_way, end_itr);
    }
    unsigned int HistoAxis::GetNbins() const { return m_nbins + 1; }
    double HistoAxis::GetBinCenter(unsigned int bin) const {
        if (bin == 0) {
            return FLT_MIN / 2;
        } else if (bin == m_nbins + 1) {
            return FLT_MAX / 2;
        }
        return 1;
    }
    double HistoAxis::GetBinLowEdge(unsigned int bin) const {
        if (bin == 0) return FLT_MIN;

        return 1;
    }
    double HistoAxis::GetBinUpEdge(unsigned int bin) const {
        if (bin == m_nbins + 1) return FLT_MAX;
        return 1;
    }
    std::vector<double> HistoAxis::GetBinning() const {
        std::vector<double> binning;
        binning.reserve(GetNbins());
        binning.push_back(m_low_edge);
        if (!m_equal_distance) {
            std::copy(m_itr_begin, m_itr_end, binning.end());
        } else {
            for (unsigned int k = 0; k < m_nbins; ++k) { binning.push_back(m_low_edge + k * m_bin_width); }
        }
        binning.push_back(m_high_edge);
        return binning;
    }
    //##################################################################
    //                          Histogram
    //##################################################################
    Histogram::Histogram(unsigned int nbins, double low_edge, double up_edge) : Histogram() {
        m_x_axis = std::make_shared<HistoAxis>(nbins, low_edge, up_edge);
        m_dim = 1;
    }
    Histogram::Histogram(unsigned int nbins_x, double x_low, double x_high, unsigned int nbins_y, double y_low, double y_high) :
        Histogram() {
        m_x_axis = std::make_shared<HistoAxis>(nbins_x, x_low, x_high);
        m_y_axis = std::make_shared<HistoAxis>(nbins_y, y_low, y_high);
        m_dim = 2;
    }
    Histogram::Histogram(unsigned int nbins_x, double x_low, double x_high, unsigned int nbins_y, double y_low, double y_high,
                         unsigned int nbins_z, double z_low, double z_high) :
        Histogram() {
        m_x_axis = std::make_shared<HistoAxis>(nbins_x, x_low, x_high);
        m_y_axis = std::make_shared<HistoAxis>(nbins_y, y_low, y_high);
        m_z_axis = std::make_shared<HistoAxis>(nbins_z, z_low, z_high);

        m_dim = 3;
    }
    Histogram::Histogram(const Histogram& other) : Histogram() {
        m_x_axis = other.m_x_axis;
        m_y_axis = other.m_y_axis;
        m_z_axis = other.m_z_axis;
        m_dim = other.m_dim;
    }
    Histogram::Histogram() : m_dim(0), m_x_axis(nullptr), m_y_axis(nullptr), m_z_axis(nullptr), m_bins(), m_entries(0) {}

}  // namespace XAMPP
