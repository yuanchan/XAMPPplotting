#include <XAMPPplotting/ArithmetricReaders.h>
#include <XAMPPplotting/PlottingUtils.h>
#include <XAMPPplotting/TreeVarReader.h>
#include <XAMPPplotting/TreeVarWriter.h>
#include <XAMPPplotting/UtilityReader.h>

#include <XAMPPplotting/Cuts.h>
#include <XAMPPplotting/Weight.h>
#include <cmath>
#include <functional>
#include <sstream>

namespace XAMPP {

    //#########################################################################################
    //                                  ITreeVarReaderStorage
    //#########################################################################################
    ITreeVarReaderStorage* ITreeVarReaderStorage::m_Inst = nullptr;
    ITreeVarReaderStorage::ITreeVarReaderStorage() : m_DB() {}
    ITreeVarReader* ITreeVarReaderStorage::GetReader(const std::string& Name) const {
        size_t Hash = std::hash<std::string>()(Name);
        std::map<size_t, std::shared_ptr<ITreeVarReader>>::const_iterator Itr = m_DB.find(Hash);
        if (Itr != m_DB.end()) return Itr->second.get();
        return nullptr;
    }
    bool ITreeVarReaderStorage::Register(ITreeVarReader* R) {
        size_t Hash = std::hash<std::string>()(R ? R->name() : "");

        if (!R || R->name().empty() || m_DB.find(Hash) != m_DB.end()) {
            Error("ITreeVarReaderStorage::Register()", "Could not register reader " + (R ? R->name() : "no reader given."));
            return false;
        }
        m_DB.insert(std::pair<size_t, std::shared_ptr<ITreeVarReader>>(Hash, std::shared_ptr<ITreeVarReader>(R)));
        Info("ITreeVarReaderStorage::Register()", "Register new reader " + R->name());
        return true;
    }
    ITreeVarReaderStorage::~ITreeVarReaderStorage() {
        delete ParReaderStorage::GetInstance();
        delete ITreeVarWriterStorage::GetInstance();
        m_DB.clear();
        m_Inst = nullptr;
    }
    ITreeVarReaderStorage* ITreeVarReaderStorage::GetInstance() {
        if (!m_Inst) m_Inst = new ITreeVarReaderStorage();
        return m_Inst;
    }
    //#########################################################################################
    //                                  ParReaderStorage
    //#########################################################################################
    ParReaderStorage* ParReaderStorage::m_Inst = nullptr;
    ParReaderStorage* ParReaderStorage::GetInstance() {
        if (!m_Inst) m_Inst = new ParReaderStorage();
        return m_Inst;
    }
    ParReaderStorage::ParReaderStorage() : m_DB() {}
    IParticleReader* ParReaderStorage::GetReader(const std::string& Name) const {
        size_t Hash = std::hash<std::string>()(Name);
        std::map<size_t, std::shared_ptr<IParticleReader>>::const_iterator Itr = m_DB.find(Hash);
        if (Itr != m_DB.end()) return Itr->second.get();
        return nullptr;
    }
    bool ParReaderStorage::Register(IParticleReader* R) {
        size_t Hash = std::hash<std::string>()(R ? R->name() : "");
        if (!R || R->name().empty() || m_DB.find(Hash) != m_DB.end()) {
            if (!R)
                Error("ParReaderStorage::Register()", "No reader was given");
            else
                Error("ParReaderStorage::Register()", "Reader " + R->name() + " is already stored. Please choose an other name");
            return false;
        }
        Info("ParReaderStorage::Register()", "Register new particle " + R->name());
        m_DB.insert(std::pair<size_t, std::shared_ptr<IParticleReader>>(Hash, std::shared_ptr<IParticleReader>(R)));
        return true;
    }
    ParReaderStorage::~ParReaderStorage() { m_Inst = nullptr; }

    //#########################################################################################
    //                                  IScalarReader
    //#########################################################################################
    IScalarReader::IScalarReader() : m_updated(false) {}
    bool IScalarReader::update() {
        if (m_updated) return false;
        m_updated = true;
        return true;
    }
    void IScalarReader::reset() { m_updated = false; }

    double IScalarReader::readEntry(const size_t I) const {
        if (I > 1) {
            Warning("IScalarReader::readEntry()", Form("%lu is out of range. Return 1.e25", I));
            return 1.e25;
        }
        return read();
    }
    size_t IScalarReader::entries() const { return 1; }
    //#########################################################################################
    //                                  IVectorReader
    //#########################################################################################
    double IVectorReader::read() const { return readEntry(m_index); }
    size_t IVectorReader::index() const { return m_index; }
    bool IVectorReader::update() { return setIndex(m_index + 1); }
    bool IVectorReader::setIndex(const size_t I) {
        if (I >= entries()) return false;
        m_index = I;
        return true;
    }
    void IVectorReader::reset() { m_index = -1; }
    IVectorReader::IVectorReader() : m_index(-1) {}

    //#########################################################################################
    //                                  IParticleVariable
    //#########################################################################################
    size_t IParticleVariable::entries() const { return m_Particle->Size(); }
    IParticleVariable::IParticleVariable(IParticleReader* particle) : m_Particle(particle), m_updated(false), m_Idx(-1) {}
    size_t IParticleVariable::index() const {
        if (!m_updated) return m_Particle->Index();
        return m_Idx;
    }
    double IParticleVariable::read() const { return readEntry(index()); }
    IParticleReader* IParticleVariable::getParticle() const { return m_Particle; }
    bool IParticleVariable::update() { return At(m_Idx + 1); }
    void IParticleVariable::reset() {
        m_Idx = -1;
        m_updated = false;
        m_Particle->reset();
    }
    bool IParticleVariable::At(size_t I) {
        if (I >= m_Particle->Size()) return false;
        m_Idx = I;
        m_updated = true;
        return m_Particle->At(I);
    }

    //#########################################################################################
    //                                  IMatrixReader
    //#########################################################################################
    size_t IMatrixReader::entries() const {
        if (m_service->currentEvent() != m_ev_number) {
            m_ev_number = m_service->currentEvent();
            m_row_sizes.clear();
            m_tot_entries = 0;
            if (m_row_sizes.capacity() < num_rows()) m_row_sizes.reserve(num_rows() + m_row_sizes.capacity());
            for (size_t r = 0; r < num_rows(); ++r) {
                size_t n = row_entries(r);
                m_tot_entries += n;
                m_row_sizes.push_back(n);
            }
        }
        return m_tot_entries;
    }

    double IMatrixReader::read() const { return readMatrixEntry(m_row_idx, m_col_idx); }
    double IMatrixReader::readEntry(const size_t I) const {
        if (I > entries()) {
            Warning("IMatrixReader::readEntry()", Form("%s has not enough capacity to read %lu. Return 1.e25", name().c_str(), I));
            return 1.e25;
        }
        std::pair<size_t, size_t> idx = get_indices(I);
        return readMatrixEntry(idx.first, idx.second);
    }
    bool IMatrixReader::update() { return setIndex(index() + 1); }
    bool IMatrixReader::setIndex(size_t I) {
        std::pair<size_t, size_t> idx = get_indices(I);
        return setMatrixIndex(idx.first, idx.second);
    }
    std::pair<size_t, size_t> IMatrixReader::get_indices(const size_t I) const {
        if (I < entries()) {
            size_t row(0), tot(0);
            for (const auto& row_size : m_row_sizes) {
                tot += row_size;
                if (tot >= I) return std::pair<size_t, size_t>(row, tot - I);
                ++row;
            }
        }
        return std::pair<size_t, size_t>(-1, -1);
    }
    bool IMatrixReader::setMatrixIndex(const size_t i, const size_t j) {
        // Num rows is explicitly checked inside the row_entries method
        if (j >= row_entries(i)) return false;
        m_row_idx = i;
        m_col_idx = j;
        return true;
    }
    void IMatrixReader::reset() { m_col_idx = m_row_idx = -1; }

    size_t IMatrixReader::index() const {
        if (m_row_idx == (size_t)-1) return -1;
        size_t i = m_col_idx;
        std::vector<size_t>::const_iterator begin_itr = m_row_sizes.begin();
        std::vector<size_t>::const_iterator end_itr = m_row_sizes.end();
        std::vector<size_t>::const_iterator rng_itr = begin_itr + m_row_idx;
        for (std::vector<size_t>::const_iterator itr = m_row_sizes.begin(); itr != rng_itr && itr != end_itr; ++itr) { i += (*itr); }
        return i;
    }
    size_t IMatrixReader::row_index() const { return m_row_idx; }
    size_t IMatrixReader::column_index() const { return m_col_idx; }
    IMatrixReader::IMatrixReader() :
        m_row_idx(-1),
        m_col_idx(-1),
        m_service(EventService::getService()),
        m_ev_number(-1),
        m_tot_entries(0),
        m_row_sizes() {}
    //#########################################################################################
    //                                  IParticleCollection
    //#########################################################################################
    size_t IParticleCollection::Index() const { return m_Idx; }
    double IParticleCollection::read() const {
        Warning("IParticleCollection::read()",
                "The ParticleReader manages very different VectorVarReaders inside and it is designed to enable the user to define cuts on "
                "one of them.");
        Warning("IParticleCollection::read()", "A priori it is not clear which variable to take inside. Return dummy value 1.e25");
        return 1.e25;
    }
    double IParticleCollection::readEntry(const size_t) const { return read(); }
    bool IParticleCollection::update() { return At(m_Idx + 1); }
    void IParticleCollection::reset() { m_Idx = -1; }
    bool IParticleCollection::At(size_t I) {
        if (I >= Size()) return false;
        m_Idx = I;
        return true;
    }
    size_t IParticleCollection::entries() const { return 0; }
    IParticleCollection::IParticleCollection() : m_Idx(-1) {}
    //#########################################################################################
    //                                  IParticleVector
    //#########################################################################################
    IParticleReader* IParticleVector::getParticle() const { return m_Particle; }
    IParticleVector::IParticleVector(IParticleReader* particle) :
        m_Particle(particle),
        m_par_idx(-1),
        m_var_idx(-1),
        m_service(EventService::getService()),
        m_ev_number(-1),
        m_tot_entries(0),
        m_row_sizes() {}
    bool IParticleVector::At(size_t I) {
        if (I >= m_Particle->Size()) return false;
        m_par_idx = I;
        return true;
    }
    bool IParticleVector::update() {
        if (!setMatrixIndex(m_par_idx, m_var_idx + 1)) { return setMatrixIndex(m_par_idx + 1, 0); }
        return true;
    }
    void IParticleVector::reset() { m_par_idx = m_var_idx = -1; }
    double IParticleVector::read() const { return readMatrixEntry(m_par_idx, m_var_idx); }
    double IParticleVector::readEntry(const size_t I) const {
        if (I > entries()) {
            Warning("IMatrixReader::readEntry()", Form("%s has not enough capacity to read %lu. Return 1.e25", name().c_str(), I));
            return 1.e25;
        }
        std::pair<size_t, size_t> idx = get_indices(I);
        return readMatrixEntry(idx.first, idx.second);
    }
    bool IParticleVector::setMatrixIndex(const size_t i, const size_t j) {
        // Num rows is explicitly checked inside the row_entries method
        if (j >= row_entries(i)) return false;
        m_par_idx = i;
        m_var_idx = j;
        return true;
    }
    size_t IParticleVector::entries() const {
        if (m_service->currentEvent() != m_ev_number) {
            m_ev_number = m_service->currentEvent();
            m_row_sizes.clear();
            m_tot_entries = 0;
            if (m_row_sizes.capacity() < m_Particle->Size()) m_row_sizes.reserve(m_Particle->Size() + m_row_sizes.capacity());
            for (size_t r = 0; r < m_Particle->Size(); ++r) {
                size_t n = row_entries(r);
                m_tot_entries += n;
                m_row_sizes.push_back(n);
            }
        }
        return m_tot_entries;
    }
    std::pair<size_t, size_t> IParticleVector::get_indices(const size_t I) const {
        if (I < entries()) {
            size_t row(0), tot(0);
            for (const auto& row_size : m_row_sizes) {
                tot += row_size;
                if (tot >= I) return std::pair<size_t, size_t>(row, tot - I);
                ++row;
            }
        }
        return std::pair<size_t, size_t>(-1, -1);
    }
    //#########################################################################################
    //                                  ParticleReader
    //#########################################################################################
    ParticleReader::ParticleReader(const std::string& Name, const std::string& Coll) :
        m_Name(Name),
        m_ParCollection(),
        m_ParCollectionHash(-1),
        m_Setup(false),
        m_Registered(false),
        m_UseGeVReader(true),
        m_Vars(),
        m_VarsVec(),
        m_Cuts(),
        m_CurrentEv(-1),
        m_TargetMap(),
        m_TargetEnd(m_TargetMap.end()),
        m_Size(0),
        m_FistLookUp(-1),
        m_service(EventService::getService()),
        m_TimeComReader(nullptr),
        m_PtReader(nullptr),
        m_EtaReader(nullptr),
        m_PhiReader(nullptr),
        m_P4() {
        SetParticleCollection(Coll);
        m_Registered = ParReaderStorage::GetInstance()->Register(this);
    }
    void ParticleReader::DisableMeVtoGeV() {
        XAMPP::PrintFooter();
        Info("ParticleReader::DisableMeVtoGeV()", name() + " disables the conversion of MeVtoGeV. Please be aware that");
        Info("ParticleReader::DisableMeVtoGeV()", "for the sake of consistency other Particles accessing the same branches ");
        Info("ParticleReader::DisableMeVtoGeV()", "should have switched this option too.");
        XAMPP::PrintFooter();
        m_UseGeVReader = false;
    }

    void ParticleReader::SetParticleCollection(const std::string& Coll) {
        if (!m_Setup && Coll != name()) {
            m_ParCollection = Coll;
            m_ParCollectionHash = std::hash<std::string>()(Coll);
        } else if (Coll == name())
            Error("ParticleReader::SetParticleCollection()", "Collection and name are the same " + name());
    }
    std::string ParticleReader::name() const { return m_Name; }
    ParticleReader* ParticleReader::GetReader(const std::string& Name, const std::string& Coll) {
        ParReaderStorage* S = ParReaderStorage::GetInstance();
        if (S->GetReader(Name) == nullptr) { return new ParticleReader(Name, Coll); }
        return dynamic_cast<ParticleReader*>(S->GetReader(Name));
    }
    bool ParticleReader::Setup(std::ifstream& InStream) {
        XAMPP::PrintHeadLine("ParticlerReader::Setup(): Setup particle " + m_Name);
        if (!m_Registered) return false;
        if (m_ParCollection.empty()) {
            Error("ParticleReader::Setup()", "Missing the input Collection for Reader " + name());
            return false;
        } else
            Info("ParticleReader::Setup()", "Use input Collection " + m_ParCollection);
        if (m_Setup) {
            Error("ParticleReader::Setup()", " The particle reader " + name() + " is already setup");
            return false;
        }
        std::string line;
        IfDefLineParser ReadLine;
        while (ReadLine(InStream, line) == IfDefLineParser::NewProperty) {
            std::stringstream sstr(line);
            std::string word = GetWordFromStream(sstr);
            if (word == "End_Particle") {
                SetupDone();
                return true;
            } else if (word == "Var" && !AddVariable(sstr))
                return false;
            else if (word.find("Cut") + 3 == word.size()) {
                if (word == "CombCut" && !AddCombinedCut(InStream, GetCombineModeFromLine(line)))
                    return false;
                else if (word == "Cut" && !AddCut(sstr))
                    return false;
            }
        }
        Error("ParticleReader::Setup()", "Missing the End_Particle statement in the config file for particle " + name());
        return false;
    }
    bool ParticleReader::AddCombinedCut(std::ifstream& InStream, Condition::Mode Mode) {
        Info("ParticleReader::AddCombinedCut()", "Create a new combined cut to specify " + name());
        std::shared_ptr<Condition> C;
        Condition::Mode OrigMode = Mode;
        if (Mode == Condition::Mode::NOTAND) {
            Mode = Condition::Mode::AND;
        } else if (Mode == Condition::Mode::NOTOR) {
            Mode = Condition::Mode::OR;
        }
        std::string line;
        IfDefLineParser Reader;
        while (Reader(InStream, line) == IfDefLineParser::NewProperty) {
            if (line.find("End_CombCut") < line.size()) {
                if (!C) {
                    Error("ParticleReader::AddCombinedCut()", " No conditions have been defined.");
                    return false;
                } else if (OrigMode == Condition::Mode::NOTAND || OrigMode == Condition::Mode::NOTOR)
                    C = std::make_shared<ConditionNOT>(C);

                Info("ParticleReader::AddCombinedCut()", "Created succesfully a combined cut " + C->name());
                m_Cuts.push_back(C);
                return true;
            } else if (line.find("Cut") < line.size()) {
                if (line.find("CombCut") < line.size()) {
                    if (!AddCombinedCut(InStream, GetCombineModeFromLine(line))) return false;

                } else {
                    std::stringstream sstr(line);
                    sstr >> line;
                    if (!AddCut(sstr)) return false;
                }
                std::shared_ptr<Condition> C1 = m_Cuts.back();
                if (!C)
                    C = C1;
                else
                    C = Condition::Combine(C, C1, Mode);
                m_Cuts.erase(m_Cuts.end() - 1);
            }
        }
        Error("ParticleReader::AddCombinedCut()", "Missing 'End_CombCut' statement.");
        return false;
    }
    void ParticleReader::SetupDone() {
        Info("ParticleReader::SetupDone()", "The following requirments have been imposed to define " + name());
        for (const auto& C : m_Cuts) Info("ParticleReader::SetupDone()", WhiteSpaces(15) + "*** " + C->name());
        XAMPP::PrintFooter();
        if (m_Setup) {
            Warning("ParticleReader::SetupDone()", "Function already called");
            return;
        }
        AddP4Readers();
        m_VarsVec.reserve(m_Vars.size());
        for (auto& V : m_Vars) m_VarsVec.push_back(V.second);
        m_Setup = true;
    }
    void ParticleReader::AddP4Readers() {
        if (!m_PtReader) m_PtReader = NewVariableGeV<float>("pt");
        if (!m_EtaReader) m_EtaReader = NewVariable<float>("eta");
        if (!m_PhiReader) m_PhiReader = NewVariable<float>("phi");

        if (!m_TimeComReader) m_TimeComReader = NewVariableGeV<float>(time_component());
    }
    std::string ParticleReader::time_component() const { return "e"; }
    std::shared_ptr<Condition> ParticleReader::CombineCuts(std::shared_ptr<Condition> cut1, std::shared_ptr<Condition> cut2,
                                                           Condition::Mode mode) {
        if (m_Setup) {
            Error("ParticleReader::CombineCuts()", "The particle " + name() + " is already defined and frozen");
            return nullptr;
        } else if (cut1 == nullptr || cut2 == nullptr) {
            Error("ParticleReader::CombineCuts()", "Stop giving nullptr. you fool");
            return nullptr;
        }
        if (!IsElementInList(m_Cuts, cut1) || !IsElementInList(m_Cuts, cut2) || cut1 == cut2) {
            Error("ParticleReader::CombineCuts()", "One of the cuts is not created by me. I only accept those");
            return nullptr;
        }
        RemoveElement(m_Cuts, cut1);
        RemoveElement(m_Cuts, cut2);
        std::shared_ptr<Condition> combined = Condition::Combine(cut1, cut2, mode);
        m_Cuts.push_back(combined);
        return combined;
    }

    bool ParticleReader::AddVariable(std::string V) {
        AddP4Readers();
        std::stringstream sstr(V);
        if (!AddVariable(sstr)) return false;
        return true;
    }
    ITreeVectorReader* ParticleReader::GetVariable(const std::string& Name) const {
        if (IsVariableDefined(Name)) return m_Vars.at(Name);
        return nullptr;
    }
    ITreeMatrixReader* ParticleReader::GetMatrixVariable(const std::string& Name) const {
        if (IsVariableDefined(Name)) return dynamic_cast<ITreeMatrixReader*>(m_Vars.at(Name));
        return nullptr;
    }
    bool ParticleReader::AddArithmetricVariable(const std::string& alias, const std::string& first, const std::string& second,
                                                const std::string& math_operator) {
        if (m_Setup) {
            Error("ParticleReader::AddArithmetricVariable()", name() + " is already setup");
            return false;
        }
        AddP4Readers();
        ITreeVectorReader* V1 = GetVariable(first);
        ITreeVectorReader* V2 = GetVariable(second);
        if (V1 == nullptr || V2 == nullptr) {
            Error("ParticleReader::AddArithmetricVariable()",
                  "Either " + first + " or " + second + " is not defined yet. Please make sure that both variables are known beforehand.");
            return false;
        }
        if (alias.empty()) {
            Error("ParticleReader::AddArithmetricVariable()", "Empty aliases are not allowed in case of a particle");
            return false;
        }
        if (IsVariableDefined(alias)) return true;
        ITreeVectorReader* Arith = MathITreeVarReader::GetReader(V1, V2, math_operator);
        if (Arith == nullptr) {
            Error("ParticleReader::AddArithmetricVariable()",
                  name() + " could not create a valid mathemtical ITreeVarReader from operator " + math_operator);
            return false;
        }
        m_Vars.insert(std::pair<std::string, ITreeVectorReader*>(alias, Arith));
        return true;
    }
    ITreeVectorReader* ParticleReader::NewArithmetricVariable(const std::string& alias, const std::string& first, const std::string& second,
                                                              const std::string& math_operator) {
        if (!AddArithmetricVariable(alias, first, second, math_operator)) return nullptr;
        return GetVariable(alias);
    }
    bool ParticleReader::AddCut(std::stringstream& sstr) {
        if (m_Setup) {
            Error("ParticleReader::AddCut()", "The " + name() + " is already setup.");
            return false;
        }
        std::stringstream::pos_type Pos = sstr.tellg();

        ITreeVectorReader* Var = AddVariable(sstr);
        if (Var == nullptr) return false;
        ITreeMatrixReader* IsVector = dynamic_cast<ITreeMatrixReader*>(Var);
        if (IsVector == nullptr) {
            std::shared_ptr<Condition> NewCut = XAMPP::CreateCut(Var, sstr);
            if (!NewCut) return false;
            m_Cuts.push_back(NewCut);
        } else {
            sstr.clear();
            sstr.seekg(Pos, sstr.beg);
            // Type is not of interest
            std::string OrigVarName(GetWordFromStream(sstr));
            OrigVarName = GetWordFromStream(sstr);
            // Now we have the VariableName
            // Lets obtain the ranges
            Cube Rng = GetMatrixRangesToRead(OrigVarName);
            if (Rng.first.first == -1) {
                Error("ParticleReader::AddCut()", "You want to apply a cut on a vector variable and did not specify the index");
                return false;
            }
            ITreeVectorReader* RowReader = MatrixNthColumnReader::GetReader(IsVector, Rng.first.first);
            std::shared_ptr<Condition> NewCut = XAMPP::CreateCut(RowReader, sstr);
            if (!NewCut) return false;
            m_VarsVec.push_back(RowReader);
            m_Cuts.push_back(NewCut);
        }
        return true;
    }
    unsigned int ParticleReader::GetCollectionHash() const { return m_ParCollectionHash; }
    std::string ParticleReader::GetCollectionName() const { return m_ParCollection; }
    ITreeVectorReader* ParticleReader::AddVariable(std::stringstream& sstr) {
        if (m_Setup) {
            Error("ParticleReader::AddVariable()", "The " + name() + " is already setup.");
            return nullptr;
        }
        std::string Type, Name;
        sstr >> Type >> Name;
        // These are the standard scalarTypes
        if (Type == "char") {
            return NewVariable<char>(Name);
        } else if (Type == "double") {
            return NewVariable<double>(Name);
        } else if (Type == "doubleGeV") {
            return NewVariableGeV<double>(Name);
        } else if (Type == "float") {
            return NewVariable<float>(Name);
        } else if (Type == "floatGeV") {
            return NewVariableGeV<float>(Name);
        } else if (Type == "int") {
            return NewVariable<int>(Name);
        } else if (Type == "Uint") {
            return NewVariable<unsigned int>(Name);
        } else if (Type == "bool") {
            return NewVariable<bool>(Name);
        }
        // Now lets come to the matrix types
        else if (Type == "charVec") {
            return NewMatrixVariable<char>(Name);
        } else if (Type == "doubleVec") {
            return NewMatrixVariable<double>(Name);
        } else if (Type == "boolVec") {
            return NewMatrixVariable<bool>(Name);
        } else if (Type == "intVec") {
            return NewMatrixVariable<int>(Name);
        } else if (Type == "UintVec") {
            return NewMatrixVariable<unsigned int>(Name);
        } else if (Type == "floatVec") {
            return NewMatrixVariable<float>(Name);
        } else if (Type == "doubleGeVVec") {
            return NewMatrixVariableGeV<double>(Name);
        } else if (Type == "floatGeVVec") {
            return NewMatrixVariableGeV<float>(Name);
        } else if (Type == "VecSize") {
            ITreeMatrixReader* matrix = GetMatrixVariable(Name);
            if (matrix == nullptr) return nullptr;
            if (!IsVariableDefined("N_" + Name))
                m_Vars.insert(std::pair<std::string, ITreeVectorReader*>("N_" + Name, VectorRowSizeReader::GetReader(matrix)));
            return GetVariable("N_" + Name);
        }
        // Arithmetric readers are for the basic variables possible
        else if (Type == "calc") {
            std::string first(""), second(""), op("");
            sstr >> first >> op >> second;
            return NewArithmetricVariable(Name, first, second, op);
        } else if (Type == "mathop") {
            std::string first(""), op("");
            sstr >> op >> first;
            return NewArithmetricVariable(Name, first, first, op);
        }
        Error("ParticleReader::AddVariable()", "Could not determine the type " + Type + " to create variable " + Name);
        return nullptr;
    }
    bool ParticleReader::InsertVariable(const std::string& Name, ITreeVectorReader* Variable) {
        if (IsVariableDefined(Name)) {
            Error("InsertVariable()", "The variable " + Name + " has already been defined");
            return false;
        }
        m_Vars.insert(std::pair<std::string, ITreeVectorReader*>(Name, Variable));
        return true;
    }
    bool ParticleReader::IsVariableDefined(const std::string& Name) const { return m_Vars.find(Name) != m_Vars.end(); }
    bool ParticleReader::PassCuts(size_t I) const {
        for (const auto& C : m_Cuts) {
            if (!C->PassVector(I)) return false;
        }
        return true;
    }
    bool ParticleReader::init(TTree* t) {
        if (!m_Setup) {
            Error("ParticleReader::init()", "The " + name() + " is not setup.");
            return false;
        }
        for (const auto& Reader : m_VarsVec) {
            if (!Reader->init(t)) return false;
        }
        m_CurrentEv = -1;
        reset();
        return true;
    }
    size_t ParticleReader::GetTargetEntry(size_t Idx) const {
        if (Idx < m_FistLookUp) return Idx;
        std::unordered_map<size_t, size_t>::const_iterator itr = m_TargetMap.find(Idx);
        if (itr != m_TargetEnd) return itr->second;
        return Idx;
    }
    size_t ParticleReader::Size() {
        if (!m_Setup) {
            Error("ParticleReader::Size()",
                  "The SetupDone() method has not been called for " + name() +
                      " during initialization. This method tells the reader that all definitions of the particle are done ");
            return 0;
        }
        //################################################################################
        //   The method is called on a new event. The cuts on the particle need to be    #
        //   checked once and the failing entries shall be marked                        #
        //################################################################################
        if (m_service->currentEvent() != m_CurrentEv) {
            m_CurrentEv = m_service->currentEvent();
            //###############################################################################################
            //  If there is no cut defined then we do not have anything which reduces the size              #
            //  of the particle selection.  We can go the short cut and set the number of particles         #
            //  to the size of the associated pt-branch                                                     #
            //###############################################################################################
            if (m_Cuts.empty()) {
                m_Size = m_PtReader->entries();
                return m_Size;
            }
            //##################################################################################
            //   Check which entries in the input branches shall be skipped by the reader      #
            //##################################################################################
            m_TargetMap.clear();
            size_t last_shift = 0;
            size_t nMax = m_PtReader->entries();
            m_FistLookUp = -1;

            for (size_t N = 0; N < nMax; ++N) {
                if (!PassCuts(N)) {
                    ++last_shift;
                    if (m_FistLookUp > N) m_FistLookUp = N;
                } else if (last_shift > 0)
                    m_TargetMap.insert(std::pair<size_t, size_t>(N - last_shift, N));
            }
            m_TargetEnd = m_TargetMap.end();
            m_Size = nMax - last_shift;
        }
        return m_Size;
    }
    size_t ParticleReader::ReaderIndex() const { return GetTargetEntry(Index()); }
    size_t ParticleReader::ReaderIndex(size_t I) {
        if (I >= Size()) {
            Warning("ParticleReader::ReaderIndex()", Form("%s has too few particles. Index %lu is out of range", name().c_str(), I));
            return -1;
        }
        return GetTargetEntry(I);
    }
    TLorentzVector ParticleReader::GetP4(size_t E) {
        if (E <= Size()) {
            size_t BE = GetTargetEntry(E);
            m_P4.SetPtEtaPhiE(m_PtReader->readEntry(BE), m_EtaReader->readEntry(BE), m_PhiReader->readEntry(BE),
                              m_TimeComReader->readEntry(BE));
        } else {
            Error("ParticleReader::GetFourMomentum()",
                  "The  index " + std::to_string(E) + " given  to " + name() + " is out of range. Return empty TLorentzVector.");
            m_P4.SetPxPyPzE(0, 0, 0, 0);
        }
        return m_P4;
    }
    const TLorentzVector& ParticleReader::P4() const {
        m_P4.SetPtEtaPhiE(m_PtReader->readEntry(ReaderIndex()), m_EtaReader->readEntry(ReaderIndex()),
                          m_PhiReader->readEntry(ReaderIndex()), m_TimeComReader->readEntry(ReaderIndex()));
        return m_P4;
    }
    ITreeVectorReader* ParticleReader::RetrieveBranchAccess(const std::string& Var) const {
        if (!m_Setup) {
            Error("ParticleReader::RetriveBranchAccess()", "The particle " + name() + "  is not setup");
            return nullptr;
        }
        ITreeVectorReader* V = GetVariable(Var);
        if (V != nullptr) return V;
        Error("ParticleReader::RetriveBranchAccess()", "Could not find associated variable " + Var);
        return nullptr;
    }
    IParticleVarReader* ParticleReader::RetrieveVariable(const std::string& Var) {
        if (Var == "m" && !IsVariableDefined("m"))
            return dynamic_cast<IParticleVarReader*>(ParticleTimeReader::GetReader(this, ParticleTimeReader::MomentumComponent::Mass));
        if (Var == "e" && !IsVariableDefined("e"))
            return dynamic_cast<IParticleVarReader*>(ParticleTimeReader::GetReader(this, ParticleTimeReader::MomentumComponent::Energy));
        if (m_Vars.find(Var) != m_Vars.end() && dynamic_cast<ITreeMatrixReader*>(m_Vars.at(Var))) {
            return dynamic_cast<IParticleVarReader*>(ParticleVectorVarReader::GetReader(this, Var));
        }
        return dynamic_cast<IParticleVarReader*>(ParticleVarReader::GetReader(this, Var));
    }
    ParticleReader::~ParticleReader() {}
    //#########################################################################################
    //                                  ParticleReaderM
    //#########################################################################################
    ParticleReaderM::ParticleReaderM(const std::string& Name, const std::string& Coll) : ParticleReader(Name, Coll) {}

    ParticleReaderM* ParticleReaderM::GetReader(const std::string& Name, const std::string& Coll) {
        ParReaderStorage* S = ParReaderStorage::GetInstance();
        if (!S->GetReader(Name)) new ParticleReaderM(Name, Coll);
        return dynamic_cast<ParticleReaderM*>(S->GetReader(Name));
    }
    std::string ParticleReaderM::time_component() const { return "m"; }
    const TLorentzVector& ParticleReaderM::P4() const {
        m_P4.SetPtEtaPhiM(m_PtReader->readEntry(ReaderIndex()), m_EtaReader->readEntry(ReaderIndex()),
                          m_PhiReader->readEntry(ReaderIndex()), m_TimeComReader->readEntry(ReaderIndex()));
        return m_P4;
    }
    TLorentzVector ParticleReaderM::GetP4(size_t E) {
        if (E <= Size()) {
            size_t BE = GetTargetEntry(E);
            m_P4.SetPtEtaPhiM(m_PtReader->readEntry(BE), m_EtaReader->readEntry(BE), m_PhiReader->readEntry(BE),
                              m_TimeComReader->readEntry(BE));
        } else {
            Error("ParticleReaderM::GetFourMomentum()",
                  "The  index " + std::to_string(E) + " given  to " + name() + " is out of range. Return empty TLorentzVector.");
            m_P4.SetPxPyPzE(0, 0, 0, 0);
        }
        return m_P4;
    }
    //#########################################################################################
    //                                  ParticleReaderMTAR
    //#########################################################################################
    ParticleReaderMTAR::ParticleReaderMTAR(const std::string& Name, const std::string& Coll) : ParticleReaderM(Name, Coll) {}

    ParticleReaderMTAR* ParticleReaderMTAR::GetReader(const std::string& Name, const std::string& Coll) {
        ParReaderStorage* S = ParReaderStorage::GetInstance();
        if (!S->GetReader(Name)) new ParticleReaderMTAR(Name, Coll);
        return dynamic_cast<ParticleReaderMTAR*>(S->GetReader(Name));
    }
    std::string ParticleReaderMTAR::time_component() const { return "mTAR"; }
    //#########################################################################################
    //                                  ParticleVarReader
    //#########################################################################################
    ParticleVarReader* ParticleVarReader::GetReader(ParticleReader* P, const std::string& VarName) {
        ITreeVarReaderStorage* S = ITreeVarReaderStorage::GetInstance();
        std::string name = P->name() + " " + VarName;
        if (!S->GetReader(name)) new ParticleVarReader(P, VarName);
        return dynamic_cast<ParticleVarReader*>(S->GetReader(name));
    }
    ParticleVarReader::ParticleVarReader(ParticleReader* Particle, const std::string& VarName) : ParticleVarReader(Particle) {
        m_Name = VarName;
        if (ITreeVarReaderStorage::GetInstance()->Register(this)) m_Var = Particle->RetrieveBranchAccess(VarName);
    }
    ParticleVarReader::ParticleVarReader(ParticleReader* P) : IParticleVariable(P), m_Particle(P), m_Var(nullptr), m_Name() {}

    double ParticleVarReader::readEntry(size_t I) const {
        if (I >= m_Particle->Size()) {
            Warning("ParticleVarReader::readEntry()",
                    "The index " + std::to_string(I) + " given to " + name() + " is out of range. Return 1.e25");
            return 1.e25;
        }
        return m_Var->readEntry(m_Particle->GetTargetEntry(I));
    }
    bool ParticleVarReader::init(TTree* t) {
        if (!m_Var) Error("ParticleVarReader::init()", "Could not catch the access to the tree reading " + variable_name());
        return m_Var && m_Particle->init(t) && m_Var->init(t);
    }
    std::string ParticleVarReader::name() const { return m_Particle->name() + " " + variable_name(); }
    std::string ParticleVarReader::variable_name() const { return m_Name; }

    //#########################################################################################
    //                                  ParticleVectorVarReader
    //#########################################################################################
    ParticleVectorVarReader* ParticleVectorVarReader::GetReader(ParticleReader* P, const std::string& VarName) {
        ITreeVarReaderStorage* S = ITreeVarReaderStorage::GetInstance();
        std::string name = P->name() + " " + VarName;
        if (!S->GetReader(name)) return new ParticleVectorVarReader(P, VarName);
        return dynamic_cast<ParticleVectorVarReader*>(S->GetReader(name));
    }
    ParticleVectorVarReader::ParticleVectorVarReader(ParticleReader* Particle, const std::string& VarName) :
        IParticleVector(Particle),
        m_Particle(Particle),
        m_Var(nullptr),
        m_Name(VarName) {
        if (ITreeVarReaderStorage::GetInstance()->Register(this))
            m_Var = dynamic_cast<ITreeMatrixReader*>(Particle->RetrieveBranchAccess(variable_name()));
    }
    std::string ParticleVectorVarReader::name() const { return m_Particle->name() + " " + variable_name(); }
    std::string ParticleVectorVarReader::variable_name() const { return m_Name; }
    bool ParticleVectorVarReader::init(TTree* t) {
        if (!m_Var) Error("ParticleVectorVarReader::init()", "Could not catch the access to the tree reading " + name());
        return m_Var && m_Particle->init(t) && m_Var->init(t);
    }
    double ParticleVectorVarReader::readMatrixEntry(size_t I, size_t J) const {
        if (row_entries(I) <= J) {
            Warning("ParticleVarReader::readEntry()",
                    "The index " + std::to_string(I) + "," + std::to_string(J) + " given to " + name() + " is out of range. Return 1.e25");
            return 1.e25;
        }
        return m_Var->readMatrixEntry(m_Particle->GetTargetEntry(I), J);
    }
    size_t ParticleVectorVarReader::row_entries(size_t I) const { return m_Var->row_entries(m_Particle->GetTargetEntry(I)); }
    //#########################################################################################
    //                                  ParticleTimeReader
    //#########################################################################################
    ParticleTimeReader* ParticleTimeReader::GetReader(ParticleReader* P, MomentumComponent C) {
        std::string name = P->name() + " " + (C == MomentumComponent::Energy ? "e" : "m");
        ITreeVarReaderStorage* S = ITreeVarReaderStorage::GetInstance();
        if (!S->GetReader(name)) return new ParticleTimeReader(P, C);
        return dynamic_cast<ParticleTimeReader*>(S->GetReader(name));
    }
    double ParticleTimeReader::readEntry(size_t I) const {
        if (m_Component == MomentumComponent::Energy) return m_Particle->GetP4(I).E();
        return m_Particle->GetP4(I).M();
    }
    bool ParticleTimeReader::init(TTree* t) {
        if (!m_Registered) {
            Error("ParticleTimeReader::init()", "There were some problems in registering the reader. Please have a look");
            return false;
        }
        return m_Particle->init(t);
    }
    ParticleTimeReader::ParticleTimeReader(ParticleReader* P, MomentumComponent C) :
        ParticleVarReader(P),
        m_Component(C),
        m_Registered(false) {
        if (m_Component == MomentumComponent::Energy) {
            m_Name = "e";
        } else
            m_Name = "m";
        m_Registered = ITreeVarReaderStorage::GetInstance()->Register(this);
    }
}  // namespace XAMPP
