#include <RooAbsPdf.h>
#include <RooAddPdf.h>
#include <RooDataHist.h>
#include <RooFit.h>
#include <RooFitResult.h>
#include <RooHistPdf.h>
#include <RooPlot.h>
#include <RooRealVar.h>
#include <TFile.h>
#include <TH1.h>
#include <XAMPPplotting/BkgEstimateHelpers.h>
#include <XAMPPplotting/FinalPlotHelpers.h>
#include <XAMPPplotting/PlottingUtils.h>
namespace XAMPP {

    //##################################################################
    //                      MergeBin
    //##################################################################
    MergeBin::MergeBin(const TH1* H, int glob) :
        m_TH1(H),
        m_N(glob),
        m_content(m_TH1->GetBinContent(glob) / GetBinNormalization(H, glob)),
        m_error(m_TH1->GetBinError(glob) / GetBinNormalization(H, glob)),
        m_xlow(FLT_MAX),
        m_xhigh(-FLT_MAX),
        m_ylow(FLT_MAX),
        m_yhigh(-FLT_MAX),
        m_is_good(!isOverflowBin(H, glob)),
        m_absorbed() {
        int x(0), y(0), z(0);
        m_TH1->GetBinXYZ(glob, x, y, z);
        m_xlow = m_TH1->GetXaxis()->GetBinLowEdge(x);
        m_xhigh = m_TH1->GetXaxis()->GetBinUpEdge(x);
        if (m_TH1->GetDimension() > 1) {
            m_ylow = m_TH1->GetYaxis()->GetBinLowEdge(y);
            m_yhigh = m_TH1->GetYaxis()->GetBinUpEdge(y);
        }
    }

    int MergeBin::get() const { return m_N; }
    double MergeBin::content() const { return m_content; }
    double MergeBin::error() const { return m_error; }
    double MergeBin::relunc() const {
        if (content() <= 0.) return 2.;
        return error() / content();
    }

    bool MergeBin::adjacent(const MergeBin& other) const {
        if (m_TH1->GetDimension() == 1) { return (m_xhigh == other.m_xlow || m_xlow == other.m_xhigh); }
        return false;
    }

    double MergeBin::combined_relunc(const MergeBin& other) const {
        double den = content() + other.content();
        if (den <= 0.) return 2;
        return std::hypot(error(), other.error()) / den;
    }
    /// Combines the two bins and destroys the other
    void MergeBin::combine(std::shared_ptr<MergeBin>& other) {
        if (!other || !other->is_good() || other.get() == this) return;
        if (m_TH1->GetDimension() == 1) {
            m_xlow = std::min(m_xlow, other->m_xlow);
            m_xhigh = std::max(m_xhigh, other->m_xhigh);
        }
        m_absorbed.push_back(other);
        m_absorbed.insert(m_absorbed.end(), other->m_absorbed.begin(), other->m_absorbed.end());
        other->m_absorbed.clear();
        other->m_is_good = false;
        m_content += other->m_content;
        m_error = std::hypot(m_error, other->m_error);
    }

    bool MergeBin::is_good() const { return m_is_good; }
    double MergeBin::dR(const MergeBin& other) const { return std::hypot((m_xhigh - m_xlow), (other.m_xhigh - m_xlow)); }
    double MergeBin::x_low() const { return m_xlow; }
    double MergeBin::x_high() const { return m_xhigh; }

    HistogramRebinning::HistogramRebinning(const std::shared_ptr<TH1>& To_Rebin) : HistogramRebinning(To_Rebin.get()) {
        m_shared = To_Rebin;
    }
    HistogramRebinning::HistogramRebinning(const TH1* To_Rebin) :
        m_shared(nullptr),
        m_TH1(To_Rebin),
        m_max_bin(GetMaximumBin(To_Rebin)),
        m_max_bin_x(0),
        m_max_bin_y(0),
        m_max_bin_z(0) {
        To_Rebin->GetBinXYZ(m_max_bin, m_max_bin_x, m_max_bin_y, m_max_bin_z);
    }
    std::pair<int, int> HistogramRebinning::make_pair(int glob_bin) const {
        int x(0), y(0), z(0);
        m_TH1->GetBinXYZ(glob_bin, x, y, z);
        return std::pair<int, int>(x, y);
    }
    std::shared_ptr<TH1> HistogramRebinning::rebin_x(double aimed_uncert) const {
        HistogramRebinning x_binning(ProjectInto1D(m_TH1, 0, 0, m_TH1->GetNbinsY()));
        std::shared_ptr<TH1> rebinned_project_x = x_binning.rebin(aimed_uncert);
        std::vector<double> bins_x(ExtractBinning(rebinned_project_x->GetXaxis())), bins_y(ExtractBinning(m_TH1->GetYaxis()));

        std::shared_ptr<TH1> new_h = MakeTH1(bins_x, bins_y);
        TransformToBinning(m_TH1, new_h.get());
        CopyStyling(m_TH1, new_h.get());
        return new_h;
    }
    /// Rebinning methods w.r.t. the y-axis only
    /// The x-axis remains untouched
    std::shared_ptr<TH1> HistogramRebinning::rebin_y(double aimed_uncert) const {
        HistogramRebinning y_binning(ProjectInto1D(m_TH1, 0, 0, m_TH1->GetNbinsX()));
        std::shared_ptr<TH1> rebinned_project_y = y_binning.rebin(aimed_uncert);
        std::vector<double> bins_y(ExtractBinning(rebinned_project_y->GetXaxis())), bins_x(ExtractBinning(m_TH1->GetXaxis()));

        std::shared_ptr<TH1> new_h = MakeTH1(bins_x, bins_y);
        TransformToBinning(m_TH1, new_h.get());
        CopyStyling(m_TH1, new_h.get());
        return new_h;
    }
    std::shared_ptr<TH1> HistogramRebinning::rebin(double aimed_uncert) const {
        if (m_TH1->GetDimension() == 1) {
            std::vector<std::shared_ptr<MergeBin>> bins_by_content;
            bins_by_content.reserve(m_TH1->GetNbinsX());
            for (int i = m_TH1->GetNbinsX(); i > 0; --i) bins_by_content.push_back(std::make_shared<MergeBin>(m_TH1, i));

            std::sort(bins_by_content.begin(), bins_by_content.end(),
                      [](const std::shared_ptr<MergeBin>& a, std::shared_ptr<MergeBin>& b) { return a->relunc() > b->relunc(); });
            /// Nothing has to be done

            if (bins_by_content.front()->relunc() < aimed_uncert) return clone(m_TH1);
            while (bins_by_content.size() > 1 && bins_by_content.front()->relunc() > aimed_uncert) {
                std::shared_ptr<MergeBin> interest = bins_by_content.front();
                std::shared_ptr<MergeBin> best_match = nullptr;
                for (auto& b : bins_by_content) {
                    if (!b->is_good()) break;
                    if (interest->adjacent(*b) && (!best_match || interest->combined_relunc(*best_match) > interest->combined_relunc(*b)))
                        best_match = b;
                }
                if (!best_match) break;
                interest->combine(best_match);
                std::sort(bins_by_content.begin(), bins_by_content.end(),
                          [](const std::shared_ptr<MergeBin>& a, std::shared_ptr<MergeBin>& b) {
                              if (a->is_good() != b->is_good()) return a->is_good();
                              return a->relunc() > b->relunc();
                          });
            }
            /// Finally
            std::vector<double> new_bins;
            new_bins.reserve(bins_by_content.size() + 1);
            double x_max = -FLT_MAX;
            for (auto& b : bins_by_content) {
                if (!b->is_good()) break;
                new_bins.push_back(b->x_low());
                x_max = std::max(x_max, b->x_high());
            }
            new_bins.push_back(x_max);
            std::shared_ptr<TH1> new_h = MakeTH1(new_bins);
            TransformToBinning(m_TH1, new_h.get());
            CopyStyling(m_TH1, new_h.get());
            return new_h;
        }
        /// Fun stuff kicks in

        else if (m_TH1->GetDimension() == 2) {
            HistogramRebinning x_binning(ProjectInto1D(m_TH1, 0, 0, m_TH1->GetNbinsY()));
            HistogramRebinning y_binning(ProjectInto1D(m_TH1, 1, 0, m_TH1->GetNbinsX()));

            const int max_iter = 100;
            const double step_size = aimed_uncert / (2 * max_iter);
            double uncert1D_x(aimed_uncert), uncert1D_y(aimed_uncert);
            std::shared_ptr<TH1> new_h;

            for (unsigned int trial = 0; trial <= max_iter; ++trial) {
                std::shared_ptr<TH1> rebinned_project_x = x_binning.rebin(uncert1D_x);
                std::shared_ptr<TH1> rebinned_project_y = y_binning.rebin(uncert1D_y);

                std::vector<double> bins_x(ExtractBinning(rebinned_project_x->GetXaxis())),
                    bins_y(ExtractBinning(rebinned_project_y->GetXaxis()));

                new_h = MakeTH1(bins_x, bins_y);
                CopyStyling(m_TH1, new_h.get());
                TransformToBinning(m_TH1, new_h.get());
                if (trial == max_iter) break;
                HistogramRebinning tester(new_h);
                double max_relunc = 0.;
                std::vector<std::pair<int, int>> bins_to_work_on;
                for (int i = GetNbins(new_h); i > 0; --i) {
                    if (isOverflowBin(new_h, i) || tester.get_relunc(i) < aimed_uncert) continue;
                    bins_to_work_on.push_back(tester.make_pair(i));
                    max_relunc = std::max(tester.get_relunc(i), max_relunc);
                }
                if (bins_to_work_on.empty()) { break; }
                double x_mean(0), y_mean(0), x2_mean(0), y2_mean(0);
                for (auto& b : bins_to_work_on) {
                    x_mean += b.first;
                    y_mean += b.second;
                    x2_mean += b.first * b.first;
                    y2_mean += b.second * b.second;
                }
                x_mean /= bins_to_work_on.size();
                y_mean /= bins_to_work_on.size();
                x2_mean /= bins_to_work_on.size();
                y2_mean /= bins_to_work_on.size();
                double sigma_x = std::sqrt(x2_mean - x_mean * x_mean);
                double sigma_y = std::sqrt(y2_mean - y_mean * y_mean);

                /// We know from the initial conditions that each dimension respectively satisfies the
                /// precision condition
                int bin_x_max = std::min(int(x_mean) + int(sigma_x), m_TH1->GetNbinsX());
                int bin_x_min = std::max(int(x_mean) - int(sigma_x), 1);

                int bin_y_max = std::min(int(y_mean) + int(sigma_y), m_TH1->GetNbinsX());
                int bin_y_min = std::max(int(y_mean) - int(sigma_y), 1);
                /// Look at the relative uncertainty there
                double max_relunc_x(0), max_relunc_y(0);

                HistogramRebinning tester_x(rebinned_project_x), tester_y(rebinned_project_y);
                for (int i = bin_x_min; i <= bin_x_max; ++i) max_relunc_x = std::max(max_relunc_x, tester_x.get_relunc(i));
                for (int i = bin_y_min; i <= bin_y_max; ++i) max_relunc_y = std::max(max_relunc_y, tester_y.get_relunc(i));

                uncert1D_x = std::max(max_relunc_x - step_size * sigma_x / m_TH1->GetNbinsX(), step_size);
                uncert1D_y = std::max(max_relunc_y - step_size * sigma_y / m_TH1->GetNbinsY(), step_size);
            }
            return new_h;
        }
        return std::shared_ptr<TH1>();
    }
    double HistogramRebinning::get_relunc(const std::pair<int, int>& pair) const { return get_relunc(pair.first, pair.second); }
    double HistogramRebinning::get_relunc(const int x, const int y) const { return get_relunc(m_TH1->GetBin(x, y)); }
    double HistogramRebinning::get_relunc(const int bin) const {
        /// Assign a flat two
        if (m_TH1->GetBinContent(bin) <= 0.)
            return 2;
        else if (isOverflowBin(m_TH1, bin))
            return FLT_MAX;
        return m_TH1->GetBinError(bin) / m_TH1->GetBinContent(bin);
    }
    //##################################################################
    //                      FractionFit
    //##################################################################
    FractionFit::FractionFit(const std::shared_ptr<TH1>& data, const std::shared_ptr<TH1>& f1, const std::shared_ptr<TH1>& f2) :
        m_data(data),
        m_f1(f1),
        m_f2(f2),
        m_predicted(clone(data, true)),
        m_var(std::make_shared<RooRealVar>(RandomString(24).c_str(), m_data->GetXaxis()->GetTitle(), m_data->GetXaxis()->GetBinLowEdge(1),
                                           PlotRange(m_data->GetXaxis()) + m_data->GetXaxis()->GetBinLowEdge(1))),
        m_rooplot(std::shared_ptr<RooPlot>(m_var->frame())),
        m_fraction(std::make_shared<RooRealVar>("fraction", "fraction of something", 0, 1)),
        m_data_hist(nullptr),
        m_f1_hist(nullptr),
        m_f2_hist(nullptr),
        m_f1_pdf(nullptr),
        m_f2_pdf(nullptr),
        m_final_pdf(nullptr),
        m_is_valid(false),
        m_integral(Integrate(m_data)),
        m_chi2(0) {
        m_data_hist = std::make_shared<RooDataHist>("fit_data", "data", *m_var, RooFit::Import(*m_data));
        m_f1_hist = std::make_shared<RooDataHist>("f1 data", "data", *m_var, RooFit::Import(*m_f1));
        m_f2_hist = std::make_shared<RooDataHist>("f2 data", "data", *m_var, RooFit::Import(*m_f2));
        m_f1_pdf = std::make_shared<RooHistPdf>("f1_pdf", "pdf", *m_var, *m_f1_hist);
        m_f2_pdf = std::make_shared<RooHistPdf>("f2_pdf", "pdf", *m_var, *m_f2_hist);
        m_fraction = std::make_shared<RooRealVar>("fraction", "fraction of something", 0, 1);
        m_final_pdf = std::make_shared<RooAddPdf>("f1plusf2", "Added model", RooArgList(*m_f1_pdf, *m_f2_pdf), *m_fraction);
    }

    double FractionFit::evalute_fit(double x) const {
        m_var->setVal(x);
        return m_final_pdf->getVal(*m_var) * integral();
    }

    double FractionFit::integral() const { return m_integral; }
    double FractionFit::chi2() const { return m_chi2; }
    unsigned int FractionFit::nBins() const { return GetNbins(m_data); }
    std::shared_ptr<TH1> FractionFit::predicted() const { return m_predicted; }
    const std::shared_ptr<TH1> FractionFit::data() const { return m_data; }

    bool FractionFit::fit(int max_trials) {
        int status = -1;
        int trial = 0;
        for (; trial < max_trials; ++trial) {
            std::unique_ptr<RooFitResult> R(m_final_pdf->fitTo(*m_data_hist, RooFit::Minos(false), RooFit::SumW2Error(true),
                                                               RooFit::PrintLevel(-1000),
                                                               RooFit::Save(true)  // without this, you will get a null-pointer
                                                               ));
            status = R->status();
            if (status == 0) break;
        }
        if (status == 0) {
            m_data_hist->plotOn(m_rooplot.get(), RooFit::Name("Data"));
            m_final_pdf->plotOn(m_rooplot.get(), RooFit::LineColor(kRed), RooFit::Name("Model"));
            m_final_pdf->plotOn(m_rooplot.get(), RooFit::LineColor(kBlue), RooFit::Components(*m_f1_pdf), RooFit::Name("f1"));

            m_chi2 = 0;
            for (unsigned int i = 1; i <= nBins(); ++i) {
                double c = m_data->GetBinContent(i);
                double e = m_data->GetBinError(i);
                double bin = evalute_fit(m_data->GetBinCenter(i)) * m_data->GetXaxis()->GetBinWidth(i);
                predicted()->SetBinContent(i, bin);
                predicted()->SetBinError(i, std::sqrt(bin));
                m_chi2 += pow(bin - c, 2) / pow(c > 0. ? e : 1, 2);
            }
            m_is_valid = true;
            return true;
        } else if (status == 1) {
            Warning("FractionFit::fit()", "Covariance was made to be positive definte!");
        } else if (status == 2) {
            Warning("FractionFit::fit()", "Hesse is invalid!");
        } else if (status == 3) {
            Warning("FractionFit::fit()", "Edm is above max!");
        } else if (status == 4) {
            Warning("FractionFit::fit()", "Reached call limit!");
        } else if (status == 5) {
            Warning("FractionFit::fit()", "Any other failure!");
        }
        if (max_trials == trial) Warning("FractionFit::fit()", "Number of maximum trials exceeded.");

        return false;
    }
    double FractionFit::fraction_f1() const { return m_fraction->getVal(); }
    double FractionFit::fraction_f1_error() const { return m_fraction->getError(); }
    std::shared_ptr<RooPlot> FractionFit::get_frame() const { return m_rooplot; }
    bool FractionFit::is_valid() const { return m_is_valid; }
    //##################################################################
    //                      FractionSlicer
    //##################################################################
    FractionSlicer::FractionSlicer(const SampleHisto* data, const SampleHisto* f1, const SampleHisto* f2, unsigned int axis) :
        m_data(data),
        m_f1(f1),
        m_f2(f2),
        m_axis(axis),
        m_final_results(nullptr),
        m_fits() {
        RooMsgService::instance().setGlobalKillBelow(RooFit::FATAL);
    }
    std::shared_ptr<TH1> FractionSlicer::get_result() const { return m_final_results; }
    std::vector<std::shared_ptr<FractionFit>> FractionSlicer::get_fits() const { return m_fits; }
    bool FractionSlicer::fit(const TH1* ref_binning) {
        if (m_axis > 1 || m_data->GetDimension() != 2 || m_f1->GetDimension() != 2 || m_f2->GetDimension() != 2) {
            Error("FractionSlicer::fit()", "Only 2 variables supported");
            return false;
        }
        m_fits.clear();

        std::shared_ptr<TH1> data = ref_binning ? m_data->TransformToBinning(ref_binning) : m_data->GetHistogram();
        std::shared_ptr<TH1> f1 = ref_binning ? m_f1->TransformToBinning(ref_binning) : m_f1->GetHistogram();
        std::shared_ptr<TH1> f2 = ref_binning ? m_f2->TransformToBinning(ref_binning) : m_f2->GetHistogram();

        m_final_results = MakeTH1(ExtractBinning(m_axis == 0 ? data->GetXaxis() : data->GetYaxis()));
        for (int bin = 1; bin <= (m_axis == 0 ? data->GetNbinsX() : data->GetNbinsY()); ++bin) {
            std::shared_ptr<TH1> sliced_data = ProjectInto1D(data, m_axis == 0 ? 1 : 0, bin, bin + 1);
            std::shared_ptr<TH1> sliced_f1 = ProjectInto1D(f1, m_axis == 0 ? 1 : 0, bin, bin + 1);
            std::shared_ptr<TH1> sliced_f2 = ProjectInto1D(f2, m_axis == 0 ? 1 : 0, bin, bin + 1);

            truncate(sliced_data);
            truncate(sliced_f1);
            truncate(sliced_f2);

            unsigned int f_data(first_filled_bin(sliced_data)), l_data(last_filled_bin(sliced_data));

            sliced_data = cut_edges(sliced_data, f_data, l_data);
            sliced_f1 = cut_edges(sliced_f1, f_data, l_data);
            sliced_f2 = cut_edges(sliced_f2, f_data, l_data);

            sliced_data = ReBinHisto(sliced_data, 0.05);
            sliced_f1 = transform_to_bin_numbers(transform_to_binning(sliced_f1, sliced_data));
            sliced_f2 = transform_to_bin_numbers(transform_to_binning(sliced_f2, sliced_data));
            sliced_data = transform_to_bin_numbers(sliced_data);

            std::shared_ptr<FractionFit> fit = std::make_shared<FractionFit>(sliced_data, sliced_f1, sliced_f2);
            m_fits.push_back(fit);
            if (fit->fit()) {
                m_final_results->SetBinContent(bin, fit->fraction_f1());
                m_final_results->SetBinError(bin, fit->fraction_f1_error());
            } else {
                m_final_results->SetBinContent(bin, 2.);
            }
        }
        return true;
    }
    int FractionSlicer::first_filled_bin(std::shared_ptr<TH1> h) const {
        for (unsigned int i = 1; i < GetNbins(h); ++i) {
            if (isOverflowBin(h, i)) continue;
            if (h->GetBinContent(i) > 0) return i;
        }
        return GetNbins(h);
    }
    int FractionSlicer::last_filled_bin(std::shared_ptr<TH1> h) const {
        for (unsigned int i = GetNbins(h); i > 0; --i) {
            if (isOverflowBin(h, i)) continue;
            if (h->GetBinContent(i) > 0) return i;
        }
        return 0;
    }
    std::shared_ptr<TH1> FractionSlicer::transform_to_binning(std::shared_ptr<TH1> from, std::shared_ptr<TH1> to) const {
        if (ExtractBinning(from->GetXaxis()) == ExtractBinning(to->GetXaxis())) return from;
        std::shared_ptr<TH1> new_bining = clone(to, true);
        TransformToBinning(from.get(), new_bining.get());
        return new_bining;
    }
    std::shared_ptr<TH1> FractionSlicer::transform_to_bin_numbers(std::shared_ptr<TH1> from) const {
        std::shared_ptr<TH1> transformed =
            std::make_shared<TH1D>(RandomString(40).c_str(), "transformed bins", from->GetNbinsX(), 1, from->GetNbinsX());
        transformed->GetXaxis()->SetTitle(Form("Bin number of %s", from->GetXaxis()->GetTitle()));
        for (unsigned int i = 1; i < GetNbins(from); ++i) {
            transformed->SetBinContent(i, from->GetBinContent(i));
            transformed->SetBinError(i, from->GetBinError(i));
        }
        return transformed;
    }
    void FractionSlicer::truncate(std::shared_ptr<TH1> h) const {
        for (unsigned int i = 1; i <= GetNbins(h); ++i) {
            if (h->GetBinContent(i) < 0) {
                Info("FractionFit::trunacate()", Form("Truncate bin %d in histo %s", i, h->GetTitle()));
                h->SetBinContent(i, 0);
                h->SetBinError(i, 1);
            }
        }
    }
    std::shared_ptr<TH1> FractionSlicer::cut_edges(std::shared_ptr<TH1> h, unsigned int below_x, unsigned int above_x) const {
        if (below_x != 1 || above_x != GetNbins(h)) {
            std::vector<double> binning = ExtractBinning(h->GetXaxis());
            EraseFromVector<double>(binning, [&h, &below_x, above_x](const double& dbl) {
                if (below_x > 1 && dbl < h->GetXaxis()->GetBinLowEdge(below_x)) return true;
                if (above_x != GetNbins(h) && dbl > h->GetXaxis()->GetBinUpEdge(above_x)) return true;
                return false;
            });
            std::shared_ptr<TH1> cutted = MakeTH1(binning);
            TransformToBinning(h.get(), cutted.get());
            return cutted;
        }

        return h;
    }

}  // namespace XAMPP
