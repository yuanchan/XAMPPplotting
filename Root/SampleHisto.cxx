#include <XAMPPplotting/FinalPlotHelpers.h>
#include <XAMPPplotting/PlottingUtils.h>
#include <XAMPPplotting/SampleHisto.h>

#include <TFile.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>

namespace XAMPP {
    //#####################################################
    //                  SampleHisto
    //#####################################################
    SampleHisto::SampleHisto(const std::string& sample_name, const std::string& variable, const std::string& analysis,
                             const std::string& region, const std::string& root_file) :
        SampleHisto(sample_name, variable, analysis, region, std::vector<std::string>{root_file}) {}

    SampleHisto::SampleHisto(const std::string& sample_name, const std::string& variable, const std::string& analysis,
                             const std::string& region, const std::vector<std::string>& files) :
        m_analysis(analysis),
        m_region(region),
        m_variable(variable),
        m_name(sample_name),
        m_label(),
        m_nominal_histo(),
        m_sample_type(SampleHisto::SampleType::UnDefined),
        m_root_files(),
        m_tried_to_load(false),
        m_variations(),
        m_up_syst_histo(),
        m_down_syst_histo(),
        m_legend_order(-1),
        m_integral(std::nan("1")),
        m_lumi(1.),
        m_nominal(),
        m_syst(),
        m_excluded_syst(),
        m_syst_set_external(false),
        m_nominal_set_external(false),
        m_theo_uncert(-1.),
        m_drawStyle("HIST") {
        m_root_files.reserve(m_root_files.size());
        for (auto path : files) {
            std::shared_ptr<TFile> file_ptr = FileHandler::getInstance()->LoadFile(path);
            if (!file_ptr) {
                m_root_files.clear();
                break;
            } else
                m_root_files.push_back(file_ptr);
        }
    }
    SampleHisto::SampleHisto(const std::string& sample_name, const SampleHisto& signal_histo, const SampleHisto& background_histo) :
        m_analysis(signal_histo.m_analysis),
        m_region(signal_histo.m_region),
        m_variable(signal_histo.m_variable),
        m_name(sample_name),
        m_label(),
        m_nominal_histo(),
        m_sample_type(SampleHisto::SampleType::UnDefined),
        m_root_files(),
        m_tried_to_load(false),
        m_variations(),
        m_up_syst_histo(),
        m_down_syst_histo(),
        m_legend_order(signal_histo.m_legend_order),
        m_integral(std::nan("1")),
        m_lumi(1.),
        m_nominal(),
        m_syst(),
        m_excluded_syst(),
        m_syst_set_external(false),
        m_nominal_set_external(false),
        m_theo_uncert(-1.),
        m_drawStyle(signal_histo.m_drawStyle) {
        CopyVector(signal_histo.m_root_files, m_root_files);
        CopyVector(background_histo.m_root_files, m_root_files);
    }
    SampleHisto::SampleHisto(const std::string& sample_name, const SampleHisto* signal_histo, const SampleHisto* background_histo) :
        SampleHisto(sample_name, *signal_histo, *background_histo) {}
    SampleHisto::SampleHisto(const std::string& sample_name, const std::shared_ptr<SampleHisto>& signal_histo,
                             const std::shared_ptr<SampleHisto>& background_histo) :
        SampleHisto(sample_name, signal_histo.get(), background_histo.get()) {}
    std::string SampleHisto::GetTitle() const {
        if (m_label.empty()) return GetName();
        return m_label;
    }
    std::vector<std::shared_ptr<SystematicPairHistos>> SampleHisto::GetSystComponents(unsigned int sorting) const {
        if (sorting != SystErrorSorting::Naming) {
            std::vector<std::shared_ptr<SystematicPairHistos>> sorted = m_variations;
            if (sorting == SystErrorSorting::LargestUp) {
                std::sort(sorted.begin(), sorted.end(),
                          [](const std::shared_ptr<SystematicPairHistos>& a, const std::shared_ptr<SystematicPairHistos>& b) {
                              return a->total_up_dev() > b->total_up_dev();
                          });
            } else if (sorting == SystErrorSorting::LargestDown) {
                std::sort(sorted.begin(), sorted.end(),
                          [](const std::shared_ptr<SystematicPairHistos>& a, const std::shared_ptr<SystematicPairHistos>& b) {
                              return a->total_dn_dev() > b->total_dn_dev();
                          });
            } else if (sorting == SystErrorSorting::SmallestUp) {
                std::sort(sorted.begin(), sorted.end(),
                          [](const std::shared_ptr<SystematicPairHistos>& a, const std::shared_ptr<SystematicPairHistos>& b) {
                              return a->total_up_dev() < b->total_up_dev();
                          });
            } else if (sorting == SystErrorSorting::SmallestDown) {
                std::sort(sorted.begin(), sorted.end(),
                          [](const std::shared_ptr<SystematicPairHistos>& a, const std::shared_ptr<SystematicPairHistos>& b) {
                              return a->total_dn_dev() < b->total_dn_dev();
                          });
            }
            return sorted;
        }
        return m_variations;
    }
    bool SampleHisto::isData() const { return getSampleType() == SampleType::Data; }
    bool SampleHisto::isIrreducible() const { return getSampleType() == SampleType::Irreducible; }
    bool SampleHisto::isReducible() const { return getSampleType() == SampleType::Reducible; }
    bool SampleHisto::isBackground() const { return isIrreducible() || isReducible(); }
    bool SampleHisto::isSignal() const { return getSampleType() == SampleType::Signal; }
    void SampleHisto::setSampleType(unsigned int T) {
        if (T != SampleType::UnDefined && T < SampleType::Exceeds && getSampleType() == SampleType::UnDefined) m_sample_type = T;
    }
    unsigned int SampleHisto::getSampleType() const { return m_sample_type; }
    bool SampleHisto::isHistoLoaded() const { return !m_root_files.empty() && GetHistogram().get() != nullptr; }
    // Get the histogram pointer
    std::shared_ptr<TH1> SampleHisto::GetHistogram() const { return m_nominal_histo; }
    std::string SampleHisto::GetAnalysis() const { return m_analysis; }
    std::string SampleHisto::GetRegion() const { return m_region; }
    std::string SampleHisto::GetVariableName() const { return m_variable; }
    std::string SampleHisto::GetName() const { return m_name; }
    std::string SampleHisto::GetPlotPath(const std::string& systematic, const std::string& delimiter) const {
        return GetAnalysis() + std::string(!systematic.empty() ? delimiter : "") + systematic + "/" + GetRegion() + "/" + GetVariableName();
    }
    void SampleHisto::setDrawStyle(const std::string& style) { m_drawStyle = style; }

    std::shared_ptr<TH1> SampleHisto::load_from_file(const std::shared_ptr<TFile>& file, const std::string& syst) const {
        TH1* histo_in_file = nullptr;
        file->GetObject(GetPlotPath(syst).c_str(), histo_in_file);
        std::shared_ptr<TH1> histo_owner = std::shared_ptr<TH1>(histo_in_file);
        if (histo_owner)
            histo_owner->SetDirectory(nullptr);
        else {
            file->GetObject(GetPlotPath(syst, "__").c_str(), histo_in_file);
            histo_owner = std::shared_ptr<TH1>(histo_in_file);
            if (histo_owner) histo_owner->SetDirectory(nullptr);
        }
        return histo_owner;
    }
    std::shared_ptr<TH1> SampleHisto::load_from_files(const std::string& systematic) const {
        std::shared_ptr<TH1> Loaded_Ptr;
        for (const auto& file : m_root_files) {
            std::shared_ptr<TH1> histo_owner = load_from_file(file, systematic);
            if (!histo_owner && m_nominal != systematic) {
                histo_owner = load_from_file(file, m_nominal);
                static std::vector<std::string> warned_syst;
                if (!IsElementInList(warned_syst, systematic)) {
                    Warning("SampleHisto()",
                            Form("Systematic %s is not defined in %s try to use nominal instead", systematic.c_str(), file->GetName()));
                    warned_syst.push_back(systematic);
                }
            }
            if (!histo_owner) {
                Error("SampleHisto()", Form("Could not find any object at %s", GetPlotPath(systematic).c_str()));
                return std::shared_ptr<TH1>();
            } else if (!Loaded_Ptr) {
                Loaded_Ptr = histo_owner;
            } else if (!Loaded_Ptr->Add(histo_owner.get())) {
                return std::shared_ptr<TH1>();
            }
        }
        if (Loaded_Ptr) {
            Loaded_Ptr->SetTitle(GetTitle().c_str());
            // Actually these are the standard  settings from ROOT somehow TFile Get screws them up sometimes
            Loaded_Ptr->GetXaxis()->SetTitleSize(0.05);
            Loaded_Ptr->GetXaxis()->SetLabelSize(0.05);
            Loaded_Ptr->GetYaxis()->SetTitleSize(0.05);
            Loaded_Ptr->GetYaxis()->SetLabelSize(0.05);
            Loaded_Ptr->GetXaxis()->SetTitleOffset(1.4);
            Loaded_Ptr->GetYaxis()->SetTitleOffset(1.4);
        }
        return Loaded_Ptr;
    }
    unsigned int SampleHisto::GetDimension() const {
        if (!isHistoLoaded()) return 0;
        return GetHistogram()->GetDimension();
    }
    bool SampleHisto::loadIntoMemory() {
        if (isHistoLoaded()) return true;   // Everything is fine
        if (m_tried_to_load) return false;  // It has been already tried to initialize the histogram and nothing happened
        m_tried_to_load = true;
        std::string nominal;
        std::vector<std::string> common_syst;
        std::vector<FileStructure_Ptr> structures = FileHandler::getInstance()->FileStructures();
        // load first the common structures in to the memory
        // Might depend on the sample composition
        bool nominal_set = false;
        for (const auto& file : structures) {
            if (!IsElementInList(m_root_files, file->reference())) continue;
            PlotAnalysis_Ptr plot_ana = file->get_analysis(GetAnalysis());
            if (!plot_ana) {
                Error("SampleHisto()", Form("File %s does not contain stream %s", file->reference()->GetName(), GetAnalysis().c_str()));
                return false;
            }
            // First file take all information from here
            else if (!nominal_set) {
                nominal = plot_ana->nominal();
                nominal_set = true;
            } else if (nominal != plot_ana->nominal()) {
                Error("SampleHisto()", "Different nominal names.");
                return false;
            } else if (!SystematicPairer::getInstance()->do_systematics())
                continue;
            CopyVector(plot_ana->get_systematics(), common_syst);
        }
        if (m_syst_set_external) CopyVector(m_syst, common_syst, true);
        if (m_nominal_set_external)
            nominal = m_nominal;
        else
            m_nominal = nominal;

        EraseFromVector<std::string>(
            common_syst, [this, &nominal](const std::string& s) { return s == nominal || IsElementInList(m_excluded_syst, s); });
        m_nominal_histo = load_from_files(nominal);
        if (!m_nominal_histo) {
            m_root_files.clear();
            return false;
        }
        if (SystematicPairer::getInstance()->do_systematics()) {
            m_up_syst_histo = clone(m_nominal_histo, true);
            m_down_syst_histo = clone(m_nominal_histo, true);
            // Fill the systemaic map
            std::map<std::string, std::shared_ptr<TH1>> syst_map;
            for (const auto& syst : common_syst) {
                std::shared_ptr<TH1> variation_histo = load_from_files(syst);

                if (!variation_histo) {
                    m_root_files.clear();
                    return false;
                }
                if (!IsFinite(variation_histo)) {
                    Warning("SampleHisto::loadIntoMemory()",
                            Form("The variation %s does not return a number in sample %s. Discard it", syst.c_str(), GetName().c_str()));
                    continue;
                }
                variation_histo->SetTitle(Form("%s (%s)", GetName().c_str(), syst.c_str()));
                syst_map.insert(std::pair<std::string, std::shared_ptr<TH1>>(syst, variation_histo));
            }
            // Calculate the envelopes first
            for (const auto& grp : SystematicPairer::getInstance()->get_envelopes()) {
                unsigned int map_size = syst_map.size();
                std::shared_ptr<TH1> env = grp->calculate_syst(syst_map, GetHistogram());
                /// Nothing has changed actually
                if (map_size == syst_map.size() || IsElementInList(m_excluded_syst, grp->name()) ||
                    SystematicPairer::getInstance()->is_systematic_excluded(grp->name()))
                    continue;
                if ((grp->add_to_down() && !add_in_quadrature(env, m_down_syst_histo)) ||
                    (grp->add_to_up() && !add_in_quadrature(env, m_up_syst_histo))) {
                    Error("SampleHisto()", Form("Failed to evaluate systematic group %s", grp->get_pattern().c_str()));
                    return false;
                }
                m_variations.push_back(std::make_shared<SystematicPairHistos>(this, grp, env));
            }
            // Now the fun begins with the asymmetric systematics
            for (std::map<std::string, std::shared_ptr<TH1>>::iterator syst_pair = syst_map.begin(); syst_pair != syst_map.end();
                 ++syst_pair) {
                std::string assoc_sys_name = SystematicPairer::getInstance()->get_paired(syst_pair->first);
                // Standalone systematic
                variation_pair delta;
                if (assoc_sys_name.empty()) {
                    delta = evaluate_sys(syst_pair->second, syst_pair->second);
                    m_variations.push_back(std::make_shared<SystematicPairHistos>(this, syst_pair->first, delta.first));
                } else {
                    std::map<std::string, std::shared_ptr<TH1>>::iterator associated_sys = syst_map.find(assoc_sys_name);
                    // Put an error mesage here later
                    if (associated_sys == syst_map.end()) {
                        Warning("SampleHisto()", Form("The systematic %s is associated to %s but was not found in sample %s. Add the "
                                                      "existing one in quadrature to up and down.",
                                                      syst_pair->first.c_str(), assoc_sys_name.c_str(), GetName().c_str()));
                        delta = evaluate_sys(syst_pair->second, syst_pair->second);
                        m_variations.push_back(std::make_shared<SystematicPairHistos>(this, syst_pair->first, delta.first));
                    } else {
                        std::shared_ptr<TH1> up = SystematicPairer::getInstance()->is_systematic_up(syst_pair->first)
                                                      ? syst_pair->second
                                                      : associated_sys->second;
                        std::shared_ptr<TH1> down = SystematicPairer::getInstance()->is_systematic_down(syst_pair->first)
                                                        ? syst_pair->second
                                                        : associated_sys->second;
                        delta = evaluate_sys(down, up);
                        std::string s_name = syst_pair->first.substr(0, syst_pair->first.rfind("1"));
                        while (s_name.rfind("_") == s_name.size() - 1) { s_name = s_name.substr(0, s_name.rfind("_")); }
                        m_variations.push_back(std::make_shared<SystematicPairHistos>(this, s_name, delta.first, delta.second));
                        syst_map.erase(associated_sys);
                    }
                }
                if (!add_in_quadrature(delta.first, m_down_syst_histo) || !add_in_quadrature(delta.second, m_up_syst_histo)) {
                    Error("SampleHisto()", Form("Failed to evaluate systematic %s", syst_pair->first.c_str()));
                    return false;
                }
            }
            // Finally we want to add the theory uncertainties if there are any
            if (m_theo_uncert > 0) {
                std::shared_ptr<TH1> theo_up = clone(GetHistogram());
                std::shared_ptr<TH1> theo_dn = clone(GetHistogram());
                theo_up->Scale(1. + m_theo_uncert);
                if (m_theo_uncert < 0.) theo_dn->Scale(1 - m_theo_uncert);
                variation_pair delta = evaluate_sys(theo_dn, theo_up);
                if (!add_in_quadrature(delta.first, m_down_syst_histo) || !add_in_quadrature(delta.second, m_up_syst_histo)) {
                    Error("SampleHisto()", "Failed to evaluate theory uncertainty.");
                    return false;
                }
            }
        }

        EraseFromVector<std::shared_ptr<SystematicPairHistos>>(m_variations, [](const std::shared_ptr<SystematicPairHistos>& a) {
            return !a->has_valid_histo() || (a->total_dn_dev() == 0 && a->total_up_dev() == 0.);
        });
        std::sort(m_variations.begin(), m_variations.end(),
                  [](const std::shared_ptr<SystematicPairHistos>& a, const std::shared_ptr<SystematicPairHistos>& b) {
                      return a->name() < b->name();
                  });
        m_integral = Integrate(GetHistogram());
        return isHistoLoaded();
    }
    bool SampleHisto::isValid() {
        if (!loadIntoMemory()) return false;
        if (!IsFinite(m_integral)) {
            Warning("SampleHisto()", Form("%s returns in region %s not a number for sample %s.... skipping", GetVariableName().c_str(),
                                          GetRegion().c_str(), GetName().c_str()));
            return false;
        } else if (m_integral < 0) {
            Warning("SampleHisto()", Form("%s has negative integral (%.2f) in region %s for sample %s.... skipping",
                                          GetVariableName().c_str(), m_integral, GetRegion().c_str(), GetName().c_str()));
            return false;
        }
        return true;
    }
    bool SampleHisto::add_in_quadrature(std::shared_ptr<TH1> add, std::shared_ptr<TH1> to) const {
        if (!add || !to) {
            Error("SampleHisto()", "One of the histograms is empty");
            return false;
        }
        if (GetNbins() != XAMPP::GetNbins(add) || GetNbins() != XAMPP::GetNbins(to)) {
            std::cout << GetNbins() << " " << XAMPP::GetNbins(add) << XAMPP::GetNbins(to) << std::endl;
            return false;
        }
        for (int N = GetNbins(); N >= 0; --N) {
            if (isOverFlow(N) && hasAlphaNumericLabels()) continue;
            double new_content = std::pow(add->GetBinContent(N), 2) + std::pow(to->GetBinContent(N), 2);
            if (!IsFinite(new_content)) {
                Error("SampleHisto()", Form("Quadrature add failed for bin %d since the content is %f & %f.", N, add->GetBinContent(N),
                                            to->GetBinContent(N)));
                return false;
            }
            to->SetBinContent(N, std::sqrt(new_content));
        }

        return true;
    }
    bool SampleHisto::isOverFlow(int bin) const { return isOverflowBin(GetHistogram(), bin); }
    unsigned int SampleHisto::GetNbins() const { return XAMPP::GetNbins(GetHistogram()); }
    SampleHisto::variation_pair SampleHisto::GetSystErrorBands() const {
        if (SystematicPairer::getInstance()->do_systematics()) return variation_pair(m_down_syst_histo, m_up_syst_histo);
        return variation_pair(clone(GetHistogram(), true), clone(GetHistogram(), true));
    }
    SampleHisto::variation_pair SampleHisto::evaluate_sys(std::shared_ptr<TH1> down, std::shared_ptr<TH1> up) const {
        std::shared_ptr<TH1> sigma_up = clone(GetHistogram(), true);
        std::shared_ptr<TH1> sigma_down = clone(GetHistogram(), true);
        if (up == down) sigma_down = sigma_up;
        for (int N = GetNbins(); N >= 0; --N) {
            if (isOverFlow(N) && hasAlphaNumericLabels()) continue;
            double up_var = up->GetBinContent(N) - GetHistogram()->GetBinContent(N);
            double down_var = down->GetBinContent(N) - GetHistogram()->GetBinContent(N);
            sigma_up->SetBinContent(N, up_var);
            sigma_down->SetBinContent(N, down_var);
        }
        return variation_pair(sigma_down, sigma_up);
    }
    bool SampleHisto::isTH1() const { return isHistoLoaded() && !(isTH2() || isTH3() || isTH2Poly()); }
    bool SampleHisto::isTH2() const { return dynamic_cast<TH2*>(GetHistogram().get()) != nullptr; }
    bool SampleHisto::isTH3() const { return dynamic_cast<TH3*>(GetHistogram().get()) != nullptr; }
    bool SampleHisto::isTH2Poly() const { return dynamic_cast<TH2Poly*>(GetHistogram().get()) != nullptr; }
    double SampleHisto::GetBinContent(int X, int Y, int Z) const {
        if (GetDimension() == 3 && Y > -1 && Z > -1)
            return GetHistogram()->GetBinContent(X, Y, Z);
        else if (GetDimension() == 2 && Y > -1)
            return GetHistogram()->GetBinContent(X, Y);
        else if (GetDimension() >= 1)
            return GetHistogram()->GetBinContent(X);
        return std::nan("2");
    }
    double SampleHisto::GetBinError(int X, int Y, int Z) const {
        if (GetDimension() == 3 && Y > -1 && Z > -1)
            return GetHistogram()->GetBinError(X, Y, Z);
        else if (GetDimension() == 2 && Y > -1)
            return GetHistogram()->GetBinError(X, Y);
        else if (GetDimension() >= 1)
            return GetHistogram()->GetBinError(X);
        return std::nan("2");
    }
    int SampleHisto::FindBin(double x, double y, double z) const {
        if (GetDimension() == 1)
            return GetHistogram()->FindBin(x);
        else if (GetDimension() == 2)
            return GetHistogram()->FindBin(x, y);
        else if (GetDimension() == 3)
            return GetHistogram()->FindBin(x, y, z);
        return -1;
    }
    void SampleHisto::SetLumi(double lumi) {
        if (isData() || lumi == m_lumi) return;
        Scale(lumi / GetLumi());
        m_lumi = lumi;
    }
    void SampleHisto::Scale(double scale) {
        if (loadIntoMemory())
            GetHistogram()->Scale(scale);
        else
            return;

        for (const auto& H : m_variations) H->Scale(scale);

        if (m_up_syst_histo) m_up_syst_histo->Scale(scale);
        if (m_down_syst_histo) m_down_syst_histo->Scale(scale);
    }
    double SampleHisto::GetLumi() const { return m_lumi; }
    double SampleHisto::Integral() const { return m_integral; }
    TAxis* SampleHisto::GetAxis(unsigned int D) const {
        if (isHistoLoaded()) {
            if (D == 2)
                return GetHistogram()->GetZaxis();
            else if (D == 1)
                return GetHistogram()->GetYaxis();
            else if (D == 0)
                return GetHistogram()->GetXaxis();
        }
        return nullptr;
    }
    TAxis* SampleHisto::GetXaxis() const { return GetAxis(0); }
    TAxis* SampleHisto::GetYaxis() const { return GetAxis(1); }
    TAxis* SampleHisto::GetZaxis() const { return GetAxis(2); }

    std::string SampleHisto::GetDrawStyle() const {
        if (!isHistoLoaded()) return "";
        return m_drawStyle;
    }
    std::string SampleHisto::GetLegendDrawStyle() const {
        if (!isHistoLoaded()) return "";
        if (GetHistogram()->GetMarkerStyle() != -1) return "PL";
        return "L";
    }
    int SampleHisto::GetLegendOrder() const { return m_legend_order; }
    void SampleHisto::setLegendOrder(int I) { m_legend_order = I; }
    std::string SampleHisto::getSampleTypeName() const {
        if (isData())
            return "Data";
        else if (isIrreducible())
            return "Irreducible background";
        else if (isReducible())
            return "Reducible background";
        else if (isSignal())
            return "Signal";
        return "Undefined type";
    }
    void SampleHisto::SetLineColor(Color_t C) {
        if (loadIntoMemory() && C > -1) GetHistogram()->SetLineColor(C);
    }
    void SampleHisto::SetFillColor(Color_t C) {
        if (loadIntoMemory() && C > -1) GetHistogram()->SetFillColor(C);
    }
    void SampleHisto::SetFillColorAlpha(float A) {
        if (loadIntoMemory() && A > -1) GetHistogram()->SetFillColorAlpha(GetHistogram()->GetFillColor(), A);
    }
    void SampleHisto::SetMarkerColor(Color_t C) {
        if (loadIntoMemory() && C > -1) GetHistogram()->SetMarkerColor(C);
    }
    void SampleHisto::SetFillStyle(Style_t S) {
        if (loadIntoMemory() && S > -1) GetHistogram()->SetFillStyle(S);
    }
    void SampleHisto::SetLineStyle(Style_t S) {
        if (loadIntoMemory() && S > -1) GetHistogram()->SetLineStyle(S);
    }
    void SampleHisto::SetMarkerStyle(Style_t S) {
        if (loadIntoMemory() && S > -1) GetHistogram()->SetMarkerStyle(S);
    }
    void SampleHisto::SetMarkerSize(Size_t S) {
        if (loadIntoMemory() && S > -1) GetHistogram()->SetMarkerSize(S);
    }
    void SampleHisto::SetLineWidth(Width_t W) {
        if (loadIntoMemory() && W > -1) GetHistogram()->SetLineWidth(W);
    }
    bool SampleHisto::Add(std::shared_ptr<SampleHisto> other, double scale) { return Add(other.get(), scale); }
    bool SampleHisto::Add(SampleHisto* other, double scale) {
        if (!other->loadIntoMemory() || !loadIntoMemory()) return false;
        if (!Add(other->GetHistogram(), scale)) return false;
        // still to  do the systematics
        return true;
    }
    bool SampleHisto::Add(std::shared_ptr<TH1> Histo, double scale) { return Add(Histo.get(), scale); }
    bool SampleHisto::Add(const TH1* Histo, double scale) {
        if (Histo == nullptr || !loadIntoMemory()) return false;
        return GetHistogram()->Add(Histo, scale);
    }
    bool SampleHisto::Multiply(std::shared_ptr<SampleHisto> other, double scale) { return Multiply(other.get(), scale); }
    bool SampleHisto::Multiply(SampleHisto* other, double scale) {
        if (!other->loadIntoMemory() || !loadIntoMemory()) return false;
        if (!Multiply(other->GetHistogram(), scale)) return false;
        // still to  do the systematics

        return true;
    }
    void SampleHisto::Reset() {
        if (!loadIntoMemory()) return;
        GetHistogram()->Reset();
        // still to do the systematics
    }
    bool SampleHisto::Multiply(std::shared_ptr<TH1> Histo, double scale) { return Multiply(Histo.get(), scale); }
    bool SampleHisto::Divide(std::shared_ptr<SampleHisto> other, double scale) { return Divide(other.get(), scale); }
    bool SampleHisto::Divide(SampleHisto* other, double scale) {
        if (!other->loadIntoMemory() || !loadIntoMemory()) return false;
        if (!Divide(other->GetHistogram(), scale)) return false;
        // still to  do the systematics

        return true;
    }
    bool SampleHisto::Divide(std::shared_ptr<TH1> Histo, double scale) { return Divide(Histo.get(), scale); }
    bool SampleHisto::SampleHisto::Multiply(const TH1* Histo, double scale) {
        if (!Histo) return false;
        if (scale == 0) GetHistogram()->Reset();
        return GetHistogram()->Multiply(GetHistogram().get(), Histo, scale);
    }
    bool SampleHisto::Divide(const TH1* Histo, double scale) {
        if (!Histo || scale == 0.) return false;
        return GetHistogram()->Divide(GetHistogram().get(), Histo, scale, 1.);
    }
    std::shared_ptr<TH1> SampleHisto::ApplyTransformation(const std::function<std::pair<double, double>(unsigned int)>& trans_func) const {
        if (!isHistoLoaded()) return nullptr;
        std::shared_ptr<TH1> transformed = clone(GetHistogram(), true);
        for (unsigned int i = 0; i < GetNbins(); ++i) {
            std::pair<double, double> new_content = trans_func(i);
            transformed->SetBinContent(i, new_content.first);
            transformed->SetBinError(i, new_content.second);
        }
        return transformed;
    }
    std::shared_ptr<TH1> SampleHisto::Sqrt() const {
        return ApplyTransformation([this](unsigned int i) {
            if (GetBinContent(i) <= 0.) return std::pair<double, double>(0, 0);
            return std::pair<double, double>(std::sqrt(GetBinContent(i)), GetBinError(i) / (2. * std::sqrt(GetBinContent(i))));
        });
    }
    void SampleHisto::Draw(const std::string& options) {
        if (!loadIntoMemory()) return;
        GetHistogram()->Draw((!options.empty() ? options : GetDrawStyle()).c_str());
    }
    void SampleHisto::Write() {
        if (!loadIntoMemory()) return;
        GetHistogram()->Write(GetVariableName().c_str());
        if (!SystematicPairer::getInstance()->do_systematics()) m_up_syst_histo->Write(Form("%s_SigmaUp", GetVariableName().c_str()));
        m_down_syst_histo->Write(Form("%s_SigmaDown", GetVariableName().c_str()));
    }
    void SampleHisto::SetTitle(const std::string& title) {
        if (title.empty()) return;
        m_label = title;
        if (isHistoLoaded()) GetHistogram()->SetTitle(GetTitle().c_str());
    }

    void SampleHisto::exclude_systematics(const std::vector<std::string>& to_exclude) {
        if (!SystematicPairer::getInstance()->do_systematics()) return;
        CopyVector(to_exclude, m_excluded_syst, true);
        if (!m_excluded_syst.empty()) reset();
    }
    void SampleHisto::set_systematics(const std::vector<std::string>& syst_to_use) {
        if (!SystematicPairer::getInstance()->do_systematics()) return;
        CopyVector(syst_to_use, m_syst, true);
        if (m_syst_set_external || !m_syst.empty()) reset();
        m_syst_set_external = !m_syst.empty();
    }
    void SampleHisto::set_nominal(const std::string& nominal) {
        m_nominal_set_external = true;
        m_nominal = nominal;
        reset();
    }
    void SampleHisto::reset() {
        m_variations.clear();
        m_tried_to_load = false;
        m_nominal_histo = std::shared_ptr<TH1>();
    }
    void SampleHisto::setTheoryUncertainty(double unc) {
        if (!SystematicPairer::getInstance()->do_systematics() || isData()) return;
        bool reset_histo = m_theo_uncert > 0;
        m_theo_uncert = unc;
        if (reset_histo) reset();
    }
    std::vector<std::shared_ptr<TFile>> SampleHisto::get_in_files() const { return m_root_files; }
    // To make it really correct we need to evaluate the extrema of each variation which is
    // very cpu extensive. Assuming that the variations are not too big, this approximation should
    // give reliable results
    double SampleHisto::GetMaximum(double max) const {
        if (!isHistoLoaded()) return std::nan("1");
        return XAMPP::GetMaximum(GetHistogram(), max) + (m_up_syst_histo ? XAMPP::GetMaximum(m_up_syst_histo, max) : 0);
    }
    double SampleHisto::GetMinimum(double min) const {
        if (!isHistoLoaded()) return std::nan("1");
        double min_value = XAMPP::GetMinimum(GetHistogram(), min);
        double delta_min = (m_down_syst_histo ? XAMPP::GetMinimum(m_down_syst_histo, min) : 0);
        return min_value - delta_min >= min ? min_value - delta_min : min;
    }
    int SampleHisto::GetMaximumBin(double max) const { return XAMPP::GetMaximumBin(GetHistogram(), max); }
    int SampleHisto::GetMinimumBin(double min) const { return XAMPP::GetMinimumBin(GetHistogram(), min); }

    unsigned int SampleHisto::GetNbinsX() const { return GetDimension() > 0 ? GetHistogram()->GetNbinsX() + 2 : 0; }
    unsigned int SampleHisto::GetNbinsY() const { return GetDimension() > 1 ? GetHistogram()->GetNbinsY() + 2 : 0; }
    unsigned int SampleHisto::GetNbinsZ() const { return GetDimension() > 2 ? GetHistogram()->GetNbinsZ() + 2 : 0; }
    bool SampleHisto::hasAlphaNumericLabels() const { return isAlphaNumeric(GetHistogram()); }
    std::pair<double, double> SampleHisto::IntegrateWithError(unsigned int xStart, int xEnd, unsigned int yStart, int yEnd,
                                                              unsigned int zStart, int zEnd) const {
        return IntegrateAndError(GetHistogram(), xStart, xEnd, yStart, yEnd, zStart, zEnd);
    }
    std::shared_ptr<TH1> SampleHisto::ReBin(double max_stat_error) const { return ReBinHisto(GetHistogram(), max_stat_error); }
    std::shared_ptr<TH1> SampleHisto::ReBin(const std::vector<double>& x_bins, const std::vector<double>& y_bins,
                                            const std::vector<double>& z_bins) const {
        std::shared_ptr<TH1> rebinned = MakeTH1(x_bins, y_bins, z_bins);
        if ((int)GetDimension() != rebinned->GetDimension()) {
            throw std::runtime_error(
                Form("Dimensions of rebinned histogram %i and me do not match %u", rebinned->GetDimension(), GetDimension()));
        }
        XAMPP::TransformToBinning(GetHistogram().get(), rebinned.get());
        return rebinned;
    }

    std::shared_ptr<TH1> SampleHisto::TransformToBinning(const std::shared_ptr<TH1>& h) const { return TransformToBinning(h.get()); }
    std::shared_ptr<TH1> SampleHisto::TransformToBinning(const TH1* h) const {
        std::shared_ptr<TH1> transformed = clone(h);
        if (!XAMPP::TransformToBinning(GetHistogram().get(), transformed.get())) transformed.reset();
        return transformed;
    }
    std::vector<double> SampleHisto::ExtractBinningFromAxis(const TAxis* T) { return ExtractBinning(T); }
    bool SampleHisto::Truncate(double threshold, double to) {
        if (!loadIntoMemory()) return false;
        bool truncated = false;
        for (unsigned int bin = 0; bin <= GetNbins(); ++bin) {
            if (isOverFlow(bin) || GetBinContent(bin) >= threshold) continue;
            truncated = true;
            double old_content = GetBinContent(bin);
            GetHistogram()->SetBinContent(bin, to);
            GetHistogram()->SetBinError(bin, std::fabs(GetBinError(bin) - old_content));
        }
        return truncated;
    }
    //##########################################################################
    //              SystematicPairHistos
    //##########################################################################
    SystematicPairHistos::SystematicPairHistos(SampleHisto* ref_histo, const std::string& sys_name, std::shared_ptr<TH1> dn,
                                               std::shared_ptr<TH1> up) :
        m_name(sys_name),
        m_dn(dn),
        m_up(up),
        m_nominal(ref_histo->GetHistogram()),
        m_nom_up(ref_histo->ApplyTransformation([&ref_histo, &up](unsigned int i) {
            return std::pair<double, double>(ref_histo->GetBinContent(i) + up->GetBinContent(i), 0);
        })),
        m_nom_dn(ref_histo->ApplyTransformation([&ref_histo, &dn](unsigned int i) {
            return std::pair<double, double>(ref_histo->GetBinContent(i) - dn->GetBinContent(i), 0);
        })),
        m_symmetric(false),
        m_envelope(false),
        m_tot_up_dev(0),
        m_tot_dn_dev(0),
        m_dev_cached(false) {
        if (m_name.rfind("_1") == m_name.size() - 1) {
            m_name = m_name.substr(0, m_name.rfind("_1"));
            while (m_name.rfind("_") == m_name.size() - 1) { m_name = m_name.substr(0, m_name.rfind("_")); }
        }
        if (m_dn) m_dn->SetTitle(Form("%s-1#sigma", SystematicPairer::getInstance()->get_syst_title(name()).c_str()));
        if (m_up) m_up->SetTitle(Form("%s+1#sigma", SystematicPairer::getInstance()->get_syst_title(name()).c_str()));

        if (m_nom_dn) m_nom_dn->SetTitle(Form("%s-1#sigma", SystematicPairer::getInstance()->get_syst_title(name()).c_str()));
        if (m_nom_up) m_nom_up->SetTitle(Form("%s+1#sigma", SystematicPairer::getInstance()->get_syst_title(name()).c_str()));
    }
    void SystematicPairHistos::Scale(double s) {
        std::set<std::shared_ptr<TH1>> sys{m_dn, m_up, m_nom_dn, m_nom_up};
        for (auto& h : sys) {
            if (h) h->Scale(s);
        }
    }
    SystematicPairHistos::SystematicPairHistos(SampleHisto* ref_histo, std::shared_ptr<SystematicEnvelope> grp, std::shared_ptr<TH1> env) :
        SystematicPairHistos(ref_histo, grp->name(), env, env) {
        m_envelope = true;
        m_symmetric = grp->add_to_up() && grp->add_to_down();
        if (!grp->add_to_up()) {
            m_nom_up.reset();
            m_up.reset();
        }
        if (!grp->add_to_down()) {
            m_nom_dn.reset();
            m_dn.reset();
        }
        if (is_symmetric()) {
            for (auto& H : {m_dn, m_up, m_nom_dn, m_nom_up}) {
                if (H) H->SetTitle(SystematicPairer::getInstance()->get_syst_title(name()).c_str());
            }
        }
    }
    bool SystematicPairHistos::has_valid_histo() const {
        return m_nom_up.get() != nullptr || m_nom_dn.get() != nullptr || m_dn.get() != nullptr || m_up.get() != nullptr;
    }
    SystematicPairHistos::SystematicPairHistos(SampleHisto* ref_histo, const std::string& sys_name, std::shared_ptr<TH1> sym) :
        SystematicPairHistos(ref_histo, sys_name, sym, sym) {
        m_symmetric = true;
        for (auto& H : {m_dn, m_up, m_nom_dn, m_nom_up}) {
            if (H) H->SetTitle(SystematicPairer::getInstance()->get_syst_title(name()).c_str());
        }
    }

    bool SystematicPairHistos::is_symmetric() const { return m_symmetric; }
    bool SystematicPairHistos::is_envelope() const { return m_envelope; }

    std::shared_ptr<TH1> SystematicPairHistos::up_histo() const { return m_up; }
    std::shared_ptr<TH1> SystematicPairHistos::dn_histo() const { return m_dn; }
    std::shared_ptr<TH1> SystematicPairHistos::final_up_histo() const { return m_nom_up; }
    std::shared_ptr<TH1> SystematicPairHistos::final_dn_histo() const { return m_nom_dn; }

    std::string SystematicPairHistos::name() const { return m_name; }
    double SystematicPairHistos::total_up_dev() {
        cache_total_deviation();
        return m_tot_up_dev;
    }
    double SystematicPairHistos::total_dn_dev() {
        cache_total_deviation();
        return m_tot_dn_dev;
    }
    void SystematicPairHistos::cache_total_deviation() {
        if (m_dev_cached) return;
        m_dev_cached = true;
        for (unsigned int i = 0; i < GetNbins(m_nominal); ++i) {
            if (m_nominal->GetBinContent(i) != 0.) {
                m_tot_up_dev += std::fabs(up_histo()->GetBinContent(i) / m_nominal->GetBinContent(i));
                m_tot_dn_dev += std::fabs(dn_histo()->GetBinContent(i) / m_nominal->GetBinContent(i));
            } else {
                m_tot_up_dev += std::fabs(up_histo()->GetBinContent(i));
                m_tot_dn_dev += std::fabs(dn_histo()->GetBinContent(i));
            }
        }
        m_tot_up_dev /= GetNbins(m_nominal);
        m_tot_dn_dev /= GetNbins(m_nominal);
    }
}  // namespace XAMPP
