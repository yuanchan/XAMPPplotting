#include <XAMPPplotting/Histo.h>
#include <XAMPPplotting/HistoTemplates.h>
#include <XAMPPplotting/MetaDataTreeReader.h>
#include <XAMPPplotting/Selector.h>
#include <XAMPPplotting/TreeVarReader.h>
#include <XAMPPplotting/Weight.h>

#include <TSystem.h>

namespace XAMPP {
    Selector::Selector(const std::string &Name, const std::string &TreeName, const std::vector<std::string> &Files) :
        Analysis::Analysis(),
        m_RegionDefinitions() {
        ProcessSystematics(false);
        ProcessWeightVariations(false);
        SetName(Name);
        SetTreeName(TreeName);
        SetInputFiles(Files);
        EventService::getService()->set_selector(this);
    }
    bool Selector::userPartialList(const std::vector<std::string> &files, unsigned int begin, unsigned int end) {
        SetInputFiles(files);
        if (!ReadMetaDataTree()) return false;
        std::vector<std::string> partial_files;
        for (unsigned int i = begin; i < end && i < files.size(); ++i) { partial_files.push_back(files.at(i)); }
        SetInputFiles(partial_files);
        return !partial_files.empty();
    }
    bool Selector::Process(long int nMax, long int nSkip) {
        if (!ReadMetaDataTree()) return false;
        SetupReaders();
        XAMPP::PrintFooter();
        if (!InitHistos()) return false;
        XAMPP::PrintFooter();
        DoCutFlow(false);
        return Analysis::Process(nMax, nSkip);
    }
    bool Selector::AnalyzeEvent() { return true; }
    bool Selector::InitHistos() { return true; }

    void Selector::SetupReaders() {}
    bool Selector::ReadHistoConfig(const std::string &Config) { return XAMPP::HistoTemplates::getHistoTemplater()->InitTemplates(Config); }
    bool Selector::begin(TTree *) { return true; }

    bool Selector::SetupHistograms(const std::string &Syst) {
        for (auto &Region : m_RegionDefinitions) {
            TDirectory *D = CreateDirectory(Syst, Region.Name);
            for (const auto &Histo : Region.Histograms) {
                if (!Histo->init(D)) return false;
            }
        }
        return true;
    }
    bool Selector::isRegionDefined(const std::string &region) const {
        for (const auto &Defined : m_RegionDefinitions) {
            if (region == Defined.Name) return true;
        }
        return false;
    }
    bool Selector::CreateNewAnalysisRegion(const std::string &Region) {
        if (Region.empty()) {
            Error("Selector::CreateNewAnalysisRegion()", "Empty string has been given");
            return false;
        }
        if (isRegionDefined(Region)) {
            Error("Selector::CreateNewAnalysisRegion()", "The region " + Region + " has already been defined.");
            return false;
        }
        Info("Selector::CreateNewRegion()", "Create new analysis region " + Region + ".");
        m_RegionDefinitions.push_back(Selector::EventRegion(Region));
        return true;
    }

    bool Selector::InsertHistogramInRegion(const std::string &Region, Histo *H) {
        return InsertHistogramInRegion(Region, std::shared_ptr<Histo>(H));
    }
    bool Selector::InsertHistogramInRegion(const std::string &Region, std::shared_ptr<Histo> H) {
        if (!H) {
            Error("Selector::InsertHistogramInRegion()", "No histogram was given.");
            return false;
        }
        for (auto &ToInsert : m_RegionDefinitions) {
            if (ToInsert.Name == Region) {
                Info("Selector::InsertHistogramInRegion()", "Append " + H->name() + " to region " + Region);
                ToInsert.Histograms.push_back(H);
                return AppendHisto(H);
            }
        }
        Error("Selector::InsertHistogramsInRegion()",
              "Could not append histogram " + H->name() + " to region " + Region + " as it is not defined");
        return false;
    }
    Selector::~Selector() { EventService::getService()->set_selector(nullptr); }
    bool Selector::isData() const { return m_weight->isData(); }
}  // namespace XAMPP
