#include <XAMPPplotting/PlottingUtils.h>
#include <XAMPPplotting/TreeVarWriter.h>
#include <cmath>
#include <sstream>

namespace XAMPP {
    //#########################################################################################
    //                                  ITreeVarWriterStorage
    //#########################################################################################
    ITreeVarWriterStorage* ITreeVarWriterStorage::m_Inst = nullptr;
    ITreeVarWriterStorage* ITreeVarWriterStorage::GetInstance() {
        if (!m_Inst) m_Inst = new ITreeVarWriterStorage();
        return m_Inst;
    }
    ITreeVarWriterStorage::ITreeVarWriterStorage() : m_DB() {}
    ITreeVarWriter* ITreeVarWriterStorage::GetWriter(const std::string& Name) {
        if (m_DB.find(Name) != m_DB.end()) return m_DB.at(Name).get();
        return nullptr;
    }
    bool ITreeVarWriterStorage::Register(ITreeVarWriter* R) {
        if (!R || R->name().empty() || m_DB.find(R->name()) != m_DB.end()) {
            Error("ITreeVarWriterStorage::Register()", "Could not register Writer " + (R ? R->name() : ". No Writer given."));
            return false;
        }
        m_DB.insert(std::pair<std::string, std::shared_ptr<ITreeVarWriter>>(R->name(), std::shared_ptr<ITreeVarWriter>(R)));
        Info("ITreeVarWriterStorage::Register()", "Register new Writer " + R->name());
        return true;
    }
    ITreeVarWriterStorage::~ITreeVarWriterStorage() { m_Inst = nullptr; }
    //#########################################################################################
    //                                  ITreeVarWriter
    //#########################################################################################
    std::string ITreeVarWriter::RemoveProblematicStr(const std::string& str) const {
        std::string out = str;
        out = EraseWhiteSpaces(out);
        out = ReplaceExpInString(out, " ", "");
        out = ReplaceExpInString(out, "-", "__");
        return out;
    }
    bool ITreeVarWriter::init(TTree* t) {
        if (!m_Registered) {
            Error("ITreeVarWriter::init()", "The instance " + name() + " is not uniquely defined");
            return false;
        }
        if (!t) Error("ITreeVarWriter::init()", "No tree has been given ");
        m_tree = t;
        if (!m_Reader) Error("ITreeVarWriter::init()", "No ITreeVarReader was given to " + name());
        return (m_tree != nullptr && m_Reader != nullptr);
    }
    ITreeVarWriter::ITreeVarWriter(const std::string& Name, ITreeVarReader* Reader) :
        m_Name(Name),
        m_Registered(false),
        m_tree(nullptr),
        m_Reader(Reader) {
        m_Registered = ITreeVarWriterStorage::GetInstance()->Register(this);
    }
    std::string ITreeVarWriter::name() const { return m_Name; }
}  // namespace XAMPP
