#include <XAMPPplotting/Analysis.h>
#include <XAMPPplotting/AnalysisSetup.h>
#include <XAMPPplotting/BTaggingScaleFactor.h>
#include <XAMPPplotting/CombinatoricSelection.h>
#include <XAMPPplotting/Cuts.h>
#include <XAMPPplotting/DMXSecUpdater.h>
#include <XAMPPplotting/DataDrivenWeights.h>
#include <XAMPPplotting/EventDuplication.h>
#include <XAMPPplotting/HistFitterMetaReader.h>
#include <XAMPPplotting/HistFitterWeight.h>
#include <XAMPPplotting/Histo.h>
#include <XAMPPplotting/HistoTemplates.h>
#include <XAMPPplotting/IfDefHelpers.h>
#include <XAMPPplotting/LumiCalculator.h>
#include <XAMPPplotting/MetaDataTreeReader.h>
#include <XAMPPplotting/NTupleWriter.h>
#include <XAMPPplotting/ParticleTagger.h>
#include <XAMPPplotting/PlottingUtils.h>
#include <XAMPPplotting/ReaderProvider.h>
#include <XAMPPplotting/ReconstructedParticleReaders.h>
#include <XAMPPplotting/TreeVarReader.h>
#include <XAMPPplotting/UtilityReader.h>
#include <XAMPPplotting/Weight.h>

#include <TFile.h>
#include <TSystem.h>
#include <dirent.h>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <sstream>
#include <string>

namespace XAMPP {
    AnalysisRegion::AnalysisRegion(const std::string& Name) :
        m_RegionName(Name),
        m_Init(false),
        m_Cuts(),
        m_SummarizedCut(nullptr),
        m_region_reader(nullptr) {}
    AnalysisRegion::~AnalysisRegion() {}
    std::string AnalysisRegion::name() const { return m_RegionName; }
    bool AnalysisRegion::AppendCut(std::vector<std::shared_ptr<Condition>> Cuts) {
        for (auto& C : Cuts)
            if (!AppendCut(C)) return false;
        return true;
    }
    bool AnalysisRegion::PrependCut(std::vector<std::shared_ptr<Condition>> Cuts) {
        for (std::vector<std::shared_ptr<Condition>>::reverse_iterator C = Cuts.rbegin(); C != Cuts.rend(); ++C)
            if (!PrependCut(*C)) return false;
        return true;
    }
    bool AnalysisRegion::PrependCut(std::shared_ptr<Condition> C) {
        if (m_Init) {
            Error("AnalysisRegion::PrependCut()", "The region " + name() + " is already finalized.");
            return false;
        }
        if (C == nullptr) {
            Error("AnalysisRegion::PrependCut()", "Could not add the cut to region " + name() + ". nullptr ptr given");
            return false;
        }
        for (auto& c : m_Cuts) {
            if (c == C) {
                Warning("AnalysisRegion::PrependCut()", "The cut " + C->name() + " is already part of the analysis region " + name());
                return false;
            }
        }
        m_Cuts.insert(m_Cuts.begin(), C);
        return true;
    }
    bool AnalysisRegion::AppendCut(std::shared_ptr<Condition> C) {
        if (m_Init) {
            Error("AnalysisRegion::AppendCut()", "The region " + name() + " is already finalized.");
            return false;
        }
        if (C == nullptr) {
            Error("AnalysisRegion::AppendCut()", "Could not add the cut to region " + name() + ". nullptr ptr given");
            return false;
        }
        for (auto& c : m_Cuts) {
            if (c == C) {
                Warning("AnalysisRegion::AppendCut()", "The cut " + C->name() + " is already part of the analysis region. ");
                return true;
            }
        }
        m_Cuts.push_back(C);
        return true;
    }
    void AnalysisRegion::Finalize() {
        if (m_Init || m_Cuts.empty()) return;
        m_Init = true;
        Info("AnalysisRegion::Finalize()", "Finalize analysis region " + name());
        for (auto& C : m_Cuts) {
            if (!m_SummarizedCut)
                m_SummarizedCut = C;
            else
                m_SummarizedCut = Condition::Combine(m_SummarizedCut, C, Condition::Mode::AND);
        }
        m_region_reader = new CutAppliedReader(m_Cuts, std::string("IsRegion_") + name());
    }

    const std::vector<std::shared_ptr<Condition>>& AnalysisRegion::GetCuts() const { return m_Cuts; }
    ITreeVarReader* AnalysisRegion::RegionReader() const { return m_region_reader; }
    std::shared_ptr<Condition> AnalysisRegion::RetrieveCompressedCut() {
        if (!m_SummarizedCut) Warning("AnalysisRegion::RetrieveCompressedCut()", "The cut has not been defined yet for region " + name());
        return m_SummarizedCut;
    }
    std::shared_ptr<Histo> AnalysisRegion::CreateHistoArray(const std::vector<std::shared_ptr<Histo>>& HistosToUse, bool doCutFlow) {
        std::shared_ptr<HistoArray> Array = std::make_shared<HistoArray>(name());
        Array->DoCutFlow(doCutFlow);
        Array->SetCut(GetCuts());
        Array->AppendHistogram(HistosToUse);
        return Array;
    }

    //##############################################################################################
    //                                           AnalysisSetup
    //##############################################################################################
    AnalysisSetup::AnalysisSetup() :
        m_InputFiles(),
        m_MetaInputFiles(),
        m_PRWlumiFiles(),
        m_PRWconfigFiles(),
        m_Systematics(),
        m_Weights(),
        m_DataWeights(),
        m_WeightSystematics(),
        m_Readers(),
        m_Tree(""),
        m_SampleName(""),
        m_AnaName(""),
        m_outFile(""),
        m_NormSUSYprocs(true),
        m_ApplyXS(true),
        m_doSyst(true),
        m_doSystW(true),
        m_doNominal(true),
        m_doCutFlow(false),
        m_ignoreMetaInputs(false),
        m_ImportedConfigs(),
        m_RegionCuts(),
        m_ActRegion(nullptr),
        m_Analysis(),
        m_Branches(),
        m_ReplaceBranchNames(),
        m_ReplaceSystNames(),
        m_ReplaceStringOfSyst(),
        m_SystPrefix(""),
        m_LoadWeightsOnly(false),
        m_xSecDirSig(),
        m_xSecDirBkg("dev/PMGTools/PMGxsecDB_mc16.txt"),
        m_DMXSec(std::make_unique<DMXSecUpdater>()),
        m_Debug(false),
        m_NominalName(),
        m_SetNominalName(false),
        m_IsHistFitInput(false),
        m_GRLs(),
        m_uniqueRun() {
        CreateNewRegion("");
    }
    void AnalysisSetup::LoadWeightsOnly() { m_LoadWeightsOnly = true; }
    bool AnalysisSetup::loadWeightsOnly() const { return m_LoadWeightsOnly; }
    bool AnalysisSetup::do_kinematic_syst() const { return m_doSyst; }
    bool AnalysisSetup::do_weight_syst() const { return m_doSystW; }
    std::vector<std::string> AnalysisSetup::inputFiles() const { return m_InputFiles; }
    bool AnalysisSetup::InitMetaData() {
        if (!m_LoadWeightsOnly &&
            !XAMPP::NormalizationDataBase::getDataBase()->init(m_MetaInputFiles.empty() ? m_InputFiles : m_MetaInputFiles)) {
            return false;
        }
        if (XAMPP::Weight::getWeighter()->isData() && !m_GRLs.empty()) { m_ActRegion->AppendCut(std::make_shared<GRLCondition>(m_GRLs)); }
        m_GRLs.clear();
        SetCrossSectionDir();
        ParticleTaggerManager::Instance()->SetSampleName(m_SampleName);
        return true;
    }
    bool AnalysisSetup::ParseConfig(std::string HistoConfig, std::string RunConfig, std::string InputConfig) {
        if (!ImportFile(InputConfig, &AnalysisSetup::ReadInputFiles) || !InitMetaData()) return false;
        // Weights only is an option to read out all weight systematics form the tree via python

        if (!ImportFile(RunConfig, &AnalysisSetup::ReadAnaConfig)) {
            Error("AnalysisSetup::ParseConfig()", "Failed to read the run config " + RunConfig);
            return false;
        }
        if (!m_LoadWeightsOnly &&
            (!LumiCalculator::getCalculator()->GetPRWconfig().empty() || !LumiCalculator::getCalculator()->GetPRWlumi().empty())) {
            Info("AnalysisSetup::ParseConfig()", "Setup the prwTool");
            if (!LumiCalculator::getCalculator()->InitPileUpTool()) return false;
        }
        m_DMXSec->UpdateDMXSection();
        if (!m_LoadWeightsOnly && !ImportFile(HistoConfig, &AnalysisSetup::ReadHistoConfig)) return false;
        return true;
    }
    int AnalysisSetup::CheckInputConfigProperties(std::ifstream&, const std::string& line) {
        std::stringstream sstr(line);
        std::string word = GetWordFromStream(sstr);
        if (word == "Input")
            ExtractListFromStream(sstr, m_InputFiles);
        else if (!m_ignoreMetaInputs && word == "MetaInput")
            ExtractListFromStream(sstr, m_MetaInputFiles);
        else if (word == "Import") {
            if (!ImportFile(GetWordFromStream(sstr), &AnalysisSetup::ReadInputFiles)) return PropertyStatus::Failure;
        } else if (word == "PRWlumi")
            ExtractListFromStream(sstr, m_PRWlumiFiles);
        else if (word == "PRWconfig")
            ExtractListFromStream(sstr, m_PRWconfigFiles);
        else if (word == "GRL")
            ExtractListFromStream(sstr, m_GRLs);
        else if (word == "SampleName")
            m_SampleName = GetWordFromStream(sstr);
        else if (word == "SystSuffix")
            m_SystPrefix = GetWordFromStream(sstr);
        else if (word == "RenameSyst" && !RenameSystematic(sstr))
            return PropertyStatus::Failure;
        else if (word == "noDuplicateData") {
            if (!m_uniqueRun)
                m_uniqueRun = std::make_shared<UniqueEventCondition>(GetWordFromStream(sstr));
            else
                m_uniqueRun->load_duplicated_list(GetWordFromStream(sstr));
        } else if (word == "NoSUSYpidWeight") {
            m_NormSUSYprocs = false;
            Info("AnalysisSetup::CheckInputConfigProperties()", "NoSUSYpidWeight");
        } else if (word == "DMXSecUpdater")
            m_DMXSec->SetCoupling(GetFloatFromStream(sstr));
        else if (word == "xSecDir")
            m_xSecDirSig = GetWordFromStream(sstr);
        else if (word == "xSecDirBkg")
            m_xSecDirBkg = GetWordFromStream(sstr);
        else if (word == "disableXS")
            m_ApplyXS = false;
        else if (word == "IsHistFitter") {
            HistFitterNormDataBase::getDataBase();
            m_IsHistFitInput = true;
        } else if (word == "SetNominalName") {
            m_NominalName = GetWordFromStream(sstr);
            m_SetNominalName = true;
        } else if (word == "Tree") {
            m_Tree = GetWordFromStream(sstr);
        } else
            return PropertyStatus::NothingFound;

        if (m_SetNominalName && m_IsHistFitInput) {
            dynamic_cast<HistFitterNormDataBase*>(HistFitterNormDataBase::getDataBase())->SetNominalName(m_NominalName);
        }

        return PropertyStatus::New;
    }
    bool AnalysisSetup::ReadInputFiles(std::ifstream& inf) {
        std::string line;
        IfDefLineParser ReadLine;
        while (ReadLine(inf, line) == IfDefLineParser::LineStatus::NewProperty) {
            int Property = CheckInputConfigProperties(inf, line);
            if (Property == PropertyStatus::Failure)
                return false;
            else if (Property != PropertyStatus::New && Property != PropertyStatus::NothingFound) {
                Error("ReadAnaConfig()", "Unknown property code " + std::to_string(Property));
                return false;
            }
        }
        return true;
    }
    int AnalysisSetup::CheckAnaConfigProperties(std::ifstream& inf, const std::string& line) {
        XAMPP::Weight* Weight = XAMPP::Weight::getWeighter();
        std::stringstream sstr(line);
        std::string word = GetWordFromStream(sstr);
        if ((word == "Import" || word == "ImportAlways")) {
            if (!ImportFile(GetWordFromStream(sstr), &AnalysisSetup::ReadAnaConfig, word.find("Always") != std::string::npos))
                return PropertyStatus::Failure;
        } else if (word == "noPRW")
            Weight->ApplyPRW(false);
        else if (word == "Weights")
            ExtractListFromStream(sstr, m_Weights, true);
        else if (word == "DataWeights")
            ExtractListFromStream(sstr, m_DataWeights, true);
        else if (word == "FloatWeights") {
            ExtractListFromStream(sstr, m_Weights, true);
            for (auto& W : m_Weights) FloatWeightFromTree::GetWeighter(W);
        } else if (word == "LHENormalization") {
            if (IsKeyWordSatisfied(sstr, "AcceptanceOnly")) {
                Weight->setLHEnormMode(Weight::AcceptanceOnly);
            } else if (IsKeyWordSatisfied(sstr, "CrossSectionAndAcceptance")) {
                Weight->setLHEnormMode(Weight::CrossSectionAndAcceptance);
            } else {
                Error("AnalysisSetup::ReadAnaConfig()",
                      "Please choose the LHE normalization between 'AcceptanceOnly' or 'CrossSectionAndAcceptance'");
                return false;
            }
        } else if (word.find("FakeFactorWeight") != std::string::npos) {
            FakeFactorWeight* Weight = FakeFactorWeight::GetWeighter(GetWordFromStream(sstr));
            if (!Weight) {
                Error("AnalysisSetup::ReadAnaConfig()", "Could not create a FakeFactorWeight.");
                return PropertyStatus::Failure;
            }
            if (!Weight->ReadFakeFactorConfig(inf, m_Readers)) return PropertyStatus::Failure;
        } else if (word.find("New_CondWeight") != std::string::npos) {
            ConditionalWeight* Cond = ConditionalWeight::GetWeighter(GetWordFromStream(sstr));
            if (!Cond) {
                Error("AnalysisSetup::ReadAnaConfig()", "Could not create a ConditionalWeight instance.");
                return PropertyStatus::Failure;
            } else if (!Cond->LoadConfiguration(inf, m_Readers))
                return PropertyStatus::Failure;
        }
#if ATHENA_RELEASE_SERIES == 212
        // Reconfigure the Btagging scale-factors
        else if (word == "New_BTaggingWeight") {
            BTaggingSF* BTagSF = BTaggingSF::GetWeighter(GetWordFromStream(sstr));
            if (!BTagSF || !BTagSF->configureWeight(inf)) {
                Error("AnalysisSetup::ReadAnaConfig()", "Could not create a BTaggingSF.");
                return PropertyStatus::Failure;
            }
        }
#endif
        else if (word == "noDuplicateData" && !m_uniqueRun) {
            if (m_RegionCuts.size() > 1) {
                Error("AnalysisSetup::ReadAnaConfig()",
                      "Please make sure to add the 'noDuplicateData' requirement *before* you define a region");
                return PropertyStatus::Failure;
            }
            m_uniqueRun = std::make_shared<UniqueEventCondition>(GetWordFromStream(sstr));
        } else if (word == "Lumi") {
            XAMPP::NormalizationDataBase::getDataBase()->SetLumi(GetFloatFromStream(sstr) * 1000.);
        } else if (word == "noSyst") {
            Info("AnalysisSetup::ReadAnaConfig()", "Switch off all systematics");
            m_doSystW = false;
            m_doSyst = false;
        } else if (word == "noSystW") {
            Info("AnalysisSetup::ReadAnaConfig()", "Switch off weight systematics");
            m_doSystW = false;
        } else if (word == "noSystK") {
            Info("AnalysisSetup::ReadAnaConfig()", "Switch off kinematic systematics");
            m_doSyst = false;
        } else if (word == "doCutFlow") {
            m_doCutFlow = true;
        } else if (word == "Systematic") {
            if (!m_doSyst) {
                Warning("AnalysisSetup::ReadAnaConfig()", "Systematics are switched off and you want to define ones?!?!?!?!");
                m_doSyst = true;
            }
            while (sstr >> word) {
                if (IsElementInList(m_Systematics, word)) continue;
                Info("AnalysisSetup::ReadAnaConfig()", "Run over kinematic systematic " + word);
                m_Systematics.push_back(word);
            }

        } else if (word == "SystematicW")
            ExtractListFromStream(sstr, m_WeightSystematics);
        else if (word == "ExcludeSystematicW") {
            std::vector<std::string> ToExclude;
            ExtractListFromStream(sstr, ToExclude);
            Weight->excludeSystematic(ToExclude);
        } else if (word == "noNominal")
            m_doNominal = false;
        else if (word == "xSecDir")
            m_xSecDirSig = GetWordFromStream(sstr);
        else if (word == "xSecDirBkg")
            m_xSecDirBkg = GetWordFromStream(sstr);
        else if (word == "disableXS")
            Weight->ApplyXSection(false);
        else if (word == "Tree")
            m_Tree = GetWordFromStream(sstr);
        else if (word == "Analysis")
            m_AnaName = GetWordFromStream(sstr);
        else if (word == "NoSUSYpidWeight")
            m_NormSUSYprocs = false;
        // Here are the definitions of new Particle Readers
        else if (word.find("Particle") != std::string::npos) {
            XAMPP::IParticleReader* Particle = word.find("New_") == 0 ? ReaderProvider::GetInstance()->CreateParticle(inf, line)
                                                                      : ReaderProvider::GetInstance()->CreateParticle(line);
            if (!Particle)
                return PropertyStatus::Failure;
            else
                m_Readers.push_back(Particle);
        } else if (word.find("Reader") != std::string::npos) {
            ITreeVarReader* R = word.find("New_") != 0 ? ReaderProvider::GetInstance()->CreateReader(line)
                                                       : ReaderProvider::GetInstance()->CreateReader(inf, line);
            if (!R) {
                Error("AnalysisSeutp::ReadAnaConfig()", "Failed to setup reader from line " + line);
                return PropertyStatus::Failure;
            }
            m_Readers.push_back(R);
        } else if (word == "ParTagger" && !ParticleTaggerManager::Instance()->AddCommonTagger(inf, m_Readers))
            return PropertyStatus::Failure;
        // You can rename systematics which are different in AFII and FullSim
        else if (word == "RenameSyst") {
            if (!RenameSystematic(sstr)) return PropertyStatus::Failure;
            // Cuts are created here
        } else if (word.find("Cut") + 3 == word.size()) {
            std::shared_ptr<Condition> C = nullptr;
            if (word == "CombCut") {
                C = CreateCombinedCut(inf, m_Readers, line);
            } else
                C = CreateCut(line, m_Readers);
            if (!C)
                return PropertyStatus::Failure;
            else if (!m_ActRegion) {
                Error("AnalysisSetup()", "Could not find any region to associate " + C->name());
                return PropertyStatus::Failure;
            }
            if (!m_ActRegion->AppendCut(C)) return PropertyStatus::Failure;
            m_Cuts.push_back(C);
        } else if (word == "Region") {
            if (!CreateNewRegion(GetWordFromStream(sstr))) return PropertyStatus::Failure;
        } else if (word == "FinalizeRegion") {
            if (!FinalizeActRegion()) return PropertyStatus::Failure;
        } else if (word == "DebugMode") {
            m_Debug = true;
            m_Readers.push_back(ReaderProvider::GetInstance()->CreateReader("EvReader llu eventNumber"));
            m_Readers.push_back(ReaderProvider::GetInstance()->CreateReader("PeriodReader"));
        } else if (word == "SetNominalName") {
            m_NominalName = GetWordFromStream(sstr);
            m_SetNominalName = true;
        }
        // Provide a functionality to dismount Will's super Monte Carlo
        else if (word == "normalizeToPeriod")
            setPRWperiod(sstr);
        else
            return PropertyStatus::NothingFound;
        return PropertyStatus::New;
    }
    bool AnalysisSetup::ReadAnaConfig(std::ifstream& inf) {
        XAMPP::Weight* Weight = XAMPP::Weight::getWeighter();
        std::string line;
        IfDefLineParser ReadLine;
        IfDefLineParser::LineStatus LineStatus = ReadLine(inf, line);
        while (LineStatus == IfDefLineParser::LineStatus::NewProperty) {
            int Property = CheckAnaConfigProperties(inf, line);
            if (Property == PropertyStatus::Failure)
                return false;
            else if (Property != PropertyStatus::New && Property != PropertyStatus::NothingFound) {
                Error("ReadAnaConfig()", "Unknown property code " + std::to_string(Property));
                return false;
            }
            LineStatus = ReadLine(inf, line);
        }
        // Tell the weighting tool which weights to use
        if (!m_Weights.empty()) Weight->UseWeights(m_Weights);
        if (!m_DataWeights.empty()) Weight->UseDataWeights(m_DataWeights);
        if (!m_PRWlumiFiles.empty()) LumiCalculator::getCalculator()->SetPRWlumi(m_PRWlumiFiles);
        if (!m_PRWconfigFiles.empty()) LumiCalculator::getCalculator()->SetPRWconfig(m_PRWconfigFiles);
        if (Weight->normToProdProcess() != m_NormSUSYprocs) Weight->NormProcesses(m_NormSUSYprocs);
        if (!m_ApplyXS) Weight->ApplyXSection(false);
        SetCrossSectionDir();
        return (LineStatus != IfDefLineParser::LineStatus::ReadError);
    }
    void AnalysisSetup::setPRWperiod(std::stringstream& sstr) {
        if (IsKeyWordSatisfied(sstr, "data15-16")) {
            Info("AnalysisSetup::setPRWperiod()", "Normalize the MC to mc16a only");
            PileUpWeightElement::getElement()->reWeightToPeriod(PileUpWeightElement::LumiPeriod::data15_16);
        } else if (IsKeyWordSatisfied(sstr, "data17")) {
            Info("AnalysisSetup::setPRWperiod()", "Normalize the MC to mc16d only");
            PileUpWeightElement::getElement()->reWeightToPeriod(PileUpWeightElement::LumiPeriod::data17);
        } else if (IsKeyWordSatisfied(sstr, "data18")) {
            Info("AnalysisSetup::setPRWperiod()", "Normalize the MC to mc16e only");
            PileUpWeightElement::getElement()->reWeightToPeriod(PileUpWeightElement::LumiPeriod::data18);
        } else if (IsKeyWordSatisfied(sstr, "data15_17")) {
            Info("AnalysisSetup::setPRWperiod()", "Normalize the MC to mc16a & mc16d only");
            PileUpWeightElement::getElement()->reWeightToPeriod(PileUpWeightElement::LumiPeriod::data15_17);
        } else if (IsKeyWordSatisfied(sstr, "data17_18")) {
            Info("AnalysisSetup::setPRWperiod()", "Normalize the MC to mc16d & mc16e only. That's interesting.");
            PileUpWeightElement::getElement()->reWeightToPeriod(PileUpWeightElement::LumiPeriod::data17_18);
        } else if (IsKeyWordSatisfied(sstr, "data15_18")) {
            Info("AnalysisSetup::setPRWperiod()", "Normalize the MC to mc16a & mc16e only. That's interesting. Skip one year");
            PileUpWeightElement::getElement()->reWeightToPeriod(PileUpWeightElement::LumiPeriod::data15_18);
        } else if (IsKeyWordSatisfied(sstr, "all")) {
            Info("AnalysisSetup::setPRWperiod()", "Back to the ROOTs do not touch the Monte Carlo at all.");
            PileUpWeightElement::getElement()->reWeightToPeriod(PileUpWeightElement::LumiPeriod::All);
        }
    }
    void AnalysisSetup::SetCrossSectionDir() {
        if (!m_ApplyXS || m_xSecDirSig.empty() || m_xSecDirBkg.empty() || XAMPP::Weight::getWeighter()->isData()) return;
        m_DMXSec->SetCrossSectionDir(m_xSecDirSig);
        m_DMXSec->UpdateDMXSection();
        XAMPP::NormalizationDataBase::getDataBase()->LoadCrossSections(m_xSecDirBkg, m_xSecDirSig);
        XAMPP::PrintFooter();
        XAMPP::NormalizationDataBase::getDataBase()->PromptMetaDataTree();
        XAMPP::PrintFooter();
        m_xSecDirSig.clear();
        m_xSecDirBkg.clear();
    }
    bool AnalysisSetup::RenameSystematic(std::stringstream& sstr) {
        std::string Orig(""), To("");
        sstr >> Orig >> To;
        if (m_ReplaceSystNames.find(Orig) != m_ReplaceSystNames.end()) {
            Error("AnalysisSetup::RenameSystematic()",
                  "You've already renamed systematic" + Orig + " to " + m_ReplaceSystNames.at(Orig) + ". Please choose.");
            return false;
        }
        Info("AnalysisSetup::RenameSystematic()", "Rename systematic " + Orig + " to " + To);
        m_ReplaceSystNames.insert(std::pair<std::string, std::string>(Orig, To));
        return true;
    }
    bool AnalysisSetup::ReplaceStringOfSystematic(std::stringstream& sstr) {
        std::string Orig(""), To("");
        sstr >> Orig >> To;
        if (m_ReplaceStringOfSyst.find(Orig) != m_ReplaceStringOfSyst.end()) {
            Error("AnalysisSetup::ReplaceStringOfSystematic()",
                  "You've already replaced " + Orig + " by " + m_ReplaceStringOfSyst.at(Orig) + " in systematic. Please choose.");
            return false;
        }
        Info("AnalysisSetup::ReplaceStringOfSystematic()", "Replace string " + Orig + " of systematic by " + To);
        m_ReplaceStringOfSyst.insert(std::pair<std::string, std::string>(Orig, To));
        return true;
    }
    bool AnalysisSetup::FinalizeActRegion() {
        if (m_ActRegion && m_ActRegion != m_RegionCuts.at("")) {
            if (!m_ActRegion->PrependCut(m_RegionCuts.at("")->GetCuts())) return false;
            if (m_uniqueRun) m_ActRegion->AppendCut(m_uniqueRun);
            m_ActRegion->Finalize();
            m_ActRegion = std::shared_ptr<AnalysisRegion>();
        }
        return true;
    }

    std::shared_ptr<Analysis> AnalysisSetup::SetupAnalyses(unsigned int begin, unsigned int end) {
        std::shared_ptr<AnalysisRegion> Region = m_ActRegion;
        if (!Region) {
            /// The option FinalizeAct region causes some
            /// problems as the pointer m_Actregion is nullptr
            if (m_RegionCuts.size() == 2) {
                for (auto& cuts : m_RegionCuts) {
                    if (!cuts.first.empty()) Region = cuts.second;
                }
            } else {
                Region = m_RegionCuts.at("");
            }
        }
        FinalizeActRegion();

        /// Combinatorics raised from the Fake-factor service
        if (!CombinatoricService::getService()->has_permutable()) {
            m_Analysis = std::make_shared<Analysis>();
        } else {
            m_Analysis = std::make_shared<SubSetAnalysis>();
        }

        ParseOptionsToAnalysis(m_Analysis, begin, end);
        m_Analysis->SetName(m_AnaName);
        m_Analysis->SetCuts(Region->GetCuts());

        if (!m_WeightSystematics.empty()) {
            m_Analysis->SetWeightSystmatics(m_WeightSystematics);
            m_Analysis->ProcessNominal(m_doNominal);
        }
        // Order the histograms by name
        std::sort(m_Histos.begin(), m_Histos.end(),
                  [](std::shared_ptr<Histo> a, std::shared_ptr<Histo> b) { return a->name() < b->name(); });
        if (m_RegionCuts.size() > 2) {
            std::vector<std::shared_ptr<Histo>> Histos;
            for (auto& R : m_RegionCuts) {
                // Skip commonly defined regions...
                if (R.first.empty()) continue;
                std::shared_ptr<Histo> Array = R.second->CreateHistoArray(m_Histos, m_doCutFlow);
                Histos.push_back(Array);
            }
            m_Analysis->SetHistos(Histos);
            m_Analysis->SetRegion("");
            if (!m_doCutFlow) {
                m_Analysis->SetCuts(m_RegionCuts.at("")->GetCuts());
            } else {
                m_Analysis->SetCuts(std::vector<std::shared_ptr<Condition>>{});
            }
        } else {
            if (!Region->name().empty()) m_Analysis->SetRegion(Region->name());
            m_Analysis->SetHistos(m_Histos);
            m_Analysis->DoCutFlow(m_doCutFlow);
        }
        return m_Analysis;
    }
    void AnalysisSetup::ParseOptionsToAnalysis(std::shared_ptr<Analysis> Anal, unsigned int begin, unsigned int end) {
        Anal->SetTreeName(m_Tree);
        Anal->SetOutputLocation(m_outFile);
        // Parallelize unmerged input file jobs
        if (begin == 0 && m_InputFiles.size() <= end) {
            Anal->SetInputFiles(m_InputFiles);
        } else {
            std::vector<std::string> Files;
            for (unsigned int i = begin; i < end && i < m_InputFiles.size(); ++i) { Files.push_back(m_InputFiles.at(i)); }
            Anal->SetInputFiles(Files);
        }
        Anal->SetReaders(m_Readers);
        Anal->SetSystNames(m_ReplaceSystNames);
        Anal->SetSystSuffixName(m_SystPrefix);
        if (!m_Systematics.empty()) Anal->SetSystematics(m_Systematics);
        Anal->ProcessSystematics(m_doSyst);
        Anal->ProcessWeightVariations(m_doSystW);
        if (m_SetNominalName) Anal->SetNominalName(m_NominalName);
        if (!m_MetaInputFiles.empty()) Anal->setBatchMode();
        if (m_Debug) Anal->DisableProgressBar();
    }
    void AnalysisSetup::SetOutputLocation(const std::string& File) { m_outFile = File; }
    bool AnalysisSetup::ImportFile(const std::string& C, ConfigReader Parser, bool always) {
        std::ifstream Import;
        std::string Path = ResolvePath(C);
        if (Path.empty()) {
            Error("AnalysisSetup::ImportFile()", "No config path was given");
            return false;
        }
        if (!always && IsElementInList(m_ImportedConfigs, Path)) {
            Info("AnalysisSetup::ImportFile()", "The file " + Path + " has already been imported. It will not imported again.");
            return true;
        }
        m_ImportedConfigs.push_back(Path);
        Info("AnalysisSetup::ImportFile()", "Import new file " + Path);
        Import.open(Path);
        return (this->*Parser)(Import);
    }
    bool AnalysisSetup::ReadHistoConfig(std::ifstream& inf) {
        std::string line;
        XAMPP::HistoTemplates* H_temp = XAMPP::HistoTemplates::getHistoTemplater();
        IfDefLineParser ReadLine;

        IfDefLineParser::LineStatus LineStatus = ReadLine(inf, line);
        while (LineStatus == IfDefLineParser::LineStatus::NewProperty) {
            std::stringstream sstr(line);
            std::string word;
            sstr >> word;
            if (word == "1DHisto" && !H_temp->Create1D(line))
                return false;
            else if (word == "2DHisto" && !H_temp->Create2D(line))
                return false;
            else if (word == "3DHisto" && !H_temp->Create3D(line))
                return false;
            else if (word == "CrossProduct" && !H_temp->CrossProduct(line))
                return false;
            else if (word == "VarHisto" && !H_temp->CreateVarHisto(GetWordFromStream(sstr), inf))
                return false;
            else if (word == "Import" && !ImportFile(GetWordFromStream(sstr), &AnalysisSetup::ReadHistoConfig, false))
                return false;
            else if (word == "NewVar") {
                std::shared_ptr<Histo> H = CreateHisto(inf, m_Readers);
                if (!H) return false;
                m_Histos.push_back(H);
            }
            LineStatus = ReadLine(inf, line);
        }
        return (LineStatus != IfDefLineParser::LineStatus::ReadError);
    }
    AnalysisSetup::~AnalysisSetup() {
        delete ReaderProvider::GetInstance();
        if (m_LoadWeightsOnly) delete NormalizationDataBase::getDataBase();
    }
    bool AnalysisSetup::CreateNewRegion(const std::string& Name) {
        if (m_RegionCuts.find(Name) != m_RegionCuts.end()) {
            Error("AnalysisSetup::CreateNewRegion()", "The region " + Name + " already exists. Please chose another name for your region.");
            return false;
        }
        FinalizeActRegion();
        Info("AnalysisSetup::CreateNewRegion()", "Create a new analysis region " + Name);
        std::shared_ptr<AnalysisRegion> R = std::make_shared<AnalysisRegion>(Name);
        m_RegionCuts.insert(std::pair<std::string, std::shared_ptr<AnalysisRegion>>(Name, R));
        m_ActRegion = R;
        return true;
    }
    bool AnalysisSetup::ParseTreeConfig(std::string TreeConfig, std::string InputConfig) {
        if (!ImportFile(InputConfig, &AnalysisSetup::ReadInputFiles, false) || !InitMetaData()) { return false; }
        if (!ImportFile(TreeConfig, &AnalysisSetup::ReadTreeConfig, false)) { return false; }
        SetCrossSectionDir();
        return true;
    }
    bool AnalysisSetup::ReadTreeConfig(std::ifstream& inf) {
        std::string line;

        IfDefLineParser ReadLine;

        IfDefLineParser::LineStatus LineStatus = ReadLine(inf, line);
        while (LineStatus == IfDefLineParser::LineStatus::NewProperty) {
            std::stringstream sstr(line);
            std::string word = GetWordFromStream(sstr);
            if (word == "Import" && !ImportFile(GetWordFromStream(sstr), &AnalysisSetup::ReadTreeConfig, false))
                return false;
            else if (word == "RunConfig") {
                if (!ImportFile(GetWordFromStream(sstr), &AnalysisSetup::ReadAnaConfig, false)) return false;
                FinalizeActRegion();
            } else if (XAMPP::Weight::getWeighter()->isData() && word == "xSecDir") {
                SetCrossSectionDir();
            } else if (word == "New_Branch") {
                ITreeVarWriter* W = CreateOutBranch(inf, m_Readers);
                if (!W) {
                    Error("AnalysisSetup::ReadTreeConfig()", "Failed to create branch.");
                    return false;
                }
                m_Branches.push_back(W);
            } else if (word == "RenameWeight") {
                std::string Orig(""), To("");
                sstr >> Orig >> To;
                if (m_ReplaceBranchNames.find(Orig) != m_ReplaceBranchNames.end()) {
                    Error("AnalysisSetup::ReadTreeConfig()",
                          "You've already renamed weight " + Orig + " to " + m_ReplaceBranchNames.at(Orig) + ". Please choose");
                    return false;
                }
                m_ReplaceBranchNames.insert(std::pair<std::string, std::string>(Orig, To));
            }
            // You can rename systematics which are different in AFII and FullSim
            else if (word == "RenameSyst" && !RenameSystematic(sstr))
                return false;
            else if (word == "SystReplace" && !ReplaceStringOfSystematic(sstr))
                return false;
            else if (word == "noSystW") {
                Info("AnalysisSetup::ReadTreeConfig()", "Switch off weight systematics");
                m_doSystW = false;
            } else if (word == "Systematic") {
                while (sstr >> word) {
                    if (IsElementInList(m_Systematics, word)) continue;
                    Info("AnalysisSetup::ReadAnaConfig()", "Run over kinematic systematic " + word);
                    m_Systematics.push_back(word);
                }
            } else if (word == "SystPrefix") {
                sstr >> m_SystPrefix;
            } else if (word == "OutTreeName")
                m_SampleName = GetWordFromStream(sstr);

            LineStatus = ReadLine(inf, line);
        }
        return (LineStatus != IfDefLineParser::LineStatus::ReadError);
    }
    std::shared_ptr<Analysis> AnalysisSetup::SetupNTupleWriters(unsigned int begin, unsigned int end) {
        FinalizeActRegion();
        std::vector<std::shared_ptr<Condition>> SumCuts;
        for (auto& R : m_RegionCuts) {
            if (R.second->RetrieveCompressedCut()) {
                SumCuts.push_back(R.second->RetrieveCompressedCut());
                ITreeVarWriter* W = ScalarTreeVarWriter<bool>::GetWriter("Is" + R.second->name(), R.second->RegionReader());
                m_Readers.push_back(R.second->RegionReader());
                m_Branches.push_back(W);
            }
        }
        std::shared_ptr<NTupleWriter> Ana;
        if (!CombinatoricService::getService()->has_permutable()) {
            Ana = std::make_shared<NTupleWriter>();
        } else {
            Ana = std::make_shared<SubSetNTupleWriter>();
        }
        m_Analysis = Ana;
        ParseOptionsToAnalysis(Ana, begin, end);
        Ana->SetCuts(SumCuts);
        Ana->SetName(m_SampleName);
        Ana->SetBranches(m_Branches);
        Ana->SetWeightNames(m_ReplaceBranchNames);
        Ana->SetSystReplaceStrings(m_ReplaceStringOfSyst);
        return m_Analysis;
    }
    void AnalysisSetup::ClearVectors() {
        m_PRWlumiFiles.clear();
        m_PRWconfigFiles.clear();
        m_Weights.clear();
        m_DataWeights.clear();
    }
    void AnalysisSetup::ignoreMetaInput() { m_ignoreMetaInputs = true; }
    std::shared_ptr<Analysis> AnalysisSetup::SetupEventChecker() {
        m_Analysis = std::make_shared<EventIndexer>(m_AnaName, m_Tree, m_InputFiles);
        m_Analysis->SetOutputLocation(m_outFile);
        return m_Analysis;
    }
}  // namespace XAMPP
