#include <XAMPPplotting/AnalysisSetup.h>
#include <XAMPPplotting/ArithmetricReaders.h>
#include <XAMPPplotting/Cuts.h>
#include <XAMPPplotting/DataDrivenWeights.h>
#include <XAMPPplotting/HistoTemplates.h>
#include <XAMPPplotting/LumiCalculator.h>
#include <XAMPPplotting/ObservableReader.h>
#include <XAMPPplotting/ReaderProvider.h>
#include <XAMPPplotting/ReconstructedParticleReaders.h>
#include <XAMPPplotting/TreeVarReader.h>
#include <XAMPPplotting/UtilityReader.h>
#include <XAMPPplotting/Weight.h>

#include <TH1D.h>
namespace XAMPP {
    //#########################################################################################
    //                                  PseudoScalarVarReader
    //#########################################################################################
    PseudoScalarVarReader* PseudoScalarVarReader::GetReader(const std::string& varname) {
        ITreeVarReaderStorage* S = ITreeVarReaderStorage::GetInstance();
        if (!S->GetReader(varname)) return new PseudoScalarVarReader(varname);
        return dynamic_cast<PseudoScalarVarReader*>(S->GetReader(varname));
    }
    PseudoScalarVarReader* PseudoScalarVarReader::GetReader(const std::string& varname, double value) {
        PseudoScalarVarReader* Reader = GetReader(varname);
        if (Reader) Reader->SetValue(value);
        return Reader;
    }
    PseudoScalarVarReader::PseudoScalarVarReader(const std::string& varname) :
        IScalarReader(),
        m_Name(varname),
        m_Registered(false),
        m_Value(-1) {
        m_Registered = ITreeVarReaderStorage::GetInstance()->Register(this);
    }
    std::string PseudoScalarVarReader::name() const { return m_Name; }
    bool PseudoScalarVarReader::init(TTree* t) {
        reset();
        return (m_Registered && t != nullptr);
    }
    double PseudoScalarVarReader::read() const { return m_Value; }
    void PseudoScalarVarReader::SetValue(double V) { m_Value = V; }
    //############################################################
    //                  BinnedDataTakingReader
    //############################################################
    ITreeVarReader* BinnedDataTakingReader::GetReader(double lumi_slice) {
        std::string r_name = lumi_slice > 0 ? Form("Sliced lumi_reader %.2f", lumi_slice) : "Binned RunNumberReader";
        if (!ITreeVarReaderStorage::GetInstance()->GetReader(r_name)) return new BinnedDataTakingReader(lumi_slice);
        return ITreeVarReaderStorage::GetInstance()->GetReader(r_name);
    }
    std::string BinnedDataTakingReader::name() const {
        return m_lumi_slice > 0 ? Form("Sliced lumi_reader %.2f", m_lumi_slice) : "Binned RunNumberReader";
    }
    double BinnedDataTakingReader::read() const { return m_lumi_slices->find_bin(m_run_reader->read(), m_lumi_reader->read()); }
    bool BinnedDataTakingReader::init(TTree* t) {
        if (!m_Registered) {
            Error("BinnedDataTakingReader()", "Failed to register myself");
            return false;
        }
        if (!m_run_reader || !m_run_reader->init(t)) {
            Error("BinnedDataTakingReader()", "Failed to initialize the run number");
            return false;
        }
        if (!m_lumi_reader || !m_lumi_reader->init(t)) {
            Error("BinnedDataTakingReader()", "Failed to initialize the luminosity block.");
            return false;
        }
        return true;
    }
    BinnedDataTakingReader::~BinnedDataTakingReader() { delete LumiCalculator::getCalculator(); }
    BinnedDataTakingReader::BinnedDataTakingReader(double lumi_slice) :
        IScalarReader(),
        m_lumi_slice(lumi_slice),
        m_run_reader(PeriodReader::GetReader()),
        m_lumi_reader(DataLumiReader::GetReader()),
        m_lumi_slices(lumi_slice > 0 ? std::make_unique<LumiSlicer>(lumi_slice) : std::make_unique<LumiSlicer>()),
        m_Registered(false) {
        m_Registered = ITreeVarReaderStorage::GetInstance()->Register(this);

        std::string template_name = lumi_slice > 0 ? Form("PreDefined_BinnedLumiBlocks%.2f", lumi_slice) : "PreDefined_BinnedRunNumbers";

        if (!HistoTemplates::getHistoTemplater()->GetTemplate(template_name)) {
            std::shared_ptr<TH1> H = std::make_shared<TH1D>(template_name.c_str(), "", m_lumi_slices->size(), 1, m_lumi_slices->size() + 1);
            /// Assign the run numbers to the histogram
            for (unsigned int i = m_lumi_slices->size(); i > 0; --i) {
                std::shared_ptr<LumiInterval> block = m_lumi_slices->get_block(i);
                if (lumi_slice <= 0) { H->GetXaxis()->SetBinLabel(i, Form("%u", block->start_run())); }
            }
            HistoTemplates::getHistoTemplater()->InsertTemplate(template_name, H);
        }
    }
    //############################################################
    //                  CutAppliedReader
    //############################################################
    double CutAppliedReader::read() const {
        if (!m_cacheDecision || m_eventNumber != m_service->currentEvent()) {
            m_eventNumber = m_service->currentEvent();
            m_cache = true;
            for (const auto& C : m_Cuts) {
                if (!C->Pass()) {
                    m_cache = false;
                    break;
                }
            }
        }
        return m_cache;
    }
    bool CutAppliedReader::init(TTree*) {
        if (!m_Printed) {
            Info("CutAppliedReader()", "The following cutflow is read by " + name());
            for (auto& C : m_Cuts) Info("CutAppliedReader()", "   --- " + C->name());
            m_Printed = true;
        }
        reset();
        return m_Registered;
    }
    std::string CutAppliedReader::name() const { return m_name; }
    CutAppliedReader::CutAppliedReader(const std::vector<std::shared_ptr<Condition>>& cuts, const std::string& name) :
        IScalarReader(),
        m_name(name),
        m_Cuts(cuts),
        m_Registered(false),
        m_Printed(false),
        m_cacheDecision(true),
        m_cache(false),
        m_eventNumber(-1),
        m_service(EventService::getService()) {
        m_Registered = ITreeVarReaderStorage::GetInstance()->Register(this);
    }
    void CutAppliedReader::cacheDecision(bool B) { m_cacheDecision = B; }
    //#########################################################################################
    //                                  SetReader
    //#########################################################################################
    bool SetReader::init(TTree* t) {
        if (m_readers.empty()) {
            Warning("SetReader::init()", "Somehow you forgot to link the readers?!");
            return false;
        }
        for (auto& R : m_readers) {
            if (!R->init(t)) return false;
        }
        return true;
    }
    std::string SetReader::name() const { return m_name; }
    SetReader::SetReader(const std::string& name, const std::vector<ITreeVarReader*>& Set) :
        m_readers(),
        m_name(name),
        m_currentEv(-1),
        m_n_entries(0),
        m_service(EventService::getService()) {
        std::vector<ITreeVarReader*> dup_free;
        CopyVector(Set, dup_free);
        for (const auto& r : dup_free) m_readers.push_back(std::make_shared<SetItem>(r));
        if (!ITreeVarReaderStorage::GetInstance()->Register(this)) m_readers.clear();
    }
    ITreeVarReader* SetReader::GetReader(const std::string& name, const std::vector<ITreeVarReader*>& Set) {
        if (!ITreeVarReaderStorage::GetInstance()->GetReader(name)) return new SetReader(name, Set);
        return ITreeVarReaderStorage::GetInstance()->GetReader(name);
    }
    double SetReader::readEntry(const size_t I) const {
        if (I >= entries()) return 1.e25;
        if (m_currentObj == nullptr || m_currentObj->begin() > I || m_currentObj->end() <= I) {
            for (const auto& obj : m_readers) {
                if (obj->begin() > I || obj->end() <= I) continue;
                m_currentObj = obj;
                break;
            }
        }
        return m_currentObj->readEntry(I);
    }
    size_t SetReader::entries() const {
        if (m_currentEv != m_service->currentEvent()) {
            m_currentEv = m_service->currentEvent();
            size_t t = 0;
            for (const auto& item : m_readers) {
                const size_t n = item->entries();
                item->setBegin(n > 0 ? t : -1);
                t += n;
            }
            m_n_entries = t;
        }
        return m_n_entries;
    }
    SetReader::SetItem::SetItem(ITreeVarReader* R) : m_reader(R), m_begin(-1) {}

    double SetReader::SetItem::readEntry(const size_t I) const { return m_reader->readEntry(I - begin()); }
    bool SetReader::SetItem::init(TTree* t) { return m_reader && m_reader->init(t); }
    size_t SetReader::SetItem::begin() const { return m_begin; }
    size_t SetReader::SetItem::end() const { return begin() + m_reader->entries(); }
    void SetReader::SetItem::setBegin(size_t b) { m_begin = b; }
    size_t SetReader::SetItem::entries() const { return m_reader->entries(); }

    //#########################################################################################
    //                                  ClassificationReader
    //#########################################################################################
    ClassificationReader* ClassificationReader::GetReader(const std::string& Name) {
        ITreeVarReaderStorage* S = ITreeVarReaderStorage::GetInstance();
        if (!S->GetReader(Name)) return new ClassificationReader(Name);
        return dynamic_cast<ClassificationReader*>(S->GetReader(Name));
    }
    ClassificationReader::ClassificationReader(const std::string& Name) :
        IVectorReader(),
        m_Name(Name),
        m_Regist(false),
        m_Setup(false),
        m_Classification(),
        m_CutPartReaders(),
        m_tot_entries(0),
        m_currentEv(-1),
        m_service(EventService::getService()),
        m_currentObj(nullptr) {
        m_Regist = ITreeVarReaderStorage::GetInstance()->Register(this);
    }
    size_t ClassificationReader::entries() const {
        if (m_service->currentEvent() != m_currentEv) {
            m_currentEv = m_service->currentEvent();
            size_t t = 0;
            for (auto& clas : m_Classification) {
                size_t nt = clas->entries();
                clas->setBegin(nt > 0 ? t : -1);
                t += nt;
            }
            m_tot_entries = t;
        }
        return m_tot_entries;
    }
    double ClassificationReader::readEntry(const size_t I) const {
        if (I >= entries()) {
            Warning("ClassificationReader::readEntry()", Form("%s has recieved index out of range %lu", name().c_str(), I));
            return 1.e25;
        }
        if (m_currentObj == nullptr || m_currentObj->begin() > I || m_currentObj->end() >= I) {
            for (const auto& obj : m_Classification) {
                if (obj->begin() > I || obj->end() <= I) continue;
                m_currentObj = obj;
                break;
            }
        }
        return m_currentObj->readEntry(I - m_currentObj->begin());
    }
    std::string ClassificationReader::name() const { return m_Name; }
    void ClassificationReader::SetupDone() { m_Setup = true; }
    bool ClassificationReader::AddClassification(std::shared_ptr<Condition> Cond, ITreeVarReader* Obj, ITreeVarReader* Replica) {
        if (m_Setup) {
            Error("ClassificationReader::AddClassifcation()", "The definition of " + name() + " has already been terminated");
            return false;
        } else if (!Obj) {
            Error("ClassificationReader::AddClasification()", "No object has been given");
            return false;
        }
        if (!Cond) {
            Error("ClassificationReader::AddClassifcation()", "No condition has been defined.");
            return false;
        }
        if (Replica == nullptr) {
            m_Classification.push_back(std::make_shared<Classification>(Obj, Cond));
        } else {
            m_Classification.push_back(std::make_shared<ReplicatedClassification>(Obj, Cond, Replica));
        }
        m_currentObj.reset();
        return true;
    }
    bool ClassificationReader::AddClassification(std::ifstream& inf) {
        std::string line("");
        IfDefLineParser ReadLine;
        std::shared_ptr<Condition> C = nullptr;
        ITreeVarReader* Obj = nullptr;
        ITreeVarReader* SizeObj = nullptr;
        while (ReadLine(inf, line) == IfDefLineParser::NewProperty) {
            std::stringstream sstr(line);
            std::string word = GetWordFromStream(sstr);
            if (word == "End_Class")
                return AddClassification(C, Obj, SizeObj);
            else if (word.find("Cut") != std::string::npos) {
                if (C) {
                    Error("AddClassification()",
                          "There exists already an condition " + C->name() + ". No idea how to combine, please use 'CombCut'.");
                    return false;
                }
                if (word == "CombCut")
                    C = CreateCombinedCut(inf, m_CutPartReaders, line);
                else
                    C = CreateCut(line, m_CutPartReaders);
                if (!C) {
                    Error("ClassificationReader::AddClassification()", "Could not create cut");
                    return false;
                }
            } else if (word.find("Reader") != std::string::npos) {
                if (Obj != nullptr) {
                    Error("AddClassification()", "There is already an object " + Obj->name());
                    return false;
                }
                Obj = word.find("New_") == std::string::npos ? ReaderProvider::GetInstance()->CreateReader(line)
                                                             : ReaderProvider::GetInstance()->CreateReader(inf, line);
                if (!Obj) return false;
            } else if (word == "ReplicateTimes") {
                word == GetWordFromStream(sstr);
                SizeObj = word.find("New_") == std::string::npos
                              ? ReaderProvider::GetInstance()->CreateReader(line.substr(line.find(word) + word.size()))
                              : ReaderProvider::GetInstance()->CreateReader(inf, line.substr(line.find(word) + word.size()));
            }
        }
        Error("ClassificationReader::AddClassification()", "Missing 'End_Class' statement'");
        return false;
    }
    bool ClassificationReader::init(TTree* t) {
        if (!m_Regist) {
            Error("ClassificationReader::init()", "Something went wrong with the creation of the reader.");
            return false;
        }
        if (m_Classification.empty()) {
            Error("ClassificationReader::init()", "No classifications have been defined.");
            return false;
        }
        for (auto& R : m_CutPartReaders)
            if (!R->init(t)) return false;
        for (auto& C : m_Classification)
            if (!C->reader()->init(t)) return false;
        reset();
        return true;
    }
    ClassificationReader::~ClassificationReader() {}

    //#############################################
    //               Classification
    //#############################################
    bool ClassificationReader::Classification::isSatisfied() {
        if (!m_Cut) {
            Warning("Classification::isSatisfied()", "No cut has been given. Return false");
            return false;
        } else if (m_eventNumber != m_service->currentEvent()) {
            m_isSatisfied = m_Cut->Pass();
            m_eventNumber = m_service->currentEvent();
        }
        return m_isSatisfied;
    }
    size_t ClassificationReader::Classification::entries() {
        if (!isSatisfied()) return 0;
        return m_Reader->entries();
    }
    double ClassificationReader::Classification::readEntry(size_t e) const { return m_Reader->readEntry(e); }
    ITreeVarReader* ClassificationReader::Classification::reader() const { return m_Reader; }
    ClassificationReader::Classification::Classification(ITreeVarReader* R, std::shared_ptr<Condition> C) :
        m_Cut(C),
        m_Reader(R),
        m_isSatisfied(false),
        m_eventNumber(-1),
        m_service(EventService::getService()),
        m_begin(0) {}
    void ClassificationReader::Classification::setBegin(size_t b) { m_begin = b; }
    size_t ClassificationReader::Classification::begin() { return m_begin; }
    size_t ClassificationReader::Classification::end() { return m_begin + entries(); }
    //##################################################
    //              ReplicatedClassification
    //##################################################
    ClassificationReader::ReplicatedClassification::ReplicatedClassification(ITreeVarReader* R, std::shared_ptr<Condition> C,
                                                                             ITreeVarReader* S) :
        Classification(R, C),
        m_sizeReader(S) {}
    double ClassificationReader::ReplicatedClassification::readEntry(size_t e) const {
        return Classification::readEntry(e % reader()->entries());
    }
    size_t ClassificationReader::ReplicatedClassification::entries() { return reader()->entries() * m_sizeReader->entries(); }

    //#########################################################################################
    //                                  AbsIParticleVarReader
    //#########################################################################################
    IParticleVarReader* AbsIParticleVarReader::GetReader(IParticleVarReader* R) {
        ITreeVarReaderStorage* S = ITreeVarReaderStorage::GetInstance();
        if (!S->GetReader("|" + R->name() + "|")) return new AbsIParticleVarReader(R);
        return dynamic_cast<IParticleVarReader*>(S->GetReader("|" + R->name() + "|"));
    }
    double AbsIParticleVarReader::readEntry(size_t I) const { return std::fabs(m_Var->readEntry(I)); }
    bool AbsIParticleVarReader::init(TTree* t) {
        if (!m_Registered) Error("AbsIParticleVarReader::init()", "Something went wrong during initialization");
        return m_Registered && m_Var->init(t);
    }
    std::string AbsIParticleVarReader::name() const { return "|" + m_Var->name() + "|"; }
    AbsIParticleVarReader::AbsIParticleVarReader(IParticleVarReader* R) : IParticleVariable(R->getParticle()), m_Var(R) {
        m_Registered = ITreeVarReaderStorage::GetInstance()->Register(this);
    }
    //#########################################################################################
    //                                  VectorSizeReader
    //#########################################################################################
    double VectorSizeReader::read() const { return m_Reader->entries(); }
    bool VectorSizeReader::init(TTree* t) {
        if (!m_Reader) {
            Error("VectorSizeReader()", "Something went wrong during construction");
            return false;
        }
        reset();
        return m_Reader->init(t);
    }
    std::string VectorSizeReader::name() const { return "N_" + m_Reader->name(); }
    ITreeVarReader* VectorSizeReader::GetReader(ITreeVectorReader* Vector) {
        if (!Vector) {
            Error("VectorSizeReader::GetReader()", "Nothing given");
            return nullptr;
        }
        if (!ITreeVarReaderStorage::GetInstance()->GetReader("N_" + Vector->name())) { return new VectorSizeReader(Vector); }
        return ITreeVarReaderStorage::GetInstance()->GetReader("N_" + Vector->name());
    }
    VectorSizeReader::VectorSizeReader(ITreeVectorReader* Vector) : IScalarReader(), m_Reader(Vector) {
        if (!ITreeVarReaderStorage::GetInstance()->Register(this)) m_Reader = nullptr;
    }
    //#########################################################################################
    //                              VectorRangeReader
    //#########################################################################################
    size_t VectorRangeReader::entries() const { return std::min(m_Variable->entries(), m_end + 1) - m_begin; }
    double VectorRangeReader::readEntry(const size_t I) const {
        if (I >= entries()) {
            Warning("VectorRangeReader::readEntry()", Form("%s has recieved an index %lu out of range. Return 1.e25", name().c_str(), I));
            return 1.e25;
        }
        return m_Variable->readEntry(m_begin + I);
    }
    bool VectorRangeReader::init(TTree* t) {
        if (!m_Registered || m_begin >= m_end) {
            Error("RangeParticleReader::init()", "Something went wrong during creation of " + name());
            return false;
        }
        return m_Variable->init(t);
    }
    std::string VectorRangeReader::name() const {
        return m_Variable->name() + " [" + std::to_string(m_begin) + "-" + std::to_string(m_end) + "]";
    }
    ITreeVectorReader* VectorRangeReader::GetReader(ITreeVectorReader* Var, size_t begin, size_t end) {
        std::string name = Var->name() + " [" + std::to_string(begin) + "-" + std::to_string(end) + "]";
        if (!ITreeVarReaderStorage::GetInstance()->GetReader(name)) return new VectorRangeReader(Var, begin, end);
        return dynamic_cast<ITreeVectorReader*>(ITreeVarReaderStorage::GetInstance()->GetReader(name));
    }
    VectorRangeReader::VectorRangeReader(ITreeVectorReader* Variable, size_t begin, size_t end) :
        IVectorReader(),
        m_Variable(Variable),
        m_begin(begin),
        m_end(end),
        m_Registered(ITreeVarReaderStorage::GetInstance()->Register(this)) {}
    //#########################################################################################
    //                                  VectorNthEntryReader
    //#########################################################################################
    double VectorNthEntryReader::read() const { return m_Reader->readEntry(m_Entry); }
    size_t VectorNthEntryReader::entries() const { return m_Reader->entries() > m_Entry ? 1 : 0; }
    bool VectorNthEntryReader::update() { return IScalarReader::update() && entries() > 0; }
    bool VectorNthEntryReader::init(TTree* t) {
        if (!m_Reader) {
            Error("NthEntryVectorReader()", "Something went wrong during construction");
            return false;
        }
        reset();
        return m_Reader->init(t);
    }
    std::string VectorNthEntryReader::name() const { return m_Reader->name() + Form("[%lu]", m_Entry); }
    ITreeVarReader* VectorNthEntryReader::GetReader(ITreeVectorReader* Vector, size_t Entry) {
        if (!Vector) {
            Error("NthEntryVectorReader::GetReader()", "Nothing given");
            return nullptr;
        }
        if (!ITreeVarReaderStorage::GetInstance()->GetReader(Vector->name() + Form("[%lu]", Entry))) {
            return new VectorNthEntryReader(Vector, Entry);
        }
        return ITreeVarReaderStorage::GetInstance()->GetReader(Vector->name() + Form("[%lu]", Entry));
    }
    VectorNthEntryReader::VectorNthEntryReader(ITreeVectorReader* Vector, size_t Entry) :
        IScalarReader(),
        m_Reader(Vector),
        m_Entry(Entry) {
        if (!ITreeVarReaderStorage::GetInstance()->Register(this)) m_Reader = nullptr;
    }

    //#########################################################################################
    //                              MatrixBlockReader
    //#########################################################################################
    bool MatrixBlockReader::init(TTree* t) {
        if (!m_Registered) {
            Error("MatrixBlockReader::init()", "Something went wrong during creation of " + name());
            return false;
        }
        return m_Variable->init(t);
    }
    std::string MatrixBlockReader::name() const { return m_Variable->name() + " " + GetMatrixRangesFromCube(m_Cube); }
    ITreeMatrixReader* MatrixBlockReader::GetReader(ITreeMatrixReader* Reader, Cube C) {
        if (Reader == nullptr) {
            Warning("MatrixBlockReader::GetReader()", "Nothing has been given.");
            return nullptr;
        }
        std::string name = Reader->name() + " " + GetMatrixRangesFromCube(C);
        if (!ITreeVarReaderStorage::GetInstance()->GetReader(name)) return new MatrixBlockReader(Reader, C);
        return dynamic_cast<ITreeMatrixReader*>(ITreeVarReaderStorage::GetInstance()->GetReader(name));
    }
    MatrixBlockReader::MatrixBlockReader(ITreeMatrixReader* Reader, Cube C) :
        IMatrixReader(),
        m_Variable(Reader),
        m_Cube(C),
        m_Registered(ITreeVarReaderStorage::GetInstance()->Register(this)) {}

    size_t MatrixBlockReader::num_rows() const { return min(m_Variable->num_rows(), m_Cube.first.second + 1) - m_Cube.first.first; }
    size_t MatrixBlockReader::row_entries(const size_t I) const {
        if (I >= num_rows()) return 0;
        return min(m_Variable->row_entries(I), m_Cube.second.second + 1) - m_Cube.second.first;
    }
    double MatrixBlockReader::readMatrixEntry(const size_t I, const size_t J) const {
        if (J >= row_entries(I)) {
            Warning("MatrixBlockRader::readMatrixEntry()",
                    Form("The indices %lu,%lu given to %s are out of range. Return 1.e25", I, J, name().c_str()));
            return 1.e25;
        }
        return m_Variable->readMatrixEntry(I + m_Cube.first.first, J + m_Cube.second.first);
    }

    //#########################################################################################
    //                                  ConditionalWeightAppliedReader
    //#########################################################################################
    ConditionalWeightAppliedReader::ConditionalWeightAppliedReader(ConditionalWeight* Weight) :
        IScalarReader(),
        m_Weight(Weight),
        m_Registered(false) {
        if (m_Weight) m_Registered = ITreeVarReaderStorage::GetInstance()->Register(this);
    }
    ITreeVarReader* ConditionalWeightAppliedReader::GetReader(const std::string& Name) {
        return GetReader(dynamic_cast<ConditionalWeight*>(Weight::getWeighter()->GetWeightElement(Name)));
    }
    ITreeVarReader* ConditionalWeightAppliedReader::GetReader(ConditionalWeight* Weight) {
        if (!Weight) {
            Error("CondAppliedReader::GetReader()", "No conditional weight element given");
            return nullptr;
        }
        if (!ITreeVarReaderStorage::GetInstance()->GetReader("Weight condition applied" + Weight->name()))
            return new ConditionalWeightAppliedReader(Weight);
        return ITreeVarReaderStorage::GetInstance()->GetReader("Weight condition applied" + Weight->name());
    }
    double ConditionalWeightAppliedReader::read() const { return m_Weight->IsWeightApplied(); }
    bool ConditionalWeightAppliedReader::init(TTree*) {
        if (!m_Registered) {
            Error("CondAppliedReader::init()", "Something  went wrong during initialization");
            return false;
        }
        reset();
        return true;
    }
    std::string ConditionalWeightAppliedReader::name() const { return "Weight condition applied" + m_Weight->name(); }
    //#########################################################################################
    //                                 ParticleSizeReader
    //#########################################################################################
    ParticleSizeReader::ParticleSizeReader(std::string Name) : IScalarReader(), m_Particle(nullptr), m_Registered(false) {
        ParReaderStorage* S = ParReaderStorage::GetInstance();
        m_Particle = S->GetReader(Name);
        if (m_Particle) m_Registered = ITreeVarReaderStorage::GetInstance()->Register(this);
    }
    bool ParticleSizeReader::init(TTree* t) {
        if (!m_Registered) {
            Error("ParticleSizeReader::init()", "Could not initialize " + name() + "  due to storage problems.");
            return false;
        }
        return m_Particle->init(t);
    }
    double ParticleSizeReader::read() const { return m_Particle->Size(); }
    std::string ParticleSizeReader::name() const { return "N_" + m_Particle->name(); }

    ParticleSizeReader* ParticleSizeReader::GetReader(std::string Name) {
        ITreeVarReaderStorage* S = ITreeVarReaderStorage::GetInstance();
        if (!S->GetReader("N_" + Name)) new ParticleSizeReader(Name);
        return dynamic_cast<ParticleSizeReader*>(S->GetReader("N_" + Name));
    }

    //#########################################################################################
    //                                  ParticleNthEntryReader
    //#########################################################################################
    ITreeVarReader* ParticleNthEntryReader::GetReader(IParticleVarReader* Reader, size_t N) {
        ITreeVarReaderStorage* S = ITreeVarReaderStorage::GetInstance();
        std::string name = Reader->name() + Form("[%lu]", N);
        if (!S->GetReader(name)) new ParticleNthEntryReader(Reader, N);
        return dynamic_cast<ParticleNthEntryReader*>(S->GetReader(name));
    }
    ParticleNthEntryReader::ParticleNthEntryReader(IParticleVarReader* Reader, size_t N) :
        IScalarReader(),
        m_Var(Reader),
        m_Entry(N),
        m_Registered(false) {
        m_Registered = ITreeVarReaderStorage::GetInstance()->Register(this);
    }
    double ParticleNthEntryReader::read() const { return m_Var->readEntry(m_Entry); }
    size_t ParticleNthEntryReader::entries() const { return m_Entry < m_Var->getParticle()->Size() ? 1 : 0; }
    bool ParticleNthEntryReader::update() { return entries() && IScalarReader::update(); }
    bool ParticleNthEntryReader::init(TTree* t) {
        if (!m_Registered) { return false; }
        return m_Var->init(t);
    }
    std::string ParticleNthEntryReader::name() const { return m_Var->name() + Form("[%lu]", m_Entry); }

    //#########################################################################################
    //                                  MatrixNthRowReader
    //#########################################################################################
    double MatrixNthRowReader::readEntry(size_t I) const { return m_Var->readMatrixEntry(m_ColIdx, I); }
    bool MatrixNthRowReader::init(TTree* t) {
        if (!m_Registered) {
            Error("VectorNthRowReader::init()", "Something went wrong during creation of " + name());
            return false;
        }
        return m_Var->init(t);
    }
    std::string MatrixNthRowReader::name() const { return Form("%s [%lu]", m_Var->name().c_str(), m_ColIdx); }
    size_t MatrixNthRowReader::entries() const { return m_Var->row_entries(m_ColIdx); }
    ITreeVectorReader* MatrixNthRowReader::GetReader(ITreeMatrixReader* Variable, size_t C) {
        if (!Variable) return nullptr;
        std::string Name = Form("%s [%lu]", Variable->name().c_str(), C);
        if (!ITreeVarReaderStorage::GetInstance()->GetReader(Name)) return new MatrixNthRowReader(Variable, C);
        return dynamic_cast<ITreeVectorReader*>(ITreeVarReaderStorage::GetInstance()->GetReader(Name));
    }
    MatrixNthRowReader::MatrixNthRowReader(ITreeMatrixReader* Variable, size_t C) :
        IVectorReader(),
        m_Var(Variable),
        m_ColIdx(C),
        m_Registered(ITreeVarReaderStorage::GetInstance()->Register(this)) {}
    //#########################################################################################
    //                                  MatrixNthColumnReader
    //#########################################################################################
    MatrixNthColumnReader::MatrixNthColumnReader(ITreeMatrixReader* Variable, size_t C) :
        m_Var(Variable),
        m_ColIdx(C),
        m_Idx(-1),
        m_Registered(ITreeVarReaderStorage::GetInstance()->Register(this)) {}
    double MatrixNthColumnReader::read() const { return readEntry(m_Idx); }
    double MatrixNthColumnReader::readEntry(size_t I) const { return m_Var->readMatrixEntry(I, m_ColIdx); }
    bool MatrixNthColumnReader::update() { return setIndex(index() + 1); }
    bool MatrixNthColumnReader::init(TTree* t) {
        if (!m_Registered) {
            Error("VectorNthColumnReader::init()", "Something went wrong during creation of " + name());
            return false;
        }
        return m_Var->init(t);
    }
    void MatrixNthColumnReader::reset() {
        m_Var->reset();
        m_Idx = -1;
    }
    std::string MatrixNthColumnReader::name() const { return Form("%s [%lu]", m_Var->name().c_str(), m_ColIdx); }
    bool MatrixNthColumnReader::setIndex(size_t I) {
        if (!m_Var->setMatrixIndex(I, m_ColIdx)) return false;
        m_Idx = I;
        return true;
    }
    size_t MatrixNthColumnReader::entries() const { return m_Var->num_rows(); }
    size_t MatrixNthColumnReader::index() const { return m_Idx; }
    ITreeVectorReader* MatrixNthColumnReader::GetReader(ITreeMatrixReader* Variable, size_t C) {
        if (!Variable) return nullptr;
        std::string Name = Form("%s [%lu]", Variable->name().c_str(), C);
        if (!ITreeVarReaderStorage::GetInstance()->GetReader(Name)) return new MatrixNthColumnReader(Variable, C);
        return dynamic_cast<ITreeVectorReader*>(ITreeVarReaderStorage::GetInstance()->GetReader(Name));
    }

    //#########################################################################################
    //                                  ParticleNthRowReader
    //#########################################################################################
    double ParticleNthRowReader::readEntry(size_t I) const { return m_Var->readMatrixEntry(m_PartIdx, I); }
    bool ParticleNthRowReader::init(TTree* t) {
        if (!m_Var) {
            Error("ParticleNthRowReader::init()", "Something went wrong");
            return false;
        }
        return m_Var->init(t);
    }
    size_t ParticleNthRowReader::entries() const { return m_Var->row_entries(m_PartIdx); }
    std::string ParticleNthRowReader::name() const { return Form("%s[%lu]", m_Var->name().c_str(), m_PartIdx); }
    ParticleNthRowReader::ParticleNthRowReader(IParticleVectorReader* Variable, size_t P) : IVectorReader(), m_Var(Variable), m_PartIdx(P) {
        if (!ITreeVarReaderStorage::GetInstance()->Register(this)) m_Var = nullptr;
    }
    ITreeVectorReader* ParticleNthRowReader::GetReader(IParticleVectorReader* Variable, size_t P) {
        std::string Name = Form("%s[%lu]", Variable->name().c_str(), P);
        ITreeVarReaderStorage* S = ITreeVarReaderStorage::GetInstance();
        if (!S->GetReader(Name)) return new ParticleNthRowReader(Variable, P);
        return dynamic_cast<ITreeVectorReader*>(S->GetReader(Name));
    }
    //#########################################################################################
    //                                  ParticleRowSizeReader
    //#########################################################################################
    std::string ParticleRowSizeReader::name() const { return "RowSize " + m_Var->name(); }
    bool ParticleRowSizeReader::init(TTree* t) {
        if (!m_Registered) {
            Error("ParticleRowSizeReader::init()", "Something went wrong during initialization of " + name());
            return false;
        }
        return m_Var->init(t);
    }
    double ParticleRowSizeReader::readEntry(const size_t I) const {
        if (I >= m_Var->getParticle()->Size()) {
            Warning("ParticleRowSizeReader::readeEntry()", "The given index " + std::to_string(I) + " is out of range. Return 0.");
            return 0.;
        }
        return m_Var->row_entries(I);
    }
    size_t ParticleRowSizeReader::entries() const { return m_Var->getParticle()->Size(); }
    ParticleRowSizeReader::ParticleRowSizeReader(IParticleVectorReader* Variable) : IVectorReader(), m_Var(Variable), m_Registered(false) {
        m_Registered = ITreeVarReaderStorage::GetInstance()->Register(this);
    }
    ITreeVectorReader* ParticleRowSizeReader::GetReader(IParticleVectorReader* Reader) {
        if (!Reader) { return nullptr; }
        std::string ReaderName = "RowSize " + Reader->name();
        if (!ITreeVarReaderStorage::GetInstance()->GetReader(ReaderName)) { return new ParticleRowSizeReader(Reader); }
        return dynamic_cast<ITreeVectorReader*>(ITreeVarReaderStorage::GetInstance()->GetReader(ReaderName));
    }

    //#########################################################################################
    //                                  VectorRowSizeReader
    //#########################################################################################
    ITreeVectorReader* VectorRowSizeReader::GetReader(ITreeMatrixReader* Reader) {
        if (!Reader) { return nullptr; }
        std::string ReaderName = "RowSize " + Reader->name();
        if (!ITreeVarReaderStorage::GetInstance()->GetReader(ReaderName)) { return new VectorRowSizeReader(Reader); }
        return dynamic_cast<ITreeVectorReader*>(ITreeVarReaderStorage::GetInstance()->GetReader(ReaderName));
    }

    std::string VectorRowSizeReader::name() const { return "ColSize " + m_Var->name(); }
    bool VectorRowSizeReader::init(TTree* t) {
        if (!m_Registered) {
            Error("VectorRowSizeReader::init()", "Something went wrong during initialization of " + name());
            return false;
        }
        reset();
        return m_Var->init(t);
    }
    double VectorRowSizeReader::readEntry(const size_t I) const { return m_Var->row_entries(I); }
    size_t VectorRowSizeReader::entries() const { return m_Var->entries(); }
    VectorRowSizeReader::VectorRowSizeReader(ITreeMatrixReader* Variable) : IVectorReader(), m_Var(Variable), m_Registered(false) {
        m_Registered = ITreeVarReaderStorage::GetInstance()->Register(this);
    }

    //#########################################################################################
    //                                  VectorColumnSizeReader
    //#########################################################################################
    ITreeVectorReader* VectorColumnSizeReader::GetReader(ITreeMatrixReader* Reader) {
        if (!Reader) { return nullptr; }
        std::string ReaderName = "ColSize " + Reader->name();
        if (!ITreeVarReaderStorage::GetInstance()->GetReader(ReaderName)) { return new VectorColumnSizeReader(Reader); }
        return dynamic_cast<ITreeVectorReader*>(ITreeVarReaderStorage::GetInstance()->GetReader(ReaderName));
    }

    std::string VectorColumnSizeReader::name() const { return "ColSize " + m_Var->name(); }
    bool VectorColumnSizeReader::init(TTree* t) {
        if (!m_Registered) {
            Error("VectorColumnSizeReader::init()", "Something went wrong during initialization of " + name());
            return false;
        }
        reset();
        return m_Var->init(t);
    }

    double VectorColumnSizeReader::readEntry(const size_t I) const {
        if (I >= entries()) return 0;
        return m_Cache.at(I);
    }
    size_t VectorColumnSizeReader::entries() const {
        if (m_service->currentEvent() != m_EventNumber) {
            m_EventNumber = m_service->currentEvent();
            size_t max_row_size = 0;
            for (size_t r = 0; r < m_Var->entries(); ++r)
                if (max_row_size < m_Var->row_entries(r)) max_row_size = m_Var->row_entries(r);
            // Create a new vector with size of the maximum rows
            m_Cache = std::vector<size_t>(max_row_size, 0);

            std::vector<size_t>::iterator begin_itr = m_Cache.begin();
            std::vector<size_t>::iterator end_itr = m_Cache.end();
            for (size_t r = 0; r < m_Var->entries(); ++r) {
                for (std::vector<size_t>::iterator itr = begin_itr; itr != (begin_itr + m_Var->row_entries(r)); ++itr) { ++(*itr); }
            }
        }
        return m_Cache.size();
    }
    VectorColumnSizeReader::VectorColumnSizeReader(ITreeMatrixReader* Variable) :
        IVectorReader(),
        m_Var(Variable),
        m_Registered(false),
        m_service(EventService::getService()),
        m_EventNumber(-1) {
        m_Registered = ITreeVarReaderStorage::GetInstance()->Register(this);
    }
    //#########################################################################################
    //                              RangeParticleReader
    //#########################################################################################
    bool ParticleRangeReader::init(TTree* t) {
        if (!m_Registered || m_begin >= m_end) {
            Error("RangeParticleReader::init()", "Something went wrong during creation of " + name());
            return false;
        }
        return m_Variable->init(t);
    }
    std::string ParticleRangeReader::name() const {
        return m_Variable->name() + " [" + std::to_string(m_begin) + "-" + std::to_string(m_end) + "]";
    }
    ITreeVectorReader* ParticleRangeReader::GetReader(IParticleVarReader* Var, size_t begin, size_t end) {
        std::string name = Var->name() + " [" + std::to_string(begin) + "-" + std::to_string(end) + "]";
        if (!ITreeVarReaderStorage::GetInstance()->GetReader(name)) return new ParticleRangeReader(Var, begin, end);
        return dynamic_cast<ITreeVectorReader*>(ITreeVarReaderStorage::GetInstance()->GetReader(name));
    }
    ParticleRangeReader::ParticleRangeReader(IParticleVarReader* Variable, size_t begin, size_t end) :
        m_Variable(Variable),
        m_begin(begin),
        m_end(end),
        m_Registered(ITreeVarReaderStorage::GetInstance()->Register(this)) {}

    size_t ParticleRangeReader::entries() const { return std::min(m_Variable->getParticle()->Size(), m_end + 1) - m_begin; }
    double ParticleRangeReader::readEntry(size_t I) const {
        if (I >= entries()) {
            Warning("ParticleRangeReader::readEntry()", Form("The index %lu given to %s is out of range. Return 1.e25", I, name().c_str()));
            return 1.e25;
        }
        return m_Variable->readEntry(m_begin + I);
    }
    //#########################################################################################
    //                              ParticleBlockReader
    //#########################################################################################
    bool ParticleBlockReader::init(TTree* t) {
        if (!m_Registered) {
            Error("ParticleBlockReader::init()", "Something went wrong during creation of " + name());
            return false;
        }
        return m_Variable->init(t);
    }
    std::string ParticleBlockReader::name() const { return m_Variable->name() + " " + GetMatrixRangesFromCube(m_Cube); }
    ITreeMatrixReader* ParticleBlockReader::GetReader(IParticleVectorReader* Var, Cube C) {
        std::string name = Var->name() + " " + GetMatrixRangesFromCube(C);
        if (!ITreeVarReaderStorage::GetInstance()->GetReader(name)) return new ParticleBlockReader(Var, C);
        return dynamic_cast<ITreeMatrixReader*>(ITreeVarReaderStorage::GetInstance()->GetReader(name));
    }
    ParticleBlockReader::ParticleBlockReader(IParticleVectorReader* Variable, Cube C) :
        IMatrixReader(),
        m_Variable(Variable),
        m_Cube(C),
        m_Current(Ranges(C.first.first, C.second.first - 1)),
        m_Registered(ITreeVarReaderStorage::GetInstance()->Register(this)) {}

    size_t ParticleBlockReader::num_rows() const {
        return min(m_Variable->getParticle()->Size(), m_Cube.first.second + 1) - m_Cube.first.first;
    }
    size_t ParticleBlockReader::row_entries(const size_t I) const {
        if (I >= num_rows()) return 0;
        return min(m_Variable->row_entries(I), m_Cube.second.second + 1) - m_Cube.second.first;
    }
    double ParticleBlockReader::readMatrixEntry(const size_t I, const size_t J) const {
        if (J >= row_entries(I)) {
            Warning("MatrixBlockRader::readMatrixEntry()",
                    Form("The indices %lu,%lu given to %s are out of range. Return 1.e25", I, J, name().c_str()));
            return 1.e25;
        }
        return m_Variable->readMatrixEntry(I + m_Cube.first.first, J + m_Cube.second.first);
    }
}  // namespace XAMPP
