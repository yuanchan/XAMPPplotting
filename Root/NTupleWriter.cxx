#include <TSystem.h>
#include <XAMPPplotting/DataDrivenWeights.h>
#include <XAMPPplotting/NTupleWriter.h>
#include <XAMPPplotting/TreeVarWriter.h>
#include <XAMPPplotting/UtilityReader.h>
#include <XAMPPplotting/Weight.h>
namespace XAMPP {

    //####################################################
    //              NonZeroWeightSystCheck
    //####################################################
    NonZeroWeightSystCheck::NonZeroWeightSystCheck(IWeightElement* element) :
        m_element(element),
        m_nominal(nullptr),
        m_Systematics(),
        m_n_rejected(0) {
        for (auto& S : m_element->GetWeightVariations()) {
            m_element->ApplySystematic(S);
            m_Systematics.push_back(m_element->GetTreeVarReader());
        }
        m_element->ResetSystematic();
        m_nominal = m_element->GetTreeVarReader();
    }
    Long64_t NonZeroWeightSystCheck::rejected_events() const { return m_n_rejected; }
    bool NonZeroWeightSystCheck::nominal_nonZero() const { return IsFinite(m_nominal->read()) && m_nominal->read() != 0.; }
    bool NonZeroWeightSystCheck::systematics_nonZero() const {
        for (auto& S : m_Systematics) {
            if (IsFinite(S->read()) && S->read() != 0.) return true;
        }
        return false;
    }
    bool NonZeroWeightSystCheck::has_nonZeroComponents() {
        if (nominal_nonZero() || systematics_nonZero()) return true;
        ++m_n_rejected;
        return false;
    }
    std::string NonZeroWeightSystCheck::name() const { return m_element->name(); }
    NTupleWriter::NTupleWriter() :
        Analysis(),
        m_OutTree(nullptr),
        m_Branches(),
        m_wBranches(),
        m_WeightVar(),
        m_WeightNames(),
        m_isNominal(false),
        m_writtenEv(0),
        m_skippedEv(0) {
        DoCutFlow(false);
    }
    bool NTupleWriter::AnalyzeEvent() {
        if (!PassCuts(false)) return true;
        // If the nominal tree is not analyzed kick all events with zero weight
        if (m_weight->GetWeight() == 0.) {
            if (!m_isNominal) return true;
            // Check if at least one variation of the weights is non-zero
            bool HasNonZero = !m_WeightVar.empty();
            for (auto& Syst : m_WeightVar) HasNonZero &= Syst->has_nonZeroComponents();

            if (!HasNonZero) {
                ++m_skippedEv;
                return true;
            }
        }
        return FillTree();
    }
    std::string NTupleWriter::WeightBranchName(IWeightElement* element, const std::string variation) const {
        std::string weight_name = ReplaceExpInString(EraseWhiteSpaces(element->name()), " ", "");
        if (m_WeightNames.find(weight_name) != m_WeightNames.end()) weight_name = m_WeightNames.at(weight_name);
        if (!variation.empty() && !m_SystAppendix.empty()) weight_name += m_SystAppendix;
        if (!variation.empty()) weight_name += "_" + variation;
        for (const auto rename : m_ReplaceStringOfSyst) weight_name = ReplaceExpInString(weight_name, rename.first, rename.second);
        return weight_name;
    }
    bool NTupleWriter::initBranches(const std::string& Syst) {
        m_outFile->cd();
        std::string CurrentSyst = Syst;
        m_writtenEv = 0;
        m_isNominal = (Syst == NominalName());
        CurrentSyst = getFinalSystName(Syst);
        if (!CurrentSyst.empty()) { CurrentSyst = "_" + CurrentSyst; }
        std::string TreeName = name() + CurrentSyst;
        Info("NTupleWriter::initBranches()", "Create new output TTree " + TreeName);
        m_OutTree = std::make_shared<TTree>(TreeName.c_str(), "Flat NTuple for HistFitter");

        // Copy the standard weight branches to the tree
        std::vector<ITreeVarWriter*> br_to_add;
        br_to_add.insert(br_to_add.end(), m_Branches.begin(), m_Branches.end());

        // The NonZeroWeightSystCheck check whether all systematic variations of a given event are zero
        // In this case there is no chance for the event to enter in any region and it's rejected
        m_WeightVar.clear();
        // Clear the weight branches in case we've run over nominal before
        m_wBranches.clear();
        for (const auto& Weighters : m_weight->GetUsedWeighters()) {
            ITreeVarReader* nominal_reader = Weighters->GetTreeVarReader();
            if (nominal_reader == nullptr) {
                Error("NtupleWriter::initBranches()", Form("Cannot access the underlying ITreeVarReader of %s", Weighters->name().c_str()));
                return false;
            }
            ITreeVarWriter* w_branch_nominal = ScalarTreeVarWriter<double>::GetWriter(WeightBranchName(Weighters), nominal_reader);
            if (w_branch_nominal == nullptr) {
                Error("NtupleWriter::initBranches()", Form("Failed to write the event weight %s", Weighters->name().c_str()));
                return false;
            }
            m_wBranches.push_back(w_branch_nominal);
            if (m_isNominal) {
                // the variations are only written to the nominal tree
                m_WeightVar.push_back(std::make_shared<NonZeroWeightSystCheck>(Weighters));
                for (const auto& syst : Weighters->GetWeightVariations()) {
                    ITreeVarReader* variation_reader = Weighters->GetSystematicReader(syst);
                    if (variation_reader == nullptr) {
                        Error("NtupleWriter::initBranches()",
                              Form("Failed to access the systematic %s of %s", syst.c_str(), Weighters->name().c_str()));
                        return false;
                    }
                    ITreeVarWriter* w_branch_variation =
                        ScalarTreeVarWriter<double>::GetWriter(WeightBranchName(Weighters, syst), variation_reader);
                    if (w_branch_variation == nullptr) {
                        Error("NtupleWriter::initBranches()", Form("Failed to write the event weight %s", Weighters->name().c_str()));
                        return false;
                    }
                    m_wBranches.push_back(w_branch_variation);
                }
            }
            Weighters->ResetSystematic();
        }

        // Copy the weight branches to the output
        br_to_add.insert(br_to_add.end(), m_wBranches.begin(), m_wBranches.end());

        // Sort the branches by name
        std::sort(br_to_add.begin(), br_to_add.end(),
                  [](const ITreeVarWriter* a, const ITreeVarWriter* b) { return a->name() < b->name(); });

        for (auto b : br_to_add) {
            if (!b->init(m_OutTree.get())) return false;
        }

        // Simultaneous filling of the nominal systematics
        IWeightElement::FillSystSimul(m_isNominal);
        return true;
    }
    bool NTupleWriter::Process(long int nMax, long int nSkip) {
        DoCutFlow(false);
        return Analysis::Process(nMax, nSkip);
    }
    bool NTupleWriter::HandleWeightVariations() { return true; }
    void NTupleWriter::WriteOutput() {
        if (m_OutTree) {
            Info("NtupleWriter::WriteOutput()", Form("Tree %s successfully written with %llu events", m_OutTree->GetName(), m_writtenEv));
            if (m_skippedEv > 0) {
                Info("NtupleWriter::WriteOutput()", Form("%llu events were rejected due to zero weight", m_skippedEv));
                for (auto& sys : m_WeightVar) {
                    if (sys->rejected_events())
                        Info("NtupleWriter::WriteOutput()",
                             Form("    *** %s has weighted %llu events to zero", sys->name().c_str(), sys->rejected_events()));
                }
            }
            m_outFile->WriteObject(m_OutTree.get(), m_OutTree->GetName());
            m_OutTree.reset();
        }
    }
    bool NTupleWriter::FillTree() {
        for (const auto& B : m_Branches)
            if (!B->fill()) return false;
        for (const auto& B : m_wBranches)
            if (!B->fill()) return false;
        m_OutTree->Fill();
        ++m_writtenEv;
        return true;
    }
}  // namespace XAMPP
