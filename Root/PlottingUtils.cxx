#include <PathResolver/PathResolver.h>
#include <TH2Poly.h>
#include <XAMPPplotting/BkgEstimateHelpers.h>
#include <XAMPPplotting/Cuts.h>
#include <XAMPPplotting/FinalPlotHelpers.h>
#include <XAMPPplotting/Histo.h>
#include <XAMPPplotting/IfDefHelpers.h>
#include <XAMPPplotting/ParticleTagger.h>
#include <XAMPPplotting/PlottingUtils.h>
#include <XAMPPplotting/ReaderProvider.h>
#include <XAMPPplotting/ReconstructedParticleReaders.h>
#include <XAMPPplotting/TreeVarReader.h>
#include <XAMPPplotting/TreeVarWriter.h>
#include <XAMPPplotting/Weight.h>

#include <dirent.h>
#include <algorithm>
#include <cfloat>
#include <climits>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iomanip>  //for std::setw
#include <iostream>
#include <sstream>

namespace XAMPP {
    std::string LongestCommonString(const std::string &str1, const std::string &str2) {
        std::string common_string;
        if (str1.size() < str2.size()) return LongestCommonString(str2, str1);
        for (size_t l = 0; l <= str2.size(); ++l) {
            std::string sub_str = str2.substr(0, l);
            if (str1.find(sub_str) == 0) common_string = sub_str;
        }
        return common_string;
    }
    std::string RandomString(size_t length) {
        auto randchar = []() -> char {
            const char charset[] =
                "0123456789"
                "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                "abcdefghijklmnopqrstuvwxyz";
            const size_t max_index = (sizeof(charset) - 1);
            return charset[rand() % max_index];
        };
        std::string str(length, 0);
        std::generate_n(str.begin(), length, randchar);
        return str;
    }
    std::vector<std::string> GetPathResolvedFileList(const std::vector<std::string> &FileList) {
        std::vector<std::string> ResolvedList;
        for (auto &File : FileList) {
            std::string F = ResolvePath(File);
            Info("GetPathResolvedFileList()", "Add file " + F);
            if (!F.empty()) ResolvedList.push_back(F);
        }
        return ResolvedList;
    }
    std::string ResolvePath(const std::string &Config) {
        if (Config.empty()) {
            Error("ResolvePath()", "Path was given");
            return Config;
        }
        /// Assume that's an xrootd path
        else if (Config.find("root://") == 0) {
            return Config;
        }

        std::vector<std::string> Trials{ResolveEnviromentVariables(Config),
                                        // Try the PathResolver
                                        PathResolverFindCalibFile(ResolveEnviromentVariables(Config)),
                                        // TestArea
                                        ResolveEnviromentVariables("${TestArea}/" + Config),
                                        ResolveEnviromentVariables("${WorkDir_DIR}/data/" + Config),
                                        ResolveEnviromentVariables("${WorkDir_DIR}/data/" + ReplaceExpInString(Config, "data/", ""))};

        // The out of the box config does not work
        for (const auto &T : Trials) {
            std::ifstream Import;
            Import.open(T);
            if (Import.good()) { return ReplaceExpInString(T, "//", "/"); }
        }
        Trials.push_back(PathResolverFindCalibDirectory(ResolveEnviromentVariables(Config)));
        for (const auto &T : Trials) {
            if (DoesDirectoryExist(T)) { return ReplaceExpInString(T, "//", "/"); }
        }

        Error("ResolvePath()", "Could not find a valid config path " + Config);
        return std::string("");
    }
    std::string ReplaceExpInString(std::string str, const std::string &exp, const std::string &rep) {
        size_t ExpPos = str.find(exp);
        if (ExpPos == std::string::npos) return str;
        size_t ExpLen = exp.size();

        str = str.substr(0, ExpPos) + rep + str.substr(ExpPos + ExpLen, std::string::npos);
        if (str.find(exp) != std::string::npos) return ReplaceExpInString(str, exp, rep);
        return str;
    }
    bool IsFinite(double N) {
        if (std::isnan(N) || std::isinf(N)) return false;
        return true;
    }
    std::string GetWordFromStream(std::stringstream &sstr, bool Reset) {
        std::stringstream::pos_type Pos = sstr.tellg();
        std::string word;
        sstr >> word;
        if (Reset) {
            sstr.clear();
            sstr.seekg(Pos, sstr.beg);
        }
        return word;
    }
    int GetIntegerFromStream(std::stringstream &sstr, bool Reset) {
        std::stringstream::pos_type Pos = sstr.tellg();
        int integer;
        sstr >> integer;
        if (sstr.fail()) integer = INT_MIN;
        if (Reset) {
            sstr.clear();
            sstr.seekg(Pos, sstr.beg);
        }
        return integer;
    }
    float GetFloatFromStream(std::stringstream &sstr, bool Reset) {
        std::stringstream::pos_type Pos = sstr.tellg();
        float floating;
        sstr >> floating;
        if (sstr.fail()) floating = FLT_MIN;

        if (Reset) {
            sstr.clear();
            sstr.seekg(Pos, sstr.beg);
        }
        return floating;
    }
    double GetDoubleFromStream(std::stringstream &sstr, bool Reset) {
        std::stringstream::pos_type Pos = sstr.tellg();
        double db;
        sstr >> db;
        if (sstr.fail()) db = DBL_MIN;

        if (Reset) {
            sstr.clear();
            sstr.seekg(Pos, sstr.beg);
        }
        return db;
    }
    bool IsKeyWordSatisfied(std::stringstream &sstr, const std::string &word) {
        if (GetWordFromStream(sstr, true) != word) { return false; }
        GetWordFromStream(sstr);
        return true;
    }
    bool IsInNextWord(std::stringstream &sstr, const std::string &word) {
        if (GetWordFromStream(sstr, true).find(word) == std::string::npos) { return false; }
        GetWordFromStream(sstr);
        return true;
    }
    void ExtractListFromStream(std::stringstream &sstr, std::vector<std::string> &List, bool Clear) {
        std::string word;
        if (Clear) { List.clear(); }
        while (sstr >> word) {
            if (word.find("#") == 0) break;
            if (!IsElementInList(List, word)) List.push_back(word);
        }
    }
    void ExtractListFromStream(std::stringstream &sstr, std::vector<int> &List, bool ClearList) {
        if (ClearList) { List.clear(); }
        int Integer;
        while (sstr >> Integer) {
            if (IsKeyWordSatisfied(sstr, "-")) {
                int UpperInt = GetIntegerFromStream(sstr);
                Info("ExtractListFromStream()", Form("Going to add range %i - %i to the List", Integer, UpperInt));
                for (int i = UpperInt; i >= Integer; --i) { List.push_back(i); }
            } else if (!IsElementInList(List, Integer)) {
                List.push_back(Integer);
            }
        }
    }
    void ExtractListFromStream(std::stringstream &sstr, std::vector<float> &List, bool ClearList) {
        if (ClearList) { List.clear(); }
        float Float;
        while (sstr >> Float) {
            if (!IsElementInList(List, Float)) { List.push_back(Float); }
        }
    }

    void PrintHeadLine(const std::string &Title) {
        std::cout << std::endl << SeparationLine() << std::endl;
        std::cout << WhiteSpaces(SeparationLine().size() > Title.size() ? (SeparationLine().size() - Title.size()) / 2 : 0) << Title
                  << std::endl;
        std::cout << SeparationLine() << std::endl;
    }
    std::string SeparationLine() {
        return "###########################################################################################################################"
               "#########";
    }
    void PrintFooter() { std::cout << SeparationLine() << std::endl << SeparationLine() << std::endl << std::endl; }
    std::string WhiteSpaces(int N, std::string space) {
        std::string White;
        for (int n = 0; n < N; ++n) White += space;
        return White;
    }
    std::string TimeHMS(float t) {
        std::stringstream ostr;
        ostr << std::setw(2) << std::setfill('0') << (int)((t / 60. / 60.)) % 24 << ":" << std::setw(2) << std::setfill('0')
             << ((int)(t / 60.)) % 60 << ":" << std::setw(2) << std::setfill('0') << ((int)t) % 60;
        return ostr.str();
    }
    bool GetLine(std::ifstream &inf, std::string &line) {
        if (!std::getline(inf, line)) return false;
        line = EraseWhiteSpaces(line);
        if (line.find("#") == 0 || line.size() < 1) return GetLine(inf, line);
        return true;
    }
    std::string EraseWhiteSpaces(std::string str) {
        str.erase(std::remove(str.begin(), str.end(), '\t'), str.end());
        while (!str.empty() && std::find_if(str.begin(), str.end(), [](unsigned char c) { return std::isspace(c); }) == str.begin())
            str = str.substr(1, std::string::npos);
        while (!str.empty() && std::find_if(str.rbegin(), str.rend(), [](unsigned char c) { return std::isspace(c); }) == str.rbegin())
            str = str.substr(0, str.size() - 1);

        return str;
    }
    void Info(const std::string &Method, const std::string &Message) {
        unsigned int l = Method.size();
        std::cout << Method.substr(0, l < MsgMethodChars ? l : MsgMethodChars) << WhiteSpaces(MsgMethodChars - l) << " -- INFO: " << Message
                  << std::endl;
    }
    void Error(const std::string &Method, const std::string &Message) {
        unsigned int l = Method.size();
        std::cout << Method.substr(0, l < MsgMethodChars ? l : MsgMethodChars) << WhiteSpaces(MsgMethodChars - l)
                  << " -- ERROR: " << Message << std::endl;
    }
    void Warning(const std::string &Method, const std::string &Message) {
        unsigned int l = Method.size();
        std::cout << Method.substr(0, l < MsgMethodChars ? l : MsgMethodChars) << WhiteSpaces(MsgMethodChars - l)
                  << " -- WARNING: " << Message << std::endl;
    }
    std::shared_ptr<Condition> CreateCut(const std::string &line, std::vector<ITreeVarReader *> &Readers) {
        std::stringstream sstr(line.substr(0, line.rfind("Cut")) + "Reader" + line.substr(line.rfind("Cut") + 3, line.size()));
        ITreeVarReader *R = ReaderProvider::GetInstance()->CreateReader(sstr);
        if (!R) return nullptr;
        Readers.push_back(R);
        return CreateCut(R, sstr);
    }
    std::shared_ptr<Condition> CreateCut(ITreeVarReader *Var, std::stringstream &sstr) {
        std::string Cond(GetWordFromStream(sstr));
        std::stringstream::pos_type CutValuePos = sstr.tellg();

        double CutValue(GetDoubleFromStream(sstr));
        std::shared_ptr<Condition> NewCut;
        if (Cond == "<")
            NewCut = std::make_shared<LessCondition>();
        else if (Cond == "<=")
            NewCut = std::make_shared<LessEqualCondition>();
        else if (Cond == "=")
            NewCut = std::make_shared<EqualCondition>();
        else if (Cond == ">")
            NewCut = std::make_shared<GreaterCondition>();
        else if (Cond == ">=")
            NewCut = std::make_shared<GreaterEqualCondition>();
        else if (Cond == "!=")
            NewCut = std::make_shared<NotEqualCondition>();
        else if (Cond == "|>|")
            NewCut = std::make_shared<AbsGreaterCondition>();
        else if (Cond == "|>=|")
            NewCut = std::make_shared<AbsGreaterEqualCondition>();
        else if (Cond == "|<|")
            NewCut = std::make_shared<AbsLessCondition>();
        else if (Cond == "|<=|")
            NewCut = std::make_shared<AbsLessEqualCondition>();
        else if (Cond == "|=|")
            NewCut = std::make_shared<AbsEqualCondition>();
        else if (Cond == "|!=|")
            NewCut = std::make_shared<AbsNotEqualCondition>();
        else if (Cond == "isTagged" || Cond == "isNotTagged") {
            IParticleReader *particle = dynamic_cast<IParticleReader *>(Var);
            if (particle == nullptr) {
                Error("CreateCut()", "Only particles  can be used for tagging");
                return nullptr;
            }
            sstr.clear();
            sstr.seekg(CutValuePos, sstr.beg);
            std::string tagger_name = GetWordFromStream(sstr);
            if (tagger_name.empty()) {
                Error("CreateCut()", "Please provide a tagger name");
                return nullptr;
            }
            std::shared_ptr<Condition> C = std::make_shared<ContainerTaggerCut>(particle, tagger_name);
            if (Cond == "isNotTagged") C = std::make_shared<ConditionNOT>(C);
            return C;
        } else {
            Error(
                "CreateCut()",
                "The given Condition '" + Cond +
                    "' is unknown. Allowed conditions are either '<' '<=' , '=' , '>', '>=', '!=', '|>|', '|>=|', '|<|', '|<=|' or '|=|'.");
            return std::shared_ptr<Condition>();
        }
        if (NewCut->Init(Var, CutValue)) {
            Info("CreateCut()", "Add new cut " + Var->name() + " " + Cond + " " + std::to_string(CutValue));
            return NewCut;
        }
        Error("CreateCut()", "Could not add new cut " + Var->name() + " " + Cond + " " + std::to_string(CutValue));
        return std::shared_ptr<Condition>();
    }

    Condition::Mode GetCombineModeFromLine(const std::string &line) {
        if (line.find("NOTAND") != std::string::npos) {
            return Condition::Mode::NOTAND;
        } else if (line.find("NOTOR") != std::string::npos) {
            return Condition::Mode::NOTOR;
        } else if (line.find("AND") != std::string::npos) {
            return Condition::Mode::AND;
        } else if (line.find("XOR") != std::string::npos) {
            //  return Condition::Mode::XOR;
        } else if (line.find("OR") != std::string::npos) {
            return Condition::Mode::OR;
        }
        return Condition::Mode::inDef;
    }
    std::shared_ptr<Condition> CreateCombinedCut(std::ifstream &inf, std::vector<ITreeVarReader *> &Readers, const std::string &line) {
        return CreateCombinedCut(inf, Readers, GetCombineModeFromLine(line));
    }
    std::shared_ptr<Condition> CreateCombinedCut(std::ifstream &inf, std::vector<ITreeVarReader *> &Readers, Condition::Mode M) {
        std::shared_ptr<Condition> myCut;
        std::string line;
        Condition::Mode OrigMode = M;
        // !( A && B && C ) = ( !A || !B || !C)
        // (A && !B && !C) || (!A && B && !C) || (!A && !B && C)
        if (M == Condition::Mode::NOTAND) {
            M = Condition::Mode::AND;
        } else if (M == Condition::Mode::NOTOR) {
            M = Condition::Mode::OR;
        }
        IfDefLineParser Parser;
        while (Parser(inf, line) == IfDefLineParser::NewProperty) {
            if (line.find("End_CombCut") != std::string::npos) {
                if (myCut == nullptr)
                    Error("CreateCombinedCut()", "No cut has been defined.");
                else if (OrigMode == Condition::Mode::NOTAND) {
                    return std::make_shared<ConditionNOT>(myCut);
                }
                return myCut;
            }
            std::shared_ptr<Condition> C;
            // A new combined cut is defined
            if (line.find("Cut") == std::string::npos)
                continue;
            else if (line.find("CombCut") == 0)
                C = CreateCombinedCut(inf, Readers, line);
            else
                C = CreateCut(line, Readers);
            if (!C) {
                return std::shared_ptr<Condition>();
            } else if (!myCut && C)
                myCut = C;
            else if (C)
                myCut = Condition::Combine(myCut, C, M);
        }
        Error("CreateCombinedCut()", "Missing 'End_CombCut' statement in the config file");
        return std::shared_ptr<Condition>();
    }
    std::shared_ptr<Histo> CreateHisto(std::ifstream &inf, std::vector<ITreeVarReader *> &Readers) {
        std::shared_ptr<Histo> H;
        std::shared_ptr<Condition> C;
        std::string line(""), Template(""), Name(""), xLabel(""), yLabel(""), zLabel(""), Type("");
        std::map<int, std::string> BinXlabels;
        std::map<int, std::string> BinYlabels;
        std::map<int, std::string> BinZlabels;

        bool FoundEndVar = false;
        bool doRaw = false;
        bool PullOverFlow = true;
        bool PullUnderFlow = true;
        bool NormBins = true;
        bool DivideByBinWidth = false;
        std::string entriesLabel("Entries"), unitsLabel("");
        ITreeVarReader *xR = nullptr;
        ITreeVarReader *yR = nullptr;
        ITreeVarReader *zR = nullptr;
        IfDefLineParser Parser;
        while (!FoundEndVar && Parser(inf, line) == IfDefLineParser::NewProperty) {
            ITreeVarReader *R = nullptr;
            std::stringstream sstr(line);
            std::string word;
            sstr >> word;
            if (word == "EndVar")
                FoundEndVar = true;
            else if (word == "noWeighting")
                doRaw = true;
            else if (word == "Type")
                sstr >> Type;
            else if (word == "Template")
                sstr >> Template;
            else if (word == "Name")
                sstr >> Name;
            else if (word == "xLabel")
                xLabel = line.substr(line.find(word) + word.size(), line.size());
            else if (word == "yLabel")
                yLabel = line.substr(line.find(word) + word.size(), line.size());
            else if (word == "zLabel")
                zLabel = line.substr(line.find(word) + word.size(), line.size());
            else if (word == "noOverFlowPull")
                PullOverFlow = false;
            else if (word == "noUnderFlowPull")
                PullUnderFlow = false;
            else if (word == "noBinNormalization")
                NormBins = false;
            else if (word == "DivideByBinWidth")
                DivideByBinWidth = true;
            else if (word == "EntriesLabel")
                entriesLabel = line.substr(line.find(word) + word.size(), line.size());
            else if (word == "UnitsLabel")
                unitsLabel = line.substr(line.find(word) + word.size(), line.size());
            else if (word == "BinXLabel" || word == "BinYLabel" || word == "BinZLabel") {
                int Bin(0);
                std::string Label("");
                sstr >> Bin >> Label;
                line = line.substr(word.size() + 1, line.size());
                Label = line.substr(line.find(Label), line.size());
                std::map<int, std::string> &BinLabel = word == "BinXLabel" ? BinXlabels : BinYlabels;
                if (BinLabel.find(Bin) == BinLabel.end())
                    BinLabel.insert(std::pair<int, std::string>(Bin, Label));
                else
                    BinLabel.at(Bin) = Label;
            } else if (word.find("Reader") < word.size()) {
                R = word.find("New_") == std::string::npos ? ReaderProvider::GetInstance()->CreateReader(line)
                                                           : ReaderProvider::GetInstance()->CreateReader(inf, line);
                if (!R) return std::shared_ptr<Histo>();
            } else if (word.find("Cut") < word.size()) {
                if (C) {
                    Error("CreateHisto()",
                          "There exists already an condition " + C->name() + ". No idea how to combine, please use 'CombCut'");
                    return std::shared_ptr<Histo>();
                }
                if (word == "CombCut")
                    C = CreateCombinedCut(inf, Readers, line);
                else
                    C = CreateCut(line, Readers);
                if (C == nullptr) {
                    Error("CreateHisto()", "You want to apply a cut for good reason. What is it?");
                    return std::shared_ptr<Histo>();
                }
            }
            if (R) {
                if (!xR)
                    xR = R;
                else if (!yR)
                    yR = R;
                else if (!zR)
                    zR = R;
                else
                    Warning("CreateHisto()",
                            "Could not determine whether the reader " + R->name() + " is meant for the x-, y- or z-variable");
            }
        }
        if (FoundEndVar && xR) {
            Readers.push_back(xR);
            if (yR) Readers.push_back(yR);
            if (zR) Readers.push_back(zR);

            if (Type == "1D")
                H = std::make_shared<Histo>(xR, Name, Template);
            else if (Type == "2D")
                H = std::make_shared<Histo2D>(xR, yR, Name, Template);
            else if (Type == "3D")
                H = std::make_shared<Histo3D>(xR, yR, zR, Name, Template);
            else if (Type == "Cul1D")
                H = std::make_shared<CumulativeHisto>(xR, Name, Template);
            else if (Type == "Cul2D")
                H = std::make_shared<CumulativeHisto2D>(xR, yR, Name, Template);
            else if (Type == "RCl1D")
                H = std::make_shared<RevCumulativeHisto>(xR, Name, Template);
            else {
                Error("CreateHisto()", "Unknown Histo type '" + Type + "'. Allowed types are '1D', '2D', '3D', 'Cul1D', 'Cul2D', 'RCl1D'");
            }
            if (H) {
                H->SetCut(C);
                H->SetXaxisLabel(xLabel);
                H->DoRaw(doRaw);
                H->PullOverFlowBins(PullOverFlow);
                H->PullUnderFlowBins(PullUnderFlow);
                H->SetNormToFirstBin(NormBins);
                H->SetDivideByBinWidth(DivideByBinWidth);
                H->SetEntriesLabel(entriesLabel);
                H->SetUnitsLabel(unitsLabel);
                if (!yLabel.empty()) H->SetYaxisLabel(yLabel);
                if (!zLabel.empty()) H->SetZaxisLabel(zLabel);
                for (const auto &BinLabel : BinXlabels) H->SetBinLabelX(BinLabel.first, BinLabel.second);
                for (const auto &BinLabel : BinYlabels) H->SetBinLabelY(BinLabel.first, BinLabel.second);
                for (const auto &BinLabel : BinZlabels) H->SetBinLabelZ(BinLabel.first, BinLabel.second);
                Info("CreateHisto()", "New histogram " + H->name() + " using " + H->GetTemplate() + ".");
                return H;
            }
        }
        if (!xR)
            Error("CreateHisto()", "No reader found");
        else
            Error("CreateHisto()", "Missing the 'EndVar' statement in the config file");
        return std::shared_ptr<Histo>();
    }
    bool DoesDirectoryExist(const std::string &Path) {
        DIR *dir = opendir(Path.c_str());
        bool Exist = dir != nullptr;
        if (Exist) closedir(dir);
        return Exist;
    }
    std::vector<std::string> ListDirectory(std::string Path, std::string WildCard) {
        std::vector<std::string> List;
        DIR *dir;
        dirent *ent;
        Info("ListDirectory()", "Read content of directory " + Path);
        if ((dir = opendir(Path.c_str())) != nullptr) {
            while ((ent = readdir(dir)) != nullptr) {
                std::string Entry = ent->d_name;
                if (Entry == "." || Entry == "..") continue;  // skip the .. and . lines in ls
                if (WildCard.size() == 0 || Entry.find(WildCard) < Entry.size()) List.push_back(Path + "/" + Entry);
            }
            closedir(dir);
        } else
            Error("ListDirectory()", "No such file or directory " + Path);

        return List;
    }
    bool DoesFileExist(const std::string &Path) { return std::ifstream(Path).is_open(); }
    ITreeVarWriter *CreateOutBranch(std::ifstream &inf, std::vector<ITreeVarReader *> &Readers) {
        std::string line(""), Name(""), Type("");
        ITreeVarReader *R = nullptr;
        IfDefLineParser Parser;
        while (Parser(inf, line) == IfDefLineParser::NewProperty) {
            if (line.find("End_Branch") < line.size()) {
                if (R) {
                    Readers.push_back(R);
                    if (Type == "int")
                        return ScalarTreeVarWriter<int>::GetWriter(Name, R);
                    else if (Type == "uint")
                        return ScalarTreeVarWriter<unsigned int>::GetWriter(Name, R);
                    else if (Type == "llu")
                        return ScalarTreeVarWriter<unsigned long long>::GetWriter(Name, R);
                    else if (Type == "float")
                        return ScalarTreeVarWriter<float>::GetWriter(Name, R);
                    else if (Type == "double")
                        return ScalarTreeVarWriter<double>::GetWriter(Name, R);
                    else if (Type == "char")
                        return ScalarTreeVarWriter<char>::GetWriter(Name, R);
                    else if (Type == "bool")
                        return ScalarTreeVarWriter<bool>::GetWriter(Name, R);
                    else if (Type == "intVec")
                        return VectorTreeVarWriter<int>::GetWriter(Name, R);
                    else if (Type == "floatVec")
                        return VectorTreeVarWriter<float>::GetWriter(Name, R);
                    else if (Type == "doubleVec")
                        return VectorTreeVarWriter<double>::GetWriter(Name, R);
                    else if (Type == "charVec")
                        return VectorTreeVarWriter<char>::GetWriter(Name, R);
                    else if (Type == "boolVec")
                        return VectorTreeVarWriter<bool>::GetWriter(Name, R);
                    else if (Type == "uintVec")
                        return VectorTreeVarWriter<unsigned int>::GetWriter(Name, R);
                    Error("CreateOutBranch()", "The type " + Type + " is unknown.");
                    return nullptr;
                } else {
                    Error("CreateOutBranch()", "No ITreeVarReader has been found");
                    return nullptr;
                }
            }
            std::stringstream sstr(line);
            std::string word(GetWordFromStream(sstr));
            if (word.find("Reader") != std::string::npos) {
                R = word.find("New_") == std::string::npos ? ReaderProvider::GetInstance()->CreateReader(line)
                                                           : ReaderProvider::GetInstance()->CreateReader(inf, sstr);
                if (!R) return nullptr;
            } else if (word == "Type")
                sstr >> Type;
            else if (word == "Name") {
                Name = GetWordFromStream(sstr);
            }
        }
        Error("CreateOutBranch()", "Missing 'End_Branch' statement");
        return nullptr;
    }
    std::string ResolveEnviromentVariables(std::string str) {
        while (str.find("${") != std::string::npos) {
            std::string VarName = str.substr(str.find("${") + 2, str.find("}") - str.find("${") - 2);
            std::string EnvVar(std::getenv(VarName.c_str()) ? std::getenv(VarName.c_str()) : "");
            str = ReplaceExpInString(str, str.substr(str.find("${"), str.find("}") - str.find("${") + 1), EnvVar);
        }
        return str;
    }
    int gcd(int a, int b) { return b == 0 ? a : gcd(b, a % b); }
    Cube GetMatrixRangesToRead(std::string &Instr) {
        Ranges x(-1, -1), y(-1, -1);
        Cube cub(x, y);

        // Checck if the string is of format MyStr[0] or MyStr[0-1] or MyStr[0][0-2] MyStr[0-3][0-1]
        size_t bracket_open = Instr.find("[");
        size_t bracket_open_back = Instr.rfind("[");

        size_t bracket_close = Instr.find("]");
        size_t bracket_close_back = Instr.rfind("]");

        if (bracket_open == std::string::npos) return cub;
        std::string xInd(""), yInd("");

        // MyStr[0] is the only possibility
        if (bracket_open == bracket_open_back) {
            xInd = Instr.substr(bracket_open + 1, bracket_close - (bracket_open + 1));
        }
        // We've something like [1-2][3] [0][1-2]
        else {
            xInd = Instr.substr(bracket_open + 1, bracket_close - (bracket_open + 1));
            yInd = Instr.substr(bracket_open_back + 1, bracket_close_back - (bracket_open_back + 1));
        }
        // Remove the brackets from the expression
        Instr = Instr.substr(0, bracket_open);
        cub.first = GetRangesFromStr(xInd);
        cub.second = GetRangesFromStr(yInd);
        return cub;
    }
    std::string GetMatrixRangesFromCube(Cube C) {
        return "[" + std::to_string(C.first.first) + "-" + std::to_string(C.first.second) + "][" + std::to_string(C.second.first) + "-" +
               std::to_string(C.second.second) + "]";
    }
    Ranges GetRangesFromStr(std::string str) {
        Ranges Rng(-1, -1);
        std::size_t Pos = str.find("-");
        if (str.empty()) return Rng;
        if (Pos == std::string::npos)
            Rng.first = Rng.second = atoi(str.c_str());
        else {
            Rng.first = atoi(str.substr(0, Pos).c_str());
            Rng.second = atoi(str.substr(Pos + 1, str.size()).c_str());
        }
        return Rng;
    }
    void PrintHistogram(TH1 *H) {
        if (H == nullptr) {
            Warning("PrintHistogram()", "No histogram is given");
            return;
        }
        // 1D Histogram
        PrintHeadLine(Form("Print content of %s", H->GetTitle()));
        if (H->GetDimension() == 1) {
            Info("PrintHistogram()", Form("x axis: %s", H->GetXaxis()->GetTitle()));
            for (int b = 0; b <= H->GetNbinsX() + 1; ++b) {
                Info("PrintHistogram()", Form("Bin %d (%.3f - %.3f): %.3f", b, H->GetXaxis()->GetBinLowEdge(b),
                                              H->GetXaxis()->GetBinUpEdge(b), H->GetBinContent(b)));
            }
        } else if (H->GetDimension() == 2) {
            std::vector<size_t> max_widths(H->GetXaxis()->GetNbins() + 1, 0);

            std::vector<std::string> cells;

            std::function<void(const std::string &)> add_cell = [&max_widths, &cells](const std::string &str) {
                size_t col = cells.size() % max_widths.size();
                max_widths[col] = std::max(str.size(), max_widths[col]);
                cells.push_back(str);
            };
            add_cell(Form("%s vs. %s", EraseWhiteSpaces(H->GetXaxis()->GetTitle()).c_str(),
                          EraseWhiteSpaces(H->GetYaxis()->GetTitle()).c_str()));
            for (int x = 1; x <= H->GetXaxis()->GetNbins(); ++x) {
                add_cell(Form("%.3f -- %.3f", H->GetXaxis()->GetBinLowEdge(x), H->GetXaxis()->GetBinUpEdge(x)));
            }
            for (int y = 1; y <= H->GetYaxis()->GetNbins(); ++y) {
                add_cell(Form("%.3f -- %.3f", H->GetYaxis()->GetBinLowEdge(y), H->GetYaxis()->GetBinUpEdge(y)));
                for (int x = 1; x <= H->GetXaxis()->GetNbins(); ++x) {
                    add_cell(Form("%.3f #pm %.3f", H->GetBinContent(x, y), H->GetBinError(x, y)));
                }
            }
            std::function<std::string(size_t)> print_white_left = [&max_widths, &cells](size_t e) {
                size_t L = (max_widths[e % max_widths.size()] - cells[e].size());
                return WhiteSpaces((L - L % 2) / 2);
            };
            std::function<std::string(size_t)> print_white_right = [&max_widths, &cells](size_t e) {
                size_t L = (max_widths[e % max_widths.size()] - cells[e].size());
                return WhiteSpaces((L + L % 2) / 2);
            };
            for (size_t e = 0; e < cells.size(); ++e) {
                std::cout << print_white_left(e) << cells[e] << print_white_right(e);
                if (e % max_widths.size() == max_widths.size() - 1)
                    std::cout << std::endl;
                else if (e % max_widths.size() == 0)
                    std::cout << " || ";
                else
                    std::cout << " | ";
            }
            std::cout << std::endl;
        }
        PrintFooter();
    }
    unsigned int GetNbins(const std::shared_ptr<TH1> &Histo) { return GetNbins(Histo.get()); }
    unsigned int GetNbins(const TH1 *Histo) {
        if (Histo == nullptr) return 0;
        if (Histo->GetDimension() == 2) {
            const TH2Poly *poly = dynamic_cast<const TH2Poly *>(Histo);
            if (poly != nullptr) return poly->GetNumberOfBins() + 1;
        }
        unsigned int N = Histo->GetNbinsX() + 2;
        if (Histo->GetDimension() >= 2) N *= (Histo->GetNbinsY() + 2);
        if (Histo->GetDimension() == 3) N *= (Histo->GetNbinsZ() + 2);
        return N;
    }
    double Integrate(const std::shared_ptr<TH1> &Histo, unsigned int xStart, int xEnd, unsigned int yStart, int yEnd, unsigned int zStart,
                     int zEnd) {
        return Integrate(Histo.get(), xStart, xEnd, yStart, yEnd, zStart, zEnd);
    }
    double Integrate(const TH1 *Histo, unsigned int xStart, int xEnd, unsigned int yStart, int yEnd, unsigned int zStart, int zEnd) {
        return IntegrateAndError(Histo, xStart, xEnd, yStart, yEnd, zStart, zEnd).first;
    }

    std::pair<double, double> IntegrateAndError(const std::shared_ptr<TH1> &Histo, unsigned int xStart, int xEnd, unsigned int yStart,
                                                int yEnd, unsigned int zStart, int zEnd) {
        return IntegrateAndError(Histo.get(), xStart, xEnd, yStart, yEnd, zStart, zEnd);
    }
    std::pair<double, double> IntegrateAndError(const TH1 *Histo, unsigned int xStart, int xEnd, unsigned int yStart, int yEnd,
                                                unsigned int zStart, int zEnd) {
        if (Histo == nullptr) return std::pair<double, double>(std::nan("1"), std::nan("1"));
        double integral(0), error(0);
        if (xEnd == -1) xEnd = Histo->GetNbinsX() + 2;
        if (yEnd == -1 && Histo->GetDimension() >= 2) yEnd = Histo->GetNbinsY() + 2;
        if (zEnd == -1 && Histo->GetDimension() == 3) zEnd = Histo->GetNbinsZ() + 2;

        for (int x = xStart; x < xEnd; ++x) {
            double xW = Histo->GetXaxis()->GetBinWidth(x) / Histo->GetXaxis()->GetBinWidth(1);
            if (Histo->GetDimension() == 1) {
                integral += Histo->GetBinContent(x) * xW;
                error += std::pow(Histo->GetBinError(x) * xW, 2);
            } else {
                for (int y = yStart; y < yEnd; ++y) {
                    double yW = xW * (Histo->GetYaxis()->GetBinWidth(y) / Histo->GetYaxis()->GetBinWidth(1));
                    if (Histo->GetDimension() == 2) {
                        integral += Histo->GetBinContent(x, y) * yW;
                        error += std::pow(Histo->GetBinError(x, y) * yW, 2);
                    } else {
                        for (int z = zStart; z < zEnd; ++z) {
                            double zW = yW * (Histo->GetZaxis()->GetBinWidth(z) / Histo->GetZaxis()->GetBinWidth(1));
                            integral += Histo->GetBinContent(x, y, z) * zW;
                            error += std::pow(Histo->GetBinError(x, y, z) * zW, 2);
                        }
                    }
                }
            }
        }
        return std::pair<double, double>(integral, std::sqrt(error));
    }
    double GetMinimum(const std::shared_ptr<TH1> &histo, double min_value) { return GetMinimum(histo.get(), min_value); }
    double GetMinimum(const TH1 *histo, double min_value) {
        double min = DBL_MAX;
        for (int n = GetNbins(histo); n > 0; --n) {
            if (histo->GetBinContent(n) < min && histo->GetBinContent(n) >= min_value) min = histo->GetBinContent(n);
        }
        return min;
    }
    double GetMaximum(const std::shared_ptr<TH1> &histo, double max_value) { return GetMaximum(histo.get(), max_value); }
    double GetMaximum(const TH1 *histo, double max_value) {
        double max = DBL_MIN;
        for (int n = GetNbins(histo); n > 0; --n) {
            if (histo->GetBinContent(n) > max && histo->GetBinContent(n) <= max_value) max = histo->GetBinContent(n);
        }
        return max;
    }
    int GetMinimumBin(const std::shared_ptr<TH1> &histo, double min_value) { return GetMinimumBin(histo.get(), min_value); }
    int GetMinimumBin(const TH1 *histo, double min_value) {
        double min = DBL_MAX;
        int min_bin = -1;
        for (int n = GetNbins(histo); n > 0; --n) {
            if (histo->GetBinContent(n) < min && histo->GetBinContent(n) >= min_value) {
                min = histo->GetBinContent(n);
                min_bin = n;
            }
        }
        return min_bin;
    }
    int GetMaximumBin(const std::shared_ptr<TH1> &histo, double max_value) { return GetMaximumBin(histo.get(), max_value); }
    int GetMaximumBin(const TH1 *histo, double max_value) {
        double max = -DBL_MAX;
        int max_bin = -1;
        for (int n = GetNbins(histo); n > 0; --n) {
            if (histo->GetBinContent(n) > max && histo->GetBinContent(n) <= max_value) {
                max = histo->GetBinContent(n);
                max_bin = n;
            }
        }
        return max_bin;
    }
    std::shared_ptr<TH1> clone(const std::shared_ptr<TH1> &histo, bool reset) { return clone(histo.get(), reset); }
    std::shared_ptr<TH1> clone(const TH1 *histo, bool reset) {
        if (histo == nullptr) return std::shared_ptr<TH1>();
        std::shared_ptr<TH1> cloned(dynamic_cast<TH1 *>(histo->Clone(RandomString(85).c_str())));
        if (reset) cloned->Reset();
        cloned->SetDirectory(0);
        return cloned;
    }
    double truncate(const double &dbl, unsigned int prec) {
        double prec_power = std::pow(10, prec);
        double truncated = std::trunc(dbl * prec_power);
        return truncated / prec_power;
    }
    double GetBinNormalization(const TH1 *H1, int Bin) {
        int x(-1), y(-1), z(-1);
        H1->GetBinXYZ(Bin, x, y, z);
        double norm = H1->GetXaxis()->GetBinWidth(1) / H1->GetXaxis()->GetBinWidth(x);

        if (H1->GetDimension() >= 2) { norm *= H1->GetYaxis()->GetBinWidth(1) / H1->GetYaxis()->GetBinWidth(y); }
        if (H1->GetDimension() == 3) { norm *= H1->GetZaxis()->GetBinWidth(1) / H1->GetZaxis()->GetBinWidth(z); }
        return norm;
    }
    double GetBinNormalization(const std::shared_ptr<TH1> &H1, int Bin) { return GetBinNormalization(H1.get(), Bin); }

    std::shared_ptr<TFile> Open(const std::string &Path) {
        std::shared_ptr<TFile> File(TFile::Open(ResolvePath(Path).c_str(), "READ"));
        if (!File || !File->IsOpen()) {
            Error("TFile::Open()", "Could not open file");
            return std::shared_ptr<TFile>();
        }
        return File;
    }
    std::shared_ptr<TH1> ProjectInto1D(const std::shared_ptr<TH1> &H3, unsigned int project_along, int bin1_start, int bin1_end,
                                       int bin2_start, int bin2_end) {
        return ProjectInto1D(H3.get(), project_along, bin1_start, bin1_end, bin2_start, bin2_end);
    }
    std::shared_ptr<TH1> ProjectInto1D(const TH1 *H3, unsigned int project_along, int bin1_start, int bin1_end, int bin2_start,
                                       int bin2_end) {
        const TAxis *axis_to_project = nullptr;

        if (project_along >= 3 || H3->GetDimension() < 2 || (project_along == 2 && H3->GetDimension() == 2)) {
            Error("ProjectInto1D()", Form("You're a very funny guy. Where is my %d dimension>?", project_along + 1));
            return std::shared_ptr<TH1>();
        } else if (project_along == 2) {
            axis_to_project = H3->GetZaxis();
        } else if (project_along == 1) {
            axis_to_project = H3->GetYaxis();
        } else {
            axis_to_project = H3->GetXaxis();
        }
        std::vector<double> bins_x = ExtractBinning(axis_to_project);

        std::shared_ptr<TH1> new_histo = std::make_shared<TH1D>(RandomString(25).c_str(), "Projection", bins_x.size() - 1, bins_x.data());
        for (int to = 0; to <= axis_to_project->GetNbins() + 1; ++to) {
            std::pair<double, double> integral;
            if (project_along == 2) {
                integral = IntegrateAndError(H3, bin1_start, bin1_end + 1, bin2_start, bin2_end + 1, to, to + 1);
            } else if (project_along == 1) {
                integral = IntegrateAndError(H3, bin1_start, bin1_end + 1, to, to + 1, bin2_start, bin2_end + 1);
            } else if (project_along == 0) {
                integral = IntegrateAndError(H3, to, to + 1, bin1_start, bin1_end + 1, bin2_start, bin2_end + 1);
            }
            new_histo->SetBinContent(to, integral.first * GetBinNormalization(new_histo, to));
            new_histo->SetBinError(to, integral.second * GetBinNormalization(new_histo, to));
        }
        return new_histo;
    }
    bool IsFinite(const std::shared_ptr<TH1> &histo) { return IsFinite(histo.get()); }
    bool IsFinite(const TH1 *histo) {
        if (!histo) {
            Warning("IsFinite()", "No histogram given");
            return false;
        }
        for (unsigned int i = 0; i <= GetNbins(histo); ++i) {
            if (!IsFinite(histo->GetBinContent(i))) return false;
        }
        return true;
    }

    unsigned int binomial(unsigned int n, unsigned int k) {
        std::vector<unsigned int> C(k + 1, 0);
        C[0] = 1;  // nC0 is 1
        for (unsigned int i = 1; i <= n; i++) {
            // Compute next row of pascal triangle using
            // the previous row
            for (unsigned int j = std::min(i, k); j > 0; j--) C[j] = C[j] + C[j - 1];
        }
        return C[k];
    }
    std::vector<size_t> sequence(size_t to, size_t start) {
        std::vector<size_t> v;
        v.reserve(to - start);
        size_t n(start);
        std::generate_n(std::back_inserter(v), to - start, [n]() mutable { return n++; });
        return v;
    }

    std::shared_ptr<TH1> ReBinHisto(const std::shared_ptr<TH1> &H, double max_stat_error) { return ReBinHisto(H.get(), max_stat_error); }
    std::shared_ptr<TH1> ReBinHisto(const TH1 *H, double max_stat_error) {
        if (!H) {
            Error("ReBinHisto()", "No histogram given");
            return std::shared_ptr<TH1>();
        }
        std::pair<double, double> int_err = IntegrateAndError(H);
        if (int_err.second / int_err.first > max_stat_error) {
            Warning("ReBinHisto()", Form("The full distribution already has an relative error of %f exceeding %f",
                                         int_err.second / int_err.first, max_stat_error));
        }
        HistogramRebinning binning(H);
        return binning.rebin(max_stat_error);
    }
    void CopyStyling(const TH1 *from, TH1 *to) {
        to->GetXaxis()->SetTitle(from->GetXaxis()->GetTitle());
        to->GetYaxis()->SetTitle(from->GetYaxis()->GetTitle());
        to->GetZaxis()->SetTitle(from->GetZaxis()->GetTitle());
        to->SetTitle(from->GetTitle());
        to->SetLineColor(from->GetLineColor());
        to->SetLineStyle(from->GetLineStyle());
    }
    bool TransformToBinning(const TH1 *H, TH1 *new_h) {
        if (!H || !new_h || H->GetDimension() != new_h->GetDimension()) return false;
        new_h->Reset();
        CopyStyling(H, new_h);
        for (unsigned int i = 1; i < GetNbins(H); ++i) {
            int x(0), y(0), z(0), new_i(0);
            H->GetBinXYZ(i, x, y, z);
            if (H->GetDimension() == 1) {
                new_i = new_h->FindBin(H->GetXaxis()->GetBinCenter(x));
            } else if (H->GetDimension() == 2) {
                new_i = new_h->FindBin(H->GetXaxis()->GetBinCenter(x), H->GetYaxis()->GetBinCenter(y));
            } else {
                new_i = new_h->FindBin(H->GetXaxis()->GetBinCenter(x), H->GetYaxis()->GetBinCenter(y), H->GetZaxis()->GetBinCenter(z));
            }
            new_h->SetBinContent(new_i, new_h->GetBinContent(new_i) + H->GetBinContent(i) / GetBinNormalization(H, i));
            new_h->GetBinError(new_i, std::sqrt(H->GetBinError(i) * H->GetBinError(i) / GetBinNormalization(H, i) +
                                                new_h->GetBinError(new_i) * new_h->GetBinError(new_i)));
        }
        for (unsigned int i = 1; i < GetNbins(new_h); ++i) {
            new_h->SetBinContent(i, new_h->GetBinContent(i) * GetBinNormalization(new_h, i));
            new_h->SetBinError(i, new_h->GetBinError(i) * GetBinNormalization(new_h, i));
        }
        return true;
    }

    std::shared_ptr<TH1> ProjectInto2D(const std::shared_ptr<TH1> &H3, unsigned int project_along, int bin_start, int bin_end) {
        return ProjectInto2D(H3.get(), project_along, bin_start, bin_end);
    }
    std::shared_ptr<TH1> ProjectInto2D(const TH1 *H3, unsigned int project_along, int bin_start, int bin_end) {
        const TAxis *first_axis = nullptr;
        const TAxis *second_axis = nullptr;
        if (project_along >= 3 || H3->GetDimension() < 3) {
            Error("ProjectInto2D()", Form("You're a very funny guy. Where is my %d dimension>?", project_along + 1));
            return std::shared_ptr<TH1>();
        }
        // Generate a slice perpendicular to the Z-axis
        if (project_along == 2) {
            first_axis = H3->GetXaxis();
            second_axis = H3->GetYaxis();
        } else if (project_along == 1) {
            first_axis = H3->GetXaxis();
            second_axis = H3->GetZaxis();
        } else if (project_along == 0) {
            first_axis = H3->GetYaxis();
            second_axis = H3->GetZaxis();
        }
        std::vector<double> bins_x = ExtractBinning(first_axis);
        std::vector<double> bins_y = ExtractBinning(second_axis);

        std::shared_ptr<TH1> new_histo = std::make_shared<TH2D>(RandomString(26).c_str(), "projection", bins_x.size() - 1, bins_x.data(),
                                                                bins_y.size() - 1, bins_y.data());
        for (int b1 = 0; b1 <= first_axis->GetNbins() + 1; ++b1) {
            for (int b2 = 0; b2 <= second_axis->GetNbins() + 1; ++b2) {
                std::pair<double, double> integral;
                if (project_along == 2)
                    integral = IntegrateAndError(H3, b1, b1 + 1, b2, b2 + 1, bin_start, bin_end + 1);
                else if (project_along == 1)
                    integral = IntegrateAndError(H3, b1, b1 + 1, bin_start, bin_end + 1, b2, b2 + 1);
                else if (project_along == 0)
                    integral = IntegrateAndError(H3, bin_start, bin_end + 1, b1, b1 + 1, b2, b2 + 1);
                new_histo->SetBinContent(b1, b2, integral.first * GetBinNormalization(new_histo, new_histo->GetBin(b1, b2)));
                new_histo->SetBinError(b1, b2, integral.second * GetBinNormalization(new_histo, new_histo->GetBin(b1, b2)));
            }
        }
        return new_histo;
    }

    std::vector<double> ExtractBinning(const TAxis *Axis) {
        std::vector<double> Binning;
        if (Axis) {
            int N = Axis->GetNbins();
            Binning.reserve(N + 1);
            for (int i = 1; i <= N; ++i) { Binning.push_back(Axis->GetBinLowEdge(i)); }
            Binning.push_back(Axis->GetBinUpEdge(N));
        }
        ClearFromDuplicates(Binning);
        std::sort(Binning.begin(), Binning.end());
        return Binning;
    }
    std::shared_ptr<TH1> MakeTH1(std::vector<double> BinEdgesX, std::vector<double> BinEdgesY, std::vector<double> BinEdgesZ) {
        ClearFromDuplicates(BinEdgesX);
        ClearFromDuplicates(BinEdgesY);
        ClearFromDuplicates(BinEdgesZ);
        std::sort(BinEdgesX.begin(), BinEdgesX.end());
        std::sort(BinEdgesY.begin(), BinEdgesY.end());
        std::sort(BinEdgesZ.begin(), BinEdgesZ.end());

        std::shared_ptr<TH1> H;
        if (!BinEdgesX.empty() && !BinEdgesY.empty() && !BinEdgesZ.empty()) {
            H = std::make_shared<TH3D>(RandomString(45).c_str(), "Variable binning", BinEdgesX.size() - 1, BinEdgesX.data(),
                                       BinEdgesY.size() - 1, BinEdgesY.data(), BinEdgesZ.size() - 1, BinEdgesZ.data());
        } else if (!BinEdgesX.empty() && !BinEdgesY.empty()) {
            H = std::make_shared<TH2D>(RandomString(45).c_str(), "Variable binning", BinEdgesX.size() - 1, BinEdgesX.data(),
                                       BinEdgesY.size() - 1, BinEdgesY.data());
        } else if (!BinEdgesX.empty()) {
            H = std::make_shared<TH1D>(RandomString(45).c_str(), "Variable binning", BinEdgesX.size() - 1, BinEdgesX.data());
        }
        if (H) H->SetDirectory(nullptr);
        return H;
    }

    bool isOverflowBin(const std::shared_ptr<TH1> &Histo, int bin) { return isOverflowBin(Histo.get(), bin); }
    bool isOverflowBin(const TH1 *Histo, int bin) {
        int x(-1), y(-1), z(-1);
        Histo->GetBinXYZ(bin, x, y, z);
        if (isOverflowBin(Histo->GetXaxis(), x)) return true;
        if (Histo->GetDimension() >= 2 && isOverflowBin(Histo->GetYaxis(), y)) return true;
        if (Histo->GetDimension() == 3 && isOverflowBin(Histo->GetZaxis(), z)) return true;
        return false;
    }
    bool isOverflowBin(const TAxis *a, int bin) { return bin <= 0 || bin >= a->GetNbins() + 1; }
    bool isAlphaNumeric(std::shared_ptr<TH1> histo) { return isAlphaNumeric(histo.get()); }
    bool isAlphaNumeric(TH1 *histo) {
        if (histo->GetXaxis()->IsAlphanumeric()) { return true; }
        if (histo->GetDimension() >= 2 && histo->GetYaxis()->IsAlphanumeric()) { return true; }
        if (histo->GetDimension() == 3 && histo->GetZaxis()->IsAlphanumeric()) { return true; }
        return false;
    }
    double PlotRange(const TAxis *a) { return a->GetBinUpEdge(a->GetNbins()) - a->GetBinLowEdge(1); }

    TDirectory *mkdir(const std::string &name, TDirectory *in_dir) {
        if (name.find("/") == std::string::npos) return in_dir;
        std::string dir_name = name.substr(0, name.find("/"));
        TDirectory *out_dir = in_dir->GetDirectory(dir_name.c_str());
        if (!out_dir) {
            in_dir->mkdir(dir_name.c_str());
            return mkdir(name, in_dir);
        }
        std::string remain = name.substr(name.find("/") + 1, std::string::npos);
        if (remain.empty() || remain.find("/") == std::string::npos) return out_dir;
        return mkdir(remain, out_dir);
    }
    std::shared_ptr<TH1> fetch_from_file(const std::string &path, TDirectory *in_dir) {
        if (!in_dir) return std::shared_ptr<TH1>();
        TH1 *h = nullptr;
        in_dir->GetObject(path.c_str(), h);
        std::shared_ptr<TH1> ret(h);
        if (ret) {
            ret->SetDirectory(0);
            std::string key_name = path.substr(path.rfind("/") == std::string::npos ? 0 : path.rfind("/") + 1);
            ret->SetName(key_name.c_str());
        }
        return ret;
    }
    std::vector<std::string> recursive_ls(TDirectory *d, const std::string &pre_path) {
        std::vector<std::string> content;
        if (!d) return content;
        for (const auto &key : *d->GetListOfKeys()) {
            TDirectory *sub_dir = nullptr;
            d->GetObject(key->GetName(), sub_dir);
            if (sub_dir)
                CopyVector(recursive_ls(sub_dir, pre_path + key->GetName() + "/"), content);
            else
                content.push_back(pre_path + key->GetName());
        }
        std::sort(content.begin(), content.end());
        return content;
    }
    bool isElementSubstr(const std::vector<std::string> &vec, const std::string &str) {
        return std::find_if(vec.begin(), vec.end(), [&str](const std::string &ele) { return str.find(ele) != std::string::npos; }) !=
               vec.end();
    }
    bool same_binning(const std::shared_ptr<TH1> &H1, const std::shared_ptr<TH1> &H2) {
        if (H1->GetDimension() != H2->GetDimension()) return false;
        if (H1->GetNbinsX() != H2->GetNbinsX()) return false;
        if (H1->GetNbinsY() != H2->GetNbinsY()) return false;
        if (H1->GetNbinsZ() != H2->GetNbinsZ()) return false;
        if (ExtractBinning(H1->GetXaxis()) != ExtractBinning(H2->GetXaxis())) return false;

        return true;
    }
}  // namespace XAMPP
