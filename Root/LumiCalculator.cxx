#include <XAMPPplotting/LumiCalculator.h>
#include <XAMPPplotting/MetaDataTreeReader.h>
#include <XAMPPplotting/PlottingUtils.h>
#include <XAMPPplotting/Weight.h>

#include <PathResolver/PathResolver.h>
#include <PileupReweighting/PileupReweightingTool.h>
namespace XAMPP {
    //###################################################################
    //                       LumiCalculator
    //###################################################################
    LumiCalculator* LumiCalculator::m_Inst = nullptr;
    LumiCalculator* LumiCalculator::getCalculator() {
        if (m_Inst == nullptr) m_Inst = new LumiCalculator();
        return m_Inst;
    }
    LumiCalculator::LumiCalculator() : m_recorded_runs(), m_prwTool(), m_PRWlumiFiles(), m_PRWconfigFiles() {}
    LumiCalculator::~LumiCalculator() { m_Inst = nullptr; }
    void LumiCalculator::SetPRWlumi(const std::vector<std::string>& lumi) { m_PRWlumiFiles = GetPathResolvedFileList(lumi); }
    void LumiCalculator::SetPRWconfig(const std::vector<std::string>& config) { m_PRWconfigFiles = GetPathResolvedFileList(config); }
    double LumiCalculator::getExpectedLumiFromRun(unsigned int runNumber) const {
        if (!GetPileUpTool()) return std::nan("nan");
        return GetPileUpTool()->GetIntegratedLumi(runNumber, runNumber) / 1.e3;
    }
    double LumiCalculator::getRecordedLumiFromRun(unsigned int runNumber) const {
        double Lumi = 0.;
        if (!GetPileUpTool()) return std::nan("nan");
        if (!IsElementInList(NormalizationDataBase::getDataBase()->GetRunNumbers(), runNumber)) return 0.;
        for (auto& Block : NormalizationDataBase::getDataBase()->GetTotalLumiBlocks(runNumber)) {
            Lumi += GetPileUpTool()->expert()->GetLumiBlockIntegratedLumi(runNumber, Block) / 1.e3;
        }
        return Lumi;
    }
    double LumiCalculator::getExpectedLumi(unsigned int begin, unsigned int end) const {
        double Lumi = 0.;
        for (unsigned int run = begin; run <= end; ++run) { Lumi += getExpectedLumiFromRun(run); }
        return Lumi;
    }
    double LumiCalculator::getRecordedLumi(unsigned int begin, unsigned int end) const {
        double Lumi = 0.;
        for (unsigned int run = begin; run <= end; ++run) { Lumi += getRecordedLumiFromRun(run); }
        return Lumi;
    }
    bool LumiCalculator::InitPileUpTool() {
        if (m_recorded_runs.empty()) {
            for (auto& prw_file : GetPRWlumi()) {
                std::vector<LumiCalculator::Run_Nblocks> runs;
                ReadLumiCalcFile(prw_file, runs);
                CopyVector(runs, m_recorded_runs);
            }
            std::sort(m_recorded_runs.begin(), m_recorded_runs.end(),
                      [](const Run_Nblocks& a, const Run_Nblocks& b) { return a.first < b.first; });
        }
        // Weight::getWeighter()->SetupEventInfo(true);
        if (!m_prwTool.get()) {
            for (const auto& File : m_PRWlumiFiles) Info("LumiCalculator::InitPileUpTool()", "Use PRWLumiFile " + File);
            for (const auto& File : m_PRWconfigFiles) Info("LumiCalculator::InitPileUpTool()", "Use PRWConfigFile " + File);
            m_prwTool = std::make_shared<CP::PileupReweightingTool>("prwTool");
            if (!m_prwTool->setProperty("ConfigFiles", m_PRWconfigFiles).isSuccess()) return false;
            if (!m_prwTool->setProperty("LumiCalcFiles", m_PRWlumiFiles).isSuccess()) return false;
            if (!m_prwTool->setProperty("DataScaleFactor", 1.0 / 1.03).isSuccess()) return false;
            if (!m_prwTool->setProperty("DataScaleFactorUP", 1.0 / 0.99).isSuccess()) return false;
            if (!m_prwTool->setProperty("DataScaleFactorDOWN", 1.0 / 1.07).isSuccess()) return false;
            if (!m_prwTool->initialize().isSuccess()) return false;
        }

        return true;
    }
    bool LumiCalculator::isGoodLumiBlock(unsigned int run, unsigned int block) const {
        return GetPileUpTool()->expert()->GetLumiBlockIntegratedLumi(run, block) > 0.;
    }
    void LumiCalculator::ReadLumiCalcFile(const std::string& path, std::vector<LumiCalculator::Run_Nblocks>& runset) const {
        std::unique_ptr<TFile> File(TFile::Open(ResolvePath(path).c_str(), "READ"));
        if (!File || !File->IsOpen()) {
            Error("LumiCalculator::ReadLumiCalcFile()", "Could not open " + path);
            return;
        }
        TTree* Tree = nullptr;
        File->GetObject("LumiMetaData", Tree);
        if (!Tree) {
            Error("LumiCalculator::ReadLumiCalcFile()", "Could not read the LumiMetaData tree");
            return;
        }
        unsigned int run;
        if (Tree->SetBranchAddress("RunNbr", &run) != 0) { return; }
        long nEntries = Tree->GetEntries();
        runset.reserve(runset.capacity() + nEntries);
        for (long i = 0; i < nEntries; ++i) {
            Tree->GetEntry(i);
            TH1* lumi_histo = nullptr;
            File->GetObject(Form("run%u_intlumi", run), lumi_histo);
            runset.push_back(Run_Nblocks(run, lumi_histo->GetXaxis()->GetBinUpEdge(lumi_histo->GetNbinsX())));
        }
        std::sort(runset.begin(), runset.end(), [](const Run_Nblocks& a, const Run_Nblocks& b) { return a.first < b.first; });
    }
    std::vector<unsigned int> LumiCalculator::good_runs() const {
        std::vector<unsigned int> runs;
        for (const auto& r_lb : m_recorded_runs) { runs.push_back(r_lb.first); }
        return runs;
    }
    // Retrieves the IlumiCalc/MC config files used for the PRW tool
    const std::vector<std::string>& LumiCalculator::GetPRWconfig() const { return m_PRWconfigFiles; }
    const std::vector<std::string>& LumiCalculator::GetPRWlumi() const { return m_PRWlumiFiles; }
    std::shared_ptr<CP::PileupReweightingTool> LumiCalculator::GetPileUpTool() const { return m_prwTool; }
    unsigned int LumiCalculator::GetNLumiBlocks(unsigned int run) const {
        for (const auto& record : m_recorded_runs) {
            if (record.first == run) return record.second;
        }
        return 0;
    }
    double LumiCalculator::getExpectedLumiFromRun(unsigned int run, unsigned int block) const {
        if (!IsElementInList(good_runs(), run)) return 0.;
        return m_prwTool->expert()->GetLumiBlockIntegratedLumi(run, block);
    }
    //######################################################
    //          LumiInterval
    //######################################################
    LumiInterval::LumiInterval(unsigned int bin) :
        m_start_run(-1),
        m_start_block(-1),
        m_end_run(-1),
        m_end_block(-1),
        m_lumi(0),
        m_bin(bin) {}

    void LumiInterval::set_start(unsigned int run, unsigned int lumi_block) {
        m_start_run = run;
        m_start_block = lumi_block;
    }
    void LumiInterval::set_end(unsigned int run, unsigned int lumi_block) {
        m_end_run = run;
        m_end_block = std::min(lumi_block, LumiCalculator::getCalculator()->GetNLumiBlocks(run));
    }

    bool LumiInterval::in_interval(unsigned int run, unsigned int lumi_block) const {
        if (run < start_run() || run > end_run())
            return false;
        else if (run == start_run() && lumi_block < start_block())
            return false;
        else if (run == end_run() && lumi_block > end_block())
            return false;
        return true;
    }

    unsigned int LumiInterval::start_run() const { return m_start_run; }
    unsigned int LumiInterval::end_run() const { return m_end_run; }
    unsigned int LumiInterval::start_block() const { return m_start_block; }
    unsigned int LumiInterval::end_block() const { return m_end_block; }
    unsigned int LumiInterval::bin_number() const { return m_bin; }
    void LumiInterval::set_lumi(double l) { m_lumi = l; }
    double LumiInterval::lumi() const { return m_lumi; }
    //#####################################################################
    //                      LumiSlicer
    //#####################################################################
    LumiSlicer::LumiSlicer(double slice_size) : m_blocks() {
        double int_lumi = 0;
        std::shared_ptr<LumiInterval> block = std::make_shared<LumiInterval>(1);
        for (const auto& run : LumiCalculator::getCalculator()->good_runs()) {
            unsigned int n = LumiCalculator::getCalculator()->GetNLumiBlocks(run);
            for (unsigned int b = 0; b <= n; ++b) {
                if (block->start_run() > run) block->set_start(run, b);
                int_lumi += LumiCalculator::getCalculator()->getExpectedLumiFromRun(run, b) / 1.e3;
                if (int_lumi < slice_size) continue;
                block->set_end(run, b);
                block->set_lumi(int_lumi);
                m_blocks.push_back(block);
                block = std::make_shared<LumiInterval>(block->bin_number() + 1);
                int_lumi = 0;
            }
        }

        block->set_lumi(int_lumi);
        m_blocks.push_back(block);
    }
    LumiSlicer::LumiSlicer() : m_blocks() {
        std::shared_ptr<LumiInterval> block = std::make_shared<LumiInterval>(1);
        for (const auto& run : LumiCalculator::getCalculator()->good_runs()) {
            block->set_start(run, 0);
            block->set_end(run, LumiCalculator::getCalculator()->GetNLumiBlocks(run));
            block->set_lumi(LumiCalculator::getCalculator()->getExpectedLumi(run, run));
            m_blocks.push_back(block);
            block = std::make_shared<LumiInterval>(block->bin_number() + 1);
        }
    }
    unsigned int LumiSlicer::find_bin(unsigned int run, unsigned int lumi_block) const {
        for (const auto& block : m_blocks) {
            if (block->in_interval(run, lumi_block)) return block->bin_number();
        }
        return 0;
    }
    std::shared_ptr<LumiInterval> LumiSlicer::get_block(unsigned int bin) const {
        if (bin - 1 < size()) return m_blocks.at(bin - 1);
        return std::shared_ptr<LumiInterval>();
    }
    size_t LumiSlicer::size() const { return m_blocks.size(); }
}  // namespace XAMPP
