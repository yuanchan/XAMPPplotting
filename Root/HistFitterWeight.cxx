#include <XAMPPplotting/HistFitterMetaReader.h>
#include <XAMPPplotting/HistFitterWeight.h>
#include <XAMPPplotting/UtilityReader.h>

namespace XAMPP {
    //##########################################################
    //                      HistFitterWeight
    //##########################################################
    HistFitterWeight::HistFitterWeight(bool isData) : Weight(isData) {
        Info("HistFitterWeight()",
             "Replace the XAMPPplotting Weighter by the version implemented in XAMPPtmva. isData: " + std::to_string(isData));
    }
    Weight* HistFitterWeight::getWeighter(bool isData) {
        if (!m_DB) m_DB = new HistFitterWeight(isData);
        return m_DB;
    }
    bool HistFitterWeight::InitWeights() {
        if (!isData()) {
            AppendWeight(WeightElementFromTree::GetWeighter("NormWeight"));
            if (applyPRW()) AppendWeight(WeightElementFromTree::GetWeighter("prwWeight"));
        }
        if (!Weight::InitWeights()) return false;
        return true;
    }
    bool HistFitterWeight::init(TTree* tree) {
        for (auto& w : GetUsedWeighters()) {
            if (!w->init(tree)) return false;
        }
        return true;
    }
    std::vector<std::string> HistFitterWeight::excluded_weights() const { return std::vector<std::string>{}; }
    //##########################################################
    //                      HybridWeight
    //##########################################################

    Weight* HybridWeight::getWeighter(bool isData) {
        if (!m_DB) m_DB = new HybridWeight(isData);
        return m_DB;
    }
    bool HybridWeight::InitWeights() {
        HybridNormalizationWeight::getElement();
        return Weight::InitWeights();
    }
    HybridWeight::HybridWeight(bool isData) : Weight(isData), m_isMC(!isData), m_current_isMC(!isData) {
        Info("HybridWeight()",
             "Replace the XAMPPplotting Weighter by the version implemented in XAMPPtmva. This inherited class does not distinguish "
             "between data and mc");
    }
    bool HybridWeight::init(TTree* tree) {
        // mcChannelNumber is used to distinguish whether the tree is data or not
        m_current_isMC = ScalarVarReader<int>::GetReader("mcChannelNumber")->init(tree);
        if (!m_current_isMC) {
            // Initialize the weighters by hand making
            // to ensure that they are not applied if we're running on data
            for (auto& W : GetUsedWeighters()) { W->init(tree); }
            return true;
        }
        return Weight::init(tree);
    }
    bool HybridWeight::isData() const { return !m_isMC; }
    int HybridWeight::mcChannelNumber() const {
        if (m_current_isMC) return Weight::mcChannelNumber();
        return -1;
    }
    //##########################################################
    //                      HybridNormalizationWeight
    //##########################################################
    NormalizationWeight* HybridNormalizationWeight::getElement() {
        if (!Weight::getWeighter()->GetWeightElement("NormWeight")) new HybridNormalizationWeight();
        return dynamic_cast<NormalizationWeight*>(Weight::getWeighter()->GetWeightElement("NormWeight"));
    }

    HybridNormalizationWeight::HybridNormalizationWeight() : NormalizationWeight() {}

    std::shared_ptr<NormalizationVariation> HybridNormalizationWeight::newVariation(const std::string& variation) {
        return std::make_shared<HybdridNormalizationVariation>(variation);
    }
    //##########################################################
    //                      HybdridNormalizationVariation
    //##########################################################
    HybdridNormalizationVariation::HybdridNormalizationVariation(const std::string& norm) :
        NormalizationVariation(norm),
        m_weighter(Weight::getWeighter()) {}
    double HybdridNormalizationVariation::Read() {
        if (m_weighter->mcChannelNumber() == -1) return 1.;
        return NormalizationVariation::Read();
    }
}  // namespace XAMPP
