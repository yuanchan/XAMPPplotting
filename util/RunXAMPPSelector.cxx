#include <TChain.h>
#include <TFile.h>
#include <TH1F.h>
#include <TStopwatch.h>
#include <TTree.h>
#include "TSystem.h"

#include <cstring>
#include <iostream>
#include <sstream>
#include <string>

#include "AsgTools/Check.h"
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/ReturnCheck.h"

#include <XAMPPplotting/PlottingUtils.h>
#include <XAMPPplotting/Selector.h>
int main(int argc, char* argv[]) {
    RETURN_CHECK("WriteDefaultHistos", xAOD::Init("WriteDefaultHistos"));

    std::string HistoCfg = std::string(getenv("ROOTCOREBIN")) + "/data/XAMPPplotting/HistoConf/ttYAnalysis/Histos.conf";
    std::string InputCfg = std::string(getenv("ROOTCOREBIN")) + "/";
    std::string TreeName = "SUSYAnalysisConfig";
    std::string Name = "MyAnalysis";
    std::string output = "output_histograms.root";
    std::string InDir = "";
    long long int nmax = -1;
    long long int skip = 0;
    // read the config provided by the user
    for (int k = 1; k < argc - 1; ++k) {
        if (std::string(argv[k]).find("-h") == 0) { HistoCfg = argv[k + 1]; }
        if (std::string(argv[k]).find("-i") == 0) { InputCfg = argv[k + 1]; }
        if (std::string(argv[k]).find("-d") == 0) { InDir = argv[k + 1]; }
        if (std::string(argv[k]).find("-a") == 0) { Name = argv[k + 1]; }
        if (std::string(argv[k]).find("-t") == 0) { TreeName = argv[k + 1]; }
        if (std::string(argv[k]).find("-n") == 0) { nmax = atoi(argv[k + 1]); }
        if (std::string(argv[k]).find("-skip") == 0) { skip = atoi(argv[k + 1]); }
        if (std::string(argv[k]).find("-o") == 0) { output = argv[k + 1]; }
    }
    std::vector<std::string> Files;
    if (!InDir.empty())
        Files = XAMPP::ListDirectory(InDir, ".root");
    else if (InputCfg.find(".root") < InputCfg.size())
        Files.push_back(InputCfg);
    else {
        std::ifstream inf(InputCfg);
        if (!inf) return EXIT_FAILURE;
        std::string Line;
        while (XAMPP::GetLine(inf, Line)) {
            std::stringstream sstr(Line);
            std::string Input, Filename;
            sstr >> Input >> Filename;
            if (Input == "Input")
                Files.push_back(Filename);
            else if (Input.find(".root") < Input.size())
                Files.push_back(Input);
        }
    }
    XAMPP::Selector MyAnalysis(Name, TreeName, Files);
    MyAnalysis.SetOutputLocation(output);
    //    MyAnalysis.ProcessSystematics(true);
    if (!MyAnalysis.ReadHistoConfig(HistoCfg) || !MyAnalysis.Process(nmax, skip)) return EXIT_FAILURE;
    MyAnalysis.FinalizeOutput();
    return EXIT_SUCCESS;
}
