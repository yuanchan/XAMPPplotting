#include <TFile.h>
#include <TTree.h>
#include <memory>

#include <cstring>
#include <iostream>
#include <string>

#include <TSystem.h>
#include <XAMPPplotting/AnalysisSetup.h>
#include <XAMPPplotting/MetaDataTreeReader.h>
#include <XAMPPplotting/PlottingUtils.h>

int main(int argc, char* argv[]) {
    std::string RunCfg = "";
    std::string InputCfg = "";
    std::string output = "duplicated_events.root";

    // read the config provided by the user
    for (int k = 1; k < argc - 1; ++k) {
        if (std::string(argv[k]).find("-r") == 0) {
            RunCfg = argv[k + 1];
        } else if (std::string(argv[k]).find("-i") == 0) {
            InputCfg = argv[k + 1];
        } else if (std::string(argv[k]).find("-o") == 0) {
            output = argv[k + 1];
        }
    }

    XAMPP::AnalysisSetup setup;
    setup.SetOutputLocation(output);
    if (!setup.ParseConfig(RunCfg, RunCfg, InputCfg)) return EXIT_FAILURE;

    std::shared_ptr<XAMPP::Analysis> analysis = setup.SetupEventChecker();
    if (!analysis->Process(-1, 0)) {
        system(Form("rm -rf %s", output.c_str()));
        return EXIT_FAILURE;
    }
    analysis->FinalizeOutput();
    system(Form("rm -rf %s", output.c_str()));
    return EXIT_SUCCESS;
}
