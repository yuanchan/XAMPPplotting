#include <AsgTools/Check.h>
#include <TFile.h>
#include <XAMPPplotting/AnalysisSetup.h>
#include <XAMPPplotting/IfDefHelpers.h>
#include <xAODRootAccess/Init.h>
#include <xAODRootAccess/tools/ReturnCheck.h>
#include <cstring>
#include <iostream>
#include <string>

int main(int argc, char* argv[]) {
    RETURN_CHECK("WriteDefaultTrees", xAOD::Init("WriteDefaultTrees"));

    std::string TreeCfg = "";
    std::string InputCfg = "";
    std::string output = "output_trees.root";

    long long int nmax = -1;
    long long int skip = 0;

    int FirstFile = 0;
    int LastFile = -1;
    // read the config provided by the user
    for (int k = 1; k < argc - 1; ++k) {
        if (std::string(argv[k]).find("-t") == 0) {
            TreeCfg = argv[k + 1];
        } else if (std::string(argv[k]).find("-i") == 0) {
            InputCfg = argv[k + 1];
        } else if (std::string(argv[k]).find("-n") == 0) {
            nmax = atoi(argv[k + 1]);
        } else if (std::string(argv[k]).find("--skip") == 0) {
            skip = atoi(argv[k + 1]);
        } else if (std::string(argv[k]).find("-o") == 0) {
            output = argv[k + 1];
        } else if (std::string(argv[k]).find("--begin") == 0) {
            FirstFile = atoi(argv[k + 1]);
        } else if (std::string(argv[k]).find("--end") == 0) {
            LastFile = atoi(argv[k + 1]);
        } else if (std::string(argv[k]).find("--nevents") == 0) {
            nmax = atoi(argv[k + 1]);
        }
    }
    // Define a flag useful in some config files
    XAMPP::IfDefFlags::Instance()->AddFlag("isSlimmedTreeMaking");

    XAMPP::AnalysisSetup setup;
    setup.SetOutputLocation(output);
    if (!setup.ParseTreeConfig(TreeCfg, InputCfg)) return EXIT_FAILURE;

    std::shared_ptr<XAMPP::Analysis> analysis = setup.SetupNTupleWriters(FirstFile, LastFile);
    if (!analysis->Process(nmax, skip)) {
        system(Form("rm -rf %s", output.c_str()));
        return EXIT_FAILURE;
    }
    analysis->FinalizeOutput();
    return EXIT_SUCCESS;
}
