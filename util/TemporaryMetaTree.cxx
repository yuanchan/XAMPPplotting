#include <TFile.h>
#include <TTree.h>
#include <memory>

#include <cstring>
#include <iostream>
#include <string>

#include <TSystem.h>
#include <XAMPPplotting/AnalysisSetup.h>
#include <XAMPPplotting/MetaDataTreeReader.h>
#include <XAMPPplotting/PlottingUtils.h>

int main(int argc, char* argv[]) {
    std::string InputCfg = "";
    std::string output = "output_metadatatree.root";

    // read the config provided by the user
    for (int k = 1; k < argc - 1; ++k) {
        if (std::string(argv[k]).find("-i") == 0) {
            InputCfg = argv[k + 1];
        } else if (std::string(argv[k]).find("-o") == 0) {
            output = argv[k + 1];
        }
    }
    XAMPP::AnalysisSetup setup;
    setup.ignoreMetaInput();
    if (!setup.ParseTreeConfig(InputCfg, InputCfg)) return EXIT_FAILURE;
    XAMPP::NormalizationDataBase* DB = XAMPP::NormalizationDataBase::getDataBase();
    DB->PromptMetaDataTree();

    if (output.rfind("/") != std::string::npos) {
        std::string OutDir = output.substr(0, output.rfind("/"));
        if (XAMPP::DoesDirectoryExist(OutDir) == false) {
            XAMPP::Info("TemporaryMetaTree()", "Create directory " + OutDir + " to store the output in there");
            gSystem->mkdir(OutDir.c_str(), true);
        } else
            XAMPP::Info("TemporaryMetaTree()", "Output file will be stored in " + OutDir);
    }

    std::unique_ptr<TFile> OutFile(TFile::Open(output.c_str(), "RECREATE"));
    OutFile->cd();
    TTree* MetaDataTree = new TTree("MetaDataTree", "MetaData Tree for Small Analysis Ntuples");

    bool isData = DB->isData();
    // Total events
    Long64_t TotalEvents = 0;
    Long64_t ProcessedEvents = 0;

    // DSID and process ID
    unsigned int mcChannelNumber = 0;
    unsigned int runNumber = 0;
    unsigned int ProcID = 0;

    // Cross-section information
    double xSection = 0;
    double kFactor = 0;
    double FilterEff = 0.;

    double SumW = 0.;
    double SumW2 = 0.;
    double prwLumi = 0;
    // Luminosity information
    std::set<unsigned int> ProcessedBlocks;
    std::set<unsigned int> TotalBlocks;

    std::set<unsigned int>* ProcLumiPtr = &ProcessedBlocks;
    std::set<unsigned int>* TotalLumiPtr = &TotalBlocks;

    // Prepare tree structure
    MetaDataTree->Branch("isData", &isData);
    MetaDataTree->Branch("runNumber", &runNumber);

    MetaDataTree->Branch("TotalEvents", &TotalEvents);
    MetaDataTree->Branch("ProcessedEvents", &ProcessedEvents);

    if (!isData) {
        MetaDataTree->Branch("mcChannelNumber", &mcChannelNumber);
        MetaDataTree->Branch("ProcessID", &ProcID);
        MetaDataTree->Branch("xSection", &xSection);
        MetaDataTree->Branch("kFactor", &kFactor);
        MetaDataTree->Branch("FilterEfficiency", &FilterEff);
        MetaDataTree->Branch("TotalSumW", &SumW);
        MetaDataTree->Branch("TotalSumW2", &SumW2);
        MetaDataTree->Branch("prwLuminosity", &prwLumi);
    } else {
        MetaDataTree->Branch("ProcessedLumiBlocks", ProcLumiPtr);
        MetaDataTree->Branch("TotalLumiBlocks", TotalLumiPtr);
    }
    // Write the Monte Carlo  meta-data in its most compact form
    if (!isData) {
        std::vector<unsigned int> DSIDs = DB->GetListOfMCSamples();
        for (auto ds : DSIDs) {
            mcChannelNumber = ds;
            // Retrieve the list of process ids for this particular sample
            std::vector<unsigned int> proc_ids = DB->GetListOfProcesses(ds);
            // Also the period handlers to distinguis between mc16a/d/f
            std::shared_ptr<XAMPP::MonteCarloPeriodHandler> period_handler = DB->getMCperiodHandler(ds);

            for (auto pid : proc_ids) {
                ProcID = pid;
                xSection = DB->GetxSection(ds, pid);
                FilterEff = DB->GetFilterEfficiency(ds, pid);
                kFactor = DB->GetkFactor(ds, pid);

                for (auto period : period_handler->getMCcampaigns()) {
                    runNumber = period;
                    std::shared_ptr<XAMPP::MetaDataStore> store = period_handler->getStore(pid, period);
                    if (!store) {
                        Warning("TempraryMetaDataTree()", "Monte Carlo period %d is unknown for channel %d with process ID %d", period,
                                mcChannelNumber, pid);
                        continue;
                    }
                    TotalEvents = store->TotalEvents();
                    ProcessedEvents = store->ProcessedEvents();
                    SumW = store->SumW();
                    SumW2 = store->SumW2();
                    prwLumi = store->prwLuminosity();
                    MetaDataTree->Fill();
                }
            }
        }
    } else {
        std::vector<unsigned int> runs = DB->GetRunNumbers();
        for (const auto& run : runs) {
            runNumber = run;
            XAMPP::CopyVector(DB->GetProcessedLumiBlocks(run), ProcessedBlocks, true);
            XAMPP::CopyVector(DB->GetTotalLumiBlocks(run), TotalBlocks, true);
            TotalEvents = DB->GetTotalEvents(run);
            ProcessedEvents = DB->GetProcessedEvents(run);
            MetaDataTree->Fill();
        }
    }
    OutFile->Write("", TObject::kOverwrite);
    OutFile->Close();
    // Delete the database to load the new file and print it on the screen
    delete XAMPP::NormalizationDataBase::getDataBase();
    XAMPP::NormalizationDataBase::getDataBase()->init(std::vector<std::string>{output});
    XAMPP::NormalizationDataBase::getDataBase()->PromptMetaDataTree();

    return EXIT_SUCCESS;
}
