#include <TFile.h>

#include <iostream>
#include <string>

#include "AsgTools/Check.h"
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/ReturnCheck.h"

#include <XAMPPplotting/AnalysisSetup.h>
#include <XAMPPplotting/PlottingUtils.h>

//// Standard executable to run the post processing
//// arguments:  WriteDefaultHistos [-n <-- optional number of pairs to process] [-i <input config>] [-h <histo config>] [-r <run config>]
///-o Output file / The tool will parse all files within the folders
//
int main(int argc, char* argv[]) {
    RETURN_CHECK("WriteDefaultHistos", xAOD::Init("WriteDefaultHistos"));

    //
    //    // some defaults...

    std::string HistoCfg = "";
    std::string RunCfg = "";
    std::string InputCfg = "";
    std::string output = "output_histograms.root";

    long long int nmax = -1;
    long long int skip = 0;

    int FirstFile = 0;
    int LastFile = -1;

    // read the config provided by the user
    for (int k = 1; k < argc - 1; ++k) {
        if (std::string(argv[k]).find("-h") == 0) {
            HistoCfg = argv[k + 1];
        } else if (std::string(argv[k]).find("-r") == 0) {
            RunCfg = argv[k + 1];
        } else if (std::string(argv[k]).find("-i") == 0) {
            InputCfg = argv[k + 1];
        } else if (std::string(argv[k]).find("-n") == 0) {
            nmax = atoi(argv[k + 1]);
        } else if (std::string(argv[k]).find("--skip") == 0) {
            skip = atoi(argv[k + 1]);
        } else if (std::string(argv[k]).find("-o") == 0) {
            output = argv[k + 1];
        } else if (std::string(argv[k]).find("--begin") == 0) {
            FirstFile = atoi(argv[k + 1]);
        } else if (std::string(argv[k]).find("--end") == 0) {
            LastFile = atoi(argv[k + 1]);
        }
    }

    XAMPP::AnalysisSetup setup;
    setup.SetOutputLocation(output);
    if (!setup.ParseConfig(HistoCfg, RunCfg, InputCfg)) return EXIT_FAILURE;

    std::shared_ptr<XAMPP::Analysis> analysis = setup.SetupAnalyses(FirstFile, LastFile);
    if (!analysis->Process(nmax, skip)) {
        system(Form("rm -rf %s", output.c_str()));
        return EXIT_FAILURE;
    }
    analysis->FinalizeOutput();

    return EXIT_SUCCESS;
}
