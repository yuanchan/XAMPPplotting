from XAMPPplotting.Defs import *
SignalPath = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-06-10/Stau_Test/"

Bkg = DSconfig(sampletype=SampleTypes.Irreducible, name="SM_Bkg", label="Background", filepath=SignalPath + "DD_Fakes_PowHeg.root")

StauStau_100_1 = DSconfig(
    sampletype=SampleTypes.Signal,
    name='StauStau_100_1',
    label='StauStau_100_1',
    filepath=SignalPath + '/StauStau_100_1.root',
    ##scale = 100.,
)
StauStau_120_1 = DSconfig(sampletype=SampleTypes.Signal,
                          name='StauStau_120_1',
                          label='StauStau_120_1',
                          filepath=SignalPath + '/StauStau_120_1.root')
StauStau_120_40 = DSconfig(sampletype=SampleTypes.Signal,
                           name='StauStau_120_40',
                           label='StauStau_120_40',
                           filepath=SignalPath + '/StauStau_120_40.root')
StauStau_140_1 = DSconfig(sampletype=SampleTypes.Signal,
                          name='StauStau_140_1',
                          label='StauStau_140_1',
                          filepath=SignalPath + '/StauStau_140_1.root')
StauStau_140_40 = DSconfig(sampletype=SampleTypes.Signal,
                           name='StauStau_140_40',
                           label='StauStau_140_40',
                           filepath=SignalPath + '/StauStau_140_40.root')
StauStau_160_1 = DSconfig(sampletype=SampleTypes.Signal,
                          name='StauStau_160_1',
                          label='StauStau_160_1',
                          filepath=SignalPath + '/StauStau_160_1.root')
StauStau_160_40 = DSconfig(sampletype=SampleTypes.Signal,
                           name='StauStau_160_40',
                           label='StauStau_160_40',
                           filepath=SignalPath + '/StauStau_160_40.root')
StauStau_160_80 = DSconfig(sampletype=SampleTypes.Signal,
                           name='StauStau_160_80',
                           label='StauStau_160_80',
                           filepath=SignalPath + '/StauStau_160_80.root')
StauStau_180_1 = DSconfig(sampletype=SampleTypes.Signal,
                          name='StauStau_180_1',
                          label='StauStau_180_1',
                          filepath=SignalPath + '/StauStau_180_1.root')
StauStau_180_40 = DSconfig(sampletype=SampleTypes.Signal,
                           name='StauStau_180_40',
                           label='StauStau_180_40',
                           filepath=SignalPath + '/StauStau_180_40.root')
StauStau_200_1 = DSconfig(sampletype=SampleTypes.Signal,
                          name='StauStau_200_1',
                          label='StauStau_200_1',
                          filepath=SignalPath + '/StauStau_200_1.root')
StauStau_200_120 = DSconfig(sampletype=SampleTypes.Signal,
                            name='StauStau_200_120',
                            label='StauStau_200_120',
                            filepath=SignalPath + '/StauStau_200_120.root')
StauStau_200_1_mmix_s12 = DSconfig(sampletype=SampleTypes.Signal,
                                   name='StauStau_200_1_mmix_s12',
                                   label='StauStau_200_1_mmix_s12',
                                   filepath=SignalPath + '/StauStau_200_1_mmix_s12.root')
StauStau_200_40 = DSconfig(sampletype=SampleTypes.Signal,
                           name='StauStau_200_40',
                           label='StauStau_200_40',
                           filepath=SignalPath + '/StauStau_200_40.root')
StauStau_200_80 = DSconfig(sampletype=SampleTypes.Signal,
                           name='StauStau_200_80',
                           label='StauStau_200_80',
                           filepath=SignalPath + '/StauStau_200_80.root')
StauStau_220_1 = DSconfig(sampletype=SampleTypes.Signal,
                          name='StauStau_220_1',
                          label='StauStau_220_1',
                          filepath=SignalPath + '/StauStau_220_1.root')
StauStau_220_40 = DSconfig(sampletype=SampleTypes.Signal,
                           name='StauStau_220_40',
                           label='StauStau_220_40',
                           filepath=SignalPath + '/StauStau_220_40.root')
StauStau_240_1 = DSconfig(sampletype=SampleTypes.Signal,
                          name='StauStau_240_1',
                          label='StauStau_240_1',
                          filepath=SignalPath + '/StauStau_240_1.root')
StauStau_240_120 = DSconfig(sampletype=SampleTypes.Signal,
                            name='StauStau_240_120',
                            label='StauStau_240_120',
                            filepath=SignalPath + '/StauStau_240_120.root')
StauStau_240_160 = DSconfig(sampletype=SampleTypes.Signal,
                            name='StauStau_240_160',
                            label='StauStau_240_160',
                            filepath=SignalPath + '/StauStau_240_160.root')
StauStau_240_40 = DSconfig(sampletype=SampleTypes.Signal,
                           name='StauStau_240_40',
                           label='StauStau_240_40',
                           filepath=SignalPath + '/StauStau_240_40.root')
StauStau_240_80 = DSconfig(sampletype=SampleTypes.Signal,
                           name='StauStau_240_80',
                           label='StauStau_240_80',
                           filepath=SignalPath + '/StauStau_240_80.root')
StauStau_260_1 = DSconfig(sampletype=SampleTypes.Signal,
                          name='StauStau_260_1',
                          label='StauStau_260_1',
                          filepath=SignalPath + '/StauStau_260_1.root')
StauStau_260_40 = DSconfig(sampletype=SampleTypes.Signal,
                           name='StauStau_260_40',
                           label='StauStau_260_40',
                           filepath=SignalPath + '/StauStau_260_40.root')
StauStau_280_1 = DSconfig(sampletype=SampleTypes.Signal,
                          name='StauStau_280_1',
                          label='StauStau_280_1',
                          filepath=SignalPath + '/StauStau_280_1.root')
StauStau_280_120 = DSconfig(sampletype=SampleTypes.Signal,
                            name='StauStau_280_120',
                            label='StauStau_280_120',
                            filepath=SignalPath + '/StauStau_280_120.root')
StauStau_280_160 = DSconfig(sampletype=SampleTypes.Signal,
                            name='StauStau_280_160',
                            label='StauStau_280_160',
                            filepath=SignalPath + '/StauStau_280_160.root')
StauStau_280_40 = DSconfig(sampletype=SampleTypes.Signal,
                           name='StauStau_280_40',
                           label='StauStau_280_40',
                           filepath=SignalPath + '/StauStau_280_40.root')
StauStau_280_80 = DSconfig(sampletype=SampleTypes.Signal,
                           name='StauStau_280_80',
                           label='StauStau_280_80',
                           filepath=SignalPath + '/StauStau_280_80.root')
StauStau_300_1 = DSconfig(sampletype=SampleTypes.Signal,
                          name='StauStau_300_1',
                          label='StauStau_300_1',
                          filepath=SignalPath + '/StauStau_300_1.root')
StauStau_300_40 = DSconfig(sampletype=SampleTypes.Signal,
                           name='StauStau_300_40',
                           label='StauStau_300_40',
                           filepath=SignalPath + '/StauStau_300_40.root')
StauStau_320_1 = DSconfig(sampletype=SampleTypes.Signal,
                          name='StauStau_320_1',
                          label='StauStau_320_1',
                          filepath=SignalPath + '/StauStau_320_1.root')
StauStau_320_120 = DSconfig(sampletype=SampleTypes.Signal,
                            name='StauStau_320_120',
                            label='StauStau_320_120',
                            filepath=SignalPath + '/StauStau_320_120.root')
StauStau_320_160 = DSconfig(sampletype=SampleTypes.Signal,
                            name='StauStau_320_160',
                            label='StauStau_320_160',
                            filepath=SignalPath + '/StauStau_320_160.root')
StauStau_320_200 = DSconfig(sampletype=SampleTypes.Signal,
                            name='StauStau_320_200',
                            label='StauStau_320_200',
                            filepath=SignalPath + '/StauStau_320_200.root')
StauStau_320_40 = DSconfig(sampletype=SampleTypes.Signal,
                           name='StauStau_320_40',
                           label='StauStau_320_40',
                           filepath=SignalPath + '/StauStau_320_40.root')
StauStau_320_80 = DSconfig(sampletype=SampleTypes.Signal,
                           name='StauStau_320_80',
                           label='StauStau_320_80',
                           filepath=SignalPath + '/StauStau_320_80.root')
StauStau_340_1 = DSconfig(sampletype=SampleTypes.Signal,
                          name='StauStau_340_1',
                          label='StauStau_340_1',
                          filepath=SignalPath + '/StauStau_340_1.root')
StauStau_340_40 = DSconfig(sampletype=SampleTypes.Signal,
                           name='StauStau_340_40',
                           label='StauStau_340_40',
                           filepath=SignalPath + '/StauStau_340_40.root')
StauStau_360_1 = DSconfig(sampletype=SampleTypes.Signal,
                          name='StauStau_360_1',
                          label='StauStau_360_1',
                          filepath=SignalPath + '/StauStau_360_1.root')
StauStau_360_120 = DSconfig(sampletype=SampleTypes.Signal,
                            name='StauStau_360_120',
                            label='StauStau_360_120',
                            filepath=SignalPath + '/StauStau_360_120.root')
StauStau_360_160 = DSconfig(sampletype=SampleTypes.Signal,
                            name='StauStau_360_160',
                            label='StauStau_360_160',
                            filepath=SignalPath + '/StauStau_360_160.root')
StauStau_360_200 = DSconfig(sampletype=SampleTypes.Signal,
                            name='StauStau_360_200',
                            label='StauStau_360_200',
                            filepath=SignalPath + '/StauStau_360_200.root')
StauStau_360_40 = DSconfig(sampletype=SampleTypes.Signal,
                           name='StauStau_360_40',
                           label='StauStau_360_40',
                           filepath=SignalPath + '/StauStau_360_40.root')
StauStau_360_80 = DSconfig(sampletype=SampleTypes.Signal,
                           name='StauStau_360_80',
                           label='StauStau_360_80',
                           filepath=SignalPath + '/StauStau_360_80.root')
StauStau_380_1 = DSconfig(sampletype=SampleTypes.Signal,
                          name='StauStau_380_1',
                          label='StauStau_380_1',
                          filepath=SignalPath + '/StauStau_380_1.root')
StauStau_400_1 = DSconfig(sampletype=SampleTypes.Signal,
                          name='StauStau_400_1',
                          label='StauStau_400_1',
                          filepath=SignalPath + '/StauStau_400_1.root')
StauStau_400_120 = DSconfig(sampletype=SampleTypes.Signal,
                            name='StauStau_400_120',
                            label='StauStau_400_120',
                            filepath=SignalPath + '/StauStau_400_120.root')
StauStau_400_160 = DSconfig(sampletype=SampleTypes.Signal,
                            name='StauStau_400_160',
                            label='StauStau_400_160',
                            filepath=SignalPath + '/StauStau_400_160.root')
StauStau_400_40 = DSconfig(sampletype=SampleTypes.Signal,
                           name='StauStau_400_40',
                           label='StauStau_400_40',
                           filepath=SignalPath + '/StauStau_400_40.root')
StauStau_400_80 = DSconfig(sampletype=SampleTypes.Signal,
                           name='StauStau_400_80',
                           label='StauStau_400_80',
                           filepath=SignalPath + '/StauStau_400_80.root')
StauStau_440_1 = DSconfig(sampletype=SampleTypes.Signal,
                          name='StauStau_440_1',
                          label='StauStau_440_1',
                          filepath=SignalPath + '/StauStau_440_1.root')
StauStau_440_40 = DSconfig(sampletype=SampleTypes.Signal,
                           name='StauStau_440_40',
                           label='StauStau_440_40',
                           filepath=SignalPath + '/StauStau_440_40.root')
StauStau_80_1 = DSconfig(sampletype=SampleTypes.Signal,
                         name='StauStau_80_1',
                         label='StauStau_80_1',
                         filepath=SignalPath + '/StauStau_80_1.root')
