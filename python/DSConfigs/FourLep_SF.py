import ROOT, os, sys
from XAMPPplotting.Defs import *
from XAMPPplotting.CheckMetaData import GetNormalizationDB
from XAMPPplotting.CalculateLumiFromIlumicalc import CalculateRecordedLumi
from XAMPPplotting.FileUtils import ResolvePath, ReadInputConfig

PATH = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-05-20/FourLep_SF/"
CONFIGPATH = ResolvePath("XAMPPmultilep/InputConf/MPI/SFAnalysis/")

Periods = [Cfg[:Cfg.rfind(".")] for Cfg in os.listdir(CONFIGPATH) if Cfg.find("data") != -1]
Files = []
CalcLumi = False

if CalcLumi:
    # for S in Samples.itervalues():
    for P in Periods:
        Files += ReadInputConfig("%s/%s.conf" % (CONFIGPATH, P))
    LUMI = 0.
    for R in GetNormalizationDB(Files).GetRunNumbers():
        LUMI += CalculateRecordedLumi(R)
    print LUMI
    exit(1)

LUMI = 139.

mcperiod = ""

Data = DSconfig(lumi=LUMI,
                colour=ROOT.kBlack,
                label="Data",
                name="Data",
                filepath=["%s/%s.root" % (PATH, P) for P in Periods],
                sampletype=SampleTypes.Data)

top = DSconfig(colour=ROOT.kBlue - 2,
               name="top",
               label="top",
               filepath=[
                   "%s/PowHegPy8_ttbar_incl%s.root" % (PATH, ("" if len(mcperiod) == 0 else "_") + mcperiod),
               ],
               sampletype=SampleTypes.Irreducible)

Zjets = DSconfig(
    colour=ROOT.kViolet - 9,
    name="Zjets",
    label="Z+Jets",
    filepath=[
        "%s/PowHegPy8_Zee%s.root" % (PATH, ("" if len(mcperiod) == 0 else "_") + mcperiod),
        "%s/PowHegPy8_Zmumu%s.root" % (PATH, ("" if len(mcperiod) == 0 else "_") + mcperiod),
        "%s/PowHegPy8_Ztautau%s.root" % (PATH, ("" if len(mcperiod) == 0 else "_") + mcperiod),
        # "%s/PowHegPy8_DYee%s.root" % (PATH, ("" if len(mcperiod) == 0 else "_") + mcperiod),
        # "%s/PowHegPy8_DYmumu%s.root" % (PATH, ("" if len(mcperiod) == 0 else "_") + mcperiod),
        # "%s/PowHegPy8_DYtautau%s.root" % (PATH, ("" if len(mcperiod) == 0 else "_") + mcperiod),

        #"%s/Sherpa221_Zee%s.root" % (PATH, ("" if len(mcperiod) == 0 else "_") + mcperiod),
        #"%s/Sherpa221_Zmumu%s.root" % (PATH, ("" if len(mcperiod) == 0 else "_") + mcperiod),
        #"%s/Sherpa221_Ztautau%s.root" % (PATH, ("" if len(mcperiod) == 0 else "_") + mcperiod),
    ],
    sampletype=SampleTypes.Irreducible)

Wjets = DSconfig(colour=ROOT.kViolet - 7,
                 name="Wjets",
                 label="W+Jets",
                 filepath=[
                     "%s/PowHegPy8_Wenu%s.root" % (PATH, ("" if len(mcperiod) == 0 else "_") + mcperiod),
                     "%s/PowHegPy8_Wmunu%s.root" % (PATH, ("" if len(mcperiod) == 0 else "_") + mcperiod),
                     "%s/PowHegPy8_Wtaunu%s.root" % (PATH, ("" if len(mcperiod) == 0 else "_") + mcperiod),
                 ],
                 sampletype=SampleTypes.Irreducible)

VV = DSconfig(colour=ROOT.kGreen - 3,
              name="DiBosons",
              label="VV",
              filepath=[
                  "%s/PowHegPy8_VV%s.root" % (PATH, ("" if len(mcperiod) == 0 else "_") + mcperiod),
              ],
              sampletype=SampleTypes.Irreducible)
