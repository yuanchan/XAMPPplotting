#! /usr/bin/env python
from XAMPPplotting.Defs import *

BasePath = '/ptmp/mpp/niko/Cluster/Output/2018-05-02/stop_ISR_signals/'

# Signal MC

# TT_directTT_200_27 = DSconfig(colour=ROOT.kMagenta,name="TT_directTT_200_27",label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{0}_{1}})=(200,27) GeV",filepath=BasePath+"TT_directTT_200_27_a821_r7676_Input.root",sampletype=SampleTypes.Signal)
# TT_directTT_250_77 = DSconfig(colour=ROOT.kMagenta+2,name="TT_directTT_250_77",label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{0}_{1}})=(250,77) GeV",filepath=BasePath+"TT_directTT_250_77_a821_r7676_Input.root",sampletype=SampleTypes.Signal)
# TT_directTT_300_127 = DSconfig(colour=ROOT.kMagenta+4,name="TT_directTT_300_127",label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{0}_{1}})=(300,127) GeV",filepath=BasePath+"TT_directTT_300_127_a821_r7676_Input.root",sampletype=SampleTypes.Signal)
TT_directTT_350_177 = DSconfig(colour=ROOT.kRed,
                               name="TT_directTT_350_177",
                               label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{0}_{1}})=(350,177) GeV",
                               filepath=BasePath + "TT_directTT_350_177_a821_r7676_Input.root",
                               sampletype=SampleTypes.Signal,
                               markerstyle=ROOT.kOpenCircle,
                               linestyle=1)
# TT_directTT_400_227 = DSconfig(colour=ROOT.kMagenta+8,name="TT_directTT_400_227",label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{0}_{1}})=(400,227) GeV",filepath=BasePath+"TT_directTT_400_227_a821_r7676_Input.root",sampletype=SampleTypes.Signal)
# TT_directTT_450_277 = DSconfig(colour=ROOT.kMagenta+10,name="TT_directTT_450_277",label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{0}_{1}})=(450,277) GeV",filepath=BasePath+"TT_directTT_450_277_a821_r7676_Input.root",sampletype=SampleTypes.Signal)
TT_directTT_500_327 = DSconfig(colour=ROOT.kGreen,
                               name="TT_directTT_500_327",
                               label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{0}_{1}})=(500,327) GeV",
                               filepath=BasePath + "TT_directTT_500_327_a821_r7676_Input.root",
                               sampletype=SampleTypes.Signal,
                               markerstyle=ROOT.kOpenTriangleUp,
                               linestyle=1)
# TT_directTT_550_377 = DSconfig(colour=ROOT.kMagenta+14,name="TT_directTT_550_377",label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{0}_{1}})=(550,377) GeV",filepath=BasePath+"TT_directTT_550_377_a821_r7676_Input.root",sampletype=SampleTypes.Signal)
TT_directTT_600_427 = DSconfig(colour=ROOT.kBlue,
                               name="TT_directTT_600_427",
                               label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{0}_{1}})=(600,427) GeV",
                               filepath=BasePath + "TT_directTT_600_427_a821_r7676_Input.root",
                               sampletype=SampleTypes.Signal,
                               markerstyle=ROOT.kOpenTriangleDown,
                               linestyle=1)
# TT_directTT_650_477 = DSconfig(colour=ROOT.kMagenta+18,name="TT_directTT_650_477",label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{0}_{1}})=(650,477) GeV",filepath=BasePath+"TT_directTT_650_477_a838_r7676_Input.root",sampletype=SampleTypes.Signal)
