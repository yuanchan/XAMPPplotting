#! /usr/bin/env python
from XAMPPplotting.Defs import *

# BasePath = '/ptmp/mpp/niko/Cluster/Output/2018-01-31/CRSTforMatthias_4/'
BasePath = '/ptmp/mpp/niko/Cluster/Output/2018-02-05/forMatthias/'

# Data
Data = DSconfig(lumi=36.1,
                colour=ROOT.kBlack,
                name="Data",
                label="Data",
                filepath=BasePath + "Data_Period_Run2_Input.root",
                sampletype=SampleTypes.Data)

# Signal MC
# TT_directTT_800_1 = DSconfig(colour=ROOT.kMagenta,name="TT_directTT_800_1",label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{0}_{1}})=(800,1)",filepath=BasePath+"TT_directTT_800_1_a821_r7676_Input.root",sampletype=SampleTypes.Signal)
# TT_directTT_1000_1 = DSconfig(colour=ROOT.kMagenta,name="TT_directTT_1000_1",label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{0}_{1}})=(1000,1) GeV",filepath=BasePath+"TT_directTT_1000_1_a821_r7676_Input.root",sampletype=SampleTypes.Signal)

# TT_directTT_600_300 = DSconfig(colour=ROOT.kMagenta,name="TT_directTT_600_300",label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{0}_{1}})=(600,300)",filepath=BasePath+"TT_directTT_600_300_a821_r7676_Input.root",sampletype=SampleTypes.Signal)
# TT_directTT_600_1 = DSconfig(colour=ROOT.kMagenta,name="TT_directTT_600_1",label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{0}_{1}})=(600,1)",filepath=BasePath+"TT_directTT_600_1_a821_r7676_Input.root",sampletype=SampleTypes.Signal)

# TT_onestepBB_400_100_50 = DSconfig(colour=ROOT.kGreen-2,name="TT_onestepBB_400_100_50",label="#splitline{(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{#pm}_{1}},m_{#tilde{#chi}^{0}_{1}})}{=(400,100,50)GeV}",filepath=BasePath+"TT_onestepBB_400_100_50_a821_r7676_Input.root",sampletype=SampleTypes.Signal)

# TT_directTT_500_327 = DSconfig(colour=ROOT.kMagenta,name="TT_directTT_500_327",label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{0}_{1}})=(500,327)",filepath=BasePath+"TT_directTT_500_327_a821_r7676_Input.root",sampletype=SampleTypes.Signal)
# TT_directTT_400_227 = DSconfig(colour=ROOT.kMagenta,name="TT_directTT_400_227",label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{0}_{1}})=(400,227)",filepath=BasePath+"TT_directTT_400_227_a821_r7676_Input.root",sampletype=SampleTypes.Signal)
# TT_directTT_300_127 = DSconfig(colour=ROOT.kMagenta,name="TT_directTT_300_127",label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{0}_{1}})=(300,127)",filepath=BasePath+"TT_directTT_300_127_a821_r7676_Input.root",sampletype=SampleTypes.Signal)

# TT_mixedBT_600_400_200 = DSconfig(colour=ROOT.kBlue+1,name="TT_mixedBT_600_400_200",label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{#pm}_{1}},m_{#tilde{#chi}^{0}_{1}})=(600,400,200) mixed",filepath=BasePath+"TT_mixedBT_600_400_200_Input.root",sampletype=SampleTypes.Signal)
# TT_mixedBT_600_200_100 = DSconfig(colour=ROOT.kBlue+1,name="TT_mixedBT_600_200_100",label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{#pm}_{1}},m_{#tilde{#chi}^{0}_{1}})=(600,200,100) mixed",filepath=BasePath+"TT_mixedBT_600_200_100_Input.root",sampletype=SampleTypes.Signal)

#TT_onestepBB_600_400_200 = DSconfig(colour=ROOT.kGreen-2,name="TT_onestepBB_600_400_200",label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{#pm}_{1}},m_{#tilde{#chi}^{0}_{1}})=(600,400,200)",filepath=BasePath+"TT_onestepBB_600_400_200_Input.root",sampletype=SampleTypes.Signal)
#TT_onestepBB_600_200_100 = DSconfig(colour=ROOT.kGreen-2,name="TT_onestepBB_600_200_100",label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{#pm}_{1}},m_{#tilde{#chi}^{0}_{1}})=(600,200,100)",filepath=BasePath+"TT_onestepBB_600_200_100_Input.root",sampletype=SampleTypes.Signal)
## #
## # # TT_onestepBB_600_200_100 = DSconfig(colour=ROOT.kMagenta,name="TT_onestepBB_600_200_100",label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{#pm}_{1}},m_{#tilde{#chi}^{0}_{1}})=(600,200,100)",filepath=BasePath+"TT_onestepBB_600_200_100_Input.root",sampletype=SampleTypes.Signal)
#TT_onestepBB_700_100_50 = DSconfig(colour=ROOT.kMagenta,name="TT_onestepBB_700_100_50",label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{#pm}_{1}},m_{#tilde{#chi}^{0}_{1}})=(700,100,50)",filepath=BasePath+"TT_onestepBB_700_100_50_Input.root",sampletype=SampleTypes.Signal)
# TT_onestepBB_400_100_50 = DSconfig(colour=ROOT.kMagenta,name="TT_onestepBB_400_100_50",label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{#pm}_{1}},m_{#tilde{#chi}^{0}_{1}})=(400,100,50)",filepath=BasePath+"TT_onestepBB_400_100_50_Input.root",sampletype=SampleTypes.Signal)

# TT_onestepBB_600_300_150 = DSconfig(colour=ROOT.kMagenta,name="TT_onestepBB_600_300_150",label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{#pm}_{1}},m_{#tilde{#chi}^{0}_{1}})=(600,300,150)",filepath=BasePath+"TT_onestepBB_600_300_150_a821_r7676_Input.root",sampletype=SampleTypes.Signal)

# TT_onestepBB_700_100_50 = DSconfig(colour=ROOT.kMagenta,name="TT_onestepBB_700_100_50",label="(700,100,50) BB",filepath=BasePath+"TT_onestepBB_700_100_50_Input.root",sampletype=SampleTypes.Signal)
#
# # TT_directTT_800_1 = DSconfig(colour=ROOT.kMagenta,name="TT_directTT_800_1",label="(800,1) TT",filepath=BasePath+"TT_directTT_800_1_Input.root",sampletype=SampleTypes.Signal)
# TT_mixedBT_600_400_200 = DSconfig(colour=ROOT.kRed-3,name="TT_mixedBT_600_400_200",label="(600,400,200) BT",filepath=BasePath+"TT_mixedBT_600_400_200_Input.root",sampletype=SampleTypes.Signal)
# TT_mixedBT_600_200_100 = DSconfig(colour=ROOT.kRed-3,name="TT_mixedBT_600_200_100",label="(600,200,100) BT",filepath=BasePath+"TT_mixedBT_600_200_100_Input.root",sampletype=SampleTypes.Signal)

# Background MC

ttV = DSconfig(colour=ROOT.kRed + 3,
               name="ttV",
               label="t#bar{t}+V",
               filepath=BasePath + "ttV_Input.root",
               sampletype=SampleTypes.Irreducible)
ttgamma = DSconfig(colour=ROOT.kRed - 6,
                   name="ttgamma",
                   label="t#bar{t}+#gamma",
                   filepath=BasePath + "ttGamma_a821_r7676_Input.root",
                   sampletype=SampleTypes.Irreducible)
Diboson = DSconfig(colour=ROOT.kYellow,
                   name="Diboson",
                   label="Diboson",
                   filepath=BasePath + "DibosonNew_Input.root",
                   sampletype=SampleTypes.Reducible)
SingleTop = DSconfig(colour=ROOT.kGreen + 3,
                     name="SingleTop",
                     label="Single Top",
                     filepath=BasePath + "singleTop_Input.root",
                     sampletype=SampleTypes.Reducible)
ttbar = DSconfig(colour=ROOT.kGreen - 9,
                 name="ttbar",
                 label="t#bar{t}",
                 filepath=BasePath + "ttbar_Input.root",
                 sampletype=SampleTypes.Reducible)
Wjets = DSconfig(colour=ROOT.kAzure + 1,
                 name="Wjets",
                 label="W+jets",
                 filepath=BasePath + "Wjets_Sherpa221_Input.root",
                 sampletype=SampleTypes.Reducible)
Zjets = DSconfig(colour=ROOT.kBlue + 3,
                 name="Zjets",
                 label="Z+jets",
                 filepath=BasePath + "Zjets_Sherpa221_Input.root",
                 sampletype=SampleTypes.Reducible)
SinglePhoton = DSconfig(colour=ROOT.kViolet + 5,
                        name="SinglePhoton",
                        label="#gamma+jets",
                        filepath=BasePath + "SinglePhoton_Input.root",
                        sampletype=SampleTypes.Reducible)
