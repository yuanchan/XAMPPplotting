#! /usr/bin/env python
from XAMPPplotting.Defs import *
from XAMPPplotting.Stop0LColors import *

BasePath = '/ptmp/mpp/niko/Cluster/Output/2018-05-04/stop0L_ISRpresel_sys/'

sigColors = {
    '600_300': ROOT.kMagenta,
    '1000_1': ROOT.kOrange + 6,
    '300_127': ROOT.kMagenta,
    '400_277': ROOT.kOrange + 6,
    '450_277': ROOT.kOrange + 6,
    '500_327': ROOT.kCyan - 3
}
bkgColors = {
    'Z': ROOT.kYellow,
    'W': ROOT.kAzure + 1,
    'ttbar': ROOT.kGreen - 9,
    'ttV': ROOT.kRed + 3,
    'ttGamma': 909,
    'singleTop': ROOT.kGreen + 3,
    'dibosons': 603,
    'QCD': 0,
    'VGamma': ROOT.kCyan
}

# Data
Data = DSconfig(lumi=36.1,
                colour=ROOT.kBlack,
                name="Data",
                label="Data",
                filepath=BasePath + "Data_Period_Run2_Input.root",
                sampletype=SampleTypes.Data)

# Signal MC
TT_directTT_500_327 = DSconfig(colour=sigColors['500_327'],
                               name="TT_directTT_500_327",
                               label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{0}_{1}})=(500,327) GeV",
                               filepath=BasePath + "TT_directTT_500_327_a821_r7676_Input.root",
                               sampletype=SampleTypes.Signal,
                               markerstyle=ROOT.kOpenTriangleUp,
                               linestyle=1)
TT_directTT_450_277 = DSconfig(colour=sigColors['450_277'],
                               name="TT_directTT_450_277",
                               label="(m_{#tilde{t}_{1}},m_{#tilde{#chi}^{0}_{1}})=(450,277) GeV",
                               filepath=BasePath + "TT_directTT_450_277_a821_r7676_Input.root",
                               sampletype=SampleTypes.Signal)

# Background MC
ttV = DSconfig(colour=bkgColors['ttV'],
               name="ttV",
               label="t#bar{t}+V",
               filepath=BasePath + "ttV_Input.root",
               sampletype=SampleTypes.Irreducible)
# ttgamma = DSconfig(colour=bkgColors['ttGamma'],name="ttgamma",label="t#bar{t}+#gamma",filepath=BasePath+"ttGamma_a821_r7676_Input.root",sampletype=SampleTypes.Irreducible)
Diboson = DSconfig(colour=bkgColors['dibosons'],
                   name="Diboson",
                   label="Diboson",
                   filepath=BasePath + "DibosonNew_Input.root",
                   sampletype=SampleTypes.Reducible)
SingleTop = DSconfig(colour=bkgColors['singleTop'],
                     name="SingleTop",
                     label="Single Top",
                     filepath=BasePath + "singleTop_Input.root",
                     sampletype=SampleTypes.Reducible)
ttbar = DSconfig(colour=bkgColors['ttbar'],
                 name="ttbar",
                 label="t#bar{t}",
                 filepath=BasePath + "ttbar_Input.root",
                 sampletype=SampleTypes.Reducible)
Wjets = DSconfig(colour=bkgColors['W'],
                 name="Wjets",
                 label="W+jets",
                 filepath=BasePath + "Wjets_Sherpa221_Input.root",
                 sampletype=SampleTypes.Reducible)
Zjets = DSconfig(colour=bkgColors['Z'],
                 name="Zjets",
                 label="Z+jets",
                 filepath=BasePath + "Zjets_Sherpa221_Input.root",
                 sampletype=SampleTypes.Reducible)
# SinglePhoton = DSconfig(colour=ROOT.kViolet+5,name="SinglePhoton",label="#gamma+jets",filepath=BasePath+"SinglePhoton_Input.root",sampletype=SampleTypes.Reducible)
