#! /usr/bin/env python
import ROOT, os, sys
from XAMPPplotting.Defs import *

Path = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-04-09/Rel20p7/"

C_Name = DSConfigName("Release 20.7")
SignalPath = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-04-09/Rel20p7/"
DataPath = Path

Periods = [Cfg[:Cfg.rfind(".")] for Cfg in os.listdir(Path) if Cfg.startswith("data")]
LUMI = 36.1

# Samples
Data = DSconfig(lumi=LUMI,
                colour=ROOT.kBlack,
                label="data",
                name="data",
                filepath=["%s/%s.root" % (DataPath, P) for P in Periods],
                sampletype=SampleTypes.Data)

ZZ = DSconfig(colour=ROOT.kAzure - 4,
              label="ZZ",
              name="ZZ",
              filepath=[
                  "%s/Sherpa222_ZZ.root" % (Path),
                  "%s/Sherpa_ggZZnoH.root" % (Path),
              ],
              sampletype=SampleTypes.Irreducible,
              TheoUncert=0.1)
Higgs = DSconfig(
    colour=ROOT.kViolet - 9,
    label="Higgs",
    name="VH/H",
    filepath=[
        "%s/PowHegPy8_WH.root" % (Path),
        "%s/PowHegPy8_ZH.root" % (Path),
        "%s/Higgs_VBF.root" % (Path),
        "%s/Higgs_ggF.root" % (Path),
        #"%s/aMcAtNloPy8_ttH.root" % (Path),
    ],
    sampletype=SampleTypes.Irreducible)
VVV = DSconfig(colour=ROOT.kMagenta - 9,
               label="VVV",
               name="VVV",
               filepath=["%s/Sherpa221_VVV.root" % (Path)],
               sampletype=SampleTypes.Irreducible)
TwoFakes = DSconfig(colour=ROOT.kYellow,
                    label="2-fakes",
                    name="TwoFakes",
                    filepath=[
                        "%s/MGPy8EG_HT_Zmumu.root" % (Path),
                        "%s/MGPy8EG_NP_Zee.root" % (Path),
                        "%s/MGPy8EG_NP_Ztautau.root" % (Path),
                        "%s/PowHegPy8_Wjets.root" % (Path),
                        "%s/PowHegPy6_top.root" % (Path),
                    ],
                    sampletype=SampleTypes.Reducible)
#ttbar = DSconfig(colour=ROOT.TColor.GetColor(102, 252, 245),
#                 label="t#bar{t}",
#                 name="ttbar",
#                 filepath=[
#                     "%s/PowHegPy6_top.root" % (Path),
#                 ],
#                 sampletype=SampleTypes.Reducible)
ttV = DSconfig(colour=ROOT.kTeal - 5,
               label="t#bar{t}Z(WW)",
               name="ttV",
               filepath=[
                   "%s/MGPy8EG_ttZ.root" % (Path),
                   "%s/aMCNLO_tWZ.root" % (Path),
                   "%s/MGPy8EG_MultiTop.root" % (Path),
                   "%s/MGPy8EG_ttWW.root" % (Path),
                   "%s/MGPy8EG_ttWZ.root" % (Path),
               ],
               sampletype=SampleTypes.Irreducible)

OneFakeRed = DSconfig(
    colour=ROOT.TColor.GetColor(20, 54, 208),
    label="1-fakes",
    name="one-fakes",
    filepath=[
        "%s/Sherpa221_WZ.root" % (Path),
        "%s/MGPy8EG_ttW.root" % (Path),
        #"%s/Reducible.root" % (Path),
    ],
    sampletype=SampleTypes.Reducible)

#C1C1_800_200_LLE12k = DSconfig (colour=ROOT.kMagenta , label="m_{#tilde{#chi}^{0}_{1}}=800 GeV" , name="C1C1_800_200_LLE12k" , filepath=SignalPath+"C1C1_800_200_LLE12k.root", sampletype=SampleTypes.Signal)
#C1C1_1000_200_LLE12k = DSconfig (colour=ROOT.kBlack , label="m_{#tilde{#chi}^{#pm}_{1}}=1 TeV, #lambda_{12k}" , name="C1C1_1000_200_LLE12k" , filepath=SignalPath+"C1C1_1000_200_LLE12k.root", sampletype=SampleTypes.Signal)

#C1C1_1000_100_LLEi33 = DSconfig(colour=ROOT.kBlue + 2,
#                                label="m_{#tilde{#chi}^{#pm}_{1}}=1200 GeV, #lambda_{i33}",
#                                name="Wino_1200_600_LLEi33",
#                                filepath=SignalPath + "Wino_1200_600_LLEi33.root",
#                                sampletype=SampleTypes.Signal)

#GGM = DSconfig(
#    colour=ROOT.kBlue + 2,
#    label="GGM",
#    name="GGM",
#    filepath=[
#        "%s/GGMHinoZh50_200.root" % (Path),
#    ],
#    sampletype=SampleTypes.Signal)

#
SignalPath = '/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-04-09/Rel20p7/'
GG_1000_100_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='GG_1000_100_LLEi33',
                              label='GG_1000_100_LLEi33',
                              filepath=SignalPath + '/GG_1000_100_LLEi33.root')
GG_1000_10_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                             name='GG_1000_10_LLEi33',
                             label='GG_1000_10_LLEi33',
                             filepath=SignalPath + '/GG_1000_10_LLEi33.root')
GG_1000_500_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='GG_1000_500_LLEi33',
                              label='GG_1000_500_LLEi33',
                              filepath=SignalPath + '/GG_1000_500_LLEi33.root')
GG_1000_50_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                             name='GG_1000_50_LLEi33',
                             label='GG_1000_50_LLEi33',
                             filepath=SignalPath + '/GG_1000_50_LLEi33.root')
GG_1000_990_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='GG_1000_990_LLEi33',
                              label='GG_1000_990_LLEi33',
                              filepath=SignalPath + '/GG_1000_990_LLEi33.root')
GG_1100_100_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='GG_1100_100_LLEi33',
                              label='GG_1100_100_LLEi33',
                              filepath=SignalPath + '/GG_1100_100_LLEi33.root')
GG_1100_1090_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                               name='GG_1100_1090_LLEi33',
                               label='GG_1100_1090_LLEi33',
                               filepath=SignalPath + '/GG_1100_1090_LLEi33.root')
GG_1100_10_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                             name='GG_1100_10_LLEi33',
                             label='GG_1100_10_LLEi33',
                             filepath=SignalPath + '/GG_1100_10_LLEi33.root')
GG_1100_50_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                             name='GG_1100_50_LLEi33',
                             label='GG_1100_50_LLEi33',
                             filepath=SignalPath + '/GG_1100_50_LLEi33.root')
GG_1100_700_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='GG_1100_700_LLEi33',
                              label='GG_1100_700_LLEi33',
                              filepath=SignalPath + '/GG_1100_700_LLEi33.root')
GG_1200_100_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='GG_1200_100_LLEi33',
                              label='GG_1200_100_LLEi33',
                              filepath=SignalPath + '/GG_1200_100_LLEi33.root')
GG_1200_10_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                             name='GG_1200_10_LLEi33',
                             label='GG_1200_10_LLEi33',
                             filepath=SignalPath + '/GG_1200_10_LLEi33.root')
GG_1200_1190_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                               name='GG_1200_1190_LLEi33',
                               label='GG_1200_1190_LLEi33',
                               filepath=SignalPath + '/GG_1200_1190_LLEi33.root')
GG_1200_500_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='GG_1200_500_LLEi33',
                              label='GG_1200_500_LLEi33',
                              filepath=SignalPath + '/GG_1200_500_LLEi33.root')
GG_1200_50_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                             name='GG_1200_50_LLEi33',
                             label='GG_1200_50_LLEi33',
                             filepath=SignalPath + '/GG_1200_50_LLEi33.root')
GG_1300_100_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='GG_1300_100_LLEi33',
                              label='GG_1300_100_LLEi33',
                              filepath=SignalPath + '/GG_1300_100_LLEi33.root')
GG_1300_10_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                             name='GG_1300_10_LLEi33',
                             label='GG_1300_10_LLEi33',
                             filepath=SignalPath + '/GG_1300_10_LLEi33.root')
GG_1300_1290_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                               name='GG_1300_1290_LLEi33',
                               label='GG_1300_1290_LLEi33',
                               filepath=SignalPath + '/GG_1300_1290_LLEi33.root')
GG_1300_50_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                             name='GG_1300_50_LLEi33',
                             label='GG_1300_50_LLEi33',
                             filepath=SignalPath + '/GG_1300_50_LLEi33.root')
GG_1300_700_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='GG_1300_700_LLEi33',
                              label='GG_1300_700_LLEi33',
                              filepath=SignalPath + '/GG_1300_700_LLEi33.root')
GG_1400_100_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='GG_1400_100_LLEi33',
                              label='GG_1400_100_LLEi33',
                              filepath=SignalPath + '/GG_1400_100_LLEi33.root')
GG_1400_10_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                             name='GG_1400_10_LLEi33',
                             label='GG_1400_10_LLEi33',
                             filepath=SignalPath + '/GG_1400_10_LLEi33.root')
GG_1400_1390_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                               name='GG_1400_1390_LLEi33',
                               label='GG_1400_1390_LLEi33',
                               filepath=SignalPath + '/GG_1400_1390_LLEi33.root')
GG_1400_500_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='GG_1400_500_LLEi33',
                              label='GG_1400_500_LLEi33',
                              filepath=SignalPath + '/GG_1400_500_LLEi33.root')
GG_1400_50_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                             name='GG_1400_50_LLEi33',
                             label='GG_1400_50_LLEi33',
                             filepath=SignalPath + '/GG_1400_50_LLEi33.root')
GG_1400_900_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='GG_1400_900_LLEi33',
                              label='GG_1400_900_LLEi33',
                              filepath=SignalPath + '/GG_1400_900_LLEi33.root')
GG_1500_100_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='GG_1500_100_LLEi33',
                              label='GG_1500_100_LLEi33',
                              filepath=SignalPath + '/GG_1500_100_LLEi33.root')
GG_1500_10_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                             name='GG_1500_10_LLEi33',
                             label='GG_1500_10_LLEi33',
                             filepath=SignalPath + '/GG_1500_10_LLEi33.root')
GG_1500_1100_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                               name='GG_1500_1100_LLEi33',
                               label='GG_1500_1100_LLEi33',
                               filepath=SignalPath + '/GG_1500_1100_LLEi33.root')
GG_1500_1490_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                               name='GG_1500_1490_LLEi33',
                               label='GG_1500_1490_LLEi33',
                               filepath=SignalPath + '/GG_1500_1490_LLEi33.root')
GG_1500_300_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='GG_1500_300_LLEi33',
                              label='GG_1500_300_LLEi33',
                              filepath=SignalPath + '/GG_1500_300_LLEi33.root')
GG_1500_50_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                             name='GG_1500_50_LLEi33',
                             label='GG_1500_50_LLEi33',
                             filepath=SignalPath + '/GG_1500_50_LLEi33.root')
GG_1500_700_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='GG_1500_700_LLEi33',
                              label='GG_1500_700_LLEi33',
                              filepath=SignalPath + '/GG_1500_700_LLEi33.root')
GG_1600_100_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='GG_1600_100_LLEi33',
                              label='GG_1600_100_LLEi33',
                              filepath=SignalPath + '/GG_1600_100_LLEi33.root')
GG_1600_10_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                             name='GG_1600_10_LLEi33',
                             label='GG_1600_10_LLEi33',
                             filepath=SignalPath + '/GG_1600_10_LLEi33.root')
GG_1600_1300_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                               name='GG_1600_1300_LLEi33',
                               label='GG_1600_1300_LLEi33',
                               filepath=SignalPath + '/GG_1600_1300_LLEi33.root')
GG_1600_1590_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                               name='GG_1600_1590_LLEi33',
                               label='GG_1600_1590_LLEi33',
                               filepath=SignalPath + '/GG_1600_1590_LLEi33.root')
GG_1600_500_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='GG_1600_500_LLEi33',
                              label='GG_1600_500_LLEi33',
                              filepath=SignalPath + '/GG_1600_500_LLEi33.root')
GG_1600_50_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                             name='GG_1600_50_LLEi33',
                             label='GG_1600_50_LLEi33',
                             filepath=SignalPath + '/GG_1600_50_LLEi33.root')
GG_1600_900_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='GG_1600_900_LLEi33',
                              label='GG_1600_900_LLEi33',
                              filepath=SignalPath + '/GG_1600_900_LLEi33.root')
GG_1700_100_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='GG_1700_100_LLEi33',
                              label='GG_1700_100_LLEi33',
                              filepath=SignalPath + '/GG_1700_100_LLEi33.root')
GG_1700_10_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                             name='GG_1700_10_LLEi33',
                             label='GG_1700_10_LLEi33',
                             filepath=SignalPath + '/GG_1700_10_LLEi33.root')
GG_1700_1100_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                               name='GG_1700_1100_LLEi33',
                               label='GG_1700_1100_LLEi33',
                               filepath=SignalPath + '/GG_1700_1100_LLEi33.root')
GG_1700_1500_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                               name='GG_1700_1500_LLEi33',
                               label='GG_1700_1500_LLEi33',
                               filepath=SignalPath + '/GG_1700_1500_LLEi33.root')
GG_1700_1690_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                               name='GG_1700_1690_LLEi33',
                               label='GG_1700_1690_LLEi33',
                               filepath=SignalPath + '/GG_1700_1690_LLEi33.root')
GG_1700_300_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='GG_1700_300_LLEi33',
                              label='GG_1700_300_LLEi33',
                              filepath=SignalPath + '/GG_1700_300_LLEi33.root')
GG_1700_50_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                             name='GG_1700_50_LLEi33',
                             label='GG_1700_50_LLEi33',
                             filepath=SignalPath + '/GG_1700_50_LLEi33.root')
GG_1700_700_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='GG_1700_700_LLEi33',
                              label='GG_1700_700_LLEi33',
                              filepath=SignalPath + '/GG_1700_700_LLEi33.root')
GG_1800_100_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='GG_1800_100_LLEi33',
                              label='GG_1800_100_LLEi33',
                              filepath=SignalPath + '/GG_1800_100_LLEi33.root')
GG_1800_10_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                             name='GG_1800_10_LLEi33',
                             label='GG_1800_10_LLEi33',
                             filepath=SignalPath + '/GG_1800_10_LLEi33.root')
GG_1800_1300_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                               name='GG_1800_1300_LLEi33',
                               label='GG_1800_1300_LLEi33',
                               filepath=SignalPath + '/GG_1800_1300_LLEi33.root')
GG_1800_1700_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                               name='GG_1800_1700_LLEi33',
                               label='GG_1800_1700_LLEi33',
                               filepath=SignalPath + '/GG_1800_1700_LLEi33.root')
GG_1800_1790_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                               name='GG_1800_1790_LLEi33',
                               label='GG_1800_1790_LLEi33',
                               filepath=SignalPath + '/GG_1800_1790_LLEi33.root')
GG_1800_500_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='GG_1800_500_LLEi33',
                              label='GG_1800_500_LLEi33',
                              filepath=SignalPath + '/GG_1800_500_LLEi33.root')
GG_1800_50_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                             name='GG_1800_50_LLEi33',
                             label='GG_1800_50_LLEi33',
                             filepath=SignalPath + '/GG_1800_50_LLEi33.root')
GG_1800_900_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='GG_1800_900_LLEi33',
                              label='GG_1800_900_LLEi33',
                              filepath=SignalPath + '/GG_1800_900_LLEi33.root')
GG_1900_100_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='GG_1900_100_LLEi33',
                              label='GG_1900_100_LLEi33',
                              filepath=SignalPath + '/GG_1900_100_LLEi33.root')
GG_1900_10_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                             name='GG_1900_10_LLEi33',
                             label='GG_1900_10_LLEi33',
                             filepath=SignalPath + '/GG_1900_10_LLEi33.root')
GG_1900_1100_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                               name='GG_1900_1100_LLEi33',
                               label='GG_1900_1100_LLEi33',
                               filepath=SignalPath + '/GG_1900_1100_LLEi33.root')
GG_1900_1500_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                               name='GG_1900_1500_LLEi33',
                               label='GG_1900_1500_LLEi33',
                               filepath=SignalPath + '/GG_1900_1500_LLEi33.root')
GG_1900_1890_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                               name='GG_1900_1890_LLEi33',
                               label='GG_1900_1890_LLEi33',
                               filepath=SignalPath + '/GG_1900_1890_LLEi33.root')
GG_1900_300_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='GG_1900_300_LLEi33',
                              label='GG_1900_300_LLEi33',
                              filepath=SignalPath + '/GG_1900_300_LLEi33.root')
GG_1900_50_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                             name='GG_1900_50_LLEi33',
                             label='GG_1900_50_LLEi33',
                             filepath=SignalPath + '/GG_1900_50_LLEi33.root')
GG_1900_700_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='GG_1900_700_LLEi33',
                              label='GG_1900_700_LLEi33',
                              filepath=SignalPath + '/GG_1900_700_LLEi33.root')
GG_2000_100_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='GG_2000_100_LLEi33',
                              label='GG_2000_100_LLEi33',
                              filepath=SignalPath + '/GG_2000_100_LLEi33.root')
GG_2000_10_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                             name='GG_2000_10_LLEi33',
                             label='GG_2000_10_LLEi33',
                             filepath=SignalPath + '/GG_2000_10_LLEi33.root')
GG_2000_1300_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                               name='GG_2000_1300_LLEi33',
                               label='GG_2000_1300_LLEi33',
                               filepath=SignalPath + '/GG_2000_1300_LLEi33.root')
GG_2000_1700_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                               name='GG_2000_1700_LLEi33',
                               label='GG_2000_1700_LLEi33',
                               filepath=SignalPath + '/GG_2000_1700_LLEi33.root')
GG_2000_1990_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                               name='GG_2000_1990_LLEi33',
                               label='GG_2000_1990_LLEi33',
                               filepath=SignalPath + '/GG_2000_1990_LLEi33.root')
GG_2000_500_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='GG_2000_500_LLEi33',
                              label='GG_2000_500_LLEi33',
                              filepath=SignalPath + '/GG_2000_500_LLEi33.root')
GG_2000_50_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                             name='GG_2000_50_LLEi33',
                             label='GG_2000_50_LLEi33',
                             filepath=SignalPath + '/GG_2000_50_LLEi33.root')
GG_2000_900_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='GG_2000_900_LLEi33',
                              label='GG_2000_900_LLEi33',
                              filepath=SignalPath + '/GG_2000_900_LLEi33.root')
