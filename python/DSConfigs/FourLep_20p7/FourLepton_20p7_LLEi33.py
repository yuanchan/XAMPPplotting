#! /usr/bin/env python
import ROOT, os, sys
from XAMPPplotting.Defs import *

Path = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-04-08/Rel20p7/"
SignalPath = '/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-04-08/Rel20p7/'

C_Name = DSConfigName("Release 20.7")
SignalPath = Path
DataPath = Path
mc_period = ""
Periods = [Cfg[:Cfg.rfind(".")] for Cfg in os.listdir(Path) if Cfg.startswith("data")]
LUMI = 36.5

# Samples
Data = DSconfig(lumi=LUMI,
                colour=ROOT.kBlack,
                label="data",
                name="data",
                filepath=["%s/%s.root" % (DataPath, P) for P in Periods],
                sampletype=SampleTypes.Data)

ZZ = DSconfig(colour=ROOT.kAzure - 4,
              label="ZZ",
              name="ZZ",
              filepath=[
                  "%s/Sherpa222_ZZ.root" % (Path),
                  "%s/Sherpa_ggZZnoH.root" % (Path),
              ],
              sampletype=SampleTypes.Irreducible,
              TheoUncert=0.1)
Higgs = DSconfig(
    colour=ROOT.kViolet - 9,
    label="Higgs",
    name="VH/H",
    filepath=[
        "%s/PowHegPy8_WH.root" % (Path),
        "%s/PowHegPy8_ZH.root" % (Path),
        "%s/Higgs_VBF.root" % (Path),
        "%s/Higgs_ggF.root" % (Path),
        #"%s/aMcAtNloPy8_ttH.root" % (Path),
    ],
    sampletype=SampleTypes.Irreducible)
VVV = DSconfig(colour=ROOT.kMagenta - 9,
               label="VVV",
               name="VVV",
               filepath=["%s/Sherpa221_VVV.root" % (Path)],
               sampletype=SampleTypes.Irreducible)
TwoFakes = DSconfig(colour=ROOT.kYellow,
                    label="2-fakes",
                    name="TwoFakes",
                    filepath=[
                        "%s/MGPy8EG_HT_Zmumu.root" % (Path),
                        "%s/MGPy8EG_NP_Zee.root" % (Path),
                        "%s/MGPy8EG_NP_Ztautau.root" % (Path),
                        "%s/PowHegPy8_Wjets.root" % (Path),
                        "%s/PowHegPy6_top.root" % (Path),
                    ],
                    sampletype=SampleTypes.Reducible)
#ttbar = DSconfig(colour=ROOT.TColor.GetColor(102, 252, 245),
#                 label="t#bar{t}",
#                 name="ttbar",
#                 filepath=[
#                     "%s/PowHegPy6_top.root" % (Path),
#                 ],
#                 sampletype=SampleTypes.Reducible)
ttV = DSconfig(colour=ROOT.kTeal - 5,
               label="t#bar{t}Z(WW)",
               name="ttV",
               filepath=[
                   "%s/MGPy8EG_ttZ.root" % (Path),
                   "%s/aMCNLO_tWZ.root" % (Path),
                   "%s/MGPy8EG_MultiTop.root" % (Path),
                   "%s/MGPy8EG_ttWW.root" % (Path),
                   "%s/MGPy8EG_ttWZ.root" % (Path),
               ],
               sampletype=SampleTypes.Irreducible)

OneFakeRed = DSconfig(
    colour=ROOT.TColor.GetColor(20, 54, 208),
    label="1-fakes",
    name="one-fakes",
    filepath=[
        "%s/Sherpa221_WZ.root" % (Path),
        "%s/MGPy8EG_ttW.root" % (Path),
        #"%s/Reducible.root" % (Path),
    ],
    sampletype=SampleTypes.Reducible)

VVz_700_200_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                              name='VVz_700_200_LLEi33',
                              label='#tilde{W} (700,200)',
                              filepath=SignalPath + '/VVz_700_200_LLEi33.root',
                              colour=ROOT.kMagenta)

LV_600_300_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                             name='LV_600_300_LLEi33',
                             label='#tilde{#it{l}}/#tilde{#nu} (600,300)',
                             filepath=SignalPath + '/LV_600_300_LLEi33.root',
                             colour=ROOT.kTeal)
LV_800_300_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                             name='LV_800_300_LLEi33',
                             label='#tilde{#it{l}}/#tilde{#nu} (800,300)',
                             filepath=SignalPath + '/LV_800_300_LLEi33.root',
                             colour=ROOT.kOrange)
VVz_700_50_LLEi33 = DSconfig(sampletype=SampleTypes.Signal,
                             name='VVz_700_50_LLEi33',
                             label='#tilde{W} (700,50)',
                             filepath=SignalPath + '/VVz_700_50_LLEi33.root',
                             colour=ROOT.kViolet + 1)
