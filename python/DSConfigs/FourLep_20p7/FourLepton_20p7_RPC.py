#! /usr/bin/env python
import ROOT, os, sys
from XAMPPplotting.Defs import *

C_Name = DSConfigName("Release 20.7")
SignalPath = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-04-10/GGM_20p7/"
Path = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-04-10/GGM_20p7/"

DataPath = Path

Periods = [Cfg[:Cfg.rfind(".")] for Cfg in os.listdir(Path) if Cfg.startswith("data")]
LUMI = 36.1

# Samples
#Data = DSconfig(lumi=LUMI,
#                colour=ROOT.kBlack,
#                label="data",
#                name="data",
#                filepath=["%s/.root" % (DataPath, P) for P in Periods],
#                sampletype=SampleTypes.Data)

ZZ = DSconfig(colour=ROOT.kAzure - 4,
              label="ZZ",
              name="ZZ",
              filepath=[
                  "%s/Sherpa222_VV.root" % (Path),
                  "%s/Sherpa222_ggZZ.root" % (Path),
              ],
              sampletype=SampleTypes.Irreducible,
              TheoUncert=0.1)
Higgs = DSconfig(
    colour=ROOT.kViolet - 9,
    label="Higgs",
    name="VH/H",
    filepath=[
        "%s/PowHegPy8_ZH.root" % (Path),
        "%s/PowHegPy8_WH.root" % (Path),
        #"%s/PowHegPy8_ggH.root" % (Path),
        #"%s/PowHegPy8_VBFH.root" % (Path),
        "%s/aMcAtNloPy8_ttH.root" % (Path),
    ],
    sampletype=SampleTypes.Irreducible)
VVV = DSconfig(colour=ROOT.kMagenta - 9,
               label="VVV",
               name="VVV",
               filepath=["%s/Sherpa221_VVV.root" % (Path)],
               sampletype=SampleTypes.Irreducible)
ZJets = DSconfig(colour=ROOT.kYellow,
                 label="V+jets",
                 name="Vjets",
                 filepath=[
                     "%s/PowHegPy8_Zee.root" % (Path),
                     "%s/PowHegPy8_Zmumu.root" % (Path),
                     "%s/PowHegPy8_Ztautau.root" % (Path),
                     "%s/PowHegPy8_Wenu.root" % (Path),
                     "%s/PowHegPy8_Wmunu.root" % (Path),
                     "%s/PowHegPy8_Wtaunu.root" % (Path),
                 ],
                 sampletype=SampleTypes.Reducible)
ttbar = DSconfig(colour=ROOT.kYellow + 1,
                 label="t#bar{t}",
                 name="ttbar",
                 filepath=[
                     "%s/PowHegPy8_ttbar_incl.root" % (Path),
                     "%s/PowhegPy_top.root" % (Path),
                 ],
                 sampletype=SampleTypes.Reducible)
ttV = DSconfig(colour=ROOT.kTeal - 5,
               label="t#bar{t}Z(WW)",
               name="ttV",
               filepath=[
                   "%s/aMCatNLOPy8_ttZ.root" % (Path),
                   "%s/aMcAtNlo_tWZ.root" % (Path),
                   "%s/MG5Py8_4t.root" % (Path),
                   "%s/MG5Py8_ttWW.root" % (Path),
                   "%s/MG5Py8_ttWZ.root" % (Path),
               ],
               sampletype=SampleTypes.Irreducible)

OneFakeRed = DSconfig(colour=ROOT.TColor.GetColor(20, 54, 208),
                      label="1-fakes",
                      name="one-fakes",
                      filepath=[
                          "%s/Sherpa221_VV.root" % (Path),
                          "%s/aMCatNLOPy8_ttW.root" % (Path),
                      ],
                      sampletype=SampleTypes.Reducible)

SignalPath = '/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-04-10/GGM_20p7/'

GGM_110_25 = DSconfig(sampletype=SampleTypes.Signal,
                      name='GGM_110_50',
                      label='GGM (110,25)',
                      filepath=SignalPath + '/GGM_110_25.root',
                      colour=ROOT.kMagenta)
GGM_400_50 = DSconfig(sampletype=SampleTypes.Signal,
                      name='GGM_400_50',
                      label='GGM (400,50)',
                      filepath=SignalPath + '/GGM_400_50.root',
                      colour=ROOT.kTeal)
GGM_300_50 = DSconfig(sampletype=SampleTypes.Signal,
                      name='GGM_300_50',
                      label='GGM (300,50)',
                      filepath=SignalPath + '/GGM_300_50.root',
                      colour=ROOT.kAzure + 1)
GGM_95_25 = DSconfig(sampletype=SampleTypes.Signal,
                     name='GGM_95_25',
                     label='GGM (95,25)',
                     filepath=SignalPath + '/GGM_95_25.root',
                     colour=ROOT.kOrange)
