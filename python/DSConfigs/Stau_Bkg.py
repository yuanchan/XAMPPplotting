#! /usr/bin/env python

from XAMPPplotting.Defs import *
from XAMPPplotting.FileUtils import ReadInputConfig, ResolvePath
from XAMPPplotting.CheckMetaData import GetNormalizationDB
from XAMPPplotting.CalculateLumiFromIlumicalc import CalculateRecordedLumi
from XAMPPplotting.FileStructureHandler import GetSystPairer

GetSystPairer().create_group("JET", "jets", ROOT.XAMPP.SystematicGroup.UpOnly)
GetSystPairer().create_group("JET", "jets", ROOT.XAMPP.SystematicGroup.DownOnly)

GetSystPairer().create_group("MUON", "#mu", ROOT.XAMPP.SystematicGroup.UpOnly)
GetSystPairer().create_group("MUON", "#mu", ROOT.XAMPP.SystematicGroup.DownOnly)

GetSystPairer().create_group("EG", "ele", ROOT.XAMPP.SystematicGroup.UpOnly)
GetSystPairer().create_group("EG", "ele", ROOT.XAMPP.SystematicGroup.DownOnly)

for eg in [
        "EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR",
        "EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR",
        "EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR",
        "EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR",
]:
    GetSystPairer().find_envelope("EG_1DOWN").append_systematic("%s__1down" % (eg))
    GetSystPairer().find_envelope("EG_1UP").append_systematic("%s__1up" % (eg))

#GetSystPairer().create_group("TAUS", "#tau", ROOT.XAMPP.SystematicGroup.UpOnly)
#GetSystPairer().create_group("TAUS", "#tau", ROOT.XAMPP.SystematicGroup.DownOnly)

#GetSystPairer().create_group("FT_EFF", "b-tagging", ROOT.XAMPP.SystematicGroup.UpOnly)
#GetSystPairer().create_group("FT_EFF", "b-tagging", ROOT.XAMPP.SystematicGroup.DownOnly)

#GetSystPairer().create_group("MET", "E_{T}^{miss}")
#GetSystPairer().set_systematic_title("PRW_DATASF", "<#mu>")

BasePath = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-01-27/lephad_ff_applied/"
BasePath = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-06-07/Stau_FakeTest/"
DoReducible = True

ConfigPath = ResolvePath("XAMPPplotting/InputConf/LepHadStau/Data/")

Periods = [Cfg[:Cfg.rfind(".")] for Cfg in os.listdir(BasePath) if Cfg.find("data") == 0]
ROOT_Files = []
#for P in Periods:
#    ROOT_Files += ReadInputConfig("%s/%s.conf" % (ConfigPath, P))
lumi = 138.9

V_Jets_Generators = {
    "Sherpa": {
        "Zjets": [
            "Sherpa221_Zee.root",
            "Sherpa221_Zmumu.root",
            "Sherpa221_Ztautau.root",
        ],
        "Wjets": [
            "Sherpa221_Wenu.root",
            "Sherpa221_Wmunu.root",
            "Sherpa221_Wtaunu.root",
        ],
        "Label": "Sh"
    },
    "MG5": {
        "Zjets": ["MG5Py8_Zee.root", "MG5Py8_Zmumu.root", "MG5Py8_Ztautau.root"],
        "Wjets": ["MGPy8EG_Wenu.root", "MGPy8EG_Wmunu.root", "MGPy8EG_Wtaunu.root"],
        "Label": "MG5",
    },
    "PowHeg": {
        "Zjets": [
            "PowHegPy8_Zee.root", "PowHegPy8_Zmumu.root", "PowHegPy8_Ztautau.root", "PowHegPy8_DYee.root", "PowHegPy8_DYmumu.root",
            "PowHegPy8_DYtautau.root"
        ],
        "Wjets": [
            "PowHegPy8_Wenu.root", "PowHegPy8_Wmunu.root", "PowHegPy8_Wtaunu.root", "PowHegPy8_DYenu.root", "PowHegPy8_DYmunu.root",
            "PowHegPy8_DYtaunu.root"
        ],
        "Label":
        "PH",
    }
}

gen_to_pick = "PowHeg"
Data = DSconfig(colour=ROOT.kBlack,
                fillstyle=0,
                name="Data",
                label="data",
                filepath=["%s/%s.root" % (BasePath, P) for P in Periods],
                sampletype=SampleTypes.Data,
                lumi=lumi)

VV = DSconfig(
    colour=ROOT.kRed + 3,
    #              fillstyle = 0,
    name="Sherpa_VV",
    label="VV",
    filepath=[BasePath + "Sherpa222_VV.root", BasePath + "Higgs.root"],
    sampletype=SampleTypes.Irreducible)

Zjets = DSconfig(
    colour=ROOT.kBlue - 6,
    #                  fillstyle = 0,
    name="Zjets",
    label="Z+jets (%s)" % (V_Jets_Generators[gen_to_pick]["Label"]),
    filepath=["%s/%s" % (BasePath, F) for F in V_Jets_Generators[gen_to_pick]["Zjets"]],
    sampletype=SampleTypes.Irreducible)

Wjets = DSconfig(
    colour=ROOT.kOrange,
    #                  fillstyle = 0,
    name="Wjets",
    label="W+jets (%s)" % (V_Jets_Generators[gen_to_pick]["Label"]),
    filepath=["%s/%s" % (BasePath, F) for F in V_Jets_Generators[gen_to_pick]["Wjets"]],
    sampletype=SampleTypes.Irreducible)

Top = DSconfig(
    colour=ROOT.kGreen + 2,
    name="PowHegPy8_top",
    label="top",
    filepath=[
        BasePath + "PowHegPy8_ttbar_incl.root",
        BasePath + "PowHegPy8_ttbar_MET_sliced.root",
        BasePath + "PowHegPy8_ttbar_HT_sliced.root",
        BasePath + "PowHegPy8_single_top_s_chan.root",
        BasePath + "PowHegPy8_single_top_t_chan.root",
        #BasePath + "PowHegPy8_single_top_Wt_chan_incl.root",
        BasePath + "MG5_aMCatNLO_Py8_ttV_tV_tVV.root",
        BasePath + "MG5Py8_multi_t.root",
    ],
    sampletype=SampleTypes.Irreducible)

if DoReducible:
    Reducible = DSconfig(
        colour=ROOT.kGray + 2,
        name="datadriven",
        label="reducible",
        filepath=BasePath + "Reducible_PowHeg.root",
        #[
        #    BasePath + "DD_PowHegPy8_ttbar_incl.root",
        #    BasePath + "DD_PowHegPy8_ttbar_MET_sliced.root",
        #    BasePath + "DD_PowHegPy8_ttbar_HT_sliced.root",
        #    BasePath + "DD_PowHegPy8_single_top_s_chan.root",
        #    BasePath + "DD_PowHegPy8_single_top_t_chan.root",
        #    BasePath + "DD_MG5_aMCatNLO_Py8_ttV_tV_tVV.root",
        #    BasePath + "DD_MG5Py8_multi_t.root",
        #    BasePath + "DD_Sherpa222_VV.root",
        #    BasePath + "DD_Higgs.root",
        #] + ["%s/DD_%s.root" % (BasePath, P) for P in Periods] +
        #["%s/DD_%s" % (BasePath, F) for F in V_Jets_Generators[gen_to_pick]["Zjets"] + V_Jets_Generators[gen_to_pick]["Wjets"]],
        sampletype=SampleTypes.Reducible)
