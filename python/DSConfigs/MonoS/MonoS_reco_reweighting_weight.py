#!/usr/bin/env python
import os
from XAMPPplotting.Defs import *
ROOT.gStyle.SetOptStat(0)
BasePath = '/ptmp/mpp/pgadow/Cluster/OUTPUT/2018-12-12/testhad/'

# Specify samples
# Signal MC
ds_signal = os.path.join(BasePath, "monoSwwalldm200hs160mzp1000reweightWithMET2500.root")
monoSWW_zp1000_dm200_hs160_MET = DSconfig(name="monoSWW_zp1000_dm200_hs160_MET",
                                          label="(m_{Z'},m_{hs})=(1000#rightarrow2500(MET),160) GeV",
                                          colour=ROOT.kRed,
                                          filepath=ds_signal,
                                          sampletype=SampleTypes.Signal)
ds_signal = os.path.join(BasePath, "monoSwwalldm200hs160mzp1000reweightWithSPT2500.root")
monoSWW_zp1000_dm200_hs160_SPT = DSconfig(name="monoSWW_zp1000_dm200_hs160_SPT",
                                          label="(m_{Z'},m_{hs})=(1000#rightarrow2500(SPT),160) GeV",
                                          colour=ROOT.kViolet,
                                          filepath=ds_signal,
                                          sampletype=SampleTypes.Signal)

ds_signal = os.path.join(BasePath, "monoSwwalldm200hs160mzp1000reweightWith2DMETSPT2500.root")
monoSWW_zp1000_dm200_hs160_2DMETSPT = DSconfig(name="monoSWW_zp1000_dm200_hs160_2DMETSPT",
                                               label="(m_{Z'},m_{hs})=(1000#rightarrow2500(2D),160) GeV",
                                               colour=ROOT.kCyan,
                                               filepath=ds_signal,
                                               sampletype=SampleTypes.Signal)

ds_signal = os.path.join(BasePath, "monoSwwalldm200hs160mzp2500.root")
monoSWW_zp2500_dm200_hs160 = DSconfig(name="monoSWW_zp2500_dm200_hs160",
                                      label="(m_{Z'},m_{hs})=(2500,160) GeV",
                                      colour=ROOT.kBlue,
                                      filepath=ds_signal,
                                      sampletype=SampleTypes.Signal)
