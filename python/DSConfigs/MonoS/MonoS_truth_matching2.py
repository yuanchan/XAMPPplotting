#!/usr/bin/env python
import os
from XAMPPplotting.Defs import *
ROOT.gStyle.SetOptStat(0)
BasePath = '/nfs/dust/atlas/user/pgadow/monoS/plotting/truth/'

# Specify samples
# Signal MC
ds_signal = os.path.join(BasePath, "monoSWW_CKKWL_01jet_zp2500_dm200_hs160.root")
monoSWW_CKKWL_01jet_zp2500_dm200_hs160 = DSconfig(name="monoSWW_CKKWL_01jet_zp2500_dm200_hs160",
                                                  label="CKKW: (m_{Z'},m_{hs},m_{DM})=(2500,160,200)",
                                                  colour=ROOT.kRed,
                                                  filepath=ds_signal,
                                                  sampletype=SampleTypes.Signal)
ds_signal = os.path.join(BasePath, "monoSWW_MLM_01jet_zp2500_dm200_hs160.root")
monoSWW_MLM_01jet_zp2500_dm200_hs160 = DSconfig(name="monoSWW_MLM_01jet_zp2500_dm200_hs160",
                                                label="MLM: (m_{Z'},m_{hs},m_{DM})=(2500,160,200)",
                                                colour=ROOT.kViolet,
                                                filepath=ds_signal,
                                                sampletype=SampleTypes.Signal)
ds_signal = os.path.join(BasePath, "monoSWW_zp2500_dm200_hs160.root")
monoSWW_zp2500_dm200_hs160 = DSconfig(name="monoSWW_zp2500_dm200_hs160",
                                      label="simple: (m_{Z'},m_{hs},m_{DM})=(2500,160,200)",
                                      colour=ROOT.kRed - 7,
                                      filepath=ds_signal,
                                      sampletype=SampleTypes.Signal)
