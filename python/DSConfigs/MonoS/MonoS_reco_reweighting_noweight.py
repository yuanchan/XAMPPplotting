#!/usr/bin/env python
import os
from XAMPPplotting.Defs import *
ROOT.gStyle.SetOptStat(0)
BasePath = '/ptmp/mpp/pgadow/Cluster/OUTPUT/2018-12-12/testhad_noweight/'

# Specify samples
# Signal MC
ds_signal = os.path.join(BasePath, "monoSwwalldm200hs160mzp1000reweightWithMET2500.root")
monoSWW_zp1000_dm200_hs160_MET = DSconfig(name="monoSWW_zp1000_dm200_hs160_MET",
                                          label="(m_{Z'},m_{hs})=(1000,160) GeV",
                                          colour=ROOT.kRed,
                                          filepath=ds_signal,
                                          sampletype=SampleTypes.Signal)

ds_signal = os.path.join(BasePath, "monoSwwalldm200hs160mzp2500.root")
monoSWW_zp2500_dm200_hs160 = DSconfig(name="monoSWW_zp2500_dm200_hs160",
                                      label="(m_{Z'},m_{hs})=(2500,160) GeV",
                                      colour=ROOT.kBlue,
                                      filepath=ds_signal,
                                      sampletype=SampleTypes.Signal)
