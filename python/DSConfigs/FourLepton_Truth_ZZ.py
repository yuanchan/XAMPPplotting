#! /usr/bin/env python
from XAMPPplotting.Defs import *
from XAMPPplotting.FileStructureHandler import GetFileHandler, GetSystPairer

#Path = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2019-05-07/Truth4L_Syst/"
#Path = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2019-12-12/4LTrue_Sys/"

Path = " /ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-01-26/Truth_Syst/"
Path1 = " /ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-01-26/Truth_Syst_combined/"

GetSystPairer().create_deviation("PDFset=261", "PDF")
GetSystPairer().set_systematic_title("FACTOR", "#mu_{F}")
GetSystPairer().set_systematic_title("RENORM", "#mu_{R}")
GetSystPairer().set_systematic_title("ALPHAS", "#alpha_{s}")
GetSystPairer().set_systematic_title("RENORM_AND-FACTOR", "#mu_{F}#oplus#mu_{R}")
GetSystPairer().set_systematic_title("VV", "Generator")
GetSystPairer().pair_systematics("QSF025", "QSF4")
GetSystPairer().pair_systematics("CKKW15", "CKKW30")

GetSystPairer().set_systematic_title("QSF025", "#mu_{Q}")
GetSystPairer().set_systematic_title("QSF4", "#mu_{Q}")

#GetSystPairer().exclude_systematic("PDFset=261054")
#GetSystPairer().exclude_systematic("PDFset=261095")
#GetSystPairer().exclude_systematic("PDFset=261031")
#GetSystPairer().exclude_systematic("PDFset=261049")

Nominal = DSconfig(
    colour=ROOT.kBlack,  #ROOT.TColor.GetColor(111,224,5) , 
    label="ZZ (Nominal)",
    name="Nominal",
    filepath=Path1 + "Sherpa222_VV.root",
    fillstyle=0,
    markerstyle=0,
    markersize=0,
    sampletype=SampleTypes.Irreducible,
    LegendOrder=3)

GetFileHandler().LoadFile(Nominal.Filepaths[0])

RENORM__1UP = DSconfig(colour=ROOT.kGreen + 3,
                       label="ZZ (#mu_{R}#uparrow)",
                       name="RENORM__1UP",
                       Nominal="RENORM__1UP",
                       ExcludeSyst=[S for S in GetSystPairer().get_systematics()],
                       filepath=Path + "Sherpa222_VV.root",
                       fillstyle=0,
                       markerstyle=0,
                       markersize=0,
                       sampletype=SampleTypes.Irreducible,
                       LegendOrder=4)

RENORM__1DOWN = DSconfig(colour=ROOT.kGreen - 2,
                         label="ZZ (#mu_{R}#downarrow)",
                         name="RENORM__1DOWN",
                         Nominal="RENORM__1DOWN",
                         ExcludeSyst=[S for S in GetSystPairer().get_systematics()],
                         filepath=Path + "Sherpa222_VV.root",
                         fillstyle=0,
                         markerstyle=0,
                         markersize=0,
                         sampletype=SampleTypes.Irreducible,
                         LegendOrder=2)

FACTOR__1UP = DSconfig(colour=ROOT.kBlue + 2,
                       label="ZZ (#mu_{F}#uparrow)",
                       name="FACTOR__1UP",
                       Nominal="FACTOR__1UP",
                       ExcludeSyst=[S for S in GetSystPairer().get_systematics()],
                       filepath=Path + "Sherpa222_VV.root",
                       fillstyle=0,
                       markerstyle=0,
                       markersize=0,
                       sampletype=SampleTypes.Irreducible,
                       LegendOrder=5)

FACTOR__1DOWN = DSconfig(colour=ROOT.kBlue - 3,
                         label="ZZ (#mu_{F}#downarrow)",
                         name="FACTOR__1DOWN",
                         Nominal="FACTOR__1DOWN",
                         ExcludeSyst=[S for S in GetSystPairer().get_systematics()],
                         filepath=Path + "Sherpa222_VV.root",
                         fillstyle=0,
                         markerstyle=0,
                         markersize=0,
                         sampletype=SampleTypes.Irreducible,
                         LegendOrder=1)

ALPHAS__1UP = DSconfig(colour=ROOT.kMagenta + 1,
                       label="ZZ (#alpha_{s}#uparrow)",
                       name="ALPHAS__1UP",
                       Nominal="ALPHAS__1UP",
                       ExcludeSyst=[S for S in GetSystPairer().get_systematics()],
                       filepath=Path + "Sherpa222_VV.root",
                       fillstyle=0,
                       markerstyle=0,
                       markersize=0,
                       sampletype=SampleTypes.Irreducible,
                       LegendOrder=5)

ALPHAS__1DOWN = DSconfig(colour=ROOT.kMagenta - 4,
                         label="ZZ (#alpha_{s}#downarrow)",
                         name="ALPHAS__1DOWN",
                         Nominal="ALPHAS__1DOWN",
                         ExcludeSyst=[S for S in GetSystPairer().get_systematics()],
                         filepath=Path + "Sherpa222_VV.root",
                         fillstyle=0,
                         markerstyle=0,
                         markersize=0,
                         sampletype=SampleTypes.Irreducible,
                         LegendOrder=1)

RENORM_1UP_FACTOR__1UP = DSconfig(colour=ROOT.kRed,
                                  label="ZZ (#mu_{R}#uparrow, #mu_{F}#uparrow)",
                                  name="RENORM_1UP-FACTOR__1UP",
                                  Nominal="RENORM_AND-FACTOR__1UP",
                                  ExcludeSyst=[S for S in GetSystPairer().get_systematics()],
                                  filepath=Path + "Sherpa222_VV.root",
                                  fillstyle=0,
                                  markerstyle=0,
                                  markersize=0,
                                  sampletype=SampleTypes.Irreducible,
                                  LegendOrder=5)

RENORM_1DOWN_FACTOR__1DOWN = DSconfig(colour=ROOT.kRed - 4,
                                      label="ZZ (#mu_{R}#downarrow, #mu_{F}#downarrow)",
                                      name="RENORM_1DOWN-FACTOR__1DOWN",
                                      Nominal="RENORM_AND-FACTOR__1DOWN",
                                      ExcludeSyst=[S for S in GetSystPairer().get_systematics()],
                                      filepath=Path + "Sherpa222_VV.root",
                                      fillstyle=0,
                                      markerstyle=0,
                                      markersize=0,
                                      sampletype=SampleTypes.Irreducible,
                                      LegendOrder=1)

Generator = DSconfig(colour=ROOT.kViolet + 1,
                     label="ZZ (PowHeg)",
                     name="Generator",
                     ExcludeSyst=[S for S in GetSystPairer().get_systematics()],
                     filepath=Path + "PowHegPy8_VV.root",
                     fillstyle=0,
                     markerstyle=0,
                     markersize=0,
                     sampletype=SampleTypes.Irreducible,
                     LegendOrder=1)

#for syst in GetSystPairer().get_systematics():
#    if syst.find("PDFset") != -1: GetSystPairer().exclude_systematic(syst)
