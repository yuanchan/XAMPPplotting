import ROOT, os, sys
from XAMPPplotting.Defs import *
from XAMPPplotting.CheckMetaData import GetNormalizationDB
#from XAMPPplotting.PeriodRunConverter import GetPeriodRunConverter
from XAMPPplotting.CalculateLumiFromIlumicalc import CalculateRecordedLumi

from XAMPPplotting.FileUtils import ResolvePath, ReadInputConfig

# ~ PATH = "/ptmp/mpp/maren/Cluster/OUTPUT/2019-10-29/4L_Fakefactor_lightlepton_ttbar/"

PATH = "/ptmp/mpp/maren/Cluster/OUTPUT/2019-11-18/4L_Fakefactor_lightlepton_ttbar/"

ttbar = DSconfig(
    colour=ROOT.kBlue - 2,
    name="ttbar",
    label="top",
    filepath=[PATH + "PowHegPy8_ttbar_incl.root"],  #please use only PowhegPy_top.root when xsec for single top is OK
    sampletype=SampleTypes.Irreducible)
