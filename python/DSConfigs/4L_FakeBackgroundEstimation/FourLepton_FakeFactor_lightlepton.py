import ROOT, os, sys
from XAMPPplotting.Defs import *
from XAMPPplotting.CheckMetaData import GetNormalizationDB
#from XAMPPplotting.PeriodRunConverter import GetPeriodRunConverter
from XAMPPplotting.CalculateLumiFromIlumicalc import CalculateRecordedLumi

from XAMPPplotting.FileUtils import ResolvePath, ReadInputConfig

# ~ PATH = "/ptmp/mpp/maren/Cluster/OUTPUT/2019-08-12/4L_Fakefactor_lightlepton/"
PATH = "/ptmp/mpp/maren/Cluster/OUTPUT/2019-10-01/4L_Fakefactor_lightlepton/"

CONFIGPATH = ResolvePath("samples/SFAnalysis/")

Periods = [Cfg.replace(".conf", "") for Cfg in os.listdir(CONFIGPATH) if Cfg.find("data") != -1]

CalcLumi = False

if CalcLumi:
    Files = []
    for P in Periods:
        Files.extend(ReadInputConfig("%s/%s.conf" % (CONFIGPATH, P)))
    LUMI = 0.
    for R in GetNormalizationDB(Files).GetRunNumbers():
        LUMI += CalculateRecordedLumi(R)
    print LUMI
    exit(1)
else:
    LUMI = 139.731385567

ttbar = DSconfig(
    colour=ROOT.kBlue - 2,
    name="ttbar",
    label="top",
    filepath=[PATH + "PowHegPy8_ttbar_incl.root"],  #please use only PowhegPy_top.root when xsec for single top is OK
    sampletype=SampleTypes.Irreducible)

Zjets = DSconfig(
    colour=ROOT.kViolet - 9,
    name="Zjets",
    label="Z+Jets",
    filepath=[
        PATH + "PowHegPy8_Zee.root", PATH + "PowHegPy8_Zmumu.root", PATH + "PowHegPy8_Ztautau.root"
        #PATH + "Sherpa221_Zee.root",
        #PATH + "Sherpa221_Zmumu.root",
        #PATH + "Sherpa221_Ztautau.root"
    ],
    sampletype=SampleTypes.Irreducible)
