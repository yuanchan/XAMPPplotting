import ROOT
from XAMPPplotting.Defs import *
import os, sys
Folder = "/ptmp/mpp/maren/FakeFactorFiles/"
print "Info: FakeRatio_DataMC_Tau.py folder is %s" % (Folder)
if not os.path.isdir(Folder):
    sys.exit("Error: folder %s is not existing..." % (Folder))

datafile = os.path.join(Folder, "FakeFactors_Data.root")
if not os.path.isfile(datafile):
    sys.exit("Data file is not valid")

mcfile = os.path.join(Folder, "FakeFactors_SumBG.root")
if not os.path.isfile(mcfile):
    sys.exit("MC file is not valid")

Data = DSconfig(
    colour=ROOT.kBlack,
    #lumi       = 36.17,
    name="Data",
    label="Data",
    filepath=datafile,
    sampletype=SampleTypes.Data)

MC = DSconfig(colour=ROOT.kYellow + 1, name="MC", label="MC", filepath=mcfile, sampletype=SampleTypes.Irreducible)
