#! /usr/bin/env python
from XAMPPplotting.Defs import *

# BasePath = '/ptmp/mpp/niko/Cluster/Output/2018-05-17/dmhf_jetMult/' # for jet multiplicity
BasePath = '/ptmp/mpp/niko/Cluster/Output/2018-05-17/dmhf_jetPts/'  # for jet pt requirments

SMs = [
    "Zjets_Sherpa221_Input.root", "Wjets_Sherpa221_Input.root", "ttbar_Input.root", "singleTop_Input.root", "Diboson_Input.root",
    "ttV_Input.root"
]
AllSM = DSconfig(colour=ROOT.kBlack,
                 name="AllSM",
                 label="SM",
                 filepath=[BasePath + S for S in SMs],
                 sampletype=SampleTypes.Irreducible,
                 markerstyle=5,
                 linestyle=1,
                 fillstyle=0)

# Signal MC
# DM_TTpseudo_20_c1 = DSconfig(colour=ROOT.kRed,name="DM_TTpseudo_20_c1",label="(m_{a},m_{#chi})=(20,1) GeV",filepath=BasePath+"DM_TTpseudo_20_c1_a821_r7676_Input.root",sampletype=SampleTypes.Signal, markerstyle=ROOT.kOpenCircle)
DM_TTpseudo_300_c1 = DSconfig(linestyle=5,
                              colour=ROOT.kRed,
                              name="DM_TTpseudo_300_c1",
                              label="(m_{a},m_{#chi})=(300,1) GeV",
                              filepath=BasePath + "DM_TTpseudo_300_c1_a821_r7676_p2666_Input.root",
                              sampletype=SampleTypes.Signal,
                              markerstyle=ROOT.kOpenCircle)
DM_TTscalar_20_c1 = DSconfig(colour=ROOT.kGreen,
                             name="DM_TTscalar_20_c1",
                             label="(m_{#phi},m_{#chi})=(20,1) GeV",
                             filepath=BasePath + "DM_TTscalar_20_c1_a821_r7676_p2949_Input.root",
                             sampletype=SampleTypes.Signal,
                             markerstyle=ROOT.kOpenTriangleUp)
DM_TTscalar_300_c1 = DSconfig(colour=ROOT.kBlue,
                              name="DM_TTscalar_300_c1",
                              label="(m_{#phi},m_{#chi})=(300,1) GeV",
                              filepath=BasePath + "DM_TTscalar_300_c1_a821_r7676_p2949_Input.root",
                              sampletype=SampleTypes.Signal,
                              markerstyle=ROOT.kOpenTriangleDown)
