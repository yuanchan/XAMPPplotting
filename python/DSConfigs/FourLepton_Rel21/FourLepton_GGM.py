#! /usr/bin/env python
import ROOT, os, sys
from XAMPPplotting.Defs import *
from XAMPPplotting.CheckMetaData import GetNormalizationDB
#from XAMPPplotting.PeriodRunConverter import GetPeriodRunConverter
from XAMPPplotting.CalculateLumiFromIlumicalc import CalculateRecordedLumi
from XAMPPplotting.FileUtils import ResolvePath, ReadInputConfig

Path = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-04-21/FourLep_Histos/"

C_Name = DSConfigName("stanard GRL")
SignalPath = Path
DataPath = Path
CONFIGPATH = ResolvePath("XAMPPmultilep/InputConf/MPI/v028/Data/")
mc_period = ""
Periods = [
    Cfg.replace(".conf", "") for Cfg in os.listdir(CONFIGPATH)
    if (Cfg.find("data") != -1 and (len(mc_period) == 0 or (
        (Cfg.find("data15") != -1 or Cfg.find("data16") != -1) and mc_period.find("a") != -1) or
                                    (Cfg.find("data17") != -1 and mc_period.find("d") != -1) or
                                    (Cfg.find("data18") != -1 and mc_period.find("e") != -1))) and Cfg.find("debugrec_hlt") == -1
]
mc_period = ""
Files = []
LUMI = 139
for P in Periods:
    Files += ReadInputConfig("%s/%s.conf" % (CONFIGPATH, P))

#for R in GetNormalizationDB(Files).GetRunNumbers():
#    LUMI += CalculateRecordedLumi(R)

#LUMI = CalculateLumiFromPeriod([], [2015, 2016]) / 1000.
#print LUMI

# Samples

Data = DSconfig(lumi=LUMI,
                colour=ROOT.kBlack,
                label="data",
                name="data",
                filepath=["%s/%s.root" % (DataPath, P) for P in Periods],
                sampletype=SampleTypes.Data)

ZZ = DSconfig(colour=ROOT.kAzure - 4,
              label="ZZ",
              name="ZZ",
              filepath=[
                  "%s/Sherpa222_VV%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                  "%s/Sherpa222_ggZZ%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
              ],
              sampletype=SampleTypes.Irreducible,
              TheoUncert=0.1)
Higgs = DSconfig(
    colour=ROOT.kViolet - 9,
    label="Higgs",
    name="VH/H",
    filepath=[
        "%s/PowHegPy8_ZH%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
        "%s/PowHegPy8_WH%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
        #"%s/PowHegPy8_ggH%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
        #"%s/PowHegPy8_VBFH%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
        "%s/PowHegPy8_ttH%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
    ],
    sampletype=SampleTypes.Irreducible)
VVV = DSconfig(colour=ROOT.kMagenta - 9,
               label="VVV",
               name="VVV",
               filepath=["%s/Sherpa221_VVV%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period)],
               sampletype=SampleTypes.Irreducible)
ZJets = DSconfig(colour=ROOT.kYellow,
                 label="V+jets",
                 name="Vjets",
                 filepath=[
                     "%s/PowHegPy8_Zee%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                     "%s/PowHegPy8_Zmumu%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                     "%s/PowHegPy8_Ztautau%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                     "%s/PowHegPy8_Wenu%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                     "%s/PowHegPy8_Wmunu%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                     "%s/PowHegPy8_Wtaunu%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                 ],
                 sampletype=SampleTypes.Reducible)
ttbar = DSconfig(colour=ROOT.kYellow + 1,
                 label="t#bar{t}",
                 name="ttbar",
                 filepath=[
                     "%s/PowHegPy8_ttbar_incl%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                     "%s/PowhegPy_top%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                 ],
                 sampletype=SampleTypes.Reducible)
ttV = DSconfig(colour=ROOT.kTeal - 5,
               label="t#bar{t}Z(WW)",
               name="ttV",
               filepath=[
                   "%s/aMCatNLOPy8_ttZ%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                   "%s/aMcAtNlo_tWZ%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                   "%s/MG5Py8_4t%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                   "%s/MG5Py8_ttWW%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                   "%s/MG5Py8_ttWZ%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
               ],
               sampletype=SampleTypes.Irreducible)

OneFakeRed = DSconfig(colour=ROOT.TColor.GetColor(20, 54, 208),
                      label="1-fakes",
                      name="one-fakes",
                      filepath=[
                          "%s/Sherpa221_VV%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                          "%s/aMCatNLOPy8_ttW%s.root" % (Path, "" if len(mc_period) == 0 else "_" + mc_period),
                      ],
                      sampletype=SampleTypes.Reducible)
SignalPath = '/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-04-21/FourLep_Histos/'
GGM_110_0 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_110_0', label='GGM_110_0', filepath=SignalPath + '/GGM_110_0.root')
GGM_110_100 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_110_100', label='GGM_110_100', filepath=SignalPath + '/GGM_110_100.root')
GGM_110_25 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_110_25', label='GGM_110_25', filepath=SignalPath + '/GGM_110_25.root')
GGM_110_50 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_110_50', label='GGM_110_50', filepath=SignalPath + '/GGM_110_50.root')
GGM_110_75 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_110_75', label='GGM_110_75', filepath=SignalPath + '/GGM_110_75.root')
GGM_130_0 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_130_0', label='GGM_130_0', filepath=SignalPath + '/GGM_130_0.root')
GGM_130_100 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_130_100', label='GGM_130_100', filepath=SignalPath + '/GGM_130_100.root')
GGM_130_25 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_130_25', label='GGM_130_25', filepath=SignalPath + '/GGM_130_25.root')
GGM_130_50 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_130_50', label='GGM_130_50', filepath=SignalPath + '/GGM_130_50.root')
GGM_130_75 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_130_75', label='GGM_130_75', filepath=SignalPath + '/GGM_130_75.root')
GGM_150_0 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_150_0', label='GGM_150_0', filepath=SignalPath + '/GGM_150_0.root')
GGM_150_100 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_150_100', label='GGM_150_100', filepath=SignalPath + '/GGM_150_100.root')
GGM_150_25 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_150_25', label='GGM_150_25', filepath=SignalPath + '/GGM_150_25.root')
GGM_150_50 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_150_50', label='GGM_150_50', filepath=SignalPath + '/GGM_150_50.root')
GGM_150_75 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_150_75', label='GGM_150_75', filepath=SignalPath + '/GGM_150_75.root')
GGM_200_0 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_200_0', label='GGM_200_0', filepath=SignalPath + '/GGM_200_0.root')
GGM_200_100 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_200_100', label='GGM_200_100', filepath=SignalPath + '/GGM_200_100.root')
GGM_200_25 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_200_25', label='GGM_200_25', filepath=SignalPath + '/GGM_200_25.root')
GGM_200_50 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_200_50', label='GGM_200_50', filepath=SignalPath + '/GGM_200_50.root')
GGM_200_75 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_200_75', label='GGM_200_75', filepath=SignalPath + '/GGM_200_75.root')
GGM_250_0 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_250_0', label='GGM_250_0', filepath=SignalPath + '/GGM_250_0.root')
GGM_250_100 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_250_100', label='GGM_250_100', filepath=SignalPath + '/GGM_250_100.root')
GGM_250_25 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_250_25', label='GGM_250_25', filepath=SignalPath + '/GGM_250_25.root')
GGM_250_50 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_250_50', label='GGM_250_50', filepath=SignalPath + '/GGM_250_50.root')
GGM_250_75 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_250_75', label='GGM_250_75', filepath=SignalPath + '/GGM_250_75.root')
GGM_300_0 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_300_0', label='GGM_300_0', filepath=SignalPath + '/GGM_300_0.root')
GGM_300_100 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_300_100', label='GGM_300_100', filepath=SignalPath + '/GGM_300_100.root')
GGM_300_25 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_300_25', label='GGM_300_25', filepath=SignalPath + '/GGM_300_25.root')
GGM_300_50 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_300_50', label='GGM_300_50', filepath=SignalPath + '/GGM_300_50.root')
GGM_300_75 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_300_75', label='GGM_300_75', filepath=SignalPath + '/GGM_300_75.root')
GGM_325_0 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_325_0', label='GGM_325_0', filepath=SignalPath + '/GGM_325_0.root')
GGM_325_100 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_325_100', label='GGM_325_100', filepath=SignalPath + '/GGM_325_100.root')
GGM_325_25 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_325_25', label='GGM_325_25', filepath=SignalPath + '/GGM_325_25.root')
GGM_325_50 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_325_50', label='GGM_325_50', filepath=SignalPath + '/GGM_325_50.root')
GGM_325_75 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_325_75', label='GGM_325_75', filepath=SignalPath + '/GGM_325_75.root')
GGM_350_0 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_350_0', label='GGM_350_0', filepath=SignalPath + '/GGM_350_0.root')
GGM_350_100 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_350_100', label='GGM_350_100', filepath=SignalPath + '/GGM_350_100.root')
GGM_350_25 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_350_25', label='GGM_350_25', filepath=SignalPath + '/GGM_350_25.root')
GGM_350_50 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_350_50', label='GGM_350_50', filepath=SignalPath + '/GGM_350_50.root')
GGM_350_75 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_350_75', label='GGM_350_75', filepath=SignalPath + '/GGM_350_75.root')
GGM_375_0 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_375_0', label='GGM_375_0', filepath=SignalPath + '/GGM_375_0.root')
GGM_375_100 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_375_100', label='GGM_375_100', filepath=SignalPath + '/GGM_375_100.root')
GGM_375_25 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_375_25', label='GGM_375_25', filepath=SignalPath + '/GGM_375_25.root')
GGM_375_50 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_375_50', label='GGM_375_50', filepath=SignalPath + '/GGM_375_50.root')
GGM_375_75 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_375_75', label='GGM_375_75', filepath=SignalPath + '/GGM_375_75.root')
GGM_400_0 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_400_0', label='GGM_400_0', filepath=SignalPath + '/GGM_400_0.root')
GGM_400_100 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_400_100', label='GGM_400_100', filepath=SignalPath + '/GGM_400_100.root')
GGM_400_25 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_400_25', label='GGM_400_25', filepath=SignalPath + '/GGM_400_25.root')
GGM_400_50 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_400_50', label='GGM_400_50', filepath=SignalPath + '/GGM_400_50.root')
GGM_400_75 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_400_75', label='GGM_400_75', filepath=SignalPath + '/GGM_400_75.root')
GGM_425_0 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_425_0', label='GGM_425_0', filepath=SignalPath + '/GGM_425_0.root')
GGM_425_100 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_425_100', label='GGM_425_100', filepath=SignalPath + '/GGM_425_100.root')
GGM_425_25 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_425_25', label='GGM_425_25', filepath=SignalPath + '/GGM_425_25.root')
GGM_425_50 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_425_50', label='GGM_425_50', filepath=SignalPath + '/GGM_425_50.root')
GGM_425_75 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_425_75', label='GGM_425_75', filepath=SignalPath + '/GGM_425_75.root')
GGM_450_0 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_450_0', label='GGM_450_0', filepath=SignalPath + '/GGM_450_0.root')
GGM_450_100 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_450_100', label='GGM_450_100', filepath=SignalPath + '/GGM_450_100.root')
GGM_450_25 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_450_25', label='GGM_450_25', filepath=SignalPath + '/GGM_450_25.root')
GGM_450_50 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_450_50', label='GGM_450_50', filepath=SignalPath + '/GGM_450_50.root')
GGM_450_75 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_450_75', label='GGM_450_75', filepath=SignalPath + '/GGM_450_75.root')
GGM_475_0 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_475_0', label='GGM_475_0', filepath=SignalPath + '/GGM_475_0.root')
GGM_475_100 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_475_100', label='GGM_475_100', filepath=SignalPath + '/GGM_475_100.root')
GGM_475_25 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_475_25', label='GGM_475_25', filepath=SignalPath + '/GGM_475_25.root')
GGM_475_50 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_475_50', label='GGM_475_50', filepath=SignalPath + '/GGM_475_50.root')
GGM_475_75 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_475_75', label='GGM_475_75', filepath=SignalPath + '/GGM_475_75.root')
GGM_500_0 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_500_0', label='GGM_500_0', filepath=SignalPath + '/GGM_500_0.root')
GGM_500_100 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_500_100', label='GGM_500_100', filepath=SignalPath + '/GGM_500_100.root')
GGM_500_25 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_500_25', label='GGM_500_25', filepath=SignalPath + '/GGM_500_25.root')
GGM_500_50 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_500_50', label='GGM_500_50', filepath=SignalPath + '/GGM_500_50.root')
GGM_500_75 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_500_75', label='GGM_500_75', filepath=SignalPath + '/GGM_500_75.root')
GGM_525_0 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_525_0', label='GGM_525_0', filepath=SignalPath + '/GGM_525_0.root')
GGM_525_100 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_525_100', label='GGM_525_100', filepath=SignalPath + '/GGM_525_100.root')
GGM_525_25 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_525_25', label='GGM_525_25', filepath=SignalPath + '/GGM_525_25.root')
GGM_525_50 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_525_50', label='GGM_525_50', filepath=SignalPath + '/GGM_525_50.root')
GGM_525_75 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_525_75', label='GGM_525_75', filepath=SignalPath + '/GGM_525_75.root')
GGM_550_0 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_550_0', label='GGM_550_0', filepath=SignalPath + '/GGM_550_0.root')
GGM_550_100 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_550_100', label='GGM_550_100', filepath=SignalPath + '/GGM_550_100.root')
GGM_550_25 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_550_25', label='GGM_550_25', filepath=SignalPath + '/GGM_550_25.root')
GGM_550_50 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_550_50', label='GGM_550_50', filepath=SignalPath + '/GGM_550_50.root')
GGM_550_75 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_550_75', label='GGM_550_75', filepath=SignalPath + '/GGM_550_75.root')
GGM_600_0 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_600_0', label='GGM_600_0', filepath=SignalPath + '/GGM_600_0.root')
GGM_600_100 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_600_100', label='GGM_600_100', filepath=SignalPath + '/GGM_600_100.root')
GGM_600_25 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_600_25', label='GGM_600_25', filepath=SignalPath + '/GGM_600_25.root')
GGM_600_50 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_600_50', label='GGM_600_50', filepath=SignalPath + '/GGM_600_50.root')
GGM_600_75 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_600_75', label='GGM_600_75', filepath=SignalPath + '/GGM_600_75.root')
GGM_650_0 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_650_0', label='GGM_650_0', filepath=SignalPath + '/GGM_650_0.root')
GGM_650_100 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_650_100', label='GGM_650_100', filepath=SignalPath + '/GGM_650_100.root')
GGM_650_25 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_650_25', label='GGM_650_25', filepath=SignalPath + '/GGM_650_25.root')
GGM_650_50 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_650_50', label='GGM_650_50', filepath=SignalPath + '/GGM_650_50.root')
GGM_650_75 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_650_75', label='GGM_650_75', filepath=SignalPath + '/GGM_650_75.root')
GGM_700_0 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_700_0', label='GGM_700_0', filepath=SignalPath + '/GGM_700_0.root')
GGM_700_100 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_700_100', label='GGM_700_100', filepath=SignalPath + '/GGM_700_100.root')
GGM_700_25 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_700_25', label='GGM_700_25', filepath=SignalPath + '/GGM_700_25.root')
GGM_700_50 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_700_50', label='GGM_700_50', filepath=SignalPath + '/GGM_700_50.root')
GGM_700_75 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_700_75', label='GGM_700_75', filepath=SignalPath + '/GGM_700_75.root')
GGM_750_0 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_750_0', label='GGM_750_0', filepath=SignalPath + '/GGM_750_0.root')
GGM_750_100 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_750_100', label='GGM_750_100', filepath=SignalPath + '/GGM_750_100.root')
GGM_750_25 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_750_25', label='GGM_750_25', filepath=SignalPath + '/GGM_750_25.root')
GGM_750_50 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_750_50', label='GGM_750_50', filepath=SignalPath + '/GGM_750_50.root')
GGM_750_75 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_750_75', label='GGM_750_75', filepath=SignalPath + '/GGM_750_75.root')
GGM_800_0 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_800_0', label='GGM_800_0', filepath=SignalPath + '/GGM_800_0.root')
GGM_800_100 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_800_100', label='GGM_800_100', filepath=SignalPath + '/GGM_800_100.root')
GGM_800_25 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_800_25', label='GGM_800_25', filepath=SignalPath + '/GGM_800_25.root')
GGM_800_50 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_800_50', label='GGM_800_50', filepath=SignalPath + '/GGM_800_50.root')
GGM_800_75 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_800_75', label='GGM_800_75', filepath=SignalPath + '/GGM_800_75.root')
GGM_95_0 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_95_0', label='GGM_95_0', filepath=SignalPath + '/GGM_95_0.root')
GGM_95_100 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_95_100', label='GGM_95_100', filepath=SignalPath + '/GGM_95_100.root')
GGM_95_25 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_95_25', label='GGM_95_25', filepath=SignalPath + '/GGM_95_25.root')
GGM_95_50 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_95_50', label='GGM_95_50', filepath=SignalPath + '/GGM_95_50.root')
GGM_95_75 = DSconfig(sampletype=SampleTypes.Signal, name='GGM_95_75', label='GGM_95_75', filepath=SignalPath + '/GGM_95_75.root')
