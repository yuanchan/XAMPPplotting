#! /usr/bin/env python
from XAMPPplotting.Defs import *
from XAMPPplotting.FileStructureHandler import GetFileHandler, GetSystPairer
Path = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-01-28/Truth_Syst/"
Path1 = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-01-28/Truth_Syst_combined/"

Path = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-04-05/FourLep_RunII/"
Path1 = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-04-05/FourLep_RunII_combined/"
### Version with SR0G
Path1 = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-04-21/SR0G_combined/"
Path = "/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-04-21/SR0G/"

#Path ="/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-05-14/AlfredQuak/"
#Path1 ="/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-05-14/AlfredQuak_combined/"
GetSystPairer().create_envelope("otherPDF", "PDF (alt.)")
GetSystPairer().find_envelope("otherPDF").append_systematic("CT14nlo")
GetSystPairer().find_envelope("otherPDF").append_systematic("MMHT")
GetSystPairer().find_envelope("otherPDF").append_systematic("CT14NNLO")
GetSystPairer().find_envelope("otherPDF").append_systematic("MMHT2014nlo68")
#GetSystPairer().find_envelope("otherPDF").append_systematic("PDF4LHC15_nlo_30")
#GetSystPairer().find_envelope("otherPDF").append_systematic("PDFset=261000")
#GetSystPairer().find_envelope("otherPDF").append_systematic("PDFset=260000")

## aMCatNLO systematics
GetSystPairer().create_deviation("PDFset=260", "PDF")
#### Sherpa systematics
GetSystPairer().create_deviation("PDFset=261", "PDF")
GetSystPairer().create_deviation("PDF4LHC15_nlo_30", "PDF")

### Reject the PowHeg variations of the PDF set in combination with the scales
GetSystPairer().exclude_systematic("PDF4LHC15_FACTOR__1DOWN")
GetSystPairer().exclude_systematic("PDF4LHC15_FACTOR__1UP")
GetSystPairer().exclude_systematic("PDF4LHC15_RENORM_1DOWN-FACTOR__1DOWN")
GetSystPairer().exclude_systematic("PDF4LHC15_RENORM_1DOWN-FACTOR__1UP")
GetSystPairer().exclude_systematic("PDF4LHC15_RENORM_1UP-FACTOR__1DOWN")
GetSystPairer().exclude_systematic("PDF4LHC15_RENORM_1UP-FACTOR__1UP")
GetSystPairer().exclude_systematic("PDF4LHC15_RENORM__1DOWN")
GetSystPairer().exclude_systematic("PDF4LHC15_RENORM__1UP")

### Reject the PowHeg variations of the PDF set in combination with the scales
GetSystPairer().exclude_systematic("CT14nlo_RENORM_1UP-FACTOR")
GetSystPairer().exclude_systematic("CT14nlo_RENORM_1DOWN-FACTOR__1DOWN")
GetSystPairer().exclude_systematic("CT14nlo_RENORM_1UP-FACTOR__1DOWN")
GetSystPairer().exclude_systematic("CT14nlo_RENORM__1DOWN")
GetSystPairer().exclude_systematic("CT14nlo_FACTOR__1UP")
GetSystPairer().exclude_systematic("CT14nlo_FACTOR__1DOWN")
GetSystPairer().exclude_systematic("CT14nlo_RENORM_1DOWN-FACTOR__1UP")
GetSystPairer().exclude_systematic("CT14nlo_RENORM_1UP-FACTOR__1UP")
GetSystPairer().exclude_systematic("CT14nlo_RENORM__1UP")

### Reject the PowHeg variations of the PDF set in combination with the scales
GetSystPairer().exclude_systematic("2muF_0p5muR_MMHT")
GetSystPairer().exclude_systematic("0p5muF_2muR_MMHT")
GetSystPairer().exclude_systematic("2muF_2muR_MMHT")
GetSystPairer().exclude_systematic("0p5muF_0p5muR_MMHT")
GetSystPairer().exclude_systematic("0p5muR_MMHT")
GetSystPairer().exclude_systematic("2muR_MMHT")
GetSystPairer().exclude_systematic("0p5muF_MMHT")
GetSystPairer().exclude_systematic("2muF_MMHT")

GetSystPairer().create_envelope("ALPHAS", "#alpha_{S}")
GetSystPairer().set_systematic_title("RENORM", "#mu_{R}")
GetSystPairer().set_systematic_title("FACTOR", "#mu_{F}")
GetSystPairer().set_systematic_title("ALPHAS", "#alpha_{s}")
GetSystPairer().set_systematic_title("RENORM_AND-FACTOR", "#mu_{R}#wedge#mu_{F}")
GetSystPairer().set_systematic_title("RENORM_1UP-FACTOR", "#mu_{R}#uparrow#mu_{F}")
GetSystPairer().set_systematic_title("RENORM_1DOWN-FACTOR", "#mu_{R}#downarrow#mu_{F}")
GetSystPairer().set_systematic_title("A14VarDn", "A14 tune")
GetSystPairer().set_systematic_title("A14VarUp", "A14 tune")
GetSystPairer().set_systematic_title("QSF025", "#mu_{Q}")
GetSystPairer().set_systematic_title("QSF4", "#mu_{Q}")

GetSystPairer().exclude_systematic("A14VarDn")
GetSystPairer().exclude_systematic("A14VarUp")

GetSystPairer().pair_systematics("0p5muF_MMHT", "2muF_MMHT")
GetSystPairer().pair_systematics("0p5muR_MMHT", "2muR_MMHT")
GetSystPairer().pair_systematics("0p5muF_0p5muR_MMHT", "0p5muF_2muR_MMHT")
GetSystPairer().pair_systematics("2muF_0p5muR_MMHT", "2muF_2muR_MMHT")
GetSystPairer().pair_systematics("QSF025", "QSF4")
GetSystPairer().pair_systematics("CKKW15", "CKKW30")
GetSystPairer().pair_systematics("A14VarDn", "A14VarUp")

GetSystPairer().create_envelope("ALPHAS", "#alpha_{S}")
GetSystPairer().create_group("Generator", "Generator")
GetSystPairer().find_envelope("Generator").append_systematic("VV")
GetSystPairer().find_envelope("Generator").append_systematic("ttH")
GetSystPairer().find_envelope("Generator").append_systematic("multileg")
#GetSystPairer().exclude_systematic("Generator")

#GetSystPairer().set_systematic_title("Renorm_1DOWN-Factor", "#mu_{R}#pm1#sigma #mu_{F}")

# python XAMPPplotting/python/MCPlots.py  -c XAMPPplotting/python/DSConfigs/FourLepton_Truth_ttZ.py --RatioSmp Nominal --noStack --regions 4L
aMCatNLOttZ = DSconfig(colour=ROOT.TColor.GetColor(111, 224, 5),
                       label="t#bar{t}Z",
                       name="aMCatNLOttZ",
                       filepath=[Path1 + "aMCatNLOPy8_ttZ.root"],
                       fillstyle=0,
                       markerstyle=0,
                       markersize=0,
                       sampletype=SampleTypes.Irreducible,
                       LegendOrder=-1)

#aMCatNLOttH = DSconfig(
#    colour=ROOT.kBlack,
#    #  ROOT.TColor.GetColor(111, 224, 5),
#    label="t#bar{t}H",
#    name="aMCatNLOttH",
#    filepath=[Path + "aMcAtNloPy8_ttH.root"],
#    fillstyle=0,
#    markerstyle=0,
#    markersize=0,
#    sampletype=SampleTypes.Irreducible,
#    LegendOrder=-1)

PowHegPy8_ttH = DSconfig(
    colour=ROOT.kBlack,
    #  ROOT.TColor.GetColor(111, 224, 5),
    label="t#bar{t}H",
    name="PowHegPy8_ttH",
    filepath=[Path1 + "PowHegPy8_ttH.root"],
    ExcludeSyst=[
        "CT14NNLO",
        "CT14nlo",
        "MMHT",
        "MMHT2014nlo68",
        "NNPDF31_nlo",
        "ALPHAS1DOWN_FACTOR__1DOWN",
        "ALPHAS1DOWN_FACTOR__1UP",
        "ALPHAS1DOWN_RENORM_1DOWN-FACTOR__1DOWN",
        "ALPHAS1DOWN_RENORM_1DOWN-FACTOR__1UP",
        "ALPHAS1DOWN_RENORM_1UP-FACTOR__1DOWN",
        "ALPHAS1DOWN_RENORM_1UP-FACTOR__1UP",
        "ALPHAS1DOWN_RENORM__1DOWN",
        "ALPHAS1DOWN_RENORM__1UP",
        "ALPHAS1UP_FACTOR__1DOWN",
        "ALPHAS1UP_FACTOR__1UP",
        "ALPHAS1UP_RENORM__1UP",
    ],
    fillstyle=0,
    markerstyle=0,
    markersize=0,
    sampletype=SampleTypes.Irreducible,
    LegendOrder=-1)

Sherpa222_ZZ = DSconfig(
    colour=ROOT.kBlack,
    #   ROOT.TColor.GetColor(111, 224, 5),
    label="VV",
    name="Sherpa222_ZZ",
    filepath=Path1 + "Sherpa222_VV.root",
    fillstyle=0,
    markerstyle=0,
    markersize=0,
    sampletype=SampleTypes.Irreducible,
    LegendOrder=-1)

Sherpa222_VVV = DSconfig(
    colour=ROOT.kBlack,
    #  ROOT.TColor.GetColor(111, 224, 5),
    label="VVV",
    name="Sherpa221_VVV",
    filepath=Path + "Sherpa221_VVV.root",
    fillstyle=0,
    markerstyle=0,
    markersize=0,
    sampletype=SampleTypes.Irreducible,
    LegendOrder=-1)

Higgs = DSconfig(
    colour=ROOT.kBlack,  #ROOT.TColor.GetColor(111,224,5) ,
    label="Higgs",
    name="Higgs",
    filepath=[
        # Path + "PowHegPy8_WH.root",
        Path + "PowHegPy8_ggH.root",
        # Path + "PowHegPy8_ZH.root",
    ],
    ### The H4L people only use the PDF4-LHC as source of uncertainty
    ExcludeSyst=["PDFset=%d" % (i) for i in range(261000, 261101)] + ["PDFset=%d" % (i) for i in range(260000, 260101)] + [
        "CT14NNLO",
        "CT14nlo",
        "MMHT",
        "MMHT2014nlo68",
        "NNPDF31_nlo",
        "ALPHAS1DOWN_FACTOR__1DOWN",
        "ALPHAS1DOWN_FACTOR__1UP",
        "ALPHAS1DOWN_RENORM_1DOWN-FACTOR__1DOWN",
        "ALPHAS1DOWN_RENORM_1DOWN-FACTOR__1UP",
        "ALPHAS1DOWN_RENORM_1UP-FACTOR__1DOWN",
        "ALPHAS1DOWN_RENORM_1UP-FACTOR__1UP",
        "ALPHAS1DOWN_RENORM__1DOWN",
        "ALPHAS1DOWN_RENORM__1UP",
        "ALPHAS1UP_FACTOR__1DOWN",
        "ALPHAS1UP_FACTOR__1UP",
        "ALPHAS1UP_RENORM__1UP",
    ],
    fillstyle=0,
    markerstyle=0,
    markersize=0,
    sampletype=SampleTypes.Irreducible,
    LegendOrder=-1)
