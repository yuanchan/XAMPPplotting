#! /usr/bin/env python
from XAMPPplotting import TauCommon as TC
from XAMPPplotting import TauLumiCalc as TLC
from XAMPPplotting.Defs import *


def check(method):
    if not method:
        print("Unable to load method...")
        exit(1)


#lumi
lumi = 139.0  #fb^-1

#via JOs - To Do
data_conf_path = "XAMPPplotting/InputConf/LepHadStau/Data/"

main_root_files_path = "/afs/cern.ch/work/m/mayoub/public/postdoc/XAMPP-WHTauTau-Branch/source/MergedInputsForFF/"

calculate_lumi = False

years = ["data15", "data16", "data17", "data18"]

#calculate lumi
if calculate_lumi:
    #localSetupPyAMI
    #voms-proxy-init -voms atlas
    lc = TLC.TauLumiCalc("Lumi calculation", data_path=main_root_files_path, conf_path=data_conf_path, years=years)
    check(lc.calculate())
    lumi = lc.lumi_ifb
    print("Lumi {}".format(lumi))
    exit(0)

#load samples

tc = TC.TauCommon.ctr_for_ff("stau lephad fakes", path=main_root_files_path, lumi=lumi)

check(tc.load_samples())

#unfortunately this won't work in a for-loop
q0 = tc.get_ds_configs()[0]  # data
q1 = tc.get_ds_configs()[1]  # Ztautau
q2 = tc.get_ds_configs()[2]  # Multi-V
q3 = tc.get_ds_configs()[3]  # Higgs
q4 = tc.get_ds_configs()[4]  # Top
q5 = tc.get_ds_configs()[5]  # Zll
q6 = tc.get_ds_configs()[6]  # Wjets

#q7 = tc.get_ds_configs()[7] # signal


def main(argv):
    print("Done!")


##
# @brief execute main
##
if __name__ == "__main__":
    main(sys.argv[1:])
