"""
@short Hello, I am keeping common functionalities for the making fancy stau lephad plots
@date January, 2018
 
"""

#! /usr/bin/env python

import ROOT, os, sys, glob
from XAMPPplotting.Defs import *
from XAMPPplotting import TauLogger as TL


class TauCommon(object):
    def __init__(self, name="tau common", main_path="", aux_path="", lumi=0., include_fakes=True, include_signal=True, signal_point=""):
        "Common setup methods for plotting"
        #parsed options
        self.__name = name
        self.__main_path = main_path
        self.__aux_path = aux_path
        self.__include_fakes = include_fakes
        self.__include_signal = include_signal
        self.__signal_point = signal_point
        #derived properties
        self.__msg = TL.msg(2)
        self.__msg.info("TauCommon", "'%s' loaded ..." % (self.__name))
        self.__luminosity = lumi
        self.__sanity_checks_status = self.__sanity_checks()

        self.__dsconfig_holder = []

    @classmethod
    def ctr_common_path(cls, name="", path="", lumi=0., include_fakes=True, include_signal=True, signal_point=""):
        return cls(name=name,
                   main_path=path,
                   aux_path=path,
                   lumi=lumi,
                   include_fakes=include_fakes,
                   include_signal=include_signal,
                   signal_point=signal_point)

    @classmethod
    def ctr_for_ff(cls, name="", path="", lumi=0.):
        return cls(name=name, main_path=path, aux_path=path, lumi=lumi, include_fakes=False, include_signal=False)

    def __hold(self, ds=None):
        if ds:
            self.__dsconfig_holder.append(ds)
        else:
            self.__msg.error("TauCommon", "Unable to hold a null DS config object...")

    def __sanity_checks(self):

        if not os.path.isdir(self.__main_path):
            self.__msg.error("TauCommon", "Main path '{}' is not valid".format(self.__main_path))
            return False

        if not os.path.isdir(self.__aux_path):
            self.__msg.error("TauCommon", "Auxiliary path '{}' is not valid".format(self.__aux_path))
            return False

        return True

    def __check_files(self, sname="", sgen="", slist=[]):

        if not slist:
            self.__msg.error("TauCommon", "Error: Empty `{}` for generator `{}` ...".format(sname, sgen))
            return False

        for l in slist:
            if not os.path.isfile(l):
                self.__msg.fatal("TauCommon", "Invalid file `{}` for generator `{}` ...".format(l, sgen))
            else:
                self.__msg.info("TauCommon", "Sample %s is registered ..." % (l))

        return True

    def __get_multi_samples(self, sample_names=[]):

        samples = []
        for sample in sample_names:
            isamples = glob.glob(os.path.join(self.__main_path, "%s*.root" % (sample)))
            samples.extend(isamples)

        if not self.__check_files("", "", samples):
            samples_str = ', '.join(sample_names)
            self.__msg.error("TauCommon", "Problem in retrieving the samples for '%s' " % (samples_str))

        return samples

    def __get_samples(self, generator="", process="", fstates=[]):
        samples = []
        if process == "Wjets":
            suffix = "W"
        elif process == "Z":
            suffix = "Z"
        else:
            suffix = ""

        for fstate in fstates:
            isamples = glob.glob(os.path.join(self.__main_path, "%s_%s%s*.root" % (generator, suffix, fstate)))
            samples.extend(isamples)
            #samples.append( os.path.join(self.__main_path, "%s_%s%s.root"%(generator, suffix, fstate)) )

        if not self.__check_files(process, generator, samples):
            fstates_s = ', '.join(fstates)
            self.__msg.error("TauCommon", "Problem in getting the samples for '%s', '%s', '%s' " % (generator, process, fstates_s))

        return samples

    def __get_data_periods(self):

        return {
            "15": ["D", "E", "F", "G", "H", "J"],
            "16": ["A", "B", "C", "D", "E", "F", "G", "I", "K", "L"],
            "17": ["B", "C", "D", "E", "F", "H", "I", "K"],
            "18": ["B", "C", "D", "F", "I", "K", "L", "M", "O", "Q"],
        }

    def __get_data(self):

        samples = []

        for year, periods in self.__get_data_periods().items():
            for period in periods:
                filename = "data{}_period{}.root".format(year, period)
                fullname = os.path.join(self.__main_path, filename)
                if not os.path.isfile(fullname):
                    self.__msg.warning("TauCommon", "Data file %s does not exist." % (fullname))
                else:
                    samples.append(fullname)

        if not samples:
            self.__msg.warning("TauCommon", "No data found! Returning an empty list of data samples.")

        if not self.__check_files("Data", "Data", samples):
            self.__msg.error("TauCommon", "Problem in getting data samples for data ...")

        return samples

    def __get_fakes(self):

        fake_samples = []

        mc_samples = [
            "DD_PowhegPy_top", "DD_PowHegPy8_ttbar_incl", "DD_Sherpa221_Wenu", "DD_Sherpa221_Wmunu", "DD_Sherpa221_Wtaunu",
            "DD_Sherpa221_Zee", "DD_Sherpa221_Zmumu", "DD_Sherpa221_Ztautau", "DD_Sherpa222_VV"
        ]

        for year, periods in self.__get_data_periods().items():
            for period in periods:
                filename = "DD_data{}_period{}.root".format(year, period)
                fullname = os.path.join(self.__aux_path, filename)
                if not os.path.isfile(fullname):
                    self.__msg.warning("TauCommon", "Fake Data file %s in directory %s does not exist." % (filename, self.__aux_path))
                else:
                    fake_samples.append(fullname)

        for mc in mc_samples:
            fullname = os.path.join(self.__aux_path, filename + ".root")
            if not os.path.isfile(fullname):
                self.__msg.warning("TauCommon", "Fake MC file %s does not exist." % (fullname))
            else:
                fake_samples.append(fullname)

        if not fake_samples:
            self.__msg.warning("TauCommon", "No data found! Returning an empty list of data samples.")

        if not self.__check_files("Fake", "", fake_samples):
            self.__msg.error("TauCommon", "Problem in getting fake samples...")

        return fake_samples

    def __get_signal(self, signal):

        samples = glob.glob(os.path.join(self.__main_path, "%s*.root" % (signal)))

        self.__msg.info("TauCommon", "Signal samples: %s" % (''.join(samples)))
        if not self.__check_files(signal, "", samples):
            self.__msg.error("TauCommon", "Problem in getting the signal samples '%s'" % (signal))

        return samples

    def load_samples(self):

        self.__msg.info("TauCommon", "Loading Data ...")
        self.__hold(
            DSconfig(lumi=self.__luminosity,
                     colour=ROOT.kBlack,
                     label="Data",
                     name="Data" + self.__name,
                     filepath=self.__get_data(),
                     sampletype=SampleTypes.Data))
        #----------------------------------------------------------------------------------------------------
        self.__msg.info("TauCommon", "Loading Multi-V ...")
        self.__hold(
            DSconfig(
                colour=ROOT.kMagenta + 1,
                label="Multi-V",
                name="Multi-V" + self.__name,
                #filepath=self.__get_samples(generator="Sherpa222", process="", fstates=["VV", "VVV"]),
                filepath=self.__get_multi_samples(["Sherpa222_VV", "Sherpa221_VVV"]),
                sampletype=SampleTypes.Irreducible))
        #----------------------------------------------------------------------------------------------------
        self.__msg.info("TauCommon", "Loading Higgs ... Warning! Missing!")
        self.__hold(
            DSconfig(colour=ROOT.kGray + 1,
                     label="Higgs",
                     name="Higgs" + self.__name,
                     filepath=self.__get_multi_samples(["Higgs"]),
                     sampletype=SampleTypes.Irreducible))
        #----------------------------------------------------------------------------------------------------
        self.__msg.info("TauCommon", "Loading top ... Warning! Missing: single top, ttV")
        self.__hold(
            DSconfig(colour=ROOT.kOrange + 1,
                     label="Top",
                     name="Top" + self.__name,
                     filepath=self.__get_multi_samples(
                         ["PowHegPy8_ttbar_incl", "PowHegPy8_t", "MG5Py8_multi_t", "MG5_aMCatNLO_Py8_ttV_tV_tVV"]),
                     sampletype=SampleTypes.Irreducible))
        #----------------------------------------------------------------------------------------------------
        self.__msg.info("TauCommon", "Loading Zll ...")
        self.__hold(
            DSconfig(colour=ROOT.kGreen + 1,
                     label="Zll",
                     name="Zll" + self.__name,
                     filepath=self.__get_samples(generator="Sherpa221", process="Z", fstates=["ee", "mumu"]),
                     sampletype=SampleTypes.Irreducible))
        #----------------------------------------------------------------------------------------------------
        self.__msg.info("TauCommon", "Loading Ztautau ...")
        self.__hold(
            DSconfig(colour=ROOT.kAzure + 1,
                     label="Z#tau#tau",
                     name="Ztautau" + self.__name,
                     filepath=self.__get_samples(generator="Sherpa221", process="Z", fstates=["tautau"]),
                     sampletype=SampleTypes.Irreducible))
        #----------------------------------------------------------------------------------------------------
        self.__msg.info("TauCommon", "Loading Wjets ...")
        self.__hold(
            DSconfig(colour=ROOT.kViolet + 1,
                     label="W+jets",
                     name="Wjets" + self.__name,
                     filepath=self.__get_samples(generator="Sherpa221", process="Wjets", fstates=["munu", "enu", "taunu"]),
                     sampletype=SampleTypes.Irreducible))
        #----------------------------------------------------------------------------------------------------
        if self.__include_signal:
            self.__msg.info("TauCommon", "Loading signal ...")
            self.__hold(
                DSconfig(colour=ROOT.kRed,
                         label="Signal",
                         name="Signal" + self.__name,
                         filepath=self.__get_signal("StauStau_" + self.__signal_point),
                         sampletype=SampleTypes.Signal))
        #----------------------------------------------------------------------------------------------------
        if self.__include_fakes:
            self.__msg.info("TauCommon", "Loading Fakes ...")
            self.__hold(
                DSconfig(colour=ROOT.kRed + 1,
                         label="Fakes",
                         name="Fakes" + self.__name,
                         filepath=self.__get_fakes(),
                         sampletype=SampleTypes.Reducible))

        return True

    def get_ds_configs(self):
        if self.__dsconfig_holder:
            self.__msg.info("TauCommon", "DS configs found %i" % (len(self.__dsconfig_holder)))
        else:
            self.__msg.warning("TauCommon", "No DS configs found")

        return self.__dsconfig_holder
