#!/usr/bin/python
import sys, logging, os, ROOT
from ClusterSubmission.PeriodRunConverter import getGRL
from ClusterSubmission.Utils import convertStdVector
# prevent ROOT from hijacking the command line arguments
ROOT.PyConfig.IgnoreCommandLineOptions = True

m_PRWFiles = ["dev/PileupReweighting/mc16_13TeV/pileup_mc16%s_dsid%d_FS.root" % (c, mc) for c in ["a", "d", "e"] for mc in [361107]]
m_ilumicalcFiles = getGRL(flavour='lumiCalc')

m_prw_tool = None


def SetupPRWTool(PRWFiles=m_PRWFiles, ilumicalcFiles=m_ilumicalcFiles):
    global m_prw_tool
    if m_prw_tool: return m_prw_tool
    try:
        m_prw_tool = ROOT.XAMPP.LumiCalculator.getCalculator()
    except:
        #pythonROOT.gROOT.Macro("$ROOTCOREDIR/scripts/load_packages.C")
        ROOT.xAOD.Init().ignore()
        m_prw_tool = ROOT.XAMPP.LumiCalculator.getCalculator()

    m_prw_tool.SetPRWlumi(convertStdVector(ilumicalcFiles))
    m_prw_tool.SetPRWconfig(convertStdVector(PRWFiles))
    if not m_prw_tool.InitPileUpTool(): exit(1)

    return m_prw_tool


def CalculateLumiFromRuns(FirstRun, LastRun):
    return SetupPRWTool().getExpectedLumi(int(FirstRun), int(LastRun))


def CalculateRecordedLumi(Run):
    return SetupPRWTool().getRecordedLumiFromRun(Run)


def GetRuns(ilumicalcFiles=m_ilumicalcFiles):
    runs = []
    for ifile in ilumicalcFiles:
        TFile = ROOT.TFile(ifile, "READ")
        if not TFile:
            logging.error('invalid ilumicalc file')
            continue
        Tree = TFile.Get("LumiMetaData")
        if not Tree:
            logging.error('invalid ilumicalc tree in file %s' % ifile)
            continue
        Nentries = Tree.GetEntries()
        if Nentries == 0: continue
        for iEntry in range(0, Nentries):
            Tree.GetEntry(iEntry)
            run = Tree.RunNbr
            if not run in runs: runs.append(run)
    return sorted(runs)


def GetMissingLumiBlocks(runNumber, MaxRange=1.e5):
    MissingLumiBlocks = []
    for i in range(int(MaxRange)):
        if SetupPRWTool().isGoodLumiBlock(runNumber, i) == True and not i in MetaDataLumiBlocks:
            MissingLumiBlocks.append(i)
    return MissingLumiBlocks


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description="Script for calculating luminosities",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--PRWFile', help='Specify PRWFile(s)', nargs='+', default=m_PRWFiles)
    parser.add_argument('--ilumicalcFile', help='Specify ilumicalc file(s)', nargs='+', default=m_ilumicalcFiles)
    parser.add_argument('-f', '--FirstRun', help='Directly read from folder containing merged root files', type=int, default=0)
    parser.add_argument('-l', '--LastRun', help='Directly read from folder containing merged root files', type=int, default=999999)
    parser.add_argument('-r', '--Run', help='Specify one specific run to estimate luminosity from', type=int, default=-1)
    options = parser.parse_args()

    SetupPRWTool(options.PRWFile, options.ilumicalcFile)

    if options.Run != -1 and (options.FirstRun != 0 or options.LastRun != 999999):
        logging.error('ERROR: You have to specify either --FirstRun/--LastRun or --Run, but not both. Exiting...')
        sys.exit(1)
    elif options.Run != -1:
        print 'Integrated luminosity of run %s:\t\t %s' % (options.Run, CalculateLumiFromRuns(options.Run, options.Run))
    else:
        print 'Integrated luminosity of runs %s-%s:\t\t %s' % (options.FirstRun, options.LastRun,
                                                               CalculateLumiFromRuns(options.FirstRun, options.LastRun))
