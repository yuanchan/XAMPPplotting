#! /usr/bin/env python
from XAMPPplotting.Defs import *
from XAMPPplotting.FileStructureHandler import GetFileHandler, ClearServices
from XAMPPplotting.PrintYields import CutFlowYield
import logging


# this functions prints out the CutFlowHisto
def printCutFlow(case="",
                 CutFlowHisto=None,
                 doRaw=False,
                 systematic='',
                 lumi=1.,
                 show_empty=False,
                 ofile=None,
                 do_latex=False,
                 relative_yields=False,
                 percentage_relative_yields=False):
    Nbins = CutFlowHisto.GetNbinsX()
    labellengths = []
    for ibin in range(1, Nbins + 1):
        labellengths.append(len(CutFlowHisto.GetXaxis().GetBinLabel(ibin)) + 2)
    # if not do_latex:
    #     prettyPrint(preamble="CutFlowHisto", data1=CutFlowHisto.GetName(), data2="", width1=max(labellengths), ofile=ofile)
    if not doRaw:
        if not do_latex:
            prettyPrint("Scaled to integrated luminosity of:", "%.1f fb^{-1}" % lumi, "", width1=max(labellengths), ofile=ofile)
    if not do_latex:
        prettyPrint("Systematic variation", systematic, "", width1=max(labellengths), ofile=ofile)

    if ofile:
        if not do_latex:
            ofile.write('###############################################################################\n')
        else:
            printLatexUp(ofile, case)
    else:
        print('###############################################################################')

    Yields = "Yields (%)" if percentage_relative_yields else "Yields"
    if not do_latex:
        prettyPrint("Cut", Yields, "", width1=max(labellengths), ofile=ofile)
    else:
        prettyPrintLatex("Cut", Yields, "", ofile=ofile)
    if ofile:
        if not do_latex:
            ofile.write('###############################################################################\n')
    else:
        print('###############################################################################')
    width2 = 0
    for ibin in range(1, Nbins + 1):
        thecontent = CutFlowHisto.GetBinContent(ibin)
        theerror = CutFlowHisto.GetBinError(ibin)
        thelabel = CutFlowHisto.GetXaxis().GetBinLabel(ibin)

        if relative_yields:
            lumi = 1.
            if ibin == 1:
                ref_content = thecontent
                ref_error = theerror

            thecontent, theerror = eff_divide(thecontent, theerror, ref_content, ref_error)
            if percentage_relative_yields:
                thecontent *= 100.
                theerror *= 100.

        if not show_empty:
            if thecontent == 0:
                break
        if doRaw:
            content = '%.0f ' % (thecontent)
            error = ' %.2f' % (theerror)
        else:
            content = '%.8f ' % (thecontent * lumi)
            error = ' %.8f' % (theerror * lumi)
        if ibin == 1:
            width2 = len(content)

        if not do_latex:
            prettyPrint(
                thelabel,
                content,
                error,
                width1=max(labellengths),
                width2=width2,
                #separator2=u'\u00B1',
                separator2='+-',
                ofile=ofile)
        else:
            prettyPrintLatex(thelabel, content, error, ofile=ofile)

    if ofile:
        if not do_latex:
            ofile.write('###############################################################################\n')
        else:
            printLatexDown(ofile, case)

    else:
        print '###############################################################################\n'


# this function prints uniformly-formatted lines of the type 'preamble : data1 data2' (if separator1=':')
def prettyPrint(preamble, data1, data2, width1=24, width2=10, separator1="", separator2="", ofile=None):
    preamble = preamble.ljust(width1)
    data1 = data1.ljust(width2)
    if ofile:
        line = preamble + separator1 + data1 + separator2 + data2 + "\n"
        ofile.write(line)
    else:
        print(preamble + separator1 + data1 + separator2 + data2)


def printLatexUp(f=None, case=""):
    f.write('''\\begin{table}
\\begin{center}
\\begin{tabular}{l|l}
''')


def printLatexDown(f=None, case=""):
    Case = case.replace("_", " ").replace(".", ", ")
    f.write('''
\\hline
\\end{tabular}
\\end{center}
\\caption{Cutflow for %s.}
\\label{tb:cutflow%s}
\\end{table}
''' % (Case, case))


def prettyPrintLatex(preamble="", data1="", data2="", ofile=None):
    label = adjust(preamble)
    symb = "$\\pm$" if data2 else ""
    lines = "\\hline\\hline" if not data2 else ""
    if ofile:
        line = label + "\t&" + data1 + symb + data2 + "\\\\" + lines + "\n"
        ofile.write(line)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog='PrintCutFlow', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-c', '--case', help='physics case', default='')
    parser.add_argument('-i', '--inputFile', help='choose input ROOT file', default='AnalysisOutput.root')
    parser.add_argument('-a', '--analysis', help='choose CutFlow to be printed', default='MyCutFlow')
    parser.add_argument('-o', '--outfile', help='output text file', default='yields')
    parser.add_argument('-r', '--region', help='choose region to be printed', default='MyCutFlow', required=True)
    parser.add_argument('-e', '--show-empty', help='show empty bins', action='store_true', default=False)
    parser.add_argument('-f', '--store-file', help='store file', action='store_true', default=False)
    parser.add_argument('-x', '--latex-style', help='store latex file', action='store_true', default=False)
    parser.add_argument('-w', '--weighted', help='use for weighted cutflow comparisons', action='store_true', default=False)
    parser.add_argument('-s', '--systematic', help='Systematic to choose', default="Nominal")
    parser.add_argument('-l', '--lumi', help='specify a luminosity for which the CutFlow shall be printed', default=1., type=float)
    parser.add_argument('-y', '--relative-yields', help='print fractional, relative yields', action='store_true', default=False)
    parser.add_argument('-p',
                        '--percentage-relative-yields',
                        help='print fractional, relative yields as percentage numbers',
                        action='store_true',
                        default=False)

    RunOptions = parser.parse_args()

    # Set batch mode and atlas style
    ROOT.gROOT.SetBatch(True)

    ext = ".tex" if RunOptions.latex_style else ".txt"
    outf_name = RunOptions.outfile + ext
    outf = open(outf_name, "w") if RunOptions.store_file else None
    case = RunOptions.case + "." + RunOptions.region
    if RunOptions.store_file and not outf:
        logging.error("Cannot create output file '%s'..." % (RunOptions.outfile))
        exit(1)

    CutFlowHisto = CutFlowYield(smp_name=RunOptions.inputFile[RunOptions.inputFile.find("/") + 1:RunOptions.inputFile.rfind(".")],
                                analysis=RunOptions.analysis,
                                region=RunOptions.region,
                                root_files=[RunOptions.inputFile],
                                use_weighted=RunOptions.weighted)

    printCutFlow(case=case,
                 CutFlowHisto=CutFlowHisto.get_histo(),
                 systematic=RunOptions.systematic,
                 lumi=RunOptions.lumi,
                 show_empty=RunOptions.show_empty,
                 ofile=outf,
                 doRaw=not RunOptions.weighted,
                 do_latex=RunOptions.latex_style,
                 relative_yields=RunOptions.relative_yields,
                 percentage_relative_yields=RunOptions.percentage_relative_yields)
    if outf:
        print "Info: output file created:", outf_name
    ClearServices()
