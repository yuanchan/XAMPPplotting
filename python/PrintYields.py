#! /usr/bin/env python
import math, logging
from ClusterSubmission.Utils import FillWhiteSpaces, ClearFromDuplicates, convertStdVector
from XAMPPplotting.Defs import *
from XAMPPplotting.Utils import setupBaseParser
from XAMPPplotting.PlottingHistos import CreateHistoSets
from XAMPPplotting.CheckMetaData import prettyPrint, max_width, writeTableHeader, formatItem, writeTableFooter
from XAMPPplotting.FileStructureHandler import GetStructure, ClearServices
from XAMPPplotting.StatisticalFunctions import ButtingerLefebvreSignificance


class CutFlowYield(object):
    def __init__(self, smp_histo=None, smp_name="", analysis="", region="", systematic="", root_files=[], use_weighted=True, lumi=1.):

        self.__smp_histo = smp_histo
        if not self.__smp_histo:
            self.__smp_histo = ROOT.XAMPP.SampleHisto(smp_name, "InfoHistograms/CutFlow%s" % ("" if not use_weighted else "_weighted"),
                                                      analysis, region,
                                                      convertStdVector(root_files) if not isinstance(root_files, str) else root_files)
            if len(systematic) > 0: self.__smp_histo.set_nominal(systematic)
            if not self.__smp_histo.loadIntoMemory():
                raise NameError('Failed to load cutflow')
            if use_weighted:
                self.__smp_histo.SetLumi(lumi)
        self.__cf_bin = [i for i in range(self.get_histo().GetNbinsX()) if self.get_histo().GetXaxis().GetBinLabel(i) == "Final"][0]

    def get_histo(self):
        return self.__smp_histo

    def get_n_bins(self):
        return self.__cf_bin

    def label(self):
        return self.__smp_histo.GetTitle()

    def region(self):
        return self.__smp_histo.GetRegion()

    def name(self):
        return self.__smp_histo.GetName()

    def get_yield(self, cf_bin=0):
        return self.__smp_histo.GetBinContent(self.__cf_bin if cf_bin < 1 or cf_bin > self.get_n_bins() else cf_bin)

    def get_stat_error(self, cf_bin=0):
        return self.__smp_histo.GetBinError(self.__cf_bin if cf_bin < 1 or cf_bin > self.get_n_bins() else cf_bin)

    def get_sys_up(self, cf_bin=0):
        if not self.has_sys():
            logging.warning("No systematic band has been defined for %s" % (self.label()))
            return 0.
        return self.__smp_histo.GetSystErrorBands()[1].GetBinContent(self.__cf_bin if cf_bin < 1 or cf_bin > self.get_n_bins() else cf_bin)

    def get_sys_dn(self, cf_bin=0):
        if not self.has_sys():
            logging.warning("No systematic band has been defined for %s" % (self.label()))
            return 0.
        return self.__smp_histo.GetSystErrorBands()[0].GetBinContent(self.__cf_bin if cf_bin < 1 or cf_bin > self.get_n_bins() else cf_bin)

    def get_max_sys(self, cf_bin=0):
        return max(self.get_sys_up(cf_bin=cf_bin), self.get_sys_dn(cf_bin=cf_bin))

    def get_comb_error(self, cf_bin=0):
        return math.sqrt(self.get_max_sys(cf_bin)**2 + self.get_stat_error(cf_bin)**2)

    def has_sys(self):
        return not self.__smp_histo.GetSystComponents().empty()

    def sample_type(self):
        return self.__smp_histo.getSampleType()

    def is_data(self):
        return self.__smp_histo.isData()

    def is_signal(self):
        return self.__smp_histo.isSignal()

    def print_yield(self, rel_error=False, combine_sys=False, doLaTeX=False):
        ln_10 = int(math.log10(math.fabs(self.get_yield()))) - 1 if self.get_yield() != 0. and self.get_yield() <= 1.e-2 else 0
        yield_str = ""
        if not rel_error:

            yield_str = "%.2f \\pm %.2f" % (self.get_yield() * pow(10., math.fabs(ln_10)),
                                            (self.get_stat_error() if not combine_sys or not self.has_sys() else self.get_comb_error()) *
                                            pow(10., math.fabs(ln_10)))

            if not combine_sys and self.has_sys():
                yield_str += "^{%.2f}_{%.2f}" % (self.get_sys_up() * pow(10., math.fabs(ln_10)),
                                                 self.get_sys_dn() * pow(10., math.fabs(ln_10)))
        else:
            yield_str = "%.2f \\pm %.2f%%" % (self.get_yield() * pow(10., math.fabs(ln_10)),
                                              (self.get_stat_error() if not combine_sys or not self.has_sys() else self.get_comb_error()) /
                                              self.get_yield() if self.get_yield() != 0. else 0.)
            if not combine_sys and self.has_sys():
                yield_str += "^{%.2f %%}_{%.2f %%}" % (self.get_sys_up() / self.get_yield() if self.get_yield() != 0. else 0.,
                                                       self.get_sys_up() / self.get_yield() if self.get_yield() != 0. else 0.)
        if ln_10 != 0: yield_str = "(%s)\\cdot 10^{-%d}" % (yield_str, math.fabs(ln_10))
        if doLaTeX: yield_str = "$%s$" % (yield_str)
        return formatItem(yield_str, doLaTeX=doLaTeX)

    def print_purity(self, sum_bg=None, in_percent=False, include_sys=False, doLaTeX=False):
        if sum_bg.get_yield() < 0.:
            logging.warning('Background seems to be empty for Sample %s, returning 1\pm1' % sum_bg)
            return "nan"
        purity = self.get_yield() / sum_bg.get_yield()
        stat_error = math.sqrt((self.get_stat_error() / sum_bg.get_yield())**2 + (self.get_yield() * sum_bg.get_stat_error() /
                                                                                  (sum_bg.get_yield()**2))**2)

        tot_error = stat_error
        if self.has_sys() and sum_bg.has_sys() and include_sys:
            sys_error = math.sqrt((self.get_max_sys() / sum_bg.get_yield())**2 + (self.get_yield() * sum_bg.get_max_sys() /
                                                                                  (sum_bg.get_yield()**2))**2)
            tot_error = math.sqrt(sys_error**2 + stat_error**2)
        if in_percent:
            purity *= 100.
            tot_error *= 100.
        to_return = "%.4f \\pm %.4f" % (purity, tot_error) if not in_percent else "%.1f \\pm %.1f%%" % (purity, tot_error)
        if doLaTeX: to_return = "$%s$" % (to_return)
        return formatItem(to_return, doLaTeX=doLaTeX)

    def calculate_sf(self, sum_bg=None, data=None, include_sys=False, doLaTeX=False):
        remain_bkg = sum_bg.get_yield() - self.get_yield()
        remain_error = math.sqrt(math.fabs(sum_bg.get_stat_error()**2 - self.get_stat_error()**2))

        sf = (data.get_yield() - remain_bkg) / self.get_yield()

        stat_error = math.sqrt((data.get_stat_error() / self.get_yield())**2 + (remain_error / self.get_yield())**2 +
                               ((data.get_yield() - remain_bkg) * self.get_stat_error() / (self.get_yield()**2))**2)

        tot_error = stat_error
        if self.has_sys() and sum_bg.has_sys() and include_sys:
            remain_sys_error = math.sqrt(math.fabs(sum_bg.get_max_sys()**2 - self.get_max_sys()**2))
            sys_error = math.sqrt((data.get_max_sys() / self.get_yield())**2 + (remain_sys_error / self.get_yield())**2 +
                                  ((data.get_yield() - remain_bkg) * self.get_max_sys() / (self.get_yield()**2))**2)
            tot_error = math.sqrt(sys_error**2 + stat_error**2)

        to_return = "%.2f \\pm %.2f" % (sf, tot_error)
        if doLaTeX: to_return = "$%s$" % (to_return)
        return formatItem(to_return, doLaTeX=doLaTeX)

    def print_significance(self, sum_bg, rel_error=0.3, use_buttinger=False, doLaTeX=False):
        if not self.is_signal():
            logging.error("Significances only for signal samples possible")
            return "invalid"
        significance = self.calculate_significance(sum_bg=sum_bg, rel_error=rel_error, use_buttinger=use_buttinger)
        if significance == 0.: return "---"
        to_return = "%.2fsigma" % (significance)
        # ~ if doLaTeX: to_return = "%s$" % (to_return)
        return formatItem(to_return, doLaTeX=doLaTeX)

    def calculate_significance(self, sum_bg, rel_error=0.3, use_buttinger=False):
        background = sum_bg.get_yield()
        significance = 0.
        if not use_buttinger:
            significance = ROOT.RooStats.NumberCountingUtils.BinomialExpZ(self.get_yield(), sum_bg.get_yield(), rel_error)
        else:
            significance = ButtingerLefebvreSignificance(self.get_yield(), sum_bg.get_yield(), rel_error * sum_bg.get_yield())
        return min(max(significance, 0.), 10.)


class CutFlowSample(object):
    def __init__(self, smp_name, smp_label, cfg_name=""):
        self.__smp_name = smp_name
        self.__ds_cfg_name = cfg_name
        self.__smp_label = smp_label
        self.__regions = []

    def is_data(self):
        return self.__regions[0].is_data()

    def is_signal(self):
        return self.__regions[0].is_signal()

    def ds_cfg_name(self):
        return self.__ds_cfg_name

    def label(self):
        return self.__smp_label

    def name(self):
        return self.__smp_name

    def get_regions(self):
        return [r.region() for r in self.__regions]

    def get_region_cf(self, region):
        for r in self.__regions:
            if r.region() == region: return r
        logging.warning("Invalid region %s" (region))
        return None

    def add_region(self, cf_yield):
        if cf_yield.name() != self.name():
            logging.warning("The labels between %s and %s are incompatible" % (cf_yield.name(), self.name()))
            return
        self.__regions += [cf_yield]

    def print_yield(self, rel_error=False, combine_sys=False, doLaTeX=False):
        smp_name = formatItem(self.label(), doLaTeX=False)
        #if doLaTeX and smp_name != self.label(): smp_name = "$%s$" % (smp_name)
        return [smp_name] + [r.print_yield(rel_error=rel_error, combine_sys=combine_sys, doLaTeX=doLaTeX) for r in self.__regions]

    def print_purity(self, summed_bg, in_percent=False, include_sys=False, doLaTeX=False):
        smp_name = formatItem(self.label(), doLaTeX=False)
        if doLaTeX and smp_name != self.label(): smp_name = "$%s$" % (smp_name)
        return ["Purity %s" % (smp_name)] + [
            r.print_purity(sum_bg=summed_bg.get_region_cf(r.region()), in_percent=in_percent, include_sys=include_sys, doLaTeX=doLaTeX)
            for r in self.__regions
        ]

    def calculate_sf(self, summed_bg, data_sample, include_sys=False, doLaTeX=False):
        smp_name = formatItem(self.label(), doLaTeX=doLaTeX)
        #if doLaTeX and smp_name != self.label(): smp_name = "$%s$" % (smp_name)
        return ["Scale factor %s" % (smp_name)] + [
            r.calculate_sf(sum_bg=summed_bg.get_region_cf(r.region()),
                           data=data_sample.get_region_cf(r.region()),
                           include_sys=include_sys,
                           doLaTeX=doLaTeX) for r in self.__regions
        ]

    def print_significance(self, sum_bg, rel_error=0.3, use_buttinger=False, doLaTeX=True):
        smp_name = formatItem(self.label(), doLaTeX=False)
        #if doLaTeX and smp_name != self.label(): smp_name = "$%s$" % (smp_name)
        return ["Significance %s" % (smp_name)] + [
            r.print_significance(sum_bg=sum_bg.get_region_cf(r.region()), rel_error=rel_error, use_buttinger=use_buttinger, doLaTeX=doLaTeX)
            for r in self.__regions
        ]

    def calculate_significance(self, sum_bg, rel_error=0.3, use_buttinger=False):
        return min(
            math.sqrt(
                sum([
                    r.calculate_significance(sum_bg=sum_bg.get_region_cf(r.region()), rel_error=rel_error, use_buttinger=use_buttinger)**2
                    for r in self.__regions
                ])), 10.)


def yield_sample_sort(x, y):
    if x.sample_type() != y.sample_type(): return 1 if x.sample_type() - y.sample_type() > 0 else -1
    if x.label() < y.label(): return 1
    return -1


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='This script print yields for different regions -h\"',
                                     prog='PrintYields',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser = setupBaseParser(parser)
    parser.add_argument('--purity', help='specify sample name for purity calculation', nargs='+', default=[])
    parser.add_argument("--relError", action="store_true", default=False)
    parser.add_argument("--purityInPercent", help='print the purity in percent', action="store_true", default=False)
    parser.add_argument("--NDigits", help='specify number of digits after dot', type=int, default=2)
    parser.add_argument('-d', "--useData", action="store_true", default=False)
    parser.add_argument('--SF', help='specify sample name for scale factor calculation', nargs='+', default=[])
    parser.add_argument('--SignalPercent',
                        help='print percentage of signal contribution compared to background',
                        action="store_true",
                        default=False)
    parser.add_argument('--PrintSignificance', help='print significance of signals in addition', action="store_true", default=False)
    parser.add_argument('-t', '-T', '--tex', help='specify filename for writing table to a LaTeX file', default='')

    parser.add_argument('--ScaleTeXVertically',
                        action="store_true",
                        help='scale LaTeX table vertically instead of horizontally',
                        default=False)
    parser.add_argument('--includeSysError',
                        action="store_true",
                        help='include systematic error when calculating SF and purity',
                        default=False)
    parser.add_argument('--combineErrors', action="store_true", help='Combine the statistical and systematic error', default=False)
    parser.add_argument("--UseSUSYSignificance",
                        help="Use the Buttinger significance definition instead",
                        action="store_true",
                        default=False)
    parser.add_argument('--BackgroundUncertainty',
                        help='specify relative uncertainty on background for significance calculation',
                        type=float,
                        default=0.3)

    parser.set_defaults(outputDir="./")
    RunOptions = parser.parse_args()

    if len(RunOptions.config) == 0:
        logging.error('Please specify a DSConfig to use for printing significances!')
        sys.exit(1)

    FileStructure = GetStructure(RunOptions)

    if not RunOptions.useData and len(RunOptions.SF) > 0:
        logging.error('Cannot calculate Data/MC scale factor without data! Did you forget to turn on the "--useData" option?')
        sys.exit(1)

    OutFile = None
    if RunOptions.tex != '':
        OutFile = open("%s/%s.tex" % (RunOptions.outputDir, RunOptions.tex), "w")
        OutFile.write("\\documentclass{article}\n")
        OutFile.write("\\usepackage[a4paper,margin=1in,landscape]{geometry}\n")
        OutFile.write("\\usepackage{booktabs}\n")
        OutFile.write("\\usepackage{graphicx}\n")
        OutFile.write("\\usepackage{adjustbox}\n")
        OutFile.write("\\begin{document}\n")

    luminosity = RunOptions.lumi
    cf_samples = []
    #### Extract all the cutflows from the input rootfiles
    for ana in FileStructure.GetConfigSet().GetAnalyses():
        for region in FileStructure.GetConfigSet().GetAnalysisRegions(ana):
            if len(RunOptions.regions) > 0 and not region in RunOptions.regions:
                continue
            HistoSets = CreateHistoSets(RunOptions, ana, region, "InfoHistograms/CutFlow_weighted", UseData=False)
            if not HistoSets: continue
            ### use a tuple of two items to match the raw histosets with the weighted cutflow sets
            smp_histos = []
            if RunOptions.useData:
                RawHistoSets = CreateHistoSets(RunOptions, ana, region, "InfoHistograms/CutFlow", UseData=RunOptions.useData)
                smp_histos = [(mc_set, rawset) for mc_set in HistoSets for rawset in RawHistoSets if mc_set.GetName() == rawset.GetName()]
            else:
                smp_histos = [(mc_set, mc_set) for mc_set in HistoSets]

            ### Now construct CutFlow samples for each set
            for mc, data in smp_histos:
                if math.isnan(mc.GetSummedBackground().Integral()):
                    logging.warning("Invalid background %s" % (mc.GetName()))
                    continue
                mc.SetLumi(data.GetLumi() if RunOptions.useData else RunOptions.lumi)
                ### assign each CutFlowYield to a CutFlowSample
                for h in mc.GetBackgrounds() + [mc.GetSummedBackground()
                                                ] + mc.GetSignals() + ([] if not RunOptions.useData else data.GetData()):
                    cf_yield = CutFlowYield(h)
                    try:
                        cf_smp = [cf for cf in cf_samples if cf.name() == cf_yield.name() and mc.GetName() == cf.ds_cfg_name()][0]
                    except:
                        cf_smp = CutFlowSample(smp_name=cf_yield.name(), smp_label=cf_yield.label(), cfg_name=mc.GetName())
                        cf_samples += [cf_smp]
                    cf_smp.add_region(cf_yield)

    ### Sort out the DSConfigs given
    cfg_names = ClearFromDuplicates([cf.ds_cfg_name() for cf in cf_samples])
    for cfg in cfg_names:
        ### In the next iteration the samples inside each DSConfig
        smp_in_cfg = [cf for cf in cf_samples if cf.ds_cfg_name() == cfg_name] if len(cfg_names) > 1 else cf_samples

        regions = smp_in_cfg[0].get_regions()
        the_header = [[''] + regions]

        ### Build the table one sample per line
        the_table = the_header + [
            smp.print_yield(rel_error=RunOptions.relError, combine_sys=RunOptions.combineErrors, doLaTeX=False) for smp in smp_in_cfg
        ]
        if len(RunOptions.purity) > 0:
            summed_bg = [cf for cf in smp_in_cfg if cf.name() == "SumBG"][-1]
            for wanted_purity in RunOptions.purity:
                try:
                    purity_smp = [cf for cf in smp_in_cfg if cf.name() == wanted_purity][0]
                except:
                    logging.warning("No background sample has been found named %s" % (wanted_purity))
                    continue
                the_table += [
                    purity_smp.print_purity(summed_bg=summed_bg,
                                            in_percent=RunOptions.purityInPercent,
                                            include_sys=RunOptions.includeSysError,
                                            doLaTeX=False)
                ]

        if len(RunOptions.SF) > 0:
            summed_bg = [cf for cf in smp_in_cfg if cf.name() == "SumBG"][-1]
            data = [cf for cf in smp_in_cfg if cf.is_data()][-1]
            for wanted_sf in RunOptions.SF:
                try:
                    sf_smp = [cf for cf in smp_in_cfg if cf.name() == wanted_sf][0]
                except:
                    logging.warning("No background sample has been found named %s" % (wanted_sf))
                    continue
                the_table += [
                    sf_smp.calculate_sf(summed_bg=summed_bg, data_sample=data, include_sys=RunOptions.includeSysError, doLaTeX=False)
                ]
        if RunOptions.PrintSignificance:
            sum_bg = [cf for cf in smp_in_cfg if cf.name() == "SumBG"][-1]
            for cf in smp_in_cfg:
                if cf.is_signal():
                    the_table += [
                        cf.print_significance(sum_bg=sum_bg,
                                              use_buttinger=RunOptions.UseSUSYSignificance,
                                              rel_error=RunOptions.BackgroundUncertainty,
                                              doLaTeX=False)
                    ]
        max_widths = max_width(the_table, doLaTeX=False)

        for the_line in the_table:
            print prettyPrint(itemsToPrint=the_line, doLaTeX=False, row_widths=max_widths)

        if OutFile:
            OutFile.write(writeTableHeader([''] + regions, vertical=RunOptions.ScaleTeXVertically))
            the_table = [
                smp.print_yield(rel_error=RunOptions.relError, combine_sys=RunOptions.combineErrors, doLaTeX=True) for smp in smp_in_cfg
            ]
            summed_bg = [cf for cf in smp_in_cfg if cf.name() == "SumBG"][-1]
            for wanted_purity in RunOptions.purity:
                try:
                    purity_smp = [cf for cf in smp_in_cfg if cf.name() == wanted_purity][0]
                except:
                    logging.warning("No background sample has been found named %s" % (wanted_purity))
                    continue
                the_table += [
                    purity_smp.print_purity(summed_bg=summed_bg,
                                            in_percent=RunOptions.purityInPercent,
                                            include_sys=RunOptions.includeSysError,
                                            doLaTeX=True)
                ]
            if len(RunOptions.SF) > 0:
                data = [cf for cf in smp_in_cfg if cf.is_data()][-1]
                for wanted_sf in RunOptions.SF:
                    try:
                        sf_smp = [cf for cf in smp_in_cfg if cf.name() == wanted_sf][0]
                    except:
                        logging.warning("No background sample has been found named %s" % (wanted_sf))
                        continue
                    the_table += [
                        sf_smp.calculate_sf(summed_bg=summed_bg, data_sample=data, include_sys=RunOptions.includeSysError, doLaTeX=True)
                    ]
            if RunOptions.PrintSignificance:
                for cf in smp_in_cfg:
                    if cf.is_signal():
                        the_table += [
                            cf.print_significance(sum_bg=summed_bg,
                                                  use_buttinger=RunOptions.UseSUSYSignificance,
                                                  rel_error=RunOptions.BackgroundUncertainty,
                                                  doLaTeX=True)
                        ]

            max_widths = max_width(the_table, doLaTeX=True)
            for the_line in the_table:
                OutFile.write(prettyPrint(itemsToPrint=the_line, doLaTeX=True, row_widths=max_widths))

            OutFile.write(writeTableFooter("hehehe", vertical=RunOptions.ScaleTeXVertically))

    if OutFile:
        OutFile.write("\\end{document}\n")
        OutFile.close()
        os.system("pdflatex %s.tex" % RunOptions.tex)
        os.system("rm %s.log" % RunOptions.tex)
        os.system("rm %s.aux" % RunOptions.tex)
    ClearServices()
