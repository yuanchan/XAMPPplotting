import os, ROOT
from ROOT import TH1F, TFile, TCanvas, TLegend, TPad, gStyle, gROOT, TLine, TGraphAsymmErrors, TLatex


#
def runs():
    return "276262_364292"


#
def leg(h=None):
    return h.GetName().replace("Period_" + runs(), "").replace("_", " ").replace("nominal", "")


def test(h=None, name=""):
    if not h:
        print "Error: Null histo '%s'" % (name)
        return False

    return True


#
#


def ZeroOutError(histo=None):

    #for i in xrange(1, histo.GetNbinsX()+1):
    #    histo.SetBinError(i, 0.)

    return histo


def HaxisZoom(h=None):

    h.GetXaxis().SetLabelSize(h.GetXaxis().GetLabelSize() * 1.1)
    h.GetXaxis().SetTitleSize(h.GetXaxis().GetTitleSize() * 1.1)


def get_template(histo=None):
    hempty = histo.Clone(histo.GetName() + "_clone")
    hempty.Sumw2(True)
    hempty.Reset()
    hempty.SetTitle("")

    return hempty


def HaxisNull(g=None):

    g.GetXaxis().SetLabelSize(0)
    g.GetXaxis().SetTitleSize(0)


def get_histo_limits(histo=None):
    if not histo:
        print "Error: Null histo, cannot retrieve range ..."
        return 0., 0.

    n = histo.GetNbinsX()
    return histo.GetBinLowEdge(1), histo.GetBinLowEdge(n) + histo.GetBinWidth(n)


def HLine(x1=0., x2=0., y=1.):
    line = TLine(x1, y, x2, y)
    line.SetLineColor(41)
    line.SetLineStyle(7)

    return line


def VLine(x=0., ymin=0., ymax=0.):
    vline = TLine(x, ymin, x, ymax)
    vline.SetLineColor(41)
    vline.SetLineStyle(7)
    vline.SetLineWidth(2)

    return vline


def HaxisSet(g=None, title=""):

    g.GetXaxis().SetLabelSize(g.GetXaxis().GetLabelSize() * 2.)
    g.GetXaxis().SetTitleSize(g.GetXaxis().GetTitleSize() * 2.)
    g.GetXaxis().SetTitleOffset(1.)
    if title: g.GetXaxis().SetTitle(title)


def VaxisSet(g=None, title="", miny=0., maxy=2.):

    g.GetYaxis().SetLabelSize(g.GetYaxis().GetLabelSize() * 2.)
    g.GetYaxis().SetTitleSize(g.GetYaxis().GetTitleSize() * 2.)
    g.GetYaxis().SetTitleOffset(0.75)
    if title: g.GetYaxis().SetTitle(title)
    g.GetYaxis().CenterTitle()
    g.SetMinimum(miny)
    g.SetMaximum(maxy)


def squeeze_last_bin_graph(histo=None, NoYerrors=False):
    print_result = False

    bins = histo.GetNbinsX()
    graph = TGraphAsymmErrors(bins)
    graph.SetTitle("")

    #range for histo = 1 : N
    #range for graph = 0 : N-1

    first_bin = 1
    prelast_bin = bins - 1
    last_bin = bins

    x_axis_ref = []

    for j, i in enumerate(xrange(first_bin, last_bin + 1)):  # enum = j

        y = histo.GetBinContent(i)
        x = histo.GetBinCenter(i)
        dx = histo.GetBinWidth(i)
        dy = histo.GetBinError(i)
        x_low_edge = x - dx / 2.
        x_high_edge = x + dx / 2.

        if i == first_bin:
            first_bin_dx = dx

        if i == prelast_bin:
            prelast_bin_high_edge = x + dx / 2.

        if i < last_bin:
            graph.SetPoint(j, x, y)
            graph.SetPointEYlow(j, dy)
            graph.SetPointEYhigh(j, dy)
            graph.SetPointEXlow(j, dx / 2.)
            graph.SetPointEXhigh(j, dx / 2.)

            x_axis_ref.append((x_low_edge, x - dx / 2.))

        else:
            last_bin_x = prelast_bin_high_edge + first_bin_dx / 2
            last_bin_dx = first_bin_dx / 2
            print "last bin", last_bin_x, last_bin_dx
            graph.SetPoint(j, last_bin_x, y)
            graph.SetPointEYlow(j, dy)
            graph.SetPointEYhigh(j, dy)
            graph.SetPointEXlow(j, last_bin_dx)
            graph.SetPointEXhigh(j, last_bin_dx)
            x_axis_ref.append((x_low_edge, last_bin_x - last_bin_dx))
            x_axis_ref.append((x_high_edge, last_bin_x + last_bin_dx))

        if print_result:
            X = ROOT.Double(0)
            Y = ROOT.Double(0)
            P = graph.GetPoint(j, X, Y)
            print j, graph.GetErrorXlow(j), X, graph.GetErrorXhigh(j), ":", Y

        if NoYerrors:
            graph.SetPointEYlow(j, 0.)
            graph.SetPointEYhigh(j, 0.)

    return graph, x_axis_ref


def retrieve_histo(f=None, name=""):

    h = f.Get(name)
    if not test(h, name):
        print "Error: problem with '%s'" % (name)
        exit(1)

    return h


def graph_nominal(rfile=None, SR="", CH="", ETA="", PRONG="", PERIOD=""):

    histo = retrieve_histo(rfile,
                           "FF_combined_%s_%s_CR_NotApplied_SR_%s_Eta_%s_Prong_%s_Period_%s" % ("nominal", CH, SR, ETA, PRONG, PERIOD))

    g = squeeze_last_bin_graph(histo, True)

    return g


def are_close(a, b, rel_tol=1e-06, abs_tol=0.0):
    return abs(a - b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)


def combine_graphs(glow=None, gcent=None, ghigh=None):

    print_result = False

    g = ROOT.TGraphAsymmErrors(ghigh.GetN())
    g.SetTitle("")

    for ibin in xrange(ghigh.GetN()):
        x_high = ROOT.Double(0)
        y_high = ROOT.Double(0)
        point_high = ghigh.GetPoint(ibin, x_high, y_high)

        x_cent = ROOT.Double(0)
        y_cent = ROOT.Double(0)
        point_cent = gcent.GetPoint(ibin, x_cent, y_cent)

        x_low = ROOT.Double(0)
        y_low = ROOT.Double(0)
        point_low = glow.GetPoint(ibin, x_low, y_low)

        if not are_close(x_high, x_cent) or not are_close(x_cent, x_low):
            print "Error: Graphs are not compatible..."
            return None

        dx_up = gcent.GetErrorXhigh(ibin)
        dx_down = gcent.GetErrorXlow(ibin)

        y_cent = (y_high - y_low) / 2.
        dy_high = y_high - y_cent
        dy_low = y_cent - y_low

        #set point
        g.SetPoint(ibin, x_cent, y_cent)

        #set dy
        g.SetPointEYlow(ibin, dy_low)
        g.SetPointEYhigh(ibin, dy_high)

        #set dx
        g.SetPointEXlow(ibin, dx_down)
        g.SetPointEXhigh(ibin, dx_up)

        if print_result:
            print "comb", ibin, dx_down, x_high, dx_up, ":", y_cent, dy_low, dy_high

    return g


def graph_stat(rfile=None, SR="", CH="", ETA="", PRONG="", PERIOD=""):

    histo_no = retrieve_histo(rfile,
                              "FF_combined_%s_%s_CR_NotApplied_SR_%s_Eta_%s_Prong_%s_Period_%s" % ("nominal", CH, SR, ETA, PRONG, PERIOD))
    histo_up = retrieve_histo(
        rfile, "FF_combined_%s_%s_CR_NotApplied_SR_%s_Eta_%s_Prong_%s_Period_%s" % ("stat_1sigma_up", CH, SR, ETA, PRONG, PERIOD))
    histo_do = retrieve_histo(
        rfile, "FF_combined_%s_%s_CR_NotApplied_SR_%s_Eta_%s_Prong_%s_Period_%s" % ("stat_1sigma_down", CH, SR, ETA, PRONG, PERIOD))

    g_no, x_axis = squeeze_last_bin_graph(histo_no)
    g_up, x_axis = squeeze_last_bin_graph(histo_up)
    g_do, x_axis = squeeze_last_bin_graph(histo_do)

    g = combine_graphs(g_do, g_no, g_up)

    return g


def Eta(s=""):
    return "#eta=[" + s.replace("p", ".").replace("_", ", ") + "]"


def Channel(s=""):
    if s == "Muo":
        return "#mu-#tau"
    elif s == "Ele":
        return "e-#tau"
    else:
        return "what?"


def Prong(s=""):
    return s + "-prong"


def get_stuff(rf=None, SR="", chan="", eta="", prong="", runs=""):

    eta_str = Eta(eta)
    chan_str = Channel(chan)
    prong_str = Prong(prong)

    name = "%s  %s  %s  %s" % (chan_str, SR, eta_str, prong_str)
    g, x_axis = graph_nominal(rf, SR, chan, eta, prong, runs)
    gs = graph_stat(rf, SR, chan, eta, prong, runs)
    gs.SetFillColor(3)
    gs.SetLineColor(3)

    return name, x_axis, g, gs


def core(SR="", prongs=""):

    margin = 0.02
    draw_graph_stat_opt = "a z2"
    draw_graph_nom_opt = "epz same"
    legx_min = 0.7
    legy_min = 0.8
    legy_max = 0.99
    g_name = "Nominal"
    gs_name = "Stat. Uncert."
    y_max_prong1 = .4
    y_max_prong3 = 0.4

    #output
    outdir = "./FF"
    outfile = "FF_combined_LepTau_SR_%s_%s_Period_%s" % (SR, prong, runs())

    #1
    name1, x_axis_label_pairs1, g1, g1s = get_stuff(rf, SR, "Muo", "0p0_0p8", prongs, runs())
    y1_min = -0.1
    y1_max = y_max_prong1 if prongs == "1" else y_max_prong3
    print x_axis_label_pairs1

    #2
    name2, x_axis_label_pairs2, g2, g2s = get_stuff(rf, SR, "Ele", "0p0_0p8", prongs, runs())
    y2_min = -0.1
    y2_max = y_max_prong1 if prongs == "1" else y_max_prong3
    print x_axis_label_pairs2

    #3
    name3, x_axis_label_pairs3, g3, g3s = get_stuff(rf, SR, "Muo", "0p8_2p5", prongs, runs())
    y3_min = -0.1
    y3_max = y_max_prong1 if prongs == "1" else y_max_prong3
    print x_axis_label_pairs3

    #2
    name4, x_axis_label_pairs4, g4, g4s = get_stuff(rf, SR, "Ele", "0p8_2p5", prongs, runs())
    y4_min = -0.1
    y4_max = y_max_prong1 if prongs == "1" else y_max_prong3
    print x_axis_label_pairs4

    #canvas
    c = TCanvas("c_" + outfile, outfile, 10, 10, 700, 1000)

    pad_offset = 0.05
    pad_y = (1 - pad_offset) / 4.

    pad_1_l = 0.
    pad_1_h = pad_offset + pad_y

    pad_2_l = pad_1_h
    pad_2_h = pad_2_l + pad_y

    pad_3_l = pad_2_h
    pad_3_h = pad_3_l + pad_y

    pad_4_l = pad_3_h
    pad_4_h = pad_4_l + pad_y

    ##1 ###################################################
    ## bottom
    c.cd()
    pad1 = TPad("pad1", "pad1", 0, pad_1_l, 1, pad_1_h)
    pad1.SetTopMargin(margin)
    pad1.SetBottomMargin(pad1.GetBottomMargin() * 1.5)
    pad1.SetRightMargin(pad1.GetRightMargin() * 0.7)
    pad1.SetGridx()
    pad1.SetTicks(1, 1)
    pad1.Draw()
    pad1.cd()
    gStyle.SetOptStat(0)

    HaxisSet(g1s, "p_{T} [GeV]")
    VaxisSet(g1s, "FF", y1_min, y1_max)

    g1s.Draw(draw_graph_stat_opt)
    g1.Draw(draw_graph_nom_opt)

    hline1 = HLine(x_axis_label_pairs1[0][0], x_axis_label_pairs1[-1][1], 0)
    hline1.Draw()

    vline1 = VLine(x_axis_label_pairs1[-2][0], y1_min, y1_max)
    vline1.Draw()

    legend1 = TLegend(legx_min, legy_min, 0.99, legy_max)
    legend1.SetHeader(name1)
    legend1.AddEntry(g1, g_name, "l")
    legend1.AddEntry(g1s, gs_name, "fs")
    legend1.Draw()

    ##2 ###################################################
    c.cd()
    pad2 = TPad("pad2", "pad2", 0, pad_2_l, 1, pad_2_h)
    pad2.SetTopMargin(margin)
    pad2.SetBottomMargin(margin)
    pad2.SetRightMargin(pad2.GetRightMargin() * 0.7)
    pad2.SetGridx()
    pad2.SetTicks(1, 1)
    pad2.Draw()
    pad2.cd()
    gStyle.SetOptStat(0)

    HaxisNull(g2s)
    VaxisSet(g2s, "FF", y2_min, y2_max)

    g2s.Draw(draw_graph_stat_opt)
    g2.Draw(draw_graph_nom_opt)

    hline2 = HLine(x_axis_label_pairs2[0][0], x_axis_label_pairs2[-1][1], 0)
    hline2.Draw()

    vline2 = VLine(x_axis_label_pairs2[-2][0], y2_min, y2_max)
    vline2.Draw()

    legend2 = TLegend(legx_min, legy_min, 0.99, legy_max)
    legend2.SetHeader(name2)
    legend2.AddEntry(g2, g_name, "l")
    legend2.AddEntry(g2s, gs_name, "fs")
    legend2.Draw()

    ##3
    c.cd()
    pad3 = TPad("pad3", "pad3", 0, pad_3_l, 1, pad_3_h)
    pad3.SetTopMargin(margin)
    pad3.SetBottomMargin(margin)
    pad3.SetRightMargin(pad3.GetRightMargin() * 0.7)
    pad3.SetGridx()
    pad3.SetTicks(1, 1)
    pad3.Draw()
    pad3.cd()
    gStyle.SetOptStat(0)

    HaxisNull(g3s)
    VaxisSet(g3s, "FF", y3_min, y3_max)

    g3s.Draw(draw_graph_stat_opt)
    g3.Draw(draw_graph_nom_opt)

    hline3 = HLine(x_axis_label_pairs3[0][0], x_axis_label_pairs3[-1][1], 0)
    hline3.Draw()

    vline3 = VLine(x_axis_label_pairs3[-2][0], y3_min, y3_max)
    vline3.Draw()

    legend3 = TLegend(legx_min, legy_min, 0.99, legy_max)
    legend3.SetHeader(name3)
    legend3.AddEntry(g3, g_name, "l")
    legend3.AddEntry(g3s, gs_name, "fs")
    legend3.Draw()

    ##4
    c.cd()
    pad4 = TPad("pad4", "pad4", 0, pad_4_l, 1, pad_4_h)
    pad4.SetTopMargin(margin * 1.1)
    pad4.SetBottomMargin(margin)
    pad4.SetRightMargin(pad4.GetRightMargin() * 0.7)
    pad4.SetGridx()
    pad4.SetTicks(1, 1)
    pad4.Draw()
    pad4.cd()
    gStyle.SetOptStat(0)

    HaxisNull(g4s)
    VaxisSet(g4s, "FF", y4_min, y4_max)

    g4s.Draw(draw_graph_stat_opt)
    g4.Draw(draw_graph_nom_opt)

    hline4 = HLine(x_axis_label_pairs4[0][0], x_axis_label_pairs4[-1][1], 0)
    hline4.Draw()

    vline4 = VLine(x_axis_label_pairs4[-2][0], y4_min, y4_max)
    vline4.Draw()

    legend4 = TLegend(legx_min, legy_min, 0.99, legy_max)
    legend4.SetHeader(name4)
    legend4.AddEntry(g4, g_name, "l")
    legend4.AddEntry(g4s, gs_name, "fs")
    legend4.Draw()

    ###
    c.Modified()
    c.Update()

    ##
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    name = "%s/%s.pdf" % (outdir, outfile)
    c.SaveAs(name)


##########################################################
#
rfilename = "TauFF.root"
rf = TFile.Open(rfilename)

if not rf:
    print "Error: input root file not existing..."
    exit(1)

SRs = ["PreSel", "0jet", "1jet"]

prongs = ["1", "3"]

if len(SRs) > 1:
    gROOT.SetBatch(1)

for SR in SRs:
    for prong in prongs:
        core(SR, prong)

## wait for input to keep the GUI (which lives on a ROOT event dispatcher) alive
if __name__ == '__main__':
    rep = ''
    while not rep in ['q', 'Q']:
        rep = raw_input('enter "q" to quit: ')
        if 1 < len(rep):
            rep = rep[0]
