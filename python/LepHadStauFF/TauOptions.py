import sys
import argparse
from XAMPPplotting.Utils import setupBaseParser
from XAMPPplotting import TauLogger as TL


class TauOptions(object):
    """
    Options parser class
    """
    def __init__(self, name=""):
        self.__name = name
        self.__msg = TL.msg(2)
        self.__parser = None
        self.__options = None
        self.__ready = self.parser()

    def parser(self):
        self.__parser = argparse.ArgumentParser(description='Options for the tau FF analysis"',
                                                prog=self.__name,
                                                formatter_class=argparse.ArgumentDefaultsHelpFormatter)

        self.__parser = setupBaseParser(self.__parser)
        self.__parser.set_defaults(noSyst=True)
        #common
        self.__parser.add_argument('--only-sum-bkg', help='Consider only the SumBG in plotting', action='store_true', default=False)
        self.__parser.add_argument('--dynamic_rebinning',
                                   help='Rebin the histograms using the signal-data distribution',
                                   action='store_true',
                                   default=False)
        self.__parser.add_argument('--aim_stat_error', help='Which is the relative statistical error to aim for', type=float, default=0.05)
        #### Observables to load
        self.__parser.add_argument("--fake_ratio_obs", help="Name of the observable to be used for the F calculation", default="TauPt")
        self.__parser.add_argument("--rfactor_obs", help="Name of the observable to be used for the F calculation", default="TauPtWidth")
        self.__parser.add_argument("--isolation_obs", help="Name of the observable to be used for the F calculation", default="")
        self.__parser.add_argument("--tau_definition",
                                   help="Name of the observable to be used for the F calculation",
                                   default="Base",
                                   choices=["LepTrigger", "TauTrigger", "Base"])

        self.__parser.add_argument("--generator", help="Which generator to pick of V+jets", default="", choices=["Sherpa", "MG5", "PowHeg"])

        self.__parser.add_argument("--out_file", help="Name of the observable to be used for the F calculation", default="TauFF.root")

        #FF related
        self.__parser.add_argument('--ff', help='Calculate FFs', action='store_true', default=False)
        self.__parser.add_argument('--flat-ff', help='Force to flattened FFs', action='store_true', default=False)

        #tau composition related
        self.__parser.add_argument('--composition', help='Calculate tau composition', action='store_true', default=False)

        self.__options = self.__parser.parse_args()

        if not self.__options:
            self.__parser.print_help()
            return False

        return True

    @property
    def instance(self):
        if self.__ready:
            self.__msg.info("TauOptions", "Getting valid options...")
            return self.__options
        self.__msg.fatal("TauOptions", "Invalid options...")
