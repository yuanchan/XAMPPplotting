import sys


class bcolors:
    VERBOSE = '\033[95m'
    DEBUG = '\033[94m'
    INFO = '\033[92m'
    WARNING = '\033[93m'
    ERROR = '\033[91m'
    FATAL = '\033[95m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


class msg(object):
    """
    Class to handle the printout verbosity
    """
    def __init__(self, level=2):
        self.__level = level
        #
        self.__fatal_level = 5
        self.__error_level = 4
        self.__warning_level = 3
        self.__info_level = 2
        self.__debug_level = 1
        self.__verbose_level = 0

    def fatal(self, where, what):
        if self.__level <= self.__fatal_level:
            print bcolors.FATAL + "Fatal!   " + bcolors.ENDC \
                + where + " : "+ what
            sys.exit("Exiting...")

    def error(self, where, what):
        if self.__level <= self.__error_level:
            print bcolors.ERROR + "Error!   " + bcolors.ENDC \
                + where + " : "+ what

    def warning(self, where, what):
        if self.__level <= self.__warning_level:
            print bcolors.WARNING + "Warning! " + bcolors.ENDC \
                + where + " : "+ what

    def info(self, where, what):
        if self.__level <= self.__info_level:
            print bcolors.INFO + "Info!    " + bcolors.ENDC \
                + where + " : "+ what

    def debug(self, where, what):
        if self.__level <= self.__debug_level:
            print bcolors.DEBUG + "Debug!   " + bcolors.ENDC \
                + where + " : "+ what

    def verbose(self, where, what):
        if self.__level <= self.__verbose_level:
            print bcolors.VERBOSE + "Verbose!   " + bcolors.ENDC \
                + where + " : "+ what
