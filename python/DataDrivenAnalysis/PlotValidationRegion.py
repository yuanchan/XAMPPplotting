import ROOT
import sys
import argparse
from XAMPPplotting.Defs import *
from XAMPPplotting.PlotUtils import *
import XAMPPplotting.Utils
import XAMPPplotting.PlottingHistos
import Common as com
import math
import os
from array import array
from ClusterSubmission.Utils import CreateDirectory
from XAMPPplotting.PlotUtils import PlotUtils, draw2DHisto, Stack


def CreateReducibleBkg(Options):
    CR1_VR0 = CreateCRHisto(Options, "CR1_VR0")
    CR2_VR0 = CreateCRHisto(Options, "CR2_VR0")

    CR1_VR1 = CreateCRHisto(Options, "CR1_VR1")
    CR2_VR1 = CreateCRHisto(Options, "CR2_VR1")

    CR1_VR2 = CreateCRHisto(Options, "CR1_VR2")
    CR2_VR2 = CreateCRHisto(Options, "CR2_VR2")

    Reducible_VR0 = CR1_VR0.Clone("VR0 Reducible")
    Reducible_VR0.Add(CR2_VR0)

    Reducible_VR1 = CR1_VR1.Clone("VR1 Reducible")
    Reducible_VR1.Add(CR2_VR1)

    Reducible_VR2 = CR1_VR2.Clone("VR2 Reducible")
    Reducible_VR2.Add(CR2_VR2)

    ReducibleHistos = {"VR0": Reducible_VR0, "VR1": Reducible_VR1, "VR2": Reducible_VR2}
    return ReducibleHistos


def PlotVRHisto(Options, VR, ReducibleHisto):
    ROOT.gStyle.SetOptStat(0)
    ROOT.gStyle.SetOptTitle(0)
    HistoSets = XAMPPplotting.PlottingHistos.CreateHistoSets(Options, "Fourlepton", "Validationhistos", VR, True, True)
    if not HistoSets:
        return None
    HistoSet = HistoSets[0]

    pu = PlotUtils(status=Options.label, size=24)
    CanvasName = VR

    pu.Prepare1PadCanvas(CanvasName, 800, 600, Options.quadCanvas)
    can = pu.GetCanvas()
    can.SetBottomMargin(0.15)

    for histo in HistoSet.GetBackgrounds():
        histo.GetHistogram().GetXaxis().SetTitle("m_{eff} [GeV]")

    pu.CreateLegend(Options.LegendXCoords[0], Options.LegendYCoords[0], Options.LegendXCoords[1], Options.LegendYCoords[1])
    stk = Stack(HistoSet.GetBackgrounds())
    stk.GetXaxis().SetTitle("m_{eff} [GeV]")
    pu.AddToLegend(stk.GetHistogramStack(), Style="FL")
    ReducibleHisto.SetName("Reducible")
    ReducibleHisto.SetTitle("Reducible")
    ReducibleHisto.SetLineColor(ROOT.kYellow)
    ReducibleHisto.SetMarkerColor(ROOT.kYellow)
    ReducibleHisto.SetFillColor(ROOT.kYellow)
    pu.AddToLegend(ReducibleHisto, Style="FL")
    stk.Add(ReducibleHisto, ROOT.kYellow)

    datasamples = [D for D in HistoSet.GetData()]
    HistList = []
    HistToDraw = []
    for Data in datasamples:
        Data.GetHistogram().GetXaxis().SetTitle("m_{eff} [GeV]")
        Halo = pu.CreateHaloHistogram(Data.GetHistogram())
        HistToDraw.append((Halo, "same"))
        HistToDraw.append((Data, "same"))
        HistList.append(Data)

    HistList.append(HistoSet.GetSummedBackground())
    ymin, ymax = pu.GetFancyAxisRanges(HistList, Options)

    if math.fabs(ymin) <= 1.e-10 and math.fabs(ymax) <= 1.e-10:
        logging.info("Night time %.2f day time %.2f... Skip plot" % (ymin, ymax))
        return False

    pu.drawStyling(HistoSet.GetSummedBackground(), ymin, ymax, RemoveLabel=not Options.noRatio, TopPad=not Options.noRatio)

    stk.Draw("sameHist")
    # ~ pu.drawSumBg(HistoSet.GetSummedBackground(), (not Options.noSyst and not Options.noUpperPanelSyst))
    for H, Opt in HistToDraw:
        H.Draw(Opt)

    ### Add the histograms to legend
    pu.AddToLegend(datasamples, Style="PL")
    # ~ pu.AddToLegend(stk.GetHistogramStack(), Style="FL")
    # ~ pu.AddToLegend(HistoSet.GetSignals(), Style="L")
    pu.DrawLegend(NperCol=Options.EntriesPerLegendColumn)
    xCoord = 0.195
    yCoord = 0.83
    if not Options.noATLAS:
        pu.DrawAtlas(xCoord, yCoord)
        yCoord -= 0.08
    pu.DrawRegionLabel("ReducibleBkg", VR, xCoord, yCoord)

    pu.saveHisto("%s/%s" % (Options.outputDir, CanvasName), Options.OutFileType)
    can.SaveAs("%s/AllVRPlots.pdf" % (Options.outputDir))


def CreateCRHisto(Options, CR_Histo):
    CRHisto = None

    HistoSets = XAMPPplotting.PlottingHistos.CreateHistoSets(Options, "Fourlepton", "Validationhistos", CR_Histo, True, True)
    if not HistoSets:
        return
    HistoSet = HistoSets[0]
    for histo in HistoSet.GetBackgrounds():
        histo.GetHistogram().GetXaxis().SetTitle("m_{eff} [GeV]")
        if not CRHisto:
            CRHisto = histo.GetHistogram().Clone()
        else:
            CRHisto.Add(histo.GetHistogram().Clone())

    # ~ CRHisto.Scale(HistoSet.GetLumi())
    for histo in HistoSet.GetData():
        histo.GetHistogram().GetXaxis().SetTitle("m_{eff} [GeV]")
        if not CRHisto:
            CRHisto = histo.GetHistogram().Clone()
        else:
            CRHisto.Add(histo.GetHistogram().Clone())
    return CRHisto


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='This Script produces testhistos in VR"',
                                     prog='CreateVRPlots',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser = XAMPPplotting.Utils.setupBaseParser(parser)

    Options = parser.parse_args()
    CreateDirectory(Options.outputDir, False)
    dummycanvas = ROOT.TCanvas("AllVRPlots", "", 1200, 800)
    dummycanvas.SaveAs("%s/AllVRPlots.pdf[" % (Options.outputDir))
    ReducibleHistos = CreateReducibleBkg(Options)

    PlotVRHisto(Options, "VR0", ReducibleHistos["VR0"])
    PlotVRHisto(Options, "VR1", ReducibleHistos["VR1"])
    PlotVRHisto(Options, "VR2", ReducibleHistos["VR2"])

    dummycanvas.SaveAs("%s/AllVRPlots.pdf]" % (Options.outputDir))
