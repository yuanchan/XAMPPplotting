import ROOT
import argparse
import XAMPPplotting.Utils
import XAMPPplotting.PlottingHistos
import collections
import Common as com
import CalculateProcessFractions
import math
import itertools
from array import array
from ClusterSubmission.Utils import CreateDirectory


class FakeFactorWeight(object):
    def __init__(self, options):
        self.FakeFactor = {}
        print "initialising Fakefactor Histos"
        Regions = [
            "3L1l", "3L1lnoZ", "3L1lZ", "2L2l", "2L2lnoZ", "2L2lZ", "2L1l1t", "2L1l1tnoZ", "2L1l1tZ", "2L1T1l", "2L1T1lnoZ", "2L1T1lZ",
            "2L1T1t", "2L1T1tnoZ", "2L1T1tZ", "2L2t", "2L2tnoZ", "2L2tZ", "3L1t", "3L1tnoZ", "3L1tZ"
        ]
        for region in Regions:
            self.FakeFactor[region] = FakeFactor(region, options)
        print "initialising Fakefactor Histos. Done"

    def IsReal(self, event, particle, pt, eta):

        if particle == "LightLepton":
            for i in range(len(event.RealLooseLightLep_pt)):
                if event.RealLooseLightLep_pt[i] == pt and event.RealLooseLightLep_eta[i] == eta:
                    return True

        elif particle == "Tau":
            for i in range(len(event.RealLooseInclTau_pt)):
                if event.RealLooseInclTau_pt[i] == pt and event.RealLooseInclTau_eta[i] == eta:
                    return True
        else:
            print "unknown particle"
            return False
        return False

    def EvaluateFakeFactorWeight(self, event, region, lightleptonnumber, taunumber, RealSignalLightLep, RealSignalTau, isCR2=False):

        FakeFactors_lightlepton_fake = []
        FakeFactors_lightlepton_real = []
        FakeFactors_tau_fake = []
        FakeFactors_tau_real = []
        IrreducibleBkg = False
        if len(event.RealSignalLightLep_pt) >= RealSignalLightLep and len(event.RealLooseLightLep_pt) >= lightleptonnumber and len(
                event.RealSignalInclTau_pt) >= RealSignalTau and len(event.RealLooseInclTau_pt) >= taunumber:
            IrreducibleBkg = True

        if not lightleptonnumber == 0:
            for i in range(len(event.LooseElectron_pt)):  # just loop over pt, eta has the same number of entries
                pt = event.LooseElectron_pt[i]
                eta = event.LooseElectron_eta[i]
                FakeFactors_lightlepton_fake.append(self.FakeFactor[region].EvaluateWeight("Electron", pt, abs(eta)))
                # ~ if not self.IsReal(event, "LightLepton", pt, eta):
                # ~ FakeFactors_lightlepton_fake.append(self.FakeFactor[region].EvaluateWeight("Electron", pt, abs(eta)))
                # ~ else:
                # ~ FakeFactors_lightlepton_real.append(self.FakeFactor[region].EvaluateWeight("Electron", pt, abs(eta)))

            for i in range(len(event.LooseMuon_pt)):
                pt = event.LooseMuon_pt[i]
                eta = event.LooseMuon_eta[i]
                FakeFactors_lightlepton_fake.append(self.FakeFactor[region].EvaluateWeight("Muon", pt, abs(eta)))
                # ~ if not self.IsReal(event, "LightLepton", pt, eta):
                # ~ FakeFactors_lightlepton_fake.append(self.FakeFactor[region].EvaluateWeight("Muon", pt, abs(eta)))
                # ~ else:
                # ~ FakeFactors_lightlepton_real.append(self.FakeFactor[region].EvaluateWeight("Muon", pt, abs(eta)))
        FakeFactors_lightlepton = FakeFactors_lightlepton_fake + FakeFactors_lightlepton_real
        LightLepDenominator = math.factorial(len(FakeFactors_lightlepton)) / (
            math.factorial(lightleptonnumber) * math.factorial(len(FakeFactors_lightlepton) - lightleptonnumber))
        LightLepCombinations = itertools.combinations(FakeFactors_lightlepton, lightleptonnumber)

        if not taunumber == 0:
            for i in range(len(event.Loose1PTau_pt)):
                pt = event.Loose1PTau_pt[i]
                eta = event.Loose1PTau_eta[i]
                FakeFactors_tau_fake.append(self.FakeFactor[region].EvaluateWeight("1P_Tau", pt, abs(eta)))
                # ~ if not self.IsReal(event, "Tau", pt, eta):
                # ~ FakeFactors_tau_fake.append(self.FakeFactor[region].EvaluateWeight("1P_Tau", pt, abs(eta)))
                # ~ else:
                # ~ FakeFactors_tau_real.append(self.FakeFactor[region].EvaluateWeight("1P_Tau", pt, abs(eta)))

            for i in range(len(event.Loose3PTau_pt)):
                pt = event.Loose3PTau_pt[i]
                eta = event.Loose3PTau_eta[i]
                FakeFactors_tau_fake.append(self.FakeFactor[region].EvaluateWeight("3P_Tau", pt, abs(eta)))
                # ~ if not self.IsReal(event, "Tau", pt, eta):
                # ~ FakeFactors_tau_fake.append(self.FakeFactor[region].EvaluateWeight("3P_Tau", pt, abs(eta)))
                # ~ else:
                # ~ FakeFactors_tau_real.append(self.FakeFactor[region].EvaluateWeight("3P_Tau", pt, abs(eta)))

        FakeFactors_tau = FakeFactors_tau_fake + FakeFactors_tau_real
        TauDenominator = math.factorial(
            len(FakeFactors_tau)) / (math.factorial(taunumber) * math.factorial(len(FakeFactors_tau) - taunumber))
        TauCombinations = itertools.combinations(FakeFactors_tau, taunumber)

        FakeFactorWeight_lightlep = 0
        FakeFactorWeight_tau = 0

        if lightleptonnumber == 1:
            for FF in LightLepCombinations:
                FakeFactorWeight_lightlep += FF[0]
                # ~ if FF[0] in FakeFactors_lightlepton_real:
                # ~ FakeFactorWeight_lightlep -= FF[0]
                # ~ else:
                # ~ FakeFactorWeight_lightlep += FF[0]
        elif lightleptonnumber == 2:
            for FF in LightLepCombinations:
                FakeFactorWeight_lightlep += FF[0] * FF[1]
                # ~ if FF[0] in FakeFactors_lightlepton_real and FF[1] in FakeFactors_lightlepton_real:
                # ~ FakeFactorWeight_lightlep -= FF[0]*FF[1]
                # ~ else:
                # ~ FakeFactorWeight_lightlep += FF[0]*FF[1]

        FakeFactorWeight_lightlep = FakeFactorWeight_lightlep / LightLepDenominator

        if taunumber == 1:
            for FF in TauCombinations:
                FakeFactorWeight_tau += FF[0]
                # ~ if FF[0] in FakeFactors_tau_real:
                # ~ FakeFactorWeight_tau -= FF[0]
                # ~ else:
                # ~ FakeFactorWeight_tau += FF[0]
        elif taunumber == 2:
            for FF in TauCombinations:
                FakeFactorWeight_tau += FF[0] * FF[1]
                # ~ if FF[0] in FakeFactors_tau_real and FF[1] in FakeFactors_tau_real:
                # ~ FakeFactorWeight_tau -= FF[0]*FF[1]
                # ~ else:
                # ~ FakeFactorWeight_tau += FF[0]*FF[1]

        FakeFactorWeight_tau = FakeFactorWeight_tau / TauDenominator

        if not lightleptonnumber == 0 and not taunumber == 0:
            FakeFactorWeight = FakeFactorWeight_lightlep * FakeFactorWeight_tau
            # ~ if FakeFactorWeight_lightlep > 0 and FakeFactorWeight_tau > 0:
            # ~ FakeFactorWeight = FakeFactorWeight_lightlep*FakeFactorWeight_tau
            # ~ else:
            # ~ FakeFactorWeight = -FakeFactorWeight_lightlep*FakeFactorWeight_tau
        elif not lightleptonnumber == 0:
            FakeFactorWeight = FakeFactorWeight_lightlep
        elif not taunumber == 0:
            FakeFactorWeight = FakeFactorWeight_tau
        else:
            print "Error: number of tau and lightlepton is 0"
            return 0
        if IrreducibleBkg == True:
            FakeFactorWeight = -FakeFactorWeight
        if isCR2 == True:
            FakeFactorWeight = -FakeFactorWeight
        return FakeFactorWeight


class FakeFactor(object):
    def __init__(self, CRregion, options):

        self.FakeTypes = options.faketypes  # Faketypes to be considered
        self.Processes = options.processes  # processes to be considered
        self.useProcessfraction = True
        # name of the fakefactorhistos

        self.FakeFactorElectron_ttbar = {
            "LF": "LF_Nominal/CRt#bar{t}/CRt#bar{t}_Electrons_origLF_pteta",
            "HF": "HF_Nominal/CRt#bar{t}/CRt#bar{t}_Electrons_origHF_pteta",
            "Conv": "Conv_Nominal/CRt#bar{t}/CRt#bar{t}_Electrons_origConv_pteta"
        }
        self.FakeFactorElectron_Zjets = {
            "LF": "LF_Nominal/Z#mu#mu/Z#mu#mu_Electrons_origLF_pteta",
            "HF": "HF_Nominal/Z#mu#mu/Z#mu#mu_Electrons_origHF_pteta",
            "Conv": "Conv_Nominal/Z#mu#mu/Z#mu#mu_Electrons_origConv_pteta"
        }

        self.FakeFactorMuon_ttbar = {
            "LF": "LF_Nominal/CRt#bar{t}/CRt#bar{t}_Muons_origLF_pteta",
            "HF": "HF_Nominal/CRt#bar{t}/CRt#bar{t}_Muons_origHF_pteta"
        }
        self.FakeFactorMuon_Zjets = {
            "LF": "LF_Nominal/Z#mu#mu/Z#mu#mu_Muons_origLF_pteta",
            "HF": "HF_Nominal/Z#mu#mu/Z#mu#mu_Muons_origHF_pteta"
        }

        self.FakeFactor1PTau = {
            "LF": "LF_Nominal/FakeFactor/FakeFactor_Tau_origLF_prong1P_pteta",
            "HF": "HF_Nominal/FakeFactor/FakeFactor_Tau_origHF_prong1P_pteta",
            "Gluon": "Gluon_Nominal/FakeFactor/FakeFactor_Tau_origGluon_prong1P_pteta",
            "Elec": "Elec_Nominal/FakeFactor/FakeFactor_Tau_origElec_prong1P_pteta",
            "Conv": "Conv_Nominal/FakeFactor/FakeFactor_Tau_origConv_prong1P_pteta"
        }

        self.FakeFactor3PTau = {
            "LF": "LF_Nominal/FakeFactor/FakeFactor_Tau_origLF_prong3P_pteta",
            "HF": "HF_Nominal/FakeFactor/FakeFactor_Tau_origHF_prong3P_pteta",
            "Gluon": "Gluon_Nominal/FakeFactor/FakeFactor_Tau_origGluon_prong3P_pteta",
            "Elec": "Elec_Nominal/FakeFactor/FakeFactor_Tau_origElec_prong3P_pteta",
            "Conv": "Conv_Nominal/FakeFactor/FakeFactor_Tau_origConv_prong3P_pteta"
        }

        self.ProcFracElectron = {
            "ttbar": {
                "LF": "%s/ttbar/LF/ProcessFraction_LooseElectrons_origLF_pteta" % (CRregion),
                "HF": "%s/ttbar/HF/ProcessFraction_LooseElectrons_origHF_pteta" % (CRregion),
                "Conv": "%s/ttbar/Conv/ProcessFraction_LooseElectrons_origConv_pteta" % (CRregion)
            },
            "Zjets": {
                "LF": "%s/Zjets/LF/ProcessFraction_LooseElectrons_origLF_pteta" % (CRregion),
                "HF": "%s/Zjets/HF/ProcessFraction_LooseElectrons_origHF_pteta" % (CRregion),
                "Conv": "%s/Zjets/Conv/ProcessFraction_LooseElectrons_origConv_pteta" % (CRregion)
            }
        }

        self.ProcFracMuon = {
            "ttbar": {
                "LF": "%s/ttbar/LF/ProcessFraction_LooseMuons_origLF_pteta" % (CRregion),
                "HF": "%s/ttbar/HF/ProcessFraction_LooseMuons_origHF_pteta" % (CRregion)
            },
            "Zjets": {
                "LF": "%s/Zjets/LF/ProcessFraction_LooseMuons_origLF_pteta" % (CRregion),
                "HF": "%s/Zjets/HF/ProcessFraction_LooseMuons_origHF_pteta" % (CRregion)
            }
        }

        self.ProcFrac1PTau = {
            "ttbar": {
                "LF": "%s/ttbar/LF/ProcessFraction_LooseTau_origLF_prong1P_pteta" % (CRregion),
                "HF": "%s/ttbar/HF/ProcessFraction_LooseTau_origHF_prong1P_pteta" % (CRregion),
                "Gluon": "%s/ttbar/Gluon/ProcessFraction_LooseTau_origGluon_prong1P_pteta" % (CRregion),
                "Elec": "%s/ttbar/Elec/ProcessFraction_LooseTau_origElec_prong1P_pteta" % (CRregion),
                "Conv": "%s/ttbar/Conv/ProcessFraction_LooseTau_origConv_prong1P_pteta" % (CRregion)
            },
            "Zjets": {
                "LF": "%s/Zjets/LF/ProcessFraction_LooseTau_origLF_prong1P_pteta" % (CRregion),
                "HF": "%s/Zjets/HF/ProcessFraction_LooseTau_origHF_prong1P_pteta" % (CRregion),
                "Gluon": "%s/Zjets/Gluon/ProcessFraction_LooseTau_origGluon_prong1P_pteta" % (CRregion),
                "Elec": "%s/Zjets/Elec/ProcessFraction_LooseTau_origElec_prong1P_pteta" % (CRregion),
                "Conv": "%s/Zjets/Conv/ProcessFraction_LooseTau_origConv_prong1P_pteta" % (CRregion)
            }
        }

        self.ProcFrac3PTau = {
            "ttbar": {
                "LF": "%s/ttbar/LF/ProcessFraction_LooseTau_origLF_prong3P_pteta" % (CRregion),
                "HF": "%s/ttbar/HF/ProcessFraction_LooseTau_origHF_prong3P_pteta" % (CRregion),
                "Gluon": "%s/ttbar/Gluon/ProcessFraction_LooseTau_origGluon_prong3P_pteta" % (CRregion),
                "Elec": "%s/ttbar/Elec/ProcessFraction_LooseTau_origElec_prong3P_pteta" % (CRregion),
                "Conv": "%s/ttbar/Conv/ProcessFraction_LooseTau_origConv_prong3P_pteta" % (CRregion)
            },
            "Zjets": {
                "LF": "%s/Zjets/LF/ProcessFraction_LooseTau_origLF_prong3P_pteta" % (CRregion),
                "HF": "%s/Zjets/HF/ProcessFraction_LooseTau_origHF_prong3P_pteta" % (CRregion),
                "Gluon": "%s/Zjets/Gluon/ProcessFraction_LooseTau_origGluon_prong3P_pteta" % (CRregion),
                "Elec": "%s/Zjets/Elec/ProcessFraction_LooseTau_origElec_prong3P_pteta" % (CRregion),
                "Conv": "%s/Zjets/Conv/ProcessFraction_LooseTau_origConv_prong3P_pteta" % (CRregion)
            }
        }

        self.FakeFactorFile_lightlep_ttbar = ROOT.TFile.Open("/ptmp/mpp/maren/Fakefactor/FakeFactors_lightlepton_ttbar.root")
        self.FakeFactorFile_lightlep_Zjets = ROOT.TFile.Open("/ptmp/mpp/maren/Fakefactor/FakeFactors_lightlepton_Zjets.root")

        self.FakeFactorFile_tau_ttbar = ROOT.TFile.Open("/ptmp/mpp/maren/Fakefactor/FakeFactors_tau_ttbar.root")
        self.FakeFactorFile_tau_Zjets = ROOT.TFile.Open("/ptmp/mpp/maren/Fakefactor/FakeFactors_tau_Zjets.root")

        if "ttbar" in self.Processes and not "Zjets" in self.Processes:
            self.ProcessfractionFile_tau = ROOT.TFile.Open("/ptmp/mpp/maren/Fakefactor/ttbar/ProcessFraction_tau_ttbar.root")
            self.ProcessfractionFile_lightlep = ROOT.TFile.Open("/ptmp/mpp/maren/Fakefactor/ttbar/ProcessFraction_lightlepton_ttbar.root")
        elif "Zjets" in self.Processes and not "ttbar" in self.Processes:
            self.ProcessfractionFile_tau = ROOT.TFile.Open("/ptmp/mpp/maren/Fakefactor/Zjets/ProcessFraction_tau_Zjets.root")
            self.ProcessfractionFile_lightlep = ROOT.TFile.Open("/ptmp/mpp/maren/Fakefactor/Zjets/ProcessFraction_lightlepton_Zjets.root")

        else:
            self.ProcessfractionFile_tau = ROOT.TFile.Open("/ptmp/mpp/maren/Fakefactor/ProcessFraction_tau.root")
            self.ProcessfractionFile_lightlep = ROOT.TFile.Open("/ptmp/mpp/maren/Fakefactor/ProcessFraction_lightlepton.root")
        self.FakeFactorHistos_Electron = {}
        self.ProcessFractionHistos_Electron = {}
        self.FakeFactorHistos_Muon = {}
        self.ProcessFractionHistos_Muon = {}
        self.FakeFactorHistos_1prong = {}
        self.ProcessFractionHistos_1prong = {}
        self.FakeFactorHistos_3prong = {}
        self.ProcessFractionHistos_3prong = {}

        # loading the fakefactorhistos
        for process in self.Processes:
            self.FakeFactorHistos_Electron[process] = {}
            self.ProcessFractionHistos_Electron[process] = {}
            self.FakeFactorHistos_Muon[process] = {}
            self.ProcessFractionHistos_Muon[process] = {}
            self.FakeFactorHistos_1prong[process] = {}
            self.ProcessFractionHistos_1prong[process] = {}
            self.FakeFactorHistos_3prong[process] = {}
            self.ProcessFractionHistos_3prong[process] = {}

        if "ttbar" in self.Processes:
            for faketype, histoname in self.FakeFactorElectron_ttbar.items():
                if faketype in self.FakeTypes:
                    Histo = self.FakeFactorFile_lightlep_ttbar.Get(histoname)
                    self.FakeFactorHistos_Electron["ttbar"][faketype] = Histo
        if "Zjets" in self.Processes:
            for faketype, histoname in self.FakeFactorElectron_Zjets.items():
                if faketype in self.FakeTypes:
                    Histo = self.FakeFactorFile_lightlep_Zjets.Get(histoname)
                    self.FakeFactorHistos_Electron["Zjets"][faketype] = Histo

        if "ttbar" in self.Processes:
            for faketype, histoname in self.FakeFactorMuon_ttbar.items():
                if faketype in self.FakeTypes:
                    Histo = self.FakeFactorFile_lightlep_ttbar.Get(histoname)
                    self.FakeFactorHistos_Muon["ttbar"][faketype] = Histo
        if "Zjets" in self.Processes:
            for faketype, histoname in self.FakeFactorMuon_Zjets.items():
                if faketype in self.FakeTypes:
                    Histo = self.FakeFactorFile_lightlep_Zjets.Get(histoname)
                    self.FakeFactorHistos_Muon["Zjets"][faketype] = Histo

        for faketype, histoname in self.FakeFactor1PTau.items():
            if faketype in self.FakeTypes:
                if "ttbar" in self.Processes:
                    Histo = self.FakeFactorFile_tau_ttbar.Get(histoname)
                    self.FakeFactorHistos_1prong["ttbar"][faketype] = Histo
                if "Zjets" in self.Processes:
                    Histo = self.FakeFactorFile_tau_Zjets.Get(histoname)
                    self.FakeFactorHistos_1prong["Zjets"][faketype] = Histo

        for faketype, histoname in self.FakeFactor3PTau.items():
            if faketype in self.FakeTypes:
                if "ttbar" in self.Processes:
                    Histo = self.FakeFactorFile_tau_ttbar.Get(histoname)
                    self.FakeFactorHistos_3prong["ttbar"][faketype] = Histo
                if "Zjets" in self.Processes:
                    Histo = self.FakeFactorFile_tau_Zjets.Get(histoname)
                    self.FakeFactorHistos_3prong["Zjets"][faketype] = Histo

        for process in self.ProcFracElectron:
            for faketype, histoname in self.ProcFracElectron[process].items():
                if faketype in self.FakeTypes and process in self.Processes:
                    Histo = self.ProcessfractionFile_lightlep.Get(histoname)
                    self.ProcessFractionHistos_Electron[process][faketype] = Histo

        for process in self.ProcFracMuon:
            for faketype, histoname in self.ProcFracMuon[process].items():
                if faketype in self.FakeTypes and process in self.Processes:
                    Histo = self.ProcessfractionFile_lightlep.Get(histoname)
                    self.ProcessFractionHistos_Muon[process][faketype] = Histo

        for process in self.ProcFrac1PTau:
            for faketype, histoname in self.ProcFrac1PTau[process].items():
                if faketype in self.FakeTypes and process in self.Processes:
                    Histo = self.ProcessfractionFile_tau.Get(histoname)
                    self.ProcessFractionHistos_1prong[process][faketype] = Histo

        for process in self.ProcFrac3PTau:
            for faketype, histoname in self.ProcFrac3PTau[process].items():
                if faketype in self.FakeTypes and process in self.Processes:
                    Histo = self.ProcessfractionFile_tau.Get(histoname)
                    self.ProcessFractionHistos_3prong[process][faketype] = Histo

    def EvaluateWeight(self, particle, pt, eta):

        if particle == "Electron":
            FakeFactorHistos = self.FakeFactorHistos_Electron
            ProcFracHistos = self.ProcessFractionHistos_Electron
        elif particle == "Muon":
            FakeFactorHistos = self.FakeFactorHistos_Muon
            ProcFracHistos = self.ProcessFractionHistos_Muon
        elif particle == "1P_Tau":
            FakeFactorHistos = self.FakeFactorHistos_1prong
            ProcFracHistos = self.ProcessFractionHistos_1prong
        elif particle == "3P_Tau":
            FakeFactorHistos = self.FakeFactorHistos_3prong
            ProcFracHistos = self.ProcessFractionHistos_3prong
        else:
            print "unknown particle"
            return 0

        AverageFakeFactor = 0
        for process in FakeFactorHistos:

            for faketype in FakeFactorHistos[process]:

                FakeFactorHisto = FakeFactorHistos[process][faketype]

                FakeFactorBin = FakeFactorHisto.FindBin(pt, eta)
                FakeFactor = FakeFactorHisto.GetBinContent(FakeFactorBin)

                ProcFracHisto = ProcFracHistos[process][faketype]
                ProcFracBin = ProcFracHisto.FindBin(pt, eta)
                ProcFrac = ProcFracHisto.GetBinContent(ProcFracBin)

                if self.useProcessfraction:
                    AverageFakeFactor += FakeFactor * ProcFrac
                else:
                    AverageFakeFactor += FakeFactor

        return AverageFakeFactor


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description=
        'This script calculates the fakefactorweight from histfittertrees and creates closurehistos \"python CalculateFakeFactors.py -h\"',
        prog='ApplyFakeFactors',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--inputFile", help="the input histfittertrees", default="output_trees.root")
    parser.add_argument("--treeName", help="the name of the tree", default="DebugTree_Nominal")
    parser.add_argument("--outDir", help="Specify an output folder where the closureplots get stored", default='ClosurePlots/')
    parser.add_argument("--faketypes",
                        help="specify the faketypes that should be used",
                        nargs='+',
                        default=["LF", "HF", "Gluon", "Elec", "Conv"])
    parser.add_argument("--processes", help="specify the processes used for the fakefactors", nargs='+', default=["ttbar"])

    options = parser.parse_args()
    print options.processes
    CreateDirectory(options.outDir, False)
    File = ROOT.TFile.Open(options.inputFile)

    Tree = File.Get(options.treeName)

    newFileName = options.inputFile.replace(".root", "")
    # ~ newFileName += "_withFakeFactor.root"
    # ~ newFile = ROOT.TFile(newFileName, "RECREATE")
    # ~ newTree = Tree.CloneTree()

    FakeFactorarray = array('d', [0])
    # ~ branch = newTree.Branch("FakefactorWeight", FakeFactorarray, 'FakefactorWeight/D')
    FakeFactors = FakeFactorWeight(options)

    SRregions = ["4L", "4LnoZ", "4LZ", "3L1T", "3L1TnoZ", "3L1TZ", "2L2T", "2L2TnoZ", "2L2TZ", "3L", "3LnoZ", "3LZ"]
    met_weighted = {}
    met_SR = {}
    meff_weighted = {}
    meff_SR = {}
    for region in SRregions:
        met_weighted[region] = ROOT.TH1F("met_weighted_%s" % (region), "", 20, 0, 500)
        met_SR[region] = ROOT.TH1F("met_SR_%s" % (region), "", 20, 0, 500)
        meff_weighted[region] = ROOT.TH1F("meff_weighted_%s" % (region), "", 20, 0, 1000)
        meff_SR[region] = ROOT.TH1F("meff_SR_%s" % (region), "", 20, 0, 1000)

    print "Start Loop over Events"
    for event in Tree:
        Weight = event.NormWeight
        # ~ Weight =  event.EleWeight * event.MuoWeight * event.JetWeight * event.TauWeight
        FakeFactorWeight = 1
        try:
            if event.Is3L == 1:
                met_SR["3L"].Fill(event.Met, Weight)
                meff_SR["3L"].Fill(event.SignalMeff, Weight)
        except:
            pass
        try:
            if event.Is3LnoZ == 1:
                met_SR["3LnoZ"].Fill(event.Met, Weight)
                meff_SR["3LnoZ"].Fill(event.SignalMeff, Weight)
        except:
            pass
        try:
            if event.Is3LZ == 1:
                met_SR["3LZ"].Fill(event.Met, Weight)
                meff_SR["3LZ"].Fill(event.SignalMeff, Weight)
        except:
            pass
        try:
            if event.Is2L1l == 1:
                FakeFactorWeight = FakeFactors.EvaluateFakeFactorWeight(event,
                                                                        "2L2l",
                                                                        lightleptonnumber=1,
                                                                        taunumber=0,
                                                                        RealSignalLightLep=2,
                                                                        RealSignalTau=0)
                met_weighted["3L"].Fill(event.Met, FakeFactorWeight * Weight)
                meff_weighted["3L"].Fill(event.BaseMeff, FakeFactorWeight * Weight)
        except:
            pass
        try:
            if event.Is2L1lnoZ == 1:
                FakeFactorWeight = FakeFactors.EvaluateFakeFactorWeight(event,
                                                                        "2L2l",
                                                                        lightleptonnumber=1,
                                                                        taunumber=0,
                                                                        RealSignalLightLep=2,
                                                                        RealSignalTau=0)
                met_weighted["3LnoZ"].Fill(event.Met, FakeFactorWeight * Weight)
                meff_weighted["3LnoZ"].Fill(event.BaseMeff, FakeFactorWeight * Weight)
        except:
            pass
        try:
            if event.Is2L1lZ == 1:
                FakeFactorWeight = FakeFactors.EvaluateFakeFactorWeight(event,
                                                                        "2L2l",
                                                                        lightleptonnumber=1,
                                                                        taunumber=0,
                                                                        RealSignalLightLep=2,
                                                                        RealSignalTau=0)
                met_weighted["3LZ"].Fill(event.Met, FakeFactorWeight * Weight)
                meff_weighted["3LZ"].Fill(event.BaseMeff, FakeFactorWeight * Weight)
        except:
            pass
        try:
            if event.Is1L2l == 1:
                FakeFactorWeight = FakeFactors.EvaluateFakeFactorWeight(event,
                                                                        "2L2l",
                                                                        lightleptonnumber=2,
                                                                        taunumber=0,
                                                                        RealSignalLightLep=1,
                                                                        RealSignalTau=0,
                                                                        isCR2=True)
                met_weighted["3L"].Fill(event.Met, FakeFactorWeight * Weight)
                meff_weighted["3L"].Fill(event.BaseMeff, FakeFactorWeight * Weight)
        except:
            pass
        try:
            if event.Is1L2lnoZ == 1:
                FakeFactorWeight = FakeFactors.EvaluateFakeFactorWeight(event,
                                                                        "2L2l",
                                                                        lightleptonnumber=2,
                                                                        taunumber=0,
                                                                        RealSignalLightLep=1,
                                                                        RealSignalTau=0,
                                                                        isCR2=True)
                met_weighted["3LnoZ"].Fill(event.Met, FakeFactorWeight * Weight)
                meff_weighted["3LnoZ"].Fill(event.BaseMeff, FakeFactorWeight * Weight)
        except:
            pass
        try:
            if event.Is1L2lZ == 1:
                FakeFactorWeight = FakeFactors.EvaluateFakeFactorWeight(event,
                                                                        "2L2l",
                                                                        lightleptonnumber=2,
                                                                        taunumber=0,
                                                                        RealSignalLightLep=1,
                                                                        RealSignalTau=0,
                                                                        isCR2=True)
                met_weighted["3LZ"].Fill(event.Met, FakeFactorWeight * Weight)
                meff_weighted["3LZ"].Fill(event.BaseMeff, FakeFactorWeight * Weight)
        except:
            pass
        # 4L CR1

        try:
            if event.Is3L1l == 1:

                FakeFactorWeight = FakeFactors.EvaluateFakeFactorWeight(event,
                                                                        "2L2l",
                                                                        lightleptonnumber=1,
                                                                        taunumber=0,
                                                                        RealSignalLightLep=3,
                                                                        RealSignalTau=0)
                met_weighted["4L"].Fill(event.Met, FakeFactorWeight * Weight)
                meff_weighted["4L"].Fill(event.BaseMeff, FakeFactorWeight * Weight)
        except:
            pass
        try:
            if event.Is3L1lnoZ == 1:

                FakeFactorWeight = FakeFactors.EvaluateFakeFactorWeight(event,
                                                                        "2L2lnoZ",
                                                                        lightleptonnumber=1,
                                                                        taunumber=0,
                                                                        RealSignalLightLep=3,
                                                                        RealSignalTau=0)
                met_weighted["4LnoZ"].Fill(event.Met, FakeFactorWeight * Weight)
                meff_weighted["4LnoZ"].Fill(event.BaseMeff, FakeFactorWeight * Weight)
        except:
            pass
        try:
            if event.Is3L1lZ == 1:

                FakeFactorWeight = FakeFactors.EvaluateFakeFactorWeight(event,
                                                                        "2L2lZ",
                                                                        lightleptonnumber=1,
                                                                        taunumber=0,
                                                                        RealSignalLightLep=3,
                                                                        RealSignalTau=0)
                met_weighted["4LZ"].Fill(event.Met, FakeFactorWeight * Weight)
                meff_weighted["4LZ"].Fill(event.BaseMeff, FakeFactorWeight * Weight)
        except:
            pass
        # 4L CR2
        try:
            if event.Is2L2l == 1:

                FakeFactorWeight = FakeFactors.EvaluateFakeFactorWeight(event,
                                                                        "2L2l",
                                                                        lightleptonnumber=2,
                                                                        taunumber=0,
                                                                        RealSignalLightLep=2,
                                                                        RealSignalTau=0,
                                                                        isCR2=True)
                met_weighted["4L"].Fill(event.Met, FakeFactorWeight * Weight)
                meff_weighted["4L"].Fill(event.BaseMeff, FakeFactorWeight * Weight)
        except:
            pass
        try:
            if event.Is2L2lnoZ == 1:

                FakeFactorWeight = FakeFactors.EvaluateFakeFactorWeight(event,
                                                                        "2L2lnoZ",
                                                                        lightleptonnumber=2,
                                                                        taunumber=0,
                                                                        RealSignalLightLep=2,
                                                                        RealSignalTau=0,
                                                                        isCR2=True)
                met_weighted["4LnoZ"].Fill(event.Met, FakeFactorWeight * Weight)
                meff_weighted["4LnoZ"].Fill(event.BaseMeff, FakeFactorWeight * Weight)
        except:
            pass
        try:
            if event.Is2L2lZ == 1:

                FakeFactorWeight = FakeFactors.EvaluateFakeFactorWeight(event,
                                                                        "2L2lZ",
                                                                        lightleptonnumber=2,
                                                                        taunumber=0,
                                                                        RealSignalLightLep=2,
                                                                        RealSignalTau=0,
                                                                        isCR2=True)
                met_weighted["4LZ"].Fill(event.Met, FakeFactorWeight * Weight)
                meff_weighted["4LZ"].Fill(event.BaseMeff, FakeFactorWeight * Weight)
        except:
            pass
        # 3L1T CR1
        try:
            if event.Is2L1T1l == 1:

                FakeFactorWeight = FakeFactors.EvaluateFakeFactorWeight(event,
                                                                        "2L1l1t",
                                                                        lightleptonnumber=1,
                                                                        taunumber=0,
                                                                        RealSignalLightLep=2,
                                                                        RealSignalTau=1)
                met_weighted["3L1T"].Fill(event.Met, FakeFactorWeight * Weight)
                meff_weighted["3L1T"].Fill(event.BaseMeff, FakeFactorWeight * Weight)
        except:
            pass
        try:
            if event.Is2L1T1lnoZ == 1:

                FakeFactorWeight = FakeFactors.EvaluateFakeFactorWeight(event,
                                                                        "2L1l1tnoZ",
                                                                        lightleptonnumber=1,
                                                                        taunumber=0,
                                                                        RealSignalLightLep=2,
                                                                        RealSignalTau=1)
                met_weighted["3L1TnoZ"].Fill(event.Met, FakeFactorWeight * Weight)
                meff_weighted["3L1TnoZ"].Fill(event.BaseMeff, FakeFactorWeight * Weight)
        except:
            pass
        try:
            if event.Is2L1T1lZ == 1:

                FakeFactorWeight = FakeFactors.EvaluateFakeFactorWeight(event,
                                                                        "2L1l1tZ",
                                                                        lightleptonnumber=1,
                                                                        taunumber=0,
                                                                        RealSignalLightLep=2,
                                                                        RealSignalTau=1)
                met_weighted["3L1TZ"].Fill(event.Met, FakeFactorWeight * Weight)
                meff_weighted["3L1TZ"].Fill(event.BaseMeff, FakeFactorWeight * Weight)
        except:
            pass

        try:
            if event.Is3L1t == 1:

                FakeFactorWeight = FakeFactors.EvaluateFakeFactorWeight(event,
                                                                        "2L1l1t",
                                                                        lightleptonnumber=0,
                                                                        taunumber=1,
                                                                        RealSignalLightLep=3,
                                                                        RealSignalTau=0)
                met_weighted["3L1T"].Fill(event.Met, FakeFactorWeight * Weight)
                meff_weighted["3L1T"].Fill(event.BaseMeff, FakeFactorWeight * Weight)
        except:
            pass
        try:
            if event.Is3L1tnoZ == 1:

                FakeFactorWeight = FakeFactors.EvaluateFakeFactorWeight(event,
                                                                        "2L1l1tnoZ",
                                                                        lightleptonnumber=0,
                                                                        taunumber=1,
                                                                        RealSignalLightLep=3,
                                                                        RealSignalTau=0)
                met_weighted["3L1TnoZ"].Fill(event.Met, FakeFactorWeight * Weight)
                meff_weighted["3L1TnoZ"].Fill(event.BaseMeff, FakeFactorWeight * Weight)
        except:
            pass
        try:
            if event.Is3L1tZ == 1:

                FakeFactorWeight = FakeFactors.EvaluateFakeFactorWeight(event,
                                                                        "2L1l1tZ",
                                                                        lightleptonnumber=0,
                                                                        taunumber=1,
                                                                        RealSignalLightLep=3,
                                                                        RealSignalTau=0)
                met_weighted["3L1TZ"].Fill(event.Met, FakeFactorWeight * Weight)
                meff_weighted["3L1TZ"].Fill(event.BaseMeff, FakeFactorWeight * Weight)
        except:
            pass
        # 3L1T CR2
        try:
            if event.Is2L1l1t == 1:
                FakeFactorWeight = FakeFactors.EvaluateFakeFactorWeight(event,
                                                                        "2L1l1t",
                                                                        lightleptonnumber=1,
                                                                        taunumber=1,
                                                                        RealSignalLightLep=2,
                                                                        RealSignalTau=0,
                                                                        isCR2=True)
                met_weighted["3L1T"].Fill(event.Met, FakeFactorWeight * Weight)
                meff_weighted["3L1T"].Fill(event.BaseMeff, FakeFactorWeight * Weight)
        except:
            pass
        try:
            if event.Is2L1l1tnoZ == 1:
                FakeFactorWeight = FakeFactors.EvaluateFakeFactorWeight(event,
                                                                        "2L1l1tnoZ",
                                                                        lightleptonnumber=1,
                                                                        taunumber=1,
                                                                        RealSignalLightLep=2,
                                                                        RealSignalTau=0,
                                                                        isCR2=True)
                met_weighted["3L1TnoZ"].Fill(event.Met, FakeFactorWeight * Weight)
                meff_weighted["3L1TnoZ"].Fill(event.BaseMeff, FakeFactorWeight * Weight)
        except:
            pass
        try:
            if event.Is2L1l1tZ == 1:
                FakeFactorWeight = FakeFactors.EvaluateFakeFactorWeight(event,
                                                                        "2L1l1tZ",
                                                                        lightleptonnumber=1,
                                                                        taunumber=1,
                                                                        RealSignalLightLep=2,
                                                                        RealSignalTau=0,
                                                                        isCR2=True)
                met_weighted["3L1TZ"].Fill(event.Met, FakeFactorWeight * Weight)
                meff_weighted["3L1TZ"].Fill(event.BaseMeff, FakeFactorWeight * Weight)
        except:
            pass

        # 2L2T CR1
        try:
            if event.Is2L1T1t == 1:

                FakeFactorWeight = FakeFactors.EvaluateFakeFactorWeight(event,
                                                                        "2L2t",
                                                                        lightleptonnumber=0,
                                                                        taunumber=1,
                                                                        RealSignalLightLep=2,
                                                                        RealSignalTau=1)
                met_weighted["2L2T"].Fill(event.Met, FakeFactorWeight * Weight)
                meff_weighted["2L2T"].Fill(event.BaseMeff, FakeFactorWeight * Weight)
        except:
            pass
        try:
            if event.Is2L1T1tnoZ == 1:

                FakeFactorWeight = FakeFactors.EvaluateFakeFactorWeight(event,
                                                                        "2L2tnoZ",
                                                                        lightleptonnumber=0,
                                                                        taunumber=1,
                                                                        RealSignalLightLep=2,
                                                                        RealSignalTau=1)
                met_weighted["2L2TnoZ"].Fill(event.Met, FakeFactorWeight * Weight)
                meff_weighted["2L2TnoZ"].Fill(event.BaseMeff, FakeFactorWeight * Weight)
        except:
            pass
        try:
            if event.Is2L1T1tZ == 1:

                FakeFactorWeight = FakeFactors.EvaluateFakeFactorWeight(event,
                                                                        "2L2tZ",
                                                                        lightleptonnumber=0,
                                                                        taunumber=1,
                                                                        RealSignalLightLep=2,
                                                                        RealSignalTau=1)
                met_weighted["2L2TZ"].Fill(event.Met, FakeFactorWeight * Weight)
                meff_weighted["2L2TZ"].Fill(event.BaseMeff, FakeFactorWeight * Weight)
        except:
            pass
        # 2L2T CR2
        try:
            if event.Is2L2t == 1:

                FakeFactorWeight = FakeFactors.EvaluateFakeFactorWeight(event,
                                                                        "2L2t",
                                                                        lightleptonnumber=0,
                                                                        taunumber=2,
                                                                        RealSignalLightLep=2,
                                                                        RealSignalTau=0,
                                                                        isCR2=True)
                met_weighted["2L2T"].Fill(event.Met, FakeFactorWeight * Weight)
                meff_weighted["2L2T"].Fill(event.BaseMeff, FakeFactorWeight * Weight)
        except:
            pass
        try:
            if event.Is2L2tnoZ == 1:

                FakeFactorWeight = FakeFactors.EvaluateFakeFactorWeight(event,
                                                                        "2L2tnoZ",
                                                                        lightleptonnumber=0,
                                                                        taunumber=2,
                                                                        RealSignalLightLep=2,
                                                                        RealSignalTau=0,
                                                                        isCR2=True)
                met_weighted["2L2TnoZ"].Fill(event.Met, FakeFactorWeight * Weight)
                meff_weighted["2L2TnoZ"].Fill(event.BaseMeff, FakeFactorWeight * Weight)
        except:
            pass
        try:
            if event.Is2L2tZ == 1:

                FakeFactorWeight = FakeFactors.EvaluateFakeFactorWeight(event,
                                                                        "2L2tZ",
                                                                        lightleptonnumber=0,
                                                                        taunumber=2,
                                                                        RealSignalLightLep=2,
                                                                        RealSignalTau=0,
                                                                        isCR2=True)
                met_weighted["2L2TZ"].Fill(event.Met, FakeFactorWeight * Weight)
                meff_weighted["2L2TZ"].Fill(event.BaseMeff, FakeFactorWeight * Weight)
        except:
            pass
        # SR
        try:
            if event.Is4L == 1:

                met_SR["4L"].Fill(event.Met, Weight)
                meff_SR["4L"].Fill(event.SignalMeff, Weight)
        except:
            pass
        try:
            if event.Is4LnoZ == 1:

                met_SR["4LnoZ"].Fill(event.Met, Weight)
                meff_SR["4LnoZ"].Fill(event.SignalMeff, Weight)
        except:
            pass
        try:
            if event.Is4LZ == 1:

                met_SR["4LZ"].Fill(event.Met, Weight)
                meff_SR["4LZ"].Fill(event.SignalMeff, Weight)
        except:
            pass
        try:
            if event.Is3L1T == 1:
                met_SR["3L1T"].Fill(event.Met, Weight)
                meff_SR["3L1T"].Fill(event.SignalMeff, Weight)
        except:
            pass
        try:
            if event.Is3L1TnoZ == 1:
                met_SR["3L1TnoZ"].Fill(event.Met, Weight)
                meff_SR["3L1TnoZ"].Fill(event.SignalMeff, Weight)
        except:
            pass
        try:
            if event.Is3L1TZ == 1:
                met_SR["3L1TZ"].Fill(event.Met, Weight)
                meff_SR["3L1TZ"].Fill(event.SignalMeff, Weight)
        except:
            pass
        try:
            if event.Is2L2T == 1:

                met_SR["2L2T"].Fill(event.Met, Weight)
                meff_SR["2L2T"].Fill(event.SignalMeff, Weight)
        except:
            pass
        try:
            if event.Is2L2TnoZ == 1:

                met_SR["2L2TnoZ"].Fill(event.Met, Weight)
                meff_SR["2L2TnoZ"].Fill(event.SignalMeff, Weight)
        except:
            pass
        try:
            if event.Is2L2TZ == 1:

                met_SR["2L2TZ"].Fill(event.Met, Weight)
                meff_SR["2L2TZ"].Fill(event.SignalMeff, Weight)
        except:
            pass
        # ~ FakeFactorarray[0] = FakeFactorWeight
        # ~ newTree.Fill()
    # ~ newTree.Write()
    # ~ newFile.Close()
    print "Loop over Events done"
    print "Save Histos"

    ROOT.gStyle.SetOptStat(0)
    for region in met_weighted:
        canvas = ROOT.TCanvas("canvasname_%s" % (region), "", 1200, 800)
        Pad1 = ROOT.TPad("p1_%s" % (region), "", 0.0, 0.37, 1.0, 1.0)
        Pad2 = ROOT.TPad("p2_%s" % (region), "", 0.0, 0.0, 1.0, 0.37)
        Pad1.SetBottomMargin(0.03)  # set to 0 for space between top and bottom pad
        Pad1.SetTopMargin(0.09)
        Pad1.Draw()
        Pad2.SetTopMargin(0.01)  # set to 0 for space between top and bottom pad
        Pad2.SetBottomMargin(0.35)
        Pad2.SetGridy()
        Pad2.Draw()
        Pad1.cd()
        # ~ print "p value: ", met_SR[region].Chi2Test(met_weighted[region], "WW")
        SRmax = met_SR[region].GetMaximum()
        CRmax = met_weighted[region].GetMaximum()
        met_SR[region].SetMaximum(max(SRmax, CRmax) * 1.5)

        met_SR[region].Draw("E")
        met_weighted[region].SetMarkerColor(ROOT.kRed)
        met_weighted[region].SetLineColor(ROOT.kRed)
        met_weighted[region].Draw("E same")
        Legend = ROOT.TLegend(0.06, 0.9, 0.7, 1.0)
        Legend.SetFillStyle(0)
        Legend.SetBorderSize(0)
        Legend.SetTextSize(0.04)
        Legend.AddEntry(met_weighted[region], "CR1-CR2, weighted", "L")
        Legend.AddEntry(met_SR[region], "SR not weighted", "L")
        Legend.Draw()
        Pad2.cd()
        RatioHisto = met_weighted[region].Clone("%s_Ratio" % (region))
        RatioHisto.Divide(met_SR[region])
        RatioHisto.SetMaximum(2)
        RatioHisto.SetMinimum(0)
        RatioHisto.GetXaxis().SetTitle("E_{T}^{miss} [GeV]")
        RatioHisto.Draw("E")
        canvas.Update()
        canvas.Print("%s/Closure_met_%s.pdf" % (options.outDir, region))

    for region in met_weighted:
        canvas = ROOT.TCanvas("canvasname_%s" % (region), "", 1200, 800)
        Pad1 = ROOT.TPad("p1_%s" % (region), "", 0.0, 0.37, 1.0, 1.0)
        Pad2 = ROOT.TPad("p2_%s" % (region), "", 0.0, 0.0, 1.0, 0.37)
        Pad1.SetBottomMargin(0.03)  # set to 0 for space between top and bottom pad
        Pad1.SetTopMargin(0.09)
        Pad1.Draw()
        Pad2.SetTopMargin(0.01)  # set to 0 for space between top and bottom pad
        Pad2.SetBottomMargin(0.35)
        Pad2.SetGridy()
        Pad2.Draw()
        Pad1.cd()
        # ~ print "p value: ", met_SR[region].Chi2Test(met_weighted[region], "WW")
        SRmax = meff_SR[region].GetMaximum()
        CRmax = meff_weighted[region].GetMaximum()
        meff_SR[region].SetMaximum(max(SRmax, CRmax) * 1.5)

        meff_SR[region].Draw("E")
        meff_weighted[region].SetMarkerColor(ROOT.kRed)
        meff_weighted[region].SetLineColor(ROOT.kRed)
        meff_weighted[region].Draw("E same")
        Legend = ROOT.TLegend(0.06, 0.9, 0.7, 1.0)
        Legend.SetFillStyle(0)
        Legend.SetBorderSize(0)
        Legend.SetTextSize(0.04)
        Legend.AddEntry(meff_weighted[region], "CR1-CR2, weighted", "L")
        Legend.AddEntry(meff_SR[region], "SR not weighted", "L")
        Legend.Draw()
        Pad2.cd()
        RatioHisto = meff_weighted[region].Clone("%s_Ratio" % (region))
        RatioHisto.Divide(meff_SR[region])
        RatioHisto.SetMaximum(2)
        RatioHisto.SetMinimum(0)
        RatioHisto.GetXaxis().SetTitle("m_{eff} [GeV]")
        RatioHisto.Draw("E")
        canvas.Update()
        canvas.Print("%s/Closure_meff_%s.pdf" % (options.outDir, region))

    File.Close()
