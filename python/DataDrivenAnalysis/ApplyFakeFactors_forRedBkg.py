import ROOT
import argparse

import collections

import math
import itertools
from array import array
from ClusterSubmission.Utils import CreateDirectory


class FakeFactorWeight(object):
    def __init__(self, options):
        self.FakeFactor = {}
        print "initialising Fakefactor Histos"
        Regions = [
            "3L1l", "3L1lnoZ", "3L1lZ", "3L1lZZ", "2L2l", "2L2lnoZ", "2L2lZ", "2L2lZZ", "2L1l1t", "2L1l1tnoZ", "2L1l1tZ", "2L1T1l",
            "2L1T1lnoZ", "2L1T1lZ", "2L1T1t", "2L1T1tnoZ", "2L1T1tZ", "2L2t", "2L2tnoZ", "2L2tZ", "3L1t", "3L1tnoZ", "3L1tZ", "3L1l-bveto",
            "3L1lnoZ-bveto", "3L1lZ-bveto", "3L1lZZ-bveto", "2L2l-bveto", "2L2lnoZ-bveto", "2L2lZ-bveto", "2L2lZZ-bveto", "2L1l1t-bveto",
            "2L1l1tnoZ-bveto", "2L1l1tZ-bveto", "2L1T1l-bveto", "2L1T1lnoZ-bveto", "2L1T1lZ-bveto", "2L1T1t-bveto", "2L1T1tnoZ-bveto",
            "2L1T1tZ-bveto", "2L2t-bveto", "2L2tnoZ-bveto", "2L2tZ-bveto", "3L1t-bveto", "3L1tnoZ-bveto", "3L1tZ-bveto", "2L2l_CR_ttZ",
            "2L2l_CR_ZZ", "3L1l_CR_ttZ", "3L1l_CR_ZZ"
        ]
        for region in Regions:
            self.FakeFactor[region] = FakeFactor(region, options)
        print "initialising Fakefactor Histos. Done"

    def EvaluateFakeFactorWeight(self, event, region, lightleptonnumber, taunumber, isCR2=False):

        Fakeweights = {
            "Nominal": 1,
            "FF_up": 1,
            "FF_down": 1,
            "PF_up": 1,
            "PF_down": 1,
            "SF_Tau_LF_up": 1,
            "SF_Tau_LF_down": 1,
            "SF_Tau_HF_up": 1,
            "SF_Tau_HF_down": 1,
            "SF_Electron_HF_up": 1,
            "SF_Electron_HF_down": 1,
            "SF_Muon_HF_up": 1,
            "SF_Muon_HF_down": 1
        }

        if (len(event.LooseEle_pt) + len(event.LooseMuo_pt) < lightleptonnumber):
            print "Warning: Number of loose light lepton is lower than the needed number in this Control region. N_elec = {}, N_muon = {}, Region: {}. returning 1".format(
                len(event.LooseEle_pt), len(event.LooseMuo_pt), region)
            return Fakeweights
        if (len(event.LooseTau_pt) < taunumber):
            print "Warning: Number of loose tau is lower than the needed number in this Control region. N_tau = {}, Region: {}. returning 1".format(
                len(event.LooseTau_pt), region)
            return Fakeweights

        if (lightleptonnumber + taunumber) > 2:
            print "ERROR: number of fakeleptons > 2, not implemented, returning 1"
            return Fakeweights

        FakeFactors_lightlepton = []

        FakeFactors_electrons = []  # used to get the uncertainties on SF right
        FakeFactors_muons = []

        FakeFactors_lightlepton_FF_up = []
        FakeFactors_lightlepton_FF_down = []
        FakeFactors_lightlepton_PF_up = []
        FakeFactors_lightlepton_PF_down = []
        FakeFactors_lightlepton_SF_Electron_HF_up = []
        FakeFactors_lightlepton_SF_Electron_HF_down = []
        FakeFactors_lightlepton_SF_Muon_HF_up = []
        FakeFactors_lightlepton_SF_Muon_HF_down = []

        FakeFactors_tau = []
        FakeFactors_tau_FF_up = []
        FakeFactors_tau_FF_down = []
        FakeFactors_tau_PF_up = []
        FakeFactors_tau_PF_down = []
        FakeFactors_tau_SF_HF_up = []
        FakeFactors_tau_SF_HF_down = []
        FakeFactors_tau_SF_LF_up = []
        FakeFactors_tau_SF_LF_down = []

        if not lightleptonnumber == 0:
            for i in range(len(event.LooseEle_pt)):  # just loop over pt, eta has the same number of entries
                pt = event.LooseEle_pt[i]
                eta = event.LooseEle_eta[i]
                FakeFactors_lightlepton.append(self.FakeFactor[region].EvaluateWeight("Electron", pt, abs(eta)))
                FakeFactors_electrons.append(self.FakeFactor[region].EvaluateWeight("Electron", pt, abs(eta)))
                FakeFactors_lightlepton_FF_up.append(self.FakeFactor[region].CalculateVariation("Electron",
                                                                                                pt,
                                                                                                abs(eta),
                                                                                                Fakefactorvariation=True))
                FakeFactors_lightlepton_FF_down.append(self.FakeFactor[region].CalculateVariation("Electron",
                                                                                                  pt,
                                                                                                  abs(eta),
                                                                                                  Fakefactorvariation=True,
                                                                                                  downvariation=True))
                FakeFactors_lightlepton_PF_up.append(self.FakeFactor[region].CalculateVariation("Electron",
                                                                                                pt,
                                                                                                abs(eta),
                                                                                                Processfractionvariation=True))
                FakeFactors_lightlepton_PF_down.append(self.FakeFactor[region].CalculateVariation("Electron",
                                                                                                  pt,
                                                                                                  abs(eta),
                                                                                                  Processfractionvariation=True,
                                                                                                  downvariation=True))
                FakeFactors_lightlepton_SF_Electron_HF_up.append(self.FakeFactor[region].CalculateVariation("Electron",
                                                                                                            pt,
                                                                                                            abs(eta),
                                                                                                            Scalefactorvariation=True,
                                                                                                            VariedFaketype="HF"))
                FakeFactors_lightlepton_SF_Electron_HF_down.append(self.FakeFactor[region].CalculateVariation("Electron",
                                                                                                              pt,
                                                                                                              abs(eta),
                                                                                                              Scalefactorvariation=True,
                                                                                                              downvariation=True,
                                                                                                              VariedFaketype="HF"))

            for i in range(len(event.LooseMuo_pt)):
                pt = event.LooseMuo_pt[i]
                eta = event.LooseMuo_eta[i]
                FakeFactors_lightlepton.append(self.FakeFactor[region].EvaluateWeight("Muon", pt, abs(eta)))
                FakeFactors_muons.append(self.FakeFactor[region].EvaluateWeight("Muon", pt, abs(eta)))
                FakeFactors_lightlepton_FF_up.append(self.FakeFactor[region].CalculateVariation("Muon",
                                                                                                pt,
                                                                                                abs(eta),
                                                                                                Fakefactorvariation=True))
                FakeFactors_lightlepton_FF_down.append(self.FakeFactor[region].CalculateVariation("Muon",
                                                                                                  pt,
                                                                                                  abs(eta),
                                                                                                  Fakefactorvariation=True,
                                                                                                  downvariation=True))
                FakeFactors_lightlepton_PF_up.append(self.FakeFactor[region].CalculateVariation("Muon",
                                                                                                pt,
                                                                                                abs(eta),
                                                                                                Processfractionvariation=True))
                FakeFactors_lightlepton_PF_down.append(self.FakeFactor[region].CalculateVariation("Muon",
                                                                                                  pt,
                                                                                                  abs(eta),
                                                                                                  Processfractionvariation=True,
                                                                                                  downvariation=True))
                FakeFactors_lightlepton_SF_Muon_HF_up.append(self.FakeFactor[region].CalculateVariation("Muon",
                                                                                                        pt,
                                                                                                        abs(eta),
                                                                                                        Scalefactorvariation=True,
                                                                                                        VariedFaketype="HF"))
                FakeFactors_lightlepton_SF_Muon_HF_down.append(self.FakeFactor[region].CalculateVariation("Muon",
                                                                                                          pt,
                                                                                                          abs(eta),
                                                                                                          Scalefactorvariation=True,
                                                                                                          downvariation=True,
                                                                                                          VariedFaketype="HF"))

        FakeFactors_lightlepton_SF_Electron_HF_up += FakeFactors_muons  # adding the muon nominal fakefactors to get the same number of combinations
        FakeFactors_lightlepton_SF_Electron_HF_down += FakeFactors_muons  # since there are no muons in this variation so far

        FakeFactors_lightlepton_SF_Muon_HF_up += FakeFactors_electrons  # adding the electron nominal fakefactors to get the same number of combinations
        FakeFactors_lightlepton_SF_Muon_HF_down += FakeFactors_electrons

        LightLepDenominator = math.factorial(len(FakeFactors_lightlepton)) / (
            math.factorial(lightleptonnumber) * math.factorial(len(FakeFactors_lightlepton) - lightleptonnumber))

        LightLepCombinations = itertools.combinations(FakeFactors_lightlepton, lightleptonnumber)

        LightLepCombinations_FF_up = itertools.combinations(FakeFactors_lightlepton_FF_up, lightleptonnumber)
        LightLepCombinations_FF_down = itertools.combinations(FakeFactors_lightlepton_FF_down, lightleptonnumber)
        LightLepCombinations_PF_up = itertools.combinations(FakeFactors_lightlepton_PF_up, lightleptonnumber)
        LightLepCombinations_PF_down = itertools.combinations(FakeFactors_lightlepton_PF_down, lightleptonnumber)
        LightLepCombinations_SF_Electron_HF_up = itertools.combinations(FakeFactors_lightlepton_SF_Electron_HF_up, lightleptonnumber)
        LightLepCombinations_SF_Electron_HF_down = itertools.combinations(FakeFactors_lightlepton_SF_Electron_HF_down, lightleptonnumber)
        LightLepCombinations_SF_Muon_HF_up = itertools.combinations(FakeFactors_lightlepton_SF_Muon_HF_up, lightleptonnumber)
        LightLepCombinations_SF_Muon_HF_down = itertools.combinations(FakeFactors_lightlepton_SF_Muon_HF_down, lightleptonnumber)

        if not taunumber == 0:
            for i in range(len(event.LooseTau_pt)):

                pt = event.LooseTau_pt[i]
                eta = event.LooseTau_eta[i]

                if event.LooseTau_prongs[i] == 1:
                    particle = "1P_Tau"
                if event.LooseTau_prongs[i] == 3:
                    particle = "3P_Tau"
                FakeFactors_tau.append(self.FakeFactor[region].EvaluateWeight(particle, pt, abs(eta)))
                FakeFactors_tau_FF_up.append(self.FakeFactor[region].CalculateVariation(particle, pt, abs(eta), Fakefactorvariation=True))
                FakeFactors_tau_FF_down.append(self.FakeFactor[region].CalculateVariation(particle,
                                                                                          pt,
                                                                                          abs(eta),
                                                                                          Fakefactorvariation=True,
                                                                                          downvariation=True))
                FakeFactors_tau_PF_up.append(self.FakeFactor[region].CalculateVariation(particle,
                                                                                        pt,
                                                                                        abs(eta),
                                                                                        Processfractionvariation=True))
                FakeFactors_tau_PF_down.append(self.FakeFactor[region].CalculateVariation(particle,
                                                                                          pt,
                                                                                          abs(eta),
                                                                                          Processfractionvariation=True,
                                                                                          downvariation=True))
                FakeFactors_tau_SF_HF_up.append(self.FakeFactor[region].CalculateVariation(particle,
                                                                                           pt,
                                                                                           abs(eta),
                                                                                           Scalefactorvariation=True,
                                                                                           VariedFaketype="HF"))
                FakeFactors_tau_SF_HF_down.append(self.FakeFactor[region].CalculateVariation(particle,
                                                                                             pt,
                                                                                             abs(eta),
                                                                                             Scalefactorvariation=True,
                                                                                             downvariation=True,
                                                                                             VariedFaketype="HF"))
                FakeFactors_tau_SF_LF_up.append(self.FakeFactor[region].CalculateVariation(particle,
                                                                                           pt,
                                                                                           abs(eta),
                                                                                           Scalefactorvariation=True,
                                                                                           VariedFaketype="LF"))
                FakeFactors_tau_SF_LF_down.append(self.FakeFactor[region].CalculateVariation(particle,
                                                                                             pt,
                                                                                             abs(eta),
                                                                                             Scalefactorvariation=True,
                                                                                             downvariation=True,
                                                                                             VariedFaketype="LF"))

        TauDenominator = math.factorial(
            len(FakeFactors_tau)) / (math.factorial(taunumber) * math.factorial(len(FakeFactors_tau) - taunumber))

        TauCombinations = itertools.combinations(FakeFactors_tau, taunumber)

        TauCombinations_FF_up = itertools.combinations(FakeFactors_tau_FF_up, taunumber)
        TauCombinations_FF_down = itertools.combinations(FakeFactors_tau_FF_down, taunumber)
        TauCombinations_PF_up = itertools.combinations(FakeFactors_tau_PF_up, taunumber)
        TauCombinations_PF_down = itertools.combinations(FakeFactors_tau_PF_down, taunumber)
        TauCombinations_SF_HF_up = itertools.combinations(FakeFactors_tau_SF_HF_up, taunumber)
        TauCombinations_SF_HF_down = itertools.combinations(FakeFactors_tau_SF_HF_down, taunumber)
        TauCombinations_SF_LF_up = itertools.combinations(FakeFactors_tau_SF_LF_up, taunumber)
        TauCombinations_SF_LF_down = itertools.combinations(FakeFactors_tau_SF_LF_down, taunumber)

        FakeFactorWeight_lightlep = 0

        FakeFactorWeight_lightlep_FF_up = 0
        FakeFactorWeight_lightlep_FF_down = 0
        FakeFactorWeight_lightlep_PF_up = 0
        FakeFactorWeight_lightlep_PF_down = 0
        FakeFactorWeight_lightlep_SF_Electron_HF_up = 0
        FakeFactorWeight_lightlep_SF_Electron_HF_down = 0
        FakeFactorWeight_lightlep_SF_Muon_HF_up = 0
        FakeFactorWeight_lightlep_SF_Muon_HF_down = 0

        FakeFactorWeight_tau = 0

        FakeFactorWeight_tau_FF_up = 0
        FakeFactorWeight_tau_FF_down = 0
        FakeFactorWeight_tau_PF_up = 0
        FakeFactorWeight_tau_PF_down = 0
        FakeFactorWeight_tau_SF_HF_up = 0
        FakeFactorWeight_tau_SF_HF_down = 0
        FakeFactorWeight_tau_SF_LF_up = 0
        FakeFactorWeight_tau_SF_LF_down = 0

        if lightleptonnumber == 1:
            for FF in LightLepCombinations:
                FakeFactorWeight_lightlep += FF[0]
            # variations
            for FF in LightLepCombinations_FF_up:
                FakeFactorWeight_lightlep_FF_up += FF[0]
            for FF in LightLepCombinations_FF_down:
                FakeFactorWeight_lightlep_FF_down += FF[0]
            for FF in LightLepCombinations_PF_up:
                FakeFactorWeight_lightlep_PF_up += FF[0]
            for FF in LightLepCombinations_PF_down:
                FakeFactorWeight_lightlep_PF_down += FF[0]
            for FF in LightLepCombinations_SF_Electron_HF_up:
                FakeFactorWeight_lightlep_SF_Electron_HF_up += FF[0]
            for FF in LightLepCombinations_SF_Electron_HF_down:
                FakeFactorWeight_lightlep_SF_Electron_HF_down += FF[0]
            for FF in LightLepCombinations_SF_Muon_HF_up:
                FakeFactorWeight_lightlep_SF_Muon_HF_up += FF[0]
            for FF in LightLepCombinations_SF_Muon_HF_down:
                FakeFactorWeight_lightlep_SF_Muon_HF_down += FF[0]

        elif lightleptonnumber == 2:
            for FF in LightLepCombinations:
                FakeFactorWeight_lightlep += FF[0] * FF[1]
            # variations
            for FF in LightLepCombinations_FF_up:
                FakeFactorWeight_lightlep_FF_up += FF[0] * FF[1]
            for FF in LightLepCombinations_FF_down:
                FakeFactorWeight_lightlep_FF_down += FF[0] * FF[1]
            for FF in LightLepCombinations_PF_up:
                FakeFactorWeight_lightlep_PF_up += FF[0] * FF[1]
            for FF in LightLepCombinations_PF_down:
                FakeFactorWeight_lightlep_PF_down += FF[0] * FF[1]
            for FF in LightLepCombinations_SF_Electron_HF_up:
                FakeFactorWeight_lightlep_SF_Electron_HF_up += FF[0] * FF[1]
            for FF in LightLepCombinations_SF_Electron_HF_down:
                FakeFactorWeight_lightlep_SF_Electron_HF_down += FF[0] * FF[1]
            for FF in LightLepCombinations_SF_Muon_HF_up:
                FakeFactorWeight_lightlep_SF_Muon_HF_up += FF[0] * FF[1]
            for FF in LightLepCombinations_SF_Muon_HF_down:
                FakeFactorWeight_lightlep_SF_Muon_HF_down += FF[0] * FF[1]

        FakeFactorWeight_lightlep = FakeFactorWeight_lightlep / LightLepDenominator

        FakeFactorWeight_lightlep_FF_up = FakeFactorWeight_lightlep_FF_up / LightLepDenominator
        FakeFactorWeight_lightlep_FF_down = FakeFactorWeight_lightlep_FF_down / LightLepDenominator
        FakeFactorWeight_lightlep_PF_up = FakeFactorWeight_lightlep_PF_up / LightLepDenominator
        FakeFactorWeight_lightlep_PF_down = FakeFactorWeight_lightlep_PF_down / LightLepDenominator
        FakeFactorWeight_lightlep_SF_Electron_HF_up = FakeFactorWeight_lightlep_SF_Electron_HF_up / LightLepDenominator
        FakeFactorWeight_lightlep_SF_Electron_HF_down = FakeFactorWeight_lightlep_SF_Electron_HF_down / LightLepDenominator
        FakeFactorWeight_lightlep_SF_Muon_HF_up = FakeFactorWeight_lightlep_SF_Muon_HF_up / LightLepDenominator
        FakeFactorWeight_lightlep_SF_Muon_HF_down = FakeFactorWeight_lightlep_SF_Muon_HF_down / LightLepDenominator

        if taunumber == 1:
            for FF in TauCombinations:
                FakeFactorWeight_tau += FF[0]

            for FF in TauCombinations_FF_up:
                FakeFactorWeight_tau_FF_up += FF[0]
            for FF in TauCombinations_FF_down:
                FakeFactorWeight_tau_FF_down += FF[0]
            for FF in TauCombinations_PF_up:
                FakeFactorWeight_tau_PF_up += FF[0]
            for FF in TauCombinations_PF_down:
                FakeFactorWeight_tau_PF_down += FF[0]
            for FF in TauCombinations_SF_HF_up:
                FakeFactorWeight_tau_SF_HF_up += FF[0]
            for FF in TauCombinations_SF_HF_down:
                FakeFactorWeight_tau_SF_HF_down += FF[0]
            for FF in TauCombinations_SF_LF_up:
                FakeFactorWeight_tau_SF_LF_up += FF[0]
            for FF in TauCombinations_SF_LF_down:
                FakeFactorWeight_tau_SF_LF_down += FF[0]

        elif taunumber == 2:
            for FF in TauCombinations:
                FakeFactorWeight_tau += FF[0] * FF[1]

            for FF in TauCombinations_FF_up:
                FakeFactorWeight_tau_FF_up += FF[0] * FF[1]
            for FF in TauCombinations_FF_down:
                FakeFactorWeight_tau_FF_down += FF[0] * FF[1]
            for FF in TauCombinations_PF_up:
                FakeFactorWeight_tau_PF_up += FF[0] * FF[1]
            for FF in TauCombinations_PF_down:
                FakeFactorWeight_tau_PF_down += FF[0] * FF[1]
            for FF in TauCombinations_SF_HF_up:
                FakeFactorWeight_tau_SF_HF_up += FF[0] * FF[1]
            for FF in TauCombinations_SF_HF_down:
                FakeFactorWeight_tau_SF_HF_down += FF[0] * FF[1]
            for FF in TauCombinations_SF_LF_up:
                FakeFactorWeight_tau_SF_LF_up += FF[0] * FF[1]
            for FF in TauCombinations_SF_LF_down:
                FakeFactorWeight_tau_SF_LF_down += FF[0] * FF[1]

        FakeFactorWeight_tau = FakeFactorWeight_tau / TauDenominator

        FakeFactorWeight_tau_FF_up = FakeFactorWeight_tau_FF_up / LightLepDenominator
        FakeFactorWeight_tau_FF_down = FakeFactorWeight_tau_FF_down / LightLepDenominator
        FakeFactorWeight_tau_PF_up = FakeFactorWeight_tau_PF_up / LightLepDenominator
        FakeFactorWeight_tau_PF_down = FakeFactorWeight_tau_PF_down / LightLepDenominator
        FakeFactorWeight_tau_SF_HF_up = FakeFactorWeight_tau_SF_HF_up / LightLepDenominator
        FakeFactorWeight_tau_SF_HF_down = FakeFactorWeight_tau_SF_HF_down / LightLepDenominator
        FakeFactorWeight_tau_SF_LF_up = FakeFactorWeight_tau_SF_LF_up / LightLepDenominator
        FakeFactorWeight_tau_SF_LF_down = FakeFactorWeight_tau_SF_LF_down / LightLepDenominator

        if not lightleptonnumber == 0 and not taunumber == 0:
            FakeFactorWeight = FakeFactorWeight_lightlep * FakeFactorWeight_tau

            FakeFactorWeight_FF_up = FakeFactorWeight_lightlep_FF_up * FakeFactorWeight_tau_FF_up
            FakeFactorWeight_FF_down = FakeFactorWeight_lightlep_FF_down * FakeFactorWeight_tau_FF_down
            FakeFactorWeight_PF_up = FakeFactorWeight_lightlep_PF_up * FakeFactorWeight_tau_PF_up
            FakeFactorWeight_PF_down = FakeFactorWeight_lightlep_PF_down * FakeFactorWeight_tau_PF_down
            FakeFactorWeight_SF_electron_HF_up = FakeFactorWeight_lightlep_SF_Electron_HF_up * FakeFactorWeight_tau
            FakeFactorWeight_SF_electron_HF_down = FakeFactorWeight_lightlep_SF_Electron_HF_down * FakeFactorWeight_tau
            FakeFactorWeight_SF_muon_HF_up = FakeFactorWeight_lightlep_SF_Muon_HF_up * FakeFactorWeight_tau
            FakeFactorWeight_SF_muon_HF_down = FakeFactorWeight_lightlep_SF_Muon_HF_down * FakeFactorWeight_tau

            FakeFactorWeight_SF_tau_HF_up = FakeFactorWeight_lightlep * FakeFactorWeight_tau_SF_HF_up
            FakeFactorWeight_SF_tau_HF_down = FakeFactorWeight_lightlep * FakeFactorWeight_tau_SF_HF_down
            FakeFactorWeight_SF_tau_LF_up = FakeFactorWeight_lightlep * FakeFactorWeight_tau_SF_LF_up
            FakeFactorWeight_SF_tau_LF_down = FakeFactorWeight_lightlep * FakeFactorWeight_tau_SF_LF_down

        elif not lightleptonnumber == 0:
            FakeFactorWeight = FakeFactorWeight_lightlep

            FakeFactorWeight_FF_up = FakeFactorWeight_lightlep_FF_up
            FakeFactorWeight_FF_down = FakeFactorWeight_lightlep_FF_down
            FakeFactorWeight_PF_up = FakeFactorWeight_lightlep_PF_up
            FakeFactorWeight_PF_down = FakeFactorWeight_lightlep_PF_down
            FakeFactorWeight_SF_electron_HF_up = FakeFactorWeight_lightlep_SF_Electron_HF_up
            FakeFactorWeight_SF_electron_HF_down = FakeFactorWeight_lightlep_SF_Electron_HF_down
            FakeFactorWeight_SF_muon_HF_up = FakeFactorWeight_lightlep_SF_Muon_HF_up
            FakeFactorWeight_SF_muon_HF_down = FakeFactorWeight_lightlep_SF_Muon_HF_down

            FakeFactorWeight_SF_tau_HF_up = FakeFactorWeight_lightlep
            FakeFactorWeight_SF_tau_HF_down = FakeFactorWeight_lightlep
            FakeFactorWeight_SF_tau_LF_up = FakeFactorWeight_lightlep
            FakeFactorWeight_SF_tau_LF_down = FakeFactorWeight_lightlep

        elif not taunumber == 0:
            FakeFactorWeight = FakeFactorWeight_tau

            FakeFactorWeight_FF_up = FakeFactorWeight_tau_FF_up
            FakeFactorWeight_FF_down = FakeFactorWeight_tau_FF_down
            FakeFactorWeight_PF_up = FakeFactorWeight_tau_PF_up
            FakeFactorWeight_PF_down = FakeFactorWeight_tau_PF_down
            FakeFactorWeight_SF_electron_HF_up = FakeFactorWeight_tau
            FakeFactorWeight_SF_electron_HF_down = FakeFactorWeight_tau
            FakeFactorWeight_SF_muon_HF_up = FakeFactorWeight_tau
            FakeFactorWeight_SF_muon_HF_down = FakeFactorWeight_tau

            FakeFactorWeight_SF_tau_HF_up = FakeFactorWeight_tau_SF_HF_up
            FakeFactorWeight_SF_tau_HF_down = FakeFactorWeight_tau_SF_HF_down
            FakeFactorWeight_SF_tau_LF_up = FakeFactorWeight_tau_SF_LF_up
            FakeFactorWeight_SF_tau_LF_down = FakeFactorWeight_tau_SF_LF_down

        else:
            print "Error: number of tau and lightlepton is 0"
            return 0
        if isCR2 == True:
            FakeFactorWeight = -FakeFactorWeight

            FakeFactorWeight_FF_up = -FakeFactorWeight_FF_up
            FakeFactorWeight_FF_down = -FakeFactorWeight_FF_down
            FakeFactorWeight_PF_up = -FakeFactorWeight_PF_up
            FakeFactorWeight_PF_down = -FakeFactorWeight_PF_down
            FakeFactorWeight_SF_electron_HF_up = -FakeFactorWeight_SF_electron_HF_up
            FakeFactorWeight_SF_electron_HF_down = -FakeFactorWeight_SF_electron_HF_down
            FakeFactorWeight_SF_muon_HF_up = -FakeFactorWeight_SF_muon_HF_up
            FakeFactorWeight_SF_muon_HF_down = -FakeFactorWeight_SF_muon_HF_down
            FakeFactorWeight_SF_tau_HF_up = -FakeFactorWeight_SF_tau_HF_up
            FakeFactorWeight_SF_tau_HF_down = -FakeFactorWeight_SF_tau_HF_down
            FakeFactorWeight_SF_tau_LF_up = -FakeFactorWeight_SF_tau_LF_up
            FakeFactorWeight_SF_tau_LF_down = -FakeFactorWeight_SF_tau_LF_down

        Fakeweights = {
            "Nominal": FakeFactorWeight,
            "FF_up": FakeFactorWeight_FF_up,
            "FF_down": FakeFactorWeight_FF_down,
            "PF_up": FakeFactorWeight_PF_up,
            "PF_down": FakeFactorWeight_PF_down,
            "SF_Tau_LF_up": FakeFactorWeight_SF_tau_LF_up,
            "SF_Tau_LF_down": FakeFactorWeight_SF_tau_LF_down,
            "SF_Tau_HF_up": FakeFactorWeight_SF_tau_HF_up,
            "SF_Tau_HF_down": FakeFactorWeight_SF_tau_HF_down,
            "SF_Electron_HF_up": FakeFactorWeight_SF_electron_HF_up,
            "SF_Electron_HF_down": FakeFactorWeight_SF_electron_HF_down,
            "SF_Muon_HF_up": FakeFactorWeight_SF_muon_HF_up,
            "SF_Muon_HF_down": FakeFactorWeight_SF_muon_HF_down
        }

        return Fakeweights


class FakeFactor(object):
    def __init__(self, CRregion, options):
        self.options = options
        self.FakeTypes = options.faketypes  # Faketypes to be considered
        self.Processes = options.processes  # processes to be considered
        self.useProcessfraction = True
        # name of the fakefactorhistos

        self.FakeFactorElectron = {
            "ttbar": {
                "LF": "FakeFactor/ttbar/LF/FakeFactor_Electrons_origLF_pteta",
                "HF": "FakeFactor/ttbar/HF/FakeFactor_Electrons_origHF_pteta"
            },
            # ~ "Conv" : "FakeFactor/ttbar/Conv/FakeFactor_Electrons_origConv_pteta"},
            "Zjets": {
                "LF": "FakeFactor/Zjets/LF/FakeFactor_Electrons_origLF_pteta",
                "HF": "FakeFactor/Zjets/HF/FakeFactor_Electrons_origHF_pteta"
            }
            # ~ "Conv" : "FakeFactor/Zjets/Conv/FakeFactor_Electrons_origConv_pteta"}
        }

        self.FakeFactorMuon = {
            "ttbar": {
                "LF": "FakeFactor/ttbar/LF/FakeFactor_Muons_origLF_pteta",
                "HF": "FakeFactor/ttbar/HF/FakeFactor_Muons_origHF_pteta"
            },
            "Zjets": {
                "LF": "FakeFactor/Zjets/LF/FakeFactor_Muons_origLF_pteta",
                "HF": "FakeFactor/Zjets/HF/FakeFactor_Muons_origHF_pteta"
            }
        }
        # ~ self.FakeFactorElectron_ttbar = {
        # ~ "LF": "LF_Nominal/FakeFactor/FakeFactor_Electrons_origLF_pteta",
        # ~ "HF": "HF_Nominal/FakeFactor/FakeFactor_Electrons_origHF_pteta",
        # ~ "Conv": "Conv_Nominal/FakeFactor/FakeFactor_Electrons_origConv_pteta"
        # ~ }

        # ~ self.FakeFactorElectron_Zjets = {
        # ~ "LF": "LF_Nominal/FakeFactor/FakeFactor_Electrons_origLF_pteta",
        # ~ "HF": "HF_Nominal/FakeFactor/FakeFactor_Electrons_origHF_pteta",
        # ~ "Conv": "Conv_Nominal/FakeFactor/FakeFactor_Electrons_origConv_pteta"
        # ~ }

        # ~ self.FakeFactorMuon_ttbar = {
        # ~ "LF": "LF_Nominal/FakeFactor/FakeFactor_Muons_origLF_pteta",
        # ~ "HF": "HF_Nominal/FakeFactor/FakeFactor_Muons_origHF_pteta"
        # ~ }

        # ~ self.FakeFactorMuon_Zjets = {
        # ~ "LF": "LF_Nominal/FakeFactor/FakeFactor_Muons_origLF_pteta",
        # ~ "HF": "HF_Nominal/FakeFactor/FakeFactor_Muons_origHF_pteta"
        # ~ }

        self.FakeFactor1PTau = {
            "ttbar": {
                "LF": "FakeFactor/ttbar/LF/FakeFactor_Tau_origLF_prong1P_pteta",
                "HF": "FakeFactor/ttbar/HF/FakeFactor_Tau_origHF_prong1P_pteta",
                "Gluon": "FakeFactor/ttbar/Gluon/FakeFactor_Tau_origGluon_prong1P_pteta",
                "Elec": "FakeFactor/ttbar/Elec/FakeFactor_Tau_origElec_prong1P_pteta"
            },
            "Zjets": {
                "LF": "FakeFactor/Zjets/LF/FakeFactor_Tau_origLF_prong1P_pteta",
                "HF": "FakeFactor/Zjets/HF/FakeFactor_Tau_origHF_prong1P_pteta",
                "Gluon": "FakeFactor/Zjets/Gluon/FakeFactor_Tau_origGluon_prong1P_pteta",
                "Elec": "FakeFactor/Zjets/Elec/FakeFactor_Tau_origElec_prong1P_pteta"
            }
        }

        self.FakeFactor3PTau = {
            "ttbar": {
                "LF": "FakeFactor/ttbar/LF/FakeFactor_Tau_origLF_prong3P_pteta",
                "HF": "FakeFactor/ttbar/HF/FakeFactor_Tau_origHF_prong3P_pteta",
                "Gluon": "FakeFactor/ttbar/Gluon/FakeFactor_Tau_origGluon_prong3P_pteta",
                "Elec": "FakeFactor/ttbar/Elec/FakeFactor_Tau_origElec_prong3P_pteta"
            },
            "Zjets": {
                "LF": "FakeFactor/Zjets/LF/FakeFactor_Tau_origLF_prong3P_pteta",
                "HF": "FakeFactor/Zjets/HF/FakeFactor_Tau_origHF_prong3P_pteta",
                "Gluon": "FakeFactor/Zjets/Gluon/FakeFactor_Tau_origGluon_prong3P_pteta",
                "Elec": "FakeFactor/Zjets/Elec/FakeFactor_Tau_origElec_prong3P_pteta"
            }
        }

        # ~ self.FakeFactor1PTau = {
        # ~ "LF": "LF_Nominal/FakeFactor/FakeFactor_Tau_origLF_prong1P_pteta",
        # ~ "HF": "HF_Nominal/FakeFactor/FakeFactor_Tau_origHF_prong1P_pteta",
        # ~ "Gluon": "Gluon_Nominal/FakeFactor/FakeFactor_Tau_origGluon_prong1P_pteta",
        # ~ "Elec": "Elec_Nominal/FakeFactor/FakeFactor_Tau_origElec_prong1P_pteta"
        # ~ }

        # ~ self.FakeFactor3PTau = {
        # ~ "LF": "LF_Nominal/FakeFactor/FakeFactor_Tau_origLF_prong3P_pteta",
        # ~ "HF": "HF_Nominal/FakeFactor/FakeFactor_Tau_origHF_prong3P_pteta",
        # ~ "Gluon": "Gluon_Nominal/FakeFactor/FakeFactor_Tau_origGluon_prong3P_pteta",
        # ~ "Elec": "Elec_Nominal/FakeFactor/FakeFactor_Tau_origElec_prong3P_pteta"
        # ~ }

        self.ScaleFactorElectron = {"HF": "HF/ScaleFactor/ScaleFactor_Electrons_origHF_pt"}

        self.ScaleFactorMuon = {"HF": "HF/ScaleFactor/ScaleFactor_Muons_origHF_pt"}

        self.ScaleFactor1PTau = {
            "LF": "LF/ScaleFactor/ScaleFactor_Tau_origLF_prong1P_pt",
            "HF": "HF/ScaleFactor/ScaleFactor_Tau_origHF_prong1P_pt"
        }

        self.ScaleFactor3PTau = {
            "LF": "LF/ScaleFactor/ScaleFactor_Tau_origLF_prong3P_pt",
            "HF": "HF/ScaleFactor/ScaleFactor_Tau_origHF_prong3P_pt"
        }

        self.ProcFracElectron = {
            "ttbar": {
                "LF": "%s/ttbar/LF/ProcessFraction_LooseElectrons_origLF_pteta" % (CRregion),
                "HF": "%s/ttbar/HF/ProcessFraction_LooseElectrons_origHF_pteta" % (CRregion),
                # ~ "Conv": "%s/ttbar/Conv/ProcessFraction_LooseElectrons_origConv_pteta" % (CRregion)
            },
            "Zjets": {
                "LF": "%s/Zjets/LF/ProcessFraction_LooseElectrons_origLF_pteta" % (CRregion),
                "HF": "%s/Zjets/HF/ProcessFraction_LooseElectrons_origHF_pteta" % (CRregion),
                # ~ "Conv": "%s/Zjets/Conv/ProcessFraction_LooseElectrons_origConv_pteta" % (CRregion)
            }
        }

        self.ProcFracMuon = {
            "ttbar": {
                "LF": "%s/ttbar/LF/ProcessFraction_LooseMuons_origLF_pteta" % (CRregion),
                "HF": "%s/ttbar/HF/ProcessFraction_LooseMuons_origHF_pteta" % (CRregion)
            },
            "Zjets": {
                "LF": "%s/Zjets/LF/ProcessFraction_LooseMuons_origLF_pteta" % (CRregion),
                "HF": "%s/Zjets/HF/ProcessFraction_LooseMuons_origHF_pteta" % (CRregion)
            }
        }

        self.ProcFrac1PTau = {
            "ttbar": {
                "LF": "%s/ttbar/LF/ProcessFraction_LooseTau_origLF_prong1P_pteta" % (CRregion),
                "HF": "%s/ttbar/HF/ProcessFraction_LooseTau_origHF_prong1P_pteta" % (CRregion),
                "Gluon": "%s/ttbar/Gluon/ProcessFraction_LooseTau_origGluon_prong1P_pteta" % (CRregion),
                "Elec": "%s/ttbar/Elec/ProcessFraction_LooseTau_origElec_prong1P_pteta" % (CRregion)
            },
            "Zjets": {
                "LF": "%s/Zjets/LF/ProcessFraction_LooseTau_origLF_prong1P_pteta" % (CRregion),
                "HF": "%s/Zjets/HF/ProcessFraction_LooseTau_origHF_prong1P_pteta" % (CRregion),
                "Gluon": "%s/Zjets/Gluon/ProcessFraction_LooseTau_origGluon_prong1P_pteta" % (CRregion),
                "Elec": "%s/Zjets/Elec/ProcessFraction_LooseTau_origElec_prong1P_pteta" % (CRregion)
            }
        }

        self.ProcFrac3PTau = {
            "ttbar": {
                "LF": "%s/ttbar/LF/ProcessFraction_LooseTau_origLF_prong3P_pteta" % (CRregion),
                "HF": "%s/ttbar/HF/ProcessFraction_LooseTau_origHF_prong3P_pteta" % (CRregion),
                "Gluon": "%s/ttbar/Gluon/ProcessFraction_LooseTau_origGluon_prong3P_pteta" % (CRregion),
                "Elec": "%s/ttbar/Elec/ProcessFraction_LooseTau_origElec_prong3P_pteta" % (CRregion)
            },
            "Zjets": {
                "LF": "%s/Zjets/LF/ProcessFraction_LooseTau_origLF_prong3P_pteta" % (CRregion),
                "HF": "%s/Zjets/HF/ProcessFraction_LooseTau_origHF_prong3P_pteta" % (CRregion),
                "Gluon": "%s/Zjets/Gluon/ProcessFraction_LooseTau_origGluon_prong3P_pteta" % (CRregion),
                "Elec": "%s/Zjets/Elec/ProcessFraction_LooseTau_origElec_prong3P_pteta" % (CRregion)
            }
        }
        self.FakeFactorFile_lightlep = ROOT.TFile.Open("%s/Fakefactors_lightlepton.root" % (options.FakefactorFiles))
        # ~ self.FakeFactorFile_lightlep_ttbar = ROOT.TFile.Open("%s/FakeFactors_lightlepton_ttbar.root" % (options.FakefactorFiles))
        # ~ self.FakeFactorFile_lightlep_Zjets = ROOT.TFile.Open("%s/FakeFactors_lightlepton_Zjets.root" % (options.FakefactorFiles))
        self.FakeFactorFile_tau = ROOT.TFile.Open("%s/Fakefactors_tau.root" % (options.FakefactorFiles))
        # ~ self.FakeFactorFile_tau_ttbar = ROOT.TFile.Open("%s/FakeFactors_tau_ttbar.root" % (options.FakefactorFiles))
        # ~ self.FakeFactorFile_tau_Zjets = ROOT.TFile.Open("%s/FakeFactors_tau_Zjets.root" % (options.FakefactorFiles))

        if self.options.addScalefactor:
            self.ScaleFactorFile_lightlep = ROOT.TFile.Open("%s/Scalefactors_lightlepton.root" % (options.FakefactorFiles))
            self.ScaleFactorFile_tau = ROOT.TFile.Open("%s/Scalefactors_tau.root" % (options.FakefactorFiles))

        if "ttbar" in self.Processes and not "Zjets" in self.Processes:
            self.ProcessfractionFile_tau = ROOT.TFile.Open("%s/ttbar/Processfractions_tau_ttbar.root" % (options.FakefactorFiles))
            self.ProcessfractionFile_lightlep = ROOT.TFile.Open("%s/ttbar/Processfractions_lightlepton_ttbar.root" %
                                                                (options.FakefactorFiles))
        elif "Zjets" in self.Processes and not "ttbar" in self.Processes:

            self.ProcessfractionFile_tau = ROOT.TFile.Open("%s/Zjets/Processfractions_tau_Zjets.root" % (options.FakefactorFiles))
            self.ProcessfractionFile_lightlep = ROOT.TFile.Open("%s/Zjets/Processfractions_lightlepton_Zjets.root" %
                                                                (options.FakefactorFiles))

        else:
            self.ProcessfractionFile_tau = ROOT.TFile.Open("%s/Processfractions_tau.root" % (options.FakefactorFiles))
            self.ProcessfractionFile_lightlep = ROOT.TFile.Open("%s/Processfractions_lightlepton.root" % (options.FakefactorFiles))

        self.FakeFactorHistos_Electron = {}
        self.ProcessFractionHistos_Electron = {}
        self.FakeFactorHistos_Muon = {}
        self.ProcessFractionHistos_Muon = {}
        self.FakeFactorHistos_1prong = {}
        self.ProcessFractionHistos_1prong = {}
        self.FakeFactorHistos_3prong = {}
        self.ProcessFractionHistos_3prong = {}

        self.ScaleFactorHistos_Electron = {}
        self.ScaleFactorHistos_Muon = {}
        self.ScaleFactorHistos_1PTau = {}
        self.ScaleFactorHistos_3PTau = {}

        # loading the fakefactorhistos
        for process in self.Processes:
            self.FakeFactorHistos_Electron[process] = {}
            self.ProcessFractionHistos_Electron[process] = {}
            self.FakeFactorHistos_Muon[process] = {}
            self.ProcessFractionHistos_Muon[process] = {}
            self.FakeFactorHistos_1prong[process] = {}
            self.ProcessFractionHistos_1prong[process] = {}
            self.FakeFactorHistos_3prong[process] = {}
            self.ProcessFractionHistos_3prong[process] = {}

        for process in self.FakeFactorElectron:
            if process in self.Processes:
                for faketype, histoname in self.FakeFactorElectron[process].items():
                    if faketype in self.FakeTypes:
                        Histo = self.FakeFactorFile_lightlep.Get(histoname)
                        self.FakeFactorHistos_Electron[process][faketype] = Histo
        for process in self.FakeFactorMuon:
            if process in self.Processes:
                for faketype, histoname in self.FakeFactorMuon[process].items():
                    if faketype in self.FakeTypes:
                        Histo = self.FakeFactorFile_lightlep.Get(histoname)
                        self.FakeFactorHistos_Muon[process][faketype] = Histo
        for process in self.FakeFactor1PTau:
            if process in self.Processes:
                for faketype, histoname in self.FakeFactor1PTau[process].items():
                    if faketype in self.FakeTypes:
                        Histo = self.FakeFactorFile_tau.Get(histoname)
                        self.FakeFactorHistos_1prong[process][faketype] = Histo
        for process in self.FakeFactor3PTau:
            if process in self.Processes:
                for faketype, histoname in self.FakeFactor3PTau[process].items():
                    if faketype in self.FakeTypes:
                        Histo = self.FakeFactorFile_tau.Get(histoname)
                        self.FakeFactorHistos_3prong[process][faketype] = Histo
        # ~ if "ttbar" in self.Processes:
        # ~ for faketype, histoname in self.FakeFactorElectron_ttbar.items():
        # ~ if faketype in self.FakeTypes:
        # ~ Histo = self.FakeFactorFile_lightlep_ttbar.Get(histoname)

        # ~ self.FakeFactorHistos_Electron["ttbar"][faketype] = Histo
        # ~ if "Zjets" in self.Processes:
        # ~ for faketype, histoname in self.FakeFactorElectron_Zjets.items():
        # ~ if faketype in self.FakeTypes:
        # ~ Histo = self.FakeFactorFile_lightlep_Zjets.Get(histoname)

        # ~ self.FakeFactorHistos_Electron["Zjets"][faketype] = Histo

        # ~ if "ttbar" in self.Processes:
        # ~ for faketype, histoname in self.FakeFactorMuon_ttbar.items():
        # ~ if faketype in self.FakeTypes:
        # ~ Histo = self.FakeFactorFile_lightlep_ttbar.Get(histoname)

        # ~ self.FakeFactorHistos_Muon["ttbar"][faketype] = Histo
        # ~ if "Zjets" in self.Processes:
        # ~ for faketype, histoname in self.FakeFactorMuon_Zjets.items():
        # ~ if faketype in self.FakeTypes:
        # ~ Histo = self.FakeFactorFile_lightlep_Zjets.Get(histoname)

        # ~ self.FakeFactorHistos_Muon["Zjets"][faketype] = Histo

        # ~ for faketype, histoname in self.FakeFactor1PTau.items():
        # ~ if faketype in self.FakeTypes:
        # ~ if "ttbar" in self.Processes:
        # ~ Histo = self.FakeFactorFile_tau_ttbar.Get(histoname)

        # ~ self.FakeFactorHistos_1prong["ttbar"][faketype] = Histo
        # ~ if "Zjets" in self.Processes:
        # ~ Histo = self.FakeFactorFile_tau_Zjets.Get(histoname)

        # ~ self.FakeFactorHistos_1prong["Zjets"][faketype] = Histo

        # ~ for faketype, histoname in self.FakeFactor3PTau.items():
        # ~ if faketype in self.FakeTypes:
        # ~ if "ttbar" in self.Processes:
        # ~ Histo = self.FakeFactorFile_tau_ttbar.Get(histoname)

        # ~ self.FakeFactorHistos_3prong["ttbar"][faketype] = Histo
        # ~ if "Zjets" in self.Processes:
        # ~ Histo = self.FakeFactorFile_tau_Zjets.Get(histoname)

        # ~ self.FakeFactorHistos_3prong["Zjets"][faketype] = Histo

        for process in self.ProcFracElectron:
            for faketype, histoname in self.ProcFracElectron[process].items():
                if faketype in self.FakeTypes and process in self.Processes:
                    Histo = self.ProcessfractionFile_lightlep.Get(histoname)

                    self.ProcessFractionHistos_Electron[process][faketype] = Histo

        for process in self.ProcFracMuon:
            for faketype, histoname in self.ProcFracMuon[process].items():
                if faketype in self.FakeTypes and process in self.Processes:
                    Histo = self.ProcessfractionFile_lightlep.Get(histoname)

                    self.ProcessFractionHistos_Muon[process][faketype] = Histo

        for process in self.ProcFrac1PTau:
            for faketype, histoname in self.ProcFrac1PTau[process].items():
                if faketype in self.FakeTypes and process in self.Processes:
                    Histo = self.ProcessfractionFile_tau.Get(histoname)

                    self.ProcessFractionHistos_1prong[process][faketype] = Histo

        for process in self.ProcFrac3PTau:
            for faketype, histoname in self.ProcFrac3PTau[process].items():
                if faketype in self.FakeTypes and process in self.Processes:
                    Histo = self.ProcessfractionFile_tau.Get(histoname)

                    self.ProcessFractionHistos_3prong[process][faketype] = Histo

        if self.options.addScalefactor:
            for faketype, histoname in self.ScaleFactorElectron.items():
                if faketype in self.FakeTypes:
                    Histo = self.ScaleFactorFile_lightlep.Get(histoname)

                    self.ScaleFactorHistos_Electron[faketype] = Histo

            for faketype, histoname in self.ScaleFactorMuon.items():
                if faketype in self.FakeTypes:
                    Histo = self.ScaleFactorFile_lightlep.Get(histoname)

                    self.ScaleFactorHistos_Muon[faketype] = Histo

            for faketype, histoname in self.ScaleFactor1PTau.items():
                if faketype in self.FakeTypes:
                    Histo = self.ScaleFactorFile_tau.Get(histoname)

                    self.ScaleFactorHistos_1PTau[faketype] = Histo

            for faketype, histoname in self.ScaleFactor3PTau.items():
                if faketype in self.FakeTypes:
                    Histo = self.ScaleFactorFile_tau.Get(histoname)

                    self.ScaleFactorHistos_3PTau[faketype] = Histo

    def EvaluateWeight(self, particle, pt, eta):

        if particle == "Electron":
            FakeFactorHistos = self.FakeFactorHistos_Electron
            if self.options.addScalefactor:
                ScaleFactorHistos = self.ScaleFactorHistos_Electron
            ProcFracHistos = self.ProcessFractionHistos_Electron
        elif particle == "Muon":
            FakeFactorHistos = self.FakeFactorHistos_Muon
            if self.options.addScalefactor:
                ScaleFactorHistos = self.ScaleFactorHistos_Muon
            ProcFracHistos = self.ProcessFractionHistos_Muon
        elif particle == "1P_Tau":
            FakeFactorHistos = self.FakeFactorHistos_1prong
            if self.options.addScalefactor:
                ScaleFactorHistos = self.ScaleFactorHistos_1PTau
            ProcFracHistos = self.ProcessFractionHistos_1prong
        elif particle == "3P_Tau":
            FakeFactorHistos = self.FakeFactorHistos_3prong
            if self.options.addScalefactor:
                ScaleFactorHistos = self.ScaleFactorHistos_3PTau
            ProcFracHistos = self.ProcessFractionHistos_3prong
        else:
            print "unknown particle"
            return 0

        AverageFakeFactor = 0
        for process in FakeFactorHistos:

            for faketype in FakeFactorHistos[process]:

                FakeFactorHisto = FakeFactorHistos[process][faketype]

                FakeFactorBin = FakeFactorHisto.FindBin(pt, eta)
                FakeFactor = FakeFactorHisto.GetBinContent(FakeFactorBin)

                ScaleFactor = 1.
                if self.options.addScalefactor:
                    if faketype in ScaleFactorHistos:
                        ScaleFactorHisto = ScaleFactorHistos[faketype]
                        ScaleFactorBin = ScaleFactorHisto.FindBin(pt, eta)
                        ScaleFactor = ScaleFactorHisto.GetBinContent(ScaleFactorBin)

                ProcFracHisto = ProcFracHistos[process][faketype]

                ProcFracBin = ProcFracHisto.FindBin(pt, eta)
                ProcFrac = ProcFracHisto.GetBinContent(ProcFracBin)

                if self.useProcessfraction:
                    AverageFakeFactor += FakeFactor * ProcFrac * ScaleFactor
                else:
                    AverageFakeFactor += FakeFactor * ScaleFactor

        return AverageFakeFactor

    def CalculateVariation(self,
                           particle,
                           pt,
                           eta,
                           Fakefactorvariation=False,
                           Scalefactorvariation=False,
                           Processfractionvariation=False,
                           VariedFaketype="",
                           downvariation=False):
        if particle == "Electron":
            FakeFactorHistos = self.FakeFactorHistos_Electron
            ScaleFactorHistos = self.ScaleFactorHistos_Electron
            ProcFracHistos = self.ProcessFractionHistos_Electron
        elif particle == "Muon":
            FakeFactorHistos = self.FakeFactorHistos_Muon
            ScaleFactorHistos = self.ScaleFactorHistos_Muon
            ProcFracHistos = self.ProcessFractionHistos_Muon
        elif particle == "1P_Tau":
            FakeFactorHistos = self.FakeFactorHistos_1prong
            ScaleFactorHistos = self.ScaleFactorHistos_1PTau
            ProcFracHistos = self.ProcessFractionHistos_1prong
        elif particle == "3P_Tau":
            FakeFactorHistos = self.FakeFactorHistos_3prong
            ScaleFactorHistos = self.ScaleFactorHistos_3PTau
            ProcFracHistos = self.ProcessFractionHistos_3prong
        else:
            print "unknown particle"
            return 0
        if not Fakefactorvariation and not Scalefactorvariation and not Processfractionvariation:
            print "Error: no variation given"
            return 0
        if (Fakefactorvariation and (Scalefactorvariation or Processfractionvariation)) or (
                Scalefactorvariation and
            (Fakefactorvariation or Processfractionvariation)) or (Processfractionvariation and
                                                                   (Fakefactorvariation or Scalefactorvariation)):
            print "Error: Cannot calculate multiple variations simultaniously"
            return 0
        if Scalefactorvariation and VariedFaketype == "":
            print "Error: for which faketype do you want to calculate the scalefactorvariation"
            return 0

        AverageFakeFactor = 0
        for process in FakeFactorHistos:

            for faketype in FakeFactorHistos[process]:

                FakeFactorHisto = FakeFactorHistos[process][faketype]

                FakeFactorBin = FakeFactorHisto.FindBin(pt, eta)
                if Fakefactorvariation:
                    if not downvariation:
                        FakeFactor = FakeFactorHisto.GetBinContent(FakeFactorBin) + FakeFactorHisto.GetBinError(FakeFactorBin)
                    else:
                        FakeFactor = FakeFactorHisto.GetBinContent(FakeFactorBin) - FakeFactorHisto.GetBinError(FakeFactorBin)
                else:
                    FakeFactor = FakeFactorHisto.GetBinContent(FakeFactorBin)

                ScaleFactor = 1.
                if self.options.addScalefactor:
                    if faketype in ScaleFactorHistos:
                        ScaleFactorHisto = ScaleFactorHistos[faketype]
                        ScaleFactorBin = ScaleFactorHisto.FindBin(pt, eta)
                        if Scalefactorvariation and faketype == VariedFaketype:
                            if not downvariation:
                                ScaleFactor = ScaleFactorHisto.GetBinContent(ScaleFactorBin) + ScaleFactorHisto.GetBinError(ScaleFactorBin)
                            else:
                                ScaleFactor = ScaleFactorHisto.GetBinContent(ScaleFactorBin) - ScaleFactorHisto.GetBinError(ScaleFactorBin)
                        else:
                            ScaleFactor = ScaleFactorHisto.GetBinContent(ScaleFactorBin)
                ProcFracHisto = ProcFracHistos[process][faketype]

                ProcFracBin = ProcFracHisto.FindBin(pt, eta)
                if Processfractionvariation:
                    if not downvariation:
                        ProcFrac = ProcFracHisto.GetBinContent(ProcFracBin) + ProcFracHisto.GetBinError(ProcFracBin)
                    else:
                        ProcFrac = ProcFracHisto.GetBinContent(ProcFracBin) - ProcFracHisto.GetBinError(ProcFracBin)
                else:
                    ProcFrac = ProcFracHisto.GetBinContent(ProcFracBin)

                if self.useProcessfraction:
                    AverageFakeFactor += FakeFactor * ProcFrac * ScaleFactor
                else:
                    AverageFakeFactor += FakeFactor * ScaleFactor
        # ~ if particle == "Muon" and  Scalefactorvariation and downvariation:
        # ~ print AverageFakeFactor

        return AverageFakeFactor


def CalculateFakefactorWeight(event, ProcFracRegion, lightleptonnumber, taunumber, isCR2=False):
    FakeFactor = FakeFactors.EvaluateFakeFactorWeight(event, ProcFracRegion, lightleptonnumber, taunumber, isCR2)
    FakeFactorWeight = FakeFactor["Nominal"]
    FakeFactorWeight_FF_up = FakeFactor["FF_up"]
    FakeFactorWeight_FF_down = FakeFactor["FF_down"]
    FakeFactorWeight_PF_up = FakeFactor["PF_up"]
    FakeFactorWeight_PF_down = FakeFactor["PF_down"]
    FakeFactorWeight_SF_tau_LF_up = FakeFactor["SF_Tau_LF_up"]
    FakeFactorWeight_SF_tau_LF_down = FakeFactor["SF_Tau_LF_down"]
    FakeFactorWeight_SF_tau_HF_up = FakeFactor["SF_Tau_HF_up"]
    FakeFactorWeight_SF_tau_HF_down = FakeFactor["SF_Tau_HF_down"]
    FakeFactorWeight_SF_electron_HF_up = FakeFactor["SF_Electron_HF_up"]
    FakeFactorWeight_SF_electron_HF_down = FakeFactor["SF_Electron_HF_down"]
    FakeFactorWeight_SF_muon_HF_up = FakeFactor["SF_Muon_HF_up"]
    FakeFactorWeight_SF_muon_HF_down = FakeFactor["SF_Muon_HF_down"]

    Fakeweights = {
        "Nominal": FakeFactorWeight,
        "FF_up": FakeFactorWeight_FF_up,
        "FF_down": FakeFactorWeight_FF_down,
        "PF_up": FakeFactorWeight_PF_up,
        "PF_down": FakeFactorWeight_PF_down,
        "SF_Tau_LF_up": FakeFactorWeight_SF_tau_LF_up,
        "SF_Tau_LF_down": FakeFactorWeight_SF_tau_LF_down,
        "SF_Tau_HF_up": FakeFactorWeight_SF_tau_HF_up,
        "SF_Tau_HF_down": FakeFactorWeight_SF_tau_HF_down,
        "SF_Electron_HF_up": FakeFactorWeight_SF_electron_HF_up,
        "SF_Electron_HF_down": FakeFactorWeight_SF_electron_HF_down,
        "SF_Muon_HF_up": FakeFactorWeight_SF_muon_HF_up,
        "SF_Muon_HF_down": FakeFactorWeight_SF_muon_HF_down
    }

    return Fakeweights


def InvertSign(Fakeweights):
    InvertedFakeWeights = {}
    for variation, weight in Fakeweights.items():
        InvertedFakeWeights[variation] = -weight

    return InvertedFakeWeights


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description=
        'This script calculates the fakefactorweight from histfittertrees and creates a tree containing the fakefactorweight  \"python CalculateFakeFactors.py -h\"',
        prog='ApplyFakeFactors',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--inputFile", help="the input histfittertrees", required=True)
    parser.add_argument("--FakefactorFiles", help="Folder containing the fakefactor histograms", default="/ptmp/mpp/maren/Fakefactor/")

    parser.add_argument("--addScalefactor", help="apply scalefactors to the fakefactors", action='store_true', default=False)

    parser.add_argument("--treeName", help="the name of the tree", default="FourleptonTree_Nominal")
    parser.add_argument("--outDir",
                        help="Specify an output folder where the closureplots get stored",
                        default='/ptmp/mpp/maren/FakeHistFitterTrees/')
    parser.add_argument("--faketypes",
                        help="specify the faketypes that should be used",
                        nargs='+',
                        default=["LF", "HF", "Gluon", "Elec", "Conv"])

    parser.add_argument("--processes", help="specify the processes used for the fakefactors", nargs='+', default=["ttbar", "Zjets"])
    parser.add_argument("--CreateClosurePlots", help="create the closure plots", action='store_true', default=False)
    parser.add_argument("--nevents", help="how many events should be processed. All by default", type=int, default=-1)
    parser.add_argument("--CreateVRHistos", action="store_true", default=False)
    parser.add_argument("--outDirVRHistos",
                        help="Specify an output folder where the root files containing VR-histos get stored",
                        default='/ptmp/mpp/maren/ReducibleBkgHistos/')

    options = parser.parse_args()
    if options.addScalefactor:
        print "Apply Scalefactors aswell"
    CreateDirectory(options.outDir, False)
    if options.CreateVRHistos:
        CreateDirectory(options.outDirVRHistos, False)

    if options.nevents > 0:
        limitEvents = True
    else:
        limitEvents = False

    FakeFactors = FakeFactorWeight(options)

    File = ROOT.TFile.Open(options.inputFile)

    Tree = File.Get(options.treeName)
    if not options.CreateClosurePlots:
        newFileName = options.inputFile.split("/")[-1]

        newFile = ROOT.TFile("{}/{}".format(options.outDir, newFileName), "RECREATE")

        newTree = Tree.CloneTree(options.nevents)

        FakeFactorarray = array('f', [0])
        FakeFactorarray_FF_up = array('f', [0])
        FakeFactorarray_FF_down = array('f', [0])
        FakeFactorarray_PF_up = array('f', [0])
        FakeFactorarray_PF_down = array('f', [0])
        FakeFactorarray_SF_tau_LF_up = array('f', [0])
        FakeFactorarray_SF_tau_LF_down = array('f', [0])
        FakeFactorarray_SF_tau_HF_up = array('f', [0])
        FakeFactorarray_SF_tau_HF_down = array('f', [0])
        FakeFactorarray_SF_electron_HF_up = array('f', [0])
        FakeFactorarray_SF_electron_HF_down = array('f', [0])
        FakeFactorarray_SF_muon_HF_up = array('f', [0])
        FakeFactorarray_SF_muon_HF_down = array('f', [0])

        branch = newTree.Branch("FakefactorWeight", FakeFactorarray, 'FakefactorWeight/F')
        branch_FF_up = newTree.Branch("FakefactorWeight_FF_up", FakeFactorarray_FF_up, 'FakefactorWeight_FF_up/F')
        branch_FF_down = newTree.Branch("FakefactorWeight_FF_down", FakeFactorarray_FF_down, 'FakefactorWeight_FF_down/F')
        branch_PF_up = newTree.Branch("FakefactorWeight_PF_up", FakeFactorarray_PF_up, 'FakefactorWeight_PF_up/F')
        branch_PF_down = newTree.Branch("FakefactorWeight_PF_down", FakeFactorarray_PF_down, 'FakefactorWeight_PF_down/F')
        branch_SF_tau_LF_up = newTree.Branch("FakefactorWeight_SF_tau_LF_up", FakeFactorarray_SF_tau_LF_up,
                                             'FakefactorWeight_SF_tau_LF_up/F')
        branch_SF_tau_LF_down = newTree.Branch("FakefactorWeight_SF_tau_LF_down", FakeFactorarray_SF_tau_LF_down,
                                               'FakefactorWeight_SF_tau_LF_down/F')
        branch_SF_tau_HF_up = newTree.Branch("FakefactorWeight_SF_tau_HF_up", FakeFactorarray_SF_tau_HF_up,
                                             'FakefactorWeight_SF_tau_HF_up/F')
        branch_SF_tau_HF_down = newTree.Branch("FakefactorWeight_SF_tau_HF_down", FakeFactorarray_SF_tau_HF_down,
                                               'FakefactorWeight_SF_tau_HF_down/F')
        branch_SF_electron_HF_up = newTree.Branch("FakefactorWeight_SF_Electron_HF_up", FakeFactorarray_SF_electron_HF_up,
                                                  'FakefactorWeight_SF_Electron_HF_up/F')
        branch_SF_electron_HF_down = newTree.Branch("FakefactorWeight_SF_Electron_HF_down", FakeFactorarray_SF_electron_HF_down,
                                                    'FakefactorWeight_SF_Electron_HF_down/F')
        branch_SF_muon_HF_up = newTree.Branch("FakefactorWeight_SF_Muon_HF_up", FakeFactorarray_SF_muon_HF_up,
                                              'FakefactorWeight_SF_Muon_HF_up/F')
        branch_SF_muon_HF_down = newTree.Branch("FakefactorWeight_SF_Muon_HF_down", FakeFactorarray_SF_muon_HF_down,
                                                'FakefactorWeight_SF_Muon_HF_down/F')

    if options.inputFile.split("/")[-1].startswith("data"):
        isData = True
    else:
        isData = False

    print "Start Loop over Events"
    Nevents = 0
    Totalevents = Tree.GetEntries()

    if not isData and options.CreateClosurePlots:
        SRregions = ["4L", "4LnoZ", "4LZ", "3L1T", "3L1TnoZ", "3L1TZ", "2L2T", "2L2TnoZ", "2L2TZ", "3L", "3LnoZ", "3LZ"]
        met_weighted = {}
        met_SR = {}
        meff_weighted = {}
        meff_SR = {}

        pt_elec_weighted = {}
        pt_elec_SR = {}
        pt_mu_weighted = {}
        pt_mu_SR = {}
        pt_tau_weighted = {}
        pt_tau_SR = {}

        for region in SRregions:
            met_weighted[region] = ROOT.TH1D("met_weighted_%s" % (region), "", 20, 0, 500)
            met_SR[region] = ROOT.TH1D("met_SR_%s" % (region), "", 20, 0, 500)
            meff_weighted[region] = ROOT.TH1D("meff_weighted_%s" % (region), "", 20, 0, 1000)
            meff_SR[region] = ROOT.TH1D("meff_SR_%s" % (region), "", 20, 0, 1000)

            pt_elec_weighted[region] = ROOT.TH1D("pt_elec_weighted_%s" % (region), "", 20, 0, 500)
            pt_elec_SR[region] = ROOT.TH1D("pt_elec_SR_%s" % (region), "", 20, 0, 500)
            pt_mu_weighted[region] = ROOT.TH1D("pt_mu_weighted_%s" % (region), "", 20, 0, 500)
            pt_mu_SR[region] = ROOT.TH1D("pt_mu_SR_%s" % (region), "", 20, 0, 500)
            pt_tau_weighted[region] = ROOT.TH1D("pt_tau_weighted_%s" % (region), "", 20, 0, 500)
            pt_tau_SR[region] = ROOT.TH1D("pt_tau_SR_%s" % (region), "", 20, 0, 500)

    n_weighted = 0
    n_CR1 = 0
    n_CR2 = 0
    n_sig = 0

    FF_CR1 = 0
    FF_CR2 = 0

    if options.outDirVRHistos == options.outDir:
        print "Error: outputfolder for Validationhistos are the same as outputfolder for trees"
        exit(1)
    if options.CreateVRHistos:

        CR1_VR0Histo = ROOT.TH1D("CR1_VR0", "", 5, 0, 600)
        CR2_VR0Histo = ROOT.TH1D("CR2_VR0", "", 5, 0, 600)
        VR0Histo = ROOT.TH1D("VR0", "", 5, 0, 600)
        CR1_VR1Histo = ROOT.TH1D("CR1_VR1", "", 5, 0, 600)
        CR2_VR1Histo = ROOT.TH1D("CR2_VR1", "", 5, 0, 600)
        VR1Histo = ROOT.TH1D("VR1", "", 5, 0, 600)
        CR1_VR2Histo = ROOT.TH1D("CR1_VR2", "", 5, 0, 600)
        CR2_VR2Histo = ROOT.TH1D("CR2_VR2", "", 5, 0, 600)
        VR2Histo = ROOT.TH1D("VR2", "", 5, 0, 600)

    for event in Tree:
        if limitEvents == True and Nevents >= options.nevents:
            break
        if Nevents > 0 and Nevents % 1000 == 0:
            print "Executed {}/{} events".format(Nevents, Totalevents)

        Fakeweights = {
            "Nominal": 1,
            "FF_up": 1,
            "FF_down": 1,
            "PF_up": 1,
            "PF_down": 1,
            "SF_Tau_LF_up": 1,
            "SF_Tau_LF_down": 1,
            "SF_Tau_HF_up": 1,
            "SF_Tau_HF_down": 1,
            "SF_Electron_HF_up": 1,
            "SF_Electron_HF_down": 1,
            "SF_Muon_HF_up": 1,
            "SF_Muon_HF_down": 1
        }

        if not isData:

            Weight = event.NormWeight
        else:
            Weight = 1

        if not isData and options.CreateClosurePlots:
            if event.Is4L == 1:
                met_SR["4L"].Fill(event.Met, Weight)
                meff_SR["4L"].Fill(event.Meff, Weight)
                for i in range(len(event.SigEle_pt)):
                    pt_elec_SR["4L"].Fill(event.SigEle_pt[i], Weight)
                for i in range(len(event.SigMuo_pt)):
                    pt_mu_SR["4L"].Fill(event.SigMuo_pt[i], Weight)
                for i in range(len(event.SigTau_pt)):
                    pt_tau_SR["4L"].Fill(event.SigTau_pt[i], Weight)

            if event.Is4LnoZ == 1:
                met_SR["4LnoZ"].Fill(event.Met, Weight)
                meff_SR["4LnoZ"].Fill(event.Meff, Weight)
                for i in range(len(event.SigEle_pt)):
                    pt_elec_SR["4LnoZ"].Fill(event.SigEle_pt[i], Weight)
                for i in range(len(event.SigMuo_pt)):
                    pt_mu_SR["4LnoZ"].Fill(event.SigMuo_pt[i], Weight)
                for i in range(len(event.SigTau_pt)):
                    pt_tau_SR["4LnoZ"].Fill(event.SigTau_pt[i], Weight)

            if event.Is4LZ == 1:

                met_SR["4LZ"].Fill(event.Met, Weight)
                meff_SR["4LZ"].Fill(event.Meff, Weight)
                for i in range(len(event.SigEle_pt)):
                    pt_elec_SR["4LZ"].Fill(event.SigEle_pt[i], Weight)
                for i in range(len(event.SigMuo_pt)):
                    pt_mu_SR["4LZ"].Fill(event.SigMuo_pt[i], Weight)
                for i in range(len(event.SigTau_pt)):
                    pt_tau_SR["4LZ"].Fill(event.SigTau_pt[i], Weight)

            if event.Is3L == 1:
                met_SR["3L"].Fill(event.Met, Weight)
                meff_SR["3L"].Fill(event.Meff, Weight)
                for i in range(len(event.SigEle_pt)):
                    pt_elec_SR["3L"].Fill(event.SigEle_pt[i], Weight)
                for i in range(len(event.SigMuo_pt)):
                    pt_mu_SR["3L"].Fill(event.SigMuo_pt[i], Weight)
                for i in range(len(event.SigTau_pt)):
                    pt_tau_SR["3L"].Fill(event.SigTau_pt[i], Weight)

            if event.Is3LnoZ == 1:
                met_SR["3LnoZ"].Fill(event.Met, Weight)
                meff_SR["3LnoZ"].Fill(event.Meff, Weight)
                for i in range(len(event.SigEle_pt)):
                    pt_elec_SR["3LnoZ"].Fill(event.SigEle_pt[i], Weight)
                for i in range(len(event.SigMuo_pt)):
                    pt_mu_SR["3LnoZ"].Fill(event.SigMuo_pt[i], Weight)
                for i in range(len(event.SigTau_pt)):
                    pt_tau_SR["3LnoZ"].Fill(event.SigTau_pt[i], Weight)

            if event.Is3LZ == 1:

                met_SR["3LZ"].Fill(event.Met, Weight)
                meff_SR["3LZ"].Fill(event.Meff, Weight)
                for i in range(len(event.SigEle_pt)):
                    pt_elec_SR["3LZ"].Fill(event.SigEle_pt[i], Weight)
                for i in range(len(event.SigMuo_pt)):
                    pt_mu_SR["3LZ"].Fill(event.SigMuo_pt[i], Weight)
                for i in range(len(event.SigTau_pt)):
                    pt_tau_SR["3LZ"].Fill(event.SigTau_pt[i], Weight)

            if event.Is3L1T == 1:
                met_SR["3L1T"].Fill(event.Met, Weight)
                meff_SR["3L1T"].Fill(event.Meff, Weight)
                for i in range(len(event.SigEle_pt)):
                    pt_elec_SR["3L1T"].Fill(event.SigEle_pt[i], Weight)
                for i in range(len(event.SigMuo_pt)):
                    pt_mu_SR["3L1T"].Fill(event.SigMuo_pt[i], Weight)
                for i in range(len(event.SigTau_pt)):
                    pt_tau_SR["3L1T"].Fill(event.SigTau_pt[i], Weight)
            if event.Is3L1TnoZ == 1:
                met_SR["3L1TnoZ"].Fill(event.Met, Weight)
                meff_SR["3L1TnoZ"].Fill(event.Meff, Weight)
                for i in range(len(event.SigEle_pt)):
                    pt_elec_SR["3L1TnoZ"].Fill(event.SigEle_pt[i], Weight)
                for i in range(len(event.SigMuo_pt)):
                    pt_mu_SR["3L1TnoZ"].Fill(event.SigMuo_pt[i], Weight)
                for i in range(len(event.SigTau_pt)):
                    pt_tau_SR["3L1TnoZ"].Fill(event.SigTau_pt[i], Weight)

            if event.Is3L1TZ == 1:
                met_SR["3L1TZ"].Fill(event.Met, Weight)
                meff_SR["3L1TZ"].Fill(event.Meff, Weight)
                for i in range(len(event.SigEle_pt)):
                    pt_elec_SR["3L1TZ"].Fill(event.SigEle_pt[i], Weight)
                for i in range(len(event.SigMuo_pt)):
                    pt_mu_SR["3L1TZ"].Fill(event.SigMuo_pt[i], Weight)
                for i in range(len(event.SigTau_pt)):
                    pt_tau_SR["3L1TZ"].Fill(event.SigTau_pt[i], Weight)

            if event.Is2L2T == 1:

                met_SR["2L2T"].Fill(event.Met, Weight)
                meff_SR["2L2T"].Fill(event.Meff, Weight)
                for i in range(len(event.SigEle_pt)):
                    pt_elec_SR["2L2T"].Fill(event.SigEle_pt[i], Weight)
                for i in range(len(event.SigMuo_pt)):
                    pt_mu_SR["2L2T"].Fill(event.SigMuo_pt[i], Weight)
                for i in range(len(event.SigTau_pt)):
                    pt_tau_SR["2L2T"].Fill(event.SigTau_pt[i], Weight)
                n_sig += 1

            if event.Is2L2TnoZ == 1:

                met_SR["2L2TnoZ"].Fill(event.Met, Weight)
                meff_SR["2L2TnoZ"].Fill(event.Meff, Weight)
                for i in range(len(event.SigEle_pt)):
                    pt_elec_SR["2L2TnoZ"].Fill(event.SigEle_pt[i], Weight)
                for i in range(len(event.SigMuo_pt)):
                    pt_mu_SR["2L2TnoZ"].Fill(event.SigMuo_pt[i], Weight)
                for i in range(len(event.SigTau_pt)):
                    pt_tau_SR["2L2TnoZ"].Fill(event.SigTau_pt[i], Weight)

            if event.Is2L2TZ == 1:

                met_SR["2L2TZ"].Fill(event.Met, Weight)
                meff_SR["2L2TZ"].Fill(event.Meff, Weight)
                for i in range(len(event.SigEle_pt)):
                    pt_elec_SR["2L2TZ"].Fill(event.SigEle_pt[i], Weight)
                for i in range(len(event.SigMuo_pt)):
                    pt_mu_SR["2L2TZ"].Fill(event.SigMuo_pt[i], Weight)
                for i in range(len(event.SigTau_pt)):
                    pt_tau_SR["2L2TZ"].Fill(event.SigTau_pt[i], Weight)

        if options.CreateVRHistos:
            if event.IsVR0:
                if isData:
                    VR0Histo.Fill(event.Meff, Weight)
                if not isData and event.IsRealVR0:
                    VR0Histo.Fill(event.Meff, Weight)

            if event.IsVR1:
                if isData:
                    VR1Histo.Fill(event.Meff, Weight)
                if not isData and event.IsRealVR1:
                    VR1Histo.Fill(event.Meff, Weight)

            if event.IsVR2:
                if isData:
                    VR2Histo.Fill(event.Meff, Weight)
                if not isData and event.IsRealVR2:
                    VR2Histo.Fill(event.Meff, Weight)

        # 3L CR1
        if not isData and options.CreateClosurePlots:
            if event.Is2L1l == 1:
                Fakeweights = CalculateFakefactorWeight(event, "2L2l", lightleptonnumber=1, taunumber=0)
                met_weighted["3L"].Fill(event.Met, Fakeweights["Nominal"] * Weight)
                meff_weighted["3L"].Fill(event.BaseMeff, Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseEle_pt)):
                    pt_elec_weighted["3L"].Fill(event.LooseEle_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseMuo_pt)):
                    pt_mu_weighted["3L"].Fill(event.LooseMuo_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseTau_pt)):
                    pt_tau_weighted["3L"].Fill(event.LooseTau_pt[i], Fakeweights["Nominal"] * Weight)

            if event.Is2L1lnoZ == 1:
                Fakeweights = CalculateFakefactorWeight(event, "2L2l", lightleptonnumber=1, taunumber=0)
                met_weighted["3LnoZ"].Fill(event.Met, Fakeweights["Nominal"] * Weight)
                meff_weighted["3LnoZ"].Fill(event.BaseMeff, Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseEle_pt)):
                    pt_elec_weighted["3LnoZ"].Fill(event.LooseEle_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseMuo_pt)):
                    pt_mu_weighted["3LnoZ"].Fill(event.LooseMuo_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseTau_pt)):
                    pt_tau_weighted["3LnoZ"].Fill(event.LooseTau_pt[i], Fakeweights["Nominal"] * Weight)

            if event.Is2L1lZ == 1:
                Fakeweights = CalculateFakefactorWeight(event, "2L2l", lightleptonnumber=1, taunumber=0)
                met_weighted["3LZ"].Fill(event.Met, Fakeweights["Nominal"] * Weight)
                meff_weighted["3LZ"].Fill(event.BaseMeff, Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseEle_pt)):
                    pt_elec_weighted["3LZ"].Fill(event.LooseEle_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseMuo_pt)):
                    pt_mu_weighted["3LZ"].Fill(event.LooseMuo_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseTau_pt)):
                    pt_tau_weighted["3LZ"].Fill(event.LooseTau_pt[i], Fakeweights["Nominal"] * Weight)
        # 3L CR2

            if event.Is1L2l == 1:
                Fakeweights = CalculateFakefactorWeight(event, "2L2l", lightleptonnumber=2, taunumber=0, isCR2=True)
                met_weighted["3L"].Fill(event.Met, Fakeweights["Nominal"] * Weight)
                meff_weighted["3L"].Fill(event.BaseMeff, Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseEle_pt)):
                    pt_elec_weighted["3L"].Fill(event.LooseEle_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseMuo_pt)):
                    pt_mu_weighted["3L"].Fill(event.LooseMuo_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseTau_pt)):
                    pt_tau_weighted["3L"].Fill(event.LooseTau_pt[i], Fakeweights["Nominal"] * Weight)

            if event.Is1L2lnoZ == 1:
                Fakeweights = CalculateFakefactorWeight(event, "2L2l", lightleptonnumber=2, taunumber=0, isCR2=True)
                met_weighted["3LnoZ"].Fill(event.Met, Fakeweights["Nominal"] * Weight)
                meff_weighted["3LnoZ"].Fill(event.BaseMeff, Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseEle_pt)):
                    pt_elec_weighted["3LnoZ"].Fill(event.LooseEle_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseMuo_pt)):
                    pt_mu_weighted["3LnoZ"].Fill(event.LooseMuo_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseTau_pt)):
                    pt_tau_weighted["3LnoZ"].Fill(event.LooseTau_pt[i], Fakeweights["Nominal"] * Weight)

            if event.Is1L2lZ == 1:
                Fakeweights = CalculateFakefactorWeight(event, "2L2l", lightleptonnumber=2, taunumber=0, isCR2=True)
                met_weighted["3LZ"].Fill(event.Met, Fakeweights["Nominal"] * Weight)
                meff_weighted["3LZ"].Fill(event.BaseMeff, Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseEle_pt)):
                    pt_elec_weighted["3LZ"].Fill(event.LooseEle_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseMuo_pt)):
                    pt_mu_weighted["3LZ"].Fill(event.LooseMuo_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseTau_pt)):
                    pt_tau_weighted["3LZ"].Fill(event.LooseTau_pt[i], Fakeweights["Nominal"] * Weight)

        # 4L CR1

        if event.Is3L1l == 1:
            Fakeweights = CalculateFakefactorWeight(event, "2L2l", lightleptonnumber=1, taunumber=0)
            if not isData and event.IsReal3L1l == 1:
                Fakeweights = InvertSign(Fakeweights)

            if not isData and options.CreateClosurePlots:
                met_weighted["4L"].Fill(event.Met, Fakeweights["Nominal"] * Weight)
                meff_weighted["4L"].Fill(event.BaseMeff, Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseEle_pt)):
                    pt_elec_weighted["4L"].Fill(event.LooseEle_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseMuo_pt)):
                    pt_mu_weighted["4L"].Fill(event.LooseMuo_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseTau_pt)):
                    pt_tau_weighted["4L"].Fill(event.LooseTau_pt[i], Fakeweights["Nominal"] * Weight)

        if not options.CreateClosurePlots:
            if event.Is3L1l_bveto == 1:
                Fakeweights = CalculateFakefactorWeight(event, "2L2l-bveto", lightleptonnumber=1, taunumber=0)
                if not isData and event.IsReal3L1l_bveto == 1:
                    Fakeweights = InvertSign(Fakeweights)

        if event.Is3L1lnoZ == 1:
            Fakeweights = CalculateFakefactorWeight(event, "2L2lnoZ", lightleptonnumber=1, taunumber=0)
            if not isData and event.IsReal3L1lnoZ == 1:

                Fakeweights = InvertSign(Fakeweights)
            if not isData and options.CreateClosurePlots:
                met_weighted["4LnoZ"].Fill(event.Met, Fakeweights["Nominal"] * Weight)
                meff_weighted["4LnoZ"].Fill(event.BaseMeff, Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseEle_pt)):
                    pt_elec_weighted["4LnoZ"].Fill(event.LooseEle_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseMuo_pt)):
                    pt_mu_weighted["4LnoZ"].Fill(event.LooseMuo_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseTau_pt)):
                    pt_tau_weighted["4LnoZ"].Fill(event.LooseTau_pt[i], Fakeweights["Nominal"] * Weight)

        if not options.CreateClosurePlots:
            if event.Is3L1lnoZ_bveto == 1:
                Fakeweights = CalculateFakefactorWeight(event, "2L2lnoZ-bveto", lightleptonnumber=1, taunumber=0)
                if not isData and event.IsReal3L1lnoZ_bveto == 1:
                    Fakeweights = InvertSign(Fakeweights)
                if options.CreateVRHistos:
                    if isData:
                        CR1_VR0Histo.Fill(event.BaseMeff, Fakeweights["Nominal"] * Weight)
                    if not isData and event.IsReal3L1lnoZ_bveto == 1:
                        CR1_VR0Histo.Fill(event.BaseMeff, Fakeweights["Nominal"] * Weight)

        if event.Is3L1lZ == 1:
            Fakeweights = CalculateFakefactorWeight(event, "2L2lZ", lightleptonnumber=1, taunumber=0)
            if not isData and event.IsReal3L1lZ == 1:
                Fakeweights = InvertSign(Fakeweights)
            if not isData and options.CreateClosurePlots:
                met_weighted["4LZ"].Fill(event.Met, Fakeweights["Nominal"] * Weight)
                meff_weighted["4LZ"].Fill(event.BaseMeff, Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseEle_pt)):
                    pt_elec_weighted["4LZ"].Fill(event.LooseEle_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseMuo_pt)):
                    pt_mu_weighted["4LZ"].Fill(event.LooseMuo_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseTau_pt)):
                    pt_tau_weighted["4LZ"].Fill(event.LooseTau_pt[i], Fakeweights["Nominal"] * Weight)

        if not options.CreateClosurePlots:
            if event.Is3L1lZ_bveto == 1:
                Fakeweights = CalculateFakefactorWeight(event, "2L2lZ-bveto", lightleptonnumber=1, taunumber=0)
                if not isData and event.IsReal3L1lZ_bveto == 1:
                    Fakeweights = InvertSign(Fakeweights)

        if event.Is3L1lZZ == 1:
            Fakeweights = CalculateFakefactorWeight(event, "2L2lZZ", lightleptonnumber=1, taunumber=0)
            if not isData and event.IsReal3L1lZ == 1:
                Fakeweights = InvertSign(Fakeweights)

        if not options.CreateClosurePlots:
            if event.Is3L1lZZ_bveto == 1:
                Fakeweights = CalculateFakefactorWeight(event, "2L2lZZ-bveto", lightleptonnumber=1, taunumber=0)
                if not isData and event.IsReal3L1lZZ_bveto == 1:
                    Fakeweights = InvertSign(Fakeweights)

        if event.Is3L1l_CR_ZZ == 1:
            Fakeweights = CalculateFakefactorWeight(event, "2L2l_CR_ZZ", lightleptonnumber=1, taunumber=0)
            if not isData and event.IsReal3L1l_CR_ZZ == 1:
                Fakeweights = InvertSign(Fakeweights)

        if event.Is3L1l_CR_ttZ == 1:
            Fakeweights = CalculateFakefactorWeight(event, "2L2l_CR_ttZ", lightleptonnumber=1, taunumber=0)
            if not isData and event.IsReal3L1l_CR_ttZ == 1:
                Fakeweights = InvertSign(Fakeweights)

        # 4L CR2
        if event.Is2L2l == 1:
            Fakeweights = CalculateFakefactorWeight(event, "2L2l", lightleptonnumber=2, taunumber=0, isCR2=True)
            if not isData and event.IsReal2L2l == 1:
                Fakeweights = InvertSign(Fakeweights)
            if not isData and options.CreateClosurePlots:
                met_weighted["4L"].Fill(event.Met, Fakeweights["Nominal"] * Weight)
                meff_weighted["4L"].Fill(event.BaseMeff, Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseEle_pt)):
                    pt_elec_weighted["4L"].Fill(event.LooseEle_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseMuo_pt)):
                    pt_mu_weighted["4L"].Fill(event.LooseMuo_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseTau_pt)):
                    pt_tau_weighted["4L"].Fill(event.LooseTau_pt[i], Fakeweights["Nominal"] * Weight)
        if not options.CreateClosurePlots:
            if event.Is2L2l_bveto == 1:
                Fakeweights = CalculateFakefactorWeight(event, "2L2l-bveto", lightleptonnumber=2, taunumber=0, isCR2=True)
                if not isData and event.IsReal2L2l_bveto == 1:
                    Fakeweights = InvertSign(Fakeweights)
        if event.Is2L2lnoZ == 1:
            Fakeweights = CalculateFakefactorWeight(event, "2L2lnoZ", lightleptonnumber=2, taunumber=0, isCR2=True)
            if not isData and event.IsReal2L2lnoZ == 1:
                Fakeweights = InvertSign(Fakeweights)
            if not isData and options.CreateClosurePlots:
                met_weighted["4LnoZ"].Fill(event.Met, Fakeweights["Nominal"] * Weight)
                meff_weighted["4LnoZ"].Fill(event.BaseMeff, Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseEle_pt)):
                    pt_elec_weighted["4LnoZ"].Fill(event.LooseEle_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseMuo_pt)):
                    pt_mu_weighted["4LnoZ"].Fill(event.LooseMuo_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseTau_pt)):
                    pt_tau_weighted["4LnoZ"].Fill(event.LooseTau_pt[i], Fakeweights["Nominal"] * Weight)
        if not options.CreateClosurePlots:
            if event.Is2L2lnoZ_bveto == 1:
                Fakeweights = CalculateFakefactorWeight(event, "2L2lnoZ-bveto", lightleptonnumber=2, taunumber=0, isCR2=True)
                if not isData and event.IsReal2L2lnoZ_bveto == 1:
                    Fakeweights = InvertSign(Fakeweights)
                if options.CreateVRHistos:
                    if isData:
                        CR2_VR0Histo.Fill(event.BaseMeff, Fakeweights["Nominal"] * Weight)
                    if not isData and event.IsReal2L2lnoZ_bveto == 1:
                        CR2_VR0Histo.Fill(event.BaseMeff, Fakeweights["Nominal"] * Weight)

        if event.Is2L2lZ == 1:
            Fakeweights = CalculateFakefactorWeight(event, "2L2lZ", lightleptonnumber=2, taunumber=0, isCR2=True)
            if not isData and event.IsReal2L2lZ == 1:
                Fakeweights = InvertSign(Fakeweights)
            if not isData and options.CreateClosurePlots:
                met_weighted["4LZ"].Fill(event.Met, Fakeweights["Nominal"] * Weight)
                meff_weighted["4LZ"].Fill(event.BaseMeff, Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseEle_pt)):
                    pt_elec_weighted["4LZ"].Fill(event.LooseEle_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseMuo_pt)):
                    pt_mu_weighted["4LZ"].Fill(event.LooseMuo_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseTau_pt)):
                    pt_tau_weighted["4LZ"].Fill(event.LooseTau_pt[i], Fakeweights["Nominal"] * Weight)
        if not options.CreateClosurePlots:
            if event.Is2L2lZ_bveto == 1:
                Fakeweights = CalculateFakefactorWeight(event, "2L2lZ-bveto", lightleptonnumber=2, taunumber=0, isCR2=True)
                if not isData and event.IsReal2L2lZ_bveto == 1:
                    Fakeweights = InvertSign(Fakeweights)
        if event.Is2L2lZZ == 1:
            Fakeweights = CalculateFakefactorWeight(event, "2L2lZZ", lightleptonnumber=2, taunumber=0, isCR2=True)
            if not isData and event.IsReal2L2lZ == 1:
                Fakeweights = InvertSign(Fakeweights)
        if not options.CreateClosurePlots:
            if event.Is2L2lZZ_bveto == 1:
                Fakeweights = CalculateFakefactorWeight(event, "2L2lZZ-bveto", lightleptonnumber=2, taunumber=0, isCR2=True)
                if not isData and event.IsReal2L2lZ_bveto == 1:
                    Fakeweights = InvertSign(Fakeweights)

        if event.Is2L2l_CR_ZZ == 1:
            Fakeweights = CalculateFakefactorWeight(event, "2L2l_CR_ZZ", lightleptonnumber=2, taunumber=0, isCR2=True)
            if not isData and event.IsReal2L2l_CR_ZZ == 1:
                Fakeweights = InvertSign(Fakeweights)

        if event.Is2L2l_CR_ttZ == 1:
            Fakeweights = CalculateFakefactorWeight(event, "2L2l_CR_ttZ", lightleptonnumber=2, taunumber=0, isCR2=True)
            if not isData and event.IsReal2L2l_CR_ttZ == 1:
                Fakeweights = InvertSign(Fakeweights)

        # 3L1T CR1
        if event.Is2L1T1l == 1:
            Fakeweights = CalculateFakefactorWeight(event, "2L1l1t", lightleptonnumber=1, taunumber=0)
            if not isData and event.IsReal2L1T1l == 1:
                Fakeweights = InvertSign(Fakeweights)
            if not isData and options.CreateClosurePlots:
                met_weighted["3L1T"].Fill(event.Met, Fakeweights["Nominal"] * Weight)
                meff_weighted["3L1T"].Fill(event.BaseMeff, Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseEle_pt)):
                    pt_elec_weighted["3L1T"].Fill(event.LooseEle_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseMuo_pt)):
                    pt_mu_weighted["3L1T"].Fill(event.LooseMuo_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseTau_pt)):
                    pt_tau_weighted["3L1T"].Fill(event.LooseTau_pt[i], Fakeweights["Nominal"] * Weight)
        if not options.CreateClosurePlots:
            if event.Is2L1T1l_bveto == 1:
                Fakeweights = CalculateFakefactorWeight(event, "2L1l1t-bveto", lightleptonnumber=1, taunumber=0)
                if not isData and event.IsReal2L1T1l_bveto == 1:
                    Fakeweights = InvertSign(Fakeweights)

        if event.Is2L1T1lnoZ == 1:
            Fakeweights = CalculateFakefactorWeight(event, "2L1l1tnoZ", lightleptonnumber=1, taunumber=0)
            if not isData and event.IsReal2L1T1lnoZ == 1:
                Fakeweights = InvertSign(Fakeweights)
            if not isData and options.CreateClosurePlots:
                met_weighted["3L1TnoZ"].Fill(event.Met, Fakeweights["Nominal"] * Weight)
                meff_weighted["3L1TnoZ"].Fill(event.BaseMeff, Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseEle_pt)):
                    pt_elec_weighted["3L1TnoZ"].Fill(event.LooseEle_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseMuo_pt)):
                    pt_mu_weighted["3L1TnoZ"].Fill(event.LooseMuo_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseTau_pt)):
                    pt_tau_weighted["3L1TnoZ"].Fill(event.LooseTau_pt[i], Fakeweights["Nominal"] * Weight)
        if not options.CreateClosurePlots:
            if event.Is2L1T1lnoZ_bveto == 1:
                Fakeweights = CalculateFakefactorWeight(event, "2L1l1tnoZ-bveto", lightleptonnumber=1, taunumber=0)
                if not isData and event.IsReal2L1T1lnoZ_bveto == 1:
                    Fakeweights = InvertSign(Fakeweights)
                if options.CreateVRHistos:
                    if isData:
                        CR1_VR1Histo.Fill(event.BaseMeff, Fakeweights["Nominal"] * Weight)
                    if not isData and event.IsReal2L1T1lnoZ_bveto == 1:
                        CR1_VR1Histo.Fill(event.BaseMeff, Fakeweights["Nominal"] * Weight)

        if event.Is2L1T1lZ == 1:
            Fakeweights = CalculateFakefactorWeight(event, "2L1l1tZ", lightleptonnumber=1, taunumber=0)
            if not isData and event.IsReal2L1T1lZ == 1:
                Fakeweights = InvertSign(Fakeweights)
            if not isData and options.CreateClosurePlots:
                met_weighted["3L1TZ"].Fill(event.Met, Fakeweights["Nominal"] * Weight)
                meff_weighted["3L1TZ"].Fill(event.BaseMeff, Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseEle_pt)):
                    pt_elec_weighted["3L1TZ"].Fill(event.LooseEle_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseMuo_pt)):
                    pt_mu_weighted["3L1TZ"].Fill(event.LooseMuo_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseTau_pt)):
                    pt_tau_weighted["3L1TZ"].Fill(event.LooseTau_pt[i], Fakeweights["Nominal"] * Weight)
        if not options.CreateClosurePlots:
            if event.Is2L1T1lZ_bveto == 1:
                Fakeweights = CalculateFakefactorWeight(event, "2L1l1tZ-bveto", lightleptonnumber=1, taunumber=0)
                if not isData and event.IsReal2L1T1lZ_bveto == 1:
                    Fakeweights = InvertSign(Fakeweights)

        if event.Is3L1t == 1:
            Fakeweights = CalculateFakefactorWeight(event, "2L1l1t", lightleptonnumber=0, taunumber=1)
            if not isData and event.IsReal3L1t == 1:
                Fakeweights = InvertSign(Fakeweights)
            if not isData and options.CreateClosurePlots:
                met_weighted["3L1T"].Fill(event.Met, Fakeweights["Nominal"] * Weight)
                meff_weighted["3L1T"].Fill(event.BaseMeff, Fakeweights["Nominal"] * Weight)
        if not options.CreateClosurePlots:
            if event.Is3L1t_bveto == 1:
                Fakeweights = CalculateFakefactorWeight(event, "2L1l1t-bveto", lightleptonnumber=0, taunumber=1)
                if not isData and event.IsReal3L1t_bveto == 1:
                    Fakeweights = InvertSign(Fakeweights)
        if event.Is3L1tnoZ == 1:
            Fakeweights = CalculateFakefactorWeight(event, "2L1l1tnoZ", lightleptonnumber=0, taunumber=1)
            if not isData and event.IsReal3L1tnoZ == 1:
                Fakeweights = InvertSign(Fakeweights)
            if not isData and options.CreateClosurePlots:
                met_weighted["3L1TnoZ"].Fill(event.Met, Fakeweights["Nominal"] * Weight)
                meff_weighted["3L1TnoZ"].Fill(event.BaseMeff, Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseEle_pt)):
                    pt_elec_weighted["3L1TnoZ"].Fill(event.LooseEle_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseMuo_pt)):
                    pt_mu_weighted["3L1TnoZ"].Fill(event.LooseMuo_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseTau_pt)):
                    pt_tau_weighted["3L1TnoZ"].Fill(event.LooseTau_pt[i], Fakeweights["Nominal"] * Weight)
        if not options.CreateClosurePlots:
            if event.Is3L1tnoZ_bveto == 1:
                Fakeweights = CalculateFakefactorWeight(event, "2L1l1tnoZ-bveto", lightleptonnumber=0, taunumber=1)
                if not isData and event.IsReal3L1tnoZ_bveto == 1:
                    Fakeweights = InvertSign(Fakeweights)
                if options.CreateVRHistos:
                    if isData:
                        CR1_VR1Histo.Fill(event.BaseMeff, Fakeweights["Nominal"] * Weight)
                    if not isData and event.IsReal3L1tnoZ_bveto == 1:
                        CR1_VR1Histo.Fill(event.BaseMeff, Fakeweights["Nominal"] * Weight)
        if event.Is3L1tZ == 1:
            Fakeweights = CalculateFakefactorWeight(event, "2L1l1tZ", lightleptonnumber=0, taunumber=1)
            if not isData and event.IsReal3L1tZ == 1:
                Fakeweights = InvertSign(Fakeweights)
            if not isData and options.CreateClosurePlots:
                met_weighted["3L1TZ"].Fill(event.Met, Fakeweights["Nominal"] * Weight)
                meff_weighted["3L1TZ"].Fill(event.BaseMeff, Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseEle_pt)):
                    pt_elec_weighted["3L1TZ"].Fill(event.LooseEle_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseMuo_pt)):
                    pt_mu_weighted["3L1TZ"].Fill(event.LooseMuo_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseTau_pt)):
                    pt_tau_weighted["3L1TZ"].Fill(event.LooseTau_pt[i], Fakeweights["Nominal"] * Weight)
        if not options.CreateClosurePlots:
            if event.Is3L1tZ_bveto == 1:
                Fakeweights = CalculateFakefactorWeight(event, "2L1l1tZ-bveto", lightleptonnumber=0, taunumber=1)
                if not isData and event.IsReal3L1tZ_bveto == 1:
                    Fakeweights = InvertSign(Fakeweights)

        # 3L1T CR2

        if event.Is2L1l1t == 1:
            Fakeweights = CalculateFakefactorWeight(event, "2L1l1t", lightleptonnumber=1, taunumber=1, isCR2=True)
            if not isData and event.IsReal2L1l1t == 1:
                Fakeweights = InvertSign(Fakeweights)
            if not isData and options.CreateClosurePlots:
                met_weighted["3L1T"].Fill(event.Met, Fakeweights["Nominal"] * Weight)
                meff_weighted["3L1T"].Fill(event.BaseMeff, Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseEle_pt)):
                    pt_elec_weighted["3L1T"].Fill(event.LooseEle_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseMuo_pt)):
                    pt_mu_weighted["3L1T"].Fill(event.LooseMuo_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseTau_pt)):
                    pt_tau_weighted["3L1T"].Fill(event.LooseTau_pt[i], Fakeweights["Nominal"] * Weight)
        if not options.CreateClosurePlots:
            if event.Is2L1l1t_bveto == 1:
                Fakeweights = CalculateFakefactorWeight(event, "2L1l1t-bveto", lightleptonnumber=1, taunumber=1, isCR2=True)
                if not isData and event.IsReal2L1l1t_bveto == 1:
                    Fakeweights = InvertSign(Fakeweights)
        if event.Is2L1l1tnoZ == 1:
            Fakeweights = CalculateFakefactorWeight(event, "2L1l1tnoZ", lightleptonnumber=1, taunumber=1, isCR2=True)
            if not isData and event.IsReal2L1l1tnoZ == 1:
                Fakeweights = InvertSign(Fakeweights)
            if not isData and options.CreateClosurePlots:
                met_weighted["3L1TnoZ"].Fill(event.Met, Fakeweights["Nominal"] * Weight)
                meff_weighted["3L1TnoZ"].Fill(event.BaseMeff, Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseEle_pt)):
                    pt_elec_weighted["3L1TnoZ"].Fill(event.LooseEle_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseMuo_pt)):
                    pt_mu_weighted["3L1TnoZ"].Fill(event.LooseMuo_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseTau_pt)):
                    pt_tau_weighted["3L1TnoZ"].Fill(event.LooseTau_pt[i], Fakeweights["Nominal"] * Weight)
        if not options.CreateClosurePlots:
            if event.Is2L1l1tnoZ_bveto == 1:
                Fakeweights = CalculateFakefactorWeight(event, "2L1l1tnoZ-bveto", lightleptonnumber=1, taunumber=1, isCR2=True)
                if not isData and event.IsReal2L1l1tnoZ_bveto == 1:
                    Fakeweights = InvertSign(Fakeweights)
                if options.CreateVRHistos:
                    if isData:
                        CR2_VR1Histo.Fill(event.BaseMeff, Fakeweights["Nominal"] * Weight)
                    if not isData and event.IsReal2L1l1tnoZ_bveto == 1:
                        CR2_VR1Histo.Fill(event.BaseMeff, Fakeweights["Nominal"] * Weight)

        if event.Is2L1l1tZ == 1:
            Fakeweights = CalculateFakefactorWeight(event, "2L1l1tZ", lightleptonnumber=1, taunumber=1, isCR2=True)
            if not isData and event.IsReal2L1l1tZ == 1:
                Fakeweights = InvertSign(Fakeweights)
            if not isData and options.CreateClosurePlots:
                met_weighted["3L1TZ"].Fill(event.Met, Fakeweights["Nominal"] * Weight)
                meff_weighted["3L1TZ"].Fill(event.BaseMeff, Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseEle_pt)):
                    pt_elec_weighted["3L1TZ"].Fill(event.LooseEle_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseMuo_pt)):
                    pt_mu_weighted["3L1TZ"].Fill(event.LooseMuo_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseTau_pt)):
                    pt_tau_weighted["3L1TZ"].Fill(event.LooseTau_pt[i], Fakeweights["Nominal"] * Weight)
        if not options.CreateClosurePlots:
            if event.Is2L1l1tZ_bveto == 1:
                Fakeweights = CalculateFakefactorWeight(event, "2L1l1tZ-bveto", lightleptonnumber=1, taunumber=1, isCR2=True)
                if not isData and event.IsReal2L1l1tZ_bveto == 1:
                    Fakeweights = InvertSign(Fakeweights)

        # 2L2T CR1

        if event.Is2L1T1t == 1:
            Fakeweights = CalculateFakefactorWeight(event, "2L2t", lightleptonnumber=0, taunumber=1)
            if not isData and event.IsReal2L1T1t == 1:
                Fakeweights = InvertSign(Fakeweights)
            if not isData and options.CreateClosurePlots:
                met_weighted["2L2T"].Fill(event.Met, Fakeweights["Nominal"] * Weight)
                meff_weighted["2L2T"].Fill(event.BaseMeff, Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseEle_pt)):
                    pt_elec_weighted["2L2T"].Fill(event.LooseEle_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseMuo_pt)):
                    pt_mu_weighted["2L2T"].Fill(event.LooseMuo_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseTau_pt)):
                    pt_tau_weighted["2L2T"].Fill(event.LooseTau_pt[i], Fakeweights["Nominal"] * Weight)
                n_weighted += Fakeweights["Nominal"]
                FF_CR1 += Fakeweights["Nominal"]
                n_CR1 += 1
        if not options.CreateClosurePlots:
            if event.Is2L1T1t_bveto == 1:
                Fakeweights = CalculateFakefactorWeight(event, "2L2t-bveto", lightleptonnumber=0, taunumber=1)
                if not isData and event.IsReal2L1T1t_bveto == 1:
                    Fakeweights = InvertSign(Fakeweights)
        if event.Is2L1T1tnoZ == 1:
            Fakeweights = CalculateFakefactorWeight(event, "2L2tnoZ", lightleptonnumber=0, taunumber=1)
            if not isData and event.IsReal2L1T1tnoZ == 1:
                Fakeweights = InvertSign(Fakeweights)
            if not isData and options.CreateClosurePlots:
                met_weighted["2L2TnoZ"].Fill(event.Met, Fakeweights["Nominal"] * Weight)
                meff_weighted["2L2TnoZ"].Fill(event.BaseMeff, Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseEle_pt)):
                    pt_elec_weighted["2L2TnoZ"].Fill(event.LooseEle_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseMuo_pt)):
                    pt_mu_weighted["2L2TnoZ"].Fill(event.LooseMuo_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseTau_pt)):
                    pt_tau_weighted["2L2TnoZ"].Fill(event.LooseTau_pt[i], Fakeweights["Nominal"] * Weight)
        if not options.CreateClosurePlots:
            if event.Is2L1T1tnoZ_bveto == 1:
                Fakeweights = CalculateFakefactorWeight(event, "2L2tnoZ-bveto", lightleptonnumber=0, taunumber=1)
                if not isData and event.IsReal2L1T1tnoZ_bveto == 1:
                    Fakeweights = InvertSign(Fakeweights)
                if options.CreateVRHistos:
                    if isData:
                        CR1_VR2Histo.Fill(event.BaseMeff, Fakeweights["Nominal"] * Weight)
                    if not isData and event.IsReal2L1T1tnoZ_bveto == 1:
                        CR1_VR2Histo.Fill(event.BaseMeff, Fakeweights["Nominal"] * Weight)

        if event.Is2L1T1tZ == 1:
            Fakeweights = CalculateFakefactorWeight(event, "2L2tZ", lightleptonnumber=0, taunumber=1)
            if not isData and event.IsReal2L1T1tZ == 1:
                Fakeweights = InvertSign(Fakeweights)
            if not isData and options.CreateClosurePlots:
                met_weighted["2L2TZ"].Fill(event.Met, Fakeweights["Nominal"] * Weight)
                meff_weighted["2L2TZ"].Fill(event.BaseMeff, Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseEle_pt)):
                    pt_elec_weighted["2L2TZ"].Fill(event.LooseEle_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseMuo_pt)):
                    pt_mu_weighted["2L2TZ"].Fill(event.LooseMuo_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseTau_pt)):
                    pt_tau_weighted["2L2TZ"].Fill(event.LooseTau_pt[i], Fakeweights["Nominal"] * Weight)
        if not options.CreateClosurePlots:
            if event.Is2L1T1tZ_bveto == 1:
                Fakeweights = CalculateFakefactorWeight(event, "2L2tZ-bveto", lightleptonnumber=0, taunumber=1)
                if not isData and event.IsReal2L1T1tZ_bveto == 1:
                    Fakeweights = InvertSign(Fakeweights)

        # 2L2T CR2

        if event.Is2L2t == 1:
            Fakeweights = CalculateFakefactorWeight(event, "2L2t", lightleptonnumber=0, taunumber=2, isCR2=True)
            if not isData and event.IsReal2L2t == 1:
                Fakeweights = InvertSign(Fakeweights)
            if not isData and options.CreateClosurePlots:
                met_weighted["2L2T"].Fill(event.Met, Fakeweights["Nominal"] * Weight)
                meff_weighted["2L2T"].Fill(event.BaseMeff, Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseEle_pt)):
                    pt_elec_weighted["2L2T"].Fill(event.LooseEle_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseMuo_pt)):
                    pt_mu_weighted["2L2T"].Fill(event.LooseMuo_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseTau_pt)):
                    pt_tau_weighted["2L2T"].Fill(event.LooseTau_pt[i], Fakeweights["Nominal"] * Weight)
                n_weighted += Fakeweights["Nominal"]
                FF_CR2 += Fakeweights["Nominal"]
                n_CR2 += 1
        if not options.CreateClosurePlots:
            if event.Is2L2t_bveto == 1:
                Fakeweights = CalculateFakefactorWeight(event, "2L2t-bveto", lightleptonnumber=0, taunumber=2, isCR2=True)
                if not isData and event.IsReal2L2t_bveto == 1:
                    Fakeweights = InvertSign(Fakeweights)
        if event.Is2L2tnoZ == 1:
            Fakeweights = CalculateFakefactorWeight(event, "2L2tnoZ", lightleptonnumber=0, taunumber=2, isCR2=True)
            if not isData and event.IsReal2L2tnoZ == 1:
                Fakeweights = InvertSign(Fakeweights)
            if not isData and options.CreateClosurePlots:
                met_weighted["2L2TnoZ"].Fill(event.Met, Fakeweights["Nominal"] * Weight)
                meff_weighted["2L2TnoZ"].Fill(event.BaseMeff, Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseEle_pt)):
                    pt_elec_weighted["2L2TnoZ"].Fill(event.LooseEle_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseMuo_pt)):
                    pt_mu_weighted["2L2TnoZ"].Fill(event.LooseMuo_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseTau_pt)):
                    pt_tau_weighted["2L2TnoZ"].Fill(event.LooseTau_pt[i], Fakeweights["Nominal"] * Weight)
        if not options.CreateClosurePlots:
            if event.Is2L2tnoZ_bveto == 1:
                Fakeweights = CalculateFakefactorWeight(event, "2L2tnoZ-bveto", lightleptonnumber=0, taunumber=2, isCR2=True)
                if not isData and event.IsReal2L2tnoZ_bveto == 1:
                    Fakeweights = InvertSign(Fakeweights)
                if options.CreateVRHistos:
                    if isData:
                        CR2_VR2Histo.Fill(event.BaseMeff, Fakeweights["Nominal"] * Weight)
                    if not isData and event.IsReal2L2tnoZ_bveto == 1:
                        CR2_VR2Histo.Fill(event.BaseMeff, Fakeweights["Nominal"] * Weight)

        if event.Is2L2tZ == 1:
            Fakeweights = CalculateFakefactorWeight(event, "2L2tZ", lightleptonnumber=0, taunumber=2, isCR2=True)
            if not isData and event.IsReal2L2tZ == 1:
                Fakeweights = InvertSign(Fakeweights)
            if not isData and options.CreateClosurePlots:
                met_weighted["2L2TZ"].Fill(event.Met, Fakeweights["Nominal"] * Weight)
                meff_weighted["2L2TZ"].Fill(event.BaseMeff, Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseEle_pt)):
                    pt_elec_weighted["2L2TZ"].Fill(event.LooseEle_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseMuo_pt)):
                    pt_mu_weighted["2L2TZ"].Fill(event.LooseMuo_pt[i], Fakeweights["Nominal"] * Weight)
                for i in range(len(event.LooseTau_pt)):
                    pt_tau_weighted["2L2TZ"].Fill(event.LooseTau_pt[i], Fakeweights["Nominal"] * Weight)
        if not options.CreateClosurePlots:
            if event.Is2L2tZ_bveto == 1:
                Fakeweights = CalculateFakefactorWeight(event, "2L2tZ-bveto", lightleptonnumber=0, taunumber=2, isCR2=True)
                if not isData and event.IsReal2L2tZ_bveto == 1:
                    Fakeweights = InvertSign(Fakeweights)
        if not options.CreateClosurePlots:
            FakeFactorarray[0] = Fakeweights["Nominal"]
            FakeFactorarray_FF_up[0] = Fakeweights["FF_up"]
            FakeFactorarray_FF_down[0] = Fakeweights["FF_down"]
            FakeFactorarray_PF_up[0] = Fakeweights["PF_up"]
            FakeFactorarray_PF_down[0] = Fakeweights["PF_down"]
            FakeFactorarray_SF_tau_LF_up[0] = Fakeweights["SF_Tau_LF_up"]
            FakeFactorarray_SF_tau_LF_down[0] = Fakeweights["SF_Tau_LF_down"]
            FakeFactorarray_SF_tau_HF_up[0] = Fakeweights["SF_Tau_HF_up"]
            FakeFactorarray_SF_tau_HF_down[0] = Fakeweights["SF_Tau_HF_down"]
            FakeFactorarray_SF_electron_HF_up[0] = Fakeweights["SF_Electron_HF_up"]
            FakeFactorarray_SF_electron_HF_down[0] = Fakeweights["SF_Electron_HF_down"]
            FakeFactorarray_SF_muon_HF_up[0] = Fakeweights["SF_Muon_HF_up"]
            FakeFactorarray_SF_muon_HF_down[0] = Fakeweights["SF_Muon_HF_down"]

            branch.Fill()
            branch_FF_up.Fill()
            branch_FF_down.Fill()
            branch_PF_up.Fill()
            branch_PF_down.Fill()
            branch_SF_tau_LF_up.Fill()
            branch_SF_tau_LF_down.Fill()
            branch_SF_tau_HF_up.Fill()
            branch_SF_tau_HF_down.Fill()
            branch_SF_electron_HF_up.Fill()
            branch_SF_electron_HF_down.Fill()
            branch_SF_muon_HF_up.Fill()
            branch_SF_muon_HF_down.Fill()

        Nevents += 1
    print "finished loop over events"
    if not options.CreateClosurePlots:
        newTree.Write()
        print "Created File: {}".format(options.inputFile.split("/")[-1])

    if options.CreateVRHistos:
        CR1_VR0Histo.GetXaxis().SetTitle("m_{eff}")
        CR2_VR0Histo.GetXaxis().SetTitle("m_{eff}")
        CR1_VR1Histo.GetXaxis().SetTitle("m_{eff}")
        CR2_VR1Histo.GetXaxis().SetTitle("m_{eff}")
        CR1_VR2Histo.GetXaxis().SetTitle("m_{eff}")
        CR2_VR2Histo.GetXaxis().SetTitle("m_{eff}")
        VR0Histo.GetXaxis().SetTitle("m_{eff}")
        VR1Histo.GetXaxis().SetTitle("m_{eff}")
        VR2Histo.GetXaxis().SetTitle("m_{eff}")
        TestHistoFile = ROOT.TFile("{}/{}".format(options.outDirVRHistos, options.inputFile.split("/")[-1]), "RECREATE")
        TestHistoFile.mkdir("Fourlepton")
        folder = TestHistoFile.Get("Fourlepton")
        folder.mkdir("Validationhistos")
        directory = TestHistoFile.GetDirectory("Fourlepton/Validationhistos")
        directory.WriteObject(CR1_VR0Histo, "CR1_VR0")
        directory.WriteObject(CR2_VR0Histo, "CR2_VR0")
        directory.WriteObject(CR1_VR1Histo, "CR1_VR1")
        directory.WriteObject(CR2_VR1Histo, "CR2_VR1")
        directory.WriteObject(CR1_VR2Histo, "CR1_VR2")
        directory.WriteObject(CR2_VR2Histo, "CR2_VR2")
        directory.WriteObject(VR0Histo, "VR0")
        directory.WriteObject(VR1Histo, "VR1")
        directory.WriteObject(VR2Histo, "VR2")

    if not isData and options.CreateClosurePlots:
        dummycanvas = ROOT.TCanvas("AllClosurePlots", "", 1200, 800)
        dummycanvas.SaveAs("%s/AllClosurePlots.pdf[" % (options.outDir))
        print "N_weighted: ", n_weighted
        print "N_SR: ", n_sig
        print "N_CR1: ", n_CR1
        print "N_CR2: ", n_CR2
        print "average F CR1: ", FF_CR1 / n_CR1
        print "average F CR2: ", FF_CR2 / n_CR2
        ROOT.gStyle.SetOptStat(0)
        for region in met_weighted:
            canvas = ROOT.TCanvas("Closure_met_%s" % (region), "", 1200, 800)
            Pad1 = ROOT.TPad("p1_%s" % (region), "", 0.0, 0.37, 1.0, 1.0)
            Pad2 = ROOT.TPad("p2_%s" % (region), "", 0.0, 0.0, 1.0, 0.37)
            Pad1.SetBottomMargin(0.03)  # set to 0 for space between top and bottom pad
            Pad1.SetTopMargin(0.09)
            Pad1.Draw()
            Pad2.SetTopMargin(0.01)  # set to 0 for space between top and bottom pad
            Pad2.SetBottomMargin(0.35)
            Pad2.SetGridy()
            Pad2.Draw()
            Pad1.cd()
            # ~ print "p value: ", met_SR[region].Chi2Test(met_weighted[region], "WW")
            SRmax = met_SR[region].GetMaximum()
            CRmax = met_weighted[region].GetMaximum()
            met_SR[region].SetMaximum(max(SRmax, CRmax) * 1.5)

            met_SR[region].Draw("E")
            met_weighted[region].SetMarkerColor(ROOT.kRed)
            met_weighted[region].SetLineColor(ROOT.kRed)
            met_weighted[region].Draw("E same")
            Legend = ROOT.TLegend(0.06, 0.9, 0.7, 1.0)
            Legend.SetFillStyle(0)
            Legend.SetBorderSize(0)
            Legend.SetTextSize(0.04)
            Legend.AddEntry(met_weighted[region], "CR1-CR2, weighted", "L")
            Legend.AddEntry(met_SR[region], "SR not weighted", "L")
            Legend.Draw()
            Pad2.cd()
            RatioHisto = met_weighted[region].Clone("%s_Ratio" % (region))
            RatioHisto.Divide(met_SR[region])
            RatioHisto.SetMaximum(2)
            RatioHisto.SetMinimum(0)
            RatioHisto.GetXaxis().SetTitle("E_{T}^{miss} [GeV]")
            RatioHisto.Draw("E")
            canvas.Update()
            canvas.SaveAs("%s/Closure_met_%s.pdf" % (options.outDir, region))
            canvas.SaveAs("%s/AllClosurePlots.pdf" % (options.outDir))

        for region in meff_weighted:
            canvas = ROOT.TCanvas("Closure_meff_%s" % (region), "", 1200, 800)
            Pad1 = ROOT.TPad("p1_%s" % (region), "", 0.0, 0.37, 1.0, 1.0)
            Pad2 = ROOT.TPad("p2_%s" % (region), "", 0.0, 0.0, 1.0, 0.37)
            Pad1.SetBottomMargin(0.03)  # set to 0 for space between top and bottom pad
            Pad1.SetTopMargin(0.09)
            Pad1.Draw()
            Pad2.SetTopMargin(0.01)  # set to 0 for space between top and bottom pad
            Pad2.SetBottomMargin(0.35)
            Pad2.SetGridy()
            Pad2.Draw()
            Pad1.cd()
            # ~ print "p value: ", met_SR[region].Chi2Test(met_weighted[region], "WW")
            SRmax = meff_SR[region].GetMaximum()
            CRmax = meff_weighted[region].GetMaximum()
            meff_SR[region].SetMaximum(max(SRmax, CRmax) * 1.5)

            meff_SR[region].Draw("E")
            meff_weighted[region].SetMarkerColor(ROOT.kRed)
            meff_weighted[region].SetLineColor(ROOT.kRed)
            meff_weighted[region].Draw("E same")
            Legend = ROOT.TLegend(0.06, 0.9, 0.7, 1.0)
            Legend.SetFillStyle(0)
            Legend.SetBorderSize(0)
            Legend.SetTextSize(0.04)
            Legend.AddEntry(meff_weighted[region], "CR1-CR2, weighted", "L")
            Legend.AddEntry(meff_SR[region], "SR not weighted", "L")
            Legend.Draw()
            Pad2.cd()
            RatioHisto = meff_weighted[region].Clone("%s_Ratio" % (region))
            RatioHisto.Divide(meff_SR[region])
            RatioHisto.SetMaximum(2)
            RatioHisto.SetMinimum(0)
            RatioHisto.GetXaxis().SetTitle("m_{eff} [GeV]")
            RatioHisto.Draw("E")
            canvas.Update()
            canvas.SaveAs("%s/Closure_meff_%s.pdf" % (options.outDir, region))
            canvas.SaveAs("%s/AllClosurePlots.pdf" % (options.outDir))

        for region in pt_elec_weighted:
            canvas = ROOT.TCanvas("Closure_elec_pt_%s" % (region), "", 1200, 800)
            Pad1 = ROOT.TPad("p1_%s" % (region), "", 0.0, 0.37, 1.0, 1.0)
            Pad2 = ROOT.TPad("p2_%s" % (region), "", 0.0, 0.0, 1.0, 0.37)
            Pad1.SetBottomMargin(0.03)  # set to 0 for space between top and bottom pad
            Pad1.SetTopMargin(0.09)
            Pad1.Draw()
            Pad2.SetTopMargin(0.01)  # set to 0 for space between top and bottom pad
            Pad2.SetBottomMargin(0.35)
            Pad2.SetGridy()
            Pad2.Draw()
            Pad1.cd()
            # ~ print "p value: ", met_SR[region].Chi2Test(met_weighted[region], "WW")
            SRmax = pt_elec_SR[region].GetMaximum()
            CRmax = pt_elec_weighted[region].GetMaximum()
            pt_elec_SR[region].SetMaximum(max(SRmax, CRmax) * 1.5)

            pt_elec_SR[region].Draw("E")
            pt_elec_weighted[region].SetMarkerColor(ROOT.kRed)
            pt_elec_weighted[region].SetLineColor(ROOT.kRed)
            pt_elec_weighted[region].Draw("E same")
            Legend = ROOT.TLegend(0.06, 0.9, 0.7, 1.0)
            Legend.SetFillStyle(0)
            Legend.SetBorderSize(0)
            Legend.SetTextSize(0.04)
            Legend.AddEntry(pt_elec_weighted[region], "CR1-CR2, weighted", "L")
            Legend.AddEntry(pt_elec_SR[region], "SR not weighted", "L")
            Legend.Draw()
            Pad2.cd()
            RatioHisto = pt_elec_weighted[region].Clone("%s_Ratio" % (region))
            RatioHisto.Divide(pt_elec_SR[region])
            RatioHisto.SetMaximum(2)
            RatioHisto.SetMinimum(0)
            RatioHisto.GetXaxis().SetTitle("p_{T}^{e} [GeV]")
            RatioHisto.Draw("E")
            canvas.Update()
            canvas.SaveAs("%s/Closure_elec_pt_%s.pdf" % (options.outDir, region))
            canvas.SaveAs("%s/AllClosurePlots.pdf" % (options.outDir))

        for region in pt_mu_weighted:
            canvas = ROOT.TCanvas("Closure_mu_pt_%s" % (region), "", 1200, 800)
            Pad1 = ROOT.TPad("p1_%s" % (region), "", 0.0, 0.37, 1.0, 1.0)
            Pad2 = ROOT.TPad("p2_%s" % (region), "", 0.0, 0.0, 1.0, 0.37)
            Pad1.SetBottomMargin(0.03)  # set to 0 for space between top and bottom pad
            Pad1.SetTopMargin(0.09)
            Pad1.Draw()
            Pad2.SetTopMargin(0.01)  # set to 0 for space between top and bottom pad
            Pad2.SetBottomMargin(0.35)
            Pad2.SetGridy()
            Pad2.Draw()
            Pad1.cd()
            # ~ print "p value: ", met_SR[region].Chi2Test(met_weighted[region], "WW")
            SRmax = pt_mu_SR[region].GetMaximum()
            CRmax = pt_mu_weighted[region].GetMaximum()
            pt_mu_SR[region].SetMaximum(max(SRmax, CRmax) * 1.5)

            pt_mu_SR[region].Draw("E")
            pt_mu_weighted[region].SetMarkerColor(ROOT.kRed)
            pt_mu_weighted[region].SetLineColor(ROOT.kRed)
            pt_mu_weighted[region].Draw("E same")
            Legend = ROOT.TLegend(0.06, 0.9, 0.7, 1.0)
            Legend.SetFillStyle(0)
            Legend.SetBorderSize(0)
            Legend.SetTextSize(0.04)
            Legend.AddEntry(pt_mu_weighted[region], "CR1-CR2, weighted", "L")
            Legend.AddEntry(pt_mu_SR[region], "SR not weighted", "L")
            Legend.Draw()
            Pad2.cd()
            RatioHisto = pt_mu_weighted[region].Clone("%s_Ratio" % (region))
            RatioHisto.Divide(pt_mu_SR[region])
            RatioHisto.SetMaximum(2)
            RatioHisto.SetMinimum(0)
            RatioHisto.GetXaxis().SetTitle("p_{T}^{#mu} [GeV]")
            RatioHisto.Draw("E")
            canvas.Update()
            canvas.SaveAs("%s/Closure_mu_pt_%s.pdf" % (options.outDir, region))
            canvas.SaveAs("%s/AllClosurePlots.pdf" % (options.outDir))

        for region in pt_tau_weighted:
            canvas = ROOT.TCanvas("Closure_tau_pt_%s" % (region), "", 1200, 800)
            Pad1 = ROOT.TPad("p1_%s" % (region), "", 0.0, 0.37, 1.0, 1.0)
            Pad2 = ROOT.TPad("p2_%s" % (region), "", 0.0, 0.0, 1.0, 0.37)
            Pad1.SetBottomMargin(0.03)  # set to 0 for space between top and bottom pad
            Pad1.SetTopMargin(0.09)
            Pad1.Draw()
            Pad2.SetTopMargin(0.01)  # set to 0 for space between top and bottom pad
            Pad2.SetBottomMargin(0.35)
            Pad2.SetGridy()
            Pad2.Draw()
            Pad1.cd()
            # ~ print "p value: ", met_SR[region].Chi2Test(met_weighted[region], "WW")
            SRmax = pt_tau_SR[region].GetMaximum()
            CRmax = pt_tau_weighted[region].GetMaximum()
            pt_tau_SR[region].SetMaximum(max(SRmax, CRmax) * 1.5)

            pt_tau_SR[region].Draw("E")
            pt_tau_weighted[region].SetMarkerColor(ROOT.kRed)
            pt_tau_weighted[region].SetLineColor(ROOT.kRed)
            pt_tau_weighted[region].Draw("E same")
            Legend = ROOT.TLegend(0.06, 0.9, 0.7, 1.0)
            Legend.SetFillStyle(0)
            Legend.SetBorderSize(0)
            Legend.SetTextSize(0.04)
            Legend.AddEntry(pt_tau_weighted[region], "CR1-CR2, weighted", "L")
            Legend.AddEntry(pt_tau_SR[region], "SR not weighted", "L")
            Legend.Draw()
            Pad2.cd()
            RatioHisto = pt_tau_weighted[region].Clone("%s_Ratio" % (region))
            RatioHisto.Divide(pt_tau_SR[region])
            RatioHisto.SetMaximum(2)
            RatioHisto.SetMinimum(0)
            RatioHisto.GetXaxis().SetTitle("p_{T}^{#tau} [GeV]")
            RatioHisto.Draw("E")
            canvas.Update()
            canvas.SaveAs("%s/Closure_tau_pt_%s.pdf" % (options.outDir, region))
            canvas.SaveAs("%s/AllClosurePlots.pdf" % (options.outDir))
        dummycanvas.SaveAs("AllClosurePlots.pdf]")
    if not options.CreateClosurePlots:
        newFile.Close()

    File.Close()
