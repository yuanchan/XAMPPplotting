import ROOT
import argparse, sys, array

obj_list = []


def ParsedOptions():

    parser = argparse.ArgumentParser(
        description='This script combines the SF x FF x R inputs into 1 for the <FF> method \"python CreateAveragedFakeFactorInputs.py -h\"',
        prog='CreateAveragedFakeFactorInputs',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--ff-input", help='Name of FF root file (fake factors)', required=True)
    parser.add_argument("--sf-input", help='Name of SF root file (scale factors)', required=True)
    parser.add_argument("--pf-input", help='Name of PF root file (process pfractions)', required=True)
    parser.add_argument("--output", help='Name of the output root file (average fake factors)', default="average_fake_factors.root")

    return parser.parse_args()


def Output(Options):

    if not Options.output.endswith(".root"):
        print "ERROR: No valid outfile name %s. The file must end with .root" % (Options.out_file)
        return False

    return True


def open_root_file(fname=''):

    if not fname:
        sys.exit("Error: empty file name...")

    rfile = ROOT.TFile(fname, "READ")

    if not rfile:
        sys.exit("Error: invalid root file...")

    return rfile


def obj_is_histo(obj=None):
    return obj and issubclass(type(obj), ROOT.TObject) and (obj.InheritsFrom("TH1") or obj.InheritsFrom("TH2"))


def obj_is_1d_histo(obj=None):
    return obj and issubclass(type(obj), ROOT.TObject) and obj.InheritsFrom("TH1") and ("TH1D" in obj.IsA().GetName()
                                                                                        or "TH1F" in obj.IsA().GetName())


def key_is_histo(key=None):
    return "TH1" in key.GetClassName() and key.InheritsFrom("TObject") and not key.IsZombie()


def set_has_histo(hset=None, histo=None):
    for hs in hset:
        if hs.GetName() == histo.GetName():
            return True

    return False


def get_file_histos(fname='', histos=set()):

    f = open_root_file(fname)

    keys = f.GetListOfKeys()

    if not keys or keys == None:
        print "Error: no keys have been retrieved for file {} ...".format(fname)
        return False

    for ikey in keys:
        if key_is_histo(ikey):
            histo = ikey.ReadObj()
            #print "-->", fname, histo.GetName()
            if obj_is_1d_histo(histo):
                if not set_has_histo(histos, histo):
                    histos.add(histo)
    for h in histos:
        h.SetDirectory(0)

    if not histos:
        print "Warning: no histos have been retrieved from {}".format(fname)
        return False

    return True


def get_position_quantity():
    return -1


def get_field_value(histo=None, field=""):
    ##template:  Nom_SumBGGluonZmumuRatio_Nom_anaZmumu_partTau_defSignal_origGluon_etaIncl_ptIncl_prong3P_pt

    if not obj_is_histo(histo):
        sys.exit("Error: get_field_value - input object is of type '%s' for field '%s' " % (type(histo), field))

    varname = histo.GetName()
    tokens = varname.split("_")
    for i, token in enumerate(tokens):
        if field in token and i != get_position_quantity():
            return token.replace(field, "")

    print "Warning: not recognised field {} for variable {} ".format(field, varname)

    return "None"


def get_analysis(histo):
    return get_field_value(histo, 'ana')


def get_origin(histo):
    return get_field_value(histo, 'orig')


def get_definition(histo):
    return get_field_value(histo, 'def')


def get_prongs(histo):
    return get_field_value(histo, 'prong')


def get_ntrks(histo):
    return get_field_value(histo, 'prong').replace("P", "")


def get_quantity(histo):
    varname = histo.GetName()
    return varname.split("_")[get_position_quantity()]


def get_particle(histo):
    return get_field_value(histo, 'part')


def get_variation(histo):
    return "Nominal"  # to be done


def is_origin_inclusive(histo):
    return get_field_value(histo, 'orig') == "Incl"


def is_1_prong(histo):
    return get_prongs(histo) == "1P"


def is_3_prong(histo):
    return get_prongs(histo) == "3P"


def has_prongs(histo):
    return "prong" in histo.GetName()


def find_pair_histo(h=None, histos=set()):

    pair_name = ''

    if is_1_prong(h):
        pair_name = h.GetName().replace("prong1P", "prong3P")
    elif is_3_prong(h):
        pair_name = h.GetName().replace("prong3P", "prong1P")
    else:
        print "Error: what is this plot ", h.GetName()

    for histo in histos:
        if histo.GetName() == pair_name:
            return histo

    print "Warning: histo %s cannot be paired..." % (h.GetName())
    return None


def make_histo_name(h=None):
    origin = get_origin(h)
    quantity = get_quantity(h)
    particle = get_particle(h)
    delim = "_"
    xvar = quantity
    yvar = "prong" if has_prongs(h) else "hmmm"

    name = particle + delim + "Orig" + origin + delim + "xVar" + delim + xvar + delim + "yVar" + delim + yvar

    return name


def get_2d_histo_template(h=None, n_bins_y=0):

    n = h.GetNbinsX()
    x0 = h.GetBinLowEdge(1)
    x1 = h.GetBinLowEdge(n) + h.GetBinWidth(n)
    name = "TH2_" + h.GetName()
    return ROOT.TH2F(name, "", n, x0, x1, n_bins_y, 0, n_bins_y)


def get_2d_histo(hlist=[]):
    #hlist range: 0, 1, 2
    #th2 range: 1, 2, ...

    th2 = get_2d_histo_template(hlist[0], len(hlist))

    for j in xrange(1, th2.GetNbinsY() + 1):
        k = j - 1
        for i in xrange(1, th2.GetNbinsX() + 1):
            c = hlist[k].GetBinContent(i)
            e = hlist[k].GetBinError(i)
            th2.SetBinContent(i, j, c)
            th2.SetBinError(i, j, e)

    return th2


def get_2d_histo_template(h=None, n_bins_y=0, y0=0, y1=0):

    binsx = []
    nx = h.GetNbinsX()
    for i in xrange(1, nx + 1):
        x0 = h.GetBinLowEdge(i)
        binsx.append(x0)
    x1 = h.GetBinLowEdge(nx) + h.GetBinWidth(nx)
    binsx.append(x1)

    name = "TH2_" + h.GetName()
    xarray = array.array('d', binsx)

    return ROOT.TH2F(name, "", len(xarray) - 1, xarray, n_bins_y, y0, y1)


def get_2d_histo(hlist=[], ynbins=0, ymin=1, ymax=0):

    th2 = get_2d_histo_template(hlist[0], ynbins, ymin, ymax)

    for ih, h in enumerate(hlist):
        if not h:
            print "Warning: get_2d_histo - skipping histogram %i from list because it is likely null..." % (ih)
            continue
        prong = int(get_ntrks(h))
        for i in xrange(1, h.GetNbinsX() + 1):
            x0_source = h.GetBinLowEdge(i)
            x0_target = h.GetBinLowEdge(i)
            if x0_source != x0_target:
                sys.exit("Fatal: get_2d_histo - source %s and target histograms have incompatible x axes" % s(h.GetName()))
            c = h.GetBinContent(i)
            e = h.GetBinError(i)
            th2.SetBinContent(i, prong, c)
            th2.SetBinError(i, prong, e)

    return th2


def get_structure(component='', h=None):

    folder = ''
    subfolder = ''
    if not h:
        print "Error: get_structure - null histo..."
        return False, folder, subfolder

    if not obj_is_histo(h):
        print "Error: get_structure - get_field_value - input object is of type '%s'" % (type(h))
        return False, folder, subfolder

    folder = component + "_" + get_variation(h)

    subfolder = get_analysis(h)

    return True, folder, subfolder


def make_structure(component='', h=None, f=None):

    if not component:
        print "Error: make_structure - empty component..."
        return False

    if not h:
        print "Error: make_structure - null histo..."
        return False

    if not obj_is_histo(h):
        print "Error: make_structure - input object is of type '%s'" % (type(h))
        return False

    ok, folder, subfolder = get_structure(component, h)

    if not ok:
        print "Error: problem with getting the structure..."
        return False

    if not f:
        print "Error: null output root file..."
        return False

    if not f.cd():
        print "Error: cannot cd root file..."
        return False

    if folder not in f.GetListOfKeys():
        f.mkdir(folder)

    Folder = f.Get(folder)

    if not Folder:
        print "Error: cannot enter directory: '{}'".format(folder)
        return False

    if subfolder not in Folder.GetListOfKeys():
        Folder.mkdir(subfolder)

    Subfolder = Folder.Get(subfolder)

    if not Subfolder:
        print "Error: cannot enter subdirectory: '{}'".format(subfolder)
        return False

    return True


def store_histo_in_structure(component='', h=None, f=None):

    if not f:
        print "Error: store_histo_in_structure - null output root file..."
        return False

    if not h:
        print "Error: store_histo_in_structure - null histo..."
        return False

    ok, folder, subfolder = get_structure(component, h)

    if not ok:
        print "Error: store_histo_in_structure - problem with getting the structure..."
        return False

    if not f.cd():
        print "Error: cannot enter in file '{}'".format(f.GetName())
        return False

    parent = ROOT.gDirectory.GetPathStatic()

    if ROOT.gDirectory.cd(folder):
        if ROOT.gDirectory.cd(subfolder):
            ROOT.gDirectory.WriteTObject(h, make_histo_name(h))
            if ROOT.gDirectory.cd("../../"):
                if ROOT.gDirectory.GetPathStatic() != parent:
                    print "Static path not identical to parent directory '{}'".format(parent)
                    return False
            else:
                print "Cannot go back to parent directory '{}'".format(parent)
                return False
        else:
            print "Cannot enter in subfolder '{}'".format(subfolder)
            return False
    else:
        print "Cannot enter in folder '{}'".format(folder)
        return False

    return True


def gethistos(dire):

    histos = []
    for key in dire.GetListOfKeys():
        if "TH1" in key.GetClassName():
            histos.append(key)
    return histos


def PrintStructureLvl0(f=None):

    if not f.cd():
        print "Error: cannot enter in file :", f.GetName()
        histos = gethistos(ROOT.gDirectory)
        for histo in histos:
            h = f.Get(ROOT.gDirectory.GetPathStatic() + "/" + histo.GetName())
            print "\tHisto=%s Entries=%i Area=%f" % (subfold.GetName(), int(h.GetEntries()), h.GetEntries())


def avgFF_element_structure(component="", filename="", rfile=None):

    print "Info: avgFF_element_structure - creating structure for", component, filename

    if not component:
        print "Error: empty component name..."
        return False

    if not filename:
        print "Error: empty input filename name for {} ...".format(component)
        return False

    histos = set()

    if not get_file_histos(filename, histos):
        print "Error: unbale to get histos for {} ...".format(component)
        return False

    if not histos:
        print "Warning: no histos to analyze for {} ...".format(component)
        return False

    histos_1p = set()
    histos_3p = set()

    for histo in histos:
        #print component, histo.GetName()
        if is_1_prong(histo):
            histos_1p.add(histo)
        elif is_3_prong(histo):
            histos_3p.add(histo)

    if not histos_1p:
        print "Warning: no 1p histos to analyze for {} ...".format(component)
        return False

    if not histos_3p:
        print "Warning: no 3p histos to analyze for {} ...".format(component)
        return False

    for h1p in histos_1p:
        h3p = find_pair_histo(h1p, histos_3p)
        h2d = get_2d_histo([h1p, h3p], 4, 1, 5)
        if not make_structure(component=component, h=h1p, f=rfile):
            print "Warning: unable to create stucture for {} ...".format(component)
            return False

        if not store_histo_in_structure(component=component, h=h2d, f=rfile):
            print "Warning: unable to store histo {} in the created stucture for {}...".format(h2d.GetName(), component)
            return False

    return True


"""
def get_output_root_file(fname = '', output = None):

    if not fname:
        print "Error: empty name of output file..."
        return False
    
    output = ROOT.TFile(fname, "RECREATE")
    obj_list.append(output)

    print "TMP: ", output

    return True
"""
if __name__ == "__main__":

    Options = ParsedOptions()

    if not Output(Options):
        sys.exit("Error: cannot set up output. Exiting...")

    output = output = ROOT.TFile(Options.output, "RECREATE")

    if not output:
        sys.exit("Error: created output file is invalid. Exiting...")

    if not avgFF_element_structure("FakeFactor", Options.ff_input, output):
        sys.exit("Error: create FF input. Exiting...")

    if not avgFF_element_structure("ScaleFactor", Options.sf_input, output):
        sys.exit("Error: create SF input. Exiting...")

    if not avgFF_element_structure("ProcessFractions", Options.pf_input, output):
        sys.exit("Error: create PF input. Exiting...")

    print "Info: File created:"
    print "\t", Options.output

    output.Close()

    print "Done!"
