from ClusterSubmission.Utils import ResolvePath


def ReadInputConfig(config, sample=None):
    """Read files contained in InputConfig text file and return as list."""
    files = []
    final_path = ResolvePath(config)
    if not final_path:
        return files
    with open(final_path) as cfile:
        for line in cfile:
            # Ignore comment lines and empty lines
            line = line.strip()
            if line.startswith('#'):
                continue
            if line.startswith('SampleName '):
                sample = line.replace('SampleName ', '').replace('\n', '')
            if line.startswith('Input '):
                files.extend(line.replace('Input ', '').replace('\n', '').split())
            if line.startswith("Import "):
                files.extend(ReadInputConfig(line.replace('Import', '').replace('\n', '').strip(), sample))
    return files


def ReadMetaInputs(config):
    final_path = ResolvePath(config)
    if not final_path: return []
    meta_in = []
    with open(final_path) as cfile:
        for line in cfile:
            # Ignore comment lines and empty lines
            line = line.strip()
            if line.startswith('#'): continue
            if line.startswith("Import "): meta_in += ReadMetaInputs(line.replace('Import', '').replace('\n', '').strip())
            if line.startswith('MetaInput '): meta_in.extend(line.replace('MetaInput ', '').replace('\n', '').split())
    return meta_in
