#! /usr/bin/env python
import ROOT, math, sys, gc, os, argparse, logging
from array import array

ROOT.gROOT.SetBatch(1)
gc.disable()


class SampleTypes(object):
    Data = "Data",
    Signal = "Signal",
    Irreducible = "Irreducible",
    Reducible = "Reducible"


# utility class for input datasets used for plotting:
# specify things like 'colour', an unique 'name' and a 'label' for the dataset to be printed in the plots


class DSConfigName(object):
    def __init__(self, Name="ConfigName", ReferenceSample=""):
        self.Name = Name
        self.Reference = ReferenceSample


class DSconfig(object):
    def __init__(
        self,
        name="",
        label="",
        filepath="",
        sampletype=SampleTypes.Irreducible,
        lumi=1.,
        colour=-1,
        markercolour=-1,
        linecolour=-1,
        linewidth=-1,
        fillcolour=-1,
        fillcolourAlpha=-1,
        linestyle=-1,
        fillstyle=-1,
        markerstyle=-1,
        markersize=-1,
        TheoUncert=-1.,
        #Assign up variation to the upside systematic uncertainty
        #Assign down variations to the downside systematic uncertainty
        Nominal="",
        ExcludeSyst=[],
        SystToUse=[],
        scalefactor=1.,
        LegendOrder=-1,
        DrawStyle="",
    ):
        #Name. label and type of the
        #sample. Lumi important for data MC -> Scaled data with highest lumi
        self.Name = name
        self.Label = label
        self.SampleType = sampletype
        self.Lumi = lumi
        self.Filepaths = []
        if isinstance(filepath, str): self.Filepaths = [filepath]
        else: self.Filepaths = filepath

        ### Some colouring options the user can define
        self.setColour(colour=colour,
                       markercolour=markercolour,
                       linecolour=linecolour,
                       fillcolour=fillcolour,
                       fillcolourAlpha=fillcolourAlpha)
        self.LineWidth = linewidth if linewidth > 0 else 2
        ##Styles are also possible as well as the marker size
        self.LineStyle = linestyle if linestyle > 0 else -1
        self.FillStyle = fillstyle
        self.MarkerStyle = markerstyle if markerstyle > 0 else -1
        self.MarkerSize = markersize if markersize > 0 else -1

        self.TheoUncert = TheoUncert

        self.NominalSyst = Nominal
        self.SetNominal = len(Nominal) > 0
        self.ExcludeSyst = ExcludeSyst
        self.SystToUse = SystToUse
        self.ScaleFactor = scalefactor
        self.LegendOrder = LegendOrder
        self.DrawStyle = DrawStyle
        self.DrawStyle = DrawStyle

    def setColour(self, colour=-1, markercolour=-1, linecolour=-1, fillcolour=-1, fillcolourAlpha=-1):
        self.MarkerColour = markercolour if markercolour > 0 else colour
        self.LineColour = linecolour if linecolour > 0 else colour
        self.FillColour = fillcolour if fillcolour > 0 else colour
        self.FillColourAlpha = fillcolourAlpha
