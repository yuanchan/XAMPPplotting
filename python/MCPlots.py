#! /usr/bin/env python
from XAMPPplotting.Defs import *
from XAMPPplotting.PlotUtils import draw2DHisto, PlotUtils, Stack
from XAMPPplotting.Utils import CreateRatioHistos, setupBaseParser, store_yields, RemoveSpecialChars
from XAMPPplotting.YieldsHandler import *
from ClusterSubmission.Utils import CreateDirectory
from XAMPPplotting.FileStructureHandler import ClearServices, GetStructure
from XAMPPplotting.PlottingHistos import CreateHistoSets
import logging


def drawHisto(Options, analysis, region, var, bonusstr="", yields_handler=None):
    HistList = []
    HistToDraw = []
    ratios = []

    FullHistoSet = CreateHistoSets(Options, analysis, region, var, UseData=False)

    if not FullHistoSet or not FullHistoSet[0].CheckHistoSet():
        logging.warning("I cannot draw stacked background MC histogram without background samples, skipping...")
        return False

    DefaultSet = FullHistoSet[0]
    pu = PlotUtils(status=Options.label, size=24, lumi=DefaultSet.GetLumi(), normalizedToUnity=Options.ShapeComp)

    logstr = "" if not Options.doLogY else "_LogY"

    if DefaultSet.isTH2():
        if Options.skipTH2: return False
        return draw2DHisto(FullHistoSet, Options, DrawData=True, PlotType="MC", SummaryFile="AllMCPlots")

    if not DefaultSet.isTH1() or Options.skipTH1:
        return False

    if Options.noRatio and not Options.Purity:
        pu.Prepare1PadCanvas("MC_%s_%s_%s%s%s" % (var, Options.nominalName, region, bonusstr, logstr), 800, 600, Options.quadCanvas)
        if Options.doLogY:
            pu.GetCanvas().SetLogy()
    else:
        pu.Prepare2PadCanvas("MC_%s_%s_%s%s%s" % (var, Options.nominalName, region, bonusstr, logstr), 800, 600, Options.quadCanvas)
        pu.GetTopPad().cd()
        if Options.doLogY: pu.GetTopPad().SetLogy()

    pu.CreateLegend(Options.LegendXCoords[0], Options.LegendYCoords[0], Options.LegendXCoords[1], Options.LegendYCoords[1],
                    Options.LegendTextSize)

    # Create Stack containing all the backgrounds in it
    if not Options.noStack and not Options.ShapeComp:
        stk = Stack(DefaultSet.GetBackgrounds())
        if not Options.noSignal:
            if len(DefaultSet.GetSignals()) == 0:
                logging.warning("You want to draw signals but did not defined them in your config.")

            for Sig in DefaultSet.GetSignals(not Options.noSignalStack):
                HistList.append(Sig)
                HistToDraw.append((Sig, "same" + Sig.GetDrawStyle()))

        # draw the stack in order to get the histogram for the axis ranges
        if DefaultSet.GetSummedBackground() is not None and DefaultSet.GetSummedBackground().isValid():
            HistList.append(DefaultSet.GetSummedBackground())
    else:
        if Options.SumBGOnly and DefaultSet.GetSummedBackground():
            if Options.ShapeComp:
                DefaultSet.GetSummedBackground().SetLumi(1. / DefaultSet.GetSummedBackground().Integral())
            HistList.append(DefaultSet.GetSummedBackground())
        else:
            for bg in DefaultSet.GetBackgrounds():
                if Options.ShapeComp and bg.Integral() > 0:
                    bg.SetLumi(1. / bg.Integral())
                elif Options.ShapeComp:
                    continue
                HistList.append(bg)
                HistToDraw.append((bg, "same" + bg.GetDrawStyle()))
        # draw signals last
        if not Options.noSignal:
            for Sig in DefaultSet.GetSignals():
                HistList.append(Sig)
                if Options.ShapeComp and Sig.Integral() > 0.:
                    Sig.SetLumi(1. / Sig.Integral())
                elif Options.ShapeComp:
                    continue
                HistToDraw.append((Sig, "same" + Sig.GetDrawStyle()))
        if Options.ShapeComp:
            for h, o in HistToDraw:
                h.GetHistogram().GetYaxis().SetTitle("Normalised to unit area")

    # The sample to which are the ratios made
    RatioSmp = None
    if len(Options.RatioSmp) > 0:
        RatioSmp = DefaultSet.GetSample(Options.RatioSmp)
    elif DefaultSet.GetSummedBackground() is not None and DefaultSet.GetSummedBackground().isValid():
        RatioSmp = DefaultSet.GetSummedBackground()
    else:
        logging.warning("MCPlots: No valid summed background found. Will pick one random signal")
        RatioSmp = DefaultSet.GetSignals()[-1]

    if not RatioSmp:
        logging.warning("Failed to retrieve the ratio sample " + Options.RatioSmp)
        return False

    if len(HistList) == 0: return False
    ymin, ymax = pu.GetFancyAxisRanges(HistList, Options)
    if math.fabs(ymin) <= 1.e-10 and math.fabs(ymax) <= 1.e-10:
        logging.info("Night time %f day time %f" % (ymin, ymax))
        return False
    pu.drawStyling(HistList[0], ymin, ymax, RemoveLabel=not Options.noRatio, TopPad=not Options.noRatio)

    if not Options.noStack and not Options.ShapeComp:
        stk.Draw("HistSAME")

    if RatioSmp.GetHistogram() and not Options.noRatio:
        pu.drawSumBg(RatioSmp, (not Options.noSyst and not Options.noUpperPanelSyst), not Options.noStack)

    # estimate displacement for plotting error bars non-overlapping
    errorHists = []
    plotErrorBarsSeparately = False
    try:
        dx = HistToDraw[0][0].GetXaxis().GetBinWidth(1) / max(5, len(HistToDraw))
    except IndexError:
        dx = None
    # plot non-overlapping error bars (error bars next to each other): do this after plotting all histograms
    if Options.spreadErrorBars and dx and (Options.noStack or Options.ShapeComp):
        plotErrorBarsSeparately = True
        for i, HistAndOpt in enumerate(HistToDraw):
            errorHist = HistAndOpt[0].GetHistogram().Clone(HistAndOpt[0].GetName() + "_displacedErrorBar")
            errorHist.GetXaxis().Set(errorHist.GetXaxis().GetNbins(),
                                     errorHist.GetXaxis().GetXmin() + i * dx,
                                     errorHist.GetXaxis().GetXmax() + i * dx)
            errorHists.append((errorHist, "same" + "E2"))
            errorHist.Draw("sameE2")

    for H, Opt in HistToDraw:
        H.SetLineWidth(2)
        # modify draw option to remove error being drawn to avoid having two error bars
        if plotErrorBarsSeparately: Opt = Opt.lower().replace("e2", "")
        H.Draw(Opt)

    if not Options.noStack and not Options.ShapeComp:
        pu.AddToLegend(stk.GetHistogramStack(), Style="FL")

    elif not Options.SumBGOnly:
        pu.AddToLegend(DefaultSet.GetBackgrounds())
    else:
        pu.AddToLegend(DefaultSet.GetSummedBackground())

    for Sig in DefaultSet.GetSignals():
        pu.AddToLegend([Sig], Style=Sig.GetLegendDrawStyle())

    pu.DrawLegend(NperCol=Options.EntriesPerLegendColumn)

    labelY = 0.83
    if Options.noRatio: labelY = 0.88
    pu.DrawPlotLabels(0.195, labelY, Options.regionLabel if len(Options.regionLabel) > 0 else region, analysis, Options.noATLAS)

    #     if Options.ShapeComp:pu.DrawTLatex(0.195, 0.59, "normalized to unit area", 0.035, 52)

    #yields
    if yields_handler:
        yields_handler.append(Yields(DefaultSet.GetSummedBackground().GetHistogram()))

    #no ratio
    if not Options.noRatio:
        pu.GetTopPad().RedrawAxis()
        pu.GetBottomPad().cd()
        pu.GetBottomPad().SetGridy()

        RatioLabel = ""
        if Options.RatioLabel != '':
            RatioLabel = Options.RatioLabel
        elif Options.SoverB:
            RatioLabel = "Signal/SM"  # for Options.SoverB
        elif Options.SoverSqrtB:
            RatioLabel = "Signal/#sqrt{SM}"  # for Options.SoverSqrtB
        elif Options.Purity:
            RatioLabel = "Purity"
        else:
            RatioLabel = "#splitline{Ratio to}{%s}" % (RatioSmp.GetTitle())

        if Options.SoverB:
            ratios = CreateRatioHistos(DefaultSet.GetSignals(), DefaultSet.GetSummedBackground())
            pu.drawRatioErrors(DefaultSet.GetSummedBackground(), not Options.noSyst, not Options.noRatioSysLegend)
        if Options.SoverSqrtB:
            sqrtBackground = DefaultSet.GetSummedBackground().Sqrt()
            ratios = CreateRatioHistos(DefaultSet.GetSignals(), sqrtBackground.get())
            # need to correctly calculate errors for s / sqrt(b), only write errors if 1 signal is given
            if len(DefaultSet.GetSignals()) == 1:
                pu.drawRatioSignificanceErrors(DefaultSet.GetSignals()[0], DefaultSet.GetSummedBackground())
        elif Options.Purity:
            ratios = [Stack(CreateRatioHistos(DefaultSet.GetBackgrounds(), DefaultSet.GetSummedBackground()))]
            if not Options.noSignal:
                ratios.extend(CreateRatioHistos(DefaultSet.GetSignals(), DefaultSet.GetSummedBackground()))
        elif Options.RatioSmp:
            ratios += CreateRatioHistos(DefaultSet.GetBackgrounds(), RatioSmp)
            ratios += CreateRatioHistos(DefaultSet.GetSignals(), RatioSmp)
        elif Options.ShapeComp or Options.noStack:
            if len(DefaultSet.GetSignals()) > 0:
                ratios += CreateRatioHistos(DefaultSet.GetSignals(), DefaultSet.GetSignals()[-1])
                if DefaultSet.GetSummedBackground() is not None and DefaultSet.GetSummedBackground().isValid():
                    ratios += CreateRatioHistos(DefaultSet.GetSignals(), DefaultSet.GetSummedBackground())
            if not Options.SumBGOnly and len(DefaultSet.GetBackgrounds()) > 0:
                ratios += CreateRatioHistos(DefaultSet.GetBackgrounds(), DefaultSet.GetBackgrounds()[-1])

        if len(ratios) == 0:
            logging.warning("Cautious... No ratios were extracted. What do you want to do?")
            return False

        ymin, ymax = pu.GetFancyAxisRanges(ratios, Options, doRatio=True)

        if Options.Purity:
            ymin = 0
            ymax = 1.05
            if Options.RatioLogY:
                pu.GetBottomPad().SetLogy()
                ymin = 1.e-3

        pu.drawRatioStyling(RatioSmp, ymin, ymax, RatioLabel)
        pu.drawRatioErrors(RatioSmp, not Options.noSyst and len(RatioSmp.GetSystComponents()) > 0, not Options.noRatioSysLegend)

        for r in ratios:
            if Options.ShapeComp: r.SetFillStyle(0)
            r.Draw("sameHist")
            if not Options.Purity:
                rStat = r.Clone("%s_stat" % r.GetName())
                rStat.SetMarkerStyle(1)
                rStat.SetMarkerSize(1)
                # rStat.SetFillStyle(3354)
                rStat.SetFillColor(r.GetLineColor())
                rStat.SetMarkerColor(r.GetLineColor())
                rStat.Draw("sameE2")

                if yields_handler:
                    yields_handler.append(Yields(r))

        #store yields
        if yields_handler:
            if not yields_handler.store():
                sys.exit("Error: cannot store yields in file for analysis %s, region %s, variable %s" % (analysis, region, var))

    ROOT.gPad.RedrawAxis()

    PreString = region
    if analysis != "":
        PreString = analysis + "_" + region
    if not Options.summaryPlotOnly:
        pu.saveHisto("%s/MC_%s_%s%s" % (Options.outputDir, PreString, var, logstr), Options.OutFileType)
    if not Options.noRatio:
        pu.GetTopPad().cd()
        pu.DrawTLatex(0.5, 0.94, "%s%s in %s (%s analysis)" % (var, logstr, region, analysis), 0.04, 52, 22)
    else:
        pu.DrawTLatex(0.5, 0.97, "%s%s in %s (%s analysis)" % (var, logstr, region, analysis), 0.04, 52, 22)

    pu.saveHisto("%s/AllMCPlots%s" % (Options.outputDir, bonusstr), ["pdf"])
    return True


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description='This script produces MC only histograms from tree files. For more help type \"python MCPlots.py -h\"',
        prog='MCPlots',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser = setupBaseParser(parser)
    parser.add_argument('--SoverB', help='Draw signal over background in ratio panel', action='store_true', default=False)
    parser.add_argument('--SoverSqrtB', help='Draw signal over sqrt(background) in ratio panel', action='store_true', default=False)
    parser.add_argument('--Purity', help='Draw purity of each background in ratio panel', action='store_true', default=False)
    parser.add_argument('--noStack', help='do not stack the different MC samples', action='store_true', default=False)
    parser.add_argument('--RatioLogY', help='Switch on the ratio LogY', action='store_true', default=False)
    parser.add_argument('--spreadErrorBars', help='plot error bars in a non-overlapping fashion', action='store_true', default=False)
    parser.add_argument('--ShapeComp', help='draw different MC samples normalized to unit area', action='store_true', default=False)
    parser.add_argument('--SumBGOnly', help='draw only the summed BG histo', action='store_true', default=False)
    parser.add_argument('--RatioSmp', help='Specify the sample label for the ratio to be drawn', default='')
    parser.add_argument('--RatioLabel', help='Specify a y-axis label for the ration panel', default='')
    PlottingOptions = parser.parse_args()
    CreateDirectory(PlottingOptions.outputDir, False)

    # Analyze the samples and adjust the PlottingOptions
    FileStructure = GetStructure(PlottingOptions)

    # do this here, since before it destroys the argparse
    ROOT.gROOT.Macro("rootlogon.C")
    ROOT.gROOT.SetStyle("ATLAS")

    dummy = ROOT.TCanvas("dummy", "dummy", 800, 600)

    #yields handler
    yh = YieldsHandler( outdir = PlottingOptions.outputDir, \
                        outfile = PlottingOptions.store_yields_outfile, \
                        raw_yields    = PlottingOptions.store_yields_raw, \
                        latex_yields  = PlottingOptions.store_yields_latex, \
                        hbins_yields  = PlottingOptions.store_yields_hbins, \
                        histo_yields  = PlottingOptions.store_yields_histo ) if  store_yields(PlottingOptions) else None #use late binding

    if PlottingOptions.noStack:
        if PlottingOptions.SoverB or PlottingOptions.SoverSqrtB:
            print 'ERROR: Cannot draw MC histogram without stacking SM background and showing S/B at the same time! Exiting...'
            sys.exit(1)
        if PlottingOptions.Purity:
            print 'ERROR: Cannot draw MC histogram without stacking SM background and showing SM background purities at the same time! Exiting...'
            sys.exit(1)
    if PlottingOptions.ShapeComp:
        if PlottingOptions.SoverB or PlottingOptions.SoverSqrtB:
            print 'ERROR: Cannot draw MC shape comparison and showing S/B at the same time! Exiting...'
            sys.exit(1)
        if PlottingOptions.Purity:
            print 'ERROR: Cannot draw MC shape comparison and showing SM background purities at the same time! Exiting...'
            sys.exit(1)
    for ana in FileStructure.GetConfigSet().GetAnalyses():
        for region in FileStructure.GetConfigSet().GetAnalysisRegions(ana):
            bonusstr = '_%s_%s' % (ana, region)
            if PlottingOptions.doLogY:
                bonusstr += '_LogY'
            bonusstr = RemoveSpecialChars(bonusstr)

            dummy.SaveAs("%s/AllMCPlots%s.pdf[" % (PlottingOptions.outputDir, bonusstr))
            Drawn = False
            for var in FileStructure.GetConfigSet().GetVariables(ana, region):
                Drawn = drawHisto(PlottingOptions, ana, region, var, bonusstr=bonusstr, yields_handler=yh) or Drawn

            if Drawn:
                dummy.SaveAs("%s/AllMCPlots%s.pdf]" % (PlottingOptions.outputDir, bonusstr))
            else:
                os.system("rm %s/AllMCPlots%s.pdf" % (PlottingOptions.outputDir, bonusstr))

    ClearServices()
