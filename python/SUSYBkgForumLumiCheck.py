import os, sys, argparse
from XAMPPplotting.CalculateLumiFromIlumicalc import *
from XAMPPplotting.PlotUtils import *

# from https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/SusyBgForumRun2CheckList#Standard_diagnostic_plots_checks

ilumicalcFiles = [
    '/afs/ipp-garching.mpg.de/home/n/niko/0L/MPICode160429/XAMPPbase/data/GRL/data15_13TeV.periodAllYear_DetStatus-v79-repro20-02_DQDefects-00-02-02_PHYS_StandardGRL_All_Good_25ns.root',
    '/afs/ipp-garching.mpg.de/home/n/niko/0L/MPICode160429/XAMPPbase/data/GRL/data16_13TeV.periodAllYear_DetStatus-v88-pro20-21_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.root'
]


def CreateRunNumberHisto(histo, LumiPerRun, removeEmptyBins=False):
    nBins = len(LumiPerRun)
    if removeEmptyBins:
        nBins = 0
        for i in range(histo.GetNbinsX() + 1):
            if histo.GetBinContent(i) != 0:
                nBins += 1
    RNhist = ROOT.TH1F("h_RN", "h_RN", nBins, 0, nBins)
    RNhist.SetDirectory(0)
    RNhist.SetLabelSize(0.03)
    RNhist.GetXaxis().SetTitle("Run Number")
    RNhist.GetYaxis().SetTitle("Number of events / int. Lumi [pb]")
    if removeEmptyBins:
        noEmptyBin = 0
        for i, r in enumerate(sorted(LumiPerRun.iterkeys())):
            iyield = histo.GetBinContent(r)
            if iyield != 0:
                RNhist.GetXaxis().SetBinLabel(noEmptyBin + 1, str(r))
                RNhist.SetBinContent(noEmptyBin + 1, iyield / LumiPerRun[r])
                RNhist.SetBinError(noEmptyBin + 1, histo.GetBinError(r) / LumiPerRun[r])
                noEmptyBin += 1
    else:
        for i, r in enumerate(sorted(LumiPerRun.iterkeys())):
            iyield = histo.GetBinContent(r)
            RNhist.GetXaxis().SetBinLabel(i + 1, str(r))
            if iyield != 0:
                RNhist.SetBinContent(i + 1, iyield / LumiPerRun[r])
                RNhist.SetBinError(i + 1, histo.GetBinError(r) / LumiPerRun[r])
            else:
                RNhist.SetBinContent(i + 1, 0.)
    return RNhist


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description="Script checking lumi dependence of regions for SUSY background forum",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-i', '--input', help='Specify histogram file to check', nargs='+', default=m_PRWFiles)
    parser.add_argument('--ilumicalcFile', help='Specify ilumicalc file(s)', nargs='+', default=ilumicalcFiles)
    parser.add_argument('--runNumberHisto', help='Specify name of runNumber histogram', default="RunNumber")
    parser.add_argument('--skipRegion', help='Specify region to skip', default="")
    parser.add_argument('--label', help='Specify the label to be plotted', default="Internal")
    parser.add_argument('--noATLAS', help='Disable the drawing of the ATLAS Internal label', action='store_true', default=False)
    parser.add_argument('--noEmptyBins', help='Disable the drawing of empty bins', action='store_true', default=False)
    options = parser.parse_args()

    ROOT.gROOT.SetBatch(True)
    ROOT.gROOT.Macro("rootlogon.C")
    ROOT.gROOT.SetStyle("ATLAS")

    LumiPerRun = {}
    for r in GetRuns(options.ilumicalcFile):
        LumiPerRun[r] = CalculateLumiFromRuns(r, r)

        pu = PlotUtils(status=options.label, size=24)
    if options.noEmptyBins:
        pu.Prepare1PadCanvas("RN", 1000, 600)
    else:
        pu.Prepare1PadCanvas("RN", 1800, 600)
    bonusstr = ''
    if options.noEmptyBins: bonusstr = '_noEmptyBins'
    pu.GetCanvas().SaveAs("All_RunNumberChecks%s.pdf[" % bonusstr)
    for file in options.input:
        TFile = ROOT.TFile(file, "READ")
        if not TFile: print 'Cannot open file %s' % file
        for analysis in TFile.GetListOfKeys():
            TFile.cd(analysis.GetName())
            for region in ROOT.gDirectory.GetListOfKeys():
                if options.skipRegion != "" and options.skipRegion in region.GetName():
                    continue
                TFile.cd("%s/%s" % (analysis.GetName(), region.GetName()))
                for histo in ROOT.gDirectory.GetListOfKeys():
                    if not options.runNumberHisto == histo.GetName(): continue
                    rNHisto = TFile.Get("%s/%s/%s" % (analysis.GetName(), region.GetName(), histo.GetName()))
                    if not rNHisto:
                        print 'Cannot read histogram %s/%s/%s from file %s' % (analysis.GetName(), region.GetName(), histo.GetName(), file)
                    runNumberHisto = CreateRunNumberHisto(rNHisto, LumiPerRun, options.noEmptyBins)
                    runNumberHisto.SetMaximum(0.05)
                    runNumberHisto.Draw("e0")
                    print 'Region %s has %.1f events' % (region.GetName(), rNHisto.Integral())
                    pu.DrawRegionLabel("RN", region.GetName(), 0.2, 0.75)
                    if not options.noATLAS: pu.DrawAtlas(0.2, 0.8)
                    pu.GetCanvas().SaveAs("runNumber_%s%s.pdf" % (region.GetName(), bonusstr))
                    pu.GetCanvas().SaveAs("All_RunNumberChecks%s.pdf" % bonusstr)
    pu.GetCanvas().SaveAs("All_RunNumberChecks%s.pdf]" % bonusstr)
