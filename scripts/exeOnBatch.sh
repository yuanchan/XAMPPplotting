#!/bin/bash
if [  "${SLURM_JOB_USER}" == ""  ];then
    ID="${SGE_TASK_ID}"
else
    ID=$((SLURM_ARRAY_TASK_ID+IdOffSet))
fi
if [ -z "${ID}" ]; then
    ID=1
fi

JoFile=""
if [ -f "${JobCfg}" ];then
    echo "JoFile=`sed -n \"${ID}{p;q;}\" ${JobCfg}`"
    JoFile=`sed -n "${ID}{p;q;}" ${JobCfg}`
fi
InFile=""
if [ -f "${InCfg}" ];then
    echo "InFile=`sed -n \"${ID}{p;q;}\" ${InCfg}`"
    InFile=`sed -n "${ID}{p;q;}" ${InCfg}`
fi
OutFile=""
if [ -f "${OutCfg}" ];then
    echo "OutFile=`sed -n \"${ID}{p;q;}\" ${OutCfg}`"
    OutFile=`sed -n "${ID}{p;q;}" ${OutCfg}`
fi
# some initial output
echo "###############################################################################################"
echo "                     Environment variables"
echo "###############################################################################################"
export
echo "###############################################################################################"
echo " "

if [ -z "${ATLAS_LOCAL_ROOT_BASE}" ];then
    echo "###############################################################################################"
    echo "                    Setting up the environment"
    echo "###############################################################################################"
    # check if TMPDIR exists or define it as TMP
    [[ -d "${TMPDIR}" ]] || export TMPDIR=${TMP}
    echo "cd ${TMPDIR}"
    cd ${TMPDIR}
    echo "export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase"
    export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
    echo "Setting up the ATLAS environment:"
    echo "source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh"
    source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
    echo "Setup athena:"
#    echo "cd ${OriginalArea}"
#    cd ${OriginalArea}
    echo "asetup ${OriginalProject},${OriginalPatch},here"
    asetup ${OriginalProject},${OriginalPatch},here
    #Check whether we're in release 21
    if [ -f ${OriginalArea}/../build/${BINARY_TAG}/setup.sh ]; then
        echo "source ${OriginalArea}/../build/${BINARY_TAG}/setup.sh"
        source ${OriginalArea}/../build/${BINARY_TAG}/setup.sh
        WORKDIR=${OriginalArea}/../build/${BINARY_TAG}/bin/
    elif [ -f ${OriginalArea}/../build/${WorkDir_PLATFORM}/setup.sh ];then
        echo "source ${OriginalArea}/../build/${WorkDir_PLATFORM}/setup.sh"
        source ${OriginalArea}/../build/${WorkDir_PLATFORM}/setup.sh
        source ${OriginalArea}/../build/${WorkDir_PLATFORM}/setup.sh
        WORKDIR=${OriginalArea}/../build/${WorkDir_PLATFORM}/bin/
     elif [ -f ${OriginalArea}/../build/${AthAnalysis_PLATFORM}/setup.sh ];then
            echo "source ${OriginalArea}/../build/${AthAnalysis_PLATFORM}/setup.sh"
            source ${OriginalArea}/../build/${AthAnalysis_PLATFORM}/setup.sh
            WORKDIR=${OriginalArea}/../build/${AthAnalysis_PLATFORM}/bin/
    elif [ -f ${OriginalArea}/../build/${LCG_PLATFORM}/setup.sh ];then
            echo "source ${OriginalArea}/../build/${LCG_PLATFORM}/setup.sh"
            source ${OriginalArea}/../build/${LCG_PLATFORM}/setup.sh
            WORKDIR=${OriginalArea}/../build/${LCG_PLATFORM}/bin/
    elif [ -z "${CMTBIN}" ];then
        source  ${OriginalArea}/../build/x86_64*/setup.sh
        if [ $? -ne 0 ];then
            echo "Something strange happens?!?!?!"
            export
            echo " ${OriginalArea}/../build/${BINARY_TAG}/setup.sh"
            echo " ${OriginalArea}/../build/${WorkDir_PLATFORM}/setup.sh"
            [ -z "${PBS_O_QUEUE}" ] && scontrol requeuehold ${SLURM_ARRAY_JOB_ID}_${SLURM_ARRAY_TASK_ID}
            exit 100
        fi
    fi
else
    echo "Assuming your AthAnalysis environment has been set up correctly"
fi

echo "#######################################################################"
echo "cd ${TMPDIR}"
cd ${TMPDIR}
echo "Check disk-quota"
df -h
echo "#######################################################################"

echo "${Exec} -i ${InFile} -r ${JoFile} -t ${JoFile}  -o tmp_out.root  -h ${HistoCfg}"
${Exec} -i ${InFile} -r ${JoFile} -t ${JoFile}  -o tmp_out.root  -h ${HistoCfg} &> ${TMPDIR}/log.log
# write output of executable to logfile on TMPDIR and display log at the end
# for copying its content to the task log file (this reduces the strain on shared filesystems)
if [ $? -eq 0 ]; then
    cat ${TMPDIR}/log.log
    echo "###############################################################################################"
    echo "                        WriteDefaultHistos terminated successfully"
    echo "###############################################################################################"
    ls -lh
    echo "mv ${TMPDIR}/tmp_out.root ${OutFile}"
    mv ${TMPDIR}/tmp_out.root ${OutFile}
else
    cat ${TMPDIR}/log.log
    echo "###############################################################################################"
    echo "                    WriteDefaultHistos job has experienced an error"
    echo "###############################################################################################"
    [ -z "${PBS_O_QUEUE}" ] && scontrol requeuehold ${SLURM_ARRAY_JOB_ID}_${SLURM_ARRAY_TASK_ID}
    exit 100
fi
