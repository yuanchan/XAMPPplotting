#!/env/python
from ClusterSubmission.Utils import ResolvePath, WriteList, CreateDirectory
import os


def pair_id_antid_regions(RunConfDir):
    CR_Files = []
    for Fail_Conf in os.listdir(RunConfDir):
        if Fail_Conf.find("fail") == -1: continue
        #if Fail_Conf.find("CR") != -1: continue
        Pair_Found = False
        for Pass_Conf in os.listdir(RunConfDir):
            if Pass_Conf.find("pass") == -1: continue
            if Pass_Conf.replace("pass", "fail") != Fail_Conf: continue
            Pair_Found = True
            break

        if Pair_Found: CR_Files += [(Fail_Conf[:Fail_Conf.rfind(".")], Pass_Conf[:Pass_Conf.rfind(".")])]
    return CR_Files


def fake_weight_snippet(Fail, Pass):
    return [
        "Import XAMPPplotting/RunConf/LepHadStau/Regions/%s.conf" % (Fail),
        "Import XAMPPplotting/RunConf/LepHadStau/Regions/%s.conf" % (Pass),
        "Region %s" % (Fail[:Fail.rfind("TauID") - 1]),
        "New_CondWeight AppliedFF",
        "   New_Conditional",
        "       Weight %s_SR_%s" % (Fail.split("_")[1], Fail.split("_")[3 if "CR" not in Fail else 5]),
        "       EvCut region IsRegion_%s = 1" % (Fail),
        "   End_Conditional",
        "End_CondWeight",
        "defineVar FFWeight AppliedFF",
        "@ApplyWeights",
        "\n\n\n### Require true light leptons on Monte Carlo",
        #"ifdef isMC", "    NumParCut SignalTrueLeptons = 1", "endif"
    ]


if __name__ == '__main__':
    RunConfDir = ResolvePath("XAMPPplotting/RunConf/LepHadStau/Regions/")
    Applied_FF = "%s/AppliedFF/" % (RunConfDir)
    CreateDirectory(Applied_FF)
    CR_Files = pair_id_antid_regions(RunConfDir)
    for Fail, Pass in CR_Files:
        if "IsoSS" in Fail: continue
        if "QCD" in Fail: continue
        NewCfg_Content = fake_weight_snippet(Fail, Pass) + [
            "\n\n\n### Only select events with real taus",
            "ifdef useDataDrivenEstimate",
            "   EvCut region IsRegion_%s = 1" % (Fail),
            "   ifdef isMC",
            "       NumParCut BaseTausReal > 0",
            "   endif",
            ### Does that
            "endif",
            "ifndef useDataDrivenEstimate",
            "   EvCut region IsRegion_%s = 1" % (Pass),
            "   ifdef isMC",
            "       NumParCut BaseTausReal > 0",
            "   endif",
            "endif",
        ]
        WriteList(NewCfg_Content, "%s/%s.conf" % (Applied_FF, Fail[:Fail.rfind("TauID") - 1]))
