from ClusterSubmission.Utils import WriteList, ResolvePath, CreateDirectory, FillWhiteSpaces
import os, argparse


class LepHadStauRegion(object):
    def __init__(
        self,
        region_name="SR",
        SignalTauSelection="Signal",
        select_signal_tau=True,
        LeptonsToSelect="Signal",
        LeptonsToVeto="Loose",
        LeptonFlavours=["Electrons", "Muons"],
        B_JetSelection="SignalBJets",
        Invert_WCut=False,
        Invert_TopCut=False,
        Invert_OSCut=False,
        JetCategories={
            "0jet": ["NumParCut SignalHardJets = 0"],
            "1jet": ["NumParCut SignalHardJets >= 1"],
            "PreSel": [],
        },
    ):
        self.__region = region_name
        self.__select_taus = SignalTauSelection
        self.__select_sig_taus = select_signal_tau

        self.__select_lepton = LeptonsToSelect
        self.__veto_lepton = LeptonsToVeto
        self.__lep_flavours = LeptonFlavours

        self.__b_jet_selection = B_JetSelection

        self.__invert_top = Invert_TopCut
        self.__invert_wjet = Invert_WCut
        self.__invert_os_selection = Invert_OSCut
        self.__jet_categories = JetCategories

    def write(self, out_dir):
        for category, cat_cuts in self.__jet_categories.iteritems():

            for flav in self.__lep_flavours:
                channel = "Chan_%s_%s_SR_%s_TauID_%s" % (flav[0:3], self.__region, category, "pass" if self.__select_sig_taus else "fail")
                channel = channel.replace("__", "_")
                ### Apply the light lepton selection
                cfg_content = [
                    "Import XAMPPplotting/RunConf/LepHadStau/RunConfig.conf",

                    #Chan_EleTau_SR_Presel_RNNTauID_pass
                    "Region %s" % (channel),
                    FillWhiteSpaces(4, "\n"),
                    "### Lepton multiplicity cuts",
                    "NumParCut %sLightLeptons = 1" % (self.__select_lepton),
                    "NumParCut %s%s = 1" % (self.__select_lepton, flav),
                    "NumParCut BaseTaus > 0 ",
                    "##### Trigger selection weight",
                    FillWhiteSpaces(2, "\n")
                ]

                ### Avoid to apply the combined trigger in the CRs
                if True or len(self.__region) == 0:
                    cfg_content += [
                        "CombCut OR",
                        "%sCondAppliedCut TriggerWeight%s = 1" % (FillWhiteSpaces(4), flav),
                        "%sCondAppliedCut TriggerWeightTaus%s = 1" % (FillWhiteSpaces(4), flav),
                        "End_CombCut",
                    ]
                else:
                    cfg_content += ["CondAppliedCut TriggerWeight%s = 1" % (flav)]
                if self.__select_lepton != self.__veto_lepton:
                    cfg_content += ["NumParCut %s%s = 0" % (self.__veto_lepton, flav)]
                ### Select signal taus or reject them
                cfg_content += ["NumParCut %sTaus = %d" % (self.__select_taus, 1 if self.__select_sig_taus else 0)]

                #### Then apply the common topological cuts
                tau_type = self.__select_taus if self.__select_sig_taus else "Base"
                q_reader_name = "%s%s_%sTau_Q" % (self.__select_lepton, flav, tau_type)

                cfg_content += [
                    FillWhiteSpaces(2, "\n"),
                    "#### Relative charge cut",
                    "New_MathReader *",
                    "%sAlias %s" % (FillWhiteSpaces(4), q_reader_name),
                    "%sParReader %s%s charge[0]" % (FillWhiteSpaces(4), self.__select_lepton, flav),
                    "%sParReader %sTaus charge[0]" % (FillWhiteSpaces(4), tau_type),
                    "End_MathReader",
                    "EvCut math %s %s 0" % (q_reader_name, ">" if self.__invert_os_selection else "<"),
                ]
                ### Topological cuts
                cfg_content += [
                    FillWhiteSpaces(2, "\n"),
                    "dRCut %s%s[0] %sTaus[0] < 4.0" % (self.__select_lepton, flav, tau_type),
                    "ParCut DiLep m[0] > 10",
                ]
                ### Top veto cuts
                cfg_content += [
                    FillWhiteSpaces(2, "\n"),
                    "### Top CR cut",
                    "NumParCut %s %s 0" % (self.__b_jet_selection, "=" if not self.__invert_top else ">"),
                ]
                ### And then the W CR cuts
                cfg_content += [
                    FillWhiteSpaces(2, "\n"),
                    "###W CR cut",
                    "CombCut %s" % ("AND" if self.__invert_wjet else "OR"),
                    "%sMtMetCut %s%s[0] MetTST %s 65" %
                    (FillWhiteSpaces(4), self.__select_lepton, flav, ">" if self.__invert_wjet else "<"),
                    "%sMtMetCut %s%s[0] MetTST %s 110" %
                    (FillWhiteSpaces(4), self.__select_lepton, flav, "<" if self.__invert_wjet else ">"),
                    "End_CombCut",
                ]

                cfg_content += [
                    FillWhiteSpaces(2, "\n"),
                    "#### Jet category cuts",
                ] + cat_cuts

                WriteList(cfg_content, "%s/%s.conf" % (out_dir, channel))


configurations = [
    LepHadStauRegion(region_name="", SignalTauSelection="Signal", select_signal_tau=True),
    LepHadStauRegion(region_name="", SignalTauSelection="Signal", select_signal_tau=False),
    ### W-CR
    LepHadStauRegion(region_name="CR_Wjets", SignalTauSelection="Signal", select_signal_tau=True, Invert_WCut=True),
    LepHadStauRegion(region_name="CR_Wjets", SignalTauSelection="Signal", select_signal_tau=False, Invert_WCut=True),
    #LepHadStauRegion(region_name="CR_WjetsSS", SignalTauSelection="Signal", select_signal_tau=True, Invert_WCut=True, Invert_OSCut=True),
    #LepHadStauRegion(region_name="CR_WjetsSS", SignalTauSelection="Signal", select_signal_tau=False, Invert_WCut=True, Invert_OSCut=True),

    ### Toop-CR (no AntiID taus to spare some space on the disk)
    LepHadStauRegion(region_name="CR_Top", SignalTauSelection="Signal", select_signal_tau=True, Invert_TopCut=True),

    #### QCD-CR
    LepHadStauRegion(region_name="CR_QCD",
                     SignalTauSelection="Signal",
                     select_signal_tau=True,
                     LeptonsToSelect="AntiIso",
                     LeptonsToVeto="Signal"),
    LepHadStauRegion(region_name="CR_QCD",
                     SignalTauSelection="Signal",
                     select_signal_tau=False,
                     LeptonsToSelect="AntiIso",
                     LeptonsToVeto="Signal"),

    #### SS-CR (anti iso)
    LepHadStauRegion(region_name="CR_AntiIsoSS",
                     SignalTauSelection="Signal",
                     select_signal_tau=True,
                     Invert_OSCut=True,
                     LeptonsToSelect="AntiIso",
                     LeptonsToVeto="Signal"),
    LepHadStauRegion(region_name="CR_AntiIsoSS",
                     SignalTauSelection="Signal",
                     select_signal_tau=False,
                     Invert_OSCut=True,
                     LeptonsToSelect="AntiIso",
                     LeptonsToVeto="Signal"),
    ### SS- CR (iso)
    LepHadStauRegion(region_name="CR_IsoSS",
                     SignalTauSelection="Signal",
                     select_signal_tau=True,
                     Invert_OSCut=True,
                     LeptonsToSelect="Signal",
                     LeptonsToVeto="AntiIso"),
    LepHadStauRegion(region_name="CR_IsoSS",
                     SignalTauSelection="Signal",
                     select_signal_tau=False,
                     Invert_OSCut=True,
                     LeptonsToSelect="Signal",
                     LeptonsToVeto="AntiIso"),

    #LepHadStauRegion(region_name="SR", SignalTauSelection="RNNSignal", select_signal_tau=True),
    #LepHadStauRegion(region_name="SR", SignalTauSelection="RNNSignal", select_signal_tau=False),
    ### W-CR
    #LepHadStauRegion(region_name="Wjets", SignalTauSelection="RNNSignal", select_signal_tau=True, Invert_WCut=True),
    #LepHadStauRegion(region_name="Wjets", SignalTauSelection="RNNSignal", select_signal_tau=False, Invert_WCut=True),
    ### Toop-CR (no AntiID taus to spare some space on the disk)
    #LepHadStauRegion(region_name="Wjets", SignalTauSelection="RNNSignal", select_signal_tau=True, Invert_TopCut=True),
    #### QCD-CR
    #LepHadStauRegion(region_name="QCD",
    #                 SignalTauSelection="RNNSignal",
    #                 select_signal_tau=True,
    #                 LeptonsToSelect="AntiIso",
    #                 LeptonsToVeto="Signal"),
    #LepHadStauRegion(region_name="QCD",
    #                 SignalTauSelection="RNNSignal",
    #                 select_signal_tau=False,
    #                 LeptonsToSelect="AntiIso",
    #                 LeptonsToVeto="Signal"),
]

out_dir = ResolvePath("XAMPPplotting/RunConf/LepHadStau/Regions/")
for cfg in configurations:
    cfg.write(out_dir)
