#!/usr/bin/env python
import os, sys, logging, time, math, ROOT
from ClusterSubmission.Utils import convertStdVector, WriteList, ReadListFromFile, CreateDirectory, setupBatchSubmitArgParser, id_generator, setup_engine, CheckRemainingProxyTime
from ClusterSubmission.RucioListBuilder import downloadDataSets, RUCIO_RSE
from XAMPPplotting.PeriodRunConverter import GetPeriodRunConverter
from XAMPPplotting.CheckMetaData import GetNormalizationDB
from ClusterSubmission.AMIDataBase import getAMIDataBase
from ClusterSubmission.DatasetDownloader import DataSetFileHandler


class XAMPPplottingSample(DataSetFileHandler):
    def __init__(
            self,
            ### Common arguments of the DataSetFileHandler
            rucio_container,
            dest_rse,
            download=False,
            merge=False,
            download_dir="/tmp/",
            destination_dir="/tmp",
            cluster_engine=None,
            logical_name="",
            hold_jobs=[],
            max_merged_size=20 * 1024 * 1024 * 1024,

            ####
            config_out_dir="",
            ### Import common config file mapping the LHE weights
            ### to their actual names. Config file is generator dependent
            name_lhe=True,
            ### Use the particular susy processes to normalize the sample
            ### properly
            susy_fs_norm=False,
            ### Write common meta_data file
            write_meta_file=False,
            ### Run config for duplication free check
            duplicate_free_cfg="",
            ### Write the prw config import statement
            import_prw_cfg=False):

        self.__in_cfg_dir = config_out_dir
        self.__name_lhe = name_lhe
        self.__susy_fs_norm = susy_fs_norm
        self.__write_meta_file = write_meta_file
        self.__duplicate_free_cfg = duplicate_free_cfg
        self.__import_prw_cfg = import_prw_cfg
        self.__merge_helper = ROOT.XAMPP.MergeHelper()

        DataSetFileHandler.__init__(
            self,
            rucio_container=rucio_container,
            dest_rse=dest_rse,
            download=download,
            merge=merge,
            destination_dir=destination_dir,
            cluster_engine=cluster_engine,
            logical_name=logical_name,
            hold_jobs=hold_jobs,
            max_merged_size=max_merged_size,
            download_dir=download_dir,
            files_per_merge_job=4,
        )

    def meta_file(self):
        return "%s/TmpMetaFile.root" (self.ds_final_dir())

    def ds_files_with_size(self):
        self.__merge_helper.add_file(convertStdVector(self.root_files()))
        files = self.__merge_helper.get_good_files()
        sizes = self.__merge_helper.get_sizes()
        return [(files[i], sizes[i]) for i in range(len(sizes))]

    def _is_good_file(self, f):
        ### if the files are merged then the good-file check is performed later anyway
        return self.__merge_helper.add_file(f)
        #return ROOT.XAMPP.CorruptedFileChecker.isFileOk(f)

    def _split_file(self, x):
        return self.__merge_helper.split_file(x)

    def write_cfg(self):
        LHE_Variations = {
            "Sherpa": "XAMPPplotting/Misc/TruthSystematics/Sherpa_WeightVariations.conf",
            "aMC": "XAMPPplotting/Misc/TruthSystematics/aMCatNLO_WeightVariations.conf",
            "PowHeg": "XAMPPplotting/Misc/TruthSystematics/PowHeg_WeightVariations.conf",
        }
        if len(self.root_files()) == 0:
            logging.warning("No root files could be found")
            return
        CreateDirectory(self.__in_cfg_dir, False)
        ### Write the temporary meta-data tree if requested
        if self.__write_meta_file:
            tmp_cfg = WriteList(["Input %s" % (f) for f in self.root_files()], "/tmp/%s.conf" % (id_generator(81)))
            meta_cmd = "TemporaryMetaTree -i %s -o %s" % (options.outdir, tmp_cfg, self.meta_file())
            meta = not os.system(meta_cmd)
            os.system("rm %s" % (tmp_cfg))
            if not meta:
                logging.error("Could not create the temporary metadata tree for %s" % (self.logical_name()))
                return

        ### Setup a new instance of the database
        try:
            meta_db = GetNormalizationDB(self.root_files() if not self.__write_meta_file else [self.meta_file()], True)
        except:
            logging.warning("Failed to setup meta database")
            return
        ### Ensure that the data is free from duplicates
        if meta_db.isData() and len(self.__duplicate_free_cfg) > 0:
            tmp_cfg = WriteList(["Input %s" % (f) for f in self.root_files()] +
                                ([] if not self.__write_meta_file else ["MetaInput %s" % (self.meta_file())]),
                                "/tmp/%s.conf" % (id_generator(81)))
            dupl_cmd = "CheckDuplicatedEvents -r %s -i %s -o %s/duplicated_ev.txt " % (self.__duplicate_free_cfg, tmp_cfg,
                                                                                       self.ds_final_dir())
            indexing = not os.system(dupl_cmd)
            os.system("rm %s" % (tmp_cfg))
            if not indexing:
                logging.error("Something went wrong in setting up the duplication free index")
                return
        ### I think we've everything then
        MyxSecDB = ROOT.SUSY.CrossSectionDB()
        logging.info("Write input config %s to %s" % (self.logical_name(), self.__in_cfg_dir))

        with open("%s/%s.conf" % (self.__in_cfg_dir, self.logical_name()), "w") as InCfgFile:
            InCfgFile.write("#########################################################################################\n")
            InCfgFile.write("#File generated at %s by %s\n" % (time.strftime("%Y-%m-%d %H:%M:%S"), os.getenv("USER")))
            InCfgFile.write("#Rucio container used to generate dataset: %s\n" % (self.container()))
            InCfgFile.write("#########################################################################################\n")
            if not meta_db.isData():
                InCfgFile.write("#Physical samples inside this config: \n")
                for DSID in meta_db.GetListOfMCSamples():
                    if DSID == 0: continue
                    InCfgFile.write("#   --- %s (%d) \n" % (MyxSecDB.name(DSID), DSID))
            else:
                InCfgFile.write("# Recorded runs in this config: \n")
                for run in meta_db.GetRunNumbers():
                    InCfgFile.write("#    --- %d\n" % (run))
                if len(self.__duplicate_free_cfg) > 0:
                    InCfgFile.write("#########################################################################################\n")
                    InCfgFile.write("# It might be possible that data events occur twice in the files. The current recommendation\n")
                    InCfgFile.write("# is to check the n-tuples by hand the n-rtuples for these events. The following file lists\n")
                    InCfgFile.write("# all duplications and parses them to the code such that XAMPP only selects the first occurance\n")
                    InCfgFile.write("noDuplicateData %s/duplicated_ev.txt\n" % (self.ds_final_dir()))
            InCfgFile.write("#########################################################################################\n")
            InCfgFile.write("SampleName %s\n" % (self.logical_name()))
            for S in sorted(self.merged_files()):
                InCfgFile.write("Input %s\n" % (S))
            if self.__write_meta_file: InCfgFile.write("MetaInput %s\n" % (self.meta_file()))
            InCfgFile.write("#########################################################################################\n")
            if not meta_db.isData() and self.__name_lhe:
                for Generator, Config_File in LHE_Variations.iteritems():
                    if self.logical_name().lower().find(Generator.lower()) != -1:
                        InCfgFile.write("Import %s\n" % (Config_File))
                        break

            if not self.__susy_fs_norm: InCfgFile.write("NoSUSYpidWeight\n")
            if self.__import_prw_cfg:
                InCfgFile.write("#Allthough the framework does no longer support the reweighting of the pile-up on the fly\n")
                InCfgFile.write("#the lumicalc files are needed to run diagnostic plots like the runNumber histograms\n")
                InCfgFile.write("Import XAMPPplotting/Misc/LumiCalcFiles.conf\n")
            if self.logical_name() in ["Sherpa221_VVV"]:
                InCfgFile.write("define applySherpa221_MCClassFix\n")
            InCfgFile.close()


def GetProduction(DS):
    if DS.find(":") == -1:
        logging.error("No scope can be extracted from %s. Please give <scope>:<scope>.XAMPP.<Analysis>.<Production>" % (DS))
        return ""
    SubmittingAcc = DS[:DS.find(":")]
    DSName = DS[DS.find(SubmittingAcc,
                        DS.find(":") + 1) + len(SubmittingAcc) + 1:len(DS) if DS.rfind("_XAMPP") != DS.rfind("_") else DS.rfind("_")]
    ## Datasets satisfy the pattern
    ### <scope>:<scope>.XAMPP.<Analysis>.<Production>.<Physics>
    Tokens = DSName.split(".")
    return ".".join([Tokens[i] for i in range(2, len(Tokens) - 1)])


def GetAnalysis(DS):
    SubmittingAcc = DS[:DS.find(":")]
    DSName = DS[DS.find(SubmittingAcc,
                        DS.find(":") + 1) + len(SubmittingAcc) + 1:len(DS) if DS.rfind("_XAMPP") != DS.rfind("_") else DS.rfind("_")]
    return DSName.split(".")[1]


if __name__ == '__main__':
    parser = setupBatchSubmitArgParser()
    parser.set_defaults(MergeTime="08:00:00")
    parser.set_defaults(Merge_vmem=8000)
    parser.add_argument('-i', '--input', help='Input list with all datasets on the groupdisk', required=True)
    parser.add_argument('-r', '-R', '--RSE', help='specify RSE storage element which should be read', default=RUCIO_RSE)
    parser.add_argument('-o', '--outdir', help='Output directory to put file list(s) into', dest='outdir', required=True)
    parser.add_argument('-p', "--production", help="Which production shall be depcited", required=True)
    parser.add_argument("--download", help="start the download of the file to a given directory ", action="store_true", default=False)
    parser.add_argument("--localpath",
                        help="Destination of downloaded datasets.",
                        default=setupBatchSubmitArgParser().get_default("BaseFolder") + "/XAMPP_Ntuples/")
    parser.add_argument("--nameLHE",
                        help="Add the Import statement to rename the LHE variations. If applicable",
                        action="store_true",
                        default=False)
    parser.add_argument("--analysis", "-a", help="What is the analysis stream to use", required=True)
    parser.add_argument("--disableSUSY",
                        help="Switch off the SUSY FS in general by adding 'NoSUSYpidWeight' to the config files",
                        action="store_true",
                        default=False)
    parser.add_argument("--merge", help="Merge the files together", action="store_true", default=False)
    parser.add_argument("--max_size", help="Maximum size of the final files in GB", type=float, default=20)
    parser.add_argument("--HoldJob", help='Wait until the jobs are finished before starting the merge process', default=[], nargs="+")
    options = parser.parse_args()

    InputDatasets = ReadListFromFile(options.input)
    if not InputDatasets:
        logging.error("No input ds provided...")
        sys.exit(1)
    ## Make sure that everything can be downloaded properly
    CheckRemainingProxyTime()

    if options.download: downloadDataSets(InputDatasets, options.localpath)

    cluster_engine = setup_engine(options)
    samples = []
    hold_jobs = [H for H in options.HoldJob]
    prev_hold_block = []
    n = 0
    for DS in sorted(InputDatasets):
        Production = GetProduction(DS)
        if Production != options.production:
            logging.info("Discarding production %s not matching to %s" % (Production, options.production))
            continue
        if len(options.analysis) > 0 and options.analysis != GetAnalysis(DS):
            logging.info("No valid analysis %s vs. %s" % (options.analysis, GetAnalysis(DS)))
            continue

        Logical_Name = DS[DS.find(Production) + len(Production) + 1:len(DS) if DS.rfind("_XAMPP") != DS.rfind("_") else DS.rfind("_")]
        plotting_smp = XAMPPplottingSample(
            rucio_container=DS,
            dest_rse=options.RSE,
            download=options.download,
            merge=options.merge,
            download_dir=options.localpath,
            destination_dir="%s/%s_%s/" % (options.localpath, options.analysis, options.production),
            cluster_engine=cluster_engine,
            logical_name=Logical_Name,
            config_out_dir=options.outdir,
            name_lhe=True,
            max_merged_size=options.max_size * 1024 * 1024 * 1024,
            ### Make sure that the GGM samples are not weighted w.r.t. final state
            susy_fs_norm=not options.disableSUSY and (Logical_Name.find("GGM") == -1 or GetAnalysis(DS) != "SUSY4L"),
            ### Write common meta_data file
            write_meta_file=False,
            ### Run config for duplication free check
            duplicate_free_cfg="",
            ### Write the prw config import statement
            import_prw_cfg=True,
            hold_jobs=hold_jobs)

        if options.merge: plotting_smp.prepare_merge()
        samples += [plotting_smp]
        prev_hold_block += [cluster_engine.subjob_name("Move-%s" % (plotting_smp.logical_name()))]
        n += len(plotting_smp.merged_files())
        if n > (25 if options.download else 5):
            n = 0
            hold_jobs = [h for h in prev_hold_block]
            prev_hold_block = []
        if not plotting_smp.submit_merge(): exit(1)
        plotting_smp.write_cfg()

    if options.merge:
        cluster_engine.submit_clean_all(prev_hold_block + hold_jobs)
        cluster_engine.finish()
