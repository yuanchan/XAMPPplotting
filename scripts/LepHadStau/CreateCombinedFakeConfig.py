#!/usr/bin/env python
import os, sys, time, math, logging, ROOT, argparse
from ClusterSubmission.Utils import CreateDirectory, ReadListFromFile, ResolvePath, FillWhiteSpaces, WriteList, MakePathResolvable
from XAMPPplotting.FileUtils import ReadInputConfig
from XAMPPplotting.CheckMetaData import GetNormalizationDB
from XAMPPplotting.CalculateLumiFromIlumicalc import CalculateRecordedLumi
from XAMPPplotting.FileStructureHandler import GetStructure, ClearServices
from XAMPPplotting.TauCompare import get_property
from XAMPPplotting.Utils import setupBaseParser

from pprint import pprint


def read_out_ff(observable="", collection="", off_set=8):
    out = [
        "%sParticle %s" % (FillWhiteSpaces(off_set), collection),
    ]
    if "PtAbsEta" in observable:
        out += [
            "%sxVariable ParReader %s pt" % (FillWhiteSpaces(off_set), collection),
            "%syVariable ParReader %s |eta|" % (FillWhiteSpaces(off_set), collection),
        ]
    elif "PtEta" in observable:
        out += [
            "%sxVariable ParReader %s pt" % (FillWhiteSpaces(off_set), collection),
            "%syVariable ParReader %s eta" % (FillWhiteSpaces(off_set), collection),
        ]
    elif "MetDPhi" in observable:
        out += [
            "%sxVariable dPhiToMetReader |%s| MetTST" % (FillWhiteSpaces(off_set), collection),
        ]
    elif "Pt" in observable:
        out += ["%sxVariable ParReader %s pt" % (FillWhiteSpaces(off_set), collection)]
    else:
        logging.error("Unkown obsevable %s" % (observable))
        exit(1)
    return out


def constrain_trigger(tauDef="", off_set=8):
    if tauDef == "LepTrigger":
        return [
            "%sCombCut OR" % (FillWhiteSpaces(off_set)),
            "%sCondAppliedCut TriggerWeightMuons = 1" % (FillWhiteSpaces(off_set + 4)),
            "%sCondAppliedCut TriggerWeightElectrons = 1" % (FillWhiteSpaces(off_set + 4)),
            "%sEnd_CombCut" % (FillWhiteSpaces(off_set)),
        ]
    elif tauDef == "TauTrigger":
        return [
            "%sCombCut OR" % (FillWhiteSpaces(off_set)),
            "%sCondAppliedCut TriggerWeightTausMuons = 1" % (FillWhiteSpaces(off_set + 4)),
            "%sCondAppliedCut TriggerWeightTausElectrons = 1" % (FillWhiteSpaces(off_set + 4)),
            "%sEnd_CombCut" % (FillWhiteSpaces(off_set)),
        ]
    elif tauDef == "Base":
        logging.info("No trigger constrain")
    else:
        logging.error("Unkown constrain %s" % (tauDef))
        exit(1)
    return []


if __name__ == '__main__':
    parser = setupBaseParser(argparse.ArgumentParser(description="Script to create the fake-factor config for application"))
    parser.add_argument("--FakeFactorFile",
                        help="Where is the file located",
                        default="XAMPPplotting/RunConf/LepHadStau/FF_Files/TauFF_CB.root")
    parser.add_argument("--OutFile",
                        help="Where should the config be stored",
                        default="XAMPPplotting/data/RunConf/LepHadStau/CommonConfigs/FakeFactorWeights_Combined.conf")
    parser.set_defaults(config=[ResolvePath("XAMPPplotting/python/DSConfigs/Stau_MC.py")])
    options = parser.parse_args()

    # Analyze the samples and adjust the PlottingOptions
    FileStructure = GetStructure(options, use_data_ref=True)

    FileLocation = ResolvePath(options.FakeFactorFile)
    if not FileLocation or not os.path.exists(FileLocation):
        logging.error("Where is my fake factor file")
        exit(1)

    TFile = ROOT.TFile.Open(FileLocation, "READ")
    if not TFile or not TFile.IsOpen():
        logging.error("You gave some file, but that is not a ROOT file?")
        exit(1)
    FileLocation = MakePathResolvable(options.FakeFactorFile)
    if not len(FileLocation): exit(1)

    ### Retrieve the Fake-Factor file names
    FF_Histos = [Key.GetName() for Key in TFile.GetListOfKeys()]
    ### We do not need the ROOTFile -> close it
    TFile.Close()
    ## All fake factor files should follow the pattern
    ### -- FF_combined_<Systematic>_<FF_Name>_<MinEta>_<MaxEta>_Prong_<NTracks>_Range_<FirstRun>_<LastRun>
    tot_lumi = 0.

    ConfigPath = ResolvePath("XAMPPplotting/InputConf/LepHadStau/Data/")
    Periods = [Cfg[:Cfg.rfind(".")] for Cfg in os.listdir(ConfigPath) if Cfg.find("data") == 0]
    ROOT_Files = []
    for P in Periods:
        ROOT_Files += ReadInputConfig("%s/%s.conf" % (ConfigPath, P))
    #for Run in GetNormalizationDB(ROOT_Files).GetRunNumbers():
    #    tot_lumi += CalculateRecordedLumi(Run)

    GetStructure().Next()
    tot_lumi = GetStructure().GetConfigSet().GetLumi()

    OutFile = [
        "####################################################################################################",
        "# This is an automatic generated config file to apply the fake-factor weights  in the ",
        "# lep-had stau analysis. It needs to be included by the general XAMPPplotting/RunConfig ",
        "# By default the FF weight returns positive weights for data and negative weights for MC.",
        "# In this file only the weights are created, needed to be explicitly included in each region.",
        "####################################################################################################",
        "#File generated at %s by %s" % (time.strftime("%Y-%m-%d %H:%M:%S"), os.getenv("USER"))
    ]

    ### Create the dictonary to group the fake-factors into SR & Systematics
    FF_FileDict = {}
    for ff in FF_Histos:
        ## For now skip the isolation stuff
        if ff[0] == "I": continue
        ### For testing purposes the R factor as well
        #if ff[0] == "R": continue

        Chan = get_property(ff, "nominal")
        CR = get_property(ff, "CR")
        SR = get_property(ff, "SR")
        #if CR == "QCD": continue

        if Chan not in FF_FileDict.iterkeys(): FF_FileDict[Chan] = {}
        if SR not in FF_FileDict[Chan].iterkeys():
            FF_FileDict[Chan][SR] = {}
        if CR not in FF_FileDict[Chan][SR].iterkeys():
            FF_FileDict[Chan][SR][CR] = []

        FF_FileDict[Chan][SR][CR] += [ff]
        FF_FileDict[Chan][SR][CR].sort()
    for channel, channel_dict in FF_FileDict.iteritems():
        for SR, sr_dict in channel_dict.iteritems():
            OutFile += ["FakeFactorWeight %s_SR_%s" % (channel, SR)]
            all_CRs = [s for s in sr_dict.iterkeys()]
            for CR, variables in sr_dict.iteritems():
                for h in variables:
                    observable = get_property(h, "Observable")
                    Prong = get_property(h, "Prong")
                    tauDef = get_property(h, "TauDef")

                    OutFile += [
                        "%sAddFake" % (FillWhiteSpaces(4)),
                        "%sFakeFactorHisto %s %s" % (FillWhiteSpaces(8), FileLocation, h),
                        "%sSetProcessBin %d" % (FillWhiteSpaces(8), all_CRs.index(CR)),
                        "%sSetFakeType %d" % (FillWhiteSpaces(8), all_CRs.index(CR)),
                        "%sSetLumi %f" % (FillWhiteSpaces(8), tot_lumi),
                        "%sSetEndPosition 1" % (FillWhiteSpaces(8)),
                    ]
                    if h[0] == "F":
                        OutFile += [
                            "%sifdef ClosureTest" % (FillWhiteSpaces(8)),
                            "%sFreezeSign" % (FillWhiteSpaces(12)),
                            "%sendif" % (FillWhiteSpaces(8)),
                        ]
                    else:
                        OutFile += ["%sFreezeSign" % (FillWhiteSpaces(8))]
                    particle_collection = "AntiIDTaus" if observable.find("Tau") != -1 else "BaseLightLeptons"
                    particle_collection_mc = "AntiIDTausReal" if observable.find("Tau") != -1 else "BaseTrueLeptons"
                    OutFile += [
                        "%sifdef isData" % (FillWhiteSpaces(8)),
                    ] + read_out_ff(observable=observable, collection=particle_collection, off_set=12) + [
                        "%sendif" % (FillWhiteSpaces(8)),
                        "%sifdef ClosureTest" % (FillWhiteSpaces(8)),
                    ] + read_out_ff(observable=observable, collection=particle_collection, off_set=12) + [
                        "%sendif" % (FillWhiteSpaces(8)),
                        "%sifdef isMC" % (FillWhiteSpaces(8)),
                        "%sifndef ClosureTest" % (FillWhiteSpaces(12))
                    ] + read_out_ff(observable=observable, collection=particle_collection_mc, off_set=16) + [
                        "%sendif" % (FillWhiteSpaces(12)),
                        "%sendif" % (FillWhiteSpaces(8)),
                    ]
                    ### Insert the cuts
                    OutFile += [
                        "%sCombCut AND" % (FillWhiteSpaces(8)),
                    ] + constrain_trigger(tauDef, 8) + [
                        "%sifdef isData" % (FillWhiteSpaces(8)),
                        "%sParCut %s NTrks[0] = %s" % (FillWhiteSpaces(12), particle_collection, Prong),
                        "%sendif" % (FillWhiteSpaces(8)),
                        "%sifdef ClosureTest" % (FillWhiteSpaces(8)),
                        "%sParCut %s NTrks[0] = %s" % (FillWhiteSpaces(12), particle_collection, Prong),
                        "%sendif" % (FillWhiteSpaces(8)),
                        "%sifdef isMC" % (FillWhiteSpaces(8)),
                        "%sifndef ClosureTest" % (FillWhiteSpaces(12)),
                        "%sParCut %s NTrks[0] = %s" % (FillWhiteSpaces(16), particle_collection_mc, Prong),
                        "%sendif" % (FillWhiteSpaces(12)),
                        "%sendif" % (FillWhiteSpaces(8)),
                        "%sEnd_CombCut" % (FillWhiteSpaces(8)),
                    ]
                    OutFile += ["%sEnd_Fake" % (FillWhiteSpaces(4))]

            OutFile += ["End_FakeFactorWeight", "\n\n"]

    WriteList(OutFile, options.OutFile)
