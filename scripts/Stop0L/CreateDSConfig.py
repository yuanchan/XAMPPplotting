#!/usr/bin/env python

import os
import sys
import commands
import mimetypes
import re

samplesToExclude = [
    "singleTop_DS_Input", "singleTop_Herwigpp_a818_r7676_Input", "singleTop_radHi_a818_r7676_Input", "singleTop_radLo_a818_r7676_Input",
    "ttGamma_a821_r7676_Input", 'ttbar_Herwigpp_Input', 'ttbar_McAtNlo_a821_r7676_Input', 'ttbar_radHi_Input', 'ttbar_radLo_Input',
    'ttbar_Sherpa221_Input'
]

if __name__ == '__main__':

    import argparse
    parser = argparse.ArgumentParser(description="Script for creating DSconfigs", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-i', '--input', dest='indir', help='Folder with files', required=True)
    parser.add_argument('-o', '--output', help='Output file name', required=True)
    parser.add_argument('-p', '--pattern', help='pattern to grep for in signals', default=[], nargs='+')
    options = parser.parse_args()

    files = os.listdir(options.indir)

    if len(files) == 0:
        print 'No files found, exiting...'
        sys.exit(1)

    FoundFiles = {}
    for cand in files:
        if 'TT' in cand or 'GG' in cand:
            if not 'Signal' in FoundFiles.iterkeys(): FoundFiles['Signal'] = {}
            FoundAll = True
            if len(options.pattern) > 0:
                for ipatter in options.pattern:
                    if not ipatter in cand: FoundAll = False
            if not FoundAll: continue
            if not cand in FoundFiles['Signal'].iterkeys(): FoundFiles['Signal'][cand] = []
            FoundFiles['Signal'][cand].append('sampletype=SampleTypes.Signal')
        elif 'Data' in cand:
            if not 'Data' in FoundFiles.iterkeys(): FoundFiles['Data'] = {}
            #             continue
            if not cand in FoundFiles['Data'].iterkeys(): FoundFiles['Data'][cand] = []
            FoundFiles['Data'][cand].append('sampletype=SampleTypes.Data')
            FoundFiles['Data'][cand].append('lumi=36.1')
        else:
            if not 'Reducible' in FoundFiles.iterkeys(): FoundFiles['Reducible'] = {}

            skip = False
            for e in samplesToExclude:
                if e in cand: skip = True
            if skip: continue

            if not cand in FoundFiles['Reducible'].iterkeys(): FoundFiles['Reducible'][cand] = []
            FoundFiles['Reducible'][cand].append('sampletype=SampleTypes.Reducible')
            #Irreducible

    with open(options.output, "w") as fout:
        fout.write("#! /usr/bin/env python\nfrom XAMPPplotting.Defs import *\n\n")
        fout.write("BasePath = '%s/'\n\n" % options.indir)

        for type in sorted(FoundFiles.iterkeys()):
            for Sample in sorted(FoundFiles[type].iterkeys(), key=lambda s: s.lower()):
                DSConfigString = "%s = DSconfig(name='%s',filepath=BasePath+'%s'" % (Sample.replace('.root', ''), Sample.replace(
                    '.root', ''), Sample)
                for item in FoundFiles[type][Sample]:
                    DSConfigString += ',%s' % item
                DSConfigString += ')\n'
                fout.write(DSConfigString)
            fout.write('\n')
