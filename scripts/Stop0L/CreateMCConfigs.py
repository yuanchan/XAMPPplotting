#!/usr/bin/env python

import os
import sys
import commands
import mimetypes
import re

# This scripts creates Input Config files from filelists containing the paths to the grid-output of a production
#
# Usage: python scripts/Stop0L/CreateMCConfigs.py -i <directory containing dcap file lists> -o <output directory for InputConfig files>
# e.g. python scripts/Stop0L/CreateMCConfigs.py -i /ptmp/mpp/niko/Stop0L/Filelists/16F1/ -o data/InputConf/Stop0L/16F1/

date = commands.getoutput("date --iso")

DSIDs = {

    # Background MC
    #     "ttbarMET200":  [[407012], ["ttbarPowhegNonAHMET200."]], # keep all events
    #     "ttbar":        [[410000], ["ttbarPowhegNonAH."]],       # keep only events with GenFiltMET < 200.
    #
    #     "WtMET200":         [[407019,407021], ["WtPowhegMET200.","WtbarPowhegMET200."]],    # keep all events
    #     "Wt":               [[410013,410014], ["WtPowheg.","WtbarPowheg."]],                # keep only events with GenFiltMET < 200.
    #     "singleTopTChan":   [[410011,410012], ["singleTPowheg_tChan.","singleTbarPowheg_tChan."]],
    #     "singleTopSChan":   [[410025,410026], ["singleTPowheg_sChanNonAH.","singleTbarPowheg_sChanNonAH."]],

    # alternative: merge ttbar and single top:
    "ttbar": [[410000, 407012, 407322], ["ttbarPowhegNonAH.", "ttbarPowhegNonAHMET200.", "ttbarPowhegNonAHMET300."]],
    "ttbar_Sherpa221": [[410249, 410251, 410252, 410250], ["ttbarSherpa"]],
    "ttbarPowheg8": [[410500], ["ttbarPowheg8NonAH."]],
    "ttbar_radHi": [[407032, 410001], ["ttbarPhPyradHiMET200.", "ttbarPhPyradHi."]],
    "ttbar_radLo": [[407036, 410002], ["ttbarPhPyradLoMET200.", "ttbarPhPyradLo."]],
    "ttbar_Herwigpp": [[407040, 410004], ["ttbarPhHerwigppMET200.", "ttbarPhHerwigpp."]],
    "ttbar_McAtNlo": [[410225], ["ttbaraMcAtNlo."]],
    "singleTop": [[407019, 407021, 410013, 410014, 410011, 410012, 410025, 410026],
                  [
                      "WtPowhegMET200.", "WtbarPowhegMET200.", "WtPowheg.", "WtbarPowheg.", "singleTPowheg_tChan.",
                      "singleTbarPowheg_tChan.", "singleTPowheg_sChanNonAH.", "singleTbarPowheg_sChanNonAH."
                  ]],
    "singleTop_Herwigpp": [[410147, 410148], ["WtHerwigpp.", "WtbarHerwigpp."]],
    "singleTop_radHi":
    [[410018, 410020, 410099, 410101, 410107, 410109],
     ["singleT_tChan_radH.", "singleTbar_tChan_radH.", "Wt_radH.", "Wtbar_radH.", "singleT_sChan_radH.", "singleTbar_sChan_radH."]],
    "singleTop_radLo":
    [[410017, 410019, 410100, 410102, 410108, 410110],
     ["singleT_tChan_radL.", "singleTbar_tChan_radL.", "Wt_radL.", "Wtbar_radL.", "singleT_sChan_radL.", "singleTbar_sChan_radL."]],
    "singleTop_DS": [[410062, 10063], ["WtDSt.", "WtbarDS."]],
    "Diboson": [[
        361064,
        361065,
        361066,
        361067,
        361068,
        361077,
        361091,
        361092,
        361093,
        361094,
        361095,
        361096,
        361097,
    ], [
        "Diboson",
    ]],

    #     "ttW":      [[], ["A14NNPDF23LO_ttW."]],
    #     "ttZnnqq":  [[], ["A14NNPDF23LO_ttZnnqq."]],
    #     "Wenu":     [[], ["Sherpa_CT10_Wenu_BFilter.","Sherpa_CT10_Wenu_CFilterBVeto.","Sherpa_CT10_Wenu_CVetoBVeto."]],
    #     "Wmunu":    [[], ["Sherpa_CT10_Wmunu_BFilter.","Sherpa_CT10_Wmunu_CFilterBVeto.","Sherpa_CT10_Wmunu_CVetoBVeto."]],
    #     "Wtaunu":   [[], ["Sherpa_CT10_Wtaunu_BFilter.","Sherpa_CT10_Wtaunu_CFilterBVeto.","Sherpa_CT10_Wtaunu_CVetoBVeto."]],
    #     "Zee":      [[], ["Sherpa_CT10_Zee_BFilter.","Sherpa_CT10_Zee_CFilterBVeto.","Sherpa_CT10_Zee_CVetoBVeto."]],
    #     "Zmumu":    [[], ["Sherpa_CT10_Zmumu_BFilter.","Sherpa_CT10_Zmumu_CFilterBVeto."]],
    #     "Znunu":    [[], ["Sherpa_CT10_Znunu_BFilter.","Sherpa_CT10_Znunu_CFilterBVeto.","Sherpa_CT10_Znunu_CVetoBVeto."]],
    #     "Ztautau":  [[], ["Sherpa_CT10_Ztautau_BFilter.","Sherpa_CT10_Ztautau_CFilterBVeto.","Sherpa_CT10_Ztautau_CVetoBVeto."]],

    # alternative: merge ttbar+V, W*jets and Z+jets:
    "ttV": [[], ["aMcAtNlo_ttW.", "aMcAtNlo_ttZnunu", "aMcAtNlo_ttZqq", "aMcAtNlo_ttmumu", "aMcAtNlo_tttautau", "aMcAtNlo_ttee"]],
    "ttGamma": [[410083, 410084, 410088, 410089, 407320], ["ttgammaMadGraph", "aMcAtNlo_ttgamma"]],
    "Wjets": [[],
              [
                  "Sherpa_CT10_Wenu_BFilter.", "Sherpa_CT10_Wenu_CFilterBVeto.", "Sherpa_CT10_Wenu_CVetoBVeto.",
                  "Sherpa_CT10_Wmunu_BFilter.", "Sherpa_CT10_Wmunu_CFilterBVeto.", "Sherpa_CT10_Wmunu_CVetoBVeto.",
                  "Sherpa_CT10_Wtaunu_BFilter.", "Sherpa_CT10_Wtaunu_CFilterBVeto.", "Sherpa_CT10_Wtaunu_CVetoBVeto."
              ]],
    "Zjets": [[],
              [
                  "Sherpa_CT10_Zee_BFilter.", "Sherpa_CT10_Zee_CFilterBVeto.", "Sherpa_CT10_Zee_CVetoBVeto.", "Sherpa_CT10_Zmumu_BFilter.",
                  "Sherpa_CT10_Zmumu_CFilterBVeto.", "Sherpa_CT10_Znunu_BFilter.", "Sherpa_CT10_Znunu_CFilterBVeto.",
                  "Sherpa_CT10_Znunu_CVetoBVeto.", "Sherpa_CT10_Ztautau_BFilter.", "Sherpa_CT10_Ztautau_CFilterBVeto.",
                  "Sherpa_CT10_Ztautau_CVetoBVeto."
              ]],
    "Wjets_Sherpa22":
    [[],
     [
         "Sherpa_NNPDF30NNLO_Wenu_BFilter.", "Sherpa_NNPDF30NNLO_Wenu_CFilterBVeto.", "Sherpa_NNPDF30NNLO_Wenu_CVetoBVeto.",
         "Sherpa_NNPDF30NNLO_Wmunu_BFilter.", "Sherpa_NNPDF30NNLO_Wmunu_CFilterBVeto.", "Sherpa_NNPDF30NNLO_Wmunu_CVetoBVeto.",
         "Sherpa_NNPDF30NNLO_Wtaunu_BFilter.", "Sherpa_NNPDF30NNLO_Wtaunu_CFilterBVeto.", "Sherpa_NNPDF30NNLO_Wtaunu_CVetoBVeto."
     ]],
    "Zjets_Sherpa22":
    [[],
     [
         "Sherpa_NNPDF30NNLO_Zee_BFilter.", "Sherpa_NNPDF30NNLO_Zee_CFilterBVeto.", "Sherpa_NNPDF30NNLO_Zee_CVetoBVeto.",
         "Sherpa_NNPDF30NNLO_Zmumu_BFilter.", "Sherpa_NNPDF30NNLO_Zmumu_CFilterBVeto.", "Sherpa_NNPDF30NNLO_Zmumu_CVetoBVeto",
         "Sherpa_NNPDF30NNLO_Znunu_BFilter.", "Sherpa_NNPDF30NNLO_Znunu_CFilterBVeto.", "Sherpa_NNPDF30NNLO_Znunu_CVetoBVeto.",
         "Sherpa_NNPDF30NNLO_Ztautau_BFilter.", "Sherpa_NNPDF30NNLO_Ztautau_CFilterBVeto.", "Sherpa_NNPDF30NNLO_Ztautau_CVetoBVeto."
     ]],
    "QCD": [[], ["Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ."]],
    "SinglePhoton":
    [[], ["Sherpa_CT10_SinglePhoton_BFilter.", "Sherpa_CT10_SinglePhoton_CFilterBVeto.", "Sherpa_CT10_SinglePhoton_CVetoBVeto."]],
    "PhotonV": [[],
                [
                    "Sherpa_CT10_eegamma.", "Sherpa_CT10_enugamma.", "Sherpa_CT10_mumugamma.", "Sherpa_CT10_munugamma.",
                    "Sherpa_CT10_nunugamma.", "Sherpa_CT10_taunugamma.", "Sherpa_CT10_tautaugamma."
                ]],
    "Wjets_Sherpa221": [[],
                        [
                            "Sherpa_221_NNPDF30NNLO_Wenu_BFilter",
                            "Sherpa_221_NNPDF30NNLO_Wenu_CFilterBVeto",
                            "Sherpa_221_NNPDF30NNLO_Wenu_CVetoBVeto",
                            "Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV",
                            "Sherpa_221_NNPDF30NNLO_Wmunu_BFilter",
                            "Sherpa_221_NNPDF30NNLO_Wmunu_BFilter",
                            "Sherpa_221_NNPDF30NNLO_Wmunu_CFilterBVeto",
                            "Sherpa_221_NNPDF30NNLO_Wmunu_CVetoBVeto",
                            "Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV",
                            "Sherpa_221_NNPDF30NNLO_Wtaunu_BFilter",
                            "Sherpa_221_NNPDF30NNLO_Wtaunu_CFilterBVeto",
                            "Sherpa_221_NNPDF30NNLO_Wtaunu_CFilterBVeto",
                            "Sherpa_221_NNPDF30NNLO_Wtaunu_CVetoBVeto",
                            "Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV",
                        ]],
    "Zjets_Sherpa221": [[],
                        [
                            "Sherpa_221_NNPDF30NNLO_Zee_CFilterBVeto",
                            "Sherpa_221_NNPDF30NNLO_Zee_CVetoBVeto",
                            "Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV",
                            "Sherpa_221_NNPDF30NNLO_Zmumu_CFilterBVeto",
                            "Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV",
                            "Sherpa_221_NNPDF30NNLO_Znunu_BFilter",
                            "Sherpa_221_NNPDF30NNLO_Znunu_CFilterBVeto",
                            "Sherpa_221_NNPDF30NNLO_Znunu_CVetoBVeto",
                            "Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV",
                            "Sherpa_221_NNPDF30NNLO_Ztautau_BFilter",
                            "Sherpa_221_NNPDF30NNLO_Ztautau_CFilterBVeto",
                            "Sherpa_221_NNPDF30NNLO_Ztautau_CVetoBVeto",
                            "Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV",
                            "Sherpa_221_NNPDF30NNLO_Zee_BFilter",
                            "Sherpa_221_NNPDF30NNLO_Zmumu_BFilter",
                            "Sherpa_221_NNPDF30NNLO_Zmumu_CVetoBVeto",
                        ]],
}

# sample tags to analyze
rtags = ['r7772_r7676', 'r7725_r7676', 'a821_r7676', 'a818_r7676', 'a838_r7676']

# assign the PRW files to the sample tags
prwconfs = {
    'r7725_r7676': ['dev/SUSYTools/merged_prw_mc15c_latest.root', 'dev/PileupReweighting/mc15c_v2_defaults.NotRecommended.prw.root'],
    'r7772_r7676': ['dev/SUSYTools/merged_prw_mc15c_latest.root', 'dev/PileupReweighting/mc15c_v2_defaults.NotRecommended.prw.root'],
    'a821_r7676': ['dev/SUSYTools/merged_prw_mc15c_latest.root', 'dev/PileupReweighting/mc15c_v2_defaults.NotRecommended.prw.root'],
}

pwconfs_data = [
    "XAMPPbase/GRL/data15_13TeV.periodAllYear_DetStatus-v79-repro20-02_DQDefects-00-02-02_PHYS_StandardGRL_All_Good_25ns.root",
    "XAMPPbase/GRL/data16_13TeV.periodAllYear_DetStatus-v88-pro20-21_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.root",
]


def GetFiles(files, directory):
    Files = []
    for f in files:
        if os.path.isdir(directory + f):
            for myfile in GetFiles(os.listdir(directory + f), directory + f):
                Files.append(myfile)
        else:
            Files.append(directory + '/' + f)
    return Files


# in case a pattern in the DS name contains specific information (e.g. particle masses), find it and add it to the PhysicsShortName
patternsForMasses = [
    "TT_directTTMadGraph_", "ISR_TT_directTT_", "TT_onestepBBMadGraph_", "TT_mixedBTMGPy8EG_", "TT_bWNMGPy8EG_", "GG_directGtc5MadGraph_",
    "GG_directGtc5_", "DM_TTscalar_", "DM_TTpseudo_", "DM_BBscalar_", "DM_BBpseudo_"
]


def WriteToFile(fout, Sample, smplist, rtag, adding=False, doPRW=True):
    if not adding: fout.write("SampleName %s\n" % Sample)
    # if 'DM' in Sample: fout.write("DMXSecUpdater 2\n")
    if doPRW and not adding and rtag in prwconfs.iterkeys():
        for pwconf_data in pwconfs_data:
            fout.write("PRWlumi %s\n" % pwconf_data)
        for pwconf in prwconfs[rtag]:
            fout.write("PRWconfig %s\n" % pwconf)
    for cand in sorted(smplist):
        fout.write("Input %s\n" % (cand))


if __name__ == '__main__':

    import argparse
    parser = argparse.ArgumentParser(description="Script for creating MC input configs",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-i', '--input', dest='indir', help='Folder with files', required=True)
    parser.add_argument('-o', '--output', dest='outdir', help='Output directory to put file list(s) into', required=True)
    parser.add_argument('-f',
                        '--readROOTFiles',
                        help='Directly read from folder containing merged root files',
                        action='store_true',
                        default=False)
    parser.add_argument('--noPRWFiles', help='Do not add PRW files to input configs', action='store_true', default=False)
    options = parser.parse_args()

    os.system("mkdir -p %s" % options.outdir)

    files = os.listdir(options.indir)

    if len(files) == 0:
        print 'No files found, exiting...'
        sys.exit(1)
    if mimetypes.guess_type(files[0])[0] != None:
        print 'Having found a text file, assuming to create input configs from file lists...'
        if options.readROOTFiles:
            print 'Cannot use the --readROOTFiles option when reading from text files, exiting...'
            sys.exit(1)
    else:
        print 'Having found a root file, assuming to create input configs from root files...'
        files = GetFiles(files, options.indir)
        if not options.readROOTFiles:
            print 'Use the --readROOTFiles option when reading from root files, exiting...'
            sys.exit(1)

    FoundDatasets = {}
    for cand in files:
        DSname = ""
        for pattern in patternsForMasses:
            if pattern in cand:
                DSname += pattern.replace("ISR_TT_directTT_", "TT_directTT_").replace("MadGraph", "")
                if 'DM_TTscalar_p' in pattern or 'DM_TTpseudo_' in pattern or 'DM_TTscalar_' in pattern:
                    DSname += str(re.findall("%s(\d+(_c\d+)*)" % pattern, cand)[0][0])
                else:
                    DSname += str(re.findall("%s(\d+(_\d+)*)" % pattern, cand)[0][0])
                    if DSname == "TT_directTT_400_100_1": DSname = "TT_directTT_400_100"
                    if DSname == "TT_directTT_300_100_1": DSname = "TT_directTT_300_100"
        if DSname == "":
            for Sample, Info in DSIDs.iteritems():
                if len(Info[0]) > 0:
                    for DSID in Info[0]:
                        if (DSID != 0 and str(DSID) in cand):
                            if len(Info[1]) > 0:
                                for pattern in Info[1]:
                                    if pattern in cand:
                                        DSname = Sample
                            else:
                                DSname = Sample
                for pattern in Info[1]:
                    if pattern in cand:
                        DSname = Sample
        if DSname == "": continue
        for rtag in rtags:
            if rtag in cand:
                if not rtag in FoundDatasets.iterkeys():
                    FoundDatasets[rtag] = {}
                if DSname in FoundDatasets[rtag].iterkeys():
                    print 'WARNING: %s already exists in dictionnary, adding it!' % (DSname)
                else:
                    FoundDatasets[rtag][DSname] = []
                if options.readROOTFiles:
                    FoundDatasets[rtag][DSname] += [cand]
                else:
                    with open(options.indir + "/" + cand, "r") as fin:
                        FoundDatasets[rtag][DSname] += [line.replace("\n", "") for line in fin.readlines()]

    for rtag in FoundDatasets.iterkeys():
        for Sample, smplist in FoundDatasets[rtag].iteritems():
            fname = "%s/%s_Input.conf" % (options.outdir, Sample)
            if 'a' in rtag:
                fname = "%s/%s_%s_Input.conf" % (options.outdir, Sample, rtag)
            if os.path.exists(fname):
                print 'WARNING: File %s already exists, adding input files!' % fname
                with open(fname, "a") as fout:
                    WriteToFile(fout, Sample, smplist, rtag, adding=True, doPRW=not options.noPRWFiles)
            else:
                with open(fname, "w") as fout:
                    WriteToFile(fout, Sample, smplist, rtag, doPRW=not options.noPRWFiles)
