#!/usr/bin/env python

import os
import sys
import commands
import mimetypes

if __name__ == '__main__':

    import argparse
    parser = argparse.ArgumentParser(description="Script for creating MC input configs",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-i', '--input', dest='indir', help='Folder with files', required=True, nargs='+')
    parser.add_argument('-o',
                        '--output',
                        dest='outdir',
                        help='Output directory to put file list(s) into',
                        default='/eos/atlas/user/n/nkoehler/Stop0L/HistFitter/170221_Base2_4_28/')
    parser.add_argument('-l', '--list', help='list to check for DS names', default='../stop0L_dxaod/lists/Stop0L_DxList_Sig_nom_all.txt')
    options = parser.parse_args()

    # for permissions go to lxplus and do: eos attr -r set user.acl='egroup:atlas-phys-susy-directstop-zeroLep:rx,g:zp:!d' eos/atlas/user/n/nkoehler/Stop0L/xyz
    # https://twiki.cern.ch/twiki/bin/view/AtlasComputing/ATLASStorageAtCERN#Users_area_on_EOS
    # full atlas read permission: eos chmod -r 750 /eos/atlas/user/n/nkoehler/DMHF/HistFitter/170303_Base_2_4_28
    WaltersPath = '/eos/atlas/user/w/whopkins/samples/Stop0LRun2-00-01-28/'

    destination = 'root://eosatlas.cern.ch//' + options.outdir + '/'
    print 'EOS destination to which files are copied: %s' % destination

    referenceList = []
    if options.list != '' and os.path.exists(options.list):
        for line in open(options.list, "r"):
            line = line.strip()
            if line.startswith("#") or len(line) < 2: continue
            referenceList.append(line.replace('mc15_13TeV:mc15_13TeV', 'mc15_13TeV'))

    Found = commands.getoutput('xrd eosatlas.cern.ch dirlist ' + WaltersPath)
    WaltersFiles = []
    for line in Found.split('\n'):
        for item in line.split(' '):
            if not '.root' in item: continue
            WaltersFiles.append(item.replace(WaltersPath, ''))

    files = os.listdir(options.indir[0])

    if len(files) == 0:
        print 'No files found, exiting...'
        sys.exit(1)

    FileNameTranslation = {}

    print WaltersFiles

    SamplesNotFoundinWalters = 0

    for f in files:
        smpname = f.replace('_Input.root', '')
        if 'ttGamma_a821_r7676_Input.root' in f:
            FileNameTranslation[f] = 'ttGammaExt.root'
        if 'ttV_Input.root' in f:
            FileNameTranslation[f] = 'ttVExt.root'

        if '_a821_r7676' in smpname and smpname.split('_')[1] != '_a821_r7676':
            signame = smpname.split('_a821_r7676')[0].replace('TT_bWNMGPy8EG', 'TT_bWN').replace('TT_mixedBTMGPy8EG', 'TT_mixedBT')
            for fw in WaltersFiles:
                if '_'.join(signame.split('_')[:2]) in fw and ('_' + ('_'.join(signame.split('_')[2:]) + '.') in fw or 'p' +
                                                               ('_'.join(signame.split('_')[2:]) + '.') in fw or
                                                               ('_' + '_'.join(signame.split('_')[2:]) + '_MET100') in fw or
                                                               ('_' + '_'.join(signame.split('_')[2:]) + 'L20orMET') in fw):
                    if f in FileNameTranslation.iterkeys():
                        print 'File %s occurs twice!' % f
                    FileNameTranslation[f] = fw
            if not f in FileNameTranslation.iterkeys():
                for fref in referenceList:
                    if '_'.join(signame.split('_')[:2]) in fref and ('_' + ('_'.join(signame.split('_')[2:]) + '.') in fref or 'p' +
                                                                     ('_'.join(signame.split('_')[2:]) + '.') in fref or
                                                                     ('_' + '_'.join(signame.split('_')[2:]) + '_MET100') in fref or
                                                                     ('_' + '_'.join(signame.split('_')[2:]) + 'L20orMET') in fref):
                        if f in FileNameTranslation.iterkeys():
                            print 'File %s occurs twice!' % f
                        FileNameTranslation[f] = fref + 'Ext.root'

        elif not f in FileNameTranslation.iterkeys():
            for fw in WaltersFiles:
                if smpname in fw:
                    if f in FileNameTranslation.iterkeys():
                        print 'File %s occurs twice!' % f
                    FileNameTranslation[f] = fw
                elif 'Sherpa221' in fw and 'Sherpa221' in f:
                    if "Wjets_Sherpa221" in f and "WSherpa221" in fw or "Zjets_Sherpa221" in f and "ZSherpa221" in fw:
                        FileNameTranslation[f] = fw
                elif 'dibosons' in fw and 'Diboson' in f:
                    FileNameTranslation[f] = fw
                elif 'ttGamma' in fw and 'ttGamma' in f:
                    FileNameTranslation[f] = fw
                elif 'gammaExt.root' in fw and 'SinglePhoton' in f:
                    FileNameTranslation[f] = fw

        if not f in FileNameTranslation.iterkeys():
            print 'File %s was not translated yet, will use original name for copying...' % f
            SamplesNotFoundinWalters += 1
            FileNameTranslation[f] = f

    print '#samples Walter = %s, #samples MPP = %s, %s samples not found in Walters' % (len(WaltersFiles), len(files),
                                                                                        SamplesNotFoundinWalters)
    from pprint import pprint
    pprint(FileNameTranslation)

    TMP_PATH = '/ptmp/mpp/niko/Cluster/Batch_tmp/2017-02-06/test/'
    os.system("mkdir -p %s" % TMP_PATH)

    for orig, new in FileNameTranslation.iteritems():
        if os.path.exists("%s/%s" % (TMP_PATH, new)): os.system("rm %s/%s" % (TMP_PATH, new))
        haddStr = "hadd %s/%s" % (TMP_PATH, new)
        for inputdir in options.indir:
            if os.path.exists("%s/%s" % (inputdir, orig)):
                haddStr += " %s/%s" % (inputdir, orig)
        if haddStr == "hadd %s/%s" % (TMP_PATH, new):
            print 'no files to hadd, exiting...'
            sys.exit(1)
        print haddStr

        os.system(haddStr)

        os.system("xrdcp -f %s/%s %s/%s " % (TMP_PATH, new, destination, new))

        os.system("rm %s/%s" % (TMP_PATH, new))
