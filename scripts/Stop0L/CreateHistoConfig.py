#! /usr/bin/env python
from XAMPPplotting.PlotUtils import *
from ROOT import TCanvas, TGraph


def WriteTemplate(hfile):
    hfile.write('1DHisto  BDT 201 -1 1 \n')


if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog='GetRocAndOvertrain', description='This script makes multiple Run and Histo Configs')
    parser.add_argument('-i', '--inputPath', help='specify path of Training root files to compare', default='', required=True)
    parser.add_argument('-s', '--split', help='specify number of histos per histo config', default=200, type=int)

    Options = parser.parse_args()

    if not os.path.isdir(Options.inputPath):
        print "ERROR: Specified path %s does not exist, exiting..." % Options.inputPath
        sys.exit(1)

    histoconfigs = []

    nfiles = 0
    for myfile in os.listdir(Options.inputPath):
        if ".root" in myfile and not 'weights_foams' in myfile:
            if nfiles % Options.split == 0:
                nfiles = 0
            if nfiles == 0:
                HistoFile = open('HistoJonasBDT2D%i.conf' % len(histoconfigs), "a+")
                histoconfigs.append(HistoFile)
                WriteTemplate(HistoFile)

            #ROOTFiles[myfile.replace('.root','').replace('TMVAClassification_','')] = Options.inputPath+'/'+myfile
            nfiles = nfiles + 1
            ###ACHTUNG!!!!!!!! Den Namen der Methode  (2. Argument bei MVAEventReader kuenstlich veraendern
            HistoFile.write(
                'NewVar\n   Name %s\n   MVAEventReader %s SehrSehrVieleBDT\n   Template BDT\n   Type 1D\n   xLabel %s\nEndVar\n' %
                (myfile.replace('.root', '').replace('TMVAClassification_', ''), myfile.replace('.root', '').replace(
                    'TMVAClassification_', ''), myfile.replace('.root', '').replace('TMVAClassification_', '')))
            #RunConf = open('RunConfigsPlotting/RunConf_Plotting_%s.conf'%myfile.replace('.root','').replace('TMVAClassification_',''))
            #RunConf.write('# Analysis name in input tree\nTree StopHistFitter\n#Tree Stop0L\nWeights GenWeight JetWeightBTag JetWeightJVT NormWeight\n# Analysis name in output histogram file\nAnalysis Stop0L\n\nnoPRW\n# Systematics which you want to run over. If you do not specify it XAMPPplotting runs over all systematics stored\n\n# XAMPPplotting just runs over the nominal tree\nnoSyst\n\n# specify region\nRegion %s\n\nXMLPath %s'%myfile.replace('.root','').replace('TMVAClassification_','')%Options.inputPath)
            #RunConf.close()

    for h in histoconfigs:
        h.close()
