#!/usr/bin/env python

#######################################################

# File containing trigger and function definifitions used in
# TriggerEfficiencies.py, CalculateTriggerSFs.py and ValidateTriggerSFs.py

#######################################################

from ROOT import *
gROOT.LoadMacro("~/RootUtils/AtlasStyle.C")
gROOT.ProcessLine("SetAtlasStyle()")

trigger = [
    "IsTrigHLT_xe70_mht_and_CorrectRun",
    "IsTrigHLT_xe90_mht_L1XE50_and_CorrectRun",
    #           "IsTrigHLT_xe100_mht_L1XE50_and_CorrectRun",
    "IsTrigHLT_xe110_mht_L1XE50_and_CorrectRun",
    "IsTrigHLT_xe110_pufit_L1XE55_and_CorrectRun"
]

triggerLumis = {}
triggerLumis["IsTrigHLT_xe70_mht_and_CorrectRun"] = "3.2 fb^{-1}"
triggerLumis["IsTrigHLT_xe90_mht_L1XE50_and_CorrectRun"] = "6.1 fb^{-1}"
#triggerLumis["IsTrigHLT_xe100_mht_L1XE50_and_CorrectRun"] = "3.9 fb^{-1}"
triggerLumis["IsTrigHLT_xe110_mht_L1XE50_and_CorrectRun"] = "27.1 fb^{-1}"
triggerLumis["IsTrigHLT_xe110_pufit_L1XE55_and_CorrectRun"] = "43.8 fb^{-1}"

triggerPeriods = {}
triggerPeriods["IsTrigHLT_xe70_mht_and_CorrectRun"] = "2015"
triggerPeriods["IsTrigHLT_xe90_mht_L1XE50_and_CorrectRun"] = "2016, Period A-D3"
#triggerPeriods["IsTrigHLT_xe100_mht_L1XE50_and_CorrectRun"] = "2016, Period D3-E4"
triggerPeriods["IsTrigHLT_xe110_mht_L1XE50_and_CorrectRun"] = "2016, Period D3-"
triggerPeriods["IsTrigHLT_xe110_pufit_L1XE55_and_CorrectRun"] = "2017"

nBins = {}
nBins["IsTrigHLT_xe70_mht_and_CorrectRun"] = 25
nBins["IsTrigHLT_xe90_mht_L1XE50_and_CorrectRun"] = 25
#nBins["IsTrigHLT_xe100_mht_L1XE50_and_CorrectRun"] = 250
nBins["IsTrigHLT_xe110_mht_L1XE50_and_CorrectRun"] = 25
nBins["IsTrigHLT_xe110_pufit_L1XE55_and_CorrectRun"] = 25

triggerNames = {}
triggerRuns = {}
triggerLumiLabels = {}
for trig in trigger:
    noIsTrig = trig.replace("IsTrig", "")
    triggerNames[trig] = noIsTrig.replace("_and_CorrectRun", "")
    triggerRuns[trig] = trig.replace("and_Correct", "")
    triggerLumiLabels[trig] = "#sqrt{s} = 13 TeV, " + triggerLumis[trig]


def SetAtlasLabel():
    ATLAS_label = TLatex()
    ATLAS_label.SetNDC()
    ATLAS_label.SetTextFont(43)
    ATLAS_label.SetTextSize(24)
    ATLAS_label.SetTextColor(kBlack)
    ATLAS_label.SetTextAlign(11)
    ATLAS_label.DrawLatex(0.55, 0.66, "#font[72]{ATLAS} Internal")


def SetLumiLabel(trigger):
    lumi_label = TLatex()
    lumi_label.SetNDC()
    lumi_label.SetTextFont(43)
    lumi_label.SetTextSize(18)
    lumi_label.DrawLatex(0.55, 0.58, triggerLumiLabels[trigger])


def SetRegionLabel(trigger):
    region_label = TLatex()
    region_label.SetNDC()
    region_label.SetTextFont(43)
    region_label.SetTextSize(18)
    region_label.SetTextColor(kBlack)
    triggerName0 = trigger.replace("IsTrig", "")
    triggerName = triggerName0.replace("_and_CorrectRun", "")
    region_label.DrawLatex(0.55, 0.52, triggerName)


def SetPeriodLabel(trigger):
    region_label = TLatex()
    region_label.SetNDC()
    region_label.SetTextFont(43)
    region_label.SetTextSize(18)
    region_label.SetTextColor(kBlack)
    region_label.DrawLatex(0.55, 0.46, triggerPeriods[trigger])


def SetLegend(histo_MC, histo_data):
    legend = TLegend(0.55, 0.34, 0.85, 0.44)
    legend.SetBorderSize(0)
    legend.SetTextFont(42)
    legend.SetTextSize(0.035)
    legend.SetFillColor(0)
    legend.SetLineColor(0)
    legend.AddEntry(histo_MC, "MC", "l")
    legend.AddEntry(histo_data, "data", "l")
    return legend
