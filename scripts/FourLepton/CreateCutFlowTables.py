#!/usr/bin/env python
import os, sys, commands, time, argparse, logging
from pprint import pprint
from XAMPPplotting.CheckMetaData import prettyPrint, max_width, writeTableHeader, formatItem, GetNormalizationDB
from XAMPPplotting.PlotLabels import GetLabelKey, MassDic, ProcessDic
from XAMPPplotting.PrintYields import CutFlowYield
from XAMPPplotting.Utils import ExtractMasses
from XAMPPplotting.FileUtils import ReadInputConfig
from XAMPPplotting.FileStructureHandler import GetFileHandler, ClearServices
from ClusterSubmission.Utils import ResolvePath, CreateDirectory, RecursiveLS, WriteList


#####################################################################################################
##      Script to create CutFlow tables for the considered signal samples inside your analysis.     #
##      In order to properly create a cutflow make sure to run XAMPP over unskimmed DAODs.          #
##      Depending on what you consider as a GoodEvent you might optionally switch off the           #
##      trigger skimming and any dumping cut in your AnalysisConfig                                 #
#####                                                                                           #####
##      You'll need to pipe the directory containing all of the signal input configs                #
##      and the one where all outputs of WriteDefaultHistos are located                             #
##      Make sure that your signal satisfy the naming pattern                                       #
##          <model_name>_<first_param>_<second_param>_<extra information>                           #
##      Optionally you need to add your model to the GetLabelKey and MassDic functions              #
##      of the PlotLabels module.                                                                   #
#####################################################################################################
##### Object class containing all cutflows for a given signal sample
class SignalSample(object):
    def __init__(self, smp_name="", analysis="", regions=[], daod_files=[], histo_files=[]):
        self.__smp_name = smp_name
        self.__nlsp_mass, self.__lsp_mass = ExtractMasses(smp_name)
        self.__model_name = "_".join([t for t in smp_name.split("_") if not t.isdigit()])
        self.__analysis = analysis
        self.__regions = regions
        self.__dsids = GetChannelNumbers(paths=daod_files)
        ### Total number of events
        self.__tot_events = sum([GetNormalizationDB().GetTotalEvents(dsid, 0) for dsid in self.__dsids])
        ### Total sum of weights
        self.__sumW = sum([GetNormalizationDB().GetSumW(dsid, 0) for dsid in self.__dsids])
        #### How many events made it through the DAOD skimming --> ideally all
        self.__proc_events = sum([GetNormalizationDB().GetProcessedEvents(dsid, 0) for dsid in self.__dsids])
        #### What are the DAOD names of the sample
        self.__ds_names = [GetNormalizationDB().GetSampleName(dsid) for dsid in self.__dsids]
        #### Total cross-section
        self.__tot_xsec = sum([
            GetNormalizationDB().GetxSectTimes(dsid, proc) for proc in GetNormalizationDB().GetListOfProcesses(dsid)
            for dsid in self.__dsids if (proc > 0 or GetNormalizationDB().GetNumberOfProcesses(dsid) <= 1) and proc < 1000
        ]) * 1.e3
        ### Use the 0-th state
        if self.model_name().find("GGM") != -1:
            self.__tot_xsec = sum([GetNormalizationDB().GetxSectTimes(dsid, 0) for dsid in self.__dsids])

        ### Cutflows foreach region
        self.__cfs = [SignalCutFlow(signal_sample=self, region=r, histo_files=histo_files, use_weighted=False) for r in regions]
        #self.__wcfs = [SignalCutFlow(signal_sample=self, region=r, histo_files=histo_files, use_weighted=True) for r in regions]
        ### The File handler keeps all root files open forever.
        ### But they're not needed anymore. So close them
        GetFileHandler().closeAll()

    def get_regions(self):
        return self.__regions

    def get_region_cutflow(self, r):
        for cf in self.__cfs:
            if cf.region() == r: return cf
        logging.warning("Region %s is unknown to sample %s" % (r, self.sample_name()))
        return None

    def get_all_events(self):
        return self.__tot_events

    def get_processed_events(self):
        return self.__proc_events

    def get_sumW(self):
        return self.__sumW

    def get_xsection(self):
        return self.__tot_xsec

    def sample_name(self):
        return self.__smp_name

    def analysis(self):
        return self.__analysis

    def model_name(self):
        return self.__model_name

    def nlsp_mass(self):
        return self.__nlsp_mass

    def lsp_mass(self):
        return self.__lsp_mass

    def nlsp_name(self):
        ### Why Mr Graw?
        return formatItem(MassDic[GetLabelKey(self.model_name())][1].replace("[GeV]", "\\text{[GeV]}"), doLaTeX=False)

    def lsp_name(self):
        ### Why Mr Graw?
        return formatItem(MassDic[GetLabelKey(self.model_name())][2].replace("[GeV]", "\\text{[GeV]}"), doLaTeX=False)


class SignalCutFlow(CutFlowYield):
    def __init__(self, signal_sample=None, region="", histo_files=[], use_weighted=True):

        self.__smp_object = signal_sample
        self.__use_weighted = use_weighted
        CutFlowYield.__init__(self,
                              smp_name=signal_sample.sample_name(),
                              analysis=signal_sample.analysis(),
                              region=region,
                              root_files=histo_files,
                              use_weighted=use_weighted)

    def is_weighted_cf(self):
        return self.__use_weighted

    def get_sample_obj(self):
        return self.__smp_object

    def get_cut_efficiency(self, n=1):
        if n == 0: return 1.
        return self.get_yield(n) / self.get_all()

    def get_all(self):
        return self.__smp_object.get_xsection() if self.is_weighted_cf() else self.__smp_object.get_all_events()

    def get_cut_name(self, n=0):
        if n == 0: return "All"
        elif n == 1: return "Good event"
        elif n > self.get_n_bins():
            raise NameError("Out of range %n" % (n))
        bin_label = self.get_histo().GetXaxis().GetBinLabel(n)
        if skimmed_cut(bin_label): return ""
        return AdjustBinLabel(bin_label)

    def get_cut_names(self):
        return [formatItem(self.get_cut_name(n), doLaTeX=True) for n in range(self.get_n_bins()) if len(self.get_cut_name(n)) > 0]

    def get_efficiencies(self):
        return [self.get_cut_efficiency(n) for n in range(self.get_n_bins()) if len(self.get_cut_name(n)) > 0]


def skimmed_cut(cut):
    if "mcChannelNumber" in cut: return True
    return cut in [
        "Remove duplicated events",
    ]


def AdjustBinLabel(Label):
    if "ZZFinder" in Label: return "ZZ selection"
    if "appliedMuTriggerSF" in Label: return "Trigger matching"
    if "N_CollimatedLep == 0" in Label: return "$N_{\\ell}^{\\text{collimated}}=0$"
    if "Signal_Mll_Z1 >= 81.20 && Signal_Mll_Z1 <= 101.20" in Label: return "$1^{\\text{st}}$ Z"
    if "Signal_Mll_Z2 >= 61.20 && Signal_Mll_Z2 <= 101.20" in Label: return "$2^{\\text{nd}}$ Z"
    Label = Label.replace("N_SignalLightLep", "$N_{\\ell}$")
    Label = Label.replace("N_LooseLightLep", "$N_{\\ell}^{badly-measured}$")
    Label = Label.replace("N_SignalTaus", "$N_{\\tau}$")
    Label = Label.replace("N_SignalInclTaus", "$N_{\\tau}$")
    Label = Label.replace("Trigger == 1.00", "Trigger")
    Label = Label.replace("ZVeto == 1", "Z veto")
    Label = Label.replace("Signal_Meff", "\\meff")
    Label = Label.replace("N_BJets == 0", "b veto")
    Label = Label.replace("N_BJets > 0", "b selection")

    Label = Label.replace("ZVeto LightLep == 1.00", "Z veto")
    if "Meff" in Label or "MetTST_met" in Label or "\\meff" in Label: Label += " [GeV]"
    Label = Label.replace("Meff", "\meff")
    Label = Label.replace("MetTST_met", "\met")
    Label = Label.replace(".00", "")

    return Label


def GetChannelNumbers(paths=[]):
    mcChannels = []
    for F in paths:
        MetaData = GetFileHandler().LoadFile(F).Get("MetaDataTree")
        for i in range(MetaData.GetEntries()):
            MetaData.GetEntry(i)
            if MetaData.mcChannelNumber not in mcChannels: mcChannels += [MetaData.mcChannelNumber]
    return mcChannels


def setupCutFlowParser():
    parser = argparse.ArgumentParser(description="Script to write ")
    parser.add_argument('--inConfigDir',
                        help='Directory containing the input configs parsed through XAMPPplotting to write the cutflow histograms',
                        default="XAMPPmultilep/InputConf/MPI/v028/Signal/")
    parser.add_argument('--processedFiles', help='Location where the processed signals are stored.', required=True)
    parser.add_argument('--outputDir', help='Location where all of the cutflows are stored to', default="Table_CutFlows/")
    parser.add_argument("--lumi", help="To which luminosity is the expected yield scaled to", default=140., type=float)
    parser.add_argument("--points_per_table", help="How many points to show per table", default=25, type=int)
    parser.add_argument("--regions",
                        help="Only make the cutflow for the follwoing regions",
                        default=["SR0B", "SR0F", "SR1B", "SR1C", "SR2B", "SR2C"],
                        nargs="+")
    return parser


def setupCutFlowDictionary(options):
    in_cfg_dir = ResolvePath(options.inConfigDir)
    SignalSamples = [
        s[s.rfind("/") + 1:s.rfind(".")] for s in RecursiveLS(Dir=options.processedFiles, data_types=["root"])
        if os.path.isfile("%s/%s.conf" % (in_cfg_dir, s[s.rfind("/") + 1:s.rfind(".")]))
    ]
    SignalSamples.sort()
    DAOD_Paths = []
    for Sample in SignalSamples:
        DAOD_Paths += ReadInputConfig("%s/%s.conf" % (in_cfg_dir, Sample))
    ### Load the sample into the cache
    GetNormalizationDB(DAOD_Paths)
    GetNormalizationDB().LoadCrossSections("dev/PMGTools/PMGxsecDB_mc16.txt", "XAMPPplotting/xSecFiles/")
    GetNormalizationDB().PromptMetaDataTree()
    #### Now everything is loaded we can start to assemble the cutflow dictionary
    cf_samples = []
    analysis = GetFileHandler().GetFileStructure(options.processedFiles + SignalSamples[0] + ".root").get_analyses_names()[0]
    regions = [
        r for r in GetFileHandler().GetFileStructure(options.processedFiles + SignalSamples[0] + ".root").get_regions(analysis)
        if len(options.regions) == 0 or r in options.regions
    ]

    for i, Sample in enumerate(SignalSamples, 1):
        if "BiLepton" in Sample: continue
        cf_samples += [
            SignalSample(smp_name=Sample,
                         analysis=analysis,
                         regions=regions,
                         daod_files=ReadInputConfig("%s/%s.conf" % (in_cfg_dir, Sample)),
                         histo_files=[options.processedFiles + Sample + ".root"])
        ]
        if i % 10 == 0:
            logging.info("%d/%d samples have been loaded" % (i, len(SignalSamples)))
    cf_dict = {}
    while len(cf_samples) > 0:
        to_sort = cf_samples.pop()
        try:
            cf_dict[to_sort.model_name()] += [to_sort]
        except:
            cf_dict[to_sort.model_name()] = [to_sort]
    return cf_dict


def point_comp(a, b):
    if a.nlsp_mass() == b.nlsp_mass():
        return a.lsp_mass() - b.lsp_mass()
    return a.nlsp_mass() - b.nlsp_mass()


def write_footer(caption="", vertical=False):
    return "\n".join([
        "\\bottomrule",
        "\\end{tabular}",
        "\\end{adjustbox}",
        "\\caption{%s}" % (caption),
        "\\end{%s}\n" % ("table" if not vertical else "sidewaystable"),
        "\\clearpage\n\n\n",
    ])


def make_cf_table(grid_points, lumi=140., max_entries_per_table=25):
    ### Sort by lsp_mass and NLSP mass
    grid_points.sort(cmp=point_comp)
    table = []

    ref_point = grid_points[0]
    for region in ref_point.get_regions():
        header = [ref_point.nlsp_name()]
        if len(ref_point.lsp_name()) > 0: header += [ref_point.lsp_name()]
        header += ["\\multicolumn{2}{c}{%s}" % (c_name) for c_name in ref_point.get_region_cutflow(region).get_cut_names()]
        table += [writeTableHeader(header=header, id_cols=0, adjust='c', vertical=True, doLaTeX=True)]
        caption = "Cutflow of the generated signal points for the $%s$ model in region %s. Foreach cut the expectation is normalized to 140~\ifb of data. The numbers in brakets state the raw Monte-Carlo events passing the criterion." % (
            ref_point.model_name().replace("_", "\\_"), region)
        table_content = []
        for n, sig_point in enumerate(grid_points, 1):

            row = ["%d" % (sig_point.nlsp_mass()), "%d" % (sig_point.lsp_mass())]
            if ref_point.lsp_mass() < 0: row.pop()
            sig_cf = sig_point.get_region_cutflow(region)
            for effi in sig_cf.get_efficiencies():
                row += ["%.2f" % (lumi * sig_point.get_xsection() * effi), "(%.0f)" % (sig_cf.get_all() * effi)]
            table_content += [prettyPrint(itemsToPrint=row, width=10, doLaTeX=True)]
            ### The table is full make a new one
            if n % max_entries_per_table == 0:
                table += table_content
                table_content = []
                table += [write_footer(caption=caption, vertical=True)]
                table += [writeTableHeader(header=header, id_cols=0, adjust='c', vertical=True)]
        table += table_content
        table += [write_footer(caption=caption, vertical=True)]
    return table


if __name__ == '__main__':
    options = setupCutFlowParser().parse_args()
    GetFileHandler().switchOffMsg()
    CutFlows_withDSID = setupCutFlowDictionary(options)
    ClearServices()

    for model, grid_points in CutFlows_withDSID.iteritems():
        logging.info("Written %s" % (WriteList(
            make_cf_table(grid_points=grid_points, lumi=options.lumi, max_entries_per_table=options.points_per_table), "%s/CutFlow_%s.tex" %
            (options.outputDir, model))))
