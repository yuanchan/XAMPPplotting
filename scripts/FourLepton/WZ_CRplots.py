#! /usr/bin/env python
from XAMPPplotting.Defs import *
from XAMPPplotting.PlotUtils import *
import XAMPPplotting.Utils
import ClusterSubmission.Utils
import XAMPPplotting.PlottingHistos
import XAMPPplotting.FileStructureHandler
from XAMPPplotting.YieldsHandler import *


def drawComparison(Options, analysis, wz_region, wz_var, zz_region, zz_var, bonus_str=""):
    HistList = []
    HistToDraw = []
    ratios = []

    FullHistoSet_WZ = XAMPPplotting.PlottingHistos.CreateHistoSets(Options, analysis, wz_region, wz_var, UseData=False)
    FullHistoSet_ZZ = XAMPPplotting.PlottingHistos.CreateHistoSets(Options, analysis, zz_region, zz_var, UseData=False)

    if not FullHistoSet_WZ or not FullHistoSet_WZ[0].CheckHistoSet() or not FullHistoSet_ZZ or not FullHistoSet_ZZ[0].CheckHistoSet():
        print "WARNING: Skip variables"
        return False

    HistoSet_WZ = FullHistoSet_WZ[0]
    HistoSet_ZZ = FullHistoSet_ZZ[0]

    PosY2 = 0.9
    PosY1 = 0.6
    if Options.doLogY: bonus_str += "_LogY"
    canvasName = "WZComparison_%s_%s_%s_%s%s" % (wz_region, wz_var, zz_region, zz_var, bonus_str)

    pu = PlotUtils(status=Options.label, size=24, lumi=Options.lumi)

    pu.Prepare2PadCanvas(canvasName, 800, 600, Options.quadCanvas)
    pu.GetTopPad().cd()
    if Options.doLogY: pu.GetTopPad().SetLogy()

    WZSmp = HistoSet_WZ.GetSample("WZ")
    ZZSmp = HistoSet_ZZ.GetSample("ZZ")

    pu.CreateLegend(Options.LegendXCoords[0], Options.LegendYCoords[0], Options.LegendXCoords[1], Options.LegendYCoords[1])

    ### Draw the histograms
    ymin, ymax = pu.GetFancyAxisRanges([WZSmp, ZZSmp], Options)
    pu.drawStyling(ZZSmp, ymin, ymax, RemoveLabel=not Options.noRatio, TopPad=not Options.noRatio)

    ZZSmp.GetHistogram().Draw("sameHIST")
    WZSmp.GetHistogram().Draw("sameHIST")

    if zz_var != wz_var:
        ZZSmp.SetTitle("ZZ " + ZZSmp.GetAxis(0).GetTitle())
        WZSmp.SetTitle("WZ " + WZSmp.GetAxis(0).GetTitle())
    pu.AddToLegend(WZSmp)
    pu.AddToLegend(ZZSmp)
    pu.DrawLegend()

    labelY = 0.83
    if Options.noRatio: labelY = 0.88
    if wz_region.find("_") != -1:
        wz_region = wz_region[:wz_region.find("_")] + "(" + ("p_{T}(Z)  > p_{T}(W)"
                                                             if wz_region[wz_region.find("_") + 1] == "Z" else "p_{T}(W)  > p_{T}(Z)") + ")"
    pu.DrawPlotLabels(0.195, labelY, "%s vs. %s" % (zz_region, wz_region), analysis, Options.noATLAS)

    ### Draw the ratio to WZ ZZ
    pu.GetTopPad().RedrawAxis()
    pu.GetBottomPad().cd()
    pu.GetBottomPad().SetGridy()

    ratios = XAMPPplotting.Utils.CreateRatioHistos([ZZSmp], WZSmp)

    ymin, ymax = pu.GetFancyAxisRanges(ratios, Options)

    if zz_var != wz_var: ZZSmp.GetAxis(0).SetTitle("")
    pu.drawRatioStyling(ZZSmp, ymin, ymax, "ZZ/ WZ")

    for r in ratios:
        r.Draw("histSAME")
    pu.saveHisto("%s/%s" % (Options.outputDir, canvasName), Options.OutFileType)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description='This script produces MC only histograms from tree files. For more help type \"python MCPlots.py -h\"',
        prog='WZ_CRPlots',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser = XAMPPplotting.Utils.setupBaseParser(parser)
    parser.set_defaults(outputDir="WZ_CRPlots")
    PlottingOptions = parser.parse_args()
    ClusterSubmission.Utils.CreateDirectory(PlottingOptions.outputDir, False)

    # Analyze the samples and adjust the PlottingOptions
    FileStructure = XAMPPplotting.FileStructureHandler.GetStructure(PlottingOptions)

    # do this here, since before it destroys the argparse
    ROOT.gROOT.Macro("rootlogon.C")
    ROOT.gROOT.SetStyle("ATLAS")

    dummy = ROOT.TCanvas("dummy", "dummy", 800, 600)

    Selections_ToPlot = [
        ("3L", "4L"),
        ("3L", "4LnoZ"),
        ("3LnoZ", "4LnoZ"),
        ("3L_Z#geqW", "4L"),
        ("3L_W#geqZ", "4L"),
        ("WZ", "ZZ"),
        ("WZ_Z#geqW", "ZZ"),
        ("WZ_W#geqZ", "ZZ"),
    ]
    Variables_ToPlot = [
        ### Compare the Z from WZ against the best Z in ZZ
        ("BestZ_m", "BestZ_m"),
        ("BestZ_pt", "BestZ_pt"),
        ### In case the Z has less energy than the W compare it to the second Z in the event too
        ("BestZ_m", "SecondZ_m"),
        ("BestZ_pt", "SecondZ_pt"),
        #### Compare the W against the best Z in the event
        ("W_m", "BestZ_m"),
        ("W_pt", "BestZ_pt"),
        #### Also compare it against the second Z in the event in case the Z has more pt than the W
        ("W_m", "SecondZ_m"),
        ("W_pt", "SecondZ_pt"),
        ### The variable of interest to extrapolate meff
        ("meff", "meff"),
        ("Cul_meff", "Cul_meff"),
        ("DiBoson_m", "DiBoson_m"),
        ("DiBoson_pt", "DiBoson_pt"),
    ]
    for ana in FileStructure.GetConfigSet().GetAnalyses():
        for wz_region, zz_region in Selections_ToPlot:
            for wz_var, zz_var in Variables_ToPlot:
                drawComparison(PlottingOptions, ana, wz_region, wz_var, zz_region, zz_var, bonus_str="")
