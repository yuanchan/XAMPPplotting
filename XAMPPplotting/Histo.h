#ifndef XAMPPPlotting_HISTO_H
#define XAMPPPlotting_HISTO_H

#include <TH1D.h>
#include <TH2D.h>
#include <TH2Poly.h>
#include <TH3D.h>

#include <TFile.h>
#include <map>
#include <set>

#include <XAMPPplotting/HistoBins.h>
#include <XAMPPplotting/HistoTemplates.h>
#include <vector>
namespace XAMPP {
    class Weight;
    class ITreeVarReader;
    class IParticleReader;
    class Condition;
    // Standard class in XAMPPplotting to fill histograms.
    class Histo {
    public:
        enum HistoTypes { Undefined, H1, H2, H3 };

        Histo(XAMPP::ITreeVarReader* Reader, const std::string& Name, const std::string& Template);
        Histo(const std::string& Name, const std::string& Template);
        // Constructors not needing a config file based template. They are like the standard TH1D constructors without title.
        Histo(const std::string& Name, unsigned int Nbins, double low, double high);
        Histo(const std::string& Name, unsigned int Nbins, const double* xbins);
        Histo(const std::string& Name, const std::vector<double>& xbins);

        Histo(XAMPP::ITreeVarReader* Reader, const std::string& Name, unsigned int Nbins, double low, double high);
        Histo(XAMPP::ITreeVarReader* Reader, const std::string& Name, unsigned int Nbins, const double* xbins);
        Histo(XAMPP::ITreeVarReader* Reader, const std::string& Name, const std::vector<double>& xbins);

        Histo(const Histo& other);

        // Methods to initialize & fill the histogram
        virtual bool init(TDirectory* D);
        // Reads out the content of an ITreeVarReader
        virtual void fill();
        // If the histogram is not connected with an ITreeVarReader then these methods
        // Can be called
        virtual void Fill(double xValue);
        virtual void Fill(double xValue, double yValue);
        virtual void Fill(double xValue, double yValue, double zValue);

        virtual void write();
        virtual void Statistics() const;

        // No application of the Event weights at all
        void DoRaw(bool B = true);

        // Pull the content of the under and overflow bins into the first and list bin, respectively
        void PullOverFlowBins(bool B = true);
        void PullUnderFlowBins(bool B = true);

        bool PullOverFlow() const;
        bool PullUnderFlow() const;

        // Methods for labeling of the histogram
        std::string name() const;
        void SetXaxisLabel(const std::string& L);
        void SetYaxisLabel(const std::string& L);
        void SetZaxisLabel(const std::string& L);
        void SetBinLabelX(int Bin, const std::string& L);
        void SetBinLabelY(int Bin, const std::string& L);
        void SetBinLabelZ(int Bin, const std::string& L);
        void SetNormToFirstBin(bool B);
        void SetDivideByBinWidth(bool B);
        void SetEntriesLabel(const std::string& L);
        void SetUnitsLabel(const std::string& L);

        void SetCut(std::shared_ptr<Condition> C);

        virtual ~Histo();

        std::string GetTemplate() const;
        // This method has to be overloaded foreach histogram. It returns the right derived Histo class
        virtual std::shared_ptr<Histo> Clone();

    protected:
        // Constuctors allowing to define the type
        Histo(XAMPP::ITreeVarReader* Reader, const std::string& Name, const std::string& Template, Histo::HistoTypes Type);
        Histo(const std::string& Name, const std::string& Template, Histo::HistoTypes Type);

        virtual void Finalize();
        virtual void PullBinsOutOfRange();
        void PullContentInBin(int From, int To);

        void AddBinContent(int Bin) const;
        bool NoReaderConnected() const;
        virtual double weight() const;
        virtual double weight2() const;
        bool passCut() const;

        XAMPP::ITreeVarReader* m_Reader;

    private:
        void SetTitleAndLabels();

        std::string m_Name;
        std::string m_Template;
        HistoTypes m_Type;

        bool m_AltConst;
        bool m_UseWeights;
        bool m_PullOverFlow;
        bool m_PullUnderFlow;
        bool m_normToFirstBin;
        bool m_divideByBinWidth;
        std::string m_entriesLabel;
        std::string m_unitsLabel;

        mutable unsigned int m_Entries;
        std::string m_xLabel;
        std::string m_yLabel;
        std::string m_zLabel;

        std::map<int, std::string> m_xBinLabels;
        std::map<int, std::string> m_yBinLabels;
        std::map<int, std::string> m_zBinLabels;

        XAMPP::Weight* m_Weight;

        std::shared_ptr<Condition> m_Cut;
        bool m_HasCut;
        TDirectory* m_Dir;

    protected:
        std::unique_ptr<TH1> m_TH1;
    };
    class Histo2D : public Histo {
    public:
        // Constructors using ITreeVarReaders and templates
        Histo2D(XAMPP::ITreeVarReader* xReader, XAMPP::ITreeVarReader* yReader, const std::string& Name, const std::string& Template);
        Histo2D(const std::string& Name, const std::string& Template);

        // Constructors without any template. They are like the standard TH2D constructors. Just without any title
        Histo2D(const std::string& Name, unsigned int NBinsX, double xLow, double xHigh, unsigned int NBinsY, double yLow, double yHigh);
        Histo2D(const std::string& Name, unsigned int NBinsX, double xLow, double xHigh, unsigned int NBinsY, double* ybins);
        Histo2D(const std::string& Name, unsigned int NBinsX, double* xbins, unsigned int NBinsY, double yLow, double yHigh);

        Histo2D(XAMPP::ITreeVarReader* xReader, XAMPP::ITreeVarReader* yReader, const std::string& Name, unsigned int NBinsX, double xLow,
                double xHigh, unsigned int NBinsY, double yLow, double yHigh);
        Histo2D(XAMPP::ITreeVarReader* xReader, XAMPP::ITreeVarReader* yReader, const std::string& Name, unsigned int NBinsX, double xLow,
                double xHigh, unsigned int NBinsY, double* ybins);
        Histo2D(XAMPP::ITreeVarReader* xReader, XAMPP::ITreeVarReader* yReader, const std::string& Name, unsigned int NBinsX, double* xbins,
                unsigned int NBinsY, double yLow, double yHigh);

        // Fill methods.
        virtual void fill();
        virtual void Fill(double xValue);
        virtual void Fill(double xValue, double yValue);
        virtual void Fill(double xValue, double yValue, double zValue);

        virtual bool init(TDirectory* D);

        virtual std::shared_ptr<Histo> Clone();

    protected:
        virtual void PullBinsOutOfRange();
        XAMPP::ITreeVarReader* m_yReader;
    };
    class Histo2DPoly : public Histo2D {
    public:
        Histo2DPoly(const std::string& Name, const std::string& Template);
        Histo2DPoly(XAMPP::ITreeVarReader* xReader, XAMPP::ITreeVarReader* yReader, const std::string& Name, const std::string& Template);

        virtual void fill();
        virtual void Fill(double xValue);
        virtual void Fill(double xValue, double yValue);
        virtual void Fill(double xValue, double yValue, double zValue);

        virtual bool init(TDirectory* D);
        virtual std::shared_ptr<Histo> Clone();

    private:
        void FillPoly(double xValue, double yValue);
        virtual void Finalize();
        TH2Poly* m_THPoly;
    };

    class Histo3D : public Histo {
    public:
        // Constructors using ITreeVarReaders and standard templates.
        Histo3D(const std::string& Name, const std::string& Template);
        Histo3D(XAMPP::ITreeVarReader* xReader, XAMPP::ITreeVarReader* yReader, XAMPP::ITreeVarReader* zReader, const std::string& Name,
                const std::string& Template);

        Histo3D(const std::string& Name, unsigned int NBinsX, double xLow, double xHigh, unsigned int NBinsY, double yLow, double yHigh,
                unsigned int NBinsZ, double zLow, double zHigh);
        Histo3D(const std::string& Name, unsigned int NBinsX, double* xbins, unsigned int NBinsY, double* ybins, unsigned int NBinsZ,
                double* zbins);

        Histo3D(XAMPP::ITreeVarReader* xReader, XAMPP::ITreeVarReader* yReader, XAMPP::ITreeVarReader* zReader, const std::string& Name,
                unsigned int NBinsX, double xLow, double xHigh, unsigned int NBinsY, double yLow, double yHigh, unsigned int NBinsZ,
                double zLow, double zHigh);
        Histo3D(XAMPP::ITreeVarReader* xReader, XAMPP::ITreeVarReader* yReader, XAMPP::ITreeVarReader* zReader, const std::string& Name,
                unsigned int NBinsX, double* xbins, unsigned int NBinsY, double* ybins, unsigned int NBinsZ, double* zbins);

        virtual void fill();
        virtual void Fill(double xValue);
        virtual void Fill(double xValue, double yValue);
        virtual void Fill(double xValue, double yValue, double zValue);
        virtual bool init(TDirectory* D);

        virtual std::shared_ptr<Histo> Clone();

    protected:
        XAMPP::ITreeVarReader* m_yReader;
        XAMPP::ITreeVarReader* m_zReader;
    };
    //
    // Implementations to satisfy special purposes making life easier for the user
    //
    class CumulativeHisto : public Histo {
        // The CumulativeHistogram integrates the distibutions from the i-th bin
        // to inf when the Histo is written.
    public:
        CumulativeHisto(XAMPP::ITreeVarReader* Reader, const std::string& Name, const std::string& Template);
        CumulativeHisto(const std::string& Name, const std::string& Template);

        CumulativeHisto(const std::string& Name, unsigned int Nbins, double low, double high);
        CumulativeHisto(const std::string& Name, unsigned int Nbins, const double* xbins);

        CumulativeHisto(XAMPP::ITreeVarReader* Reader, const std::string& Name, unsigned int Nbins, double low, double high);
        CumulativeHisto(XAMPP::ITreeVarReader* Reader, const std::string& Name, unsigned int Nbins, const double* xbins);

        virtual std::shared_ptr<Histo> Clone();

    protected:
        virtual void Finalize();
    };
    class RevCumulativeHisto : public CumulativeHisto {
        // Same as the CumulativeHisto but the integration is performed from the 0-th to the i-th bin
    public:
        RevCumulativeHisto(XAMPP::ITreeVarReader* Reader, const std::string& Name, const std::string& Template);
        RevCumulativeHisto(const std::string& Name, const std::string& Template);

        RevCumulativeHisto(const std::string& Name, unsigned int Nbins, double low, double high);
        RevCumulativeHisto(const std::string& Name, unsigned int Nbins, const double* xbins);

        RevCumulativeHisto(XAMPP::ITreeVarReader* Reader, const std::string& Name, unsigned int Nbins, double low, double high);
        RevCumulativeHisto(XAMPP::ITreeVarReader* Reader, const std::string& Name, unsigned int Nbins, const double* xbins);
        virtual std::shared_ptr<Histo> Clone();

    protected:
        virtual void Finalize();
    };
    class CumulativeHisto2D : public Histo2D {
    public:
        CumulativeHisto2D(const std::string& Name, const std::string& Template);
        CumulativeHisto2D(XAMPP::ITreeVarReader* xReader, XAMPP::ITreeVarReader* yReader, const std::string& Name,
                          const std::string& Template);

        // Constructors without any template. They are like the standard TH2D constructors. Just without any title
        CumulativeHisto2D(const std::string& Name, unsigned int NBinsX, double xLow, double xHigh, unsigned int NBinsY, double yLow,
                          double yHigh);
        CumulativeHisto2D(const std::string& Name, unsigned int NBinsX, double xLow, double xHigh, unsigned int NBinsY, double* ybins);
        CumulativeHisto2D(const std::string& Name, unsigned int NBinsX, double* xbins, unsigned int NBinsY, double yLow, double yHigh);

        CumulativeHisto2D(XAMPP::ITreeVarReader* xReader, XAMPP::ITreeVarReader* yReader, const std::string& Name, unsigned int NBinsX,
                          double xLow, double xHigh, unsigned int NBinsY, double yLow, double yHigh);
        CumulativeHisto2D(XAMPP::ITreeVarReader* xReader, XAMPP::ITreeVarReader* yReader, const std::string& Name, unsigned int NBinsX,
                          double xLow, double xHigh, unsigned int NBinsY, double* ybins);
        CumulativeHisto2D(XAMPP::ITreeVarReader* xReader, XAMPP::ITreeVarReader* yReader, const std::string& Name, unsigned int NBinsX,
                          double* xbins, unsigned int NBinsY, double yLow, double yHigh);

        virtual std::shared_ptr<Histo> Clone();

    protected:
        virtual void Finalize();
    };
    class RevCumulativeHisto2D : public CumulativeHisto2D {
    public:
        RevCumulativeHisto2D(std::string Name, std::string Template);
        RevCumulativeHisto2D(XAMPP::ITreeVarReader* xReader, XAMPP::ITreeVarReader* yReader, std::string Name, std::string Template);
        virtual std::shared_ptr<Histo> Clone();

    protected:
        virtual void Finalize();
    };
    // This histogram is for filling the CutFlows.
    class CutFlowHisto : public Histo {
    public:
        CutFlowHisto(const std::string& Name, const std::string& Region);
        // The I-th cut is passed. Up to this bin all lower bins are filled
        virtual bool init(TDirectory* D);

        virtual void Fill(double xValue);
        // Deactivated Fill methods. New implementation to avoid compiler warnings
        virtual void Fill(double xValue, double yValue);
        virtual void Fill(double xValue, double yValue, double zValue);

        // Set the cuts. A labeled template is created..
        bool SetCuts(const std::vector<std::shared_ptr<Condition>>& Cuts);
        bool SetCuts(const std::vector<std::string>& Cuts);
        virtual std::shared_ptr<Histo> Clone();

    protected:
    };

    // This class handles vectors of XAMPP::Histograms
    class HistoArray : public Histo {
    public:
        HistoArray(const std::string& Name);
        virtual bool init(TDirectory* D);
        virtual void fill();

        virtual void write();
        virtual void Statistics() const;

        void AppendHistogram(const std::vector<std::shared_ptr<Histo>>& Histos);
        void AppendHistogram(std::shared_ptr<Histo> In);

        void SetCut(const std::vector<std::shared_ptr<Condition>>& Cuts);
        void DoCutFlow(bool B);

        virtual ~HistoArray();
        virtual std::shared_ptr<Histo> Clone();

    protected:
        void FillCutFlowHistos(unsigned int Passed);
        std::vector<std::shared_ptr<Histo>> m_ClientHistos;
        std::vector<std::shared_ptr<Condition>> m_Cuts;
        bool m_DoCutFlow;
        std::shared_ptr<CutFlowHisto> m_RawCutFlow;
        std::shared_ptr<CutFlowHisto> m_WeightedCutFlow;
    };

}  // namespace XAMPP

#endif
