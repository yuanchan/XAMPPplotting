#if ATHENA_RELEASE_SERIES == 212
#ifndef XAMPPPPlotting_BTaggingScaleFactor_H
#define XAMPPPPlotting_BTaggingScaleFactor_H

#include <AsgTools/AnaToolHandle.h>
#include <XAMPPplotting/IfDefHelpers.h>
#include <XAMPPplotting/Weight.h>
#include <string>
#include <vector>

class IBTaggingEfficiencyTool;
namespace XAMPP {

    class BTaggingSF : public IWeightElement {
    public:
        virtual std::string name() const;
        virtual bool init(TTree* t);
        virtual double read();

        virtual std::vector<std::string> FindWeightVariations(TTree*);
        virtual IWeightElement::SystStatus ApplySystematic(const std::string& variation);

        virtual void ResetSystematic();
        virtual std::vector<std::string> GetWeightVariations();
        virtual ITreeVarReader* GetTreeVarReader();
        virtual ITreeVarReader* GetSystematicReader(const std::string& Syst);

        bool configureWeight(std::ifstream& inf);

        static BTaggingSF* GetWeighter(const std::string& Name);
        virtual ~BTaggingSF();

    protected:
        BTaggingSF(const std::string& Name);
        BTaggingSF(const BTaggingSF&) = delete;
        void operator=(const BTaggingSF&) = delete;

        double GetTotalBTagSF() const;
        double CalculateBTagSF(double pt, double eta, double tagWeight, int labelID, bool isBTagged) const;

        bool InitBTaggingTool();
        bool InitJets();

    private:
        std::string m_Name;
        std::string m_JetAuthor;
        std::string m_Tagger;
        std::string m_WP;
        std::string m_CalibrationRelease;

        std::string m_JetCollection;

        std::string m_xSecDir;
        XAMPP::NormalizationDataBase* m_MetaData;

        IParticleReader* m_Jet;
        IParticleVarReader* m_JetPt;
        IParticleVarReader* m_JetEta;
        IParticleVarReader* m_JetTagWeight;
        IParticleVarReader* m_JetLabelID;
        IParticleVarReader* m_JetIsBTagged;
        IParticleVarReader* m_JetIsAssociated;

        asg::AnaToolHandle<IBTaggingEfficiencyTool> m_bTaggingTool;
        std::vector<CP::SystematicSet> m_Syst;
        std::vector<std::string> m_SystNames;

        struct bTagSFReaders {
            bTagSFReaders(PseudoScalarVarReader* P, CP::SystematicSet& S) {
                PseudoR = P;
                Set = S;
            }
            PseudoScalarVarReader* PseudoR;
            CP::SystematicSet Set;
        };
        std::vector<bTagSFReaders> m_bTagSFReaders;
        std::vector<bTagSFReaders>::iterator m_bTagSFReaders_Itr;
    };

}  // namespace XAMPP
#endif
#endif
