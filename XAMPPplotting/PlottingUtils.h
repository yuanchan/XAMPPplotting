#ifndef XAMPPPLOTTING_PLOTTINGUTILS_H
#define XAMPPPLOTTING_PLOTTINGUTILS_H

#include <XAMPPplotting/Cuts.h>
#include <cfloat>
#include <climits>
#include <cmath>
#include <fstream>
#include <future>
#include <iostream>
#include <memory>
#include <set>
#include <string>
#include <thread>
#include <vector>

class TH1;
class TAxis;
class TFile;
class TDirectory;
namespace XAMPP {

    typedef std::pair<int, int> Ranges;
    typedef std::pair<Ranges, Ranges> Cube;
    // Assign a constant to -1 casted to size_t
    constexpr size_t reset_idx = -1;
    class Condition;
    class ITreeVarWriter;
    class Histo;

    void PrintHeadLine(const std::string &Title);
    void PrintFooter();
    template <typename T> bool IsElementInList(const std::vector<T> &List, const T &Element) {
        for (const auto &Item : List) {
            if (Item == Element) return true;
        }
        return false;
    }
    ///     From: source vector
    ///     To: destination vector
    ///     func: Decider when the element should be appended
    ///     Clear: Clear the vector beforehand
    template <typename T>
    void CopyVector(const std::vector<T> &From, std::vector<T> &To, std::function<bool(const T &)> func, bool Clear = false) {
        if (Clear) To.clear();
        if (To.capacity() < From.size()) To.reserve(From.size() + To.capacity());
        for (auto &Ele : From) {
            if (func(Ele)) To.push_back(Ele);
        }
        To.shrink_to_fit();
    }
    template <typename T> void CopyVector(const std::vector<T> &From, std::vector<T> &To, bool Clear = false) {
        CopyVector<T>(From, To, [&To](const T &ele) { return !IsElementInList(To, ele); }, Clear);
    }
    template <typename T> void CopyVector(const std::vector<T> &From, std::set<T> &To, bool Clear = false) {
        if (Clear) To.clear();
        for (const auto &F : From) To.insert(F);
    }
    template <typename T> void ClearFromDuplicates(std::vector<T> &toClear) {
        std::vector<T> copy = toClear;
        CopyVector(copy, toClear, true);
    }
    template <typename T>
    unsigned int count(typename std::vector<T>::const_iterator begin, typename std::vector<T>::const_iterator end,
                       std::function<bool(const T &)> func) {
        unsigned int n = 0;
        for (; begin != end; ++begin) {
            if (func(*begin)) ++n;
        }
        return n;
    }

    template <typename T> unsigned int count(const std::vector<T> &vector, std::function<bool(const T &)> func) {
        return count<T>(vector.begin(), vector.end(), func);
    }
    template <typename T> unsigned int count_active(const std::vector<std::future<T>> &threads) {
        return count<std::future<T>>(threads, [](const std::future<T> &th) {
            using namespace std::chrono_literals;
            return th.wait_for(0ms) != std::future_status::ready;
        });
    }
    template <typename T> void EraseFromVector(std::vector<T> &vec, std::function<bool(const T &)> func) {
        typename std::vector<T>::iterator itr = std::find_if(vec.begin(), vec.end(), func);
        while (itr != vec.end()) {
            vec.erase(itr);
            itr = std::find_if(vec.begin(), vec.end(), func);
        }
    }
    template <typename T> void RemoveElement(std::vector<T> &Vec, const T &Ele) {
        EraseFromVector<T>(Vec, [&Ele](const T &in_vec) { return Ele == in_vec; });
    }

    template <class T> std::ostream &operator<<(std::ostream &os, const std::vector<T> &v) {
        os << "{";
        for (const auto &i : v) { os << " " << i; }
        os << " }";
        return os;
    }
    template <class T, class U> std::ostream &operator<<(std::ostream &os, const std::pair<T, U> &e) {
        os << "(" << e.first << "," << e.second << ")";
        return os;
    }
    template <class T> bool operator==(const std::vector<T> &v1, const std::vector<T> &v2) {
        if (v1.size() != v2.size()) return false;
        typename std::vector<T>::const_iterator begin_v1 = v1.begin();
        typename std::vector<T>::const_iterator begin_v2 = v2.begin();
        typename std::vector<T>::const_iterator end_v1 = v1.end();
        for (; begin_v1 != end_v1; ++begin_v1, ++begin_v2) {
            if (*begin_v1 != *begin_v2) return false;
        }
        return true;
    }

    template <typename T> int max_bit(const T &number) {
        for (int bit = sizeof(number) * 8 - 1; bit >= 0; --bit) {
            if (number & (1 << bit)) return bit;
        }
        return -1;
    }
    template <typename T> int min_bit(const T &number) {
        for (unsigned int bit = 0; bit <= sizeof(number) * 8 - 1; ++bit) {
            if (number & (1 << bit)) return bit;
        }
        return -1;
    }

    std::string GetWordFromStream(std::stringstream &sstr, bool Reset = false);
    int GetIntegerFromStream(std::stringstream &sstr, bool Reset = false);
    float GetFloatFromStream(std::stringstream &sstr, bool Reset = false);
    double GetDoubleFromStream(std::stringstream &sstr, bool Reset = false);

    double truncate(const double &dbl, unsigned int prec);
    bool IsKeyWordSatisfied(std::stringstream &sstr, const std::string &word);
    bool IsInNextWord(std::stringstream &sstr, const std::string &word);

    void ExtractListFromStream(std::stringstream &sstr, std::vector<std::string> &List, bool ClearList = false);
    void ExtractListFromStream(std::stringstream &sstr, std::vector<int> &List, bool ClearList = false);
    void ExtractListFromStream(std::stringstream &sstr, std::vector<float> &List, bool ClearList = false);

    // File opening functions
    std::shared_ptr<TFile> Open(const std::string &Path);

    void PrintHistogram(TH1 *H);

    // Histogram manipulation
    unsigned int GetNbins(const std::shared_ptr<TH1> &Histo);
    unsigned int GetNbins(const TH1 *Histo);

    bool isOverflowBin(const std::shared_ptr<TH1> &Histo, int bin);
    bool isOverflowBin(const TH1 *Histo, int bin);
    bool isOverflowBin(const TAxis *a, int bin);

    bool isAlphaNumeric(std::shared_ptr<TH1> histo);
    bool isAlphaNumeric(TH1 *histo);

    double PlotRange(const TAxis *a);

    double GetBinNormalization(const TH1 *H1, int Bin);
    double GetBinNormalization(const std::shared_ptr<TH1> &H1, int Bin);

    // Integral functions which take into account that the histogram is normalized to the first bin
    double Integrate(const std::shared_ptr<TH1> &Histo, unsigned int xStart = 0, int xEnd = -1, unsigned int yStart = 0, int yEnd = -1,
                     unsigned int zStart = 0, int zEnd = -1);
    double Integrate(const TH1 *Histo, unsigned int xStart = 0, int xEnd = -1, unsigned int yStart = 0, int yEnd = -1,
                     unsigned int zStart = 0, int zEnd = -1);

    std::pair<double, double> IntegrateAndError(const std::shared_ptr<TH1> &Histo, unsigned int xStart = 0, int xEnd = -1,
                                                unsigned int yStart = 0, int yEnd = -1, unsigned int zStart = 0, int zEnd = -1);
    std::pair<double, double> IntegrateAndError(const TH1 *Histo, unsigned int xStart = 0, int xEnd = -1, unsigned int yStart = 0,
                                                int yEnd = -1, unsigned int zStart = 0, int zEnd = -1);

    double GetMinimum(const std::shared_ptr<TH1> &histo, double min_value = -DBL_MAX);
    double GetMinimum(const TH1 *histo, double min_value = DBL_MIN);
    double GetMaximum(const std::shared_ptr<TH1> &histo, double max_value = DBL_MAX);
    double GetMaximum(const TH1 *histo, double max_value = DBL_MAX);

    int GetMinimumBin(const std::shared_ptr<TH1> &histo, double min_value = -DBL_MAX);
    int GetMinimumBin(const TH1 *histo, double min_value = DBL_MIN);
    int GetMaximumBin(const std::shared_ptr<TH1> &histo, double max_value = DBL_MAX);
    int GetMaximumBin(const TH1 *histo, double max_value = DBL_MAX);

    std::shared_ptr<TH1> clone(const std::shared_ptr<TH1> &histo, bool reset = false);
    std::shared_ptr<TH1> clone(const TH1 *histo, bool reset = false);

    std::shared_ptr<TH1> ProjectInto1D(const std::shared_ptr<TH1> &H3, unsigned int project_along, int bin1_start = 0, int bin1_end = -1,
                                       int bin2_start = 0, int bin2_end = -1);
    std::shared_ptr<TH1> ProjectInto1D(const TH1 *H3, unsigned int project_along, int bin1_start = 0, int bin1_end = -1, int bin2_start = 0,
                                       int bin2_end = -1);

    std::shared_ptr<TH1> ProjectInto2D(const std::shared_ptr<TH1> &H3, unsigned int project_along, int bin_start, int bin_end);
    std::shared_ptr<TH1> ProjectInto2D(const TH1 *H3, unsigned int project_along, int bin_start, int bin_end);

    std::shared_ptr<TH1> MakeTH1(std::vector<double> BinEdgesX, std::vector<double> BinEdgesY = std::vector<double>(),
                                 std::vector<double> BinEdgesZ = std::vector<double>());

    /// Rebins the histogram such that each bin has a relative error less than max_stat_error
    std::shared_ptr<TH1> ReBinHisto(const TH1 *H, double max_stat_error);
    std::shared_ptr<TH1> ReBinHisto(const std::shared_ptr<TH1> &H, double max_stat_error);

    void CopyStyling(const TH1 *from, TH1 *to);
    /// Tansmutes the binning from one shape into another
    bool TransformToBinning(const TH1 *from, TH1 *to);

    /// Extracts the binning from the Axis including high and low edges
    std::vector<double> ExtractBinning(const TAxis *axis);

    bool IsFinite(const std::shared_ptr<TH1> &histo);
    bool IsFinite(const TH1 *histo);

    std::string TimeHMS(float t);
    std::string SeparationLine();
    std::string WhiteSpaces(int N = 0, std::string space = " ");
    std::string EraseWhiteSpaces(std::string str);
    std::string LongestCommonString(const std::string &str1, const std::string &str2);
    std::string RandomString(size_t s);

    const int MsgMethodChars = 35;
    void Info(const std::string &Method, const std::string &Message);
    void Error(const std::string &Method, const std::string &Message);
    void Warning(const std::string &Method, const std::string &Message);

    std::shared_ptr<Condition> CreateCut(ITreeVarReader *Var, std::stringstream &sstr);
    std::shared_ptr<Condition> CreateCut(const std::string &line, std::vector<ITreeVarReader *> &Readers);

    Condition::Mode GetCombineModeFromLine(const std::string &line);
    std::shared_ptr<Condition> CreateCombinedCut(std::ifstream &inf, std::vector<ITreeVarReader *> &Readers, const std::string &line);
    std::shared_ptr<Condition> CreateCombinedCut(std::ifstream &inf, std::vector<ITreeVarReader *> &Readers, Condition::Mode M);

    std::shared_ptr<Histo> CreateHisto(std::ifstream &inf, std::vector<ITreeVarReader *> &Readers);
    ITreeVarWriter *CreateOutBranch(std::ifstream &inf, std::vector<ITreeVarReader *> &Readers);

    std::vector<std::string> ListDirectory(std::string Path, std::string WildCard);
    std::vector<std::string> GetPathResolvedFileList(const std::vector<std::string> &Files);
    std::string ResolvePath(const std::string &path);
    bool DoesFileExist(const std::string &Path);
    bool DoesDirectoryExist(const std::string &Path);

    bool GetLine(std::ifstream &inf, std::string &line);

    bool IsFinite(double N);
    int gcd(int a, int b);

    std::string ReplaceExpInString(std::string str, const std::string &exp, const std::string &rep);
    std::string ResolveEnviromentVariables(std::string str);

    Ranges GetRangesFromStr(std::string str);
    Cube GetMatrixRangesToRead(std::string &Instr);
    std::string GetMatrixRangesFromCube(Cube C);

    unsigned int binomial(unsigned int n, unsigned int m);

    std::vector<size_t> sequence(size_t to, size_t start = 0);

    TDirectory *mkdir(const std::string &name, TDirectory *in_dir);
    std::shared_ptr<TH1> fetch_from_file(const std::string &path, TDirectory *in_dir);
    std::vector<std::string> recursive_ls(TDirectory *d, const std::string &pre_path = "");
    bool isElementSubstr(const std::vector<std::string> &vec, const std::string &str);

    bool same_binning(const std::shared_ptr<TH1> &H1, const std::shared_ptr<TH1> &H2);

}  // namespace XAMPP
#endif
