#include <XAMPPplotting/TreeVarReader.h>
namespace XAMPP {
    //#########################################################################################
    //                                  ScalarVarReader
    //#########################################################################################
    template <class T>
    ScalarVarReader<T>::ScalarVarReader(const std::string& varname) :
        IScalarReader(),
        m_var_name(varname),
        m_var(0),
        m_TreeNumber(-1),
        m_lastEvent(-1),
        m_branch(nullptr),
        m_evService(EventService::getService()),
        m_indexer(nullptr),
        m_Registered(false) {
        ITreeVarReaderStorage* S = ITreeVarReaderStorage::GetInstance();
        m_Registered = S->Register(this);
    }
    template <class T> std::string ScalarVarReader<T>::name() const { return m_var_name; }
    template <class T> double ScalarVarReader<T>::read() const {
        if (m_evService->partialEvent() && m_branch != nullptr && m_lastEvent != m_indexer->currentEvent()) {
            m_branch->GetEntry(m_indexer->currentEvent());
            m_lastEvent = m_indexer->currentEvent();
        }
        return m_var;
    }
    template <class T> bool ScalarVarReader<T>::init(TTree* t) {
        if (!t) {
            Error("ScalarVarReader::init()", "No tree given.");
            return false;
        }
        if (!m_Registered) {
            Error("ScalarVarReader::init()", "The reader " + name() + " is not registered in the storage.");
            return false;
        } else if (m_TreeNumber == m_evService->n_thTree())
            return true;
        TBranch* br = t->GetBranch(m_var_name.c_str());
        if (!br) {
            Error("ScalarVarReader::init()", "There is no Branch " + name() + " in the input tree " + std::string(t->GetName()) + ".");
            return false;
        }

        m_indexer = m_evService->getIndexer(br);
        if (!m_indexer) {
            Error("ScalarVarReader::init()", "No corresponding indexer could be found for " + name());
            return false;
        }
        m_branch = m_indexer->tree()->GetBranch(name().c_str());
        m_lastEvent = -1;
        m_branch->SetStatus(true);
        if (m_indexer->tree()->AddBranchToCache(name().c_str()) == -1) {
            Error("ScalarVarReader::init()", "Could not add the Branch " + name() + " to the read out cache.");
            return false;
        }
        if (m_indexer->tree()->SetBranchAddress(name().c_str(), &m_var) != 0) {
            Error("ScalarVarReader::init()", "Could not connect is not the variable " + name() + " with the tree " +
                                                 std::string(t->GetName()) + ". Seems the variable is of wrong type.");
            return false;
        }
        m_branch = m_indexer->tree()->GetBranch(name().c_str());
        m_TreeNumber = m_evService->n_thTree();
        reset();
        return true;
    }
    template <class T> ScalarVarReader<T>* ScalarVarReader<T>::GetReader(const std::string& varname) {
        ITreeVarReaderStorage* S = ITreeVarReaderStorage::GetInstance();
        if (!S->GetReader(varname)) new ScalarVarReader<T>(varname);
        return dynamic_cast<ScalarVarReader<T>*>(S->GetReader(varname));
    }
    template <class T> const T& ScalarVarReader<T>::get() const { return m_var; }
    //#########################################################################################
    //                                  ScalarVarReaderGeV
    //#########################################################################################
    template <class T> ScalarVarReaderGeV<T>::ScalarVarReaderGeV(const std::string& varname) : ScalarVarReader<T>(varname) {}
    template <class T> ScalarVarReaderGeV<T>* ScalarVarReaderGeV<T>::GetReader(const std::string& varname) {
        ITreeVarReaderStorage* S = ITreeVarReaderStorage::GetInstance();
        if (!S->GetReader(varname)) new ScalarVarReaderGeV<T>(varname);
        return dynamic_cast<ScalarVarReaderGeV<T>*>(S->GetReader(varname));
    }
    template <class T> double ScalarVarReaderGeV<T>::read() const { return ScalarVarReader<T>::read() / 1000.; }

    //#########################################################################################
    //                                  VectorVarReader
    //#########################################################################################
    template <class T>
    VectorVarReader<T>::VectorVarReader(const std::string& varname) :
        IVectorReader(),
        m_Registered(false),
        m_lastEvent(-1),
        m_TreeNumber(-1),
        m_branch(nullptr),
        m_evService(EventService::getService()),
        m_indexer(nullptr),
        m_var_name(varname),
        m_var(nullptr) {
        ITreeVarReaderStorage* S = ITreeVarReaderStorage::GetInstance();
        m_Registered = S->Register(this);
    }
    template <class T> VectorVarReader<T>* VectorVarReader<T>::GetReader(const std::string& varname) {
        ITreeVarReaderStorage* S = ITreeVarReaderStorage::GetInstance();
        if (!S->GetReader(varname)) new VectorVarReader<T>(varname);
        return dynamic_cast<VectorVarReader<T>*>(S->GetReader(varname));
    }
    template <class T> std::string VectorVarReader<T>::name() const { return m_var_name; }
    template <class T> double VectorVarReader<T>::readEntry(const size_t I) const {
        if (I >= entries()) {
            Warning("VectorVarReader::readEntry()", name() + " the given index " + std::to_string(I) + " is out of range. Return 1.e25");
            return 1.e25;
        }
        return (*m_var)[I];
    }
    template <class T> size_t VectorVarReader<T>::entries() const {
        if (m_branch == nullptr)
            return 0;
        else if (m_evService->partialEvent() && m_lastEvent != m_indexer->currentEvent()) {
            m_branch->GetEntry(m_indexer->currentEvent());
            m_lastEvent = m_indexer->currentEvent();
        }
        if (m_var == nullptr) {
            Warning("VectorVarReader::entries()",
                    Form("No information about %s is available in event %llu.", name().c_str(), m_evService->currentEvent()));
            return 0;
        }
        return m_var->size();
    }
    template <class T> const std::vector<T>& VectorVarReader<T>::get() const {
        if (m_branch == nullptr || m_var == nullptr) {
            Warning("VectorVarReader::get()", name() + " is not defined. Segmentation violation ahead");
        }
        return *m_var;
    }
    template <class T> bool VectorVarReader<T>::init(TTree* t) {
        if (!t) {
            Error("VectorVarReader::init()", "No tree given.");
            return false;
        }
        if (!m_Registered) {
            Error("VectorVarReader::init()", "The reader " + name() + " is not registered in the storage.");
            return false;
        } else if (m_TreeNumber == m_evService->n_thTree())
            return true;
        TBranch* br = t->GetBranch(name().c_str());
        if (!br) {
            Error("VectorVarReader::init()", "There is no Branch " + name() + " in the input tree " + std::string(t->GetName()) + ".");
            return false;
        }
        m_indexer = m_evService->getIndexer(br);
        if (!m_indexer) {
            Error("VectorVarReader::init()", "No corresponding indexer could be found for " + name());
            return false;
        }

        m_lastEvent = -1;
        m_branch = m_indexer->tree()->GetBranch(name().c_str());
        m_branch->SetStatus(true);
        if (m_indexer->tree()->AddBranchToCache(name().c_str()) == -1) {
            Error("VectorVarReader::init()", "Could not add the Branch " + name() + " to the read out cache.");
            return false;
        }
        if (m_indexer->tree()->SetBranchAddress(name().c_str(), &m_var) != 0) {
            Error("VectorVarReader::init()", "Could not connect is not the variable " + name() + " with the tree " +
                                                 std::string(t->GetName()) + ". Seems the variable is of wrong type.");
            return false;
        }
        m_TreeNumber = m_evService->n_thTree();
        reset();
        return true;
    }
    //#########################################################################################
    //                                  VectorVarReaderGeV
    //#########################################################################################
    template <class T> VectorVarReaderGeV<T>::VectorVarReaderGeV(const std::string& varname) : VectorVarReader<T>(varname) {}
    template <class T> VectorVarReaderGeV<T>* VectorVarReaderGeV<T>::GetReader(const std::string& varname) {
        ITreeVarReaderStorage* S = ITreeVarReaderStorage::GetInstance();
        if (!S->GetReader(varname)) new VectorVarReaderGeV<T>(varname);
        return dynamic_cast<VectorVarReaderGeV<T>*>(S->GetReader(varname));
    }
    template <class T> double VectorVarReaderGeV<T>::readEntry(const size_t I) const { return VectorVarReader<T>::readEntry(I) / 1000.; }
    //#########################################################################################
    //                                  MatrixVarReader
    //#########################################################################################
    template <class T>
    MatrixVarReader<T>::MatrixVarReader(const std::string& varname) :
        IMatrixReader(),
        m_Registered(false),
        m_var_name(varname),
        m_var(0),
        m_lastEvent(-1),
        m_TreeNumber(-1),
        m_branch(nullptr),
        m_evService(EventService::getService()),
        m_indexer(nullptr) {
        ITreeVarReaderStorage* S = ITreeVarReaderStorage::GetInstance();
        m_Registered = S->Register(this);
    }
    template <class T> MatrixVarReader<T>* MatrixVarReader<T>::GetReader(const std::string& varname) {
        ITreeVarReaderStorage* S = ITreeVarReaderStorage::GetInstance();
        if (!S->GetReader(varname)) new MatrixVarReader<T>(varname);
        return dynamic_cast<MatrixVarReader<T>*>(S->GetReader(varname));
    }
    template <class T> std::string MatrixVarReader<T>::name() const { return m_var_name; }
    template <class T> size_t MatrixVarReader<T>::num_rows() const {
        if (m_branch == nullptr)
            return 0;
        else if (m_evService->partialEvent() && m_lastEvent != m_indexer->currentEvent()) {
            m_branch->GetEntry(m_indexer->currentEvent());
            m_lastEvent = m_indexer->currentEvent();
        }
        if (m_var == nullptr) {
            Warning("VectorVarReader::num_rows()",
                    Form("No information about %s available in event %llu", name().c_str(), m_evService->currentEvent()));
            return 0;
        }

        return m_var->size();
    }
    template <class T> bool MatrixVarReader<T>::init(TTree* t) {
        if (!t) {
            Error("MatrixVarReader::init()", "No tree given.");
            return false;
        }
        if (!m_Registered) {
            Error("MatrixVarReader::init()", "The reader " + name() + " is not registered in the storage.");
            return false;
        } else if (m_TreeNumber == m_evService->n_thTree())
            return true;
        TBranch* br = t->GetBranch(m_var_name.c_str());
        if (!br) {
            Error("VectorVarReader::init()", "There is no Branch " + name() + " in the input tree " + std::string(t->GetName()) + ".");
            return false;
        }
        m_indexer = m_evService->getIndexer(br);
        if (!m_indexer) {
            Error("VectorVarReader::init()", "No corresponding indexer could be found for " + name());
            return false;
        }
        m_lastEvent = -1;
        m_branch = m_indexer->tree()->GetBranch(name().c_str());
        m_branch->SetStatus(true);
        if (m_indexer->tree()->AddBranchToCache(name().c_str()) == -1) {
            Error("MatrixVarReader::init()", "Could not add the Branch " + name() + " to the read out cache.");
            return false;
        }
        if (m_indexer->tree()->SetBranchAddress(name().c_str(), &m_var) != 0) {
            Error("MatrixVarReader::init()", "Could not connect is not the variable " + name() + " with the tree " +
                                                 std::string(t->GetName()) + ". Seems the variable is of wrong type.");
            return false;
        }
        m_TreeNumber = m_evService->n_thTree();
        reset();
        return true;
    }
    template <class T> double MatrixVarReader<T>::readMatrixEntry(const size_t i, const size_t j) const {
        if (j >= row_entries(i)) {
            Warning("MatrixVarReader::readMatrixEntry()",
                    Form("The element %lu %lu,  is not part of %s. Return 1.e25", i, j, name().c_str()));
            return 1.e25;
        }
        return (*m_var)[i][j];
    }
    template <class T> size_t MatrixVarReader<T>::row_entries(size_t i) const {
        if (i >= num_rows()) { return 0; }
        return (*(m_var->begin() + i)).size();
    }
    //#########################################################################################
    //                                  MatrixVarReaderGeV
    //#########################################################################################
    template <class T> double MatrixVarReaderGeV<T>::readMatrixEntry(const size_t i, const size_t j) const {
        return MatrixVarReader<T>::readMatrixEntry(i, j) / 1000.;
    }
    template <class T> MatrixVarReaderGeV<T>* MatrixVarReaderGeV<T>::GetReader(const std::string& varname) {
        ITreeVarReaderStorage* S = ITreeVarReaderStorage::GetInstance();
        if (!S->GetReader(varname)) new MatrixVarReaderGeV<T>(varname);
        return dynamic_cast<MatrixVarReaderGeV<T>*>(S->GetReader(varname));
    }
    template <class T> MatrixVarReaderGeV<T>::MatrixVarReaderGeV(const std::string& varname) : MatrixVarReader<T>(varname) {}

    //#########################################################################################
    //                                  ParticleReader
    //#########################################################################################
    template <typename T> ITreeVectorReader* ParticleReader::NewVariable(const std::string& Name) {
        if (IsVariableDefined(Name)) return GetVariable(Name);
        ITreeVectorReader* Reader = VectorVarReader<T>::GetReader(m_ParCollection + "_" + Name);
        if (Reader) {
            m_Vars.insert(std::pair<std::string, ITreeVectorReader*>(Name, Reader));
        } else {
            Error("ParticleReader::NewVariable()", "Could not add VectorVarReader " + Name + " to particle " + name() + ".");
        }
        return Reader;
    }
    template <typename T> ITreeVectorReader* ParticleReader::NewVariableGeV(const std::string& Name) {
        if (IsVariableDefined(Name)) return GetVariable(Name);
        if (!m_UseGeVReader) {
            Warning("ParticleReader::NewVariable()", "Tanslation of MeV to GeV deactivated for " + name() + ".");
            Warning("ParticleReader::NewVariable()", "Create usual VectorReader for variable " + Name + ".");
            return NewVariable<T>(Name);
        }
        ITreeVectorReader* Reader = VectorVarReaderGeV<T>::GetReader(m_ParCollection + "_" + Name);
        if (Reader) {
            m_Vars.insert(std::pair<std::string, ITreeVectorReader*>(Name, Reader));
        } else {
            Error("ParticleReader::NewVariable()", "Could not add VectorVarReader " + Name + " to particle " + name() + ".");
        }
        return Reader;
    }
    template <typename T> ITreeMatrixReader* ParticleReader::NewMatrixVariable(const std::string& Name) {
        // Remove possible  MyName[x] in the name
        std::string VarName = Name.substr(0, Name.find("["));
        if (IsVariableDefined(VarName)) return GetMatrixVariable(VarName);
        ITreeMatrixReader* Reader = MatrixVarReader<T>::GetReader(m_ParCollection + "_" + VarName);
        if (Reader) {
            m_Vars.insert(std::pair<std::string, ITreeVectorReader*>(VarName, Reader));
        } else {
            Error("ParticleReader::NewVariable()", "Could not add MatrixVarReader " + VarName + " to particle " + name() + ".");
        }
        return Reader;
    }
    template <typename T> ITreeMatrixReader* ParticleReader::NewMatrixVariableGeV(const std::string& Name) {
        // Remove possible  MyName[x] in the name
        std::string VarName = Name.substr(0, Name.find("["));
        if (IsVariableDefined(VarName)) return GetMatrixVariable(VarName);
        if (!m_UseGeVReader) {
            Warning("ParticleReader::NewVariable()", "Tanslation of MeV to GeV deactivated for " + name() + ".");
            Warning("ParticleReader::NewVariable()", "Create usual VectorReader for variable " + VarName + ".");
            return NewMatrixVariable<T>(Name);
        }
        ITreeMatrixReader* Reader = MatrixVarReaderGeV<T>::GetReader(m_ParCollection + "_" + VarName);
        if (Reader) {
            m_Vars.insert(std::pair<std::string, ITreeVectorReader*>(VarName, Reader));
        } else {
            Error("ParticleReader::NewMatrixVariableGeV()", "Could not add MatrixVarReader " + VarName + " to particle " + name() + ".");
        }
        return Reader;
    }
    template <typename T> bool ParticleReader::AttachVariable(const std::string& var_name, bool UseGeV) {
        if (m_Setup) {
            Error("ParticleReader::AttachVariable()", Form("%s is already setup", name().c_str()));
            return false;
        }
        ITreeVectorReader* Variable = !UseGeV ? NewVariable<T>(var_name) : NewVariableGeV<T>(var_name);
        return Variable != nullptr;
    }
    template <typename T> bool ParticleReader::AttachVector(const std::string& var_name, bool UseGeV) {
        if (m_Setup) {
            Error("ParticleReader::AttachVector()", Form("%s is already setup", name().c_str()));
            return false;
        }
        ITreeMatrixReader* Variable = !UseGeV ? NewMatrixVariable<T>(var_name) : NewMatrixVariableGeV<T>(var_name);
        return Variable != nullptr;
    }
    template <typename T>
    std::shared_ptr<Condition> ParticleReader::AddSimpleCut(const std::string& name, const std::string& condition, float CutValue,
                                                            bool InGeV) {
        if (m_Setup) {
            Error("ParticleReader::AddSimpleCut()", "The definition of the paricles is already fixed");
            return nullptr;
        }
        AddP4Readers();
        ITreeVectorReader* Variable = !InGeV ? NewVariable<T>(name) : NewVariableGeV<T>(name);
        std::stringstream sstr(Form("%s %f", condition.c_str(), CutValue));
        std::shared_ptr<Condition> newCut = CreateCut(Variable, sstr);
        if (!newCut) return nullptr;
        m_Cuts.push_back(newCut);
        return newCut;
    }

    //#########################################################################################
    //                                  ArrayReader
    //#########################################################################################
    template <class T>
    ArrayReader<T>::ArrayReader(const std::string& varname, ITreeVarReader* SizeBranch, unsigned int Cache) :
        IVectorReader(),
        m_Registered(false),
        m_var_name(varname),
        m_var(new T[Cache]),
        m_lastEvent(-1),
        m_TreeNumber(-1),
        m_branch(nullptr),
        m_evService(EventService::getService()),
        m_indexer(nullptr),
        m_Size(SizeBranch) {
        ITreeVarReaderStorage::GetInstance()->Register(this);
    }
    template <class T> std::string ArrayReader<T>::name() const { return m_var_name; }
    template <class T> bool ArrayReader<T>::init(TTree* t) {
        if (!t) {
            Error("ArrayReader::init()", "No tree given.");
            return false;
        }
        if (!m_Size) {
            Error("ArrayReader::init()", "The reader " + name() + " has no associated array size branch");
            return false;
        }
        if (!m_Size->init(t)) { return false; }
        if (!m_Registered) {
            Error("ArrayReader::init()", "The reader " + name() + " is not registered in the storage.");
            return false;
        } else if (m_TreeNumber == m_evService->n_thTree())
            return true;
        TBranch* br = t->GetBranch(m_var_name.c_str());
        if (!br) {
            Error("VectorVarReader::init()", "There is no Branch " + name() + " in the input tree " + std::string(t->GetName()) + ".");
            return false;
        }
        m_lastEvent = -1;
        m_indexer = m_evService->getIndexer(br);

        if (!m_indexer) {
            Error("VectorVarReader::init()", "No corresponding indexer could be found for " + name());
            return false;
        }
        m_branch = nullptr;
        m_indexer->tree()->SetBranchStatus(m_var_name.c_str(), 1);
        if (m_indexer->tree()->AddBranchToCache(m_var_name.c_str()) == -1) {
            Error("MatrixVarReader::init()", "Could not add the Branch " + name() + " to the read out cache.");
            return false;
        }
        if (m_indexer->tree()->SetBranchAddress(m_var_name.c_str(), &m_var) != 0) {
            Error("MatrixVarReader::init()", "Could not connect is not the variable " + name() + " with the tree " +
                                                 std::string(t->GetName()) + ". Seems the variable is of wrong type.");
            return false;
        }
        m_branch = m_indexer->tree()->GetBranch(name().c_str());
        m_TreeNumber = m_evService->n_thTree();
        reset();
        return true;
    }
    template <class T> double ArrayReader<T>::readEntry(const size_t I) const {
        if (I >= entries()) {
            Warning("ArrayReader::readEntry()", name() + " the given index " + std::to_string(I) + " is out of range. Return 1.e25");
            return 1.e25;
        }
        return I;
    }
    template <class T> size_t ArrayReader<T>::entries() const {
        if (m_branch == nullptr)
            return 0;
        else if (m_evService->partialEvent() && m_lastEvent != m_indexer->currentEvent()) {
            m_branch->GetEntry(m_indexer->currentEvent());
            m_lastEvent = m_indexer->currentEvent();
        }
        return m_Size->read();
    }
    template <class T> ArrayReader<T>* ArrayReader<T>::GetReader(const std::string& varname, ITreeVarReader* Size, unsigned int Cache) {
        ITreeVarReaderStorage* S = ITreeVarReaderStorage::GetInstance();
        if (!S->GetReader(varname)) new ArrayReader<T>(varname, Size, Cache);
        return dynamic_cast<ArrayReader<T>*>(S->GetReader(varname));
    }
    template <class T> ArrayReader<T>::~ArrayReader() { delete m_var; }
}  // namespace XAMPP
