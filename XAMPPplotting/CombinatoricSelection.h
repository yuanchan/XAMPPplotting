#ifndef XAMPPPlotting_COMBINATORICSELECTION_H
#define XAMPPPlotting_COMBINATORICSELECTION_H
#include <XAMPPplotting/EventService.h>
#include <XAMPPplotting/NTupleWriter.h>
#include <XAMPPplotting/PlottingUtils.h>
#include <XAMPPplotting/TreeVarReader.h>
#include <XAMPPplotting/Weight.h>

namespace XAMPP {

    /// Helper class to draw M elements from a set with N distnictive
    /// elements.
    class DrawElementsFromSet {
    public:
        DrawElementsFromSet(unsigned int n, unsigned int m, unsigned int offset = 0);
        DrawElementsFromSet(const DrawElementsFromSet&) = delete;
        void operator=(const DrawElementsFromSet&) = delete;

        unsigned int n() const;
        unsigned int m() const;
        /// Global offset shifting the values
        unsigned int offset() const;

        /// How many possibilities to draw m elemenets from a set of n
        unsigned int size() const;
        /// Returns the  i-th value in the n-th possibility
        unsigned int value_in_tuple(unsigned int ele, unsigned int pos) const;

        /// Get all combinations in one go
        std::vector<unsigned int> draw(unsigned int ele) const;

    private:
        unsigned int value_in_tuple(unsigned int ele, unsigned int pos, unsigned int min) const;
        unsigned int get_zeroth(unsigned int ele) const;

        unsigned int m_N;
        unsigned int m_M;
        unsigned int m_offset;
        unsigned int m_size;
        std::unique_ptr<DrawElementsFromSet> m_n_minus1;
        std::vector<unsigned int> m_borders;
    };

    class IPermutationElement {
    public:
        virtual ~IPermutationElement();
        virtual IParticleReader* particle() const = 0;
        virtual unsigned int n_combinations() const = 0;
        virtual bool include_combinatorics() const = 0;
        virtual std::shared_ptr<DrawElementsFromSet> combinatoric_set() const = 0;
        // Returns a list of indeces where the particles were not in the
        // overflow/underflow of the histogram or a cut was missing
        virtual const std::vector<unsigned int>& get_entered() const = 0;

        virtual unsigned int combinatoric_offset() const = 0;
        IPermutationElement();
    };
    class CombinatoricService {
    public:
        static CombinatoricService* getService();
        ~CombinatoricService();

        std::shared_ptr<DrawElementsFromSet> comb_to_draw(unsigned int n, unsigned k, unsigned int offset = 0);
        bool next();
        unsigned int current_combinatoric() const;

        void attach(IPermutationElement* element);
        void detach(IPermutationElement* element);
        inline Long64_t last_cached_event() const { return m_event_number; };

        std::vector<IPermutationElement*> permuations() const;

        bool has_permutable() const;

    private:
        CombinatoricService();
        void operator=(const CombinatoricService&) = delete;
        CombinatoricService(const CombinatoricService&) = delete;
        static CombinatoricService* m_Inst;

        Weight* m_weighter;
        EventService* m_service;
        std::vector<std::shared_ptr<DrawElementsFromSet>> m_draw_sets;
        std::vector<IPermutationElement*> m_permut_ele;
        Long64_t m_event_number;
        unsigned int m_combinations;
        unsigned int m_ptr;
        bool m_cleared;
    };

    /// The subset n-tuple writer writes hist-fitter trees as well, but
    /// with the augmented functionallity to dump a given event twice based
    /// on a combinatoric selection applied on Particle Containers
    class SubSetNTupleWriter : public NTupleWriter {
    public:
        SubSetNTupleWriter();
        virtual ~SubSetNTupleWriter();

    private:
        virtual bool AnalyzeEvent() override;
        CombinatoricService* m_service;
    };
    /// The SubSet Analysis does the same job for writing the histograms

    class SubSetAnalysis : public Analysis {
    public:
        SubSetAnalysis();
        virtual ~SubSetAnalysis();

    private:
        virtual bool AnalyzeEvent() override;
        CombinatoricService* m_service;
    };

    class SubSetParticleReader : public IParticleCollection {
    public:
        virtual std::string name() const;
        virtual bool init(TTree* t);

        virtual IParticleVarReader* RetrieveVariable(const std::string& Var);

        virtual size_t Size();
        virtual size_t ReaderIndex() const;
        virtual size_t ReaderIndex(size_t P);
        virtual unsigned int GetCollectionHash() const;
        virtual const TLorentzVector& P4() const;
        virtual TLorentzVector GetP4(size_t E);

        IParticleReader* underyling_particle() const;
        size_t remap_entry(size_t i);

        static SubSetParticleReader* GetReader(const std::string& alias_name, IParticleReader* collection = nullptr);

    private:
        SubSetParticleReader(IParticleReader* particle, const std::string& alias_name);
        SubSetParticleReader(const SubSetParticleReader&) = delete;
        void operator=(const SubSetParticleReader&) = delete;

        std::string m_name;
        IParticleReader* m_particle;
        Weight* m_weighter;
        CombinatoricService* m_comb_service;

        std::vector<IPermutationElement*> m_possible_permutations;
        IPermutationElement* m_active_element;
        std::vector<unsigned int> m_active_permutation;
        Long64_t m_cached_event;
    };
    class SubSetParticleVariableReader : public IParticleVariable {
    public:
        virtual std::string name() const;
        virtual double readEntry(size_t I) const;
        virtual bool init(TTree* t);
        static IParticleVarReader* GetReader(SubSetParticleReader*, const std::string& var);

    private:
        SubSetParticleVariableReader(SubSetParticleReader* particle, IParticleVarReader* variable);
        SubSetParticleVariableReader(const SubSetParticleVariableReader&) = delete;
        void operator=(const SubSetParticleVariableReader&) = delete;

        SubSetParticleReader* m_subset_part;
        IParticleVarReader* m_variable;
        bool m_Registered;
    };
}  // namespace XAMPP
#endif
