#ifndef XAMPPplotting_MetaDataTreeReader_H
#define XAMPPplotting_MetaDataTreeReader_H

#include <TFile.h>
#include <TTree.h>
#include <iostream>
#include <memory>
#include <set>
#include <string>
#include <vector>

namespace XAMPP {
    //###########################################################################################
    //   The MetaData store is the basic class to store all meta information in one place       #
    //   For each mcChannel, SUSYFinalState and runNumber combination a new Store is created,   #
    //   if the file is Monte Carlo. To normalize to the combination of mc16a and mc16c samples #
    //   each DSID, SUSYFinalState has a summary meta-data store with runNumber -1. This store  #
    //   is used by default by the NormalizationDataBase                                        #
    //###########################################################################################
    class MetaDataStore {
    public:
        MetaDataStore(unsigned int DSID, unsigned int SUSYFinalState, unsigned int runNumber = -1);
        MetaDataStore(unsigned int runNumber);
        // Setter functions
        void AddTotalEvents(Long64_t TotEvents);
        void AddProcessedEvents(Long64_t ProcessedEvents);
        void AddSumW(double SumW);
        void AddSumW2(double SumW2);
        void SetxSectionInfo(double xSection, double kFactor, double Efficiency, const std::string& smp_name = "");
        void SetRelUncertainty(double UnCert);

        void ReserveSpaceTotalLumi(unsigned int N);
        void ReserveSpaceProcessedLumi(unsigned int N);

        void InsertTotalLumiBlock(unsigned int BCID);
        void InsertProcessedLumiBlock(unsigned int BCID);
        void SetPileUpLuminosity(double prwLumi);

        void Lock();
        void PinCrossSection();
        // Getter functions
        unsigned int runNumber() const;
        unsigned int DSID() const;
        unsigned int ProcID() const;
        double SumW() const;
        double SumW2() const;
        Long64_t TotalEvents() const;
        Long64_t ProcessedEvents() const;

        double xSection() const;
        double kFactor() const;
        double FilterEfficiency() const;
        double relUncertainty() const;
        double prwLuminosity() const;

        const std::vector<unsigned int>& TotalLumiBlocks() const;
        const std::vector<unsigned int>& ProcessedLumiBlocks() const;
        virtual ~MetaDataStore();
        /// Physical name of the sample
        std::string sample_name() const;

    private:
        MetaDataStore();
        MetaDataStore(const MetaDataStore&) = delete;
        void operator=(const MetaDataStore&) = delete;

        unsigned int m_DSID;
        unsigned int m_ProcID;
        unsigned int m_runNumber;
        double m_xSection;
        double m_kFactor;
        double m_Efficiency;
        double m_SumW;
        double m_SumW2;
        double m_relUncert;
        Long64_t m_TotalEvents;
        Long64_t m_ProcessedEvents;
        std::vector<unsigned int> m_TotalLumiBlocks;
        std::vector<unsigned int> m_ProcessedLumiBlocks;
        bool m_Locked;
        bool m_xSectLocked;
        double m_prwLumi;
        std::string m_smp_name;
    };

    class MonteCarloStoreHandler {
    public:
        MonteCarloStoreHandler(unsigned int DSID, unsigned int runNumber);
        std::shared_ptr<MetaDataStore> getSubProcess(unsigned int processID) const;
        std::shared_ptr<MetaDataStore> insertSubProcess(unsigned int processID);
        void Lock();

        unsigned int DSID() const;
        unsigned int runNumber() const;
        std::vector<unsigned int> getListOfProcesses() const;

        double SumW(unsigned int procID) const;
        double SumW2(unsigned int procID) const;
        double prwLuminosity(unsigned int procID) const;
        Long64_t TotalEvents(unsigned int procID) const;
        Long64_t ProcessedEvents(unsigned int procID) const;

        double xSection(unsigned int procID) const;
        double kFactor(unsigned int procID) const;
        double FilterEfficiency(unsigned int procID) const;
        double relUncertainty(unsigned int procID) const;
        std::string sample_name() const;

    private:
        MonteCarloStoreHandler(const MonteCarloStoreHandler&) = delete;
        void operator=(const MonteCarloStoreHandler&) = delete;
        std::vector<std::shared_ptr<MetaDataStore>> m_processes;
        mutable std::shared_ptr<MetaDataStore> m_current;
        unsigned int m_DSID;
        unsigned int m_runNumber;
        bool m_locked;
    };
    class MonteCarloPeriodHandler {
    public:
        MonteCarloPeriodHandler(unsigned int DSID);

        unsigned int DSID() const;
        std::vector<unsigned int> getMCcampaigns() const;
        std::shared_ptr<MonteCarloStoreHandler> getHandler(unsigned int run = -1) const;
        std::shared_ptr<MonteCarloStoreHandler> insertHandler(unsigned int run);

        double prwTotalLuminosity(unsigned int procID) const;
        std::shared_ptr<MetaDataStore> getStore(unsigned int procID, unsigned int run = -1) const;
        std::shared_ptr<MetaDataStore> insertStore(unsigned int procID, unsigned int run);

        void AddTotalEvents(Long64_t TotEvents, unsigned procID, unsigned int run = -1);
        void AddProcessedEvents(Long64_t ProcessedEvents, unsigned procID, unsigned int run = -1);
        void AddSumW(double SumW, unsigned procID, unsigned int run = -1);
        void AddSumW2(double SumW2, unsigned procID, unsigned int run = -1);

        void SetPileUpLuminosity(double prwLumi, unsigned int procID, unsigned int run = -1);

        void SetxSectionInfo(double xSection, double kFactor, double Efficiency, unsigned int procID, const std::string& smp_name = "");
        void SetRelUncertainty(double UnCert, unsigned int procID);
        void Lock();
        void PinCrossSection();

    private:
        MonteCarloPeriodHandler(const MonteCarloPeriodHandler&) = delete;
        void operator=(const MonteCarloPeriodHandler&) = delete;
        std::vector<std::shared_ptr<MonteCarloStoreHandler>> m_periods;
        std::shared_ptr<MonteCarloStoreHandler> m_summary_period;
        bool m_locked;
    };

    class NormalizationDataBase {
    public:
        /// Singelton constructor
        static NormalizationDataBase* getDataBase();
        /// Destructor of the database and make a new one
        static void resetDataBase();

        // Normalizes the sample to 1 fb
        const double& getNormalization(unsigned int DSID, unsigned int ProcID = 0);
        const double& getNormTimesXsec(unsigned int DSID, unsigned int ProcID = 0);

        /// Initialize the databse from a given list of file-paths
        /// Recommended way since this ensures that the files are read in
        /// uniquely
        virtual bool init(const std::vector<std::string>& In);

        /// Initialize the database from a list of TFile pointers
        /// It might cause a heavy load on the file system in cases where
        /// the number of files is stupidly large... Please only consider this
        /// if you know what you are doing
        virtual bool init(const std::vector<std::shared_ptr<TFile>>& In);
        /// Returns whether the input files are data or not
        virtual bool isData() const;

        /// Set the global luminosity. By default all samples are normalized to 1fb
        void SetLumi(double Lumi);
        /// Prompt the full meta data
        void PromptMetaDataTree();

        /// Load the cross sections from the cross-section databases
        ///    xSecFile_Bkg: The one and only file provided by the PMG crossection tool
        ///    xSecDir_Sig: A director containing a bunch of files in the good old SUSY format
        void LoadCrossSections(const std::string& xSecFile_Bkg, const std::string& xSecDir_Sig);
        /// Information about the processed data runs
        std::vector<unsigned int> GetRunNumbers() const;
        /// Luminosity block numbers in that particular run proceseed in the  DAOD
        const std::vector<unsigned int>& GetProcessedLumiBlocks(unsigned int runNumber);
        /// Luminosity blocks in general..
        const std::vector<unsigned int>& GetTotalLumiBlocks(unsigned int runNumber);

        /// Number of total recorded events
        Long64_t GetTotalEvents(unsigned int runNumber);
        /// Number of events in the DAOD file
        Long64_t GetProcessedEvents(unsigned int runNumber);

        /// Get the list of DSIDs in this job
        std::vector<unsigned int> GetListOfMCSamples() const;
        unsigned int GetNumMCsamples() const;

        /// SUSY samples have a process ID -> Get the number
        /// of all process ID's. This function includes the LHE weights
        /// as well for background
        unsigned int GetNumberOfProcesses(unsigned int DSID);

        /// Get the number of process IDs in the sample
        /// PiD > 1000 --> LHE variations
        std::vector<unsigned int> GetListOfProcesses(unsigned int DSID);

        /// Get the underlying helper class to manage the meta data of one MC sample
        ///     -> Process IDs
        ///     -> Monte Carlo campaigns
        std::shared_ptr<MonteCarloPeriodHandler> getMCperiodHandler(unsigned int DSID);
        /// Number of total events in the AOD for a given process ID.
        Long64_t GetTotalEvents(unsigned int DSID, unsigned int ProcID);
        /// Number of total events in the DAOD for a given processID
        Long64_t GetProcessedEvents(unsigned int DSID, unsigned int ProcID);

        /// Sum of event weights for a given DSID and process ID. That's the
        /// quantity used for proper normalization of the sample
        double GetSumW(unsigned int DSID, unsigned int ProcID = 0);

        /// sum of event weights squared for  a given DSID and process ID
        double GetSumW2(unsigned int DSID, unsigned int ProcID = 0);

        /// Cross-section read in from the PMGTool or the SUSYDatabase (if procID !=0)
        double GetxSection(unsigned int DSID, unsigned int ProcID = 0);
        double GetFilterEfficiency(unsigned int DSID, unsigned int ProcID = 0);
        double GetkFactor(unsigned int DSID, unsigned int ProcID = 0);
        /// Returns cross-section times filter efficiency times kFactor
        double GetxSectTimes(unsigned int DSID, unsigned int ProcID = 0);
        double GetRelUncertainty(unsigned int DSID, unsigned int ProcID = 0);

        /// Sample name stored in the PMGTool
        std::string GetSampleName(unsigned int DSID);
        // Some setter functions of the xSections
        void SetxSection(double xSec, double kFac, double filtEff, double relUnc, unsigned int DSID, unsigned int ProcID = 0,
                         const std::string& smp_name = "");
        virtual ~NormalizationDataBase();

    protected:
        static NormalizationDataBase* m_Inst;
        NormalizationDataBase();
        NormalizationDataBase(const NormalizationDataBase&) = delete;
        void operator=(const NormalizationDataBase&) = delete;

        enum DSIDStatus { Present, Updated, Failed };
        bool ReadInFile(const std::shared_ptr<TFile>& F);
        bool ReadTree(TTree* t);
        void initialized();
        bool isInitialized() const;

    private:
        void PromptMCMetaDataTree();
        void PromptRunMetaDataTree();
        bool ReadMCTree(TTree* t);
        bool ReadDataTree(TTree* t);

        void LockStores();

        DSIDStatus GetDSIDStatus(unsigned int DSID);
        DSIDStatus GetDSIDStatus(unsigned int DSID, unsigned int ProcID);
        DSIDStatus GetRunStatus(unsigned int runNumber);

        std::vector<std::shared_ptr<MonteCarloPeriodHandler>> m_mcDB;
        std::shared_ptr<MonteCarloPeriodHandler> m_ActMC;

        typedef std::map<unsigned int, std::shared_ptr<MetaDataStore>> MetaID;
        MetaID m_dataDB;
        std::shared_ptr<MetaDataStore> m_ActMeta;
        double m_Lumi;
        double m_weight;
        double m_Norm;
        bool m_xSecLoaded;
        bool m_init;
    };
}  // namespace XAMPP
#endif
