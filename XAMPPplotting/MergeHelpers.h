#ifndef XAMPPPLOTTING_MERGEHELPERS_H_
#define XAMPPPLOTTING_MERGEHELPERS_H_
#include <TFile.h>
#include <TTree.h>
#include <XAMPPplotting/PlottingUtils.h>
#include <memory>
#include <vector>
namespace XAMPP {
    class CorruptedFileChecker {
    public:
        static bool isFileOk(const std::string& path);
        static bool isFileOk(std::shared_ptr<TFile> f);

        static std::vector<unsigned int> getLHEWeights(std::shared_ptr<TFile> f);
        static std::vector<unsigned int> getLHEWeights(const std::string& f);

        static std::vector<unsigned int> getDSIDs(const std::string& f);
        static std::vector<unsigned int> getDSIDs(const std::shared_ptr<TFile> f);

    private:
        static bool checkFriendShip(TTree* tree);
    };

    class NtupleFileInfo {
    public:
        NtupleFileInfo(const std::string& in_path);
        bool is_good() const;
        unsigned int num_trees() const;
        unsigned int num_lhe() const;
        std::string path() const;
        Long64_t file_size() const;
        bool share_same_trees(const NtupleFileInfo& other) const;
        bool can_be_merged(const NtupleFileInfo& other) const;

    private:
        std::string m_path;
        std::vector<std::string> m_trees;
        std::vector<unsigned int> m_lhe;
        Long64_t m_size;
    };
    class MergeHelper {
    public:
        MergeHelper();
        bool add_file(const std::string& f);
        void add_file(const std::vector<std::string>& files);

        std::vector<std::string> get_good_files();
        std::vector<Long64_t> get_sizes();
        bool split_file(const std::string& f);

    private:
        void skim_bad_files();
        std::vector<NtupleFileInfo> m_files;
        std::vector<NtupleFileInfo>::const_iterator m_last;
    };

}  // namespace XAMPP
#endif
