#ifndef XAMPPPLOTTING_CUTCLASS_H
#define XAMPPPLOTTING_CUTCLASS_H

#include <AsgTools/AnaToolHandle.h>
#include <memory>
#include <set>
#include <string>
#include <vector>

class IGoodRunsListSelectionTool;

namespace XAMPP {
    class ITreeVarReader;
    class ITreeVectorReader;
    class ITreeMatrixReader;
    class Weight;
    class EventService;
    class Condition {
    public:
        enum Mode {
            inDef,
            AND,
            OR,
            NOTOR,  // ! (A || B || C || D)
            NOTAND  // !( A && B && C && D)
        };
        Condition();
        //##########################################################
        // Pass methods for ordinary (current) ITreeVarReader      #
        //              for the ITreeVectorVarReader               #
        //              for the ITreeMatrixVarReader               #
        //##########################################################
        virtual bool Pass() const;
        virtual bool PassScalar() const = 0;
        virtual bool PassVector(size_t entry) const = 0;
        virtual bool PassMatrix(size_t column, size_t row) const = 0;

        bool Init(ITreeVarReader* Reader, double Value);
        bool InitVector(ITreeVectorReader* Vector, double Value);
        bool InitMatrix(ITreeMatrixReader* Matrix, double Value);

        static std::shared_ptr<Condition> Combine(std::shared_ptr<Condition> first, std::shared_ptr<Condition> second, Mode mode = AND);

        virtual std::string name() const = 0;
        virtual ~Condition();

    protected:
        std::string reader_name() const;
        ITreeVarReader* m_Reader;
        ITreeVectorReader* m_VectorReader;
        ITreeMatrixReader* m_MatrixReader;

    private:
        bool m_hasScalarReader;
        bool m_hasVectorReader;
        bool m_hasMatrixReader;

    protected:
        double m_CutValue;

    private:
        bool m_init;
    };
    class CompositeCondition : public Condition {
    public:
        CompositeCondition(std::shared_ptr<Condition> Cut1, std::shared_ptr<Condition> Cut2);
        virtual ~CompositeCondition();

    protected:
        std::shared_ptr<Condition> m_cut_1;
        std::shared_ptr<Condition> m_cut_2;
    };
    /// Composition of two cuts via AND or OR relation.
    class CompositeConditionAND : public CompositeCondition {
    public:
        CompositeConditionAND(std::shared_ptr<Condition> Cut1, std::shared_ptr<Condition> Cut2);

        virtual bool Pass() const;
        virtual bool PassScalar() const;
        virtual bool PassVector(size_t entry) const;
        virtual bool PassMatrix(size_t column, size_t row) const;

        virtual std::string name() const;
        virtual ~CompositeConditionAND() {}
    };
    class CompositeConditionOR : public CompositeCondition {
    public:
        CompositeConditionOR(std::shared_ptr<Condition> Cut1, std::shared_ptr<Condition> Cut2);

        virtual bool Pass() const;
        virtual bool PassScalar() const;
        virtual bool PassVector(size_t entry) const;
        virtual bool PassMatrix(size_t column, size_t row) const;

        virtual std::string name() const;
        virtual ~CompositeConditionOR() {}
    };
    class ConditionNOT : public CompositeCondition {
    public:
        ConditionNOT(std::shared_ptr<Condition> Cut);

        virtual bool Pass() const;
        virtual bool PassScalar() const;
        virtual bool PassVector(size_t entry) const;
        virtual bool PassMatrix(size_t column, size_t row) const;

        virtual std::string name() const;
    };
    //###########################################
    //         Logical operator conditions
    //
    //##########################################
    class GreaterCondition : public Condition {
    public:
        virtual bool PassScalar() const;
        virtual bool PassVector(size_t entry) const;
        virtual bool PassMatrix(size_t column, size_t row) const;
        virtual std::string name() const;
    };
    class GreaterEqualCondition : public Condition {
    public:
        virtual bool PassScalar() const;
        virtual bool PassVector(size_t entry) const;
        virtual bool PassMatrix(size_t column, size_t row) const;

        virtual std::string name() const;
    };
    class EqualCondition : public Condition {
    public:
        virtual bool PassScalar() const;
        virtual bool PassVector(size_t entry) const;
        virtual bool PassMatrix(size_t column, size_t row) const;
        virtual std::string name() const;
    };
    class LessCondition : public Condition {
    public:
        virtual bool PassScalar() const;
        virtual bool PassVector(size_t entry) const;
        virtual bool PassMatrix(size_t column, size_t row) const;
        virtual std::string name() const;
    };
    class LessEqualCondition : public Condition {
    public:
        virtual bool PassScalar() const;
        virtual bool PassVector(size_t entry) const;
        virtual bool PassMatrix(size_t column, size_t row) const;
        virtual std::string name() const;
    };
    class NotEqualCondition : public Condition {
    public:
        virtual bool PassScalar() const;
        virtual bool PassVector(size_t entry) const;
        virtual bool PassMatrix(size_t column, size_t row) const;
        virtual std::string name() const;
    };
    class AbsGreaterCondition : public Condition {
    public:
        virtual bool PassScalar() const;
        virtual bool PassVector(size_t entry) const;
        virtual bool PassMatrix(size_t column, size_t row) const;
        virtual std::string name() const;
    };
    class AbsGreaterEqualCondition : public Condition {
    public:
        virtual bool PassScalar() const;
        virtual bool PassVector(size_t entry) const;
        virtual bool PassMatrix(size_t column, size_t row) const;
        virtual std::string name() const;
    };
    class AbsLessCondition : public Condition {
    public:
        virtual bool PassScalar() const;
        virtual bool PassVector(size_t entry) const;
        virtual bool PassMatrix(size_t column, size_t row) const;

        virtual std::string name() const;
    };
    class AbsLessEqualCondition : public Condition {
    public:
        virtual bool PassScalar() const;
        virtual bool PassVector(size_t entry) const;
        virtual bool PassMatrix(size_t column, size_t row) const;

        virtual std::string name() const;
    };
    class AbsEqualCondition : public Condition {
    public:
        virtual bool PassScalar() const;
        virtual bool PassVector(size_t entry) const;
        virtual bool PassMatrix(size_t column, size_t row) const;

        virtual std::string name() const;
    };

    class AbsNotEqualCondition : public Condition {
    public:
        virtual bool PassScalar() const;
        virtual bool PassVector(size_t entry) const;
        virtual bool PassMatrix(size_t column, size_t row) const;

        virtual std::string name() const;
    };

    class GRLCondition : public Condition {
    public:
        GRLCondition(const std::vector<std::string>& GRLs);
        virtual bool Pass() const;
        virtual bool PassScalar() const { return false; }
        virtual bool PassVector(size_t) const { return false; }
        virtual bool PassMatrix(size_t, size_t) const { return false; }
        virtual std::string name() const;
        virtual ~GRLCondition();
        // get rid of compiler warnings
        using Condition::Pass;

    private:
        // the GRL tool
        bool m_init;
        asg::AnaToolHandle<IGoodRunsListSelectionTool> m_grl;
    };

    class UniqueEventCondition : public Condition {
    public:
        /// Standard constructor. Optionally a path to a txt file
        /// can be piped having the list of duplicated events stored.
        /// Such a file can be made with CheckDuplicatedEvents.
        /// Each event occuring twice in data is listed in a separate line
        /// having the format: <run> <event_number>
        UniqueEventCondition(const std::string& duplicated_list);
        bool load_duplicated_list(const std::string& duplicated_list);
        virtual bool Pass() const;
        virtual bool PassScalar() const { return false; }
        virtual bool PassVector(size_t) const { return false; }
        virtual bool PassMatrix(size_t, size_t) const { return false; }
        virtual std::string name() const;
        using Condition::Pass;

    private:
        Weight* m_weighter;

        class EventSeen {
        public:
            EventSeen(unsigned int run, unsigned long long event);

            // Returns the run and event number
            unsigned int run_number() const;
            unsigned long long event_number() const;

            // Method asking whether the event is duplicated or not
            // Only at the very first call false is returned.
            // Otherwise true
            bool is_duplicate();

        private:
            unsigned int m_run;
            unsigned long long m_event;
            bool m_seen;
        };
        EventSeen* find_event(const unsigned int run, const unsigned long long event) const;

        mutable std::set<std::pair<unsigned int, unsigned long long>> m_known_ev;
        std::vector<std::shared_ptr<EventSeen>> m_ev_with_duplication;

        /// Helper variables to check whether the event has changed size the last call
        mutable unsigned int m_ev_in_tree;
        mutable bool m_ev_decision;
    };

}  // namespace XAMPP
#endif  // CUTCLASS_H
