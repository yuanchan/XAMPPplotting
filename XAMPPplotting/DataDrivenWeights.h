#ifndef XAMPPPPlotting_DataDrivenWeights_H
#define XAMPPPPlotting_DataDrivenWeights_H
#include <TF1.h>
#include <TH1.h>
#include <XAMPPplotting/CombinatoricSelection.h>
#include <XAMPPplotting/IfDefHelpers.h>
#include <XAMPPplotting/Weight.h>
#include <fstream>
#include <iostream>
#include <map>
#include <memory>
#include <set>
#include <string>
#include <vector>
namespace XAMPP {
    class PseudoScalarVarReader;
    class FakeFactor;
    class FakeFactorBundle;
    class CombinatoricParticleGrouping;

    class IExternalLookUpTable {
    public:
        virtual double read(const double x) const = 0;
        virtual double read(const double x, const double y) const = 0;
        virtual double read(const double x, const double y, const double z) const = 0;
        // Check if a given value is overflow useful for histograms.
        virtual bool isOverFlow(const double x) const = 0;
        virtual bool isOverFlow(const double x, const double y) const = 0;
        virtual bool isOverFlow(const double x, const double y, const double z) const = 0;

        virtual ~IExternalLookUpTable() = default;

        virtual std::shared_ptr<IExternalLookUpTable> clone() const = 0;
        virtual bool applyUncertainty(double w) = 0;
    };
    class TH1LookUp : public IExternalLookUpTable {
    public:
        TH1LookUp();
        TH1LookUp(const TH1LookUp& other);
        virtual ~TH1LookUp() = default;

        bool LoadHistogramFromFile(std::stringstream& sstr);
        bool LoadHistogramFromFile(const std::string& file, const std::string& histo_path);

        bool SetHisto(TH1* H);

        virtual double read(const double x) const;
        virtual double read(const double x, const double y) const;
        virtual double read(const double x, const double y, const double z) const;
        // Check if a given value is overflow useful for histograms.
        virtual bool isOverFlow(const double x) const;
        virtual bool isOverFlow(const double x, const double y) const;
        virtual bool isOverFlow(const double x, const double y, const double z) const;

        virtual std::shared_ptr<IExternalLookUpTable> clone() const;
        virtual bool applyUncertainty(double w);
        unsigned int dimension() const;
        TH1* histo() const;
        TAxis* axis(unsigned int d) const;

    private:
        bool isOverFlow(TAxis* axis, const int Bin) const;
        std::unique_ptr<TH1> m_Histo;
    };
    class TF1LookUp : public IExternalLookUpTable {
    public:
        TF1LookUp();
        TF1LookUp(const TF1LookUp& other);
        virtual ~TF1LookUp() = default;
        virtual double read(const double x) const;
        virtual double read(const double x, const double y) const;
        virtual double read(const double x, const double y, const double z) const;
        // Check if a given value is overflow useful for histograms.
        virtual bool isOverFlow(const double x) const;
        virtual bool isOverFlow(const double x, const double y) const;
        virtual bool isOverFlow(const double x, const double y, const double z) const;

        virtual std::shared_ptr<IExternalLookUpTable> clone() const;
        virtual bool applyUncertainty(double w);

    private:
        std::unique_ptr<TF1> m_Func;
    };

    /// Generic interface class to evaluate weights given from external histograms.
    /// Like fake-factors for particle or event-variable based reweighting
    class IExternalWeight {
    public:
        /// Calculate the fake-factor weight finally
        virtual double Evaluate(size_t entry = 0) = 0;
        /// Initialize the access to the TTree as usual
        virtual bool init(TTree* t) = 0;

        /// Only apply the external weight if some condition is satisfied
        /// The method is optionally
        void setCut(std::shared_ptr<Condition> Cut);
        /// Connect the Weighting class with the external data. The LookUp can
        /// either be a TH1 or TF1 object... Other implementations also possible
        void setLookUp(const std::shared_ptr<IExternalLookUpTable>& table);

        IExternalLookUpTable* lookup_table() const;

        /// Load the histogram table from the file
        bool LoadHistogramFromFile(std::stringstream& sstr);
        bool LoadHistogramFromFile(const std::string& file, const std::string& histo_path);

        virtual ~IExternalWeight() = default;

        IExternalWeight();
        IExternalWeight(const IExternalWeight& other);
        /// Apply an uncertainty based on +- w*sigma
        virtual bool applyUncertainty(double w);

        bool passCut() const;

    private:
        std::shared_ptr<Condition> m_Cut;
        std::shared_ptr<IExternalLookUpTable> m_table;
    };

    ///  Fake factors are the ratio signal / loose requirement derived from MC depending on a given choice of variables.
    ///  Following the general fake-factor recipe from the matrix formula
    ///     F = f_{ij} * sf_{j} * P_{ij}
    ///  The process-fraction can be parametrized as a function of the object kinematics or as a function of
    ///  the discriminating variable. Preferred is usally the former one. In this case the FakeFactor class
    ///  serves the purpose. Thus it's usable for all 3 cases.
    class FakeFactor : public IExternalWeight, public IPermutationElement {
    public:
        FakeFactor();
        virtual ~FakeFactor() = default;

        virtual double Evaluate(size_t entry) override;

        virtual bool init(TTree* t) override;

        virtual std::shared_ptr<FakeFactor> clone() const;

        /// Set the global particle of the fake-factor
        bool SetParticle(IParticleReader* P);
        /// All variables must be associated to the particle
        /// -> SetParticle must be called at the very first

        /// Set the variable to read out the x-axis
        bool SetXVar(IParticleVarReader* x);
        /// Set the variable to read out the y-axis
        bool SetYVar(IParticleVarReader* y);
        /// Set the variable to read out the z-axis
        bool SetZVar(IParticleVarReader* z);

        /// Type of fakes considered by the analysis.
        /// I.e. HF, LF, CONV, et al.
        /// The user himself decides about the numbering scheme with one
        /// exception -1 means no assignment is done at all and corresponds to the
        /// default setting
        int fakeType() const;
        void setFakeType(int type);

        /// Similar arguments are lead into the battle for the processBin mapping
        /// the SM-background process to the fake-factor (Z+jets, W+jets, ttbar etc)
        int processBin() const;
        void setProcessBin(int bin);

        /// Access to the x-variable
        IParticleVarReader* xVariable() const;
        /// Access to the y-variable
        IParticleVarReader* yVariable() const;
        /// Access to the z-variable
        IParticleVarReader* zVariable() const;
        /// Access to the particle itself
        virtual IParticleReader* particle() const override;

        /// Did any of the particles in the container pass the selection
        bool entered_globally() const;
        /// Did the particle from the last evaluate call pass
        bool entered_component() const;

        /// Do not swap the sign on MC w.r.t. data
        void doNotFlipDataMC(bool B = true);

        /// The FF intrinsically subtracts the event to account for double counting
        void isSubtractingFakeFF(bool B = true);

        /// Sign of the fake-factor in the event.
        virtual int sign() const;
        /// How many elements from the container were selected in total
        unsigned int nPassed() const;

        /// Only consider from the i-th element in the container for the FF
        void setBeginPosition(size_t pos);
        /// Only consider until the i-th element in the container for the FF
        void setEndPosition(size_t pos);

        /// What is the desired size of the fake-factor weight (1/2)
        void setDesignPartNumber(unsigned int p);
        unsigned int num_design_parts() const;
        /// Group particles together when considering the combinatorics
        /// e.g. treat electrons and muons as light-leptons if fake-factors
        /// are going to be applied. Only particles with non-vanishing group number
        /// are considered (i.e > 0)
        void setCombinatoricGroup(unsigned int grp);
        /// Returns the combinatoric group number
        unsigned int combinatoric_group() const;
        ///
        virtual void setCombinatoricParticleGrouping(std::shared_ptr<CombinatoricParticleGrouping> grp);

        virtual unsigned int combinatoric_offset() const override;
        virtual unsigned int n_combinations() const override;
        virtual bool include_combinatorics() const override;
        virtual std::shared_ptr<DrawElementsFromSet> combinatoric_set() const override;
        virtual const std::vector<unsigned int>& get_entered() const override;

        /// Systematic subgroups provide a mechanism to propagate systematic
        /// variations only on few components of the fake-factor. If a subgroup is
        /// defined only the components matching the group name are affected by
        /// a systematic. The combined fake-factor replicates each defined variation
        /// by the number of different subgroups
        std::string systematic_subgroup() const;
        void setSystematicSubgroup(const std::string& sys_group);

    protected:
        void set_entered(bool B);

    private:
        /// Set the variable along the i-th axis
        bool SetVar(IParticleVarReader* out, IParticleVarReader*& Ref);

        IParticleReader* m_Particle;
        IParticleVarReader* m_x;
        IParticleVarReader* m_y;
        IParticleVarReader* m_z;

        int m_fakeBin;
        int m_processBin;

        bool m_freeze_sign;
        bool m_is_subtracting;

        EventService* m_service;
        CombinatoricService* m_comb_service;
        Weight* m_weighter;

        size_t m_start_in_container;
        size_t m_end_in_container;

        unsigned int m_designpart;
        bool m_includeCombinatorics;
        std::shared_ptr<DrawElementsFromSet> m_combinatorics;
        std::vector<unsigned int> m_entered_particles;
        unsigned int m_comb_grp;
        std::shared_ptr<CombinatoricParticleGrouping> m_comb_grp_helper;
        unsigned int m_comb_offset;

        Long64_t m_last_ev;
        bool m_globally_entered;
        bool m_last_entry_entered;
        unsigned int m_n_passed;
        std::string m_syst_sub_grp;
    };
    /// Helper class to manage the combinatoric group across two
    /// or more particles
    class CombinatoricParticleGrouping {
    public:
        CombinatoricParticleGrouping(unsigned int grp_num);
        unsigned int combinatoric_group() const;
        void add_member(FakeFactor* ff);

        unsigned int nPassed() const;
        void setObject(FakeFactor* ff);

        std::shared_ptr<DrawElementsFromSet> combinatoric_set() const;
        void reset_combinatorics();
        void set_combinatoric(std::shared_ptr<DrawElementsFromSet> comb);

    private:
        unsigned int m_grp_num;
        std::vector<FakeFactor*> m_members;
        std::shared_ptr<DrawElementsFromSet> m_combinatorics;
        unsigned int m_num_passed;
        FakeFactor* m_current_obj;
        Weight* m_weighter;
        Long64_t m_last_ev;
    };
    ///  The combined fake-factor combines process fraction, fake-rate and
    ///  scale-factor of each particle to a single weight
    class CombinedFakeFactor : public FakeFactor {
    public:
        CombinedFakeFactor();

        double Evaluate(size_t p) override;
        bool init(TTree* t) override;

        int sign() const override;

        virtual std::shared_ptr<FakeFactor> clone() const override;

        bool applyUncertainty(double w) override;

        /// Add a component to the combination
        /// the method returns falls either if the particle
        /// is not the one given by particle() or the ff is a
        /// nullptr.
        virtual bool add_fake_factor(const std::shared_ptr<FakeFactor>& ff);
        void setCombinatoricParticleGrouping(std::shared_ptr<CombinatoricParticleGrouping> grp) override;

    protected:
        unsigned int global_pos(unsigned int fake_type, unsigned int process_bin) const;
        std::vector<std::shared_ptr<FakeFactor>> m_ff;
    };

    class MixedCombFakeFactor : public CombinedFakeFactor {
    public:
        MixedCombFakeFactor();

        std::shared_ptr<FakeFactor> clone() const override;
        bool add_fake_factor(const std::shared_ptr<FakeFactor>& ff) override;
    };

    ///################################################
    /// The fake-factor bundle basically defines the set of fake-factors
    /// to be applied foreach systematic...
    ///################################################

    class FakeFactorBundle {
    public:
        FakeFactorBundle(const std::string& WeightName, const std::string& Syst);

        std::string name() const;
        void Add(std::shared_ptr<FakeFactor> FF);
        double Evaluate();
        bool init(TTree* T);
        ITreeVarReader* GetReader() const;
        size_t nComponents() const;
        bool applyUncertainty(double w);
        void clone_from(std::shared_ptr<FakeFactorBundle> other);

        void setDataLumi(double lumi);
        /// Include the combinatoric selection of the fake-factor
        /// Meaning that the event is processed several times just
        /// picking the k out of n elements from the list
        void includeCombinatorics(bool B = true);

        void setCut(std::shared_ptr<Condition> cut);

        void setSystematicSubgroup(const std::string& grp);

        void allow_particle_mixing();

    private:
        bool passCut() const;

        std::string m_Syst;
        std::vector<std::shared_ptr<FakeFactor>> m_FF;
        PseudoScalarVarReader* m_Pseudo;
        Weight* m_weight;
        CombinatoricService* m_comb_service;
        /// Cache the maxima of fake-type and process bin in a local variable
        bool m_types_cached;

        double m_lumi;
        bool m_DoCombinatorics;

        std::shared_ptr<Condition> m_cut;
        Long64_t m_last_ev;
        /// Switch whether the Combined or MixedCombined fake factor is used
        bool m_allow_mixed_particles;
    };
    /// The fake-factor weight basically acts like a generic scale-factor read-out. Histograms are read from the file and their
    /// content is extracted based on the particle kinematics. The histogram can have any dimension from 1-3.
    /// Alternatively also the weight application according to TF1 is at least prepared
    class FakeFactorWeight : public IWeightElement {
    public:
        std::string name() const override;
        bool init(TTree* t) override;
        double read() override;

        std::vector<std::string> FindWeightVariations(TTree*) override;
        IWeightElement::SystStatus ApplySystematic(const std::string& variation) override;

        void ResetSystematic() override;
        std::vector<std::string> GetWeightVariations() override;
        ITreeVarReader* GetTreeVarReader() override;
        ITreeVarReader* GetSystematicReader(const std::string& Syst) override;

        virtual ~FakeFactorWeight() = default;

        bool ReadFakeFactorConfig(std::ifstream& inf, std::vector<ITreeVarReader*>& Readers);
        static FakeFactorWeight* GetWeighter(const std::string& Element = "FakeFactorWeightElement");

        bool AddFakeFactor(std::shared_ptr<FakeFactorBundle> bundle);
        bool AddFakeFactor(std::shared_ptr<FakeFactor> fakes, const std::string& syst = "");

    private:
        // Initializing the FF with systematics
        bool AppendFakeFactor(std::ifstream& inf, std::vector<ITreeVarReader*>& readers);
        void CreateStatErrors();

        /// Create a new bundle storing all fake-factors corresponding
        /// to a systematic
        void CreateSystematic(const std::string& Syst);

        /// Load the systematic into cache. False is returned if it does
        /// not exist
        bool LoadSystematic(const std::string& Syst = "");

        FakeFactorWeight(const std::string& Element);
        FakeFactorWeight(const FakeFactorWeight&) = delete;
        void operator=(const FakeFactorWeight&) = delete;

        std::string m_Name;
        std::vector<std::shared_ptr<FakeFactorBundle>> m_FF;
        /// Current systematic
        std::shared_ptr<FakeFactorBundle> m_FFSyst;

        bool m_Init;
        bool m_Registered;

        /// Settings which can be configured to the particular fake-factors themselves
        /// If an option is set then it overwrites the one  of each single fake-factor

        /// Luminosity of the used data. If this variable > 0 then the data
        /// is aditionally scaled by 1 / lumi to cancel the scaling at the
        /// final plotting stage.
        double m_lumi;
        std::shared_ptr<Condition> m_cut;
        bool m_includeCombinatorics;
        std::vector<std::string> m_syst_sub_grp;
        bool m_allow_particle_mixing;
    };

    /// Apply a flat scaling to each event. This class can only
    /// be used in combination with the Conditional weight
    class FixedExternalWeight : public IWeightElement {
    public:
        virtual std::string name() const;
        virtual bool init(TTree* t);
        virtual double read();
        virtual ITreeVarReader* GetTreeVarReader();
        virtual ITreeVarReader* GetSystematicReader(const std::string& Syst);

        virtual std::vector<std::string> FindWeightVariations(TTree* Tree);
        virtual IWeightElement::SystStatus ApplySystematic(const std::string& variation);
        virtual void ResetSystematic();
        virtual std::vector<std::string> GetWeightVariations();
        virtual ~FixedExternalWeight();
        static IWeightElement* GetWeighter(const std::string& Name, const std::string& ReaderName = "", double FixedValue = 42.);

    private:
        FixedExternalWeight(const std::string& WeightName, const std::string& ReaderName, double FixedValue);
        FixedExternalWeight(const FixedExternalWeight&) = delete;
        void operator=(const FixedExternalWeight&) = delete;

        std::string m_name;
        PseudoScalarVarReader* m_reader;
    };

    ///  Weights administrated by the conditional weight
    ///  are going to be applied if user specified conditions are satisfied
    class ConditionalWeight : public IWeightElement {
    public:
        virtual std::string name() const;
        virtual bool init(TTree* t);
        virtual double read();
        virtual ITreeVarReader* GetTreeVarReader();
        virtual ITreeVarReader* GetSystematicReader(const std::string& Syst);

        virtual std::vector<std::string> FindWeightVariations(TTree* Tree);
        virtual IWeightElement::SystStatus ApplySystematic(const std::string& variation);
        virtual void ResetSystematic();
        virtual std::vector<std::string> GetWeightVariations();
        virtual ~ConditionalWeight();

        static ConditionalWeight* GetWeighter(const std::string& Name);

        bool LoadConfiguration(std::ifstream& inf, std::vector<ITreeVarReader*>& Readers);
        bool AppendWeightElement(IWeightElement* Weight, std::shared_ptr<Condition> Cut);

        bool IsWeightApplied();

        bool ApplyWeighter(IWeightElement* Weight) const;

    protected:
        ConditionalWeight(const std::string& Name);
        ConditionalWeight(const ConditionalWeight&) = delete;
        void operator=(const ConditionalWeight&) = delete;

        struct CondWeight {
            IWeightElement* Weight;
            std::shared_ptr<Condition> Cut;
            bool Passed = false;
        };
        void UpdateCuts();
        bool GetWeightElement(std::ifstream& inf, std::vector<ITreeVarReader*>& Readers);

        bool LoadSystematic(const std::string& Variation = "");
        std::string m_Name;

        std::vector<CondWeight> m_Weighters;
        class ConditionalVariations {
        public:
            ConditionalVariations(const std::string& Variation, ConditionalWeight& Conditional);
            void AppendConditional(const std::vector<ConditionalWeight::CondWeight>& Weighters);
            void AppendConditional(ConditionalWeight::CondWeight Cond);
            void NewEvent(bool UseWeighter = false);
            ITreeVarReader* GetTreeVarReader();
            std::string variation() const;

        protected:
            std::string m_VarName;
            ConditionalWeight& m_RefInstance;
            std::map<ITreeVarReader*, IWeightElement*> m_Weights;
            PseudoScalarVarReader* m_PseudoReader;
        };
        typedef std::shared_ptr<ConditionalWeight::ConditionalVariations> Conditional_Ptr;
        std::vector<Conditional_Ptr> m_Variations;
        std::vector<Conditional_Ptr>::iterator m_VarItr;
        std::vector<Conditional_Ptr>::iterator m_VarEndItr;

        bool m_Init;
        bool m_Registered;
        unsigned int m_eventNumber;
        XAMPP::Weight* m_weightService;
        bool m_disabled;

        std::shared_ptr<Condition> m_glob_cut;
        bool m_glob_cut_passed;
    };

}  // namespace XAMPP
#endif
