#ifndef XAMPPPlotting_SAMPLEHISTO_H
#define XAMPPPlotting_SAMPLEHISTO_H

#include <map>
#include <memory>
#include <set>
#include <vector>

#include <XAMPPplotting/Histo.h>
#include <XAMPPplotting/HistoTemplates.h>
#include <XAMPPplotting/PlottingUtils.h>

#include <TH1.h>

class TFile;

namespace XAMPP {

    class SystematicPairHistos;
    class SystematicEnvelope;
    class SampleHisto {
    public:
        // Standard constructors
        SampleHisto(const std::string& sample_name, const std::string& variable, const std::string& analysis, const std::string& region,
                    const std::vector<std::string>& files);

        SampleHisto(const std::string& sample_name, const std::string& variable, const std::string& analysis, const std::string& region,
                    const std::string& root_file);

        // For drawing signals on top of SM backgrounds
        SampleHisto(const std::string& sample_name, const SampleHisto& signal_histo, const SampleHisto& background_histo);
        SampleHisto(const std::string& sample_name, const SampleHisto* signal_histo, const SampleHisto* background_histo);
        SampleHisto(const std::string& sample_name, const std::shared_ptr<SampleHisto>& signal_histo,
                    const std::shared_ptr<SampleHisto>& background_histo);

        // Sample types
        enum SampleType { UnDefined = 0, Data, Signal, Irreducible, Reducible, Exceeds };
        typedef std::pair<std::shared_ptr<TH1>, std::shared_ptr<TH1>> variation_pair;
        // Check what type of sample histo we've
        bool isData() const;
        bool isIrreducible() const;
        bool isReducible() const;
        bool isBackground() const;
        bool isSignal() const;

        // Define the sample; get the general enum
        void setSampleType(unsigned int T);
        unsigned int getSampleType() const;
        std::string getSampleTypeName() const;

        // Check if the histogram is loaded or not
        bool isHistoLoaded() const;
        bool loadIntoMemory();
        bool isValid();

        // Get the histogram pointer
        std::shared_ptr<TH1> GetHistogram() const;

        std::vector<std::shared_ptr<TFile>> get_in_files() const;

        // final error bands
        variation_pair GetSystErrorBands() const;
        // Get each component having the down and upwards variation in as well
        // as the TGraphAsymmErrors with the error band
        enum SystErrorSorting { Naming = 1, LargestUp, LargestDown, SmallestUp, SmallestDown };
        std::vector<std::shared_ptr<SystematicPairHistos>> GetSystComponents(unsigned int sorting = SystErrorSorting::Naming) const;

        // What type of histogram do we have
        bool isTH1() const;
        bool isTH2() const;
        bool isTH3() const;
        bool isTH2Poly() const;

        // Some information about the dimension and the number of bins
        unsigned int GetDimension() const;
        unsigned int GetNbins() const;

        unsigned int GetNbinsX() const;
        unsigned int GetNbinsY() const;
        unsigned int GetNbinsZ() const;

        // Find the bin contents and errors as well as have a simple wrapper for the TAxis along
        // the different dimensions
        double GetBinContent(int X, int Y = -1, int Z = -1) const;
        double GetBinError(int X, int Y = -1, int Z = -1) const;
        int FindBin(double x, double y = -1, double z = -1) const;

        TAxis* GetAxis(unsigned int D) const;

        TAxis* GetXaxis() const;
        TAxis* GetYaxis() const;
        TAxis* GetZaxis() const;

        // Both methods scale the histograms with the fine distinction that
        // SetLumi is jumping out if the sample is of data-type
        void SetLumi(double lumi);
        void Scale(double scale);

        // Find out about the luminosity and the integral of the histogram
        double GetLumi() const;
        double Integral() const;
        std::pair<double, double> IntegrateWithError(unsigned int xStart = 0, int xEnd = -1, unsigned int yStart = 0, int yEnd = -1,
                                                     unsigned int zStart = 0, int zEnd = -1) const;

        // Set a specific draw style of the histogram
        std::string GetDrawStyle() const;
        void setDrawStyle(const std::string& style);
        std::string GetLegendDrawStyle() const;

        // Define where we can find the histogram
        std::string GetTitle() const;
        std::string GetName() const;
        std::string GetRegion() const;
        std::string GetVariableName() const;
        std::string GetAnalysis() const;

        // Set the title for the legend
        void SetTitle(const std::string& title);

        int GetLegendOrder() const;
        void setLegendOrder(int I);

        // Now let's come to styling
        void SetLineColor(Color_t C);
        void SetFillColor(Color_t C);
        void SetFillColorAlpha(float C);
        void SetMarkerColor(Color_t C);

        void SetFillStyle(Style_t S);
        void SetLineStyle(Style_t S);
        void SetMarkerStyle(Style_t S);

        void SetMarkerSize(Size_t S);
        void SetLineWidth(Width_t W);

        bool Add(std::shared_ptr<SampleHisto> other, double scale = 1.);
        bool Add(SampleHisto* other, double scale = 1.);
        bool Add(std::shared_ptr<TH1> Histo, double scale = 1.);
        bool Add(const TH1* Histo, double scale = 1.);

        bool Multiply(std::shared_ptr<SampleHisto> other, double scale = 1.);
        bool Multiply(SampleHisto* other, double scale = 1.);
        bool Multiply(std::shared_ptr<TH1> Histo, double scale = 1.);
        bool Multiply(const TH1* Histo, double scale = 1.);

        bool Divide(std::shared_ptr<SampleHisto> other, double scale = 1.);
        bool Divide(SampleHisto* other, double scale = 1.);
        bool Divide(std::shared_ptr<TH1> Histo, double scale = 1.);
        bool Divide(const TH1* Histo, double scale = 1.);

        std::shared_ptr<TH1> Sqrt() const;

        // Generic function to apply a transformation on a histogram.
        // Like taking the sqrt. The method creates a clone of the holded
        // histogram and applies the operation to each bin given by the
        // user defined std::function. The input arguments are the bin number
        // To access the original histogram it's recommended to put into the lambda something like
        //      [this](unsigned int i) {GetBinContent(i);}
        std::shared_ptr<TH1> ApplyTransformation(const std::function<std::pair<double, double>(unsigned int)>& trans_func) const;

        void Draw(const std::string& options = "");
        void Write();

        void exclude_systematics(const std::vector<std::string>& to_exclude);
        void set_systematics(const std::vector<std::string>& syst_to_use);
        void set_nominal(const std::string& nominal);
        void setTheoryUncertainty(double);

        double GetMaximum(double max = DBL_MAX) const;
        double GetMinimum(double min = DBL_MIN) const;

        int GetMaximumBin(double max = DBL_MAX) const;
        int GetMinimumBin(double min = DBL_MIN) const;

        std::shared_ptr<TH1> ReBin(double max_stat_error) const;
        std::shared_ptr<TH1> ReBin(const std::vector<double>& x_bins, const std::vector<double>& y_bins = std::vector<double>(),
                                   const std::vector<double>& z_bins = std::vector<double>()) const;

        std::shared_ptr<TH1> TransformToBinning(const std::shared_ptr<TH1>& h) const;
        std::shared_ptr<TH1> TransformToBinning(const TH1* h) const;

        static std::vector<double> ExtractBinningFromAxis(const TAxis* T);

        void Reset();
        bool Truncate(double threshold = 0, double to = 0);

    private:
        SampleHisto(const SampleHisto&) = delete;
        void operator=(const SampleHisto&) = delete;

        std::string GetPlotPath(const std::string& systematic, const std::string& delimiter = "_") const;
        std::shared_ptr<TH1> load_from_file(const std::shared_ptr<TFile>& f, const std::string& syst) const;
        void reset();

        std::string m_analysis;
        std::string m_region;
        std::string m_variable;
        std::string m_name;
        std::string m_label;

        std::shared_ptr<TH1> m_nominal_histo;
        unsigned int m_sample_type;
        std::vector<std::shared_ptr<TFile>> m_root_files;
        bool m_tried_to_load;

        std::vector<std::shared_ptr<SystematicPairHistos>> m_variations;
        // Histogram representing the error bands between nominal and the systematics added in quadrature
        std::shared_ptr<TH1> m_up_syst_histo;
        std::shared_ptr<TH1> m_down_syst_histo;

        int m_legend_order;
        double m_integral;
        double m_lumi;

        // Usually the systematics are recieved from the file-structure
        // handler. But one could also parse a subset of systematics to consider
        // or exclude some  systematics. Same thing applies to the nominal case
        // If the new nominal is part of the common systematics then it's evaluation
        // is not further considered but the old nominal is used instead
        std::string m_nominal;
        std::vector<std::string> m_syst;
        std::vector<std::string> m_excluded_syst;
        bool m_syst_set_external;
        bool m_nominal_set_external;
        // Flat uncertainty scaling the cross-section
        double m_theo_uncert;

        std::string m_drawStyle;

    protected:
        std::shared_ptr<TH1> load_from_files(const std::string& systematic) const;
        bool add_in_quadrature(std::shared_ptr<TH1> add, std::shared_ptr<TH1> to) const;

    public:
        variation_pair evaluate_sys(std::shared_ptr<TH1> down, std::shared_ptr<TH1> up) const;

        bool isOverFlow(int bin) const;
        bool hasAlphaNumericLabels() const;
    };

    /// Helper class to return single systematics from the sample histo
    class SystematicPairHistos {
    public:
        // Pairer for envelopes
        SystematicPairHistos(SampleHisto* ref_histo, std::shared_ptr<SystematicEnvelope> grp, std::shared_ptr<TH1> env);
        SystematicPairHistos(SampleHisto* ref_histo, const std::string& sys_name, std::shared_ptr<TH1> sym);
        SystematicPairHistos(SampleHisto* ref_histo, const std::string& sys_name, std::shared_ptr<TH1> dn, std::shared_ptr<TH1> up);

        bool is_symmetric() const;
        bool is_envelope() const;

        /// Histograms having the deltas w.r.t. nominal
        /// Each bin is strictly below or above the nominal
        std::shared_ptr<TH1> up_histo() const;
        std::shared_ptr<TH1> dn_histo() const;

        /// histograms having the variance w.r.t nominal applied.
        /// No statistical errors are included
        std::shared_ptr<TH1> final_up_histo() const;
        std::shared_ptr<TH1> final_dn_histo() const;

        std::string name() const;

        double total_up_dev();
        double total_dn_dev();

        void Scale(double s);

        bool has_valid_histo() const;

    private:
        void cache_total_deviation();
        std::string m_name;
        std::shared_ptr<TH1> m_dn;
        std::shared_ptr<TH1> m_up;

        const std::shared_ptr<TH1> m_nominal;
        std::shared_ptr<TH1> m_nom_up;
        std::shared_ptr<TH1> m_nom_dn;

        bool m_symmetric;
        bool m_envelope;
        /// Variables to cache the total deviation from
        /// nominal histogram
        double m_tot_up_dev;
        double m_tot_dn_dev;
        bool m_dev_cached;
    };

}  // namespace XAMPP

#endif
