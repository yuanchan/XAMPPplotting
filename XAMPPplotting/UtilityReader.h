#ifndef XAMPPPLOTTING_UTILITYREADER_H
#define XAMPPPLOTTING_UTILITYREADER_H

#include <XAMPPplotting/TreeVarReader.h>
#include <cmath>
#include <fstream>
#include <map>
#include <memory>
#include <set>
#include <sstream>
namespace XAMPP {
    class Weight;
    class ConditionalWeight;
    class Condition;
    class AnalysisRegion;
    class ReaderProvider;
    class LumiSlicer;
    //
    //  The PseudoScalarVarReader has no real connection to any branch. It stores
    //  any number which can then be plotted into a histogram.
    //  This allows for a generic approach to calculate event variables or pass event weights
    //  to histograms. etc..
    class PseudoScalarVarReader : public IScalarReader {
    public:
        virtual std::string name() const;
        virtual double read() const;

        virtual bool init(TTree* t);
        void SetValue(double V);
        virtual ~PseudoScalarVarReader() {}
        static PseudoScalarVarReader* GetReader(const std::string& varname);
        static PseudoScalarVarReader* GetReader(const std::string& varname, double value);

    protected:
        PseudoScalarVarReader(const std::string& varname);
        PseudoScalarVarReader(const PseudoScalarVarReader&) = delete;
        void operator=(const PseudoScalarVarReader&) = delete;

        std::string m_Name;
        bool m_Registered;
        double m_Value;
    };
    class BinnedDataTakingReader : public IScalarReader {
    public:
        static ITreeVarReader* GetReader(double lumi_slice);
        virtual bool init(TTree* t);
        virtual std::string name() const;
        virtual double read() const;
        virtual ~BinnedDataTakingReader();

    private:
        BinnedDataTakingReader(double lumi_slice);
        BinnedDataTakingReader(const BinnedDataTakingReader&) = delete;
        void operator=(const BinnedDataTakingReader&) = delete;

        double m_lumi_slice;
        ITreeVarReader* m_run_reader;
        ITreeVarReader* m_lumi_reader;
        std::unique_ptr<LumiSlicer> m_lumi_slices;
        bool m_Registered;
    };
    class CutAppliedReader : public IScalarReader {
    public:
        virtual double read() const;
        virtual bool init(TTree* t);
        virtual std::string name() const;
        void cacheDecision(bool B);
        // In general the CutAppliedReader cannot follow a singelton pattern. To prevent people
        // from doing something stupid. Only certain classes are allowed to create an instance of this
        // class
        friend AnalysisRegion;
        friend ReaderProvider;

    protected:
        CutAppliedReader(const std::vector<std::shared_ptr<Condition>>& cuts, const std::string& name);
        CutAppliedReader(const CutAppliedReader&) = delete;
        void operator=(const CutAppliedReader&) = delete;

    private:
        std::string m_name;
        std::vector<std::shared_ptr<Condition>> m_Cuts;
        bool m_Registered;
        bool m_Printed;
        bool m_cacheDecision;

        mutable bool m_cache;
        mutable Long64_t m_eventNumber;
        EventService* m_service;
    };

    /////////////////////////////
    //  Reader to check if a conditional weight
    //   has been applied in the event or not
    //   Implicitly this is just checking a cut
    /////////////////////////////
    class ConditionalWeightAppliedReader : public IScalarReader {
    public:
        virtual double read() const;
        virtual bool init(TTree* t);
        virtual std::string name() const;
        static ITreeVarReader* GetReader(const std::string& Name);
        static ITreeVarReader* GetReader(ConditionalWeight* Weight);

    protected:
        ConditionalWeightAppliedReader(ConditionalWeight* Weight);
        ConditionalWeightAppliedReader(const ConditionalWeightAppliedReader&) = delete;
        void operator=(const ConditionalWeightAppliedReader&) = delete;
        ConditionalWeight* m_Weight;
        bool m_Registered;
    };

    //  Depending on the specific selection criteria the ClassificationReader assigns a number
    //  to this set if they're passed.
    //

    class ClassificationReader : public IVectorReader {
    public:
        virtual bool init(TTree* t);
        virtual std::string name() const;

        virtual size_t entries() const;
        virtual double readEntry(size_t i) const;

        bool AddClassification(std::shared_ptr<Condition> Cond, ITreeVarReader* Obj, ITreeVarReader* Replica = nullptr);
        bool AddClassification(std::ifstream& inf);
        void SetupDone();
        static ClassificationReader* GetReader(const std::string& Name);
        virtual ~ClassificationReader();

    protected:
        ClassificationReader(const std::string& Name);
        ClassificationReader(const ClassificationReader&) = delete;
        void operator=(const ClassificationReader&) = delete;

        class Classification {
        public:
            bool isSatisfied();
            ITreeVarReader* reader() const;
            virtual size_t entries();
            virtual double readEntry(size_t e) const;
            Classification(ITreeVarReader* R, std::shared_ptr<Condition> C);
            bool passThrough() const;
            void setBegin(size_t b);
            size_t begin();
            size_t end();

        private:
            std::shared_ptr<Condition> m_Cut;
            ITreeVarReader* m_Reader;
            bool m_isSatisfied;
            Long64_t m_eventNumber;
            EventService* m_service;
            size_t m_begin;
        };
        /// Replicated classification replicates the entries  of the
        /// First one according to the size
        class ReplicatedClassification : public Classification {
        public:
            ReplicatedClassification(ITreeVarReader* R, std::shared_ptr<Condition> C, ITreeVarReader* S);
            virtual double readEntry(size_t e) const;
            virtual size_t entries();

        private:
            ITreeVarReader* m_sizeReader;
        };
        typedef std::shared_ptr<Classification> Classification_Ptr;

    private:
        std::string m_Name;
        bool m_Regist;
        bool m_Setup;

        std::vector<Classification_Ptr> m_Classification;

        std::vector<ITreeVarReader*> m_CutPartReaders;
        mutable size_t m_tot_entries;
        mutable Long64_t m_currentEv;
        EventService* m_service;
        mutable Classification_Ptr m_currentObj;
    };
    //  The SetReader gathers multiple ITreeVarReaders together and
    //  reads them out consequently. No ordering or no check of commonly
    //  used variables is applied.
    class SetReader : public IVectorReader {
    public:
        static ITreeVarReader* GetReader(const std::string& name, const std::vector<ITreeVarReader*>& Set);
        virtual double readEntry(const size_t I) const;
        virtual size_t entries() const;
        virtual bool init(TTree* t);
        virtual std::string name() const;

    private:
        SetReader(const std::string& name, const std::vector<ITreeVarReader*>& Set);
        SetReader(const SetReader&) = delete;
        void operator=(const SetReader&) = delete;
        class SetItem {
        public:
            SetItem(ITreeVarReader* R);
            double readEntry(const size_t I) const;
            bool init(TTree* t);
            size_t begin() const;
            size_t end() const;
            size_t entries() const;
            void setBegin(size_t b);

        private:
            ITreeVarReader* m_reader;
            size_t m_begin;
        };

        std::vector<std::shared_ptr<SetItem>> m_readers;
        std::string m_name;

        mutable Long64_t m_currentEv;
        mutable size_t m_n_entries;
        mutable std::shared_ptr<SetItem> m_currentObj;
        EventService* m_service;
    };
    class VectorSizeReader : public IScalarReader {
    public:
        virtual double read() const;
        virtual bool init(TTree* t);
        virtual std::string name() const;
        static ITreeVarReader* GetReader(ITreeVectorReader* Vector);

    protected:
        VectorSizeReader(ITreeVectorReader* Vector);
        VectorSizeReader(const VectorSizeReader&) = delete;
        void operator=(const VectorSizeReader&) = delete;

        ITreeVectorReader* m_Reader;
    };
    class VectorNthEntryReader : public IScalarReader {
    public:
        virtual double read() const;
        virtual size_t entries() const;
        virtual bool update();
        virtual bool init(TTree* t);
        virtual std::string name() const;
        static ITreeVarReader* GetReader(ITreeVectorReader* Vector, size_t Entry = -1);

    protected:
        VectorNthEntryReader(ITreeVectorReader* Vector, size_t Entry);
        VectorNthEntryReader(const VectorNthEntryReader&) = delete;
        void operator=(const VectorNthEntryReader&) = delete;

    private:
        ITreeVectorReader* m_Reader;
        size_t m_Entry;
    };

    class VectorRangeReader : public IVectorReader {
    public:
        virtual double readEntry(size_t I) const;
        virtual bool init(TTree* t);
        virtual std::string name() const;

        virtual size_t entries() const;

        static ITreeVectorReader* GetReader(ITreeVectorReader* Var, size_t begin, size_t end);

    protected:
        VectorRangeReader(ITreeVectorReader* Variable, size_t begin, size_t end);
        VectorRangeReader(const VectorRangeReader&) = delete;
        void operator=(const VectorRangeReader&) = delete;

    private:
        ITreeVectorReader* m_Variable;
        size_t m_begin;
        size_t m_end;
        bool m_Registered;
    };

    class MatrixNthRowReader : public IVectorReader {
    public:
        virtual double readEntry(size_t I) const;
        virtual bool init(TTree* t);
        virtual std::string name() const;
        virtual size_t entries() const;

        static ITreeVectorReader* GetReader(ITreeMatrixReader* Variable, size_t C);

    protected:
        MatrixNthRowReader(ITreeMatrixReader* Variable, size_t C);
        MatrixNthRowReader(const MatrixNthRowReader&) = delete;
        void operator=(const MatrixNthRowReader&) = delete;

        ITreeMatrixReader* m_Var;
        size_t m_ColIdx;
        bool m_Registered;
    };

    class MatrixNthColumnReader : public IVectorReader {
    public:
        virtual double read() const;
        virtual double readEntry(size_t I) const;

        virtual std::string name() const;
        virtual size_t entries() const;
        virtual size_t index() const;

        virtual bool update();
        virtual bool init(TTree* t);
        virtual void reset();

        virtual bool setIndex(size_t I);

        static ITreeVectorReader* GetReader(ITreeMatrixReader* Variable, size_t C);

    protected:
        MatrixNthColumnReader(ITreeMatrixReader* Variable, size_t C);
        MatrixNthColumnReader(const MatrixNthColumnReader&) = delete;
        void operator=(const MatrixNthColumnReader&) = delete;

        ITreeMatrixReader* m_Var;
        size_t m_ColIdx;
        size_t m_Idx;
        bool m_Registered;
    };

    class VectorRowSizeReader : public IVectorReader {
    public:
        virtual std::string name() const;
        virtual bool init(TTree* t);
        virtual double readEntry(const size_t I) const;
        virtual size_t entries() const;

        static ITreeVectorReader* GetReader(ITreeMatrixReader* Reader);

    protected:
        VectorRowSizeReader(ITreeMatrixReader* Variable);
        VectorRowSizeReader(const VectorRowSizeReader&) = delete;
        void operator=(const VectorRowSizeReader&) = delete;

    private:
        ITreeMatrixReader* m_Var;
        bool m_Registered;
    };
    class VectorColumnSizeReader : public IVectorReader {
    public:
        // The VectorColumnSizeReader determines the size of each column in the matrix
        // Let's say the matrix in the event looks like this:
        //   X X X X X X
        //   X X
        //   X X X X X X X X X X X
        // Then the return values of the class are:
        //   3 3 2 2 2 2 1 1 1 1 1

        virtual std::string name() const;
        virtual bool init(TTree* t);
        virtual double readEntry(const size_t I) const;
        // Returns the maximum row-size found in the matrix
        virtual size_t entries() const;

        static ITreeVectorReader* GetReader(ITreeMatrixReader* Reader);

    protected:
        VectorColumnSizeReader(ITreeMatrixReader* Variable);
        VectorColumnSizeReader(const VectorColumnSizeReader&) = delete;
        void operator=(const VectorColumnSizeReader&) = delete;

    private:
        ITreeMatrixReader* m_Var;
        bool m_Registered;
        EventService* m_service;
        mutable Long64_t m_EventNumber;
        mutable std::vector<size_t> m_Cache;
    };

    class MatrixBlockReader : public IMatrixReader {
    public:
        virtual bool init(TTree* t);
        virtual std::string name() const;

        virtual size_t num_rows() const;
        virtual size_t row_entries(const size_t I) const;
        virtual double readMatrixEntry(const size_t I, const size_t J) const;

        static ITreeMatrixReader* GetReader(ITreeMatrixReader* Reader, Cube C);

    private:
        inline size_t min(size_t I, size_t J) const { return I < J ? I : J; }
        MatrixBlockReader(ITreeMatrixReader* Variable, Cube C);
        MatrixBlockReader(const MatrixBlockReader&) = delete;
        void operator=(const MatrixBlockReader&) = delete;

        ITreeMatrixReader* m_Variable;
        Cube m_Cube;
        bool m_Registered;
    };

    /////////////////////////////
    /// IParticleVarReader stuff
    /////////////////////////////

    class AbsIParticleVarReader : public IParticleVariable {
    public:
        static IParticleVarReader* GetReader(IParticleVarReader* R);
        virtual double readEntry(size_t I) const;
        virtual std::string name() const;
        virtual bool init(TTree* t);

    protected:
        AbsIParticleVarReader(IParticleVarReader* R);
        IParticleVarReader* m_Var;
        bool m_Registered;
    };

    class ParticleSizeReader : public IScalarReader {
    public:
        virtual bool init(TTree* t);
        virtual double read() const;
        virtual std::string name() const;

        static ParticleSizeReader* GetReader(std::string Name);
        virtual ~ParticleSizeReader() {}

    protected:
        ParticleSizeReader(std::string Name);
        ParticleSizeReader(const ParticleSizeReader&) = delete;
        void operator=(const ParticleSizeReader&) = delete;

        IParticleReader* m_Particle;
        bool m_Registered;
    };
    class ParticleNthEntryReader : public IScalarReader {
    public:
        static ITreeVarReader* GetReader(IParticleVarReader* Reader, size_t N);
        virtual double read() const;
        virtual size_t entries() const;
        virtual bool update();
        virtual bool init(TTree* t);
        virtual std::string name() const;
        virtual ~ParticleNthEntryReader() {}

    protected:
        ParticleNthEntryReader(IParticleVarReader* Reader, size_t N);
        ParticleNthEntryReader(const ParticleNthEntryReader&) = delete;
        void operator=(const ParticleNthEntryReader&) = delete;

    private:
        IParticleVarReader* m_Var;
        size_t m_Entry;
        bool m_Registered;
    };

    class ParticleRangeReader : public IVectorReader {
    public:
        virtual double readEntry(const size_t I) const;
        virtual size_t entries() const;
        virtual bool init(TTree* t);
        virtual std::string name() const;

        static ITreeVectorReader* GetReader(IParticleVarReader* Var, size_t begin, size_t end);

    protected:
        ParticleRangeReader(IParticleVarReader* Variable, size_t begin, size_t end);
        ParticleRangeReader(const ParticleRangeReader&) = delete;
        void operator=(const ParticleRangeReader&) = delete;

    private:
        IParticleVarReader* m_Variable;
        size_t m_begin;
        size_t m_end;
        bool m_Registered;
    };

    class ParticleNthRowReader : public IVectorReader {
    public:
        virtual double readEntry(const size_t I) const;
        virtual size_t entries() const;
        virtual std::string name() const;
        virtual bool init(TTree* t);

        static ITreeVectorReader* GetReader(IParticleVectorReader* Variable, size_t P);

    protected:
        ParticleNthRowReader(IParticleVectorReader* Variable, size_t P);
        ParticleNthRowReader(const ParticleNthRowReader&) = delete;
        void operator=(const ParticleNthRowReader&) = delete;

        IParticleVectorReader* m_Var;
        size_t m_PartIdx;
    };

    // The Particle RowSize reader returns how many
    // values of a given vector-like attribute are stored
    class ParticleRowSizeReader : public IVectorReader {
    public:
        virtual std::string name() const;
        virtual bool init(TTree* t);
        virtual double readEntry(const size_t I) const;
        virtual size_t entries() const;

        static ITreeVectorReader* GetReader(IParticleVectorReader* Reader);

    protected:
        ParticleRowSizeReader(IParticleVectorReader* Variable);
        ParticleRowSizeReader(const ParticleRowSizeReader&) = delete;
        void operator=(const ParticleRowSizeReader&) = delete;

        IParticleVectorReader* m_Var;
        bool m_Registered;
    };

    class ParticleBlockReader : public IMatrixReader {
    public:
        virtual bool init(TTree* t);
        virtual std::string name() const;
        virtual size_t num_rows() const;
        virtual size_t row_entries(const size_t I) const;
        virtual double readMatrixEntry(const size_t I, const size_t J) const;

        static ITreeMatrixReader* GetReader(IParticleVectorReader* Var, Cube C);

    protected:
        ParticleBlockReader(IParticleVectorReader* Variable, Cube C);
        ParticleBlockReader(const ParticleBlockReader&) = delete;
        void operator=(const ParticleRangeReader&) = delete;

    private:
        inline size_t min(size_t I, size_t J) const { return I < J ? I : J; }
        IParticleVectorReader* m_Variable;
        Cube m_Cube;
        Ranges m_Current;
        bool m_Registered;
    };

}  // namespace XAMPP
#endif  // OBSERVABLEREADER_H
