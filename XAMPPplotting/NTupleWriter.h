#ifndef XAMPP_NTUPLE_WRITER_H
#define XAMPP_NTUPLE_WRITER_H

#include <XAMPPplotting/Analysis.h>
#include <XAMPPplotting/TreeVarWriter.h>

#include <map>

namespace XAMPP {
    class IWeightElement;
    class NonZeroWeightSystCheck {
    public:
        NonZeroWeightSystCheck(IWeightElement* element);
        bool nominal_nonZero() const;
        bool systematics_nonZero() const;
        bool has_nonZeroComponents();
        std::string name() const;

        Long64_t rejected_events() const;

    private:
        IWeightElement* m_element;
        ITreeVarReader* m_nominal;
        std::vector<ITreeVarReader*> m_Systematics;
        Long64_t m_n_rejected;
    };

    class NTupleWriter : public Analysis {
    public:
        NTupleWriter();
        virtual ~NTupleWriter() = default;
        virtual bool Process(long int nMax = -1, long int nSkip = 0) override;
        void SetBranches(const std::vector<XAMPP::ITreeVarWriter*>& B) { m_Branches = B; }
        void SetWeightNames(const std::map<std::string, std::string>& WeightNames) { m_WeightNames = WeightNames; }
        void SetSystReplaceStrings(const std::map<std::string, std::string>& SystReplaceStr) { m_ReplaceStringOfSyst = SystReplaceStr; }

    protected:
        virtual bool initBranches(const std::string& Syst) override;
        virtual bool AnalyzeEvent() override;
        virtual bool HandleWeightVariations() override;
        virtual void WriteOutput() override;
        std::string WeightBranchName(IWeightElement* element, const std::string variation = "") const;

        bool FillTree();

        std::shared_ptr<TTree> m_OutTree;
        std::vector<XAMPP::ITreeVarWriter*> m_Branches;
        std::vector<XAMPP::ITreeVarWriter*> m_wBranches;
        std::vector<std::shared_ptr<NonZeroWeightSystCheck>> m_WeightVar;

        std::map<std::string, std::string> m_WeightNames;
        std::map<std::string, std::string> m_ReplaceStringOfSyst;
        bool m_isNominal;
        Long64_t m_writtenEv;
        Long64_t m_skippedEv;
    };
}  // namespace XAMPP

#endif
