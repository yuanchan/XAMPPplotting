#ifndef XAMPPPLOTTING_EVENTSERVICE_H_
#define XAMPPPLOTTING_EVENTSERVICE_H_
#include <TFriendElement.h>
#include <TTree.h>
#include <XAMPPplotting/PlottingUtils.h>
#include <cmath>
#include <fstream>
#include <map>
#include <memory>
#include <set>
#include <sstream>
#include <string>
#include <vector>
namespace XAMPP {
    class Weight;
    class ITreeVarReader;
    class EventService;
    class Selector;

    class IKinematicSyst {
    public:
        virtual ~IKinematicSyst() = default;
        virtual std::string syst_name() const = 0;
        virtual std::string tree_name() const = 0;
        virtual bool is_nominal() const = 0;
        virtual bool apply_syst() = 0;
        virtual void reset() = 0;
    };

    // Helper class to synchronize friend trees with the main analysis trees
    // FriendTrees in XAMPPbase can be either the CommonTrees or Systematic group
    // trees. The TreeIndexer can be either initialzied with a TTree object only
    // or in addition with a parent. In the former case it's assumed that this one
    // is the main analysis tree while the others are the friends to be synchronized
    // Recall that size of the friends is equal or greater  than the size of the parent
    class TreeIndexer {
    public:
        inline Long64_t currentEvent() {
            sync_indexer();
            return m_event;
        }
        inline Long64_t entries() const { return m_evInTree; }

        inline const ULong64_t& eventNumber() const { return m_eventHash[0]; }
        inline const ULong64_t& eventHash() const { return m_eventHash[1]; }
        inline TTree* tree() const { return m_tree; }
        inline TTree* synchronization_tree() const { return m_tree_to_sync; }

        inline void sync_indexer() {
            if (!m_parent || is_syncronized()) return;
            while (m_hash_branch->GetEntry(m_event)) {
                if (is_syncronized()) return;
                ++m_event;
            }
            Warning("TreeIndexer()",
                    Form("Failed to update hash in event %llu (%llu/%llu) for tree %s with %llu entries", m_parent->currentEvent(),
                         m_parent->eventNumber(), m_parent->eventHash(), tree()->GetName(), entries()));
            if (entries() == m_event) m_event = m_parent->currentEvent();
        }
        bool init();
        bool getEntry(Long64_t entry);
        TreeIndexer(std::shared_ptr<TFile> f, const std::string& tree_name);
        TreeIndexer(TFriendElement* tree, std::shared_ptr<TreeIndexer> parent);
        ~TreeIndexer();
        static void buildCommonHash(bool B);

    private:
        inline bool is_syncronized() const { return m_parent->eventNumber() == eventNumber() && eventHash() == m_parent->eventHash(); }
        // TTree which is going to be synchronized with. That comes directly
        // from the *same* file-pointer as from the primary analysis tree.
        TTree* m_tree_to_sync;
        std::shared_ptr<TFile> m_in_file;

        TTree* m_tree;
        // Connection of the eventIndex to the branch
        TBranch* m_hash_branch;

        std::shared_ptr<TreeIndexer> m_parent;

        ULong64_t m_eventHash[2];
        // The event number to synchronize stuff
        Long64_t m_event;
        Long64_t m_evInTree;
        static bool m_buildCommonHash;
        EventService* m_service;
        bool m_learningPhase;
    };

    class EventService {
    public:
        static EventService* getService();

        ~EventService();

        inline Long64_t currentEvent() const { return m_master_indexer ? m_master_indexer->currentEvent() : 0; }
        inline Long64_t entries() const { return m_master_indexer ? m_master_indexer->entries() : 0; }
        inline size_t cacheSize() const { return m_CacheSize; }

        // Propagate to the readers whether the
        // entire event has already been loaded by the service
        // or the ITreeVarReader has to do it itself
        inline bool partialEvent() const { return !m_loadEvent; }

        // How many trees have already been loaded by the service.
        inline unsigned int n_thTree() const { return m_TreesLoaded; }
        bool getEntry(Long64_t entry);

        /// Loads the tree from the TFile and overwrites
        /// the master indexer
        bool loadTree(const std::string& tree_name);
        /// Initializes all branches in the tree to make it ready
        /// for analysis
        bool setupTree();

        bool AppendReader(ITreeVarReader* R);
        bool SetReaders(const std::vector<ITreeVarReader*>& R);
        void flushReaders();
        void loadEntireEvent();
        void setCacheSize(size_t S);

        bool registerKinematic(std::shared_ptr<IKinematicSyst> syst);

        std::shared_ptr<IKinematicSyst> getSyst(const std::string& name) const;
        inline std::shared_ptr<IKinematicSyst> getNominal() const { return m_nominal; }
        const std::vector<std::shared_ptr<IKinematicSyst>>& all_systematics() const;

        TreeIndexer* getIndexer(TBranch* br) const;

        std::string in_file_path() const;
        bool openFile(const std::string& file_path);
        bool openFile(const std::shared_ptr<TFile>& file_obj);

        /// Special functionallity to actually attach the ordinary branches from
        /// the XAMPP::Selector to the TTree. Usually the XAMPP::Selector is only
        /// used by under graduate students...
        void set_selector(Selector* selector);

        std::shared_ptr<TreeIndexer> get_master() const;

    private:
        static EventService* m_Inst;
        EventService();
        EventService(const EventService&) = delete;
        void operator=(const EventService&) = delete;

        // Member variables needed by the
        Weight* m_weighter;
        bool m_loadEvent;
        std::vector<ITreeVarReader*> m_readerToLoad;
        size_t m_CacheSize;
        unsigned int m_TreesLoaded;
        std::vector<std::shared_ptr<IKinematicSyst>> m_kinematicSyst;
        std::shared_ptr<IKinematicSyst> m_nominal;

        std::shared_ptr<TreeIndexer> m_master_indexer;
        std::vector<std::unique_ptr<TreeIndexer>> m_friend_indexer;

        std::string m_filePath;
        std::shared_ptr<TFile> m_inFile;

        Selector* m_selector;
    };
}  // namespace XAMPP
#endif
