#ifndef XAMPPPLOTTING_RECONSTRUCTEDPARTICLEREADERS_H
#define XAMPPPLOTTING_RECONSTRUCTEDPARTICLEREADERS_H

#include <XAMPPplotting/ArithmetricReaders.h>
#include <XAMPPplotting/ObservableReader.h>
#include <XAMPPplotting/TreeVarReader.h>

#include <cmath>
#include <sstream>
namespace XAMPP {
    // This class joins two different particle into one single entity

    class DiParticleReader : public IParticleCollection {
    public:
        virtual std::string name() const;
        virtual bool init(TTree* t);

        virtual IParticleVarReader* RetrieveVariable(const std::string& Var);
        IParticleVarReader* RetrievePartialVariable(const std::string& Var, bool FirstPart);
        IParticleVarReader* RetrieveComponentReader();

        virtual size_t Size();
        virtual size_t ReaderIndex() const;
        virtual size_t ReaderIndex(size_t P);
        virtual unsigned int GetCollectionHash() const;
        virtual const TLorentzVector& P4() const;
        virtual TLorentzVector GetP4(size_t E);

        bool UseFirst(size_t N_th) const;

        size_t ComponentIndex() const;
        size_t ComponentIndex(size_t N_th) const;

        static DiParticleReader* GetReader(const std::string& Name, const std::string& First = "", const std::string& Second = "");
        static void setHashRegMode(bool B);

    protected:
        DiParticleReader(const std::string& Name, const std::string& First, const std::string& Second);
        DiParticleReader(const DiParticleReader&) = delete;
        void operator=(const DiParticleReader&) = delete;
        virtual void OrderParticleStreams();

    private:
        // If a ParticleTagger is registered in the ParticleTaggerManager a mechanisms is needed to
        // retrieve all collection hashes of both components of the IParticleReader.
        static bool m_hashRegMode;

    protected:
        std::string m_Name;
        IParticleReader* m_FirstParticle;
        IParticleReader* m_SecondParticle;
        IParticleVarReader* m_FirstPt;
        IParticleVarReader* m_SecondPt;

        struct DiPartOrdering {
            DiPartOrdering(size_t I, bool B) {
                Idx = I;
                UseFirstParticle = B;
            }
            size_t Idx;
            bool UseFirstParticle;
        };
        std::vector<DiPartOrdering> m_Order;
        void FillOrdering(std::vector<DiPartOrdering>& ordering, size_t N_part, bool isFirst);

        unsigned int m_CurrentEv;
        size_t m_Idx;

        mutable unsigned int m_FirstCachedHash;
        mutable unsigned int m_SecondCachedHash;
    };
    // The unsorted version of the DiParticleReader. Particles from the two stream are just ordered by stream and no longer by Pt
    class UnSortedDiParticleReader : public DiParticleReader {
    public:
        static DiParticleReader* GetReader(const std::string& Name, const std::string& First = "", const std::string& Second = "");

    protected:
        UnSortedDiParticleReader(const std::string& Name, const std::string& First, const std::string& Second);
        UnSortedDiParticleReader(const UnSortedDiParticleReader&) = delete;
        void operator=(const UnSortedDiParticleReader&) = delete;
        virtual void OrderParticleStreams();
    };

    class DiParticleVarReader : public IParticleVariable {
    public:
        virtual double readEntry(size_t I) const;
        virtual std::string name() const;
        virtual bool init(TTree* t);

        static IParticleVarReader* GetReader(DiParticleReader* DiReader, const std::string& VarName);

    protected:
        DiParticleVarReader(DiParticleReader* DiReader, const std::string& VarName);
        DiParticleVarReader(const DiParticleVarReader&) = delete;
        void operator=(const DiParticleVarReader&) = delete;

        DiParticleReader* m_Particle;
        IParticleVarReader* m_FirVar;
        IParticleVarReader* m_SecVar;
        std::string m_VarName;
    };
    class DiParticleComponentReader : public IParticleVariable {
    public:
        virtual std::string name() const;
        virtual double readEntry(size_t I) const;
        virtual bool init(TTree* t);

        static IParticleVarReader* GetReader(const std::string& Name);

    protected:
        DiParticleComponentReader(const std::string& Name);
        DiParticleComponentReader(const DiParticleComponentReader&) = delete;
        void operator=(const DiParticleComponentReader&) = delete;
        DiParticleReader* m_DiPartReader;
    };

    class ResortedParticleReader : public IParticleCollection {
    public:
        virtual bool init(TTree* t);
        virtual std::string name() const;

        virtual IParticleVarReader* RetrieveVariable(const std::string& Var);
        virtual IParticleVarReader* RetrieveBranchAccess(const std::string& Var) const;
        virtual TLorentzVector GetP4(size_t E);
        virtual ~ResortedParticleReader() = default;

        virtual size_t Size();
        virtual size_t ReaderIndex() const;
        virtual size_t ReaderIndex(size_t P);

        size_t Permute(size_t Idx);

        virtual unsigned int GetCollectionHash() const;
        virtual const TLorentzVector& P4() const;

        enum Sorting { ASC, DESC };

    protected:
        ResortedParticleReader(const std::string& Name, const std::string& Sorter, const std::string& Appendix = "");
        ResortedParticleReader(const ResortedParticleReader&) = delete;
        void operator=(const ResortedParticleReader&) = delete;
        // This function returns true if the New value should be sorted Before the old one
        virtual bool Sort(size_t Old, size_t New) const = 0;

    private:
        std::string m_Name;

    protected:
        IParticleReader* m_Particle;
        IParticleVarReader* m_Sorter;

    private:
        size_t m_Idx;
        std::vector<size_t> m_Sorted;
        unsigned int m_EventNumber;
        EventService* m_eventService;
    };
    class ResortedParticleVarReader : public IParticleVariable {
    public:
        virtual std::string name() const;
        virtual double readEntry(size_t I) const;
        virtual bool init(TTree* t);

        static IParticleVarReader* GetReader(ResortedParticleReader* Reader, const std::string& Var);

    private:
        ResortedParticleVarReader(ResortedParticleReader* Reader, const std::string& Var);
        ResortedParticleVarReader(const ResortedParticleVarReader&) = delete;
        void operator=(const ResortedParticleVarReader&) = delete;
        ResortedParticleReader* m_Reader;
        std::string m_VarName;
        IParticleVarReader* m_Var;
    };

    class ResortedParticleReaderDESC : public ResortedParticleReader {
    public:
        static ResortedParticleReaderDESC* GetReader(const std::string& Name, const std::string& Sorter, float Ref = 0);

    protected:
        ResortedParticleReaderDESC(const std::string& Name, const std::string& Sorter, float Ref = 0);
        ResortedParticleReaderDESC(const ResortedParticleReaderDESC&) = delete;
        void operator=(const ResortedParticleReaderDESC&) = delete;

        virtual bool Sort(size_t Old, size_t New) const;
        float m_ReferenceValue;
    };
    class ResortedParticleReaderASC : public ResortedParticleReader {
    public:
        static ResortedParticleReaderASC* GetReader(const std::string& Name, const std::string& Sorter, float Ref = 0);

    protected:
        virtual bool Sort(size_t Old, size_t New) const;
        ResortedParticleReaderASC(const std::string& Name, const std::string& Sorter, float Ref = 0);
        ResortedParticleReaderASC(const ResortedParticleReaderASC&) = delete;
        void operator=(const ResortedParticleReaderASC&) = delete;

        float m_ReferenceValue;
    };

    // This class allows you to sort the particles arcording to a specific value
    // of the event variables.... Abstract implementation since sort is missing
    class EventVarSortedParticleReader : public ResortedParticleReader {
    public:
        virtual bool init(TTree* t);

    protected:
        EventVarSortedParticleReader(const std::string& Name, const std::string& Sorter, ITreeVarReader* EvVar,
                                     const std::string& App = "");
        EventVarSortedParticleReader(const EventVarSortedParticleReader&) = delete;
        void operator=(const EventVarSortedParticleReader&) = delete;
        ITreeVarReader* m_EvSorter;
    };
    class SortedMetParticleReader_ASC : public EventVarSortedParticleReader {
    public:
        static SortedMetParticleReader_ASC* GetReader(const std::string Particle, const std::string& Met);

    protected:
        SortedMetParticleReader_ASC(const std::string& Name, const std::string& Met);
        SortedMetParticleReader_ASC(const SortedMetParticleReader_ASC&) = delete;
        void operator=(const SortedMetParticleReader_ASC&) = delete;
        virtual bool Sort(size_t Old, size_t New) const;
    };
    class SortedMetParticleReader_DESC : public EventVarSortedParticleReader {
    public:
        static SortedMetParticleReader_DESC* GetReader(const std::string Particle, const std::string& Met);

    protected:
        SortedMetParticleReader_DESC(const std::string& Name, const std::string& Met);
        SortedMetParticleReader_DESC(const SortedMetParticleReader_DESC&) = delete;
        void operator=(const SortedMetParticleReader_DESC&) = delete;
        virtual bool Sort(size_t Old, size_t New) const;
    };
    // Combine the momentum of two particles and their quantities
    class TensorCombination;
    class CombinedParticleReader : public IParticleCollection {
    public:
        virtual std::string name() const;
        virtual bool init(TTree* t);

        virtual IParticleVarReader* RetrieveVariable(const std::string& Var);

        virtual size_t Size();
        virtual size_t ReaderIndex() const;
        virtual size_t ReaderIndex(size_t P);
        virtual unsigned int GetCollectionHash() const;
        virtual const TLorentzVector& P4() const;
        virtual TLorentzVector GetP4(size_t E);

        static CombinedParticleReader* GetReader(const std::string& Name, const std::string& First = "", const std::string& Second = "");
        IParticleReader* first_incoming() const;
        IParticleReader* second_incoming() const;

        std::shared_ptr<TensorCombination> get_combination(size_t E);

    private:
        CombinedParticleReader(const std::string& name, const std::string& first, const std::string& second);
        CombinedParticleReader(const CombinedParticleReader&) = delete;
        void operator=(const CombinedParticleReader&) = delete;
        std::string m_name;
        /// Pointer to the first particle
        IParticleReader* m_first;
        IParticleReader* m_second;

        unsigned int m_hash;
        /// Allowed possibilities to combine the two input containers
        std::vector<std::shared_ptr<TensorCombination>> m_combinations;
        /// Let's use  the weighter to keep track of the current event
        Weight* m_weighter;
        Long64_t m_ev_number;
    };

    class CombinedP4VarReader : public IParticleVariable {
    public:
        virtual std::string name() const;
        virtual double readEntry(size_t I) const;
        virtual bool init(TTree* t);

        static IParticleVarReader* GetReader(CombinedParticleReader* Reader, const std::string& Var);

    private:
        CombinedP4VarReader(CombinedParticleReader* Reader, const std::string& component);
        CombinedP4VarReader(const CombinedP4VarReader&) = delete;
        void operator=(const CombinedP4VarReader&) = delete;

        CombinedParticleReader* m_Particle;
        std::string m_VarName;
        std::function<double(size_t)> m_func;
    };

    class TensorCombination {
    public:
        const TLorentzVector& P4() const;
        size_t first_idx() const;
        size_t second_idx() const;
        TensorCombination(CombinedParticleReader* ref, size_t i, size_t j);

    private:
        TLorentzVector m_p4;
        size_t m_first;
        size_t m_second;
    };
    class CombinedParticleVarReader : public IParticleVariable {
    public:
        virtual double readEntry(size_t I) const;
        virtual std::string name() const;
        virtual bool init(TTree* t);

    private:
        CombinedParticleVarReader(CombinedParticleReader* particle, const std::string& var,
                                  MathITreeVarReader::ArithmetricOperators O = MathITreeVarReader::ArithmetricOperators::Unknown);
        CombinedParticleVarReader(const CombinedParticleVarReader&) = delete;
        void operator=(const CombinedParticleVarReader&) = delete;

        CombinedParticleReader* m_particle;
        MathITreeVarReader::ArithmetricOperators m_operator;

        IParticleVarReader* m_first_var;
        IParticleVarReader* m_second_var;
        bool m_Registered;

        // The actual function pointer to evaluate
        std::shared_ptr<BinaryFunc> m_func;
        // Cache variables
        Weight* m_weighter;
        mutable Long64_t m_evNumber;
        mutable std::vector<double> m_cached;
    };

    class OverlapRemovalParticleReader : public IParticleCollection {
    public:
        virtual std::string name() const;
        virtual bool init(TTree* t);

        virtual IParticleVarReader* RetrieveVariable(const std::string& Var);

        virtual size_t Size();
        virtual size_t ReaderIndex() const;
        virtual size_t ReaderIndex(size_t P);
        virtual unsigned int GetCollectionHash() const;
        virtual const TLorentzVector& P4() const;
        virtual TLorentzVector GetP4(size_t E);

        static IParticleReader* GetReader(const std::string& resulting_part, const std::string& rem_from, const std::string& rem_with,
                                          float dR, bool keep_or = false);

        static IParticleReader* GetReader(const std::string& resulting_part, const std::string& rem_from, const std::string& rem_with,
                                          const std::string& from_label, const std::string& cont_idx, bool keep_or = false);

        size_t removeOR(size_t I);
        IParticleReader* underyling_particle() const;

    private:
        OverlapRemovalParticleReader(const std::string& r_name, const std::string& rem_from, const std::string& rem_with, bool keep_or);
        OverlapRemovalParticleReader(const std::string& r_name, const std::string& rem_from, const std::string& rem_with, float dR,
                                     bool keep_or);
        OverlapRemovalParticleReader(const std::string& r_name, const std::string& rem_from, const std::string& rem_with,
                                     const std::string& from_label, const std::string& cont_idx, bool keep_or);

        OverlapRemovalParticleReader(const OverlapRemovalParticleReader&) = delete;
        void operator=(const OverlapRemovalParticleReader&) = delete;
        std::string m_name;
        IParticleReader* m_from;
        IParticleReader* m_with;
        bool m_keep_overlapping;
        /// dR based overlap removal
        float m_dR;
        IParticleVarReader* m_close_dR;
        /// overlap removal has already been
        /// performed at the n-tuple production stage
        IParticleVarReader* m_overlap_idx_from;
        IParticleVarReader* m_idx_with;
        /// weighter to cache the variable
        Weight* m_weighter;

        Long64_t m_last_cached_ev;
        std::vector<size_t> m_surviving;
        bool m_registered;
    };

    class OverlapRemovalParticleVarReader : public IParticleVariable {
    public:
        virtual std::string name() const;
        virtual double readEntry(size_t I) const;
        virtual bool init(TTree* t);
        static IParticleVarReader* GetReader(OverlapRemovalParticleReader*, const std::string& var);

    private:
        OverlapRemovalParticleVarReader(OverlapRemovalParticleReader* particle, const std::string& var_name);
        OverlapRemovalParticleVarReader(const OverlapRemovalParticleVarReader&) = delete;
        void operator=(const OverlapRemovalParticleVarReader&) = delete;
        OverlapRemovalParticleReader* m_or_part;
        std::string m_var_name;
        IParticleVarReader* m_variable;
        bool m_Registered;
    };
}  // namespace XAMPP
#endif  // RECONSTRUCTEDPARTICLEREADERS_H
