#ifndef XAMPPPlotting_HISTOBINS_H
#define XAMPPPlotting_HISTOBINS_H
#include <XAMPPplotting/PlottingUtils.h>

#include <TH1D.h>
#include <TH2D.h>
#include <TH2Poly.h>
#include <TH3D.h>

#include <TFile.h>
#include <map>
#include <memory>
#include <set>
#include <vector>

namespace XAMPP {
    class Weight;
    class HistoBin {
    public:
        // Manipulation of the content
        void AddContent(const double& content);
        void AddError(const double& error);
        void SetContent(const double& content);
        void SetError(const double& error);
        void Fill();
        void Fill(const double& content);

        // Get the content of the bin
        double GetContent() const;
        double GetError() const;
        double GetError2() const;

        static void AddSumW2(bool B = true);

        unsigned int GetdDimension() const;

        bool isOverflow(unsigned int D) const;
        bool isUnderFlow(unsigned int D) const;
        double GetLowEdge(unsigned int D) const;
        double GetHighEdge(unsigned int D) const;
        double GetCenter(unsigned int D) const;

        virtual bool isInsideBin(const double& X) const;
        virtual bool isInsideBin(const double& X, const double& Y) const;
        virtual bool isInsideBin(const double& X, const double& Y, const double& Z) const;

        double GetVolume() const;

        HistoBin(double x_low, double x_high);
        HistoBin(double x_low, double x_high, double y_low, double y_high);
        HistoBin(double x_low, double x_high, double y_low, double y_high, double z_low, double z_high);
        // For more than 3 dimensions
        HistoBin(const std::vector<double>& low_edges, const std::vector<double>& high_edges);

    private:
        HistoBin(unsigned int D);
        bool PassAxisBorders(unsigned int D, const double& X) const;
        Weight* m_weighter;
        double m_content;
        double m_error;
        unsigned int m_dimension;

        std::unique_ptr<double> m_low_edges;
        std::unique_ptr<double> m_high_edges;

        std::unique_ptr<bool> m_is_Overflow;
        std::unique_ptr<bool> m_is_Undeflow;

        double* m_low_edges_ptr;
        double* m_high_edges_ptr;

        bool* m_underflow_ptr;
        bool* m_overflow_ptr;

        static bool m_FillSumW2;
    };
    typedef std::shared_ptr<HistoBin> HistoBin_Ptr;
    class HistoAxis {
    public:
        HistoAxis(unsigned int Nbins, double low_edge, double high_edge);
        HistoAxis(const std::vector<double>& edges);

        unsigned int FindBin(double value) const;
        unsigned int GetNbins() const;

        double GetBinCenter(unsigned int bin) const;
        double GetBinLowEdge(unsigned int bin) const;
        double GetBinUpEdge(unsigned int bin) const;
        std::vector<double> GetBinning() const;

    private:
        unsigned int FindBin(const double& value, std::vector<double>::const_iterator begin_itr,
                             std::vector<double>::const_iterator end_itr) const;
        double m_low_edge;
        double m_high_edge;
        unsigned int m_nbins;
        bool m_equal_distance;
        double m_bin_width;
        std::vector<double> m_bin_edges;
        std::vector<double>::const_iterator m_itr_begin;
        std::vector<double>::const_iterator m_itr_end;
    };
    typedef std::shared_ptr<HistoAxis> HistoAxis_Ptr;

    class Histogram {
    public:
        Histogram(unsigned int nbins, double low_edge, double up_edge);
        Histogram(unsigned int nbins_x, double x_low, double x_high, unsigned int nbins_y, double y_low, double y_high);
        Histogram(unsigned int nbins_x, double x_low, double x_high, unsigned int nbins_y, double y_low, double y_high,
                  unsigned int nbins_z, double z_low, double z_high);
        Histogram(const Histogram& other);

    protected:
        Histogram();
        unsigned int m_dim;
        HistoAxis_Ptr m_x_axis;
        HistoAxis_Ptr m_y_axis;
        HistoAxis_Ptr m_z_axis;
        std::vector<HistoBin_Ptr> m_bins;
        size_t m_entries;
    };
}  // namespace XAMPP

#endif
