#ifndef XAMPPPLOTTING_ARITHMETRICREADERS_H_
#define XAMPPPLOTTING_ARITHMETRICREADERS_H_
#include <XAMPPplotting/TreeVarReader.h>
#include <cmath>
#include <fstream>
#include <functional>
#include <map>
#include <memory>
#include <set>
#include <sstream>

namespace XAMPP {
    class Weight;
    typedef std::function<double(const size_t, const size_t)> BinaryFunc;
    typedef std::function<double(const size_t)> UnaryFunc;
    class MathITreeVarReader : virtual public IVectorReader {
    public:
        enum ArithmetricOperators {
            Unknown = 0,
            Plus,
            Minus,
            Times,
            Divide,
            MinusOnCirc,  // Essentially used to calculate deltaPhi
            Modulo,
            Absolute,
            Sqrt,
            Ln,
            Exp,
            Inverse,
            Cos,
            Sin,
            Cosh,
            Sinh,
            Atanh,
            Acos,
        };
        static MathITreeVarReader::ArithmetricOperators operator_name(const std::string& O);
        static std::string operator_name(MathITreeVarReader::ArithmetricOperators O);
        static std::string reader_name(ITreeVarReader* R1, ITreeVarReader* R2, MathITreeVarReader::ArithmetricOperators O);
        static bool isBinary(MathITreeVarReader::ArithmetricOperators O);

        static ITreeVectorReader* GetReader(ITreeVarReader* V1, ITreeVarReader* V2, const std::string& op, const std::string& alias = "");
        static ITreeVectorReader* GetReader(ITreeVarReader* V1, ITreeVarReader* V2, ArithmetricOperators op, const std::string& alias = "");
        static ITreeVectorReader* GetReader(ITreeVarReader* V1, int modulo, const std::string& alias = "");

        virtual std::string name() const;
        virtual bool init(TTree* t);

        virtual double readEntry(const size_t I) const;
        virtual size_t entries() const;
        virtual ~MathITreeVarReader();

    protected:
        void inputMustNotTheSame();
        virtual std::shared_ptr<UnaryFunc> create_functional(ArithmetricOperators O);

        MathITreeVarReader(ITreeVarReader* V1, ITreeVarReader* V2, ArithmetricOperators O = ArithmetricOperators::Unknown,
                           const std::string& alias = "");
        MathITreeVarReader(ITreeVarReader* V1, int modulo, const std::string& alias = "");
        MathITreeVarReader(const MathITreeVarReader&) = delete;
        void operator=(const MathITreeVarReader&) = delete;
        ITreeVarReader* m_V1;
        ITreeVarReader* m_V2;
        int m_modulo;

    private:
        ArithmetricOperators m_operator;
        std::shared_ptr<UnaryFunc> m_unary_fct;

        std::string m_alias;
        bool m_Register;
        bool m_inReaderDiffer;

        mutable std::vector<double> m_cache;
        mutable Long64_t m_eventNumber;
        EventService* m_service;
    };
    //  The MathVectorScalar reader requires one reader which is of vector type and the second one is of a scalar type.
    //  Thereby the vector is always the left element of the operators. If you want Scalar / Vector
    //   ->  construct (1 / Vector) * Scalar. For Scalar - Vector construct (-1.* Vector) + Scalar.
    class MathVectorScalarReader : public MathITreeVarReader {
    public:
        static ITreeVectorReader* GetReader(ITreeVarReader* V1, ITreeVarReader* V2, const std::string& op, const std::string& alias = "");
        static ITreeVectorReader* GetReader(ITreeVarReader* V1, ITreeVarReader* V2, ArithmetricOperators op, const std::string& alias = "");
        virtual size_t entries() const;

    protected:
        MathVectorScalarReader(ITreeVarReader* V1, ITreeVarReader* V2, ArithmetricOperators O = ArithmetricOperators::Unknown,
                               const std::string& alias = "");
        MathVectorScalarReader(const MathVectorScalarReader&) = delete;
        void operator=(const MathVectorScalarReader&) = delete;
        virtual std::shared_ptr<UnaryFunc> create_functional(ArithmetricOperators O);
    };

}  // namespace XAMPP
#endif /* XAMPPPLOTTING_ARITHMETRICREADERS_H_ */
