#ifndef XAMPPPLOTTING_TREEVARREADER_H
#define XAMPPPLOTTING_TREEVARREADER_H

#include <fstream>
#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <vector>

#include <TLorentzVector.h>
#include <TTree.h>

#include <XAMPPplotting/EventService.h>
#include <XAMPPplotting/IfDefHelpers.h>
#include <XAMPPplotting/PlottingUtils.h>
namespace XAMPP {
    class Weight;
    class Condition;
    class IParticleReader;
    // ITreeVarReader is the abstract class for all readers interacting with the XAMPP::Histo and XAMPP::Condition class.
    // The logic is that the histogram is filled until the update() function returns false.
    // Since a variable can be plotted in many different histogram the reader is reset() after filling
    // such that the next histogram can be filled.
    // Be aware that before your loop over the analysis tree actually starts the reader must be initialized with the TTree
    class ITreeVarReader {
    public:
        virtual std::string name() const = 0;
        virtual bool init(TTree* t) = 0;
        virtual double read() const = 0;
        virtual bool update() = 0;
        virtual void reset() = 0;

        // How many entries are stored in
        virtual size_t entries() const = 0;
        // Read a specific entry
        virtual double readEntry(const size_t I) const = 0;

        virtual ~ITreeVarReader() = default;
    };
    // If the stored variable is of a std::vector type it is useful to have the possibility to read out a specific entry of the vector,
    // For update puposes the Reader can be asked if the N-th entry exists at all and can be read out
    class ITreeVectorReader : virtual public ITreeVarReader {
    public:
        // The set Index method moves the internal value pointer to the i-th entry
        // The update method is continued from that position then
        virtual bool setIndex(const size_t I) = 0;
        // At which position is the internal pointer of the class currently
        virtual size_t index() const = 0;
    };
    // The ITreeMatrixReader allows to read out 2D data-structures, which is useful if we decorate each
    // particle with a vectorial information or determine the correlation between two particles
    class ITreeMatrixReader : virtual public ITreeVectorReader {
    public:
        // As in a usual matrix the first index denotes the row, while the second one denotes the column
        //
        //  O O O O O
        //  O O O X O
        //  E.g. the x is located at 1,4
        // The setMatrixEntry sets the internal pointer to the position in the grid
        // If the position is inside the matrix then true is returned false otherwise
        virtual bool setMatrixIndex(const size_t i, const size_t j) = 0;

        virtual double readMatrixEntry(const size_t i, const size_t j) const = 0;

        // The concrete implementations rely on nested vector<vector> containers.
        //  Where the outer vector is interpreted as the row while the inner represents the column.
        //  The num_rows() method returns the size of the outer vector
        virtual size_t num_rows() const = 0;
        //  the row_entries method depends on the actual row-number
        //  If the row_number exceeds the existing rows 0 is returned
        virtual size_t row_entries(size_t i) const = 0;

        // Returns the current pointer index to the row of the IMatrixReader
        virtual size_t row_index() const = 0;
        // Returns the current pointer index to the exact element in the i-th row
        virtual size_t column_index() const = 0;
    };
    //  The IParticleVarReader class is the helper class to read out scalar variables associated with a dynamically defined Particle
    //  inside the XAMPPplotting framework (c.f. details below).
    class IParticleVarReader : public ITreeVarReader {
    public:
        // Returns whether I-th particle exists which satisfies the particle selection criteria
        virtual bool At(size_t I) = 0;
        // Returns the connected particle
        virtual IParticleReader* getParticle() const = 0;
    };
    class IParticleVectorReader : public IParticleVarReader {
    public:
        virtual double readMatrixEntry(size_t I, size_t J) const = 0;
        virtual size_t row_entries(size_t I) const = 0;
        virtual bool setMatrixIndex(size_t I, size_t J) = 0;
    };

    class IParticleReader : public ITreeVarReader {
    public:
        virtual size_t Size() = 0;
        // Retrieves a variable associated with the particle itself.
        // Elements are kicked from the vector if the underlying selection criteria
        // are not satisfied
        virtual IParticleVarReader* RetrieveVariable(const std::string& Var) = 0;

        virtual const TLorentzVector& P4() const = 0;
        // TLorentzVector of the i-th element
        virtual TLorentzVector GetP4(size_t E) = 0;

        virtual bool At(size_t I) = 0;

        // Returns the index of the TreeVarReaders used by the IParticleReader
        virtual size_t ReaderIndex() const = 0;
        virtual size_t ReaderIndex(size_t I) = 0;

        // Returns the current index of the IParticleReader itself
        virtual size_t Index() const = 0;
        // Returns the hash value of the branch name accessed by the IParticleReader
        virtual unsigned int GetCollectionHash() const = 0;
    };

    // Central store where all ITreeVarReaders should be registered by name as identifier.
    // Deletes all readers if it is deleted avoiding memory leaks at the end.
    class ITreeVarReaderStorage {
    public:
        static ITreeVarReaderStorage* GetInstance();
        // Each ITreeVarReader should register itself with the storage. By calling
        // this method in the constructor. Returns fals if the name is already occupied.
        bool Register(ITreeVarReader* R);
        // The GetReader method returns a registered ITreeVarReader
        // So once the particular TreeReaders are created one can grab them from here
        ITreeVarReader* GetReader(const std::string& Name) const;
        virtual ~ITreeVarReaderStorage();

    private:
        static ITreeVarReaderStorage* m_Inst;
        ITreeVarReaderStorage();
        ITreeVarReaderStorage(const ITreeVarReaderStorage&) = delete;
        void operator=(const ITreeVarReaderStorage&) = delete;
        std::map<size_t, std::shared_ptr<ITreeVarReader>> m_DB;
    };
    // The framework provides a special treatment of particles as indicated above. Users can
    // define Cuts on them, add Variables etc... IParticleReader register themselves in this store
    // instead of the general ITreeVarReaderStorage. Gets deleted if the ITreeVarReaderStorage is deleted
    // and deletes all registered particles.
    class ParReaderStorage {
    public:
        static ParReaderStorage* GetInstance();
        IParticleReader* GetReader(const std::string& Name) const;
        bool Register(IParticleReader* R);
        virtual ~ParReaderStorage();

    protected:
        static ParReaderStorage* m_Inst;
        ParReaderStorage();
        ParReaderStorage(const ParReaderStorage&) = delete;
        void operator=(const ParReaderStorage&) = delete;

    private:
        // The name of each reader is encoded in a hash
        std::map<size_t, std::shared_ptr<IParticleReader>> m_DB;
    };

    // Scalar variables have a common update mechanism:
    //    entries() == 1
    //    update() -> if not true else false
    // To slim the downstream classes this pattern
    // is implemented in the ISccalarReader class
    class IScalarReader : public ITreeVarReader {
    public:
        virtual bool update();
        virtual void reset();

        virtual double readEntry(const size_t I) const;
        virtual size_t entries() const;

    protected:
        IScalarReader();
        IScalarReader(const IScalarReader&) = delete;
        void operator=(const IScalarReader&) = delete;

    private:
        bool m_updated;
    };
    // Vector variables have all update mechansims in common:
    //  update() -> setIndex( index() + 1);
    //  read() -> readEntry( index());
    //  The IVectorReader implements this pattern once for all
    //  ITreeVectorVarReader
    class IVectorReader : public ITreeVectorReader {
    public:
        virtual double read() const;
        virtual size_t index() const;

        virtual bool update();
        virtual bool setIndex(const size_t I);

        virtual void reset();

    protected:
        IVectorReader();
        IVectorReader(const IVectorReader&) = delete;
        void operator=(const IVectorReader&) = delete;

    private:
        size_t m_index;
    };
    class IMatrixReader : public ITreeMatrixReader {
    public:
        // Returns the total number of entries inside the reader
        virtual size_t entries() const;

        virtual double read() const;
        virtual double readEntry(const size_t I) const;

        virtual bool update();
        virtual bool setIndex(const size_t I);
        virtual bool setMatrixIndex(const size_t i, const size_t j);
        virtual void reset();

        // Returns the index if the IMatrixReader is interpreted as
        // as vector
        virtual size_t index() const;
        // Returns the current pointer index to the row of the IMatrixReader
        virtual size_t row_index() const;
        // Returns the current pointer index to the exact element in the i-th row
        virtual size_t column_index() const;

    protected:
        IMatrixReader();
        IMatrixReader(const IMatrixReader&) = delete;
        void operator=(const IMatrixReader&) = delete;

        std::pair<size_t, size_t> get_indices(const size_t I) const;

    private:
        size_t m_row_idx;
        size_t m_col_idx;

        EventService* m_service;
        mutable Long64_t m_ev_number;
        mutable size_t m_tot_entries;
        mutable std::vector<size_t> m_row_sizes;
    };

    class IParticleVariable : public IParticleVarReader {
    public:
        virtual size_t entries() const;
        virtual size_t index() const;

        virtual bool At(size_t I);
        // Read out the variable of the n-th particle satisfing the selection criteria
        virtual IParticleReader* getParticle() const;

        virtual bool update();
        virtual void reset();
        virtual double read() const;

    protected:
        IParticleVariable(IParticleReader* particle);
        IParticleVariable(const IParticleVariable&) = delete;
        void operator=(const IParticleVariable&) = delete;

    private:
        IParticleReader* m_Particle;
        bool m_updated;
        size_t m_Idx;
    };
    class IParticleVector : public IParticleVectorReader {
    public:
        virtual IParticleReader* getParticle() const;
        virtual size_t entries() const;

        virtual double read() const;
        virtual double readEntry(const size_t I) const;

        virtual bool At(size_t I);
        virtual bool update();

        virtual bool setMatrixIndex(size_t I, size_t J);

        virtual void reset();

    protected:
        IParticleVector(IParticleReader* particle);
        IParticleVector(const IParticleVariable&) = delete;
        void operator=(const IParticleVariable&) = delete;
        std::pair<size_t, size_t> get_indices(const size_t I) const;

    private:
        IParticleReader* m_Particle;

        size_t m_par_idx;
        size_t m_var_idx;

        EventService* m_service;
        mutable Long64_t m_ev_number;
        mutable size_t m_tot_entries;
        mutable std::vector<size_t> m_row_sizes;
    };

    class IParticleCollection : public IParticleReader {
    public:
        virtual size_t Index() const;
        virtual double read() const;
        virtual double readEntry(const size_t) const;
        virtual bool update();
        virtual void reset();
        virtual bool At(size_t I);
        // The IParticle Collection always returns 0
        virtual size_t entries() const;

    protected:
        IParticleCollection();
        IParticleCollection(const IParticleCollection&) = delete;
        void operator=(const IParticleCollection&) = delete;

    private:
        size_t m_Idx;
    };
    // This Reader is the standard class for reading out event information like
    // RunNumber, mcChannelNumer, Trigger or the missing transverse energy.
    // It is important that the data-type of the branch actually matches the
    // datatype of the reader itself. Otherwise the Reader cannot be initialized.
    template <class T> class ScalarVarReader : public IScalarReader {
    public:
        virtual std::string name() const;
        virtual double read() const;
        virtual bool init(TTree* t);
        static ScalarVarReader<T>* GetReader(const std::string& varname);
        virtual ~ScalarVarReader() {}

        // Direct access to the variable...
        const T& get() const;

    protected:
        ScalarVarReader(const std::string& varname);
        ScalarVarReader(const ScalarVarReader&) = delete;
        void operator=(const ScalarVarReader&) = delete;

    private:
        std::string m_var_name;
        mutable T m_var;
        unsigned int m_TreeNumber;
        mutable Long64_t m_lastEvent;
        TBranch* m_branch;
        EventService* m_evService;
        TreeIndexer* m_indexer;
        bool m_Registered;
    };
    // Energy variables like the missing transverse energy, effective mass or the Ht are stored in MeV units in the tree.
    // This Reader returns the values in GeV. Be aware only one type of reader can be connected to the Reader at the same time. i.e. either
    // ScalarVarReaderGeV or ScalarVarReader, but both readers can exist simultaneously
    template <class T> class ScalarVarReaderGeV : public ScalarVarReader<T> {
    public:
        virtual double read() const;

        static ScalarVarReaderGeV<T>* GetReader(const std::string& varname);
        virtual ~ScalarVarReaderGeV() = default;

    protected:
        ScalarVarReaderGeV(const std::string& varname);
        ScalarVarReaderGeV(const ScalarVarReaderGeV&) = delete;
        void operator=(const ScalarVarReaderGeV&) = delete;
    };
    // Standard class to load the vector like branches
    template <class T> class VectorVarReader : public IVectorReader {
    public:
        virtual std::string name() const;
        virtual size_t entries() const;
        virtual bool init(TTree* t);
        virtual double readEntry(const size_t I) const;

        static VectorVarReader<T>* GetReader(const std::string& varname);
        virtual ~VectorVarReader() = default;

        const std::vector<T>& get() const;

    protected:
        VectorVarReader(const std::string& varname);
        VectorVarReader(const VectorVarReader&) = delete;
        void operator=(const VectorVarReader&) = delete;

    private:
        bool m_Registered;
        mutable Long64_t m_lastEvent;
        unsigned int m_TreeNumber;

        TBranch* m_branch;
        EventService* m_evService;
        TreeIndexer* m_indexer;
        std::string m_var_name;
        std::vector<T>* m_var;
    };

    template <class T> class VectorVarReaderGeV : public VectorVarReader<T> {
    public:
        virtual double readEntry(const size_t I) const;

        static VectorVarReaderGeV<T>* GetReader(const std::string& varname);
        virtual ~VectorVarReaderGeV() = default;

    protected:
        VectorVarReaderGeV(const std::string& varname);
        VectorVarReaderGeV(const VectorVarReaderGeV&) = delete;
        void operator=(const VectorVarReaderGeV&) = delete;
    };
    template <class T> class MatrixVarReader : public IMatrixReader {
    public:
        virtual std::string name() const;
        virtual bool init(TTree* t);

        virtual double readMatrixEntry(const size_t i, const size_t j) const;
        virtual size_t num_rows() const;
        virtual size_t row_entries(size_t i) const;

        static MatrixVarReader<T>* GetReader(const std::string& varname);
        virtual ~MatrixVarReader() = default;

    protected:
        MatrixVarReader(const std::string& varname);
        MatrixVarReader(const MatrixVarReader&) = delete;
        void operator=(const MatrixVarReader&) = delete;

    private:
        bool m_Registered;
        std::string m_var_name;
        std::vector<std::vector<T>>* m_var;
        mutable Long64_t m_lastEvent;
        unsigned int m_TreeNumber;
        TBranch* m_branch;
        EventService* m_evService;
        TreeIndexer* m_indexer;
    };
    template <class T> class MatrixVarReaderGeV : public MatrixVarReader<T> {
    public:
        virtual double readMatrixEntry(const size_t i, const size_t j) const;
        static MatrixVarReaderGeV<T>* GetReader(const std::string& varname);
        virtual ~MatrixVarReaderGeV() = default;

    protected:
        MatrixVarReaderGeV(const std::string& varname);
        MatrixVarReaderGeV(const MatrixVarReaderGeV&) = delete;
        void operator=(const MatrixVarReaderGeV&) = delete;
    };

    // Although XAMPP does not provide any mechanism to write Array branches we're going to support at least their read out.
    template <class T> class ArrayReader : public IVectorReader {
    public:
        virtual std::string name() const;
        virtual double readEntry(const size_t I) const;
        virtual size_t entries() const;
        virtual bool init(TTree* t);

        static ArrayReader<T>* GetReader(const std::string& varname, ITreeVarReader* = nullptr, unsigned int Cache = 50);
        virtual ~ArrayReader();

    protected:
        ArrayReader(const std::string& varname, ITreeVarReader* SizeBranch, unsigned int Cache);
        ArrayReader(const ArrayReader&) = delete;
        void operator=(const ArrayReader&) = delete;

    private:
        bool m_Registered;
        std::string m_var_name;
        T* m_var;
        mutable Long64_t m_lastEvent;
        unsigned int m_TreeNumber;
        TBranch* m_branch;
        EventService* m_evService;
        TreeIndexer* m_indexer;
        ITreeVarReader* m_Size;
    };

    class ParticleReader : public IParticleCollection {
    public:
        static ParticleReader* GetReader(const std::string& Name, const std::string& Coll = "");

        template <typename T>
        std::shared_ptr<Condition> AddSimpleCut(const std::string& name, const std::string& condition, float CutValue, bool InGeV = false);
        template <typename T> bool AttachVariable(const std::string& var_name, bool UseGeV = false);
        template <typename T> bool AttachVector(const std::string& var_name, bool UseGeV = false);

        std::shared_ptr<Condition> CombineCuts(std::shared_ptr<Condition> cut1, std::shared_ptr<Condition> cut2, Condition::Mode mode);

        bool AddVariable(std::string V);
        bool AddArithmetricVariable(const std::string& alias, const std::string& first, const std::string& second,
                                    const std::string& math_operator);
        void SetupDone();

        virtual std::string name() const;
        // How many particles satisfy  the criteria defined inside the Particle
        // Maximum it is equivalent to the size of the branches themselves
        virtual size_t Size();
        // Returns the actual index of the std::vector connected to the branch
        virtual size_t ReaderIndex() const;
        // Returns the pointer of the std:vectors connected to the branch if for the I-th particle is asked
        // The method cannot be const since it involves the internal call of the Size method
        virtual size_t ReaderIndex(size_t I);
        // Returns the index of the particle reader itself after imposing all the cuts
        //  Unique hash encoding the branches the ParticleReader is accessing
        //  E.g. elec_<Blah>
        virtual unsigned int GetCollectionHash() const;
        // The name of the branches accessing the particle reader in raw form
        std::string GetCollectionName() const;
        //
        //  How many entries before the current one need to be skipped in order
        //  to access the proper information in the tree
        size_t GetTargetEntry(size_t Idx) const;

        virtual ITreeVectorReader* RetrieveBranchAccess(const std::string& Var) const;
        virtual IParticleVarReader* RetrieveVariable(const std::string& Var);

        virtual bool init(TTree* t);

        virtual const TLorentzVector& P4() const;
        virtual TLorentzVector GetP4(size_t E);

        bool Setup(std::ifstream& InStream);

        virtual ~ParticleReader();

        void DisableMeVtoGeV();

    protected:
        ParticleReader(const std::string& Name, const std::string& Coll);
        ParticleReader(const ParticleReader&) = delete;
        void operator=(const ParticleReader&) = delete;
        virtual std::string time_component() const;

        virtual ITreeVectorReader* AddVariable(std::stringstream& sstr);

        template <typename T> ITreeVectorReader* NewVariable(const std::string& Name);
        template <typename T> ITreeVectorReader* NewVariableGeV(const std::string& Name);
        template <typename T> ITreeMatrixReader* NewMatrixVariable(const std::string& Name);
        template <typename T> ITreeMatrixReader* NewMatrixVariableGeV(const std::string& Name);
        ITreeVectorReader* NewArithmetricVariable(const std::string& alias, const std::string& first, const std::string& second,
                                                  const std::string& math_operator);

        ITreeVectorReader* GetVariable(const std::string& Name) const;

        ITreeMatrixReader* GetMatrixVariable(const std::string& Name) const;

        bool InsertVariable(const std::string& Name, ITreeVectorReader* Variable);
        bool IsVariableDefined(const std::string& Name) const;

        void SetParticleCollection(const std::string& Coll);

    private:
        void AddP4Readers();
        bool AddCombinedCut(std::ifstream& InStream, Condition::Mode Mode);
        bool AddCut(std::stringstream& sstr);
        // Check whether the i-th entry of the branches associated with the trees
        // pass the requirements imposed.
        bool PassCuts(size_t I) const;

        std::string m_Name;
        std::string m_ParCollection;
        unsigned int m_ParCollectionHash;

        bool m_Setup;
        bool m_Registered;
        bool m_UseGeVReader;

        // Variables and cuts to define the ParticleReader
        std::map<std::string, ITreeVectorReader*> m_Vars;
        std::vector<ITreeVectorReader*> m_VarsVec;
        std::vector<std::shared_ptr<Condition>> m_Cuts;

        // ##############################################################################
        //  These three variables are needed  by the size method and needed             #
        //  for the evaluation of the size. Since the Size method has become const      #
        //  they must be mutable                                                        #
        // ##############################################################################
        Long64_t m_CurrentEv;
        std::vector<size_t> m_Skipped;
        std::unordered_map<size_t, size_t> m_TargetMap;
        std::unordered_map<size_t, size_t>::const_iterator m_TargetEnd;
        size_t m_Size;
        size_t m_FistLookUp;
        EventService* m_service;

    protected:
        // The Four momentum is saved seperately for faster access
        ITreeVectorReader* m_TimeComReader;
        ITreeVectorReader* m_PtReader;
        ITreeVectorReader* m_EtaReader;
        ITreeVectorReader* m_PhiReader;
        mutable TLorentzVector m_P4;
    };

    class ParticleReaderM : public ParticleReader {
    public:
        static ParticleReaderM* GetReader(const std::string& Name, const std::string& Coll = "");

        virtual TLorentzVector GetP4(size_t E);
        virtual const TLorentzVector& P4() const;

        virtual ~ParticleReaderM() {}

    protected:
        ParticleReaderM(const std::string& Name, const std::string& Coll);
        ParticleReaderM(const ParticleReaderM&) = delete;
        void operator=(const ParticleReaderM&) = delete;
        virtual std::string time_component() const;
    };

    class ParticleReaderMTAR : public ParticleReaderM {
    public:
        static ParticleReaderMTAR* GetReader(const std::string& Name, const std::string& Coll = "");
        virtual ~ParticleReaderMTAR() {}

    protected:
        ParticleReaderMTAR(const std::string& Name, const std::string& Coll);
        ParticleReaderMTAR(const ParticleReaderMTAR&) = delete;
        void operator=(const ParticleReaderMTAR&) = delete;
        virtual std::string time_component() const;
    };

    class ParticleVarReader : public IParticleVariable {
    public:
        static ParticleVarReader* GetReader(ParticleReader* P, const std::string& VarName);
        virtual double readEntry(size_t I) const;
        virtual std::string name() const;
        virtual std::string variable_name() const;
        virtual bool init(TTree* t);

    protected:
        ParticleVarReader(ParticleReader* Particle, const std::string& VarName);
        ParticleVarReader(const ParticleVarReader&) = delete;
        void operator=(const ParticleVarReader&) = delete;

        // This constructor is used by the Particle component reader
        ParticleVarReader(ParticleReader* Particle);

        ParticleReader* m_Particle;
        ITreeVectorReader* m_Var;
        std::string m_Name;
    };

    class ParticleVectorVarReader : public IParticleVector {
    public:
        static ParticleVectorVarReader* GetReader(ParticleReader* P, const std::string& VarName);

        virtual double readMatrixEntry(size_t I, size_t J) const;
        virtual size_t row_entries(size_t I) const;
        virtual std::string name() const;
        virtual std::string variable_name() const;
        virtual bool init(TTree* t);
        virtual ~ParticleVectorVarReader() {}

    protected:
        ParticleVectorVarReader(ParticleReader* Particle, const std::string& VarName);
        ParticleVectorVarReader(const ParticleVectorVarReader&) = delete;
        void operator=(const ParticleVarReader&) = delete;

        ParticleReader* m_Particle;
        ITreeMatrixReader* m_Var;
        std::string m_Name;
    };

    class ParticleTimeReader : public ParticleVarReader {
    public:
        enum MomentumComponent { Energy, Mass };
        static ParticleTimeReader* GetReader(ParticleReader* P, MomentumComponent C);
        virtual double readEntry(size_t I) const;
        virtual bool init(TTree* t);
        virtual ~ParticleTimeReader() {}

    protected:
        ParticleTimeReader(ParticleReader* P, MomentumComponent C);
        ParticleTimeReader(const ParticleTimeReader&) = delete;
        void operator=(const ParticleTimeReader&) = delete;

    private:
        MomentumComponent m_Component;
        bool m_Registered;
    };

}  // namespace XAMPP
#include <XAMPPplotting/TreeVarReader.ixx>
#endif  // TREEVARREADER_H
