#ifndef XAMPP_Selector_h
#define XAMPP_Selector_h

#include <XAMPPplotting/Analysis.h>
#include <XAMPPplotting/EventService.h>
#include <XAMPPplotting/PlottingUtils.h>
#include <map>

namespace XAMPP {
    class Histo;

    class Selector : public Analysis {
    public:
        Selector(const std::string &Name, const std::string &TreeName, const std::vector<std::string> &Files);

        virtual bool Process(long int nMax = -1, long int nSkip = 0);
        bool ReadHistoConfig(const std::string &Config);
        virtual ~Selector();
        bool userPartialList(const std::vector<std::string> &files, unsigned int begin, unsigned int end);

        virtual bool begin(TTree *t);

    protected:
        bool isData() const;
        bool CreateNewAnalysisRegion(const std::string &Region);
        bool InsertHistogramInRegion(const std::string &Region, Histo *H);
        bool InsertHistogramInRegion(const std::string &Region, std::shared_ptr<Histo> H);

        virtual bool SetupHistograms(const std::string &Syst);

        virtual bool AnalyzeEvent();
        virtual bool InitHistos();
        virtual void SetupReaders();
        struct EventRegion {
            EventRegion(const std::string &N) { Name = N; }
            std::string Name;
            std::vector<std::shared_ptr<Histo>> Histograms;
        };
        bool isRegionDefined(const std::string &region_name) const;

        template <typename T> bool ConnectVariable(TTree *t, T &Var, const std::string Branch) {
            TBranch *br = t->GetBranch(Branch.c_str());
            if (!br) {
                Error("Selector::ConnectVariable()", "There is no Branch " + Branch + " in the input tree " + std::string(t->GetName()));
                return false;
            }
            TreeIndexer *indexer = EventService::getService()->getIndexer(br);
            if (!indexer) {
                Error("Selector::ConnectVariable()", "Failed to find the indxer");
                return false;
            }
            indexer->tree()->SetCacheSize(EventService::getService()->cacheSize());
            indexer->tree()->SetBranchStatus(Branch.c_str(), 1);
            Var = 0;
            if (indexer->tree()->AddBranchToCache(Branch.c_str()) == -1) {
                Error("Selector::ConnectVariable()", "Could not add the Branch " + Branch + " to the read out cache.");
                return false;
            }
            if (indexer->tree()->SetBranchAddress(Branch.c_str(), &Var) != 0) {
                Error("Selector::ConnectVariable()", "Could not connect the variable " + Branch + " with the tree " +
                                                         std::string(t->GetName()) + ". Seems the variable is of wrong type");
                return false;
            }
            EventService::getService()->loadEntireEvent();
            return true;
        }

    private:
        std::vector<EventRegion> m_RegionDefinitions;
    };
}  // namespace XAMPP

#endif
