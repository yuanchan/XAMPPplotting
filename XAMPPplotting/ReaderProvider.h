#ifndef XAMPP_ReaderProvider_h
#define XAMPP_ReaderProvider_h

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include <XAMPPplotting/IfDefHelpers.h>
#include <XAMPPplotting/PlottingUtils.h>

namespace XAMPP {
    class ITreeVarReader;
    class IParticleVarReader;
    class IParticleReader;
    class ParticleReader;

    enum TimeComponent { Energy = 0, Mass = 1, MTAR = 2 };

    class ReaderProvider {
    public:
        static ReaderProvider* GetInstance(bool DisableGeVToMeV = false);

        virtual ITreeVarReader* CreateReader(std::stringstream& sstr);
        virtual ITreeVarReader* CreateReader(std::ifstream& inf, std::stringstream& sstr);

        IParticleReader* CreateParticle(const std::string& line);

        ITreeVarReader* CreateReader(const std::string& line);
        ITreeVarReader* CreateReader(std::ifstream& inf, const std::string& line);

        IParticleReader* CreateParticle(std::ifstream& inf, const std::string& line);

        virtual ParticleReader* GetParticle(XAMPP::TimeComponent t, const std::string& Name, const std::string& Coll = "");

        virtual ~ReaderProvider();

    protected:
        ReaderProvider(bool DisableGeVToMeV);
        ReaderProvider(const ReaderProvider&) = delete;
        void operator=(const ReaderProvider&) = delete;

        static ReaderProvider* m_Inst;
        IfDefLineParser ReadLine;

        bool disableGeVtoMeV() const;

    private:
        ITreeVarReader* CreateScalarReader(std::stringstream& sstr);

        ITreeVarReader* CreateParticleVarReader(std::stringstream& sstr);
        ITreeVarReader* CreateParticleVecVarSizeReader(std::stringstream& sstr);
        ITreeVarReader* CreateVectorReader(std::stringstream& sstr);
        ITreeVarReader* CreateVectorSizeReader(std::stringstream& sstr);

        ITreeVarReader* CreatePseudoReader(std::stringstream& sstr);

        ITreeVarReader* CreateNumParticleReader(std::stringstream& sstr);
        ITreeVarReader* CreateMeffReader(std::stringstream& sstr);
        ITreeVarReader* CreateHtReader(std::stringstream& sstr);
        ITreeVarReader* CreateMultiNumParticleReader(std::stringstream& sstr);

        ITreeVarReader* CreateInvariantDiMassReader(std::stringstream& sstr);
        ITreeVarReader* CreateInvariantDiPtReader(std::stringstream& sstr);
        ITreeVarReader* CreateContraTransverseMassReader(std::stringstream& sstr);

        ITreeVarReader* CreateMomentumImbalanceReader(std::stringstream& sstr);

        ITreeVarReader* CreateClosestDeltaRReader(std::stringstream& sstr);
        ITreeVarReader* CreateClosestDeltaRReaderY(std::stringstream& sstr);

        ITreeVarReader* CreateFixedDeltaRReader(std::stringstream& sstr);
        ITreeVarReader* CreateClosestDeltaPhiReader(std::stringstream& sstr);

        ITreeVarReader* CreateFixedDeltaYReader(std::stringstream& sstr);
        ITreeVarReader* CreateFixedDeltaPhiReader(std::stringstream& sstr);
        ITreeVarReader* CreateFixedDeltaEtaReader(std::stringstream& sstr);

        ITreeVarReader* CreateDeltaPhiToMetReader(std::stringstream& sstr);
        ITreeVarReader* CreateMtMetReader(std::stringstream& sstr);
        ITreeVarReader* CreateDiPartCompReader(std::stringstream& sstr);

        ITreeVarReader* CreateTaggerReader(std::stringstream& sstr);

        // Readers which are not implementable within one line
        ITreeVarReader* CreateExtremumReader(std::ifstream& inf, const std::string& Name, bool Max);
        ITreeVarReader* CreateClassificationReader(const std::string& Name, std::ifstream& inf);
        ITreeVarReader* CreateSumUpReader(const std::string& Name, std::ifstream& inf);
        ITreeVarReader* CreateSignificanceReader(const std::string& Name, std::ifstream& inf);
        ITreeVarReader* CreateTaggerReader(/*const std::string& Name,*/ std::ifstream& inf);
        ITreeVarReader* CreateSetReader(const std::string& Name, std::ifstream& inf);
        ITreeVarReader* ConstrainRanges(IParticleVarReader* Variable, Ranges Rng);

        // Arithmetric readers
        ITreeVarReader* CreateArithmetricReader(std::ifstream& inf, const std::string& math_op);
        // Overlap removal reader
        IParticleReader* CreateParticleORReader(std::ifstream& inf, const std::string& r_name);

        bool m_disableMeVtoGeV;
    };
}  // namespace XAMPP

#endif
