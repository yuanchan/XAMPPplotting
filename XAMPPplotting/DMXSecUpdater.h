#ifndef XAMPPPPlotting_DMXSecUpdater_H
#define XAMPPPPlotting_DMXSecUpdater_H

#include <string>

#if ROOTCORE_RELEASE_SERIES == 24
#include <RootCore/Packages.h>
#endif

#ifdef ROOTCORE_PACKAGE_DMXSecUtils
#include <DMXSecUtils/DMXSecTool.h>
#endif

namespace XAMPP {
    class DMXSecUpdater {
    public:
        DMXSecUpdater();
        virtual ~DMXSecUpdater() {}
        void UpdateDMXSection();
        void SetCoupling(const float &coupling) {
            m_coupling = coupling;
            m_isSet = true;
        }
        void SetCrossSectionDir(const std::string &xSecDir) { m_xSecDir = xSecDir; }

    protected:
// Interesting do we still need this file? It seems  not to run anymore
#ifdef ROOTCORE_PACKAGE_DMXSecUtils
        DMXSecTool m_DMXSecTool;
#endif
        bool m_isSet;
        float m_coupling;
        std::string m_xSecDir;
    };
}  // namespace XAMPP

#endif
