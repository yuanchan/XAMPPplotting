#ifndef XAMPPPLOTTING_BKGESTIMATEHELPERS_H
#define XAMPPPLOTTING_BKGESTIMATEHELPERS_H

#include <TFile.h>
#include <XAMPPplotting/FinalPlotHelpers.h>
#include <XAMPPplotting/PlottingUtils.h>
#include <XAMPPplotting/SampleHisto.h>
#include <map>
#include <memory>
#include <string>
#include <vector>
class TH1;
class TAxis;

class RooAbsPdf;
class RooPlot;
class RooRealVar;
class RooHistPdf;
class RooDataHist;

namespace XAMPP {

    class MergeBin {
    public:
        MergeBin(const TH1* H, int glob);

        int get() const;
        double content() const;
        double error() const;
        double relunc() const;

        bool adjacent(const MergeBin&) const;
        double dR(const MergeBin& other) const;

        double combined_relunc(const MergeBin&) const;
        /// Combines the two bins and destroys the other
        void combine(std::shared_ptr<MergeBin>& other);
        bool is_good() const;

        double x_low() const;
        double x_high() const;

    private:
        const TH1* m_TH1;
        int m_N;
        double m_content;
        double m_error;

        double m_xlow;
        double m_xhigh;

        double m_ylow;
        double m_yhigh;

        bool m_is_good;
        std::vector<std::shared_ptr<MergeBin>> m_absorbed;
    };
    class HistogramRebinning {
    public:
        HistogramRebinning(const std::shared_ptr<TH1>& To_Rebin);
        HistogramRebinning(const TH1* To_Rebin);

        std::shared_ptr<TH1> rebin(double aimed_uncert) const;

        /// Rebinning methods w.r.t. the x-axis only.
        /// The y-axis remains untouched
        std::shared_ptr<TH1> rebin_x(double aimed_uncert) const;
        /// Rebinning methods w.r.t. the y-axis only
        /// The x-axis remains untouched
        std::shared_ptr<TH1> rebin_y(double aimed_uncert) const;

        /// retrieve the relative uncertainty of a single bin
        double get_relunc(const std::pair<int, int>& pair) const;
        double get_relunc(const int x, const int y) const;
        double get_relunc(const int bin) const;

        std::pair<int, int> make_pair(int glob_bin) const;

    private:
        std::shared_ptr<TH1> m_shared;
        const TH1* m_TH1;
        int m_max_bin;
        int m_max_bin_x;
        int m_max_bin_y;
        int m_max_bin_z;
    };

    class FractionFit {
    public:
        FractionFit(const std::shared_ptr<TH1>& data, const std::shared_ptr<TH1>& f1, const std::shared_ptr<TH1>& f2);

        bool fit(int max_trials = 10);

        double fraction_f1() const;
        double fraction_f1_error() const;

        std::shared_ptr<RooPlot> get_frame() const;
        bool is_valid() const;

        double integral() const;
        double chi2() const;
        unsigned int nBins() const;

        double evalute_fit(double x) const;

        std::shared_ptr<TH1> predicted() const;
        const std::shared_ptr<TH1> data() const;

    private:
        const std::shared_ptr<TH1> m_data;
        const std::shared_ptr<TH1> m_f1;
        const std::shared_ptr<TH1> m_f2;

        std::shared_ptr<TH1> m_predicted;

        std::shared_ptr<RooRealVar> m_var;
        std::shared_ptr<RooPlot> m_rooplot;

        std::shared_ptr<RooRealVar> m_fraction;

        std::shared_ptr<RooDataHist> m_data_hist;
        std::shared_ptr<RooDataHist> m_f1_hist;
        std::shared_ptr<RooDataHist> m_f2_hist;

        std::shared_ptr<RooHistPdf> m_f1_pdf;
        std::shared_ptr<RooHistPdf> m_f2_pdf;

        std::shared_ptr<RooAbsPdf> m_final_pdf;

        bool m_is_valid;
        double m_integral;
        double m_chi2;
    };
    class FractionSlicer {
    public:
        FractionSlicer(const SampleHisto* data, const SampleHisto* f1, const SampleHisto* f2, unsigned int axis);

        bool fit(const TH1* ref_binning = nullptr);

        std::shared_ptr<TH1> get_result() const;
        std::vector<std::shared_ptr<FractionFit>> get_fits() const;

    private:
        void truncate(std::shared_ptr<TH1>) const;
        int first_filled_bin(std::shared_ptr<TH1> h) const;
        int last_filled_bin(std::shared_ptr<TH1> h) const;
        std::shared_ptr<TH1> cut_edges(std::shared_ptr<TH1> h, unsigned int below_x, unsigned int above_x) const;
        std::shared_ptr<TH1> transform_to_binning(std::shared_ptr<TH1> from, std::shared_ptr<TH1> to) const;
        std::shared_ptr<TH1> transform_to_bin_numbers(std::shared_ptr<TH1> from) const;
        const SampleHisto* m_data;
        const SampleHisto* m_f1;
        const SampleHisto* m_f2;
        unsigned int m_axis;

        std::shared_ptr<TH1> m_final_results;
        std::vector<std::shared_ptr<FractionFit>> m_fits;
    };

}  // namespace XAMPP
#endif
