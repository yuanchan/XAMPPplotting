#ifndef XAMPPPlotting_HistFitterWeight_H
#define XAMPPPlotting_HistFitterWeight_H

#include <TTree.h>
#include <XAMPPplotting/Weight.h>
#include <iostream>
#include <set>
#include <string>
#include <vector>
namespace XAMPP {
    class HybridNormDataBase;

    class HistFitterWeight : public Weight {
    public:
        virtual ~HistFitterWeight() = default;
        static Weight *getWeighter(bool isData = false);

        virtual bool InitWeights() override;
        virtual bool init(TTree *tree) override;

    protected:
        std::vector<std::string> excluded_weights() const override;
        HistFitterWeight(bool isData);
        HistFitterWeight(const HistFitterWeight &) = delete;
        void operator=(const HistFitterWeight &) = delete;
    };

    class HybridWeight : public Weight {
    public:
        virtual ~HybridWeight() = default;
        bool isData() const override;
        int mcChannelNumber() const override;
        bool init(TTree *tree) override;
        bool InitWeights() override;

    private:
        friend HybridNormDataBase;
        static Weight *getWeighter(bool isData = false);

        HybridWeight(bool isData);
        HybridWeight(const HybridWeight &) = delete;
        void operator=(const HybridWeight &) = delete;

        bool m_isMC;
        bool m_current_isMC;
    };
    class HybridNormalizationWeight : public NormalizationWeight {
    public:
        static NormalizationWeight *getElement();

    protected:
        HybridNormalizationWeight();
        HybridNormalizationWeight(const HybridNormalizationWeight &) = delete;
        void operator=(const HybridNormalizationWeight &) = delete;
        virtual std::shared_ptr<NormalizationVariation> newVariation(const std::string &variation);
    };
    class HybdridNormalizationVariation : public NormalizationVariation {
    public:
        HybdridNormalizationVariation(const std::string &norm);
        double Read() override;

    private:
        Weight *m_weighter;
    };
}  // namespace XAMPP
#endif
