#ifndef XAMPPPLOTTING_FINALPLOTHELPERS_H
#define XAMPPPLOTTING_FINALPLOTHELPERS_H

#include <TFile.h>
#include <XAMPPplotting/PlottingUtils.h>
#include <map>
#include <memory>
#include <string>
#include <vector>
class TH1;
class TAxis;
namespace XAMPP {
    class PlotRegion;
    class PlotAnalysis;
    class FileStructure;
    class SystematicEnvelope;

    typedef std::shared_ptr<PlotRegion> PlotRegion_Ptr;
    typedef std::shared_ptr<PlotAnalysis> PlotAnalysis_Ptr;
    typedef std::shared_ptr<FileStructure> FileStructure_Ptr;
    class FileHandler {
    public:
        std::shared_ptr<TFile> LoadFile(const std::string& path);
        // Retrieves the file structure to a given file. If the file has not been opened
        // yet then it's opened
        FileStructure_Ptr GetFileStructure(const std::string& path);

        FileStructure_Ptr GetFileStructure(const std::shared_ptr<TFile>& File) const;
        FileStructure_Ptr GetFileStructure(const TFile* File) const;

        std::vector<std::shared_ptr<TFile>> OpenedFiles() const;

        std::vector<FileStructure_Ptr> FileStructures() const;

        static FileHandler* getInstance();
        static void reset();
        ~FileHandler();

        void closeAll();
        void switchOffMsg();
        bool showMsg() const;

        bool variable_known(const std::string& var) const;
        void new_variable(const std::string& var);

    private:
        static FileHandler* m_Inst;
        FileHandler();
        FileHandler(const FileHandler&) = delete;
        void operator=(const FileHandler&) = delete;

        std::vector<FileStructure_Ptr> m_opened_files;
        std::set<std::string> m_known_vars;
        bool m_showMessages;
    };

    ///  Helper class to define a group of systematics which are summarized in a single systematic
    ///  envelope, i.e. taking the maximum deviation from the nominal histogram
    ///   Delta x = max_{i} ( x_{i} - nominal)^{2} where x_{i} is the i-th systematic variation
    class SystematicEnvelope {
    public:
        SystematicEnvelope(const std::string& pattern, const std::string& env_name);
        SystematicEnvelope(const SystematicEnvelope&) = delete;
        void operator=(const SystematicEnvelope&) = delete;

        enum VariationSite { UpOnly = 1 << 0, DownOnly = 1 << 1, Any = UpOnly | DownOnly };

        bool consider_in_envelope(const std::string& systematic) const;
        /// Picks up all affecting systematics from the map and calculates the envelope from it. Picked entries are removed from the map
        virtual std::shared_ptr<TH1> calculate_syst(std::map<std::string, std::shared_ptr<TH1>>& syst_map,
                                                    std::shared_ptr<TH1> nominal) const;

        /// Pattern what the systematics have to satisfy in order to be considered
        std::string get_pattern() const;
        /// Name of the envelope group. If no name is defined in the constructor the pattern is returned
        std::string name() const;

        void append_systematic(const std::string& syst);

        void Print() const;

        void apply_only_to(unsigned int direction);
        unsigned int application_mask() const;

        bool add_to_up() const;
        bool add_to_down() const;

    protected:
        virtual void add_up(unsigned int bin, double diff, std::shared_ptr<TH1> syst) const;

    private:
        std::string m_pattern;
        std::string m_name;
        std::vector<std::string> m_syst;
        unsigned int m_site;
    };

    /// Group systematics together under a common name E.g. EG_CAlLIB + EG_EFFICIENCY --> EGAMMA
    class SystematicGroup : public SystematicEnvelope {
    public:
        SystematicGroup(const std::string& pattern, const std::string& env_name, unsigned int s);

    protected:
        void add_up(unsigned int bin, double diff, std::shared_ptr<TH1> syst) const override;
    };

    /// SystematicDeviation is a simple class to calculate the systematics in the following manner
    /// Delta = 1/N sqrt(sum_{i} (x_{i} - nominal)^{2})
    class SystematicDeviation : public SystematicGroup {
    public:
        SystematicDeviation(const std::string& pattern, const std::string& env_name, unsigned int s);
        std::shared_ptr<TH1> calculate_syst(std::map<std::string, std::shared_ptr<TH1>>& syst_map,
                                            std::shared_ptr<TH1> nominal) const override;
    };

    class SystematicPairer {
    public:
        static SystematicPairer* getInstance();

        void append_systematic(const std::string& name);
        void exclude_systematic(const std::string& syst);
        void do_systematics(bool B);

        bool create_envelope(const std::string& grp_pattern, const std::string& env_name = "");
        bool create_group(const std::string& grp_pattern, const std::string& grp_name, unsigned int sel_only = SystematicGroup::Any);
        bool create_deviation(const std::string& grp_pattern, const std::string& grp_name, unsigned int seld_only = SystematicGroup::Any);

        bool pair_systematics(const std::string& down, const std::string& up);

        bool is_systematic_known(const std::string& syst) const;
        bool is_systematic_excluded(const std::string& syst) const;
        bool is_systematic_up(const std::string& syst) const;
        bool is_systematic_down(const std::string& syst) const;
        bool is_systematic_envelope(const std::string& syst) const;
        bool do_systematics() const;
        //
        std::string get_paired(const std::string& syst) const;
        std::vector<std::shared_ptr<SystematicEnvelope>> get_envelopes() const;
        std::shared_ptr<SystematicEnvelope> find_envelope(const std::string& grp_name) const;
        const std::vector<std::string>& get_systematics() const;

        static const std::vector<std::string>& up_variation_endings();
        static const std::vector<std::string>& down_variation_endings();
        ~SystematicPairer();

        void Print() const;

        std::string get_syst_title(const std::string& syst_name) const;
        void set_systematic_title(const std::string& syst_name, const std::string& syst_title);

    private:
        static SystematicPairer* m_Inst;
        SystematicPairer();
        SystematicPairer(const SystematicPairer&) = delete;
        void operator=(const SystematicPairer&) = delete;

        // List which contains all known systematics
        std::vector<std::string> m_known_syst;
        std::vector<std::string> m_excluded_syst;
        // next category paired systematics
        typedef std::pair<std::string, std::string> VariationPair;
        std::vector<VariationPair> m_paired_syst;
        std::vector<std::shared_ptr<SystematicEnvelope>> m_envelope_grps;

        std::vector<size_t> m_syst_up_hashes;
        std::vector<size_t> m_syst_dn_hashes;

        std::map<std::string, std::string> m_syst_titles;

        bool m_doSyst;
    };

    // The XMAPPplotting histogram files are built in the following way
    //  <Analysis>_<Systematic>/<Region>/
    //       -- <First_Variable>
    //       -- <Second_Variable>
    //       -- <etc>

    class PlotRegion {
    public:
        PlotRegion(TDirectory* File_Dir);
        PlotRegion(const PlotRegion&) = delete;
        void operator=(const PlotRegion&) = delete;

        const std::vector<std::string>& variables() const;
        std::string name() const;

    private:
        std::string m_region_name;
        std::vector<std::string> m_variables;
    };
    class PlotAnalysis {
    public:
        PlotAnalysis(const FileStructure* ref_structure, const std::string& ana_name);
        PlotAnalysis(const PlotAnalysis&) = delete;
        void operator=(const PlotAnalysis&) = delete;

        std::string name() const;
        std::string nominal() const;

        std::vector<std::string> get_systematics() const;
        std::vector<PlotRegion_Ptr> get_regions() const;
        PlotRegion_Ptr get_region(const std::string& reg_name) const;
        std::vector<std::string> get_region_names() const;
        std::vector<std::string> get_variables(const std::string& region_name) const;

        // I/O stuff
        const FileStructure* get_reference() const;
        std::shared_ptr<TFile> get_file() const;
        void setNominal(const std::string& syst);

    private:
        void update_regions();
        const FileStructure* m_reference;
        std::string m_name;
        std::string m_nominal;
        std::vector<std::string> m_systematics;
        std::vector<PlotRegion_Ptr> m_regions;
    };

    class FileStructure {
    public:
        FileStructure(std::shared_ptr<TFile> ROOT_File);
        FileStructure(const FileStructure&) = delete;
        void operator=(const FileStructure&) = delete;
        std::shared_ptr<TFile> reference() const;

        std::vector<PlotAnalysis_Ptr> get_analyses() const;
        PlotAnalysis_Ptr get_analysis(const std::string& ana_name) const;
        std::vector<std::string> get_analyses_names() const;
        std::vector<std::string> get_regions(const std::string& ana_name) const;
        std::vector<std::string> get_variables(const std::string& ana_name, const std::string& region) const;

    private:
        void append_analysis(std::string& name);
        std::shared_ptr<TFile> m_reference_file;
        std::vector<PlotAnalysis_Ptr> m_analyses;
    };

}  // namespace XAMPP
#endif
