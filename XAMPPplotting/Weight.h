#ifndef XAMPPPLotting_WEIGHTCLASS_H
#define XAMPPPLotting_WEIGHTCLASS_H

#include <AsgTools/ToolHandle.h>
#include <TTree.h>

#include <PATInterfaces/SystematicSet.h>

#include <XAMPPplotting/EventService.h>
#include <XAMPPplotting/MetaDataTreeReader.h>
#include <XAMPPplotting/TreeVarReader.h>

#include <xAODEventInfo/EventInfo.h>
#include <xAODEventInfo/EventInfoAuxContainer.h>
#include <xAODEventInfo/EventInfoContainer.h>

#include <string>
#include <vector>
namespace CP {
    class PileupReweightingTool;
    class SystematicSet;
}  // namespace CP

namespace SUSY {
    class CrossSectionDB;
}
namespace XAMPP {
    class PseudoScalarVarReader;
    class MonteCarloPeriodHandler;
    class NormalizationWeight;
    class Weight;

    // This class is the standard interface how the weights should be read out of the tree including the handling of all the
    // systematic uncertainties
    class IWeightElement {
    public:
        enum SystStatus { Affected, NotAffected, SystError };
        virtual bool init(TTree* tree) = 0;
        virtual double read() = 0;
        virtual std::string name() const = 0;

        // For a given Weight there may be additional systematic variations on that weight. In the XAMPP Ntuples all the variations
        // are encoded by <WeightName>_<VariationName>. This function scans through the input tree to register all variations for later use
        virtual std::vector<std::string> FindWeightVariations(TTree* Tree) = 0;
        // If the code should loop over all systematic variations, in the case of  weight systematics the nominal weight has to be replaced.
        // AS an input one element of the registered Variations is given. Usually the function returns Affected or Not as a Status.
        // The SystError is forseen for cases where a non-standard weight is implemented and something went wrong during the loading
        // of the systematic
        virtual IWeightElement::SystStatus ApplySystematic(const std::string& variation) = 0;
        virtual void ResetSystematic() = 0;
        // Returns the vector of variations which were registered during the FindWeightVariations() method-call
        virtual std::vector<std::string> GetWeightVariations() = 0;
        // XAMPPplotting may also write HistFitter trees. To connect the output branch with the input the ITreeVarReader for the current
        // loaded variation can be retrieved.
        virtual ITreeVarReader* GetTreeVarReader() = 0;
        virtual ITreeVarReader* GetSystematicReader(const std::string& Syst = "") = 0;
        virtual ~IWeightElement() = default;
        // Non-Standard weights which are not written out of the tree may register variations. If HisFitter trees are written the values of
        // all registered Weight variations must be present simultaneously which is achieved via PSeudoScalarVarReaders. This options sets
        // whether the Non-Standard weight sets pipes all the systematic uncertainties to the PseudoScalarVarReaders.
        static void FillSystSimul(bool B);
        static bool SimultaneousSyst();

    private:
        static bool m_FillSystSimul;
    };
    class WeightSystematic : public IKinematicSyst {
    public:
        WeightSystematic(const std::string& syst);
        virtual std::string syst_name() const override;
        virtual std::string tree_name() const override;
        virtual bool is_nominal() const override;
        virtual bool apply_syst() override;
        virtual void reset() override;

    private:
        std::string m_syst_name;
        Weight* m_weighter;
        EventService* m_service;
    };

    // The Weighter class is the manager of all the particular EventWeights and calculates the total Weight ones per event. It distributes
    // the total weight then to all XAMPP histograms.
    class Weight {
    public:
        // The Weighter is written in the Singleton-pattern. This function returns its instances and creates one if it has not been called
        // before. The isData argument defines whether the input files are Data/MC affecting the calculation of the event-weights
        static Weight* getWeighter(bool isData = false);
        virtual bool isData() const;

        // This is *NOT* the event number saved as branch in the ntuples!!!
        unsigned int eventNumber() const;
        // Run number either from the  pile-up tool or the ntuple itself
        unsigned int runNumber() const;
        // mc channel number
        virtual int mcChannelNumber() const;
        // The SUSY final state encodes the production mode for the Supersymmetric particles
        // (c.f. https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/SUSYSignalUncertainties)
        // For background it is 0.
        int SUSYFinalState() const;

        // the processID combines the LHE variations with the SUSYFinalState if the latter is used
        int processID() const;

        // The NewEvent method is called once per event calculating the weights
        virtual void NewEvent();
        // This function initializes the weights to the current input tree
        virtual bool init(TTree* tree);

        virtual bool InitWeights();

        // Return of the event weight (squared)
        inline double GetWeight() const { return m_weight; }
        inline double GetWeightSquared() const { return m_weight2; }

        // Weights to be applied on MC (Data) input...
        // Usually from the XAMPPbase ntuples no weights are stored for data
        // One possible data weight might be the FF of the matrix method or
        // the trigger unprescaling..
        void UseWeights(const std::vector<std::string>& W_Names);
        void UseDataWeights(const std::vector<std::string>& W_Names);

        const std::vector<std::string>& WeightNames() const;
        const std::vector<std::string>& DataWeightNames() const;

        const std::vector<IWeightElement*>& GetUsedWeighters() const;

        void SetLumi(double Lumi);

        xAOD::EventInfo* GetInfo() const;
        void SetupEventInfo(bool B = true);

        // If the x-Section is not applied the sample is normalized to 1.e3
        void ApplyXSection(bool B = true);
        bool applyXSection() const;

        // SUSY samples may contain many different production modes
        // The correct way of normalization would be to correct to each particular mode
        // However, therefore the total number of events had to be accessible during ntuple production
        void NormProcesses(bool B = true);
        bool normToProdProcess() const;
        /// LHE weights are there to evaluate the uncertainties on the theory
        /// parameters like the renormalization or the factorization scale or
        /// the choice of the PDF set. Each of the variation comes a long with its
        /// own sum of weights saved in the XAMPP metaData tree with processIDs beyond the 1k
        ///  There are two different ways to handle the uncertainties
        ///     1) Evaluate the effect of the LHE uncertainties on the acceptance only (DEFAULT).
        ///        Each of the LHE variations is normalized to its respective sum of weights.
        ///        This approach is usually taken if the value of the cross-section is taken from
        ///        external calculations.
        ///     2) Evaluate the combined effect of the LHE uncertainties on the cross-section and the
        ///        cross-section: Each LHE variation is normalized w.r.t. the nominal sum of weights
        enum LHENormalization {
            AcceptanceOnly = 0,
            CrossSectionAndAcceptance

        };
        int LHEnormMode() const;
        void setLHEnormMode(LHENormalization N);

        /// Application of the pile up weight
        void ApplyPRW(bool B = true);
        bool applyPRW() const;

        // Systematic Handling
        std::vector<std::string> FindWeightVariations(TTree* T);
        bool ApplySystematic(const std::string& Syst);
        void ResetSystematic();

        // Managing functions of particular EventWeights
        bool RegisterWeightElement(IWeightElement* W);
        bool RegisterWeightElement(std::shared_ptr<IWeightElement> W);

        IWeightElement* GetWeightElement(const std::string& Element) const;

        void excludeSystematic(const std::string& Syst);
        void excludeSystematic(const std::vector<std::string>& Syst);
        const std::vector<std::string> excludedSystematics() const;

        void UpdateEventInfo();

        virtual ~Weight();

    protected:
        static Weight* m_DB;
        Weight(bool isData = false);
        Weight(const Weight&) = delete;
        void operator=(const Weight&) = delete;

        void AppendWeight(IWeightElement* R);

        virtual std::vector<std::string> excluded_weights() const;

    private:
        bool IsWeightAlreadyUsed(IWeightElement* R) const;
        void LoadWeightElements(std::vector<std::string>& Weights);

        bool m_isData;

        bool m_ApplyXS;        // Apply the cross-section
        bool m_UpdateXsec;     // Update the cross-section database
        bool m_UseSUSYstates;  // Normalize the MC to each final-state

        bool m_CreateInfo;
        bool m_ApplyPRW;

        double m_weight;
        double m_weight2;

        XAMPP::NormalizationDataBase* m_MetaData;

        std::map<std::string, std::shared_ptr<IWeightElement>> m_RegisteredWeighters;

        ScalarVarReader<int>* m_MCchannel;
        ScalarVarReader<int>* m_FinalState;
        std::vector<IWeightElement*> m_WeightReader;
        std::vector<std::string> m_UseWeights;
        std::vector<std::string> m_UseDataWeights;
        std::vector<std::string> m_excludedSystematics;

        unsigned int m_lastEvUpdate;
        xAOD::EventInfo* m_eventInfo;
        std::unique_ptr<xAOD::EventInfoAuxContainer> m_eventInfoAux;
        std::unique_ptr<xAOD::EventInfoContainer> m_eventInfoCont;

        ScalarVarReader<float>* m_mu_actual_reader;
        ScalarVarReader<float>* m_mu_reader;
        ScalarVarReader<unsigned int>* m_RunNumber;
        ScalarVarReader<unsigned int>* m_LumiBlock;
        ScalarVarReader<unsigned long long>* m_evNumber;

        // Normalize the sample to an appropiate LHE weight
        NormalizationWeight* m_GenWeight;
        // Event number
        unsigned int m_EventNumber;
        LHENormalization m_LHENorm;
    };

    //##############################################
    //      Standard class to retrieve the weights from the
    //      TTree objects.
    //##############################################
    class WeightElementFromTree : public IWeightElement {
    public:
        virtual bool init(TTree* tree);
        virtual double read();
        virtual std::vector<std::string> FindWeightVariations(TTree* Tree);
        virtual IWeightElement::SystStatus ApplySystematic(const std::string& variation);
        virtual void ResetSystematic();
        virtual std::string name() const;

        virtual ITreeVarReader* GetTreeVarReader();
        virtual ITreeVarReader* GetSystematicReader(const std::string& Syst);
        virtual std::vector<std::string> GetWeightVariations();

        static WeightElementFromTree* GetWeighter(const std::string& Name);
        virtual ~WeightElementFromTree();

    protected:
        WeightElementFromTree(const std::string& Weight);
        WeightElementFromTree();
        WeightElementFromTree(const WeightElementFromTree&) = delete;
        void operator=(const WeightElementFromTree&) = delete;
        virtual ITreeVarReader* LoadReader(const std::string& syst = "");

        std::string m_Name;
        ITreeVarReader* m_Weighter;
        std::vector<std::string> m_Systematics;
        bool m_init;
        bool m_Registered;
        EventService* m_eventService;
    };

    class FloatWeightFromTree : public WeightElementFromTree {
    public:
        static FloatWeightFromTree* GetWeighter(const std::string& Name);
        virtual ~FloatWeightFromTree();

    protected:
        FloatWeightFromTree(const std::string& Weight);
        FloatWeightFromTree(const FloatWeightFromTree&) = delete;
        void operator=(const FloatWeightFromTree&) = delete;
        virtual ITreeVarReader* LoadReader(const std::string& syst = "");
    };

    class PileUpWeightElement : public IWeightElement {
    public:
        enum LumiPeriod {
            data15_16 = 1 << 0,
            data17 = 1 << 1,
            data18 = 1 << 2,
            All = data15_16 | data17 | data18,
            data15_17 = data15_16 | data17,
            data17_18 = data17 | data18,
            data15_18 = data15_16 | data18,
        };
        virtual bool init(TTree* tree);
        virtual double read();
        virtual std::vector<std::string> FindWeightVariations(TTree* Tree);
        virtual IWeightElement::SystStatus ApplySystematic(const std::string& variation);
        virtual void ResetSystematic();
        virtual std::string name() const;

        virtual ITreeVarReader* GetTreeVarReader();
        virtual ITreeVarReader* GetSystematicReader(const std::string& Syst);

        virtual std::vector<std::string> GetWeightVariations();

        virtual ~PileUpWeightElement() {}
        static PileUpWeightElement* getElement();
        void reWeightToPeriod(PileUpWeightElement::LumiPeriod P);

        double getPeriodWeight() const;

    protected:
        PileUpWeightElement();
        PileUpWeightElement(const PileUpWeightElement&) = delete;
        void operator=(const PileUpWeightElement&) = delete;

        Weight* m_Weighter;
        XAMPP::NormalizationDataBase* m_MetaData;

    private:
        void loadSystematics(const std::vector<std::string>& systs);
        WeightElementFromTree* m_TreePRW;
        struct FinalPRWWeight {
            PseudoScalarVarReader* CombinedReader;
            ITreeVarReader* prwReader;
            FinalPRWWeight(PseudoScalarVarReader* C, ITreeVarReader* P) {
                CombinedReader = C;
                prwReader = P;
            }
        };
        std::vector<FinalPRWWeight> m_variationReaders;
        std::vector<FinalPRWWeight>::const_iterator m_current_syst;
        LumiPeriod m_periodMode;
        mutable std::shared_ptr<MonteCarloPeriodHandler> m_current_mc;
        std::vector<unsigned int> m_allowed_periods;
    };

    //
    //  Classes to handle the normalization & its variations properly
    //
    class NormalizationVariation {
    public:
        NormalizationVariation(const std::string& norm);
        virtual ~NormalizationVariation();
        virtual double Read();
        ITreeVarReader* GetReader() const;
        int LHEvariation() const;
        int processID() const;

    private:
        int m_LHE;
        PseudoScalarVarReader* m_PseudoReader;
        ITreeVarReader* m_eventWeight;

        int m_DSID;
        int m_FinalState;

        NormalizationDataBase* m_MetaData;
        Weight* m_weighter;

        std::vector<int> m_known_FS;
        double m_NormCache;

        PseudoScalarVarReader* m_xSection;
        PseudoScalarVarReader* m_xSectUncert;
    };

    class NormalizationWeight : public IWeightElement {
    public:
        virtual bool init(TTree* tree);
        virtual double read();
        virtual std::vector<std::string> FindWeightVariations(TTree* Tree);
        virtual IWeightElement::SystStatus ApplySystematic(const std::string& variation);
        virtual void ResetSystematic();
        virtual std::string name() const;

        virtual ITreeVarReader* GetTreeVarReader();
        virtual ITreeVarReader* GetSystematicReader(const std::string& Syst);
        virtual std::vector<std::string> GetWeightVariations();

        virtual int processID() const;
        static NormalizationWeight* getElement();

    protected:
        NormalizationWeight();
        NormalizationWeight(const NormalizationWeight&) = delete;
        void operator=(const NormalizationWeight&) = delete;
        virtual std::shared_ptr<NormalizationVariation> newVariation(const std::string& variation);

    private:
        std::shared_ptr<NormalizationVariation> getVariation(const std::string& var) const;
        std::shared_ptr<NormalizationVariation> getVariation(int L) const;
        void checkNorm();

        IWeightElement* m_eventWeight;
        Weight* m_weighter;

        std::vector<std::shared_ptr<NormalizationVariation>> m_LHE;
        std::shared_ptr<NormalizationVariation> m_Norm;
    };
}  // namespace XAMPP
#endif  // WEIGHTCLASS_H
