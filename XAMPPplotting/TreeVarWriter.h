#ifndef XAMPPPLOTTING_TREEVARWRITER_H
#define XAMPPPLOTTING_TREEVARWRITER_H
#include <XAMPPplotting/TreeVarReader.h>

#include <TLorentzVector.h>
#include <TTree.h>
#include <fstream>
#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <vector>
namespace XAMPP {
    class ITreeVarWriter {
    public:
        virtual bool init(TTree* t);
        virtual bool fill() = 0;

        std::string name() const;
        virtual ~ITreeVarWriter() = default;

    protected:
        ITreeVarWriter(const std::string& Name, ITreeVarReader* Reader);
        template <typename T> bool AddBranch(std::string Name, T& Element);
        std::string RemoveProblematicStr(const std::string& str) const;

        std::string m_Name;
        bool m_Registered;
        TTree* m_tree;
        ITreeVarReader* m_Reader;
    };
    class ITreeVarWriterStorage {
    public:
        static ITreeVarWriterStorage* GetInstance();
        bool Register(ITreeVarWriter* R);
        ITreeVarWriter* GetWriter(const std::string& Name);
        virtual ~ITreeVarWriterStorage();

    protected:
        static ITreeVarWriterStorage* m_Inst;
        ITreeVarWriterStorage();
        ITreeVarWriterStorage(const ITreeVarWriterStorage&) = delete;
        void operator=(const ITreeVarWriterStorage&) = delete;

    private:
        std::map<std::string, std::shared_ptr<ITreeVarWriter>> m_DB;
    };
    template <class T> class ScalarTreeVarWriter : public ITreeVarWriter {
    public:
        virtual ~ScalarTreeVarWriter() = default;
        virtual bool init(TTree* t);
        virtual bool fill();
        static ScalarTreeVarWriter<T>* GetWriter(std::string varname, ITreeVarReader* R = nullptr);

    private:
        ScalarTreeVarWriter(const std::string& Name, ITreeVarReader* R);
        ScalarTreeVarWriter(const ScalarTreeVarWriter&) = delete;
        void operator=(const ScalarTreeVarWriter&) = delete;
        T m_Var;
    };
    template <class T> class VectorTreeVarWriter : public ITreeVarWriter {
    public:
        virtual bool init(TTree* t);
        virtual bool fill();
        static VectorTreeVarWriter<T>* GetWriter(const std::string& varname, ITreeVarReader* R = nullptr);

    private:
        VectorTreeVarWriter(const std::string& Name, ITreeVarReader* R);
        VectorTreeVarWriter(const VectorTreeVarWriter&) = delete;
        void operator=(const VectorTreeVarWriter&) = delete;
        std::vector<T> m_Var;
    };
}  // namespace XAMPP
#include <XAMPPplotting/TreeVarWriter.ixx>
#endif  // TREEVARWRITER_H
