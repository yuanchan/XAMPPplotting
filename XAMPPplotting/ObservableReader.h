#ifndef XAMPPPLOTTING_OBSERVABLEREADER_H
#define XAMPPPLOTTING_OBSERVABLEREADER_H
#include <XAMPPplotting/TreeVarReader.h>

#include <cmath>
#include <fstream>
#include <functional>
#include <map>
#include <set>
#include <sstream>

namespace XAMPP {
    const float M_Z = 91.2;

    class Weight;
    class DiParticleReader;
    class DiParticleVarReader;
    class DiParticleComponentReader;

    class PeriodReader : public IScalarReader {
    public:
        static ITreeVarReader* GetReader();
        virtual double read() const;
        virtual std::string name() const;

        virtual bool init(TTree* t);
        virtual ~PeriodReader() {}

    protected:
        PeriodReader();
        PeriodReader(const PeriodReader&) = delete;
        void operator=(const PeriodReader&) = delete;

        unsigned int GetRndRunNumber() const;
        ITreeVarReader* m_Reader;
        bool m_Registered;
        bool m_UsePRWTool;
    };

    class DataLumiReader : public IScalarReader {
    public:
        static ITreeVarReader* GetReader();
        virtual double read() const;
        virtual std::string name() const;

        virtual bool init(TTree* t);
        virtual ~DataLumiReader() = default;

    protected:
        DataLumiReader();
        DataLumiReader(const DataLumiReader&) = delete;
        void operator=(const DataLumiReader&) = delete;
        unsigned int GetRandomLumiBlock() const;
        ITreeVarReader* m_Reader;
        bool m_Registered;
        bool m_UsePRWTool;
    };

    class ParticleCorrelationReader : public IMatrixReader {
    public:
        double readMatrixEntry(const size_t i, const size_t j) const override;
        size_t row_entries(size_t i) const override;
        size_t num_rows() const override;

        bool init(TTree* t) override;

        virtual ~ParticleCorrelationReader() = default;
        inline IParticleReader* First() const { return m_FirstParticle; }
        inline IParticleReader* Second() const { return m_SecondParticle; }
        // Add DeltaPhi & DeltaEta since they're used downstream
        // by the DeltaEta/DeltaR/DeltaPhi Readers
        double DeltaPhi(size_t I, size_t J) const;
        double DeltaEta(size_t I, size_t J) const;
        double DeltaY(size_t I, size_t J) const;

        bool PointsToSameObject(size_t first, size_t second) const;

    protected:
        ParticleCorrelationReader(const std::string& First, const std::string& Second, std::function<double(size_t, size_t)> corr_fkt);
        ParticleCorrelationReader(const ParticleCorrelationReader&) = delete;
        void operator=(const ParticleCorrelationReader&) = delete;
        void Register();

    private:
        IParticleReader* m_FirstParticle;
        IParticleReader* m_SecondParticle;

        std::function<double(size_t, size_t)> m_CorrelFct;

    protected:
        IParticleVarReader* m_FirstPt;
        IParticleVarReader* m_SecondPt;

        IParticleVarReader* m_FirstEta;
        IParticleVarReader* m_SecondEta;

        IParticleVarReader* m_FirstPhi;
        IParticleVarReader* m_SecondPhi;
    };

    class InvariantDiMassReader : public ParticleCorrelationReader {
    public:
        static ParticleCorrelationReader* GetReader(const std::string& FirstParticle, const std::string& SecondParticle);
        virtual std::string name() const;

    protected:
        InvariantDiMassReader(const std::string& FirstParticle, const std::string& SecondParticle);
        InvariantDiMassReader(const InvariantDiMassReader&) = delete;
        void operator=(const InvariantDiMassReader&) = delete;
    };
    class InvariantDiPtReader : public ParticleCorrelationReader {
    public:
        static ParticleCorrelationReader* GetReader(const std::string& FirstParticle, const std::string& SecondParticle);
        virtual std::string name() const;

    protected:
        InvariantDiPtReader(const std::string& FirstParticle, const std::string& SecondParticle);
        InvariantDiPtReader(const InvariantDiPtReader&) = delete;
        void operator=(const InvariantDiPtReader&) = delete;
    };
    class TransverseMassReader : public ParticleCorrelationReader {
    public:
        static ParticleCorrelationReader* GetReader(const std::string& FirstParticle, const std::string& SecondParticle);
        virtual std::string name() const;
        virtual ~TransverseMassReader() {}

    protected:
        TransverseMassReader(const std::string& FirstParticle, const std::string& SecondParticle);
        TransverseMassReader(const TransverseMassReader&) = delete;
        void operator=(const TransverseMassReader&) = delete;
    };
    class MomentumImbalanceReader : public ParticleCorrelationReader {
    public:
        static ParticleCorrelationReader* GetReader(const std::string& FirstParticle, const std::string& SecondParticle);
        virtual std::string name() const;

    protected:
        MomentumImbalanceReader(const std::string& FirstParticle, const std::string& SecondParticle);
        MomentumImbalanceReader(const MomentumImbalanceReader&) = delete;
        void operator=(const MomentumImbalanceReader&) = delete;
    };
    class DeltaYReader : public ParticleCorrelationReader {
    public:
        virtual std::string name() const;
        static ParticleCorrelationReader* GetReader(const std::string& FirstParticle, const std::string& SecondParticle);

    protected:
        DeltaYReader(const std::string& FirstParticle, const std::string& SecondParticle);
        DeltaYReader(const DeltaYReader&) = delete;
        void operator=(const DeltaYReader&) = delete;
    };
    class DeltaRReader : public ParticleCorrelationReader {
    public:
        virtual std::string name() const;
        static ParticleCorrelationReader* GetReader(const std::string& FirstParticle, const std::string& SecondParticle);

    protected:
        DeltaRReader(const std::string& FirstParticle, const std::string& SecondParticle);
        DeltaRReader(const DeltaRReader&) = delete;
        void operator=(const DeltaRReader&) = delete;
    };
    /// Returns the deltaR using
    /// the rapidity of an object
    class DeltaRReaderY : public ParticleCorrelationReader {
    public:
        std::string name() const override;
        static ParticleCorrelationReader* GetReader(const std::string& FirstParticle, const std::string& SecondParticle);

    protected:
        DeltaRReaderY(const std::string& FirstParticle, const std::string& SecondParticle);
        DeltaRReaderY(const DeltaRReaderY&) = delete;
        void operator=(const DeltaRReaderY&) = delete;
    };
    class DeltaPhiReader : public ParticleCorrelationReader {
    public:
        virtual std::string name() const;
        static ParticleCorrelationReader* GetReader(const std::string& FirstParticle, const std::string& SecondParticle);

    protected:
        DeltaPhiReader(const std::string& FirstParticle, const std::string& SecondParticle);
        DeltaPhiReader(const DeltaPhiReader&) = delete;
        void operator=(const DeltaPhiReader&) = delete;
    };
    class DeltaEtaReader : public ParticleCorrelationReader {
    public:
        virtual std::string name() const;
        static ParticleCorrelationReader* GetReader(const std::string& FirstParticle, const std::string& SecondParticle);

    protected:
        DeltaEtaReader(const std::string& FirstParticle, const std::string& SecondParticle);
        DeltaEtaReader(const DeltaEtaReader&) = delete;
        void operator=(const DeltaEtaReader&) = delete;
    };
    /// The contratransverse mass is defined as:
    ///         m_{CT}^{2} = 2*p_{T}^{a}*p_{T}^{b}* (1 + cos (detla phi))
    ///
    class ConTransverseMassReader : public ParticleCorrelationReader {
    public:
        virtual std::string name() const;
        static ParticleCorrelationReader* GetReader(const std::string& FirstParticle, const std::string& SecondParticle);

    protected:
        ConTransverseMassReader(const std::string& FirstParticle, const std::string& SecondParticle);
        ConTransverseMassReader(const ConTransverseMassReader&) = delete;
        void operator=(const ConTransverseMassReader&) = delete;
    };

    ///
    ///     Calculates either maximum or the minimum particle correlation
    ///     between two where the first particle is taken as reference
    ///     If the second container is empty either FLT_MAX or -FLT_MAX is returned
    class ExtremumCorrelationReader : public IParticleVariable {
    public:
        static IParticleVariable* GetReader(ParticleCorrelationReader* in, bool minimum = true);

        std::string name() const override;
        bool init(TTree* t) override;
        double readEntry(size_t i) const override;

    private:
        ExtremumCorrelationReader(ParticleCorrelationReader* in, bool minimum);
        ExtremumCorrelationReader(const ExtremumCorrelationReader&) = delete;
        void operator=(const ExtremumCorrelationReader&) = delete;
        ParticleCorrelationReader* m_corr_reader;
        bool m_search_min;
    };

    class ExtremumVarReader : public IScalarReader {
    public:
        virtual double read() const;
        virtual bool init(TTree* t);
        virtual std::string name() const;
        virtual ~ExtremumVarReader() {}

    protected:
        virtual bool IsExtremum(double Current, double Test) const = 0;
        virtual double GetInitial() const = 0;
        ExtremumVarReader(const std::string& Name, const std::vector<ITreeVarReader*>& Readers);

        std::string m_Name;
        mutable double m_Ext;
        mutable unsigned int m_EventNumber;
        std::vector<ITreeVarReader*> m_Readers;
    };
    class MaximumVarReader : public ExtremumVarReader {
    public:
        virtual ~MaximumVarReader() {}
        static MaximumVarReader* GetReader(const std::string& Name, const std::vector<ITreeVarReader*>& Readers);

    protected:
        virtual bool IsExtremum(double Current, double Test) const;
        virtual double GetInitial() const;
        MaximumVarReader(const std::string& Name, const std::vector<ITreeVarReader*>& Readers);
    };
    class MinimumVarReader : public ExtremumVarReader {
    public:
        static MinimumVarReader* GetReader(const std::string& Name, const std::vector<ITreeVarReader*>& Readers);
        virtual ~MinimumVarReader() {}

    protected:
        MinimumVarReader(const std::string& Name, const std::vector<ITreeVarReader*>& Readers);
        virtual bool IsExtremum(double Current, double Test) const;
        virtual double GetInitial() const;
    };

    class SumUpReader : public IScalarReader {
    public:
        virtual double read() const;
        virtual bool init(TTree* t);
        virtual std::string name() const;
        static SumUpReader* GetReader(const std::string& Name, ITreeVarReader* FirstReader = nullptr,
                                      ITreeVarReader* SecondReader = nullptr);
        static SumUpReader* GetReader(const std::string& Name, const std::vector<ITreeVarReader*>& Sums);

    protected:
        SumUpReader(const std::string& Name, const std::vector<ITreeVarReader*>& Sums);
        double SumUp(ITreeVarReader* R) const;
        std::string m_Name;
        std::vector<ITreeVarReader*> m_Sums;
        mutable unsigned int m_EventNumber;
        mutable double m_Cache;
    };
    class PileUpReader : public IScalarReader {
    public:
        virtual double read() const;
        virtual bool init(TTree* t);
        virtual std::string name() const;

    protected:
        PileUpReader();
        PileUpReader(const PileUpReader&) = delete;
        void operator=(const PileUpReader&) = delete;

        ITreeVarReader* m_Reader;
        Weight* m_Weight;
        bool m_UsePRWTool;
    };
    class DeltaPhiToMetReader : public IParticleVariable {
    public:
        virtual double readEntry(size_t I) const;
        virtual std::string name() const;
        virtual bool init(TTree* t);
        static IParticleVarReader* GetReader(const std::string& Particle, const std::string& Met);

    protected:
        DeltaPhiToMetReader(const std::string& Particle, const std::string& Met);
        DeltaPhiToMetReader(const DeltaPhiToMetReader&) = delete;
        void operator=(const DeltaPhiToMetReader&) = delete;
        std::string m_Met;
        IParticleVarReader* m_Part_phi;
        ITreeVarReader* m_Met_phi;
        bool m_Registered;
    };

    class L1CaloReader : public IParticleVariable {
    public:
        virtual double readEntry(size_t I) const;
        virtual std::string name() const;
        virtual bool init(TTree* t);
        static IParticleVarReader* GetReader(const std::string& Name);

    protected:
        L1CaloReader(ParticleReader* Jets);
        L1CaloReader(const L1CaloReader&) = delete;
        void operator=(const L1CaloReader&) = delete;
        IParticleVarReader* m_EMFrac;
        IParticleVarReader* m_HECFrac;
    };
    class MtMetReader : public IParticleVariable {
    public:
        virtual double readEntry(size_t I) const;
        virtual std::string name() const;

        virtual bool init(TTree* t);
        static IParticleVarReader* GetReader(const std::string& Particle, const std::string& Met);

    protected:
        MtMetReader(const std::string& Particle, const std::string& Met);
        MtMetReader(const MtMetReader&) = delete;
        void operator=(const MtMetReader&) = delete;

        IParticleVarReader* m_par_pt;
        IParticleVarReader* m_par_phi;
        std::string m_met;
        ITreeVarReader* m_met_value;
        ITreeVarReader* m_met_phi;
        bool m_Registered;
    };

}  // namespace XAMPP
#endif  // OBSERVABLEREADER_H
