#ifndef XAMPPPLOTTING_ANALYSIS_H
#define XAMPPPLOTTING_ANALYSIS_H
#include <TFile.h>
#include <TStopwatch.h>
#include <TTree.h>
#include <XAMPPplotting/EventService.h>
#include <map>
#include <memory>
#include <string>
#include <vector>
namespace XAMPP {
    class Weight;
    class NormalizationDataBase;
    class Histo;
    class ITreeVarReader;
    class Condition;

    class TreeSystematic : public IKinematicSyst {
    public:
        virtual std::string syst_name() const;
        virtual std::string tree_name() const;
        virtual bool is_nominal() const;
        virtual bool apply_syst();
        virtual void reset();
        TreeSystematic(const std::string &tree_name, bool is_nominal = false);

    private:
        std::string m_tree_name;
        bool m_is_nominal;
    };

    /**
     * @brief      Class for analysis.
     */
    class Analysis {
    public:
        Analysis();
        virtual ~Analysis();

        /**
         * @brief      Process the loop on the events
         *
         * @param[in]  nMax   The maximum
         * @param[in]  nSkip  The skip
         *
         * @return     { description_of_the_return_value }
         */
        virtual bool Process(long int nMax = -1, long int nSkip = 0);

        /**
         * @brief      Set the name of the analysis
         *
         * @param[in]  N     { parameter_description }
         */
        void SetName(const std::string &N);

        /**
         * @brief      Sets the name of the region in your analysis
         *
         * @param[in]  Region  The region
         */
        void SetRegion(const std::string &Region);

        /**
         * @brief      Get the name of the analysis
         *
         * @return     { description_of_the_return_value }
         */
        std::string name() const;

        /**
         * @brief      Sets the tree name.
         *
         * @param[in]  Name  The name
         */
        void SetTreeName(const std::string &Name);

        /**
         * @brief      Sets the input files.
         *
         * @param[in]  Files  The files
         */
        void SetInputFiles(const std::vector<std::string> &Files);

        /**
         * @brief      Sets the output location.
         *
         * @param[in]  File  The file
         */
        void SetOutputLocation(const std::string &File);

        /**
         * @brief      { function_description }
         */
        void FinalizeOutput();

        /**
         * @brief      Sets the readers.
         *
         * @param[in]  R     { parameter_description }
         */
        void SetReaders(const std::vector<ITreeVarReader *> &R);

        /**
         * @brief      Sets the cuts.
         *
         * @param[in]  C     { parameter_description }
         */
        void SetCuts(const std::vector<std::shared_ptr<Condition>> &C);

        /**
         * @brief      Sets the histos.
         *
         * @param[in]  H     { parameter_description }
         */
        void SetHistos(const std::vector<std::shared_ptr<Histo>> &H);

        /**
         * @brief      { function_description }
         *
         * @param[in]  B     { parameter_description }
         */
        void ProcessSystematics(bool B);

        /**
         * @brief      { function_description }
         *
         * @param[in]  B     { parameter_description }
         */
        void ProcessWeightVariations(bool B);

        /**
         * @brief      { function_description }
         *
         * @param[in]  B     { parameter_description }
         */
        void ProcessNominal(bool B);

        /**
         * @brief      Sets the nominal name.
         *
         * @param[in]  Nominal  The nominal
         */
        void SetNominalName(const std::string &Nominal);

        /**
         * @brief      { function_description }
         *
         * @param[in]  B     { parameter_description }
         */
        void DoCutFlow(bool B);

        /**
         * @brief      Give a specific list of systematics
         *
         * @param[in]  List  The list
         */
        void SetSystematics(const std::vector<std::string> &List);

        /**
         * @brief      Give a specific list of systematics affecting the event weights
         *
         * @param[in]  List  The list
         */
        void SetWeightSystmatics(const std::vector<std::string> &List);

        /**
         * @brief      Sets the syst suffix name.
         *
         * @param[in]  N     { parameter_description }
         */
        void SetSystSuffixName(const std::string &N);

        /**
         * @brief      Sets the syst names.
         *
         * @param[in]  Names  The names
         */
        void SetSystNames(const std::map<std::string, std::string> &Names);
        bool RenameSystematic(const std::string &From, const std::string &To);

        /**
         * @brief      Disables the progress bar.
         */
        void DisableProgressBar();

        void setBatchMode();

    private:
        /**
         * @brief      Reads systematics.
         *
         * @return     { description_of_the_return_value }
         */
        bool ReadSystematics();

        /**
         * @brief      Loads a tree from file.
         *
         * @param      F     { parameter_description }
         * @param[in]  Syst  The syst
         *
         * @return     { description_of_the_return_value }
         */
        bool LoadTreeFromFile(std::shared_ptr<TFile> F, const std::string &Syst);
        /**
         * @brief      { function_description }
         *
         * @param[in]  N     { parameter_description }
         */
        void FillCutFlowHistograms(int N) const;

        /**
         * @brief      { function_description }
         *
         * @param[in]  Syst   The syst
         * @param[in]  nMax   The maximum
         * @param[in]  nSkip  The skip
         *
         * @return     { description_of_the_return_value }
         */
        bool ProcessLoop(const std::shared_ptr<IKinematicSyst> &syst, long int nMax = -1, long int nSkip = 0);

        /**
         * @brief      { function_description }
         *
         * @param[in]  nMax   The maximum
         * @param[in]  nSkip  The skip
         *
         * @return     { description_of_the_return_value }
         */
        bool ProcessTree(long int nMax = -1, long int nSkip = 0);

        /**
         * @brief      Gets the total events.
         *
         * @return     The total events.
         */
        bool GetTotalEvents();

    protected:
        /**
         * @brief      { function_description }
         *
         * @param[in]  Syst  The syst
         *
         * @return     { description_of_the_return_value }
         */
        virtual bool initBranches(const std::string &Syst);

        /**
         * @brief      Appends a reader.
         *
         * @param      R     { parameter_description }
         */
        void AppendReader(ITreeVarReader *R);

        /**
         * @brief      Appends a histo.
         *
         * @param      H     { parameter_description }
         *
         * @return     { description_of_the_return_value }
         */
        bool AppendHisto(std::shared_ptr<Histo> H);

        /**
         * @brief      { function_description }
         *
         * @param[in]  Syst  The syst
         *
         * @return     { description_of_the_return_value }
         */
        bool init(const std::string &Syst);

        /**
         * @brief      Reads a meta data tree.
         *
         * @return     { description_of_the_return_value }
         */
        bool ReadMetaDataTree();

        /**
         * @brief      { function_description }
         *
         * @param[in]  Syst  The syst
         *
         * @return     { description_of_the_return_value }
         */
        virtual bool SetupHistograms(const std::string &Syst);

        /**
         * @brief      Creates a directory.
         *
         * @param[in]  Syst    The syst
         * @param[in]  Region  The region
         *
         * @return     { description_of_the_return_value }
         */
        TDirectory *CreateDirectory(const std::string &Syst, const std::string &Region);

        /**
         * @brief      Gets the directory name.
         *
         * @param[in]  Syst    The syst
         * @param[in]  Region  The region
         *
         * @return     The directory name.
         */
        std::string GetDirectoryName(const std::string &Syst, const std::string &Region) const;

        /**
         * @brief      { function_description }
         *
         * @return     { description_of_the_return_value }
         */
        virtual bool AnalyzeEvent();

        /**
         * @brief      Writes an output.
         */
        virtual void WriteOutput();

        /**
         * @brief      { function_description }
         *
         * @param[in]  nMax   The maximum
         * @param[in]  nSkip  The skip
         *
         * @return     { description_of_the_return_value }
         */
        virtual bool HandleWeightVariations();

        /**
         * @brief      { function_description }
         *
         * @param[in]  PassAnd  The pass and
         *
         * @return     { description_of_the_return_value }
         */
        bool PassCuts(bool PassAnd = true) const;
        std::string NominalName() const;

        std::string getFinalSystName(const std::string &syst) const;

    private:
        /**
         * @brief      { function_description }
         *
         * @return     { description_of_the_return_value }
         */
        bool SetupCutFlow();
        EventService *m_eventService;
        bool m_batchMode;
        bool m_DoSyst;
        bool m_DoSyst_W;
        bool m_DoNominal;
        bool m_DoCutFlow;

        std::vector<std::string> m_Systematics;
        std::vector<std::string> m_Systematics_W;
        std::vector<std::string> m_input_dirs;
        std::vector<std::shared_ptr<IKinematicSyst>> m_systToRun;

        std::map<std::string, std::string> m_SystNames;

        std::vector<std::shared_ptr<Condition>> m_Cuts;
        std::vector<std::shared_ptr<Histo>> m_Histos;

        std::shared_ptr<TreeIndexer> m_tree;
        std::shared_ptr<Histo> m_CutFlow;
        std::shared_ptr<Histo> m_RawCutFlow;

        std::string m_TreeName;
        std::string m_Name;
        std::string m_Region;

    protected:
        std::string m_SystAppendix;
        XAMPP::NormalizationDataBase *m_NormDB;
        XAMPP::Weight *m_weight;

        std::shared_ptr<TFile> m_outFile;

    private:
        long int m_TotEvents;
        long int m_CurrentEvent;
        long int m_PrintInterval;
        TStopwatch m_tsw;
        bool m_PrintProgress;
        std::string m_NominalName;

        /**
         * @brief      Prints the progress bar during runtime
         */
        void Progress();
    };
}  // namespace XAMPP
#endif
