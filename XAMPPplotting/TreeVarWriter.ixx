#include <XAMPPplotting/PlottingUtils.h>
#include <XAMPPplotting/TreeVarWriter.h>
namespace XAMPP {

    //#########################################################################################
    //                                  ITreeVarWriter
    //#########################################################################################
    template <typename T> bool ITreeVarWriter::AddBranch(std::string Name, T& Element) {
        Name = RemoveProblematicStr(Name);
        if (m_tree->FindBranch(Name.c_str())) {
            Error("ITreeVarWriter::AddBranch()", "The branch " + Name + " already exists in TTree " + std::string(m_tree->GetName()));
            return false;
        }
        if (m_tree->Branch(Name.c_str(), &Element) == nullptr) {
            Error("ITreeVarWriter::AddBranch()", "Could not create the branch " + Name + " in  TTree " + std::string(m_tree->GetName()));
            return false;
        }
        Info("ITreeVarWriter::AddBranch()",
             "Create new branch " + Name + " in TTree " + std::string(m_tree->GetName()) + " using " + m_Reader->name());
        return true;
    }

    //#########################################################################################
    //                                  TreeScalarVarWriter
    //#########################################################################################
    template <class T>
    ScalarTreeVarWriter<T>::ScalarTreeVarWriter(const std::string& Name, ITreeVarReader* R) : ITreeVarWriter(Name, R), m_Var(0) {}
    template <class T> bool ScalarTreeVarWriter<T>::init(TTree* t) {
        if (!ITreeVarWriter::init(t)) return false;
        return AddBranch(m_Name, m_Var);
    }
    template <class T> bool ScalarTreeVarWriter<T>::fill() {
        m_Var = 0;
        if (m_Reader->entries() == 1) {
            m_Var = m_Reader->readEntry(0);
            if (!IsFinite(m_Var)) {
                double dbl_var = m_Var;
                Error("ScalarVarWriter::fill()",
                      Form("The reader %s does not return a number with %f.", m_Reader->name().c_str(), dbl_var));
                return false;
            }
        } else {
            Error("ScalarVarWriter::fill()", Form("It seems that the given reader %s is a vector type reader since it has a size %lu != 1",
                                                  m_Reader->name().c_str(), m_Reader->entries()));
            return false;
        }

        return true;
    }
    template <class T> ScalarTreeVarWriter<T>* ScalarTreeVarWriter<T>::GetWriter(std::string varname, ITreeVarReader* R) {
        ITreeVarWriterStorage* S = ITreeVarWriterStorage::GetInstance();
        if (!S->GetWriter(varname)) new ScalarTreeVarWriter<T>(varname, R);
        return dynamic_cast<ScalarTreeVarWriter<T>*>(S->GetWriter(varname));
    }
    //#########################################################################################
    //                                  TreeVectorVarWriter
    //#########################################################################################
    template <class T>
    VectorTreeVarWriter<T>::VectorTreeVarWriter(const std::string& Name, ITreeVarReader* R) : ITreeVarWriter(Name, R), m_Var() {}
    template <class T> bool VectorTreeVarWriter<T>::init(TTree* t) {
        if (!ITreeVarWriter::init(t)) return false;
        return AddBranch(m_Name, m_Var);
    }
    template <class T> bool VectorTreeVarWriter<T>::fill() {
        m_Var.clear();
        size_t N = m_Reader->entries();
        if (m_Var.capacity() < N) m_Var.reserve(m_Var.capacity() + N);
        for (size_t i = 0; i < N; ++i) {
            double val = m_Reader->readEntry(i);
            if (!IsFinite(val)) {
                Error(name() + "::fill()", Form("The reader %s does not return a number with %f.", m_Reader->name().c_str(), val));
                return false;
            } else {
                m_Var.push_back(val);
            }
        }
        return true;
    }
    template <class T> VectorTreeVarWriter<T>* VectorTreeVarWriter<T>::GetWriter(const std::string& varname, ITreeVarReader* R) {
        ITreeVarWriterStorage* S = ITreeVarWriterStorage::GetInstance();
        if (!S->GetWriter(varname)) return new VectorTreeVarWriter<T>(varname, R);
        return dynamic_cast<VectorTreeVarWriter<T>*>(S->GetWriter(varname));
    }
}  // namespace XAMPP
