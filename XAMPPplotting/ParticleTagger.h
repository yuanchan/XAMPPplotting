#ifndef XAMPPPLOTTING_PARTICLETAGGER_H
#define XAMPPPLOTTING_PARTICLETAGGER_H

#include <XAMPPplotting/PlottingUtils.h>
#include <XAMPPplotting/TreeVarReader.h>

#include <XAMPPplotting/Cuts.h>
#include <map>
#include <memory>
#include <string>
#include <vector>
namespace XAMPP {
    class ParticleTaggerManager;
    class IParticleTagger;
    class IfDefLineParser;
    typedef std::vector<std::shared_ptr<IParticleTagger>> TaggerVector;

    // The IParticleTagger class is meant to mark a particle in the event, regardless in which IParticleReader it is currently embedded.
    // In order to tag an object in the event few information is needed: In which branches the object has been saved to; at which position
    // in the vector the particle stored. The criteria used to tag the particle can be arbitrary following the XAMPPplotting Condtion logic
    class IParticleTagger {
    public:
        // Standard constructor. Which IParticleReader shall be used and which particle should be tagged from that collection based on
        // which set of conditions
        IParticleTagger(IParticleReader* P, unsigned int pos_inContainer, const std::string& tagger_name, std::shared_ptr<Condition> C);
        IParticleTagger(IParticleReader* P, unsigned int pos_inContainer, unsigned int tagger_name, std::shared_ptr<Condition> C);

        virtual ~IParticleTagger();

        // The tagging information only needs to be evaluated once per event. The newEvent method runs the tagger
        virtual void newEvent();

        bool isTagged() const;

        // Checks whether the current entry corresponds to the tagged entry of the container
        virtual bool isTagged(IParticleReader* P) const;
        virtual bool isTagged(IParticleReader* P, size_t particle_index) const;

        // Index of the particle the Tagger is looking for in the container
        unsigned int particle_index() const;
        unsigned int tree_index() const;
        // The hash value of the collection itself
        unsigned int hash() const;

        std::shared_ptr<Condition> Cut() const;
        IParticleReader* Particle() const;
        // The hash of the TaggerId itself
        int taggerID() const;
        std::string name() const;

        virtual std::shared_ptr<IParticleTagger> Clone(unsigned target_index);

    private:
        IParticleTagger(const IParticleTagger&) = delete;
        void operator=(const IParticleTagger&) = delete;
        IParticleReader* m_Particle;
        unsigned int m_ParticleIdx;
        std::shared_ptr<Condition> m_Cut;
        unsigned int m_TreeIdx;
        unsigned int m_Hash;
        int m_Id;
        bool m_Tagged;
    };

    class ParticleTaggerCut : public Condition {
    public:
        virtual bool Pass() const;
        virtual bool PassScalar() const;
        virtual bool PassVector(size_t entry) const;
        virtual bool PassMatrix(size_t column, size_t row) const;
        virtual std::string name() const;
        ParticleTaggerCut(IParticleReader* reader, const std::string tagger_name);

        bool loadTaggers() const;

    protected:
        ParticleTaggerManager* m_manager;
        IParticleReader* m_particle;
        std::string m_tagger_name;
        unsigned int m_tagger_hash;
        mutable bool m_taggers_loaded;
        mutable const TaggerVector* m_taggers;
    };

    class ContainerTaggerCut : public ParticleTaggerCut {
    public:
        ContainerTaggerCut(IParticleReader* reader, const std::string& tagger_name);

        virtual bool PassVector(size_t entry) const;

    protected:
        mutable std::shared_ptr<IParticleTagger> m_max_tagger;
        std::shared_ptr<IParticleTagger> FillUp(int begin, int end) const;
    };
    class IsParticleTaggedReader : public IParticleVariable {
    public:
        std::string name() const override;
        double readEntry(const size_t I) const override;
        bool init(TTree* t) override;
        static IParticleVarReader* getReader(const std::string& particle, const std::string& tagger_name);

    private:
        IsParticleTaggedReader(IParticleReader*, std::shared_ptr<ParticleTaggerCut> tagger_cut);
        IsParticleTaggedReader(const IsParticleTaggedReader&) = delete;
        void operator=(const IsParticleTaggedReader&) = delete;
        std::shared_ptr<ParticleTaggerCut> m_tagger_cut;
    };
    class ParticleTaggerManager {
    public:
        static ParticleTaggerManager* Instance();
        virtual ~ParticleTaggerManager();

        void Register(std::shared_ptr<IParticleTagger> P);

        bool AddCommonTagger(std::ifstream& ifstream, std::vector<ITreeVarReader*>& Readers);

        void TagParticles();
        const TaggerVector* findTaggers(unsigned int particle_hash, unsigned int tagger_ID) const;

        void SetSampleName(const std::string& name);
        std::string sampleName() const;
        size_t sampleHash() const;

        void clearStore();
        std::shared_ptr<IParticleTagger> smartPointer(IParticleTagger* tagger) const;
        std::shared_ptr<IParticleTagger> smartPointer(std::shared_ptr<IParticleTagger> tagger);

    private:
        static ParticleTaggerManager* m_Inst;
        ParticleTaggerManager();
        ParticleTaggerManager(const ParticleTaggerManager&) = delete;
        void operator=(const ParticleTaggerManager&) = delete;

        IfDefLineParser ReadLine;

        Long64_t m_CurrentEvent;
        std::string m_SampleName;
        size_t m_SampleHash;

        // This list serves as the ultimate list where all taggers need to be passed to
        TaggerVector m_TaggerStore;

        // Lookup table based on the hash of the particle's and the tagger-ID's
        // First sort by hash then by tagger ID
        typedef std::map<unsigned int, TaggerVector> TaggerID_Map;
        std::map<unsigned int, TaggerID_Map> m_Taggers;

        bool isTagged(const TaggerVector& Taggers, IParticleReader* P, int ID);
        bool isTaggerInList(const TaggerVector& Taggers, std::shared_ptr<IParticleTagger>) const;
    };
}  // namespace XAMPP
#endif  // TREEVARREADER_H
