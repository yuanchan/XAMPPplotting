#ifndef XAMPPPlotting_HISTOTEMPLATES
#define XAMPPPlotting_HISTOTEMPLATES

#include <TH1.h>
#include <TH2Poly.h>
#include <fstream>
#include <iostream>
#include <map>
#include <memory>
#include <string>
namespace XAMPP {
    class HistoTemplates {
    public:
        static HistoTemplates *getHistoTemplater();
        bool InitTemplates(const std::string &Path);

        bool Create1D(const std::string &Line);
        bool Create2D(const std::string &Line);
        bool Create3D(const std::string &Line);
        bool CrossProduct(const std::string &Line);

        bool CreateVarHisto(const std::string &Name, std::ifstream &Config);
        bool CreateVarHisto(const std::string &Name, std::vector<double> &BinEdgesX, std::vector<double> &BinEdgesY,
                            std::vector<double> &BinEdgesZ);

        bool CreateTH2Poly(const std::string &Name, std::ifstream &Config);

        TH1 *GetTemplate(const std::string &Name) const;
        bool InsertTemplate(const std::string &Name, TH1 *H);
        bool InsertTemplate(const std::string &Name, std::shared_ptr<TH1> H);

        bool IsTemplateDefined(const std::string &Name) const;

        virtual ~HistoTemplates();

    private:
        HistoTemplates();
        HistoTemplates(const HistoTemplates &) = delete;
        void operator=(const HistoTemplates &) = delete;
        void PrintVarBinning(std::vector<double> &Edges, const std::string &Axis);
        bool Import(const std::string &Config);
        bool ReadTemplateConfig(std::ifstream &Inf);
        bool AddTH2PolyBin(std::shared_ptr<TH2Poly> &Poly, std::ifstream &Inf);
        static HistoTemplates *m_DB;
        std::map<std::string, std::shared_ptr<TH1>> m_Templates;
        std::vector<std::string> m_ImportedCfgs;
    };
}  // namespace XAMPP
#endif
