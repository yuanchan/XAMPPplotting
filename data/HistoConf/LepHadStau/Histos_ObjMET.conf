Import XAMPPplotting/HistoConf/LepHadStau/Histos_Defs.conf
#################################################################################################################
#                             Object-MET Observables                                                            #
##################################################################################################################

### vec sum pT between met, lep, tau
NewVar
    Type 1D
    Template Var_pTeff
    Name met_lep_tau_vec_sum_pt
    EvReader floatGeV MET_LepTau_VecSumPt
    xLabel |#bf{p}_{T}(l) + #bf{p}_{T}(#tau) + #bf{E}_{T}^{miss}| [GeV]  
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

#### centrality ###
NewVar 
    Type 1D
    Template met_centrality
    Name met_lep_centrality
    EvReader float MET_Centrality
    xLabel Centrality(l, MET)
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

NewVar
    Type 1D
    Template Var_met_lep_tau_sum_mt
    Name met_lep_tau_sum_mt
    #defined in RunConf/LepHadStau/CommonConfigs/CutDefinitions.conf
    New_MathReader +
        MtMetReader BaseTaus[0] MetTST
        MtMetReader SignalLightLeptons[0] MetTST
    End_MathReader
    xLabel M_{T}(l, E_{T}^{miss}) + M_{T}(#tau, MET) [GeV]
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

### MT lep-met ###
NewVar
    Type 1D
    Template Var_met_mt_lep
    Name met_lep_mt
    MtMetReader SignalLightLeptons[0] MetTST
    xLabel M_{T}(l, E_{T}^{miss}) [GeV]
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

### MT tau-met ###
NewVar
    Type 1D
    Template Var_met_mt_tau
    Name met_tau_mt
    MtMetReader BaseTaus[0] MetTST
    xLabel M_{T}(#tau, MET) [GeV]
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

### MET bisect ###
NewVar
    Type 1D
    Template binary
    Name met_lep_tau_phi_bisect
    EvReader char MET_BiSect
    xLabel MET #phi(l, #tau) bisect
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

### Delta Phi #####
NewVar
    Type 1D
    Template dPhi_back2back
    Name met_tau_dPhi
    |dPhiToMetReader| BaseTaus[0] MetTST
    xLabel #Delta#phi(E_{T}^{miss}, #tau)
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

NewVar
    NumParCut SignalLightLeptons > 0
    Type 1D
    Template dPhi
    Name met_lep_dPhi
    |dPhiToMetReader| SignalLightLeptons[0] MetTST
    xLabel #Delta#phi(E_{T}^{miss}, l)
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

NewVar
    Type 1D
    Template dPhi
    Name met_lep_tau_dPhi
    #|dPhiToMetReader| SignalLightLeptonsBaseTaus[0] MetTST
    |EvReader| float MET_LepTau_DeltaPhi
    xLabel #Delta#phi(E_{T}^{miss}, l + #tau)
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar
        
NewVar
    NumParCut SignalJets > 0
    Type 1D
    Template dPhi
    Name met_leading_jet_dPhi
    |dPhiToMetReader| SignalJets[0] MetTST
    xLabel #Delta#phi(E_{T}^{miss}, leading jet)
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

### min cos phi(MET, lep)  ### (SetReader = sum )
NewVar
    Type 1D
    Template cos_phi
    EvReader float MET_CosMinDeltaPhi
    xLabel cos(min[#delta#phi(#tau, MET), #delta#phi(lep, MET)])
    Name met_lep_tau_cos_min_phi
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

### sum cos delta phi ###
NewVar
    Type 1D
    Template sum_cos_dphi
    xLabel #Sigma_{}cos#Delta#phi
    Name met_lep_tau_sum_cos_dphi
    EvReader float MET_SumCosDeltaPhi
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar
