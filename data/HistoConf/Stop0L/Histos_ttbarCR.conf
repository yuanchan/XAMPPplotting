Import XAMPPplotting/HistoConf/Stop0L/Histos.conf

##################################################################################################################
#		          With the created templates above lets plot some variables                                      #
##################################################################################################################

NewVar
    Type 1D
    Template NumJets
    Name jetleps_n
    NumParReader JetLeps
    xLabel N_{JetLeps}
EndVar

NewVar  
    Type 1D
    Template medium_pt
    Name jetlep1_pt
    ParReader JetLeps pt[0]
    xLabel Leading jetlep p_{T} [GeV]
EndVar

NewVar  
    Type 1D
    Template medium_pt
    Name jetlep2_pt
    ParReader JetLeps pt[1]
    xLabel 2nd leading jetlep p_{T} [GeV]
EndVar

NewVar  
    Type 1D
    Template low_pt
    Name jetlep3_pt
    ParReader JetLeps pt[2]
    xLabel 3rd leading jetlep p_{T} [GeV]
EndVar

NewVar  
    Type 1D
    Template low_pt
    Name jetlep4_pt
    ParReader JetLeps pt[3]
    xLabel 4th leading jetlep p_{T} [GeV]
EndVar

NewVar  
    Type 1D
    Template low_pt
    Name jetlep5_pt
    ParReader JetLeps pt[4]
    xLabel 5th leading jetlep p_{T} [GeV]
EndVar

1DHisto MtLepMet_hist 8 0 160
NewVar 
	Type 1D
	Template MtLepMet_hist
	Name MtLepMet	
	EvReader floatGeV MtLepMet
	xLabel m_{T}(Lepton,E_{T}^{miss}) [GeV]
EndVar

1DHisto mbb_ttbar 11 0 220

NewVar 
    Type 1D
    Template high_pt
    Name mbb_weightsorted
    InvDiMReader Bjets_ASC_MV2c10 Bjets_ASC_MV2c10 0 1
    xLabel m_{bb} (MV2c10-sorted) [GeV]
EndVar

NewVar 
    Type 1D
    Template phi_hist
    Name dPhi_weightsorted
    |dPhiReader| Bjets_ASC_MV2c10 Bjets_ASC_MV2c10 0 1
    xLabel #Delta#phi_{bb} (MV2c10-sorted)
EndVar

NewVar 
    Type 1D
    Template high_pt
    Name mbb_ptsorted
    InvDiMReader Bjets Bjets 0 1
    xLabel m_{bb} (p_{T}-sorted) [GeV]
EndVar

NewVar 
    Type 1D
    Template phi_hist
    Name dPhi_ptsorted
    |dPhiReader| Bjets Bjets 0 1
    xLabel #Delta#phi_{bb} (p_{T}-sorted) [GeV]
EndVar

NewVar 
    Type 1D
    Template dR_hist
    Name dR_ptsorted
    |dRReader| Bjets Bjets 0 1
    xLabel #DeltaR_{bb} (p_{T}-sorted) [GeV]
EndVar

NewVar  
    Type 1D
    Template low_pt
    Name e_pt
    ParReader SignalElectrons pt[0]
    xLabel Leading electron p_{T} [GeV]
EndVar

NewVar  
    Type 1D
    Template low_pt
    Name mu_pt
    ParReader SignalMuons pt[0]
    xLabel Leading muon p_{T} [GeV]
EndVar

NewVar  
    Type 1D
    Template fulleta_hist
    Name mu_eta
    ParReader SignalMuons eta
    xLabel #eta(#mu)
EndVar

NewVar  
    Type 1D
    Template fullphi_hist
    Name mu_phi
    ParReader SignalMuons phi
    xLabel #phi(#mu)
EndVar

NewVar  
    Type 1D
    Template eta_hist
    Name mu_abseta
    |ParReader| SignalMuons eta
    xLabel |#eta(#mu)|
EndVar

NewVar  
    Type 1D
    Template phi_hist
    Name mu_absphi
    |ParReader| SignalMuons phi
    xLabel |#phi(#mu)|
EndVar

NewVar  
    Type 1D
    Template eta_hist
    Name lep_abseta
    |ParReader| SignalLeps eta
    xLabel |Lepton #eta|
EndVar

NewVar  
    Type 1D
    Template phi_hist
    Name lep_absphi
    |ParReader| SignalLeps phi
    xLabel |Lepton #phi|
EndVar

NewVar  
    Type 1D
    Template low_pt
    Name lep_pt
    ParReader SignalLeps pt[0]
    xLabel Leading lepton p_{T} [GeV]
EndVar

NewVar  
    Type 1D
    Template low_pt
    Name jetleps_pt
    ParReader JetLeps pt[0]
    xLabel Leading JetLep p_{T} [GeV]
EndVar

1DHisto FatJet12M_hist 18 50 410
NewVar  
    Type 1D
    Template FatJet12M_hist
    Name FatJet12M1
    ParReader FatJetsR12 m[0]
    xLabel Leading FatJet (R=1.2) M [GeV]
EndVar
NewVar  
    Type 1D
    Template low_pt
    Name FatJet12M2
    ParReader FatJetsR12 m[1]
    xLabel 2nd-leading FatJet (R=1.2) M [GeV]
EndVar

NewVar  
    Type 1D
    Template low_pt
    Name FatJet8M1
    ParReader FatJetsR8 m[0]
    xLabel Leading FatJet (R=0.8) M [GeV]
EndVar
NewVar  
    Type 1D
    Template low_pt
    Name FatJet8M2
    ParReader FatJetsR8 m[1]
    xLabel 2nd-leading FatJet (R=0.8) M [GeV]
EndVar

NewVar
    Type 1D
    Template NumJets
    Name NJetLeps
    NumParReader JetLeps
    xLabel N_{jets+leps}
EndVar

NewVar 
    Type 1D
    Template phi_hist
    Name dPhibl_weightsorted
    |dPhiReader| Bjets_ASC_MV2c10 SignalLeps 0 0
    xLabel #Delta#phi_{bl} (MV2c10-sorted)
EndVar

NewVar 
    Type 1D
    Template dR_hist
    Name dRb1l_weightsorted
    |dRReader| Bjets_ASC_MV2c10 SignalLeps 0 0
    xLabel #DeltaR_{b_{1}l} (MV2c10-sorted)
EndVar

NewVar 
    Type 1D
    Template dR_hist
    Name dRb2l_weightsorted
    |dRReader| Bjets_ASC_MV2c10 SignalLeps 1 0
    xLabel #DeltaR_{b_{2}l} (MV2c10-sorted)
EndVar

NewVar 
    Type 1D
    Template dR_hist
    Name dRblmin_weightsorted
    EvReader min dRBLMinReader
    xLabel #DeltaR_{blmin} (MV2c10-sorted)
EndVar
1DHisto dRblminttbar_hist 10 0 2
NewVar 
    Type 1D
    Template dRblminttbar_hist
    Name dRblmin_weightsorted_ttbar
    EvReader min dRBLMinReader
    xLabel #DeltaR_{blmin} (MV2c10-sorted)
EndVar

1DHisto metprime_hist 8 0 800
NewVar 
    Type 1D
    Template metprime_hist
    Name metinv
    EvReader floatGeV MetLepInv_met
    xLabel E_{T}^{miss}' [GeV]
EndVar
1DHisto mtbminlep_hist 12 0 600
NewVar 
    Type 1D
    Template mtbminlep_hist
    Name mtbminlep
    EvReader floatGeV MtBMinLep
    xLabel m_{T}(b_{min},E_{T}^{miss}') [GeV]
EndVar
1DHisto mtbmaxlep_hist 16 0 800
NewVar 
    Type 1D
    Template mtbmaxlep_hist
    Name mtbmaxlep
    EvReader floatGeV MtBMaxLep
    xLabel m_{T}(b_{max},E_{T}^{miss}') [GeV]
EndVar
