##################################################################################################################
##################################################################################################################
#                             Create the  histo templates first                                                  #
##################################################################################################################

#TH1D <name> <nbins> <low_edge> <high_edge> 
1DHisto low_pt 14 0 350

1DHisto medium_pt 10 0 500

1DHisto high_pt 20 0 1000

1DHisto very_high_pt 20 0 1500

1DHisto phi_hist 10 0 3.1415

1DHisto eta_hist 10 0 2.5

1DHisto dR_hist 10 0 5

1DHisto fulleta_hist 20 -2.5 2.5

1DHisto fullphi_hist 20 -3.1415 3.1415

1DHisto NumJets 10 1.5 11.5

1DHisto NumBJets 5 1 6

1DHisto NumLeps 4 0 4

1DHisto NumPV 40 0 40

1DHisto weight 100 0 6

#TH2D <name> <nbinsX> <low_edgeX> <high_edgeX> <nbinsY> <low_edgeY> <high_edgeY>
#2DHisto eta_phi 15 0 100 10 0 200

#We can also Create histograms with variable bins

VarHisto lowVar_pt
#This line actually lists the lower_bin edges each line represents one axis
0 10 15 20 25 30 35 40 45 50 60 70 80 90 100 120 140 160 180 200 250 300 350
#So if you want to have a 2D histo
#0 10 50 60
#Mandantory for the interpreter that the definition is ended
End_VarHisto
1DHisto meff 100 0 900
#VarHisto meff
#0 50 100 150 200 250 300 350 400 500 600 700 800 900
#End_VarHisto

VarHisto low_meff
0 50 100 150 200 250 300 350 400 
End_VarHisto

VarHisto met
0 10 20 30 40 50 100 150 200 250 300 
End_VarHisto

##################################################################################################################
#		          With the created templates above lets plot some variables                                      #
##################################################################################################################


#############################################
#          Event variables                  #
#############################################

1DHisto mtbmin_hist 16 200 1000
NewVar 
	Type 1D
	Template mtbmin_hist
	Name mtbmin	
	EvReader floatGeV MtBMin
	xLabel m_{T}^{b,min} [GeV]
	EntriesLabel Events
EndVar
NewVar 
	Type 1D
	Template mtbmin_hist
	Name mtbmax	
	EvReader floatGeV MtBMax
	xLabel m_{T}^{b,max} [GeV]
EndVar
1DHisto mtbmin_hist_low 10 200 450
NewVar 
    Type 1D
    Template mtbmin_hist_low
    Name mtbmax_low
    EvReader floatGeV MtBMax
    xLabel m_{T}^{b,max} [GeV]
EndVar

1DHisto ht_hist 15 0 3000
NewVar 
    Type 1D
    Template ht_hist
    Name meff   
    EvReader floatGeV Meff
    xLabel m_{eff} [GeV]
EndVar

NewVar 
	Type 1D
	Template ht_hist
	Name ht
	EvReader floatGeV HT
	xLabel H_{T} [GeV]
EndVar

1DHisto HTSig 15 0 30
NewVar
	Type 1D
	Template HTSig
	Name HTSig
	EvReader float HtSig
	xLabel E_{T}^{miss}/#sqrt{H_{T}} [#sqrt{GeV}]
EndVar
NewVar
    Type 1D
    Template HTSig
    Name HTSigLep
    New_SignificanceReader HtSigLep
        Numerator EvReader floatGeV MetLepInv_met
        Sqrt EvReader floatGeV HT
    End_SignificanceReader
    xLabel E_{T}^{miss,'}/#sqrt{H_{T}} [#sqrt{GeV}]
EndVar

#1DHisto met_hist 13 150 800
1DHisto met_hist 20 0 1000
NewVar 
	Type 1D
	Template met_hist
	Name met	
	EvReader floatGeV MetTST_met
	xLabel E_{T}^{miss} [GeV]
EndVar

1DHisto met_hist_sys 15 250 1000
NewVar 
    Type 1D
    Template met_hist_sys
    Name met_sys  
    EvReader floatGeV MetTST_met
    xLabel E_{T}^{miss} [GeV]
EndVar

1DHisto highmet_hist 14 300 1000
NewVar 
    Type 1D
    Template highmet_hist
    Name highmet    
    EvReader floatGeV MetTST_met
    xLabel E_{T}^{miss} [GeV]
EndVar

NewVar 
    Type 1D
    Template low_pt
    Name mettrack   
    EvReader floatGeV MetTrack_met
    xLabel E_{T,Track}^{miss} [GeV]
EndVar

NewVar 
    Type 1D
    Template phi_hist
    Name deltaphimin2
    EvReader min DeltaPhiMin_2
    xLabel |#Delta#phi(jet^{0,1},E_{T}^{miss})|
EndVar
NewVar 
    Type 1D
    Template phi_hist
    Name deltaphimin3
    EvReader min DeltaPhiMin_3
    xLabel |#Delta#phi(jet^{0,1,2},E_{T}^{miss})|
EndVar
NewVar 
    Type 1D
    Template phi_hist
    Name deltaphimin4
    EvReader min DeltaPhiMin_4
    xLabel |#Delta#phi(jet^{0-3},E_{T}^{miss})|
EndVar


#############################################
#          Jets                             #
#############################################

NewVar  
    Type 1D
    Template medium_pt
    Name jet1_pt
    ParReader SignalJets pt[0]
    xLabel p^{0}_{T} [GeV]
EndVar

NewVar  
    Type 1D
    Template medium_pt
    Name jet2_pt
    ParReader SignalJets pt[1]
    xLabel p^{1}_{T} [GeV]
EndVar

NewVar  
    Type 1D
    Template low_pt
    Name jet3_pt
    ParReader SignalJets pt[2]
    xLabel p^{2}_{T} [GeV]
EndVar

NewVar  
    Type 1D
    Template low_pt
    Name jet4_pt
    ParReader SignalJets pt[3]
    xLabel p^{4}_{T} [GeV]
EndVar

NewVar  
    Type 1D
    Template low_pt
    Name jet5_pt
    ParReader SignalJets pt[4]
    xLabel p^{4}_{T} [GeV]
EndVar

NewVar  
    Type 1D
    Template medium_pt
    Name bjet1_pt
    ParReader Bjets pt[0]
    xLabel p^{0}_{T,b} [GeV]
EndVar

NewVar  
    Type 1D
    Template medium_pt
    Name bjet2_pt
    ParReader Bjets pt[1]
    xLabel p^{1}_{T,b} [GeV]
EndVar

NewVar  
    Type 1D
    Template medium_pt
    Name bjet1_weight_pt
    ParReader Bjets_ASC_MV2c10 pt[0]
    xLabel Leading MV2c10 b-jet p_{T} [GeV]
EndVar

NewVar  
    Type 1D
    Template medium_pt
    Name bjet2_weight_pt
    ParReader Bjets_ASC_MV2c10 pt[1]
    xLabel 2nd leading MV2c10 b-jet p_{T} [GeV]
EndVar

NewVar
	Type 1D
	Template NumJets
	Name jets_n
	NumParReader SignalJets
	xLabel N_{jets}
EndVar

NewVar
	Type 1D
	Template NumBJets
	Name bjets_n
	NumParReader Bjets
	xLabel N_{b-jets}
EndVar


1DHisto 1DFatJetM121 20 0 400
NewVar 
    Type 1D
    Template 1DFatJetM121
    Name FatJet12M0
    ParReader FatJetsR12 m[0]
    xLabel m^{0}_{jet, R=1.2} [GeV]
EndVar

1DHisto 1DFatJetM122 20 0 200
NewVar 
    Type 1D
    Template 1DFatJetM122
    Name FatJet12M1
    ParReader FatJetsR12 m[1]
    xLabel m^{1}_{jet, R=1.2} [GeV]
EndVar


VarHisto 1DFatJetM122zoom
0 5 10 20 30 40 50 60 80 120
End_VarHisto
NewVar 
    Type 1D
    Template 1DFatJetM122zoom
    Name FatJet12M1zoom
    ParReader FatJetsR12 m[1]
    xLabel m^{1}_{jet, R=1.2} [GeV]
EndVar


#############################################
#          Leptons                          #
#############################################

NewVar
    Type 1D
    Template NumLeps
    Name leps_n
    NumParReader SignalLeps
    xLabel N_{#font[12]{l}}
EndVar

NewVar
    Type 1D
    Template NumLeps
    Name el_n
    NumParReader SignalElectrons
    xLabel N_{e}
EndVar

NewVar
    Type 1D
    Template NumLeps
    Name mu_n
    NumParReader SignalMuons
    xLabel N_{#mu}
EndVar

NewVar 
    Type 1D
    Template dR_hist
    Name dR_weightsorted
    |dRReader| Bjets_ASC_MV2c10[1] Bjets_ASC_MV2c10[0]
    xLabel #DeltaR(b,b)
EndVar

NewVar 
    Type 1D
    Template dR_hist
    Name dR_b1L
    |dRReader| Bjets_ASC_MV2c10[0] SignalLeps[0]
    xLabel #DeltaR_{b1l} (MV2c10-sorted)
EndVar
NewVar 
    Type 1D
    Template dR_hist
    Name dR_b2L
    |dRReader| Bjets_ASC_MV2c10[1] SignalLeps[0]
    xLabel #DeltaR_{b2l} (MV2c10-sorted)
EndVar

1DHisto dRbl_hist 10 0 4
NewVar 
    Type 1D
    Template dRbl_hist
    Name dRblmin_weightsorted
    EvReader min dRBLMinReader
    xLabel #DeltaR(b_{0,1},#font[12]{l})_{min}
EndVar

#############################################
#          dRbb check for Matthias          #
#############################################
1DHisto dRbb_hist 12 1 5.5
NewVar 
    Type 1D
    Template dRbb_hist
    Name dR_weightsorted_check
    |dRReader| Bjets_ASC_MV2c10[1] Bjets_ASC_MV2c10[0]
    xLabel #DeltaR(b,b)
EndVar


#############################################
#          Cross checks                     #
#############################################

NewVar
    Type 1D
    Template NumPV
    Name npv
    EvReader int Vtx_n
    xLabel Number of primary vertices
EndVar

NewVar
    Type 1D
    Template NumPV
    Name avgIPX
    EvReader float averageInteractionsPerCrossing
    xLabel Avg. number of int. per crossing
EndVar
NewVar
    Type 1D
    Template NumPV
    Name corrAvgIPX
    EvReader float corr_avgIntPerX
    xLabel Corrected avg. number of int. per crossing
EndVar

NewVar
    Type 1D
    Template NumPV
    Name actIPX
    EvReader float actualInteractionsPerCrossing
    xLabel Act. number of int. per crossing
EndVar

ifdef isMC
    NewVar
        Type 1D
        Template NumPV
        Name avgIPX_MCavgDatacorr
        EvReader float averageInteractionsPerCrossing
        xLabel Avg. number of int. per crossing
    EndVar
endif
ifdef isData
    NewVar
        Type 1D
        Template NumPV
        Name avgIPX_MCavgDatacorr
        EvReader float corr_avgIntPerX
        xLabel Avg. number of int. per crossing
    EndVar
endif
ifdef isMC
    NewVar
        Type 1D
        Template NumPV
        Name avgIPX_MCcorrDataavg
        EvReader float corr_avgIntPerX
        xLabel Avg. number of int. per crossing
    EndVar
endif
ifdef isData
    NewVar
        Type 1D
        Template NumPV
        Name avgIPX_MCcorrDataavg
        EvReader float averageInteractionsPerCrossing
        xLabel Avg. number of int. per crossing
    EndVar
endif

NewVar
    Type 1D
    Template phi_hist
    Name DPhiMetTrackMet
    |EvReader| float DPhiMetTrackMet
    xLabel |#Delta#phi(E_{T}^{miss},E_{T}^{miss,track})|
EndVar

