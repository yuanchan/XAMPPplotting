##################################################################################################################
#                             Base Taus in general
##################################################################################################################
NewVar  
    Type 1D
    Template tauVar_pt
    Name tau_base_pt
    ParReader BaseTaus pt[0]
    xLabel p_{T}(Base #tau) [GeV]
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

NewVar  
    Type 1D
    Template eta
    Name tau_base_eta
    ParReader BaseTaus eta[0]
    xLabel #eta(Base #tau)
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

NewVar  
    Type 1D
    Template phi
    Name tau_base_phi
    ParReader BaseTaus phi[0]
    xLabel #phi(Base #tau) [rad]
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

NewVar  
    Type 1D
    Template tau_tracks
    Name tau_base_Ntrks
    ParReader BaseTaus NTrks[0]
    xLabel N_{trks}(Base #tau)
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

NewVar  
    Type 1D
    Template tau_charge
    Name tau_base_charge
    ParReader BaseTaus charge[0]
    xLabel Q(Base #tau)
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

NewVar  
    Type 1D
    Template jet_width
    Name tau_base_jet_width
    ParReader BaseTaus Width[0]
    xLabel Width(Base #tau)
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

NewVar
    Type 1D
    Template jet_tracks
    Name tau_base_jet_tracks
    ParReader BaseTaus NTrksJet[0]
    xLabel Ntrks(Base #tau)
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

NewVar  
    Type 1D
    Template Var_tau_jet_bdt
    Name tau_base_jet_bdt
    ParReader BaseTaus BDTJetScore[0]
    xLabel #tau JetBDT score
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar

NewVar
    Type 2D
    ParReader BaseTaus pt[0]
    ParReader BaseTaus BDTJetScore[0]
    Template tau_pt_jet_bdt
    Name tau_base_pt_jet_bdt
    xLabel Base #tau p_{T} [GeV]
    yLabel Base #tau JetBDT score    
EndVar


##################################################################################################################
#                             Signal Taus                                                                        #
##################################################################################################################

NewVar  
    Type 1D
    Template tauVar_pt_ff
    Name tau_pt_FF
    ParReader SignalTaus pt[0]
    xLabel p_{T}(#tau) [GeV]
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar
NewVar  
    Type 1D
    Template tauVar_pt
    Name tau_pt
    ParReader SignalTaus pt[0]
    xLabel p_{T}(#tau) [GeV]
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar
NewVar  
    Type 1D
    Template eta
    Name tau_eta
    ParReader SignalTaus eta[0]
    xLabel #eta(#tau)
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar
NewVar  
    Type 1D
    Template phi
    Name tau_phi
    ParReader SignalTaus phi[0]
    xLabel #phi(#tau) [rad]
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar
NewVar  
    Type 1D
    Template tau_tracks
    Name tau_Ntrks
    ParReader SignalTaus NTrks[0]
    xLabel N_{trks}(#tau)
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar
NewVar  
    Type 1D
    Template tau_charge
    Name tau_charge
    ParReader SignalTaus charge[0]
    xLabel Q(#tau)
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar
NewVar  
    Type 1D
    Template jet_width
    Name tau_jet_width
    ParReader SignalTaus Width[0]
    xLabel Width(#tau)
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar
NewVar
    Type 1D
    Template jet_tracks
    Name tau_jet_tracks
    ParReader SignalTaus NTrksJet[0]
    xLabel #tau^{Real}_{signal}(N tracks jet)
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar
NewVar  
    Type 1D
    Template Var_tau_jet_bdt
    Name tau_jet_bdt
    ParReader SignalTaus BDTJetScore[0]
    xLabel #tau JetBDT score
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar
NewVar
    Type 2D
    ParReader BaseTaus1P pt[0] 
    ParReader BaseTaus1P BDTJetScore[0]
    Template tau_pt_jet_bdt
    Name tau_pt_jet_bdt
    xLabel #tau p_{T} [GeV]
    yLabel #tau JetBDT score    
    ifdef NormByBinWidth
          DivideByBinWidth
    endif
EndVar
