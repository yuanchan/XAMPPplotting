###This file is an example RunConfig for the XAMPPplotting package
###
Weights GenWeight
### Lets tell the code the name of the tree in the input files i.e. SUSYAnalysisConfg_******
Tree TruthStaus
### This option defines the name of the top directories in the output histogram files.
### For the Tree files the name is taken from the SampleName property in the InputConfig
Analysis TruthStau

#Systematics which you want to run over. If you do not specify it XAMPPplotting runs over all systematics stored
#Systematic Nominal

#NoSUSYpidWeight
#For testing it is quite usefull to switch off the systematics. Then XAMPPplotting only runs over the Nominal tree


#Event weights for MonteCarlo to use. Please never add the MuWeight as the PileUp weight is treated separately in the tool
# Weights are included in the trigger conf files

#You can switch off the PileUpReweighting by uncommenting this line. Useful for MC comparisons only
noPRW
#Or redo the prw with updated lumi calc files
#reDoPRW

#Disable the cros-section to get the efficiencies plotted
#disableXS
#Lumi 0.001
# Flag for giving a custom directory with xSection files (if MetaData information is outdated)
xSecDir XAMPPplotting/xSecFiles/

#Disable the reweighting of SumW to  each SUSYproc Id -> unneccessary with the coming derivations
#NoSUSYpidWeight

# Create CutFlow histogram
doCutFlow

ifdef LHESystematics
    ImportAlways XAMPPplotting/Misc/TruthSystematics/aMCatNLO_WeightVariations.conf
    ImportAlways XAMPPplotting/Misc/TruthSystematics/PowHeg_WeightVariations.conf
    ImportAlways XAMPPplotting/Misc/TruthSystematics/Sherpa_WeightVariations.conf 
endif

#########################################################################################################
#                                   Signal particles                                                    #
#########################################################################################################

#New_Particle <Name>  <Name of the InputBranches>
New_Particle SignalMuons muons 
   Cut char signal = 1
   Cut floatGeV pt > 15
   Cut float eta |<| 2.7  
   Var float charge
   Var int truthType
   Var int truthOrigin
   Var floatGeV MT
End_Particle 

New_Particle SignalElectrons electrons
   Cut char signal = 1
   Cut floatGeV pt > 15.
   Cut float eta |<| 2.47
   Var float charge
   Var int truthType
   Var int truthOrigin
   Var floatGeV MT
End_Particle 

DiParticle BaseLeptons SignalElectrons SignalMuons

New_ParticleM SignalTaus taus  
   Cut char signal = 1
   Cut char passOR = 1
   Cut char IsHadronicTau = 1
   Cut float eta |<| 2.47
   Cut floatGeV pt > 40.
   CombCut OR
        Cut float eta |<| 1.37
        Cut float eta |>| 1.521
   End_CombCut
   Var floatGeV MT
   Var float charge
End_Particle 

New_ParticleM BaseTaus taus
    Cut char IsHadronicTau = 1
    Cut float eta |<| 2.5
   Cut floatGeV pt > 20.
    CombCut OR
        Cut float eta |<| 1.37
        Cut float eta |>| 1.521
   End_CombCut
   Var floatGeV MT
   Var float charge
   Var int truthType
   Var int truthOrigin
End_Particle

New_ParticleM SignalJets jets     
    Cut char signal = 1
    Var char bjet
    Var floatGeV MT  
End_Particle 

New_ParticleM SignalHardJets jets
    Cut floatGeV pt > 60.
End_Particle
New_ParticleM SignalBJets jets     
    Cut char signal = 1
    Cut char bjet = 1  
End_Particle 

DiParticle LightLep SignalElectrons SignalMuons
DiParticle Leptons LightLep SignalTaus

Import XAMPPplotting/RunConf/LepHadStau/LeptonTypes/RJR.conf
#########################################################################################################
#                                   Truth Objects                                                       #
#########################################################################################################
New_ParticleM TruthStaus initialParticles
    Var float charge
    Var float CosThetaStar
    Var float TauDecay
    Var float DistDecay
    CombCut OR
        Cut int pdgId |=| 1000015
        Cut int pdgId |=| 2000015        
    End_CombCut
End_Particle

New_ParticleM TruthNeutralinos initialParticles
    Cut int pdgId = 1000021
End_Particle

#New_ParticleM RJ_Z JigSawZ
#    Cut int pdgId |=| 23
#End_Particle

New_ParticleM dilep dilepton
End_Particle
New_MathReader /
	Alias e_tau_imbal
	New_MathReader -
		ParReader SignalElectrons pt[0]
		ParReader SignalTaus pt[0]
	End_MathReader
	New_MathReader +
		ParReader SignalElectrons pt[0]
		ParReader SignalTaus pt[0]
	End_MathReader	
End_MathReader

New_MathReader /
	Alias mu_tau_imbal
	New_MathReader -
		ParReader SignalMuons pt[0]
		ParReader SignalTaus pt[0]
	End_MathReader
	New_MathReader +
		ParReader SignalMuons pt[0]
		ParReader SignalTaus pt[0]
	End_MathReader	
End_MathReader

New_MathReader /
	Alias tau_tau_imbal
	New_MathReader -
		ParReader SignalTaus pt[0]
		ParReader SignalTaus pt[1]
	End_MathReader
	New_MathReader +
		ParReader SignalTaus pt[0]
		ParReader SignalTaus pt[1]
	End_MathReader	
End_MathReader    


defineMultiLine PreSelection
    NumParCut SignalBJets = 0
    EvCut floatGeV TruthMET_met > 40
    ### OS selection of real lepton tau pairs
    CombCut OR
        EvCut char OS_TauEle = 1
        EvCut char OS_TauMuo = 1
        EvCut char OS_TauTau = 1
        ### Account for the fact that the tau
        ### has been replaced by a jet 
        CombCut AND
            NumParCut LightLep = 1
            NumParCut SignalTaus = 0
            NumParCut SignalJets > 0
        End_CombCut
    End_CombCut
    
End_MultiLine
New_ParticleM TrueDiLep trueDilepton
    Cut floatGeV m > 10
End_Particle
