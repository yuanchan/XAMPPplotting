Import XAMPPplotting/RunConf/LepHadStau/RunConfig.conf
Import XAMPPplotting/RunConf/LepHadStau/CommonConfigs/MuonTrigConditions.conf

# Note: the offline requirements apply to the baseline muons
# since sme CRs rely on anti-signal muons

#single-muon triggers
New_CondWeight TriggerWeightMuons

# 2015 period D3-end: mu20 OR mu40
    New_Conditional
        Weight MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu40
        @Condition_mu20_mu40
    End_Conditional

# 2016 period A: mu24 OR mu40
    New_Conditional
        Weight MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu40
        @Condition_mu24_mu40
    End_Conditional

# 2016 period B-D3: mu24 OR mu50
    New_Conditional
        Weight MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu50
        @Condition_mu24_mu50
    End_Conditional

# 2016 >=D4, 2017, 2018 mu26 OR mu50
    New_Conditional
        Weight MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50
        @Condition_mu26_mu50
    End_Conditional

End_CondWeight

#muon-tau triggers
New_CondWeight TriggerWeightTausMuons
    CondAppliedCut TriggerWeightMuons = 0

#2015:          HLT_mu14_tau35_medium1_tracktwo 
#2016, 2017:    HLT_mu14_ivarloose_tau35_medium1_tracktwo
#2018 <= H2:    HLT_mu14_ivarloose_tau35_medium1_tracktwoEF
#2018 >= I1:    HLT_mu14_ivarloose_tau35_medium1_tracktwoEF OR 
#               HLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA
    #tau side
    New_Conditional
        Weight TauWeightTrigHLT_tau35_medium1_tracktwo
        CombCut AND
            NumParCut BaseMuons > 0
            NumParCut BaseTaus > 0
            CombCut OR
                @Condition_mu14_tau35_2015_D3_J6
                @Condition_mu14_tau35_2016_A3_A10
                @Condition_mu14_tau35_2016_B1_D3
                @Condition_mu14_tau35_2016_D4_L11_OR_2017_B1_K6        
                    #NOTE: TMP - remove when EF and MVA SFs are provided
                @Condition_mu14_tau35_2018_B1_H2
                @Condition_mu14_tau35_2018_I1_Q2
            End_CombCut
        End_CombCut
    End_Conditional

    #NOTE: SWITCH to EF when branch is available
    #New_Conditional
    #    Weight TauWeightTrigHLT_tau35_medium1_tracktwoEF
    #    CombCut OR
    #            @Condition_mu14_tau35_2018_B1_H2
    #            @Condition_mu14_tau35_2018_I1_Q2
     #   End_CombCut
    #End_Conditional

    #NOTE: SWITCH to MVA when branch is available
    #New_Conditional
    #    Weight TauWeightTrigHTL_tau35_mediumRNN_tracktwoMVA
    #    @Condition_mu14_tau35_2018_I1_Q2
    #End_Conditional

    #muon side
    #2015
    New_Conditional
        Weight MuoWeightTrigHLT_mu14
        CombCut AND
            NumParCut BaseMuons > 0
            NumParCut BaseTaus > 0
            @Condition_mu14_tau35_2015_D3_J6
        End_CombCut
    End_Conditional

    #elsewhere
    # NOTE: assume NOW mu14_ivarloose & mu14 have the same SF (18/7/2019)
    # Update to MuoWeightTrigHLT_mu14_ivarloose when available
    New_Conditional
        Weight MuoWeightTrigHLT_mu14_ivarloose
        CombCut AND
            NumParCut BaseMuons > 0
            NumParCut BaseTaus > 0
            CombCut OR            
                @Condition_mu14_tau35_2016_A3_A10
                @Condition_mu14_tau35_2016_B1_D3
                @Condition_mu14_tau35_2016_D4_L11_OR_2017_B1_K6
                @Condition_mu14_tau35_2018_B1_H2
                @Condition_mu14_tau35_2018_I1_Q2
            End_CombCut
        End_CombCut  
    End_Conditional

End_CondWeight

