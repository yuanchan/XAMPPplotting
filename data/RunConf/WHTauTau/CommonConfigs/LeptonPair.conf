Import XAMPPplotting/RunConf/WHTauTau/RunConfig.conf
Import XAMPPplotting/RunConf/WHTauTau/CommonConfigs/Weights.conf

#default definitions
defineVar SelectLepton Signal
defineVar VetoLepton Loose
defineVar SelectTau Base
defineVar SelectFlavour LightLeptons

#Veto variable
defineMultiLine VetoTau
    @{VetoLepton}LightLeptons
End_MultiLine

#single object multiplicity requirement variable
defineMultiLine SingleObject
    @{SelectLepton}LightLeptons
End_MultiLine

# WH->HadHad Selection
defineMultiLine Tau_Tau_Selection

    ### Selection
    NumParCut @{SelectLepton}LightLeptons > 0
    NumParCut @{SelectLepton}LightLeptons = 1
    NumParCut @{SelectLepton}@{SelectFlavour} = 1
    NumParCut @{SelectTau}Taus >= 2
    #NumParCut @{SingleObject} = 1

    ### Veto signal leptons or/and taus
    NumParCut @{VetoLepton}LightLeptons = 0    
    NumParCut @{VetoTau} = 0

    ### Make sure that the trigger-selection is properly applied
    CondAppliedCut TriggerWeight@{SelectFlavour} = 1
    

    #weights
    @ApplyWeights

    #charge product
    New_MathReader *
        Alias @{SelectTau}Tau_@{SelectTau}Tau_Q
        ParReader @{SelectTau}Taus charge[0]
        ParReader @{SelectTau}Taus charge[1]        
    End_MathReader
  
End_MultiLine

defineMultiLine OS_Tau_Tau_Selection
    @Tau_Tau_Selection
    EvCut charge @{SelectTau}Tau_@{SelectTau}Tau_Q < 0  
End_MultiLine





# WH->HadHad Selection
defineMultiLine Tau_Tau_SingleFailTau_Selection

    ### Selection
    NumParCut @{SelectLepton}LightLeptons > 0
    NumParCut @{SelectLepton}LightLeptons = 1
    NumParCut @{SelectLepton}@{SelectFlavour} = 1
    NumParCut @{SelectTau}Taus >= 1
    #NumParCut @{SingleObject} = 1

    ### Veto signal leptons or/and taus
    NumParCut @{VetoLepton}LightLeptons = 0    
    NumParCut @{VetoTau} = 0

    ### Make sure that the trigger-selection is properly applied
    CondAppliedCut TriggerWeight@{SelectFlavour} = 1
    

    #weights
    @ApplyWeights

    #charge product
    New_MathReader *
        Alias @{SelectTau}Tau_@{SelectTau}Tau_Q
        ParReader @{SelectTau}Taus charge[0]
        ParReader @{SelectTau}Taus charge[1]        
    End_MathReader
  
End_MultiLine

defineMultiLine OS_Tau_Tau_SingleFailTau_Selection
    @Tau_Tau_SingleFailTau_Selection
    EvCut charge @{SelectTau}Tau_@{SelectTau}Tau_Q < 0  
End_MultiLine







defineMultiLine Lep_Tau_Selection

    ### Selection
    NumParCut @{SelectLepton}LightLeptons > 0
    NumParCut @{SelectLepton}LightLeptons = 1
    NumParCut @{SelectLepton}@{SelectFlavour} = 1
    NumParCut @{SelectTau}Taus >= 1
    NumParCut @{SingleObject} = 1

    ### Veto signal leptons or/and taus
    NumParCut @{VetoLepton}LightLeptons = 0    
    NumParCut @{VetoTau} = 0

    ### Make sure that the trigger-selection is properly applied
    CondAppliedCut TriggerWeight@{SelectFlavour} = 1
    
    #weights
    @ApplyWeights

    #charge product
    New_MathReader *
        Alias @{SelectLepton}Lep_@{SelectTau}Tau_Q
        ParReader @{SelectLepton}LightLeptons charge[0]
        ParReader @{SelectTau}Taus charge[0]        
    End_MathReader
  
End_MultiLine

defineMultiLine Lep_Tau_Selection_QCDCR

    ### Selection
    NumParCut @{SelectLepton}LightLeptons > 0
    NumParCut @{SelectLepton}LightLeptons = 1
    NumParCut @{SelectLepton}@{SelectFlavour} = 1
    NumParCut @{SelectTau}Taus >= 1
    NumParCut @{SingleObject} = 1

    ### Veto signal leptons or/and taus
    NumParCut @{VetoLepton}LightLeptons = 0    
    NumParCut @{VetoTau} = 0

    ### Make sure that the trigger-selection is properly applied
    #CondAppliedCut TriggerWeight@{SelectFlavour} = 1
    
    #weights
    @ApplyWeights

    #charge product
    New_MathReader *
        Alias @{SelectLepton}Lep_@{SelectTau}Tau_Q
        ParReader @{SelectLepton}LightLeptons charge[0]
        ParReader @{SelectTau}Taus charge[0]        
    End_MathReader
  
End_MultiLine

defineMultiLine Tau_Tau_Selection_QCDCR

    ### Selection
    NumParCut @{SelectLepton}LightLeptons > 0
    NumParCut @{SelectLepton}LightLeptons = 1
    NumParCut @{SelectLepton}@{SelectFlavour} = 1
    NumParCut @{SelectTau}Taus >= 2
    #NumParCut @{SingleObject} = 1

    ### Veto signal leptons or/and taus
    #NumParCut @{VetoLepton}LightLeptons = 0    
    #NumParCut @{VetoTau} = 0

    ### Make sure that the trigger-selection is properly applied
    #CondAppliedCut TriggerWeight@{SelectFlavour} = 1
    
    #weights
    @ApplyWeights

    #charge product
    New_MathReader *
        Alias @{SelectTau}Tau_@{SelectTau}Tau_Q
        ParReader @{SelectTau}Taus charge[0]
        ParReader @{SelectTau}Taus charge[1]        
    End_MathReader
  
End_MultiLine

defineMultiLine OS_Tau_Tau_Selection_QCDCR
    @Tau_Tau_Selection_QCDCR
    EvCut charge @{SelectTau}Tau_@{SelectTau}Tau_Q < 0  
End_MultiLine

defineMultiLine OS_Lep_Tau_Selection
    @Lep_Tau_Selection
    EvCut charge @{SelectLepton}Lep_@{SelectTau}Tau_Q < 0  
End_MultiLine

defineMultiLine SS_Lep_Tau_Selection
    @Lep_Tau_Selection
    EvCut charge @{SelectLepton}Lep_@{SelectTau}Tau_Q > 0  
End_MultiLine



##Inclusive Light lepton Selection
defineMultiLine LepIncl_Tau_Selection

    ### Selection
    NumParCut @{SelectLepton}LightLeptons > 0
    NumParCut @{SelectLepton}LightLeptons = 1
    #NumParCut @{SelectLepton}@{SelectFlavour} = 1
    NumParCut @{SelectTau}Taus >= 1
    NumParCut @{SingleObject} = 1

    ### Veto signal leptons or/and taus
    NumParCut @{VetoLepton}LightLeptons = 0    
    NumParCut @{VetoTau} = 0

    ### Make sure that the trigger-selection is properly applied
    #CondAppliedCut TriggerWeight@{SelectFlavour} = 1


    CombCut OR
    	    CombCut AND
    	    	 CondAppliedCut TriggerWeightElectrons = 1
		 NumParCut SignalMuons = 0
		 #ele weights
		 #defineVar EleW EleWeightReco EleWeightId
            End_CombCut
    	    CombCut AND
    	    	 CondAppliedCut TriggerWeightMuons = 1
		 NumParCut SignalElectrons = 0
		 #defineVar MuoW MuoWeightReco MuoWeightTTVA
            End_CombCut
    End_CombCut
    
    #weights
    @ApplyWeights

    #charge product
    New_MathReader *
        Alias @{SelectLepton}Lep_@{SelectTau}Tau_Q
        ParReader @{SelectLepton}LightLeptons charge[0]
        ParReader @{SelectTau}Taus charge[0]        
    End_MathReader
  
End_MultiLine



defineMultiLine OS_LepIncl_Tau_Selection
    @LepIncl_Tau_Selection
    EvCut charge @{SelectLepton}Lep_@{SelectTau}Tau_Q < 0  
End_MultiLine





#dilepton
defineMultiLine DiLep_Selection
    #multiplicity
    NumParCut @{SelectLepton}LightLeptons > 1
    NumParCut @{SelectLepton}@{SelectFlavour} = 2

    #Z candidate
    NumParCut ZCand = 1

    #trigger
    CondAppliedCut TriggerWeight@{SelectFlavour} = 1

    #charge product
    New_MathReader *
        Alias Di@{SelectLepton}_Q
        ParReader @{SelectLepton}LightLeptons charge[0]
        ParReader @{SelectLepton}LightLeptons charge[1]
    End_MathReader

    #weights
    @ApplyWeights


End_MultiLine

defineMultiLine OS_DiLep
    @DiLep_Selection
    EvCut charge Di@{SelectLepton}_Q < 0  
End_MultiLine

#leading 1p or 3p taus
New_MathReader -
        Alias BaseTauOrder_pT
        ParReader BaseTaus1P pt[0]
        ParReader BaseTaus3P pt[0]
End_MathReader


defineMultiLine BaseTauLeading1P
    CombCut AND
            NumParCut BaseTaus1P > 0
            CombCut OR
                    NumParCut BaseTaus3P = 0
                    EvCut pt  BaseTauOrder_pT > 0
            End_CombCut
    End_CombCut
End_MultiLine

defineMultiLine BaseTauLeading3P
    CombCut AND
            NumParCut BaseTaus3P > 0
                      CombCut OR
                      NumParCut BaseTaus1P = 0
                      EvCut pt  BaseTauOrder_pT < 0
            End_CombCut
    End_CombCut
End_MultiLine
