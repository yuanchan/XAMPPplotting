## This file is the RunConfig for the mono-h(bb) analysis
## for the resolved two-muon control region.

#---------------------------------------------------------------------------------------
# The basic setup is imported from the common run config and the common definitions file
#---------------------------------------------------------------------------------------
Import XAMPPplotting/RunConf/MonoH/Common/RunConfig.conf
Import XAMPPplotting/RunConf/MonoH/Common/RunConfig_commonDefinitions.conf


#----------------------------------------------------------------------------------------------------
# Option to re-calculate the b-tagging SFs.
# If this is used, TrackJetWeight_New must be added to the list of weights and TrackJetWeight removed
#----------------------------------------------------------------------------------------------------
#Import XAMPPplotting/RunConf/MonoH/Common/RunConfig_bTagSF_Resolved.conf


#-------------------------------------------------------------------------
## Applying correct muon trigger SFs for the different data-taking periods
#-------------------------------------------------------------------------
Import XAMPPplotting/RunConf/MonoH/Triggers/MuonTriggers.conf


#------------------------------
## Region definitions
## Define variables for regions
#------------------------------
defineVar Channel CR2_mumu
defineVar PTV MetTSTlepInvis_met
defineVar BJets N_BJets_04
defineVar Topology Resolved
defineVar HJet Jet_BLight


## --------------------------------
## Region specific cuts and weights
## --------------------------------

## Weights
## The weights which should be read from the trees. DO NOT add the PileUp weight to the list in any case
Weights GenWeight GenWeightMCSampleMerging EleWeight MuoWeight TauWeight JetWeightJVT JetWeightBTag MuonTriggerWeight Znunu_Normalization Znunu_Merging

# MET significance cut to suppress ttbar
EvCut float MetTST_Significance_noPUJets_noSoftTerm < 5.

# mT variables used for ttbar suppresion
 EvCut floatGeV mT_METclosestBJet > 170.
 EvCut floatGeV mT_METfurthestBJet > 200.

@common_TwoLepton

EvCut int N_SignalMuons = 2

@common_Resolved

CombCut AND
    EvCut floatGeV m_ll >= 81.1876
    EvCut floatGeV m_ll <= 101.1876.
End_CombCut

# anti-QCD cut
EvCut float DeltaPhiMin3 |>=| 0.34906585039

# METlepInvis significance cuts
EvCut float MetTST_Significance_noPUJets_noSoftTerm_lepInvis > 12.

# pt_jj > 100 GeV for 150 GeV < MET < 350 GeV and pt_jj > 350 GeV for 350 GeV < MET < 500 GeV
@pt_jj_corr_MET_cut

# blinding m_jj around the Higgs mass (always have this applied when comparing data17 and mc16d! can be removed for data1516 and mc16a)
#@Blinding_Resolved


# special option for creating plots in Higgs mass window, should not be active if not explicitly requested
# @HiggsWindow_Resolved


#----------------------------------------------------------------------------------------------------------------------
## Config to produce plots with 2 b-tagged small-R jets, inclusive in the resolved region and in the different MET, njets bins
#----------------------------------------------------------------------------------------------------------------------
ImportAlways XAMPPplotting/RunConf/MonoH/Common/RunConfig_Regions_ResolvedMETBinning_2b.conf


#------------------------------------------------------------------------
## Config to produce plots for the 3p b-tag region
#------------------------------------------------------------------------
ImportAlways XAMPPplotting/RunConf/MonoH/Common/RunConfig_Regions_ResolvedMETBinning_3pb.conf


#-----------------------------------------------
## Config to produce plots for V+jets background
#-----------------------------------------------
ImportAlways XAMPPplotting/RunConf/MonoH/Common/RunConfig_Regions_ResolvedMETBinning_Vjets_2b.conf
ImportAlways XAMPPplotting/RunConf/MonoH/Common/RunConfig_Regions_ResolvedMETBinning_Vjets_3pb.conf
