## This file is the RunConfig for the mono-h(bb) analysis
## for the merged signal region. It extends RunConfig.conf
## where all the common options are already defined.


## Basic setup 
## ---------------------------------------------------------------------------
# The basic setup is imported from the common run config file
Import XAMPPplotting/RunConf/MonoH/Common/RunConfig.conf

## Weights
## ---------------------------------------------------------------------------
# The weights which should be read from the trees. DO NOT add the PileUp weight to the list in any case
Weights GenWeight EleWeight MuoWeight TauWeight TrackJetWeight MET_TriggerSF CorrsAndSysts_SF

## Skimming cuts
## ---------------------------------------------------------------------------
EvCut floatGeV MetTST_met > 150. 
EvCut int N_Jets10 >= 1
EvCut floatGeV m_J > 40.

Region 0L_Merged_skimming
## ------------------------------------------------------------------------------------------------------------------------------------------------------
# no additional cuts after skimming cuts

## Non-skimming cuts
## ---------------------------------------------------------------------------

Region 0L_Merged_Met_gt_500
## ------------------------------------------------------------------------------------------------------------------------------------------------------
EvCut floatGeV MetTST_met > 500. 

Region 0L_Merged_b2_mpt_30
## ------------------------------------------------------------------------------------------------------------------------------------------------------
EvCut floatGeV MetTST_met > 500. 


Region 0L_Merged_DeltaPhiMin3
## ------------------------------------------------------------------------------------------------------------------------------------------------------
EvCut floatGeV MetTST_met > 500. 


# anti-QCD cuts
EvCut float DeltaPhiMin3 |>=| 0.34906585039


Region 0L_Merged_mass_blinding
## ------------------------------------------------------------------------------------------------------------------------------------------------------
EvCut floatGeV MetTST_met > 500. 


# anti-QCD cuts
EvCut float DeltaPhiMin3 |>=| 0.34906585039

# blinding m_J around Higgs mass (always have this applied when comparing data and MC!!!)
CombCut OR
    EvCut floatGeV m_J < 70.
    EvCut floatGeV m_J > 140.
End_CombCut

Region 0L_Merged_tau
## ------------------------------------------------------------------------------------------------------------------------------------------------------
EvCut floatGeV MetTST_met > 500. 


# anti-QCD cuts
EvCut float DeltaPhiMin3 |>=| 0.34906585039

# blinding m_J around Higgs mass (always have this applied when comparing data and MC!!!)
CombCut OR
    EvCut floatGeV m_J < 70.
    EvCut floatGeV m_J > 140.
End_CombCut

## Common cuts for merged selections
EvCut int N_SignalTaus = 0

Region 0L_Merged_extended_tau
## ------------------------------------------------------------------------------------------------------------------------------------------------------
EvCut floatGeV MetTST_met > 500. 

# anti-QCD cuts
EvCut float DeltaPhiMin3 |>=| 0.34906585039

# blinding m_J around Higgs mass (always have this applied when comparing data and MC!!!)
CombCut OR
    EvCut floatGeV m_J < 70.
    EvCut floatGeV m_J > 140.
End_CombCut

## Common cuts for merged selections
EvCut int N_SignalTaus = 0
EvCut int N_TausExtended_Merged = 0

Region 0L_Merged_bveto
## ------------------------------------------------------------------------------------------------------------------------------------------------------
EvCut floatGeV MetTST_met > 500. 


# anti-QCD cuts
EvCut float DeltaPhiMin3 |>=| 0.34906585039

# blinding m_J around Higgs mass (always have this applied when comparing data and MC!!!)
CombCut OR
    EvCut floatGeV m_J < 70.
    EvCut floatGeV m_J > 140.
End_CombCut

## Common cuts for merged selections
EvCut int N_SignalTaus = 0
EvCut int N_TausExtended_Merged = 0
EvCut int N_BTags_not_associated_02 = 0

Region 0L_Merged_ht
## ------------------------------------------------------------------------------------------------------------------------------------------------------
EvCut floatGeV MetTST_met > 500. 


# anti-QCD cuts
EvCut float DeltaPhiMin3 |>=| 0.34906585039

# blinding m_J around Higgs mass (always have this applied when comparing data and MC!!!)
CombCut OR
    EvCut floatGeV m_J < 70.
    EvCut floatGeV m_J > 140.
End_CombCut

## Common cuts for merged selections
EvCut int N_SignalTaus = 0
EvCut int N_TausExtended_Merged = 0
EvCut int N_BTags_not_associated_02 = 0
EvCut float HtRatioMerged <= 0.57

Region 0L_Merged_deltar_ratio
## ------------------------------------------------------------------------------------------------------------------------------------------------------
EvCut floatGeV MetTST_met > 500. 


# anti-QCD cuts
EvCut float DeltaPhiMin3 |>=| 0.34906585039

# blinding m_J around Higgs mass (always have this applied when comparing data and MC!!!)
CombCut OR
    EvCut floatGeV m_J < 70.
    EvCut floatGeV m_J > 140.
End_CombCut

## Common cuts for merged selections
EvCut int N_SignalTaus = 0
EvCut int N_TausExtended_Merged = 0
EvCut int N_BTags_not_associated_02 = 0
EvCut float HtRatioMerged <= 0.57

# applying DeltaR cut between two leading associated VR track jets - note: this only works in ntuples from production tag 0603 or higher.
# for everything below the jobs will crash and you have to comment these lines out
CombCut AND
   EvCut int N_associated_Jets02 >= 2
   EvCut float DeltaR_ratio > 1
End_CombCut   

Region 0L_Merged_mass_window
## ------------------------------------------------------------------------------------------------------------------------------------------------------
EvCut floatGeV MetTST_met > 500. 


# anti-QCD cuts
EvCut float DeltaPhiMin3 |>=| 0.34906585039

# blinding m_J around Higgs mass (always have this applied when comparing data and MC!!!)
CombCut OR
    EvCut floatGeV m_J < 70.
    EvCut floatGeV m_J > 140.
End_CombCut

## Common cuts for merged selections
EvCut int N_SignalTaus = 0
EvCut int N_TausExtended_Merged = 0
EvCut int N_BTags_not_associated_02 = 0
EvCut float HtRatioMerged <= 0.57

# applying DeltaR cut between two leading associated VR track jets - note: this only works in ntuples from production tag 0603 or higher.
# for everything below the jobs will crash and you have to comment these lines out
CombCut AND
   EvCut int N_associated_Jets02 >= 2
   EvCut float DeltaR_ratio > 1
End_CombCut   

# Mass range cut for Higgs candidate mass (same as fit input histogram)
CombCut AND
   EvCut floatGeV m_J > 50.
   EvCut floatGeV m_J < 270.
End_CombCut

Region SR_Merged
## ------------------------------------------------------------------------------------------------------------------------------------------------------
EvCut floatGeV MetTST_met > 500. 

# anti-QCD cuts
EvCut float DeltaPhiMin3 |>=| 0.34906585039

# blinding m_J around Higgs mass (always have this applied when comparing data and MC!!!)
CombCut OR
    EvCut floatGeV m_J < 70.
    EvCut floatGeV m_J > 140.
End_CombCut

## Common cuts for merged selections
EvCut int N_SignalTaus = 0
EvCut int N_TausExtended_Merged = 0
EvCut int N_BTags_not_associated_02 = 0
EvCut float HtRatioMerged <= 0.57

# applying DeltaR cut between two leading associated VR track jets - note: this only works in ntuples from production tag 0603 or higher.
# for everything below the jobs will crash and you have to comment these lines out
CombCut AND
   EvCut int N_associated_Jets02 >= 2
   EvCut float DeltaR_ratio > 1
End_CombCut   

# Mass range cut for Higgs candidate mass (same as fit input histogram)
CombCut AND
   EvCut floatGeV m_J > 50.
   EvCut floatGeV m_J < 270.
End_CombCut

Region SR_Merged_0b
## ------------------------------------------------------------------------------------------------------------------------------------------------------
EvCut floatGeV MetTST_met > 500. 

# anti-QCD cuts
EvCut float DeltaPhiMin3 |>=| 0.34906585039

# blinding m_J around Higgs mass (always have this applied when comparing data and MC!!!)
CombCut OR
    EvCut floatGeV m_J < 70.
    EvCut floatGeV m_J > 140.
End_CombCut

## Common cuts for merged selections
EvCut int N_SignalTaus = 0
EvCut int N_TausExtended_Merged = 0
EvCut int N_BTags_not_associated_02 = 0
EvCut float HtRatioMerged <= 0.57

# applying DeltaR cut between two leading associated VR track jets - note: this only works in ntuples from production tag 0603 or higher.
# for everything below the jobs will crash and you have to comment these lines out
CombCut AND
   EvCut int N_associated_Jets02 >= 2
   EvCut float DeltaR_ratio > 1
End_CombCut   

# Mass range cut for Higgs candidate mass (same as fit input histogram)
CombCut AND
   EvCut floatGeV m_J > 50.
   EvCut floatGeV m_J < 270.
End_CombCut

EvCut int N_BTags_associated_02 = 0

Region SR_Merged_1b
## ------------------------------------------------------------------------------------------------------------------------------------------------------
EvCut floatGeV MetTST_met > 500. 

# anti-QCD cuts
EvCut float DeltaPhiMin3 |>=| 0.34906585039

# blinding m_J around Higgs mass (always have this applied when comparing data and MC!!!)
CombCut OR
    EvCut floatGeV m_J < 70.
    EvCut floatGeV m_J > 140.
End_CombCut

## Common cuts for merged selections
EvCut int N_SignalTaus = 0
EvCut int N_TausExtended_Merged = 0
EvCut int N_BTags_not_associated_02 = 0
EvCut float HtRatioMerged <= 0.57

# applying DeltaR cut between two leading associated VR track jets - note: this only works in ntuples from production tag 0603 or higher.
# for everything below the jobs will crash and you have to comment these lines out
CombCut AND
   EvCut int N_associated_Jets02 >= 2
   EvCut float DeltaR_ratio > 1
End_CombCut   

# Mass range cut for Higgs candidate mass (same as fit input histogram)
CombCut AND
   EvCut floatGeV m_J > 50.
   EvCut floatGeV m_J < 270.
End_CombCut

EvCut int N_BTags_associated_02 = 1

Region SR_Merged_2b
## ------------------------------------------------------------------------------------------------------------------------------------------------------
EvCut floatGeV MetTST_met > 500. 

# anti-QCD cuts
EvCut float DeltaPhiMin3 |>=| 0.34906585039

# blinding m_J around Higgs mass (always have this applied when comparing data and MC!!!)
CombCut OR
    EvCut floatGeV m_J < 70.
    EvCut floatGeV m_J > 140.
End_CombCut

## Common cuts for merged selections
EvCut int N_SignalTaus = 0
EvCut int N_TausExtended_Merged = 0
EvCut int N_BTags_not_associated_02 = 0
EvCut float HtRatioMerged <= 0.57

# applying DeltaR cut between two leading associated VR track jets - note: this only works in ntuples from production tag 0603 or higher.
# for everything below the jobs will crash and you have to comment these lines out
CombCut AND
   EvCut int N_associated_Jets02 >= 2
   EvCut float DeltaR_ratio > 1
End_CombCut   

# Mass range cut for Higgs candidate mass (same as fit input histogram)
CombCut AND
   EvCut floatGeV m_J > 50.
   EvCut floatGeV m_J < 270.
End_CombCut

EvCut int N_BTags_associated_02 = 2
