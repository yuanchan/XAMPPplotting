## This file is the RunConfig for the mono-h(bb) analysis
## for the resolved signal regions. It extends RunConfig.conf
## where all the common options are already defined.


## Basic setup 
## ---------------------------------------------------------------------------
# The basic setup is imported from the common run config file
Import XAMPPplotting/RunConf/MonoH/Common/RunConfig.conf


Import XAMPPplotting/RunConf/MonoH/Common/RunConfig_bTagSF_Resolved.conf

## Weights
## ---------------------------------------------------------------------------
# The weights which should be read from the trees. DO NOT add the PileUp weight to the list in any case
Weights GenWeight EleWeight MuoWeight TauWeight JetWeightJVT JetWeightBTag_New MET_TriggerSF CorrsAndSysts_SF
#JetWeightBTag

## Skimming cuts
## ---------------------------------------------------------------------------
EvCut floatGeV MetTST_met >= 150.
EvCut int N_Jets04 >= 2
EvCut floatGeV m_jj > 40


## Non-skimming cuts
## ---------------------------------------------------------------------------
EvCut floatGeV MetTST_met <= 500.


## Common cuts for resolved selections
Import XAMPPplotting/RunConf/MonoH/Common/RunConfig_common_Resolved.conf

CombCut OR
    CombCut AND
        EvCut int N_Jets04 = 2
        EvCut floatGeV sigjet012ptsum > 120.
    End_CombCut
    CombCut AND
        EvCut int N_Jets04 > 2
        EvCut floatGeV sigjet012ptsum > 150.
    End_CombCut
End_CombCut

# do not import anti-QCD cuts but define locally not all of them are applied
# EvCut float DeltaPhiMin3 |>=| 0.34906585039 not applied here

EvCut float DeltaPhiJJ  |<=| 2.44346095279  # not present in the table in the paper                                    
EvCut float DeltaPhiMetJJ  |>=| 2.09439510239

# blinding m_jj around the Higgs mass - disabled here
# Import XAMPPplotting/RunConf/MonoH/Common/RunConfig_Blinding_Resolved.conf - do not apply blinding bere but in individual regions of the ABCD run config additions


## Region definitions
## ---------------------------------------------------------------------------
# Definition of regions dependent on MET, topology and b-tags

## Define variables for region definitions
defineVar Channel SR
defineVar PTV MetTST_met
defineVar BJets N_BJets_04
defineVar Topology Resolved
defineVar HJet Jet_BLight


## Region definitions for fitting regions
Import XAMPPplotting/RunConf/MonoH/Common/RunConfig_Regions_common_2b.conf
Import XAMPPplotting/RunConf/MonoH/Common/RunConfig_Regions_ResolvedMETBinning_2b.conf

## Region definitions for ABCD method
Import XAMPPplotting/RunConf/MonoH/Other/RunConfig_Multijet_ABCD.conf
