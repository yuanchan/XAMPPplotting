## This file is the RunConfig used for the calculation of the MET trigger SFs
## in the mono-h(bb) analysis. Events are selected with muon triggers instead
## of the MET triggers. 

## Basic setup 
## ---------------------------------------------------------------------------
# The basic setup is imported from the common run config file
Import XAMPPplotting/RunConf/MonoH/Common/RunConfig.conf

# Decisions and SFs for muon triggers
Import XAMPPplotting/RunConf/MonoH/Triggers/MuonTriggers.conf  

Weights GenWeight JetWeightJVT JetWeightBTag TauWeight MuoWeight MuonTriggerWeight
                                                                                                  
##################################################################
##### Event selection 
####  (as SR resolved or CR1 resolved, but whithout cut on trigger, MET)
###################################################################


## Skimming cuts
## ---------------------------------------------------------------------------
#EvCut char IsMETTrigPassed = 1

CombCut AND
    EvCut int N_SignalElectrons = 0
    EvCut int N_BaselineElectrons = 0
End_CombCut

EvCut int N_Jets04 >= 2
EvCut floatGeV m_jj > 40

CombCut AND
    EvCut int N_SignalMuons = 1
    EvCut int N_BaselineMuons = 1
End_CombCut
 
#EvCut floatGeV MetTSTmuInvis_met >= 150.

## Non-skimming cuts
## ---------------------------------------------------------------------------

#EvCut floatGeV MetTSTmuInvis_met <= 500.


CombCut OR
    CombCut AND
        EvCut int N_Jets04 = 2
        EvCut floatGeV sigjet012ptsum > 120.
    End_CombCut
    CombCut AND
        EvCut int N_Jets04 > 2
        EvCut floatGeV sigjet012ptsum > 150.
    End_CombCut
End_CombCut

## Common cuts for resolved selections
Import XAMPPplotting/RunConf/MonoH/Common/RunConfig_common_Resolved.conf

# anti-QCD cuts
Import XAMPPplotting/RunConf/MonoH/Common/RunConfig_antiQCD_Resolved.conf

# MET significance cut (should be used only for the SF calculation for SR, but not for CR1)
EvCut float MetTST_Significance_noPUJets_noSoftTerm > 16.

###################
#### Regions
###################

# Number of b-tags

Region CR1_Resolved_2b
EvCut int N_BJets_04 = 2

Region CR1_Resolved_1b
EvCut int N_BJets_04 = 1


# MET triggers for different runs

Region TrigHLT_xe70_mht_and_CorrectRun

PeriodCut <= 296939
EvCut char TrigHLT_xe70_mht = 1
EvCut char IsMETTrigPassed = 1


Region TrigHLT_xe70_mht_Run

PeriodCut <= 296939


Region TrigHLT_xe90_mht_L1XE50_and_CorrectRun

PeriodCut >= 296939
PeriodCut <= 302872
EvCut char TrigHLT_xe90_mht_L1XE50 = 1
EvCut char IsMETTrigPassed = 1


Region TrigHLT_xe90_mht_L1XE50_Run

PeriodCut >= 296939
PeriodCut <= 302872


#Region TrigHLT_xe100_mht_L1XE50_and_CorrectRun
#
#PeriodCut >= 302919
#PeriodCut <= 303892
#EvCut char TrigHLT_xe100_mht_L1XE50 = 1
#EvCut char IsMETTrigPassed = 1
#
#
#Region TrigHLT_xe100_mht_L1XE50_Run
#
#PeriodCut >= 302919
#PeriodCut <= 303892


Region TrigHLT_xe110_mht_L1XE50_and_CorrectRun

PeriodCut >= 302919
PeriodCut <= 311481
EvCut char TrigHLT_xe110_mht_L1XE50 = 1
EvCut char IsMETTrigPassed = 1


Region TrigHLT_xe110_mht_L1XE50_Run

PeriodCut >= 302919
PeriodCut <= 311481


Region TrigHLT_xe110_pufit_L1XE55_and_CorrectRun

PeriodCut >= 324320
EvCut char TrigHLT_xe110_pufit_L1XE55 = 1
EvCut char IsMETTrigPassed = 1


Region TrigHLT_xe110_pufit_L1XE55_Run

PeriodCut >= 324320
