#########################################################################################
## INFO: New structure of XAMPPplotting configs
## There will be two sets of RunConfigs: One to process the analysis wihh the CONF cuts
## (except for the MET track cut) and one including the new event selection.
## A new RunConfig "RunConfig_commonDefinitions.conf", which includes a set of common
## definitions used by both event selections and all regions, e.g. the MultiLine
## statement "commonResolved" includes cuts like >= 2 jets, METproxy > 150 GeV etc.
## which is part in every resolved event selection, i.e. SR, CR1 and CR2 for the old and
## new event selection. All cuts, which differ between the regions, e.g. the new mT cuts,
## are always listed in the single region specific RunConfig, like this file here.
## With this new file RunConfig_commonDefinitions.conf many of the old RunConfigs are
## obsolete, e.g. RunConifg_common_SR_Resolved.conf, RunConfig_comon_Resolved.conf, etc.
## and should be deleted. Also the obsolete V+Jets RunConfigs need to be deleted.
## As the current V+jets RunConfigs are very long, some MultiLine statements have been
## added to RunConfig_commonDefinitions.conf (V_ll, etc.) which allow to make the
## RunConfigs much shorter. So these need to be implemented into the V+jets RunConfigs as
## well. (Though this is now not the biggest priority.) Another thing, which needs to
## be implemented into the configs for the resolved regions, is the binning in the is
## number of small-R jets. This should be done for the following bins: 2 <= N(jets) <= 3,
## N(jets) == 4 and N(jets) >= 5.
##########################################################################################

## This file is the RunConfig for the mono-h(bb) analysis
## for the resolved signal region

#---------------------------------------------------------------------------------------
# The basic setup is imported from the common run config and the common definitions file
#---------------------------------------------------------------------------------------
Import XAMPPplotting/RunConf/MonoH/Common/RunConfig.conf
Import XAMPPplotting/RunConf/MonoH/Common/RunConfig_commonDefinitions.conf


#----------------------------------------------------------------------------------------------------
# Option to re-calculate the b-tagging SFs.
# If this is used, TrackJetWeight_New must be added to the list of weights and TrackJetWeight removed
#----------------------------------------------------------------------------------------------------
#Import XAMPPplotting/RunConf/MonoH/Common/RunConfig_bTagSF_Resolved.conf


#------------------------------
## Region definitions
## Define variables for regions
#------------------------------
defineVar Channel SR
defineVar PTV MetTST_met
defineVar BJets N_BJets_04
defineVar Topology Resolved
defineVar HJet Jet_BLight


## --------------------------------
## Region specific cuts and weights
## --------------------------------

## ---------------------------------------------------------------------------
## Weights
# The weights which should be read from the trees. DO NOT add the PileUp weight to the list in any case
## ---------------------------------------------------------------------------
Weights GenWeight GenWeightMCSampleMerging EleWeight MuoWeight TauWeight JetWeightJVT JetWeightBTag MET_TriggerSF Znunu_Normalization Znunu_Merging

# mT variables used for ttbar suppresion
EvCut floatGeV mT_METclosestBJet > 170.
EvCut floatGeV mT_METfurthestBJet > 200.

@common_ZeroLepton
@common_Resolved

# anti-QCD cut
EvCut float DeltaPhiMin3 |>=| 0.34906585039

# MET significance cut
EvCut float MetTST_Significance_noPUJets_noSoftTerm > 12.

# pt_jj > 100 GeV for 150 GeV < MET < 350 GeV and pt_jj > 350 GeV for 350 GeV < MET < 500 GeV
@pt_jj_corr_MET_cut

# blinding m_jj around the Higgs mass (always have this applied when comparing data17 and mc16d! can be removed for data1516 and mc16a)
@Blinding_Resolved


# special option for creating plots in Higgs mass window, should not be active if not explicitly requested
# @HiggsWindow_Resolved


#----------------------------------------------------------------------------------------------------------------------
## Config to produce plots with 2 b-tagged small-R jets, inclusive in the resolved region and in the different MET, njets bins
#----------------------------------------------------------------------------------------------------------------------
ImportAlways XAMPPplotting/RunConf/MonoH/Common/RunConfig_Regions_ResolvedMETBinning_2b.conf


#------------------------------------------------------------------------
## Config to produce plots for the 3p b-tag region
#------------------------------------------------------------------------
ImportAlways XAMPPplotting/RunConf/MonoH/Common/RunConfig_Regions_ResolvedMETBinning_3pb.conf


#-----------------------------------------------
## Config to produce plots for V+jets background
#-----------------------------------------------
ImportAlways XAMPPplotting/RunConf/MonoH/Common/RunConfig_Regions_ResolvedMETBinning_Vjets_2b.conf
ImportAlways XAMPPplotting/RunConf/MonoH/Common/RunConfig_Regions_ResolvedMETBinning_Vjets_3pb.conf
