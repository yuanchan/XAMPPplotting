## This file is the RunConfig for the mono-h(bb) analysis
## for the merged signal region

#---------------------------------------------------------------------------------------
# The basic setup is imported from the common run config and the common definitions file
#---------------------------------------------------------------------------------------
Import XAMPPplotting/RunConf/MonoH/Common/RunConfig.conf
Import XAMPPplotting/RunConf/MonoH/Common/RunConfig_commonDefinitions.conf


#----------------------------------------------------------------------------------------------------
# Option to re-calculate the b-tagging SFs.
# If this is used, TrackJetWeight_New must be added to the list of weights and TrackJetWeight removed
#----------------------------------------------------------------------------------------------------
#Import XAMPPplotting/RunConf/MonoH/Common/RunConfig_bTagSF_Merged.conf


#------------------------------
## Region definitions
## Define variables for regions
#------------------------------
defineVar Channel SR
defineVar PTV MetTST_met
defineVar BJets N_BTags_associated_02
defineVar BJetsOut N_BTags_not_associated_02
defineVar Topology Merged
defineVar HJet TrackJet_


## --------------------------------
## Region specific cuts and weights
## --------------------------------

## Weights
## The weights which should be read from the trees. DO NOT add the PileUp weight to the list in any case

Weights GenWeight GenWeightMCSampleMerging EleWeight MuoWeight TauWeight TrackJetWeight MET_TriggerSF Znunu_Normalization Znunu_Merging

@common_Merged
@common_ZeroLepton

EvCut float DeltaPhiMin3 |>=| 0.34906585039

# blinding m_J around Higgs mass (always have this applied when comparing data and MC!!!)
@Blinding_Merged


# special option for creating plots in Higgs mass window, should not be active if not explicitly requested
# @HiggsWindow_Merged



#--------------------------------------------------------------------------------------------------------
## Config to produce plots with 2 associated and 0 non-associated b-tagged track jets as used in the CONF
#--------------------------------------------------------------------------------------------------------
ImportAlways XAMPPplotting/RunConf/MonoH/Common/RunConfig_Regions_Merged_2b.conf

#--------------------------------------------------------------------------------------------------------
## Config to produce plots with different MET bins with 2 associated and 0 non-associated b-tagged track jets as used in the CONF
#--------------------------------------------------------------------------------------------------------
ImportAlways XAMPPplotting/RunConf/MonoH/Common/RunConfig_Regions_MergedMETBinning_2b.conf

#----------------------------------------------------------------------------------------------------------
## Config to produce plots with 2 associated and 1+ non-associated b-tagged track b-tagged track jets
#----------------------------------------------------------------------------------------------------------
ImportAlways XAMPPplotting/RunConf/MonoH/Common/RunConfig_Regions_Merged_3pb.conf


#----------------------------------------------------------------------------------------------------------------------
## Config to produce V+jets histograms with different multiplicities of associated / non-associated b-tagged track jets
#----------------------------------------------------------------------------------------------------------------------
ImportAlways XAMPPplotting/RunConf/MonoH/Common/RunConfig_Regions_Merged_Vjets_2b.conf
ImportAlways XAMPPplotting/RunConf/MonoH/Common/RunConfig_Regions_MergedMETBinning_Vjets_2b.conf
ImportAlways XAMPPplotting/RunConf/MonoH/Common/RunConfig_Regions_Merged_Vjets_3pb.conf


